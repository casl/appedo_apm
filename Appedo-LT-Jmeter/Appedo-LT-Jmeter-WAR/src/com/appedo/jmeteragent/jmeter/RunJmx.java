package com.appedo.jmeteragent.jmeter;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.jmeteragent.common.Constants;
import com.appedo.jmeteragent.manager.LogManager;

/**
 * to import the jmx & csv files which is incoming from the appedo
 */
@WebServlet("/RunJmx")
 public class  RunJmx extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	    static final String SAVE_DIR = "/jmxtest/jmx";
	    static final int BUFFER_SIZE = 4096;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RunJmx() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * do process when Get request comes
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * do process when Post request comes
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		request.getHeader("VIA");
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		
		System.out.println("Ip Adress"+ipAddress);       
		String strCommand="";
		strCommand=request.getHeader("command");
		LogManager.infoLog("strCommand="+strCommand);
		try {
			
			if(strCommand.trim().equals("UPLOAD")) {
				doUpload(request,response);
			}
			else if(strCommand.trim().equals("RUN")) {
				doRun(request,response);
			}
			else if (strCommand.trim().equals("RESULTFILE")){
				doSendResultFile(request,response);
			}
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
		}
	}
	/**
	 * to export the result file to appedo 
	 * @param request
	 * @param response
	 * @throws Throwable
	 */
	public void doSendResultFile(HttpServletRequest request, HttpServletResponse response) throws Throwable {
		
		//Members
		String strJtlFileName="";
		
		try {
			
			strJtlFileName=request.getHeader("jtl");
			// send the result file 
	        String strJmxResultFilePath=Constants.JTLDIR+strJtlFileName;
	        String txtbody = new String(Files.readAllBytes(Paths.get(strJmxResultFilePath)));
	        response.getWriter().print(txtbody);
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
	}
	
	/**
	 * to import the jmx & csv files
	 * @param request
	 * @param response
	 * @throws Throwable
	 */
		
	public void doUpload(HttpServletRequest request, HttpServletResponse response) throws Throwable {
		
		try {
			
			// Gets file name for HTTP header
	        String fileName = request.getHeader("fileName");
	        File saveFile = new File(Constants.JMXDIR + fileName);
	         
	        // prints out all header values
	        LogManager.infoLog("===== Begin headers =====");
	        Enumeration<String> names = request.getHeaderNames();
	        while (names.hasMoreElements()) {
	            String headerName = names.nextElement();
	            LogManager.infoLog(headerName + " = " + request.getHeader(headerName));        
	        }
	        LogManager.infoLog("===== End headers =====\n");
	         
	        // opens input stream of the request for reading data
	        InputStream inputStream = request.getInputStream();
	         
	        // opens an output stream for writing file
	        FileOutputStream outputStream = new FileOutputStream(saveFile);
	         
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        LogManager.infoLog("Receiving data...");
	         
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outputStream.write(buffer, 0, bytesRead);
	        }
	         
	        LogManager.infoLog("Data received.");
	        outputStream.close();
	        inputStream.close();
	         
	        LogManager.infoLog("File written to: " + saveFile.getAbsolutePath());
	         
	        // sends response to client
	        response.getWriter().print("UPLOAD DONE");
			
		}catch(Throwable t) {
			LogManager.infoLog("Exception in doUpload()"+t.getMessage());
			throw t;
		}
	}
	
	/**
	 * to execute the jmeter script 
	 * @param request
	 * @param response
	 * @throws Throwable
	 */
	
	public void doRun(HttpServletRequest request, HttpServletResponse response) throws Throwable {
		
		//String strJmxFileName="NewTest.jmx";
		//String strResultFileName="scriptresults.jtl";
		//String argument = "./jmeter -n -t "+strJmxFilePath+" -l "+strJmxResultPath;
		
		/**==========Members===============================*/
		String strJmxFileName="",strResultFileName="";
		Process process;
		ProcessBuilder processBuilder;
		
		int exitStatus=1;//  to capture the result status
		 
		/**==========capture request parameters================*/
		strJmxFileName=request.getHeader("jmx");//JMX FILE
		strResultFileName=request.getHeader("jtl");
		//strResultFileName=strJmxFileName+"_"+String.valueOf(System.currentTimeMillis())+".jtl";
		String strJmxFileDir=Constants.JMXDIR;// phisycal directory path of jmx file 
		String strJmxFilePath=strJmxFileDir+strJmxFileName;
		String strJmxResultPath=Constants.JTLDIR+strResultFileName;
		
		
		//System.out.println("BATCHFILEPATH :"+Constants.BATCHFILEPATH);
		//System.out.println("strJmxFilePath :"+strJmxFilePath);
		//System.out.println("strJmxResultPath :"+strJmxResultPath);
		
		/** =========RUN JMETER SCRIPT===================*/
		processBuilder = new ProcessBuilder(Constants.BATCHFILEPATH, strJmxFilePath, strJmxResultPath);
		// Start the process and wait for it to finish.
		try {
			process = processBuilder.start();
			exitStatus = process.waitFor();
			if(exitStatus==0){
				LogManager.infoLog("Run finished with status: " + exitStatus);
			}else
				LogManager.infoLog("Run not completed :" + exitStatus);
			
			// sends response to client
	        response.getWriter().print("RUN COMPLETED");
		} catch (Throwable  e) {
			// TODO Auto-generated catch block
			//System.out.println("Exception in doRun() :"+e.getMessage());
			//e.printStackTrace();
			LogManager.errorLog(e);
			throw e;
		}
	}
}
