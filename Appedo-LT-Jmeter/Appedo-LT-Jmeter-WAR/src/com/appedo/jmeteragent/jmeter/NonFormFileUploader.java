package com.appedo.jmeteragent.jmeter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
public class NonFormFileUploader {
	//static String UPLOAD_URL = "http://192.168.1.16:8080/jmeteragent/RunJmx";
	static String UPLOAD_URL = "http://54.89.145.85:8080/jmeteragent/RunJmx";
	 static final int BUFFER_SIZE = 4096;
	 StringBuilder sb=new StringBuilder();
	 FileInputStream inputStream = null;
	 OutputStream outputStream = null;
	 BufferedReader reader = null;
	 
	 /** 
	  * this to test application to export files
	  * @param args
	  * @throws IOException
	  */
	 public static void main(String[] args) throws IOException {
		 NonFormFileUploader obj=new NonFormFileUploader();
		 String strNode = "54.89.145.85";
		// String strNode = "192.168.1.16";
		// String strJmxFilePath="E:/VEERU/jmxtest/NewTest.jmx";
		 
		 String strJmxFilePath="C:/Appedo/resource/scenarios/jmeter/NewTest.jmx";
		 String strScenarioName="NewTest.jmx";
		 String strJtlFileName=strScenarioName+"_"+String.valueOf(System.currentTimeMillis())+".csv";
		 
		 try {
			 
			 obj.getParametersFile("E:/jmetertest/with parameter/Thread Group.jmx","E:/jmetertest/with parameter/");
			 /*obj.uploadFile(strJmxFilePath, strNode);
			 obj.runJmeterScript(strNode, strJtlFileName,strScenarioName);
			 obj.getResultFile(strNode, strJtlFileName);
			 
			 */
		 }catch(Throwable e) {
			 System.out.println("Exception in main :"+ e.getMessage());
		 }
		 
		 
	 }
	 /**
	  * To upload a file to agent location 
	  * @param filePath
	  * @throws Throwable
	  */
	 public void uploadFile(String filePath,String strNode) throws Throwable {
		 
		 try {
			 
			// takes file path from input parameter
			File uploadFile = new File(filePath);
			 
			System.out.println("File to upload: " + filePath);
			
			UPLOAD_URL = "http://"+strNode+":8080/jmeter_agent/RunJmx";
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST");
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("fileName", uploadFile.getName());
	        httpConn.setRequestProperty("command", "UPLOAD");
	        
	        // opens output stream of the HTTP connection for writing data
	        outputStream = httpConn.getOutputStream();
	        
	        // Opens input stream of the file for reading data
	        inputStream = new FileInputStream(uploadFile);
	        
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        
	        System.out.println("Start writing data...");
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
		            outputStream.write(buffer, 0, bytesRead);
		        }
	        
	        System.out.println("Data was written.");
	        
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	        	// reads server's response
	        	reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
	        	String response = reader.readLine();
	        	System.out.println("Server's response: " + response);
		            
		    } else {
		            System.out.println("Server returned non-OK code: " + responseCode);
		    }
		 }catch(Throwable t) {
			 System.out.println("Exception in uploadFile() :"+t.getMessage());
			 throw t;
			 
		 }finally {
			 
			 outputStream.close();
			 inputStream.close();
			 reader.close();
		 }
	}
	 /***
	  * 
	  * @param strNode
	  * @param strScenarioName
	  */
	 
	public void runJmeterScript(String strNode,String strJtlFileName,String strScenarioName) throws Throwable {
		
		try {
			UPLOAD_URL = "http://"+strNode+":8080/jmeteragent/RunJmx";
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST");	        
	        
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("command", "RUN");
	        httpConn.setRequestProperty("jmx", strScenarioName);
	        httpConn.setRequestProperty("JTL", strJtlFileName);
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	        	// reads server's response
	        	reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
	        	String response = reader.readLine();
	        	System.out.println("Server's response: " + response);
		            
		     } else {
		            System.out.println("Server returned non-OK code: " + responseCode);
		     }
			
		}catch(Throwable t) {
			System.out.println("Exception in runJmeterScript() :"+t.getMessage());
			throw t;			
		}finally {
			reader.close();
		}
	}
	
	public void  getResultFile(String strNode,String strJtlFileName) throws Throwable  {
		// Members
		String str="";
		try {
			
			UPLOAD_URL = "http://"+strNode+":8080/jmeteragent/RunJmx";
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST"); 
	        
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("command", "RESULTFILE");
	        httpConn.setRequestProperty("JTL", strJtlFileName);
	        
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	            // reads server's response
	           // reader = new BufferedReader(new InputStreamReader(
	                   
	            while( (str = reader.readLine()) != null ){
	            //String response = reader.readLine();
	            	sb.append(str+"\n");
	            }
	           // System.out.println("Server's response: " + response);
	            System.out.println("Server's response: " + sb.toString());
	        } else {
	            System.out.println("Server returned non-OK code: " + responseCode);
	        }
			
			//writeToFile("C:/Appedo/resource/jmetertest/jtl/"+strJtlFileName, sb);
			writeToFile("C:/Appedo/resource/jmetertest/jtl/50601408950266495.csv", sb);
			
		}catch(Throwable t) {
			System.out.println("Exception in getResultFile() :"+t.getMessage());
			throw t;
		}finally {
			reader.close();
		}
	}
	/**
	 * to create the result file (.jtl)
	 * @param strFilePath
	 * @throws Throwable
	 */
	public void writeToFile(String strFilePath, StringBuilder sb) throws Throwable {
		
		try {
			
			File file = new File(strFilePath);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(sb.toString());
			bw.close();
 
			System.out.println("Done");
 
		} catch (Throwable  e) {
			e.printStackTrace();
		}
	}
	
	
	public List<String> getParametersFile(String strJmxPath,String strParamPath) throws Throwable {
		
		// Members
		List<String> list = new ArrayList<String>();
		String strCsvParamFilePath="";
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new FileInputStream(new File(strJmxPath)));
			
			// locate the node(s)
			XPath xPath =  XPathFactory.newInstance().newXPath();			
			//String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='Thread Group']";
			String expression = "/jmeterTestPlan/hashTree/hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
			
			System.out.println(expression);
			
			NodeList paramNode= (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			
			System.out.println("# files :"+paramNode.getLength());
			
			if(paramNode.getLength()>0) {
				for (int i = 0; i < paramNode.getLength(); i++) {
					//Node threadGroupNode = (Node) paramNode.item(i);
					System.out.println("val "+paramNode.item(i).getTextContent());
					
					if(paramNode.item(i).getTextContent().trim().length()>0 && paramNode.item(i).getTextContent().trim()!= null) {
						strCsvParamFilePath = strParamPath+paramNode.item(i).getTextContent();
						File fi = new File(strCsvParamFilePath);
						if(fi.exists()) {
							list.add(paramNode.item(i).getTextContent());
						}
						else {
							break;
						}
					}
				}
			}
		}catch(Throwable t) {
			System.out.println("Exception in getParametersFile()"+t.getMessage());			
			t.printStackTrace();
			throw t;
		}
		
		return list;
	}
}



