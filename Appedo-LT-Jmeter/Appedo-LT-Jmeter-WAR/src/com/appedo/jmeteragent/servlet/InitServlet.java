package com.appedo.jmeteragent.servlet;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.jmeteragent.common.Constants;
import com.appedo.jmeteragent.manager.LogManager;



/**
 * project initiation to set application parameters
 */

public class InitServlet extends HttpServlet {
	// set log access

	private static final long serialVersionUID = 1L;
	public static String realPath = null;
	public static String CONFIGFILEPATH = "";
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		// declare servlet context
		ServletContext context = getServletContext();
		realPath = context.getRealPath("//");		
		System.out.println("CONFIGFILEPATH="+Constants.CONFIGFILEPATH);		
		String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
		// Loads log4j configuration properties
		com.appedo.jmeteragent.manager.LogManager.initializePropertyConfigurator();
		
		try {
			Properties prop = new Properties();
     		InputStream is = new FileInputStream( Constants.CONFIGFILEPATH );
     		prop.load(is);
     		
     		// jmeter resource path 
     		Constants.RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
     		// batch file dir
     		Constants.BATCHFILEPATH=Constants.RESOURCE_PATH+prop.getProperty("BATCHFILE_PATH");
     		// jmx file directory 
     		Constants.JMXDIR = Constants.RESOURCE_PATH+prop.getProperty("JMXFILE_DIR");
     		// jtl file directory
     		Constants.JTLDIR=Constants.RESOURCE_PATH+prop.getProperty("JTLFILE_DIR");
     		
     		System.out.println("RESOURCE_PATH ="+Constants.RESOURCE_PATH);
     		System.out.println("BATCHFILEPATH ="+Constants.BATCHFILEPATH);
     		System.out.println("JMXDIR ="+Constants.JMXDIR);
     		System.out.println("JTLDIR ="+Constants.JTLDIR);
     		
			
		}catch(Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// TODO Auto-generated method stub
	}
}
