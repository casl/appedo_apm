package com.appedo.jmeteragent.common;

import com.appedo.jmeteragent.servlet.*;
/**
 * to keep the constant values which is used through out the project
 * @author veeru
 *
 */
public class Constants {
	
	public final static String CONFIGFILEPATH = InitServlet.realPath+"/WEB-INF/classes/com/appedo/jmeteragent/resource/config.properties";
	public final static String LOG4J_PROPERTIES_FILE = InitServlet.realPath+"/WEB-INF/classes/com/appedo/jmeteragent/resource/log4j.properties";
	public static String RESOURCE_PATH = "";
	public static String JMXDIR = "";
	public static String JTLDIR = "";
	public static String BATCHFILEPATH="";
	public static String CLASSPATH="";
	
	
}
