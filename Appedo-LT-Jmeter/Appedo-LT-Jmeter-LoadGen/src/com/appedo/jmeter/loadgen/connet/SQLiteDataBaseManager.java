package com.appedo.jmeter.loadgen.connet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLiteDataBaseManager {
	/**
	 * Connect to a sample database
	 */
	public static Connection connect(String strDBFilepath) {
		Connection con = null;
		
		try {
			Class.forName("org.sqlite.JDBC");
			
			// create a connection to the database
			con = DriverManager.getConnection("jdbc:sqlite:"+strDBFilepath);
			
		} catch (Throwable th) {
			System.out.println("Exception in SQLite DB connect: "+th.getMessage());
			th.printStackTrace();
		}
		
		return con;
	}
	
	public static void close(ResultSet rst) {
		try {
			if (rst != null) {
				rst.close();
			}
		} catch (Throwable th) {
			System.out.println("Exception in SQLiteDatabaseManager.close: "+th.getMessage());
			th.printStackTrace();
		}
	}
	
	public static void close(Statement stmt) {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (Throwable th) {
			System.out.println("Exception in SQLiteDatabaseManager.close: "+th.getMessage());
			th.printStackTrace();
		}
	}
	
	public static void close(PreparedStatement pstmt) {
		try {
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (Throwable th) {
			System.out.println("Exception in SQLiteDatabaseManager.close: "+th.getMessage());
			th.printStackTrace();
		}
	}
	
	public static void close(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (Throwable th) {
			System.out.println("Exception in SQLiteDatabaseManager.close: "+th.getMessage());
			th.printStackTrace();
		}
	}
}
