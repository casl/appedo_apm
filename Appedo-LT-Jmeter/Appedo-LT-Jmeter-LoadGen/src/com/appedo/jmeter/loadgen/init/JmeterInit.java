package com.appedo.jmeter.loadgen.init;

import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.jmeter.loadgen.common.Constants;
import com.appedo.jmeter.loadgen.utils.UtilsFactory;
import com.appedo.loadgen.manager.ProcessBuilderManager;
import com.appedo.loadgen.threads.JmeterQueueDrainingThread;

import net.sf.json.JSONObject;

public class JmeterInit {

	public static TimerTask timerTaskJmeter = null;
	public static Timer timerJmeter = new Timer();
	
	public static void main(String[] args) {
		ServerSocket servSoc = null;
		Socket socketCon = null;
		ObjectInputStream inputStream = null;
		ObjectOutputStream outputStream = null;
		JSONObject joAckData = null, joLoadGenData = null;
		ProcessBuilderManager processBuilder = null;
		int exitStatus = -1;//  to capture the result status
		
		try {
			joAckData = new JSONObject();
			
			Constants.loadConstantsProperties();
			
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			//1. creating a server socket - 1st parameter is port number and 2nd is the Queuelimit
			System.out.println("opening port: "+Constants.LOADGEN_SERVER_SOCKET_PORT);
			servSoc = new ServerSocket(Integer.parseInt(Constants.LOADGEN_SERVER_SOCKET_PORT) , 0);
			
			//2. Wait for an incoming connection
			System.out.println("LoadGen Server up. Waiting for connection...");
			//LogManager.infoLog("LoadGen Server up. Waiting for connection...");
			
			while(true) {
				try {
					socketCon = servSoc.accept();
					
					inputStream = new ObjectInputStream(socketCon.getInputStream());
					
					String message = (String) inputStream.readObject();
					
					System.out.println("Message Received: " + message);
					
					outputStream = new ObjectOutputStream(socketCon.getOutputStream());
					
					if (message.trim().startsWith("{") && message.trim().endsWith("}")) {
						
						joLoadGenData = JSONObject.fromObject(message);
						
						if (joLoadGenData.containsKey("stopTestRequest")) {
							
							//new JMeterTestStopper();
							try {
								System.out.println("Stop JMeter test request at : " + joLoadGenData.getString("requestTime"));
								processBuilder = new ProcessBuilderManager();
								exitStatus = processBuilder.executeProcess(new File(Constants.JMETER_BIN_PATH), new String[]{Constants.JMETER_STOP_TEST_SCRIPT});
								
								if (exitStatus == 0) {
									outputStream.writeObject(UtilsFactory.getJSONSuccessReturn("Stop Test request received, Test will be stopped in sometime.").toString());
								} else {
									outputStream.writeObject(UtilsFactory.getJSONFailureReturn("Stop Test script execution returned non-zero response : "+exitStatus).toString());
								}
								
							} catch (Exception e) {
								outputStream.writeObject(UtilsFactory.getJSONFailureReturn("Stop Test throws exception: "+e.getMessage()).toString());
							}
						} else {
							//Starting JMeter Test thread
							new JmeterQueueDrainingThread(joLoadGenData);
							/*timerTaskJmeter = new JmeterQueueDrainingThread(joLoadGenData);
							timerJmeter.schedule(timerTaskJmeter, 100, 20*1000);*/
							
							outputStream.writeObject(UtilsFactory.getJSONSuccessReturn("loadGen JSON data received.").toString());
						}
						
					} else {
						outputStream.writeObject(UtilsFactory.getJSONFailureReturn("Invalid loadGen JSON Data received.").toString());
					}
					
					inputStream.close();
					outputStream.close();
					socketCon.close();
					
					Thread.sleep(1000);
					
				} catch(Throwable t) {
					System.out.println("Exception in LoadGenServerQueue.listen: "+t.getMessage());
					t.printStackTrace();
					//LogManager.errorLog(t);
				}
			}
			
		} catch (Exception e) {
			System.out.println("Exception in JmeterInit Main");
			e.printStackTrace();
		}
	}
}
