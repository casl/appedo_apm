package com.appedo.jmeter.loadgen.init;

import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.jmeter.loadgen.common.Constants;
import com.appedo.loadgen.threads.JGSnapshotQueueDrainingThread;

/**
 * Jmeter-Grafana Collector(server) side agent.
 * For each Grafana server, which needs to store SnapShot & extra, this agent should run there.
 * 
 * @author Ramkumar R
 *
 */
public class JGCollectorAgent {
	
	public static TimerTask timerTaskSnapshot = null, timerTaskUserSetup = null;
	public static Timer timerSnapshot = new Timer(), timerUserSetup = new Timer();
	
	public static void main(String[] args) {
		Connection con = null;
		
		try {
			Constants.loadConstantsProperties();
			
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Jmeter-Collector-Agent", Constants.APPEDO_CONFIG_FILE_PATH, false);
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			Constants.loadAppedoConfigProperties(con, Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			timerTaskSnapshot = new JGSnapshotQueueDrainingThread();
			timerSnapshot.schedule(timerTaskSnapshot, 100, 20*1000);
			
			/*
			 * User registration activity is not required now
			 *
			timerTaskUserSetup = new JGUserSetupQueueDrainingThread();
			timerUserSetup.schedule(timerTaskUserSetup, 100, 20*1000);
			*/
		} catch (Throwable th) {
			System.out.println("Exception in JMeterGrafanaSnapshotCreator Main: "+th.getMessage());
			th.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}
