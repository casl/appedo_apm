package com.appedo.jmeter.loadgen.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import com.appedo.jmeter.loadgen.utils.UtilsFactory;
import com.appedo.manager.AppedoConstants;

public class Constants {
	
	public static boolean DEV_ENVIRONMENT = false;

	public static String THIS_JAR_PATH = null;
	public static String RESOURCE_PATH = "";
	public static String APPEDO_CONFIG_FILE_PATH = "";
	
	public static boolean bConfigPropertiesLoaded = false;

	public static String LT_EXECUTION_SERVICES = "";
	
	public static String LT_QUEUE_SERVICES = "";
	
	public static String JMX_CSV_FOLDERPATH = "";
	public static String JMETER_BIN_PATH = "";
	
	public static String JMETER_STOP_TEST_SCRIPT = "";
	
	public static String JMETER_EXECUTION_SCRIPT_FILEPATH = null;
	public static Boolean ON_PREM_ENV = false;
//	public static String JTL_REPORT_GENERATION_SCRIPT_FILEPATH = null;

	public static String INFLUXDB_PROTOCAL = null;
	public static String INFLUXDB_PORT = null;
	public static String INFLUXDB_DATABASE_NAME = null;
	
	public static String GRAPHITE_BACKEND_LISTENER_XML_FILEPATH = null;
	public static String GRAFANA_DATASOURCE_NAME = null;
	public static String GRAFANA_SQLITE_DB_FILEPATH = null;
	public static String GRAFANA_DASHBOARD_DEFAULT_SLUG = null;
		
	public static int GRAFANA_APPEDO_ORG_ID;
	public static int GRAFANA_APPEDO_ORG_USER_ID;
	
	public static String LOADGEN_SERVER_SOCKET_PORT;
	
//	public static String GRAFANA_NEW_USER_PASSWORD = null;
//	public static String GRAFANA_NEW_USER_PASSWORD_SALT = null;
//	public static String GRAFANA_NEW_USER_PASSWORD_RANDS = null;
	
	
	public static void loadConstantsProperties() throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			THIS_JAR_PATH = UtilsFactory.getThisJarPath();
			
			is = new FileInputStream(THIS_JAR_PATH+File.separator+"config.properties");
			prop.load(is);
			
			// Appedo application's resource directory path
			RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
			APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
			JMX_CSV_FOLDERPATH = prop.getProperty("JMX_CSV_FOLDERPATH");
			
			JMETER_BIN_PATH = prop.getProperty("JMETER_BIN_PATH");
			
			JMETER_STOP_TEST_SCRIPT = prop.getProperty("JMETER_STOP_TEST_SCRIPT");
			
			JMETER_EXECUTION_SCRIPT_FILEPATH = THIS_JAR_PATH+File.separator+prop.getProperty("JMETER_EXECUTION_SCRIPT_FILENAME");
//			JTL_REPORT_GENERATION_SCRIPT_FILEPATH = THIS_JAR_PATH+File.separator+prop.getProperty("JTL_REPORT_GENERATION_SCRIPT_FILENAME");
			
			LOADGEN_SERVER_SOCKET_PORT = prop.getProperty("LOADGEN_SERVER_SOCKET_PORT");
			
			INFLUXDB_PROTOCAL = prop.getProperty("INFLUXDB_PROTOCAL");
			INFLUXDB_PORT = prop.getProperty("INFLUXDB_PORT");
			INFLUXDB_DATABASE_NAME = prop.getProperty("INFLUXDB_DATABASE_NAME");
			
			GRAPHITE_BACKEND_LISTENER_XML_FILEPATH = THIS_JAR_PATH+File.separator+prop.getProperty("GRAPHITE_BACKEND_LISTENER_XML_FILENAME");
			GRAFANA_DATASOURCE_NAME = prop.getProperty("GRAFANA_DATASOURCE_NAME");
			GRAFANA_SQLITE_DB_FILEPATH = prop.getProperty("GRAFANA_SQLITE_DB_FILEPATH");
			GRAFANA_DASHBOARD_DEFAULT_SLUG = prop.getProperty("GRAFANA_SQLITE_DB_DEFAULT_SLUG");
			
			bConfigPropertiesLoaded = true;
		} catch(Exception e) {
			System.out.println(e);
			throw e;
		} finally {
			if(is != null){
				is.close();
			}
			is = null;
		}
	}
	
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			if( ! bConfigPropertiesLoaded ) {
				System.out.println("<AGENT_HOME>/config.properties must be read before appedo_config.properties reading.");
				throw new Exception("<AGENT_HOME>/config.properties must be read before appedo_config.properties reading.");
			}
			
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			LT_EXECUTION_SERVICES = prop.getProperty("LT_EXECUTION_SERVICES");
			
			LT_QUEUE_SERVICES = prop.getProperty("LT_QUEUE_SERVICES");
			
			GRAFANA_APPEDO_ORG_ID = Integer.parseInt( prop.getProperty("GRAFANA_APPEDO_ORG_ID") );
			GRAFANA_APPEDO_ORG_USER_ID = Integer.parseInt( prop.getProperty("GRAFANA_APPEDO_ORG_USER_ID") );
			
			// Read the property if not mentioned in config.properties
			if( JMX_CSV_FOLDERPATH == null ) {
				JMX_CSV_FOLDERPATH = prop.getProperty("JMX_CSV_FOLDERPATH");
			}
			
			if( INFLUXDB_PROTOCAL == null ) {
				INFLUXDB_PROTOCAL = prop.getProperty("INFLUXDB_PROTOCAL");
			}
			if( INFLUXDB_PORT == null ) {
				INFLUXDB_PORT = prop.getProperty("INFLUXDB_PORT");
			}
			if( INFLUXDB_DATABASE_NAME == null ) {
				INFLUXDB_DATABASE_NAME = prop.getProperty("INFLUXDB_DATABASE_NAME");
			}
			
			if( GRAPHITE_BACKEND_LISTENER_XML_FILEPATH == null ) {
				GRAPHITE_BACKEND_LISTENER_XML_FILEPATH = RESOURCE_PATH+File.separator+prop.getProperty("GRAPHITE_BACKEND_LISTENER_XML_FILENAME");
			}
			
			if( GRAFANA_SQLITE_DB_FILEPATH == null ) {
				GRAFANA_SQLITE_DB_FILEPATH = prop.getProperty("GRAFANA_SQLITE_DB_FILEPATH");
			}
			
			if( GRAFANA_DATASOURCE_NAME == null ) {
				GRAFANA_DATASOURCE_NAME = prop.getProperty("GRAFANA_DATASOURCE_NAME");
			}
			
			if( GRAFANA_DASHBOARD_DEFAULT_SLUG == null ) {
				GRAFANA_DASHBOARD_DEFAULT_SLUG = prop.getProperty("GRAFANA_SQLITE_DB_DEFAULT_SLUG");
			}
						
		} catch(Exception e) {
			System.out.println(e);
			throw e;
		} finally {
			if(is != null){
				is.close();
			}
			is = null;
		}
	}
	
	public static void loadAppedoConfigProperties(Connection con, String strAppedoConfigPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			if( ! bConfigPropertiesLoaded ) {
				System.out.println("<AGENT_HOME>/config.properties must be read before appedo_config.properties reading.");
				throw new Exception("<AGENT_HOME>/config.properties must be read before appedo_config.properties reading.");
			}
			
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			if( prop.getProperty("ENVIRONMENT") != null && prop.getProperty("ENVIRONMENT").equals("DEVELOPMENT") 
					&& AppedoConstants.getAppedoConfigProperty("ENVIRONMENT") != null && AppedoConstants.getAppedoConfigProperty("ENVIRONMENT").equals("DEVELOPMENT") ) 
			{
				DEV_ENVIRONMENT = true;
			}
			
			LT_EXECUTION_SERVICES = getProperty("LT_EXECUTION_SERVICES", prop);
			
			GRAFANA_APPEDO_ORG_ID = Integer.parseInt( getProperty("GRAFANA_APPEDO_ORG_ID", prop) );
			GRAFANA_APPEDO_ORG_USER_ID = Integer.parseInt( getProperty("GRAFANA_APPEDO_ORG_USER_ID", prop) );
			
			// Read the property if not mentioned in config.properties
			if( JMX_CSV_FOLDERPATH == null ) {
				JMX_CSV_FOLDERPATH = getProperty("JMX_CSV_FOLDERPATH", prop);
			}
			
			if( INFLUXDB_PROTOCAL == null ) {
				INFLUXDB_PROTOCAL = getProperty("INFLUXDB_PROTOCAL", prop);
			}
			if( INFLUXDB_PORT == null ) {
				INFLUXDB_PORT = getProperty("INFLUXDB_PORT", prop);
			}
			if( INFLUXDB_DATABASE_NAME == null ) {
				INFLUXDB_DATABASE_NAME = getProperty("INFLUXDB_DATABASE_NAME", prop);
			}
			
			if( GRAPHITE_BACKEND_LISTENER_XML_FILEPATH == null ) {
				GRAPHITE_BACKEND_LISTENER_XML_FILEPATH = RESOURCE_PATH+File.separator+getProperty("GRAPHITE_BACKEND_LISTENER_XML_FILENAME", prop);
			}
			if( GRAFANA_SQLITE_DB_FILEPATH == null ) {
				GRAFANA_SQLITE_DB_FILEPATH = getProperty("GRAFANA_SQLITE_DB_FILEPATH", prop);
			}
			if( GRAFANA_DATASOURCE_NAME == null ) {
				GRAFANA_DATASOURCE_NAME = getProperty("GRAFANA_DATASOURCE_NAME", prop);
			}
			if( GRAFANA_DASHBOARD_DEFAULT_SLUG == null ) {
				GRAFANA_DASHBOARD_DEFAULT_SLUG = getProperty("GRAFANA_SQLITE_DB_DEFAULT_SLUG", prop);
			}
						
		} catch(Throwable th) {
			System.out.println("Exception in loadAppedoConfigProperties: "+th.getMessage());
			throw th;
		} finally {
			if(is != null){
				is.close();
			}
			is = null;
		}
	}
	
	/**
	 * loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoConstants(Connection con) throws Throwable {
		
		try {
			AppedoConstants.getAppedoConstants().loadAppedoConstants(con);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Get the property's value from appedo_config.properties, if it is DEV environment;
	 * Otherwise get the property's value from DB.
	 * 
	 * @param strPropertyName
	 * @param prop
	 * @return
	 */
	private static String getProperty(String strPropertyName, Properties prop) throws Throwable {
		if( DEV_ENVIRONMENT && prop.getProperty(strPropertyName) != null )
			return prop.getProperty(strPropertyName);
		else
			return AppedoConstants.getAppedoConfigProperty(strPropertyName);

	}
}
