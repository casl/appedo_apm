package com.appedo.loadgen.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ProcessBuilderManager {

	private int exitStatus = 1;
	private StringBuilder sbOutput = null, sbError = null;
	
	/**
	 * 
	 * 
	 * @param filePWD
	 * @param strBatFileArgs
	 * @return
	 * @throws Throwable
	 */
	public int executeProcess(File filePWD, String... strBatFileArgs) throws Throwable {
		
		ProcessBuilder processBuilder;
		Process process;
		
		try{
			processBuilder = new ProcessBuilder(strBatFileArgs);
			processBuilder.directory( filePWD );
			
			// Start the process and wait for it to finish.
			process = processBuilder.start();
			exitStatus = process.waitFor();
			
			sbOutput = getStreamContent( process.getInputStream() );
			if( sbOutput.length() > 0 ) {
				System.out.println("Command execution Output for "+strBatFileArgs);
				System.out.println(sbOutput);
				System.out.println();
			}
			
			sbError = getStreamContent( process.getErrorStream() );
			if( sbError.length() > 0 ) {
				System.out.println("Command execution failed for "+strBatFileArgs);
				System.out.println(sbError);
				System.out.println();
			}
			
		} catch ( Throwable th ) {
			throw th;
		}
		
		return exitStatus;
	}
	
	
	private StringBuilder getStreamContent(InputStream inputStream) {
		InputStreamReader isr = null;
		BufferedReader br = null;
		String line = null;
		StringBuilder sbOutput = new StringBuilder();
		
		try{
			isr = new InputStreamReader(inputStream);
			br = new BufferedReader(isr);
			sbOutput.setLength(0);
			while ((line = br.readLine()) != null) {
				sbOutput.append(line).append("\n");
			}
			if( sbOutput.length() > 0 ){
				sbOutput.deleteCharAt(sbOutput.length()-1);
			}
		} catch ( Exception e ) {
			System.out.println("Exception in getStreamContent: "+e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				isr.close();
			} catch(Exception e) {
				System.out.println("Exception in isrError.close(): "+e.getMessage());
				e.printStackTrace();
			}
			isr = null;
			try{
				br.close();
			} catch(Exception e) {
				System.out.println("Exception in rError.destroy(): "+e.getMessage());
				e.printStackTrace();
			}
			br = null;
		}
		
		return sbOutput;
	}
	
	public StringBuilder getSbOutput() {
		return sbOutput;
	}
	public StringBuilder getSbError() {
		return sbError;
	}
}
