package com.appedo.loadgen.threads;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.appedo.jmeter.loadgen.common.Constants;
import com.appedo.jmeter.loadgen.connet.SQLiteDataBaseManager;
import com.appedo.jmeter.loadgen.utils.UtilsFactory;
import com.appedo.loadgen.manager.ProcessBuilderManager;
import com.appedo.manager.WebServiceManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JmeterQueueDrainingThread  extends Thread {
	
	JSONObject joLoadTestSchedulerBean = null;
	
	public JmeterQueueDrainingThread(JSONObject joLoadTestSchedulerBean) {
		this.joLoadTestSchedulerBean = joLoadTestSchedulerBean;
		start();
	}
	
	
	public void run() {
		URLConnection urlConn = null;
		WebServiceManager wsm = null;
		ProcessBuilderManager processBuilder = null;
		
		int exitStatus = -1;//  to capture the result status
		String strReportName = null, strLoadgenIpAddress = null, strJMXFilePath = null, strTestStartTime, strTestEndTime;
		String[] listParam;
		String loadGenIp = null;
		JSONObject joDashBoardDetails = null, joSnapShotPostData = null;
		Calendar calCurrentTime;
		long lTestStartTimeHour, lTestEndTimeHour;
		HashSet<String> hsTransactionController = null, hsHTTPSamplerProxy = null;
		StringBuilder sbRequest = new StringBuilder();
		try {
			wsm = new WebServiceManager();
			
			System.out.println("Checking for JMeter test: "+UtilsFactory.formatDateToTimestamp(new Date().getTime(), false));
			
			//joLoadTestSchedulerBean = getJMeterTestData();
			
			if (joLoadTestSchedulerBean == null ) {
				throw new Exception("getJMeterTestData() returns null.");
			}
			
			//String loadGenIps = joLoadTestSchedulerBean.getString("LoadgenIpAddress");
			
			sbRequest.append("userId=").append(joLoadTestSchedulerBean.getString("userId")).append("&scenarioName=").append(joLoadTestSchedulerBean.getString("ScenarioName"))
					.append("&runId=").append(joLoadTestSchedulerBean.getString("lRunId")).append("&neededInstance=").append(joLoadTestSchedulerBean.getString("LoadgenIpAddress").split(",").length);
			
			wsm.addParameter("runId", joLoadTestSchedulerBean.getString("lRunId"));
			wsm.addParameter("userId", joLoadTestSchedulerBean.getString("userId"));
			wsm.addParameter("scenarioName", joLoadTestSchedulerBean.getString("ScenarioName"));
			
			wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/containsCSVFiles");
			
			JSONObject joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse != null && joResponse.getBoolean("success") ) {
				if( joResponse.containsKey("message") ) {
					if(joResponse.get("message")!=null){
						
						listParam = joResponse.get("message").toString().split(",");
						for(String str : listParam){
							
							URL url = new URL(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/downloadCSVFile");
							
							urlConn = url.openConnection();			
							urlConn.setUseCaches(false);
							urlConn.setDoOutput(true);
							
							String data = "userId="+joLoadTestSchedulerBean.getString("userId")+"&fileName="+str;
							OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
							wr.write(data);
							wr.flush();
							
							if( urlConn != null ) {
								if( ! urlConn.getHeaderField("file_exist").equals("-1") ) {
									Path classPath = Paths.get(Constants.JMX_CSV_FOLDERPATH, str);
									Files.copy(urlConn.getInputStream(), classPath, StandardCopyOption.REPLACE_EXISTING);
									
								} else {
									System.out.println("NO PENDING TEST");
								}
							}
						}
					}
				} else {
					throw new Exception("Exception while receiveing CSV files: "+joResponse.toString());
				}
			} else if ( wsm.getJSONObjectResponse() == null ) {
				throw new Exception(wsm.getResponse());
			}

			URL url = new URL(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/getScenarioFile");
			
			urlConn = url.openConnection();			
			urlConn.setUseCaches(false);
			urlConn.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
			wr.write(sbRequest.toString());
			wr.flush();
			
			if( urlConn != null && ! urlConn.getHeaderField("file_exist").equals("-1") ) {
				Path classPath = Paths.get(Constants.JMX_CSV_FOLDERPATH, urlConn.getHeaderField("fileName") + ".jmx");
				strJMXFilePath = classPath.toString();
				
				Files.copy(urlConn.getInputStream(), classPath, StandardCopyOption.REPLACE_EXISTING);
				// mmdisplay.jmx <> /mnt/appedo/appedo_lt_jmeter_loadgen_1.0.002/mmdisplay.jmx <> /mnt/appedo/jmeter/extras , mmdisplay.jmx
				
			} else {
				System.out.println("NO PENDING TEST");
				throw new Exception("NO PENDING TEST");
			}
			
			
			/** =================== PreRequesties for Grafana =================== */
			hsTransactionController = new HashSet<String>();
			hsHTTPSamplerProxy = new HashSet<String>();
			
			// Add InfluxDB integration XML into JMX
			appendGraphiteBackendListenerTag(strJMXFilePath, Constants.GRAPHITE_BACKEND_LISTENER_XML_FILEPATH, hsTransactionController, hsHTTPSamplerProxy );
			
			// Clear the InfluxDB database.
			recreateInfluxDBDatabase();
			
			//joLoadTestSchedulerBean = JSONObject.fromObject( urlConn.getHeaderField("loadTestSchedulerBean") );
			
			strReportName = joLoadTestSchedulerBean.getString("ReportName");
			strLoadgenIpAddress = joLoadTestSchedulerBean.getString("LoadgenIpAddress").split(",")[0];
			
			System.out.println("Report in-progress: "+strReportName);
			
			
			// Update ReportName & Time-filter in SQLite
			// 60 is hardcoded test duration. TODO parse all <ThreadGroup>'s <stringProp name="ThreadGroup.duration"/>; and find max
			joDashBoardDetails = updatePrerequestiesInSQLite(strReportName, 60, strLoadgenIpAddress, hsTransactionController, hsHTTPSamplerProxy);
			
			System.out.println("Prerequesties in SQLite is updated.");
			
			// Calculate Time-Interval to show for future
			calCurrentTime = Calendar.getInstance();
			// Get upto Hours
			calCurrentTime.set(Calendar.MILLISECOND, 0);
			calCurrentTime.set(Calendar.SECOND, 0);
			calCurrentTime.set(Calendar.MINUTE, 0);
			lTestStartTimeHour = calCurrentTime.getTimeInMillis();
			strTestStartTime = UtilsFactory.formatDateToTimestamp(lTestStartTimeHour, true);
			
			
			/** =================== RUN JMETER SCRIPT =================== */
			// Start the process and wait for it to finish.
			System.out.println("JMX test started: " + UtilsFactory.formatDateToTimestamp(new Date().getTime(), false));
			processBuilder = new ProcessBuilderManager();
			exitStatus = processBuilder.executeProcess(new File(Constants.JMX_CSV_FOLDERPATH), new String[]{Constants.JMETER_EXECUTION_SCRIPT_FILEPATH, urlConn.getHeaderField("fileName"), joLoadTestSchedulerBean.getString("LoadgenIpAddress")});
			
			// Get test end time
			calCurrentTime = Calendar.getInstance();
			// Get upto Hours. Add one hour to complete the filter window
			calCurrentTime.set(Calendar.MILLISECOND, 0);
			calCurrentTime.set(Calendar.SECOND, 0);
			calCurrentTime.set(Calendar.MINUTE, 0);
			calCurrentTime.add(Calendar.HOUR, 1);
			lTestEndTimeHour = calCurrentTime.getTimeInMillis();
			strTestEndTime = UtilsFactory.formatDateToTimestamp(lTestEndTimeHour, true);
			System.out.println("JMX test finished with status: " + exitStatus+" <> at "+UtilsFactory.formatDateToTimestamp(new Date().getTime(), false));
			
			
			// Update ReportName & Time-filter in SQLite
			updatePostDetailsInSQLite(joDashBoardDetails, strTestStartTime, strTestEndTime);
			
			System.out.println("PostDetails in SQLite is updated.");
			
			if (exitStatus == 0) {
				
				/*
				prepareSendJTLReport(urlConn);
				*/
				
				// prepare Snapshot's Post HTTP method Data
				System.out.println("Waiting for InfluxDB to complete aggregation...");
				Thread.sleep( 20*1000 );
				
				System.out.println("Preparing Snapshot data...");
				
				joSnapShotPostData = prepareSnapshotPostData(Constants.INFLUXDB_DATABASE_NAME, strReportName, strLoadgenIpAddress, lTestStartTimeHour, lTestEndTimeHour, strTestStartTime, strTestEndTime, hsTransactionController, hsHTTPSamplerProxy);
				System.out.println("Snapshot data is prepared.");
				
				// Create Snapshot in common Grafana instance
				// This is not working
				//joSnapShotDetails = createSnapShotTemplate(strReportName, joSnapShotPostData);
				//System.out.println("created SnapShotTemplate: joSnapShotDetails: "+joSnapShotDetails);
				
				queueGrafanaSnapShotData(joLoadTestSchedulerBean.toString(), joSnapShotPostData);
				System.out.println("Snapshot is queued.");
				
				// Inform Execution completion
				wsm = new WebServiceManager();
				wsm.addParameter("loadTestSchedulerBean", joLoadTestSchedulerBean.toString());
				wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/runCompleted");
				System.out.println("Run is completed.");
			} else {
				System.out.println("JMeter test is failed : " + exitStatus+" <> "+processBuilder.getSbError());
				failJMeterRun( joLoadTestSchedulerBean.toString(), "JMeter test is failed <> statusCode: "+exitStatus+" <> Exception: "+processBuilder.getSbError());
				System.out.println("Run is not completed");
			}
			
			// Clear the InfluxDB database.
			recreateInfluxDBDatabase();
			
		} catch (Throwable th) {
			if( ! th.getMessage().equals("NO PENDING TEST") ) {
				System.out.println("Exception in : "+th.getMessage());
				th.printStackTrace();
			}
			
			if( joLoadTestSchedulerBean != null ) {
				try{
					failJMeterRun( joLoadTestSchedulerBean.toString(), "JMeter test is failed <> Exception: "+th.getMessage());
				} catch (Throwable th1) {
					System.out.println("Exception while sending failJMeterRun : "+th1.getMessage());
					th1.printStackTrace();
				}
			}
		}
	}
	
	public HashSet<String> getIpAddress() {
		HashSet<String> hsIpAddress = null;
		//getting public IP thru amazon url
		String systemipaddress = "";
        
		try
        {
        	hsIpAddress = new HashSet<String>();
            if (Constants.ON_PREM_ENV) {
            	Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
    			while (interfaces.hasMoreElements()) 
    			{
    			    NetworkInterface networkInterface = interfaces.nextElement();
    			    if (networkInterface.isLoopback())
    			        continue;    // Do not want to use the loopback interface.
    			    for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) 
    			    {
    			        //InetAddress broadcast = interfaceAddress.getBroadcast();
    			    	/*if (interfaceAddress.getBroadcast() != null)
    			    		hsIpAddress.add(interfaceAddress.getBroadcast().toString());*/
    			    	if (interfaceAddress.getAddress() != null)
    			    		hsIpAddress.add(interfaceAddress.getAddress().toString());
    			    }
    			}
            } else {
            	//URL url_name = new URL("http://bot.whatismyipaddress.com");
            	URL url_name = new URL("http://169.254.169.254/latest/meta-data/public-ipv4");
                BufferedReader sc =
                new BufferedReader(new InputStreamReader(url_name.openStream()));
     
                // reads system IPAddress
                systemipaddress = sc.readLine().trim();
                hsIpAddress.add(systemipaddress);
            }
            
        } catch (Exception e) {
        	System.out.println("Getting Ip address fails : "+e.getMessage());
			e.printStackTrace();
        }
        return hsIpAddress;
	}
	
	public JSONObject getJMeterTestData() {
		WebServiceManager wsm = null;
		JSONObject joWebResponse = null, joResp = null;
		HashSet<String> ipAddress = null;
		try {
			
			ipAddress = getIpAddress();
			
			wsm = new WebServiceManager();
			
			wsm.addParameter("ipAddress", ipAddress.toString());
			wsm.sendRequest(Constants.LT_QUEUE_SERVICES+"/ltQueue/getJMeterProcessQueue");
			
			joWebResponse = wsm.getJSONObjectResponse();
			
			if( joWebResponse != null && joWebResponse.getBoolean("success") ) {
				if( joWebResponse.containsKey("message") && joWebResponse.get("message") != null ) {
					joResp = joWebResponse.getJSONObject("message");
				} else {
					System.out.println("JMeterProcessQueue returns empty data.");
				}
			} else {
				System.out.println("JMeterProcessQueue is empty or returns null.");
			}
			
		} catch (Exception e) {
			System.out.println("Exception while collecting JMeter Data from Queue : "+e.getMessage());
			e.printStackTrace();
		} finally {
			wsm = null;
		}
		return joResp;
	}
	
	/**
	 * Not-in-use
	 * 
	 * @param urlConn
	 * @throws Throwable
	 *
	private void prepareSendJTLReport(URLConnection urlConn) throws Throwable {
		int exitStatus;
		
		try{
			// To create html using jmx and jtl
			System.out.println("JTL reporting is started.");
			
			// Start the process and wait for it to finish.
			exitStatus = ProcessBuilderManager.executeProcess(new File(Constants.THIS_JAR_PATH), new String[]{Constants.JTL_REPORT_GENERATION_SCRIPT_FILEPATH, Constants.JMX_CSV_FOLDERPATH+File.separator+urlConn.getHeaderField("fileName")+".html", Constants.JMX_CSV_FOLDERPATH+File.separator+urlConn.getHeaderField("fileName")+".jtl"});
			System.out.println("JTL reporting is finished with status: " + exitStatus);
			
			if (exitStatus == 0) {
				int BUFFER_SIZE = 4096;
				String str = Constants.JMX_CSV_FOLDERPATH + File.separator + urlConn.getHeaderField("fileName") + ".html";
				File uploadFile = new File(str);
				String UPLOAD_URL = Constants.LT_EXECUTION_SERVICES+"/ltScheduler/getResultHTMLFile";
				// creates a HTTP connection
				URL url1 = new URL(UPLOAD_URL);
				HttpURLConnection httpConn = (HttpURLConnection) url1.openConnection();
				httpConn.setUseCaches(false);
				httpConn.setDoOutput(true);
				httpConn.setRequestMethod("POST");
				httpConn.setRequestProperty("loadTestSchedulerBean", urlConn.getHeaderField("loadTestSchedulerBean"));
				httpConn.setRequestProperty("fileName", urlConn.getHeaderField("fileName") + ".html");
				// sets file name as a HTTP header
				
				// opens output stream of the HTTP connection for writing data
				OutputStream outputStream = httpConn.getOutputStream();
				
				// Opens input stream of the file for reading data
				FileInputStream inputStream = new FileInputStream(uploadFile);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				
				System.out.println("Upload started...");
				
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				System.out.println("Upload succeded.");
				
				// always check HTTP response code from server
				int responseCode = httpConn.getResponseCode();
				
				inputStream.close();
				outputStream.close();
				
				//Delete html and jtl files from csv folder path
				File f = new File(Constants.JMX_CSV_FOLDERPATH + File.separator + urlConn.getHeaderField("fileName")+".html");
				boolean fileDel = f.delete();
				System.out.println("HTML File Deleted :"+fileDel);
				f = new File(Constants.JMX_CSV_FOLDERPATH + File.separator + urlConn.getHeaderField("fileName")+".jtl");
				fileDel = f.delete();
				System.out.println("JTL File Deleted :"+fileDel);
				System.out.println("JMeter run is completed. Going to wait for next test...");
			}
		} catch (Throwable th) {
			System.out.println("Exception in prepareSendJTLReport: "+th.getMessage());
			throw th;
		}
	}
	*/
	
	/**
	 * Append Graphite BackEnd Listener tag, as the last child of the HashTree tag (Next sibling tag to ThreadGroup).
	 * 
	 * @param strJMXFileFullPath
	 * @param strGraphiteBackendListenerTagFileFullPath
	 * @throws Throwable
	 */
	private void appendGraphiteBackendListenerTag(String strJMXFileFullPath, String strGraphiteBackendListenerTagFileFullPath, HashSet<String> hsTransactionController, HashSet<String> hsHTTPSamplerProxy) throws Throwable {
		// Variables to read GraphiteBackendListenerTag from template file.
		DocumentBuilderFactory documentBuilderFactoryGraphiteBL = null;
		DocumentBuilder documentBuilderGraphiteBL = null;
		Document documentGraphiteBL = null;
		Element rootGraphiteBL = null;
		
		// variables to read the JMX script file and replace all "&" as "&amp;"
		BufferedReader brJMXScript = null;
		String strLine = null;
		StringBuilder sbJMXScript = new StringBuilder();
		String strJMXScript = null;
		byte[] byteJMXScript = null;
		InputStream isJMXScript = null;
		
		// Variables to read JMX script file as XML
		DocumentBuilderFactory documentBuilderFactory = null;
		DocumentBuilder documentBuilder = null;
		Document documentJMXFile = null;
		Element root = null, eleThreadGroupHashTree = null;
		NodeList nlThreadGroups = null;
		Node nThreadGroup = null, nTGHashtree = null;
		
		String strTestName = null;
		boolean bSetAdded = false;
		
		// variable to confirm the GraphiteBackendListenerTag's presence under the Hashtree tag
		NodeList nlHashTreeChildren = null, nlHTHTTPSamplerProxy = null;
		Node nHashTreeChildren = null, nHTHTTPSamplerProxy;
		boolean bTagAppended = false;
		
		// variable to keep the imported GraphiteBackendListenerTag tag, under JXM script's DOM
		Node nImportedGraphiteBL = null;
		boolean bThreadGroupEnabled = false;
		
		// Write the GraphiteBackendListenerTag appended JMX script file
		DOMSource source = null;
		TransformerFactory transformerFactory = null;
		Transformer transformer = null;
		StreamResult result = null;
		
		// Write the JMX which is replaced-back with "&amp;" as "&"
		BufferedWriter bwJMXScript = null;
		
		try{
			// Graphite-Backend-Listener tag which has to be appended into all <ThreadGroup/>'s sibling <hashTree/>
			documentBuilderFactoryGraphiteBL = DocumentBuilderFactory.newInstance();
			documentBuilderGraphiteBL = documentBuilderFactoryGraphiteBL.newDocumentBuilder();
			documentGraphiteBL = documentBuilderGraphiteBL.parse( strGraphiteBackendListenerTagFileFullPath );
			rootGraphiteBL = documentGraphiteBL.getDocumentElement();
			
			/*
			 * Change for following exception in possible while parsing the XML
			 * [Fatal Error] adactin_without_gbl.jmx:108:65: Character reference "&#
			 * 
			 * So read the JMX script file and replace all "&" as "&amp;"
			 * Once the "GraphiteBackendListenerTag" is added, need to replace-back "&amp;" as "&"
			 */
			try{
				brJMXScript = new BufferedReader( new FileReader( strJMXFileFullPath ) );
				while ( (strLine = brJMXScript.readLine()) != null ) {
					sbJMXScript.append(strLine).append("\n");
				}
				sbJMXScript.deleteCharAt( sbJMXScript.length()-1 );
				strJMXScript = sbJMXScript.toString();
				strJMXScript = strJMXScript.replaceAll("&", "&amp;");
				byteJMXScript = strJMXScript.getBytes("UTF-8"); 
				
				// convert String into InputStream
				isJMXScript = new ByteArrayInputStream(byteJMXScript);
			} catch ( Throwable th ) {
				System.out.println("Exception while replacing & in JMXScript: "+th.getMessage());
				throw th;
			} finally {
				if( brJMXScript != null ) {
					brJMXScript.close();
				}
			}
			
			
			// Open original JMX file
			documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			documentJMXFile = documentBuilder.parse( isJMXScript );
			root = documentJMXFile.getDocumentElement();
			
			nImportedGraphiteBL = documentJMXFile.importNode(rootGraphiteBL, true);
			
			// Read all <Threadgroup/> irrespective of <TestPlan/>
			nlThreadGroups = root.getElementsByTagName("ThreadGroup");
			
			// Loop through all <Threadgroup/>
			for(int idx = 0; idx < nlThreadGroups.getLength(); idx++ ) {
				nThreadGroup = nlThreadGroups.item(idx);
				
				// Get the sibling <hashTree/> of <Threadgroup/>
				bThreadGroupEnabled = ((Element)nThreadGroup).getAttribute("enabled").toString().equals("true");
				
				if( bThreadGroupEnabled ) {
					nTGHashtree = nThreadGroup;
					
//					// Loop till <hashTree>
//					do{
					do{
						nTGHashtree = nTGHashtree.getNextSibling();
					}while( nTGHashtree.getNodeType() != Node.ELEMENT_NODE);
//					}while( nTGHashtree.getNodeName().toLowerCase().equals("hashtree") );
					
					// Confirm whether sibling tag is <hashTree>
					if( nTGHashtree.getNodeName().toLowerCase().equals("hashtree") ) {
						
						// verify whether GraphiteBackendListenerTag is already appended
						nlHashTreeChildren = ((Element)nTGHashtree).getElementsByTagName("BackendListener");
						for(int idxHashtree = 0; idxHashtree < nlHashTreeChildren.getLength(); idxHashtree++ ) {
							nHashTreeChildren = nlHashTreeChildren.item(idxHashtree);
							
							bTagAppended = ((Element)nHashTreeChildren).getAttribute("testname").toString().equals("Appedo InfluxDB Backend Listener");
						}
						
						if( ! bTagAppended ) {
							eleThreadGroupHashTree = (Element)nTGHashtree;
							eleThreadGroupHashTree.appendChild( nImportedGraphiteBL );
							
							System.out.println("GraphiteBackendListenerTag is added.");
						} else {
							System.out.println("GraphiteBackendListenerTag (testname=\"Appedo InfluxDB Backend Listener\") is already available.");
						}
						
						System.out.println("Finding unique TransactionController...");
						// Get <TransactionController>
						nlHTHTTPSamplerProxy = ((Element)nTGHashtree).getElementsByTagName("TransactionController");
						for(int idxHashtree = 0; idxHashtree < nlHTHTTPSamplerProxy.getLength(); idxHashtree++ ) {
							nHTHTTPSamplerProxy = nlHTHTTPSamplerProxy.item(idxHashtree);
							
							strTestName = ((Element)nHTHTTPSamplerProxy).getAttribute("testname").toString();
							
							bSetAdded = hsTransactionController.add(strTestName);
							if( ! bSetAdded || hsHTTPSamplerProxy.contains( strTestName )) {
								System.out.println("Found duplicate TestName: "+strTestName);
							}
						}
						
						// Get <HTTPSamplerProxy>
						System.out.println("Finding unique HTTPSamplerProxy...");
						nlHTHTTPSamplerProxy = ((Element)nTGHashtree).getElementsByTagName("HTTPSamplerProxy");
						for(int idxHashtree = 0; idxHashtree < nlHTHTTPSamplerProxy.getLength(); idxHashtree++ ) {
							nHTHTTPSamplerProxy = nlHTHTTPSamplerProxy.item(idxHashtree);
							
							strTestName = ((Element)nHTHTTPSamplerProxy).getAttribute("testname").toString();
							
							bSetAdded = hsHTTPSamplerProxy.add(strTestName);
							if( ! bSetAdded ) {
								System.out.println("Found duplicate TestName: "+strTestName);
							}
						}
					} else {
						System.out.println("GraphiteBackendListenerTag is not added as the sibling of <ThreadGroup> is not <hashTree>.");
					}
				}
			}
		} catch ( Throwable th ) {
			System.out.println("Exception in appendGraphiteBackendListenerTag.append GraphiteBackendListenerTag: "+th.getMessage());
			throw th;
		} finally {
			if( isJMXScript != null ) {
				isJMXScript.close();
			}
		}
		
		// Rewrite the JMX
		try{
			source = new DOMSource(documentJMXFile);
			
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			result = new StreamResult( strJMXFileFullPath );
			transformer.transform(source, result);
			
		} catch ( Throwable th ) {
			System.out.println("Exception in appendGraphiteBackendListenerTag.rewrite JMX: "+th.getMessage());
			throw th;
		}
		
		/*
		 * Change for following exception in possible while parsing the XML
		 * [Fatal Error] adactin_without_gbl.jmx:108:65: Character reference "&#
		 * 
		 * So read the JMX script file and replace all "&" as "&amp;"
		 * Once the "GraphiteBackendListenerTag" is added, need to replace-back "&amp;" as "&"
		 */
		try{
			sbJMXScript.setLength(0);
			brJMXScript = new BufferedReader( new FileReader( strJMXFileFullPath ) );
			while ( (strLine = brJMXScript.readLine()) != null ) {
				sbJMXScript.append(strLine).append("\n");
			}
			sbJMXScript.deleteCharAt( sbJMXScript.length()-1 );
			strJMXScript = sbJMXScript.toString();
			strJMXScript = strJMXScript.replaceAll("&amp;", "&");
			
		} catch ( Throwable th ) {
			System.out.println("Exception while replacing back & in JMXScript: "+th.getMessage());
			throw th;
		} finally {
			if( brJMXScript != null ) {
				brJMXScript.close();
			}
		}
		
		/*
		 * Once the "GraphiteBackendListenerTag" is added and replaced "&amp;" as "&",
		 * write them back into the file
		 */
		try{
			bwJMXScript = new BufferedWriter( new FileWriter( strJMXFileFullPath ) );
			bwJMXScript.write( strJMXScript );
		} catch ( Throwable th ) {
			System.out.println("Exception while reading JMXScript file: "+th.getMessage());
			throw th;
		} finally {
			if( bwJMXScript != null ) {
				bwJMXScript.close();
			}
		}
	}
	
	private void recreateInfluxDBDatabase() {
		WebServiceManager wsm = new WebServiceManager();
		
		// Clear the InfluxDB database
		wsm.sendRequest(Constants.INFLUXDB_PROTOCAL+"://localhost:"+Constants.INFLUXDB_PORT+"/query?q=DROP+DATABASE+jmeter%3B&db=_internal");
		System.out.println("InfluxDB Database cleared: "+Constants.INFLUXDB_PROTOCAL+"://localhost:"+Constants.INFLUXDB_PORT+"/query?q=DROP+DATABASE+"+Constants.GRAFANA_DATASOURCE_NAME+"%3B&db=_internal");
		
		// Create new InfluxDB database
		wsm.sendRequest(Constants.INFLUXDB_PROTOCAL+"://localhost:"+Constants.INFLUXDB_PORT+"/query?q=CREATE+DATABASE+jmeter%3B&db=_internal");
		System.out.println("InfluxDB Database created: "+Constants.INFLUXDB_PROTOCAL+"://localhost:"+Constants.INFLUXDB_PORT+"/query?q=CREATE+DATABASE+"+Constants.GRAFANA_DATASOURCE_NAME+"%3B&db=_internal");
		
	}
	
	private void updateAverageTransactionResponseTimeAnalysis(JSONObject joDashBoardDetails, String strDataSourceName, String strDatabaseName, HashSet<String> hsTransactionController) {
		JSONArray jaRows = null, jaPanels = null, jaTargets = null;
		JSONObject joRow = null, joPanel = null, joTargetTemplate = null, joTarget = null;
		Iterator<String> iterTestName = null;
		String strTestName = null;
		
		try{
			joTargetTemplate = JSONObject.fromObject("{\"alias\": \"Add to Cart\",\"dsType\": \"influxdb\",\"groupBy\": [{\"params\": [\"$interval\"],\"type\": \"time\"},{\"params\": [\"null\"],\"type\": \"fill\"}],\"measurement\": \"jmeter.Add_to_Cart.a.pct90\",\"policy\": \"default\",\"refId\": \"A\",\"resultFormat\": \"time_series\",\"select\": [[{\"params\": [\"value\"],\"type\": \"field\"},{\"params\": [],\"type\": \"mean\"}]],\"tags\": []}");
			
			jaRows = joDashBoardDetails.getJSONArray("rows");
			
			for( int i=0; i<jaRows.size(); i++ ) {
				joRow = jaRows.getJSONObject(i);
				
				jaPanels = joRow.getJSONArray("panels");
				
				for( int idxPanels=0; idxPanels<jaPanels.size(); idxPanels++ ) {
					joPanel = jaPanels.getJSONObject(idxPanels);
					
					if( joPanel.getString("title").equalsIgnoreCase("Average Transaction Response Time Analysis") ) { // TODO bring title from Appedo-LT-Execution-Services
						jaTargets = new JSONArray();
						
						iterTestName = hsTransactionController.iterator();
						while( iterTestName.hasNext() ) {
							strTestName = iterTestName.next();
							
							joTarget = JSONObject.fromObject( joTargetTemplate );
							joTarget.put("alias", strTestName);
							joTarget.put("dsType", strDataSourceName);
							// replace special characters into Influx's special character
							joTarget.put("measurement", getInfluxTableNameForTransaction(strDatabaseName, strTestName));
							
							jaTargets.add(joTarget);
						}
						
						joPanel.put("targets", jaTargets);
					}
				}
			}
			
		} catch(Throwable th) {
			System.out.println("Exception in updateAverageTransactionResponseTimeAnalysis: "+th.getMessage());
			th.printStackTrace();
		}
	}
	
	private void updateRequestResponseTable(JSONObject joDashBoardDetails, String strDataSourceName, String strDatabaseName, HashSet<String> hsHTTPSamplerProxy) {
		JSONArray jaRows = null, jaPanels = null, jaTargets = null;
		JSONObject joRow = null, joPanel = null, joTargetTemplate = null, joTarget = null;
		Iterator<String> iterTestName = null;
		String strTestName = null;
		
		try{
			joTargetTemplate = JSONObject.fromObject("{ \"alias\": \"/url/shares_json\", \"dsType\": \"influxdb\", \"groupBy\": [{ \"params\": [\"$interval\"], \"type\": \"time\" }, { \"params\": [\"null\"], \"type\": \"fill\" }], \"measurement\": \"jmeter./url/shares_json.ok.pct99\", \"policy\": \"default\", \"refId\": \"Q\", \"resultFormat\": \"time_series\", \"select\": [[{ \"params\": [\"value\"], \"type\": \"field\" }, { \"params\": [], \"type\": \"mean\" }]], \"tags\": [] }");
			
			jaRows = joDashBoardDetails.getJSONArray("rows");
			
			for( int idxRow=0; idxRow<jaRows.size(); idxRow++ ) {
				joRow = jaRows.getJSONObject(idxRow);
				
				jaPanels = joRow.getJSONArray("panels");
				
				for( int idxPanels=0; idxPanels<jaPanels.size(); idxPanels++ ) {
					joPanel = jaPanels.getJSONObject(idxPanels);
					
					if( joPanel.getString("title").equalsIgnoreCase("Request Response Table") ) { // TODO bring title from Appedo-LT-Execution-Services
						System.out.println("Started appending in <Request Response Table>");
						jaTargets = new JSONArray();
						
						iterTestName = hsHTTPSamplerProxy.iterator();
						while( iterTestName.hasNext() ) {
							strTestName = iterTestName.next();
							
							joTarget = JSONObject.fromObject( joTargetTemplate );
							joTarget.put("alias", strTestName);
							joTarget.put("dsType", strDataSourceName);
							// replace special characters into Influx's special character
							joTarget.put("measurement", getInfluxTableNameForRequest(strDatabaseName, strTestName) );
							
							jaTargets.add(joTarget);
						}
						
						joPanel.put("targets", jaTargets);
					}
				}
			}
			
		} catch(Throwable th) {
			System.out.println("Exception in updateRequestResponseTable: "+th.getMessage());
			th.printStackTrace();
		}
	}
	
	private JSONObject updatePrerequestiesInSQLite(String strTestName, int nTestDuration, String strHostIP, HashSet<String> hsTransactionController, HashSet<String> hsHTTPSamplerProxy) throws Exception {
		Connection con = null;
		PreparedStatement pstmtRead = null, pstmtUpdateDashboard = null, pstmtUpdateDatasource = null;
		ResultSet rst = null;
		
		String strDashBoardDetails = null;
		JSONObject joDashBoardDetails = null;
		
		try{
			con = SQLiteDataBaseManager.connect( Constants.GRAFANA_SQLITE_DB_FILEPATH );
			System.out.println("GRAFANA_SQLITE_DB_FILEPATH: "+Constants.GRAFANA_SQLITE_DB_FILEPATH);
			
			// Read DashBoard properties
			pstmtRead = con.prepareStatement("SELECT data FROM dashboard WHERE slug = ?");
			pstmtRead.setString(1, Constants.GRAFANA_DASHBOARD_DEFAULT_SLUG);
			System.out.println("GRAFANA_DASHBOARD_DEFAULT_SLUG: "+Constants.GRAFANA_DASHBOARD_DEFAULT_SLUG);
			
			rst = pstmtRead.executeQuery();
			
			if( rst.next() ) {
				strDashBoardDetails = rst.getString("data");
				
				joDashBoardDetails = JSONObject.fromObject( strDashBoardDetails );
				
				joDashBoardDetails.put("title", strTestName);
				joDashBoardDetails.getJSONObject("time").put("from", "now-"+nTestDuration+"m");
				joDashBoardDetails.getJSONObject("time").put("to", "now");
				joDashBoardDetails.put("refresh", "5s");
			}
			
			
			// Update JSON for the "Average Transaction Response Time Analysis" graph
			updateAverageTransactionResponseTimeAnalysis(joDashBoardDetails, Constants.GRAFANA_DATASOURCE_NAME, Constants.INFLUXDB_DATABASE_NAME, hsTransactionController);
			
			// Update JSON for the "Request Response Table" graph
			updateRequestResponseTable(joDashBoardDetails, Constants.GRAFANA_DATASOURCE_NAME, Constants.INFLUXDB_DATABASE_NAME, hsHTTPSamplerProxy);
			
			
			// Update DashBoard properties into SQLite
			pstmtUpdateDashboard = con.prepareStatement("UPDATE dashboard SET title = ?, data = ? WHERE slug = ?");
			pstmtUpdateDashboard.setString(1, strTestName);
			pstmtUpdateDashboard.setString(2, joDashBoardDetails.toString().replaceAll("\\[\"\\\\\"null\\\\\"\"\\]", "[\"null\"]"));
			pstmtUpdateDashboard.setString(3, Constants.GRAFANA_DASHBOARD_DEFAULT_SLUG);
			pstmtUpdateDashboard.executeUpdate();
			
			pstmtUpdateDatasource = con.prepareStatement("UPDATE data_source SET URL = ?||'://'||?||':'||? /* , is_default = 1 */ WHERE name = ?");
			pstmtUpdateDatasource.setString(1, Constants.INFLUXDB_PROTOCAL);
			pstmtUpdateDatasource.setString(2, strHostIP);
			pstmtUpdateDatasource.setString(3, Constants.INFLUXDB_PORT);
			pstmtUpdateDatasource.setString(4, Constants.GRAFANA_DATASOURCE_NAME);
			pstmtUpdateDatasource.executeUpdate();
			
		} catch (Throwable th) {
			System.out.println("Exception in updatePropertiesInSQLite: "+th.getMessage());
			throw th;
		} finally {
			SQLiteDataBaseManager.close(rst);
			SQLiteDataBaseManager.close(pstmtRead);
			
			SQLiteDataBaseManager.close(pstmtUpdateDashboard);
			
			SQLiteDataBaseManager.close(pstmtUpdateDatasource);
			
			SQLiteDataBaseManager.close(con);
		}
		
		return joDashBoardDetails;
	}
	
	private void updatePostDetailsInSQLite(JSONObject joDashBoardDetails, String strTestStartTime, String strTestEndTime) throws Throwable {
		Connection con = null;
		PreparedStatement pstmtUpdate = null;
		
		try{
			// Update in JSON
			joDashBoardDetails.getJSONObject("time").put("from", strTestStartTime);
			joDashBoardDetails.getJSONObject("time").put("to", strTestEndTime);
			joDashBoardDetails.remove("refresh");
			
			// Update in DB
			con = SQLiteDataBaseManager.connect( Constants.GRAFANA_SQLITE_DB_FILEPATH );
			
			pstmtUpdate = con.prepareStatement("UPDATE dashboard SET data = ? WHERE slug = ?");
			pstmtUpdate.setString(1, joDashBoardDetails.toString());
			pstmtUpdate.setString(2, Constants.GRAFANA_DASHBOARD_DEFAULT_SLUG);
			pstmtUpdate.executeUpdate();
			
		} catch (Throwable th) {
			System.out.println("Exception in updatePropertiesInSQLite: "+th.getMessage());
			throw th;
		} finally {
			SQLiteDataBaseManager.close(pstmtUpdate);
			
			SQLiteDataBaseManager.close(con);
		}
	}
	
	private void prepareQueries(String strDatabaseName, HashSet<String> hsTransactionController, HashSet<String> hsHTTPSamplerProxy, JSONObject joSnapshotPanelQueries) throws Throwable {
		Iterator<String> iterTransactionController;
		String strTransactionControllerName, strTransactionControllerTableName, strRequestResponseName, strRequestResponseTableName;
		JSONObject joQuery, joYAxisStyle;
		JSONArray jaSnapshotPanelQuery;
		
		try{
			joYAxisStyle = JSONObject.fromObject("{\"format\": \"ms\",\"label\": null,\"logBase\": 1,\"max\": null,\"min\": null,\"show\": true}");
			
			jaSnapshotPanelQuery = joSnapshotPanelQueries.getJSONArray("Average Transaction Response Time Analysis");
			
			iterTransactionController = hsTransactionController.iterator();
			while( iterTransactionController.hasNext() ) {
				strTransactionControllerName = iterTransactionController.next();
				strTransactionControllerTableName = getInfluxTableNameForTransaction(strDatabaseName, strTransactionControllerName);
				
				joQuery = new JSONObject();
				joQuery.put("target", strTransactionControllerName);
				joQuery.put("query", "SELECT mean(\"value\") FROM \""+strTransactionControllerTableName+"\" WHERE time > #@START_TIME_EPOCH_MS@#ms and time < #@END_TIME_EPOCH_MS@#ms GROUP BY time(2s) fill(\"null\")");
				joQuery.put("epoch", "ms");
				joQuery.put("y_axis", joYAxisStyle);
				
				jaSnapshotPanelQuery.add(joQuery);
			}
			
			

			jaSnapshotPanelQuery = joSnapshotPanelQueries.getJSONArray("Request Response Table");
			
			iterTransactionController = hsHTTPSamplerProxy.iterator();
			while( iterTransactionController.hasNext() ) {
				strRequestResponseName = iterTransactionController.next();
				strRequestResponseTableName = getInfluxTableNameForRequest(strDatabaseName, strRequestResponseName);
				
				joQuery = new JSONObject();
				joQuery.put("target", strRequestResponseName);
				joQuery.put("query", "SELECT mean(\"value\") FROM \""+strRequestResponseTableName+"\" WHERE time > #@START_TIME_EPOCH_MS@#ms and time < #@END_TIME_EPOCH_MS@#ms GROUP BY time(2s) fill(\"null\")");
				joQuery.put("epoch", "ms");
				
				jaSnapshotPanelQuery.add(joQuery);
			}
			
		} catch (Throwable th) {
			System.out.println("Exception in prepareQueries: "+th.getMessage());
			throw th;
		}
	}
	
	private JSONObject prepareSnapshotPostData(String strDatabaseName, String strReportName, String strLoadgenIpAddress, long lTestStartTime, long lTestEndTime, String strTestStartTime, String strTestEndTime, HashSet<String> hsTransactionController, HashSet<String> hsHTTPSamplerProxy) throws Throwable {
		String strPanelQuery, strTarget, strEpochUnit;
		
		JSONObject joPanel = null, joData = null, joSnapShotPostData, joSnapshotPanelQueries, joSnapshotQuery, joQueryResult, joYAxis;
		JSONArray jaRows, jaPanels, jaValues, jaSnapShotData, jaSnapshotQueries, jaResults, jaSeries, jaYAxis;
		
		WebServiceManager wsm = null;
		
		FileReader frSnapShotPostData = new FileReader(Constants.THIS_JAR_PATH+File.separator+"snapshot_post_data_template.json");
		BufferedReader brSnapShotPostData = new BufferedReader( frSnapShotPostData );
		String strSnapShotPostData = brSnapShotPostData.readLine();
		brSnapShotPostData.close();
		frSnapShotPostData.close();
		
		FileReader frSnapShotQueries = new FileReader(Constants.THIS_JAR_PATH+File.separator+"snapshot_post_data_queries.json");
		BufferedReader brSnapShotQueries = new BufferedReader( frSnapShotQueries );
		String strSnapShotQueries = brSnapShotQueries.readLine();
		brSnapShotQueries.close();
		frSnapShotQueries.close();
		
		joSnapShotPostData = JSONObject.fromObject(strSnapShotPostData);
		
		joSnapshotPanelQueries = JSONObject.fromObject(strSnapShotQueries);
		
		// prepare Queries for TransactionController and HTTPSamplerProxy
		prepareQueries(strDatabaseName, hsTransactionController, hsHTTPSamplerProxy, joSnapshotPanelQueries);
		
		
		
		// loop through each Row of the snapshot template
		jaRows = joSnapShotPostData.getJSONArray("rows");
		for( int idxRow=0; idxRow < jaRows.size(); idxRow++ ) {
			jaPanels = jaRows.getJSONObject(idxRow).getJSONArray("panels");
			
			// loop through each panel of the row
			for( int idxPanel=0; idxPanel < jaPanels.size(); idxPanel++ ) {
				joPanel = jaPanels.getJSONObject(idxPanel);
				jaSnapshotQueries = joSnapshotPanelQueries.getJSONArray( joPanel.getString("title") );
				
				// loop through each chart's query.
				// But get that from Snapshot Queries Template
				for(int idxSnapShotQueries = 0; idxSnapShotQueries < jaSnapshotQueries.size(); idxSnapShotQueries++ ) {
					joSnapshotQuery = jaSnapshotQueries.getJSONObject(idxSnapShotQueries);
					strTarget = joSnapshotQuery.getString("target");
					strPanelQuery = joSnapshotQuery.getString("query");
					strEpochUnit = joSnapshotQuery.getString("epoch");
					if( joSnapshotQuery.containsKey("y_axis") ) {
						joYAxis = joSnapshotQuery.getJSONObject("y_axis");
						
						// add the query result into SnapShotPanel
						jaYAxis = joPanel.getJSONArray("yaxes");
						jaYAxis.add(joYAxis);
					} else {
						joYAxis = null;
					}
					
					strPanelQuery = strPanelQuery.replace("#@START_TIME_EPOCH_MS@#", lTestStartTime+"");
					strPanelQuery = strPanelQuery.replace("#@END_TIME_EPOCH_MS@#", lTestEndTime+"");
					
					// Send the query to InfluxDB
					wsm = new WebServiceManager();
					System.out.println("Influx query: "+Constants.INFLUXDB_PROTOCAL+"://"+strLoadgenIpAddress+":"+Constants.INFLUXDB_PORT+"/query?u=&p=&db=jmeter&q="+strPanelQuery+"&epoch="+strEpochUnit);
					wsm.sendRequest(Constants.INFLUXDB_PROTOCAL+"://"+strLoadgenIpAddress+":"+Constants.INFLUXDB_PORT+"/query?u=&p=&db=jmeter&q="+URLEncoder.encode(strPanelQuery, "UTF-8")+"&epoch="+strEpochUnit);
					joQueryResult = wsm.getJSONObjectResponse();
					
					// add the query result into SnapShotPanel
					jaSnapShotData = joPanel.getJSONArray("snapshotData");
					
					// values retured by InflxDB is like [TIME, VALUE]
					// but Grafana's snapshot requires it as [VALUE, TIME]
					jaResults = joQueryResult.getJSONArray("results");
					if( jaResults.size() > 0 && jaResults.getJSONObject(0).containsKey("series") ) {
						jaSeries = jaResults.getJSONObject(0).getJSONArray("series");
						
						if( jaSeries.size() > 0 && jaSeries.getJSONObject(0).containsKey("values") ) {
							jaValues = jaSeries.getJSONObject(0).getJSONArray("values");
							replaceTimeValue(jaValues);
						} else {
							jaValues = new JSONArray();
						}
					} else {
						jaValues = new JSONArray();
					}
					
					joData = new JSONObject();
					joData.put("target", strTarget);
					joData.put("datapoints", jaValues);
					jaSnapShotData.add(joData);
				}
			}
		}
		
		joSnapShotPostData.put("title", strReportName);
		joSnapShotPostData.getJSONObject("time").put("from", strTestStartTime);
		joSnapShotPostData.getJSONObject("time").put("to", strTestEndTime);
		joSnapShotPostData.getJSONObject("time").getJSONObject("raw").put("from", strTestStartTime);
		joSnapShotPostData.getJSONObject("time").getJSONObject("raw").put("to", strTestEndTime);
		joSnapShotPostData.getJSONObject("snapshot").put("timestamp", UtilsFactory.formatDateToTimestamp(new Date().getTime(), true));
		
		return joSnapShotPostData;
	}
	
	/*
	private static JSONObject createSnapShotTemplate(String strReportName, JSONObject joSnapShotPostData) throws Throwable {
		WebServiceManager wsm = null;
		JSONObject joReturn = null;
		
		URL obj;
		HttpURLConnection conn = null;
		
		try{
			wsm = new WebServiceManager();
			
			/*
//			wsm.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			wsm.addHeader("Accept", "application/json, text/plain, *") /*");
			wsm.addHeader("Accept-Language", "en-US,en;q=0.5");
//			wsm.addHeader("Accept-Encoding", "gzip, deflate");
			wsm.addHeader("Authorization", "Bearer "+Constants.GRAFANA_COLLECTOR_API_KEY);
//			wsm.addHeader("Content-Type", "application/json;charset=utf-8"); 
////			wsm.addHeader("Content-Length", (strReportName.length()+joSnapShotPostData.toString().length())+"");
////			wsm.addHeader("Cookie", "grafana_user=admin; grafana_remember=701f29d4ceb65d2b0cb263c56fcb90cca2907c318356ef9c; grafana_sess=5e200fe5f5312849");
//			wsm.addHeader("Connection", "keep-alive");
			
//			wsm.addHeader("Accept", "application/json, text/plain, *") /*");
//			wsm.addHeader("Content-Type", "application/json; charset=UTF-8");
//			wsm.addHeader("Authorization", "Bearer "+Constants.GRAFANA_COLLECTOR_API_KEY);
//			wsm.addHeader("Content-Length", (36+strReportName.length()+joSnapShotPostData.toString().length())+"");
//			wsm.addHeader("Connection", "keep-alive");
			
			wsm.addParameter("dashboard", joSnapShotPostData.toString());
			wsm.addParameter("name", strReportName);
			wsm.addParameter("expires", "0");
			
			System.out.println("creating Snapshot: "+"http://"+Constants.GRAFANA_COLLECTOR_IP+":"+Constants.GRAFANA_COLLECTOR_PORT+"/api/snapshots");
			System.out.println("joSnapShotPostData.toString().length(): "+joSnapShotPostData.toString().length());
			System.out.println("data length: "+(36+strReportName.length()+joSnapShotPostData.toString().length()));
			wsm.sendRequest("http://"+Constants.GRAFANA_COLLECTOR_IP+":"+Constants.GRAFANA_COLLECTOR_PORT+"/api/snapshots");
//			System.out.println("joSnapShotPostData: "+joSnapShotPostData.toString());
			
			System.out.println("response: "+wsm.getResponse());
			
			joReturn = wsm.getJSONObjectResponse();
			*/
			
			/*
			obj = new URL( "http://"+Constants.GRAFANA_COLLECTOR_IP+":"+Constants.GRAFANA_COLLECTOR_PORT+"/api/snapshots" );
			//System.out.println("GET RedirectURL: "+strRedirectURL);
			conn = (HttpURLConnection) obj.openConnection();
			conn.setConnectTimeout( 20000 );
			conn.setReadTimeout( 20000 );
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json, text/plain, *") /*");
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Authorization", "Bearer "+Constants.GRAFANA_COLLECTOR_API_KEY);
//			conn.setRequestProperty("Content-Length", joSnapShotPostData.toString().length()+"");
			
			String urlParameters = "name="+strReportName+"&expires=0&dashboard="+URLEncoder.encode(joSnapShotPostData.toString(), "UTF-8");
//			String urlParameters = "name="+strReportName+"&expires=0&dashboard="+joSnapShotPostData;
			byte[] postData = urlParameters.getBytes( StandardCharsets.UTF_8 );
			int postDataLength = postData.length;
			System.out.println("postDataLength: "+postDataLength+" <> "+urlParameters.length());
			conn.setRequestProperty("Content-Length", postDataLength+"");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0"); // Sending Data as "Fixefox 45.0.2" browser
			conn.setRequestProperty("Connection", "keep-alive");
			
			conn.setDoOutput(true);
			
			try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream() ) ) {
				wr.write( postData );
				wr.flush();
			}
			
			int nResponseCode = conn.getResponseCode();
			System.out.println("nResponseCode: "+nResponseCode);
			*
			
//			ProcessBuilderManager.executeProcess(new File(Constants.THIS_JAR_PATH), "curl -X POST '"+"http://"+Constants.GRAFANA_COLLECTOR_IP+":"+Constants.GRAFANA_COLLECTOR_PORT+"/api/snapshots' --http1.1 -H 'Accept: application/json' -H 'Content-Type: application/json' -H 'Authorization: Bearer "+Constants.GRAFANA_COLLECTOR_API_KEY+"' --data '{ \"expires\": 0, \"name\": \""+strReportName+"\" }'");
		} catch(Throwable th) {
			System.out.println("Exception in createSnapShot: "+th.getMessage());
			throw th;
		} finally {
			System.out.println("Snapshot creation is over.");
//			System.out.println("snpshot response: "+wsm.getResponse());
		}
		
		return joReturn;
	}
	*/
	
	public void queueGrafanaSnapShotData(String loadTestSchedulerBean, JSONObject joSnapShotPostData) throws Throwable {
		WebServiceManager wsm = null;
		String strReturn = null;
		
		try{
			wsm = new WebServiceManager();
			// Snapshot length may grow in MB. As unable to send in "POST", sent in "StringRequestEntity".
			wsm.setStringEntity(loadTestSchedulerBean+"#@DELIM@#"+joSnapShotPostData.toString(), "application/json", "UTF-8");
			
			do{
				System.out.println("Queueing snapshot in "+Constants.LT_EXECUTION_SERVICES+"/ltScheduler/queueGrafanaSnapShotData");
				wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/queueGrafanaSnapShotData");
				
				strReturn = wsm.getResponse();
				
				System.out.println("Queue response: "+strReturn);
				
				// if unable to send the data then,
				// sleep for 10 seconds and re-try.
				if( strReturn == null ) {
					System.out.println("Unable to queue the snapshot. Retry will happen after 10 seconds.");
					Thread.sleep(10000l);
				}
			} while(strReturn == null);
		} catch(Throwable th) {
			System.out.println("Exception in queueGrafanaSnapShotData: "+th.getMessage());
			throw th;
		}
	}
	
	public void failJMeterRun(String loadTestSchedulerBean, String strErrorMessage) throws Throwable {
		WebServiceManager wsm = null;
		String strReturn = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.addParameter("loadTestSchedulerBean", loadTestSchedulerBean);
			wsm.addParameter("errorMessage", strErrorMessage);
			
			System.out.println("Sending fail signal: "+Constants.LT_EXECUTION_SERVICES+"/ltScheduler/failJMeterRun");
			wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/failJMeterRun");
			
			strReturn = wsm.getResponse();
			
			System.out.println("JMeter test fail signal response: "+strReturn);
		} catch(Throwable th) {
			System.out.println("Exception in failJMeterRun: "+th.getMessage());
			throw th;
		}
	}
	
	private String getInfluxTableNameForTransaction(String strDatabaseName, String strTestName) {
		return strDatabaseName+"."+strTestName.replaceAll(" ", "-").replaceAll("\\.", "_")+".a.pct90";
	}
	
	private String getInfluxTableNameForRequest(String strDatabaseName, String strTestName) {
		return strDatabaseName+"."+strTestName.replaceAll(" ", "-").replaceAll("\\.", "_")+".ok.pct99";
	}
	
	private static void replaceTimeValue(JSONArray jaValueTimePairs) {
		Object obj1 = null, obj2 = null;
		JSONArray jaValueTime = null;
		
		for( int idx = 0; idx < jaValueTimePairs.size(); idx++ ) {
			jaValueTime = jaValueTimePairs.getJSONArray(idx);
			obj1 = jaValueTime.get(0);
			obj2 = jaValueTime.get(1);
			jaValueTime.clear();
			
			jaValueTime.add(obj2);
			jaValueTime.add(obj1);
		}
	}
	
}
