package com.appedo.loadgen.threads;

import java.util.TimerTask;

/**
 * JMeter-Grafana User registration in Grafana.
 * Not in use for now, as direct URLs of Dashboard & Snapshots are working with login credentails.
 * 
 * @author Ramkumar R
 *
 */
public class JGUserSetupQueueDrainingThread extends TimerTask {
	
	@Override
	public void run() {
		/*
		 * No need to run this now.
		
		WebServiceManager wsm;
		JSONObject joResponse;
		boolean bContinue = false;
		
		do{
			try {
				System.out.println("Checking for User registeration queue: "+UtilsFactory.formatDateToTimestamp(new Date().getTime(), false));
				
				wsm = new WebServiceManager();
				wsm.sendRequest(Constants.LT_QUEUE_SERVICES+"/ltQueue/pollSetupGrafanaQueue");
				joResponse = wsm.getJSONObjectResponse();
				
				if( joResponse != null && joResponse.size() > 0 ) {
					System.out.println("New user create request for: "+joResponse.getString("email_id"));
					
					addNewGrafanaUser(joResponse);
					
					System.out.println("New user created: "+joResponse.getString("email_id"));
					
					bContinue = true;
				} else {
					bContinue = false;
				}
			} catch (Throwable th) {
				System.out.println("Exception in JMeterGrafanaUserRegistrationQueueDrainingThread.run: "+th.getMessage());
				th.printStackTrace();
			}
		} while (bContinue);
		*/
	}
	
	/**
	 * Not required now.
	 * 
	 * Add user in the DB, under Organization-id "1".
	 * 
	 * @param joNewUserBean
	 * @throws Throwable
	 *
	private void addNewGrafanaUser(JSONObject joNewUserBean) throws Throwable {
		Connection con = null;
		PreparedStatement pstmtInsert = null;
		String strCreatedOn;
		
		try{
			// Update in DB
			con = SQLiteDataBaseManager.connect( Constants.GRAFANA_SQLITE_DB_FILEPATH );
			
			strCreatedOn = UtilsFactory.formatDateToTimestamp(new Date().getTime(), true);
			
			pstmtInsert = con.prepareStatement("INSERT INTO user (version, login, email, name, password, salt, rands, company, org_id, is_admin, email_verified, theme, created, updated) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmtInsert.setInt(1, 0);
			pstmtInsert.setString(2, joNewUserBean.getString("email_id"));
			pstmtInsert.setString(3, joNewUserBean.getString("email_id"));
			pstmtInsert.setString(4, joNewUserBean.getString("full_name"));
			pstmtInsert.setString(5, Constants.GRAFANA_NEW_USER_PASSWORD);
			pstmtInsert.setString(6, Constants.GRAFANA_NEW_USER_PASSWORD_SALT);
			pstmtInsert.setString(7, Constants.GRAFANA_NEW_USER_PASSWORD_RANDS);
			pstmtInsert.setString(8, "");
			pstmtInsert.setInt(9, 1);
			pstmtInsert.setInt(10, 0);
			pstmtInsert.setInt(11, 0);
			pstmtInsert.setString(12, "light");
			pstmtInsert.setString(13, strCreatedOn);
			pstmtInsert.setString(14, strCreatedOn);
			pstmtInsert.executeUpdate();
			
		} catch (Throwable th) {
			System.out.println("Exception in addNewGrafanaUser: "+th.getMessage());
			throw th;
		} finally {
			SQLiteDataBaseManager.close(pstmtInsert);
			
			SQLiteDataBaseManager.close(con);
		}
	}
	*/
}
