package com.appedo.loadgen.threads;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.TimerTask;
import java.util.UUID;

import net.sf.json.JSONObject;

import com.appedo.jmeter.loadgen.common.Constants;
import com.appedo.jmeter.loadgen.connet.SQLiteDataBaseManager;
import com.appedo.jmeter.loadgen.utils.UtilsFactory;
import com.appedo.manager.WebServiceManager;

/**
 * JMeter-Grafana's Snapshot creator, with data sent from JMeter-LoadGen.
 * In each Grafana server, this thread has to run.
 * 
 * @author Ramkumar R
 *
 */
public class JGSnapshotQueueDrainingThread extends TimerTask {
	
	@Override
	public void run() {
		WebServiceManager wsm;
		JSONObject joResponse, joLoadTestSchedulerBean;
		String strGrafanaSnapshotData, strReportKey;
		boolean bContinue = false;
		
		do{
			try {
				System.out.println("Checking for JMeter snapshot queue: "+UtilsFactory.formatDateToTimestamp(new Date().getTime(), false));
				
				wsm = new WebServiceManager();
				wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/pollGrafanaSnapShotData");
				joResponse = wsm.getJSONObjectResponse();
				
				if( joResponse != null && joResponse.getBoolean("success") ) {
					joLoadTestSchedulerBean = JSONObject.fromObject( joResponse.getString("loadTestSchedulerBean") );
					strGrafanaSnapshotData = joResponse.getString("grafanaSnapshotData");
					
					strReportKey = UUID.randomUUID().toString().replaceAll("-", "");
					
					updateSnapShotData(joLoadTestSchedulerBean, strReportKey, strGrafanaSnapshotData);
					
					System.out.println("Snapshot created.");
					
					// Send "REPORT_GENERATION_COMPLETED to LT-Execution-Service
					completeJMeterRun( joResponse.getString("loadTestSchedulerBean"), strReportKey );
					System.out.println("JMeter test is completed.");
					
					bContinue = true;
				} else {
					bContinue = false;
				}
			} catch (Throwable th) {
				System.out.println("Exception in JMeterGrafanaSnapshotQueueDrainingThread.run: "+th.getMessage());
				th.printStackTrace();
			}
		} while (bContinue);
	}
	
	private void updateSnapShotData(JSONObject joLoadTestSchedulerBean, String strReportKey, String strGrafanaSnapshotData) throws Throwable {
		Connection con = null;
		PreparedStatement pstmtInsert = null;
		String strCreatedOn;
		
		try{
			// Update in DB
			con = SQLiteDataBaseManager.connect( Constants.GRAFANA_SQLITE_DB_FILEPATH );
			
			strCreatedOn = UtilsFactory.formatDateToTimestamp(new Date().getTime(), true);
			
			pstmtInsert = con.prepareStatement("INSERT INTO dashboard_snapshot (name, key, delete_key, org_id, user_id, external, external_url, dashboard, expires, created, updated) VALUES (?, ?, ?, ?, ?, 0, '', ?, '2066-12-30 05:47:31', ?, ?)");
			pstmtInsert.setString(1, joLoadTestSchedulerBean.getString("ReportName"));
			pstmtInsert.setString(2, strReportKey);
			pstmtInsert.setString(3, strReportKey);
			pstmtInsert.setInt(4, Constants.GRAFANA_APPEDO_ORG_ID);
			pstmtInsert.setInt(5, Constants.GRAFANA_APPEDO_ORG_USER_ID);
			pstmtInsert.setString(6, strGrafanaSnapshotData);
			pstmtInsert.setString(7, strCreatedOn);
			pstmtInsert.setString(8, strCreatedOn);
			pstmtInsert.executeUpdate();
			
		} catch (Throwable th) {
			System.out.println("Exception in updateSnapShotData: "+th.getMessage());
			throw th;
		} finally {
			SQLiteDataBaseManager.close(pstmtInsert);
			
			SQLiteDataBaseManager.close(con);
		}
	}
	
	public void completeJMeterRun(String loadTestSchedulerBean, String strReportKey) throws Throwable {
		WebServiceManager wsm = null;
		String strReturn = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.addParameter("loadTestSchedulerBean", loadTestSchedulerBean);
			wsm.addParameter("report_key", strReportKey);
			
			System.out.println("Sending complete signal: "+Constants.LT_EXECUTION_SERVICES+"/ltScheduler/completeJMeterRun");
			wsm.sendRequest(Constants.LT_EXECUTION_SERVICES+"/ltScheduler/completeJMeterRun");
			
			strReturn = wsm.getResponse();
			
			System.out.println("JMeter test complete signal response: "+strReturn);
		} catch(Throwable th) {
			System.out.println("Exception in completeJMeterRun: "+th.getMessage());
			throw th;
		}
	}
}
