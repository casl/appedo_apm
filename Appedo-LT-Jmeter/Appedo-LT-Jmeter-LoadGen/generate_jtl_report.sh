# execute from Appedo-JMeter-LoadGen's location.

echo "PATH = " $PATH

JAVA_HOME="/var/java/jdk1.7.0_67"
export JAVA_HOME
PATH=$PATH:$JAVA_HOME/bin

#APACHE_ANT_HOME="/var/apache-ant-1.9.7"
#export APACHE_ANT_HOME
#PATH=$PATH:$APACHE_ANT_HOME/bin

export PATH

echo "New PATH = " $PATH

java -jar jtlreport.jar $1 $2
