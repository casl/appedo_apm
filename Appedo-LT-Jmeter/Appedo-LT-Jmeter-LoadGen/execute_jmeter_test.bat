cd C:\Appedo\resource\apache-jmeter-2.13\extras
ant -Dtest %1 run


# should be executed from C:\program files\apache-jmeter-3.1\extras

#echo "PATH = " $PATH

#JAVA_HOME="/var/java/jdk1.7.0_67"
#export JAVA_HOME

#PATH=$PATH:$JAVA_HOME/bin


#APACHE_ANT_HOME="/var/apache-ant-1.9.7"
#export APACHE_ANT_HOME
#PATH=$PATH:$APACHE_ANT_HOME/bin


#export PATH

#echo "New PATH = " $PATH

C:\program files\apache-jmeter-3.1\bin\jmeter -n -t ..\extras\%1.jmx -l ..\extras\%1.jtl

# ANT command can be also used. But not required now.
# ant -Dtest $1 run
