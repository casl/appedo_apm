# should be executed from /var/apache-jmeter-3.1/extras

echo "PATH = " $PATH

JAVA_HOME="/var/java/jdk1.7.0_67"
export JAVA_HOME

PATH=$PATH:$JAVA_HOME/bin


#APACHE_ANT_HOME="/var/apache-ant-1.9.7"
#export APACHE_ANT_HOME
#PATH=$PATH:$APACHE_ANT_HOME/bin


export PATH

echo "New PATH = " $PATH

/var/apache-jmeter-3.1/bin/jmeter -n -t ../extras/$1.jmx -l ../extras/$1.jtl -R$2

# ANT command can be also used. But not required now.
# ant -Dtest $1 run
