module.exports = {
    pgDbConfig:{
        user: 'postgres',
        database: 'appedo_apm',
        password: 'sa',
        host: 'localhost',
        port: 5432,
        max: 50, // max number of clients in the pool
        idleTimeoutMillis: 300000,
    },
    chDbConfig:{
        user: 'default',
        password: 'sa',
        host: 'localhost',
        port: 8123,
        database: 'appedo_apm',
        path: "/usr/bin"
    },
    scheduleEvery: 30*60*1000, //Every 1 Hour.
    pgDMSource:{
        host: 'localhost',
        path: "/opt/PostgreSQL/9.6",
        port: 5432,
        user: 'postgres',
        password: 'sa', 
        database: 'appedo_apm'
    },
    chDMDest:{
        host: 'localhost',
        port: 9000,
        user: 'default',
        password:'sa', 
        database: 'appedo_apm'
    },
    firstTimeMigration: false
}

//running the service in production mode for the first time below code to be used.
/*
set NODE_ENV=production //only for the first time.
node data_mig.js
*/

