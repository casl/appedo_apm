var express = require('express');
const mountRoutes = require('./routes');
const path = require('path');
const bodyParser = require('body-parser');
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());
mountRoutes(app);

console.log("Data migration Service Started");
var httpPort = 8989;
app.listen(httpPort, () => console.log('Data Migration Service listening at port '+httpPort+'!'));

process.on('exit', (code) => {
    console.log(`About to exit with code: ${code}`);
});

process.on('beforeExit', (code) => {
    console.log('Process beforeExit event with code: ', code);
});

process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', promise, 'reason:', reason);
    // Application specific logging, throwing an error, or other logic here
});
  
