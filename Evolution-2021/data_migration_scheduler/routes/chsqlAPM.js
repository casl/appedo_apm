const { ClickHouse } = require('clickhouse');
const ChConfig = require('../config/apd_constants');
global.logger = require('../log');
const chcustom = new ClickHouse({
    database: ChConfig.chDbConfig.database,
    host: ChConfig.chDbConfig.host,
    port: ChConfig.chDbConfig.port,
    basicAuth: {
        username: ChConfig.chDbConfig.user,
        password: ChConfig.chDbConfig.password
    },
    config: {
        session_timeout                         : 60,
        output_format_json_quote_64bit_integers : 0,
        enable_http_compression                 : 0
    }
});

module.exports.chDbQuery = chDbQuery;

async function chDbQuery(methodName,queryText){
    let start;
    let qryResult;
    try {
        start = Date.now();
        try {
            const result = await chcustom.query(queryText).toPromise();
            const duration = Date.now() - start;
            console.log(new Date().toLocaleString()+", "+methodName+" , "+duration+' ms');
            logger.info("PID: %d, DB:CH, MethodName: %s, Duration: %d ms",process.pid,methodName,duration)
            qryResult =  {success:true, error: false, result: result};
        } catch (e) {
            qryResult = {success:false, error: true, message: e.message};
            // logger.error("PID: %d, DB:CH, MethodName: %s, ErrMsg: %s, Stack: %s",process.pid,methodName,e.message, e.stack);
        }
    } catch (e){
        logger.error("PID: %d, DB:CH, MethodName: %s, ErrMsg: %s, Stack: %s",process.pid,methodName,e.message, e.stack);
        qryResult = {success:false, error: true, message: e.message};
    } finally {
        return qryResult;
    }
}