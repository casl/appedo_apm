const Router = require('express-promise-router');
const settings = require('../config/apd_constants');
global.logger = require('../log');
const { execSync } = require('child_process');
const psqlAPM = require('./psqlAPM');
const chAPM = require('./chsqlAPM');
const e = require('express');
const logger = require('../log');
psqlDMSource = settings.pgDMSource;
chDMDest = settings.chDMDest;
psqlDMConnStr = psqlDMSource.path+"/bin/psql -U "+psqlDMSource.user+" -h "+psqlDMSource.host+" -p "+psqlDMSource.port+" -d "+psqlDMSource.database;
chDMConnStr = "clickhouse-client --host "+chDMDest.host+" --port "+chDMDest.port+" --user "+chDMDest.user+" --password '"+chDMDest.password+"' -d "+chDMDest.database;

const router = new Router();
module.exports = router;
console.log("Data Migration scheduler started");
let maxRunTimeForBatch = Math.floor(settings.scheduleEvery*0.85);
logger.info("Scheduled every %s minutes, Max Batch Run time for every schedule is %s minutes", settings.scheduleEvery/(60*1000), maxRunTimeForBatch/(60*1000));
/** AVM Variables */
let avmUserIdArr = [];
let avmUidCnt = 0;
let avmProcessedCnt = 0;
let psqlAVMUid = {};
let chAvmUidLUpd = {};

/** Collector Variables.*/
let psqlCollectorUid = {};
let chCollectorUidLUpd = {};
let collProcessedCnt = 0;
let collUidCnt = 0;
let collUserIdArr = [];

getCollectorUIDsPSQL(); //Collector data processing
getAVMUIDsPSQL(); //Avm Log Processing 
setTimeout(() => {
  getNetworkUIDsPSQL(); //start the network collection with 20 Sec Delay.
}, 60000);
setTimeout(() => {
  getLogUIDsPSQL(); //start the log collection with 40 Sec Delay.
}, 120000);

if (!settings.firstTimeMigration){
  setInterval(() => {
    logger.info("%s minutes Schedule Triggered at %s", settings.scheduleEvery/(60*1000), new Date().toLocaleString());
    getCollectorUIDsPSQL();
    getAVMUIDsPSQL();
    setTimeout(() => {
      getNetworkUIDsPSQL();
    }, 60000);
    setTimeout(() => {
      getLogUIDsPSQL();
    }, 120000);
  }, settings.scheduleEvery);
}

/* AVM Log data processing */

async function getAVMUIDsPSQL(){
  try {
    avmUserIdArr = [];
    avmUidCnt = 0;
    avmProcessedCnt = 0;
    psqlAVMUid = {};
    chAvmUidLUpd = {};
    let queryText = "SELECT user_id, avm_test_id as uid, last_appedo_received_on FROM avm_test_master WHERE last_appedo_received_on IS NOT NULL ORDER BY user_id, avm_test_id;"
    let result = await psqlAPM.fnDbQuery("getAVMUIDsPSQL",queryText,[]);
    if (result.success && result.rowCount > 0){
      avmUidCnt = result.rowCount;
      result.rows.map(row => {
        if (psqlAVMUid['str'+row.user_id] == undefined) psqlAVMUid['str'+row.user_id] = {};
        psqlAVMUid['str'+row.user_id]['str'+row.uid] = row.last_appedo_received_on;
      });
      avmUserIdArr = [...new Set(result.rows.map(item => item.user_id))]; //to get the unique user_id
      for (let i = 0; i < avmUserIdArr.length; i++){
        let user = avmUserIdArr[i];
        await getAVMLastUpdatedDateChForUser(user);
      }
      processAvmExecCmd(avmUidCnt, avmUserIdArr);
      // avmUserIdArr.map(user => {
      // });
    } else {
      if (result.rowCount == 0){
        logger.info("No data found in AVM Test Master.")
      } else {
        logger.error("Failed to get data from AVM test master ErrMsg: %s", result.message);
      }
    }
  } catch(e){
    logger.error("getAVMUIDsPSQL Exception: errMsg: %s, errStack: %s", e.message, e.stack)
  }
}

async function getAVMLastUpdatedDateChForUser(userId){
  try{
    let tableName = "log_avm_"+userId;
    let queryText = "SELECT uid, MAX(received_on) as last_received_on FROM "+tableName+" GROUP BY uid"
    let result = await chAPM.chDbQuery("getAVMLastUpdatedDateChForUser-LastUpdatedOn",queryText);
    if (result.error){
      if (result.message.includes("404: Code: 60")){
        let crQuery = "CREATE TABLE IF NOT EXISTS "+tableName+"(uid UInt32,received_on DateTime,appedo_received_on DateTime,country Nullable(String), state Nullable(String), region Nullable(String),zone Nullable(String),city Nullable(String),resp_time_ms Nullable(Float64),bandwidth Nullable(Float64),status_code Int16,status_text String, content_length Nullable(Int64),resp_body String, err_code Nullable(String), err_msg Nullable(String), errno Nullable(String),agent_tested_on DateTime, created_on DateTime DEFAULT now()) ENGINE = MergeTree() PARTITION BY toYYYYMM(appedo_received_on) ORDER BY (uid)";
        let result1 = await chAPM.chDbQuery("getAVMLastUpdatedDateChForUser--createTable", crQuery);
        if (result1.success){
          if (chAvmUidLUpd['str'+userId] == undefined) chAvmUidLUpd['str'+userId] = {};
          logger.info("getAVMLastUpdatedDateChForUser - AVM Log - Created table in Clickhouse for User: %d", userId);
        } else {
          logger.error("getAVMLastUpdatedDateChForUser - AVM Log - Create Table failed in clickhouse for User: %d, Error: %s", userId, result1.message);
        }
      } else {
        logger.error("dataMig.getAVMLastUpdatedDateChForUser-LastUpdatedOn - %d, Error: %s", userId, result.message+" table Name "+tableName);
      }
    } else {
      if (result.result.length == 0){
        if (chAvmUidLUpd['str'+userId] == undefined) chAvmUidLUpd['str'+userId] = {};
      }
      result.result.map(row => {
        if (chAvmUidLUpd['str'+userId] == undefined) chAvmUidLUpd['str'+userId] = {};
        chAvmUidLUpd['str'+userId]['str'+row.uid] = row.last_received_on;
      });
    }
  } catch (e){
    logger.error("getAVMLastUpdatedDateChForUser Exception: User: %s, errMsg: %s, errStack: %s", userId, e.message, e.stack);
  }
  // avmProcessedCnt++;
  // if (avmUserIdArr.length == avmProcessedCnt){ 
  //   //since index is not created stopping these service -- 11-10-2020 by SK
  //   // processAvmExecCmd(avmUidCnt, avmUserIdArr);
  // }
}

async function processAvmExecCmd(rowCount, userIdArr) {
  logger.info("Total AVM Test ID to be processed %d", rowCount);
  userIdArr.map(user => {
    let uids = Object.keys(psqlAVMUid['str' + user]);
    uids.map(uid => {
      if (chAvmUidLUpd['str' + user] == undefined) {
        logger.error("processAvmExecCmd - User is undefined in chAvmUidLUpd Array %s", user);
      }
      else {
        if (chAvmUidLUpd['str' + user][uid] != undefined) {
          if (new Date(psqlAVMUid['str' + user][uid]).getTime() >= new Date(chAvmUidLUpd['str' + user][uid]).getTime()) {
            execShCmdForAVM(user, uid.replace('str', ''), chAvmUidLUpd['str' + user][uid]);
          }
          else {
            if (new Date(chAvmUidLUpd['str' + user][uid]).toString() == "Invalid Date") {
              execShCmdForAVM(user, uid.replace('str', ''), null);
            }
            else {
              logger.info("processAvmExecCmd - User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(psqlAVMUid['str' + user][uid]), new Date(chAvmUidLUpd['str' + user][uid]));
            }
          }
        }
        else {
          execShCmdForAVM(user, uid.replace('str', ''), null);
        }
      }
    });
  });
}

async function execShCmdForAVM(userId, uid, last_received_on){
  try {
    let psqlTable = "log_avm_"+uid;
    let chTable = "log_avm_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdForAVM's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'"
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdForAVM -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdForAVM - No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.info("execShCmdForAVM - processing %s Rows for User:%s UID: %s", rowCnt, userId, uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdForAVM executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdForAVM executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdForAVM - No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.error("execShCmdForAVM - Error Received for User:%s UID: %s ErrMsg: %s", userId, uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      if (last_received_on != undefined){
        query = psqlDMConnStr +" -c \"copy (SELECT avm_test_id,to_char(created_on,'yyyy-MM-dd HH24:mi:SS') AS received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') AS appedo_received_on, country, state, region, zone, city, resp_time_ms, bandwidth, status_code, status_text,content_length, resp_body, err_code, err_msg, errno, to_char(agent_tested_on,'yyyy-MM-dd HH24:mi:SS') as agent_tested_on FROM "+psqlTable+" WHERE created_on > '"+last_received_on+"' offset "+offset+" limit "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+" --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, country, state,region,zone,city,resp_time_ms,bandwidth,status_code, status_text,content_length, resp_body, err_code, err_msg, errno, agent_tested_on) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +" -c \"copy (SELECT avm_test_id,to_char(created_on,'yyyy-MM-dd HH24:mi:SS') AS received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') AS appedo_received_on, country, state, region, zone, city, resp_time_ms, bandwidth, status_code, status_text,content_length, resp_body, err_code, err_msg, errno, to_char(agent_tested_on,'yyyy-MM-dd HH24:mi:SS') as agent_tested_on FROM "+psqlTable+" OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+" --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, country, state,region, zone, city, resp_time_ms, bandwidth, status_code, status_text, content_length, resp_body, err_code, err_msg, errno, agent_tested_on) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = false;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdForAVM No Rows Found for User:%s UID: %s", userId, uid);
        } else {
          logger.error("execShCmdForAVM - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdForAVM Successfully inserted data for Batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt,userId, uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdForAVM Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uid, last_received_on, e.message, e.stack );
  }
}

/* Collector data processing */

async function getCollectorUIDsPSQL(){
  try {
    collUserIdArr = [];
    collUidCnt = 0;
    collProcessedCnt = 0;
    psqlCollectorUid = {};
    chCollectorUidLUpd = {};

    let dt = new Date().toDateString();
    let queryText = "SELECT user_id, system_id, uid, last_appedo_received_on FROM module_master WHERE module_code IN ('APPLICATION', 'DATABASE', 'SERVER') AND last_appedo_received_on IS NOT NULL ORDER BY user_id, uid;"
    let result = await psqlAPM.fnDbQuery("getCollectorUIDsPSQL",queryText,[]);
    if (result.success && result.rowCount > 0){
      collUidCnt = result.rowCount;
      result.rows.map(row => {
        if (psqlCollectorUid['str'+row.user_id] == undefined) psqlCollectorUid['str'+row.user_id] = {};
        psqlCollectorUid['str'+row.user_id]['str'+row.uid] = row.last_appedo_received_on;
      });
      collUserIdArr = [...new Set(result.rows.map(item => item.user_id))]; //to get the unique user_id
      for (let i = 0; i < collUserIdArr.length; i++){
        let user = collUserIdArr[i];
        await getLastUpdatedDateChForUser(user, collUidCnt);
      }
      processExecuteCommand(collUidCnt, collUserIdArr);
      // collUserIdArr.map(user => {
      //   getLastUpdatedDateChForUser(user, collUidCnt);
      // });
    } else {
      if (result.rowCount == 0){
        logger.info("No data found in Module Master.")
      } else {
        logger.error("Failed to get data from module master %s", result.message)
      }
    }
  } catch(e){
    logger.error("getCollectorUIDsPSQL Exception: errMsg: %s, errStack: %s", e.message, e.stack)
  }
}

async function processExecuteCommand(rowCount, userIdArr) {
  logger.info("processExecuteCommand - Total UID to be processed %d", rowCount);
  for (let i = 0; i < userIdArr.length; i++){
    let user = userIdArr[i];
    let uids = Object.keys(psqlCollectorUid['str' + user]);
    for (let j = 0; j < uids.length; j++){
      let uid = uids[j];
      if (chCollectorUidLUpd['str' + user] == undefined) {
        logger.error("processExecuteCommand - User undefined in chCollectorUidLUpd array  %s", user);
      }
      else {
        if (chCollectorUidLUpd['str' + user][uid] != undefined) {
          if (new Date(psqlCollectorUid['str' + user][uid]).getTime() >= new Date(chCollectorUidLUpd['str' + user][uid]).getTime()) {
            await execShCmdForColl(user, uid.replace('str', ''), chCollectorUidLUpd['str' + user][uid]);
          }
          else {
            if (new Date(chCollectorUidLUpd['str' + user][uid]).toString() == "Invalid Date") {
              await execShCmdForColl(user, uid.replace('str', ''), null);
            }
            else {
              logger.info("User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(psqlCollectorUid['str' + user][uid]), new Date(chCollectorUidLUpd['str' + user][uid]));
            }
          }
        }
        else {
          await execShCmdForColl(user, uid.replace('str', ''), null);
        }
      }
    }
  }
}

async function getLastUpdatedDateChForUser(userId){
  try{
    let tableName = "collector_"+userId;
    let queryText = "SELECT uid, MAX(received_on) as last_received_on FROM "+tableName+" GROUP BY uid  ORDER BY uid "
    let result = await chAPM.chDbQuery("getLastUpdatedDateChForUser-LastUpdatedOn",queryText);
    if (result.error){
      if (result.message.includes("404: Code: 60")){
        let tName = "collector_"+userId;
        let crQuery = "CREATE TABLE IF NOT EXISTS "+tName+"(uid UInt32,received_on DateTime,appedo_received_on DateTime,counter_type UInt32, counter_name String, exception Nullable(String),counter_value Float64,process_name Nullable(String), created_on DateTime DEFAULT now()) ENGINE = MergeTree() PARTITION BY toYYYYMM(appedo_received_on) ORDER BY (uid, counter_type)";
        let result1 = await chAPM.chDbQuery("getLastUpdatedDateChForUser--createTable", crQuery);
        if (result1.success){
          if (chCollectorUidLUpd['str'+userId] == undefined) chCollectorUidLUpd['str'+userId] = {};
          logger.info("getLastUpdatedDateChForUser - Agent - Created table in Clickhouse for User: %d", userId);
        } else {
          logger.error("getLastUpdatedDateChForUser - Agent - Create Table failed in clickhouse for User: %d, Error: %s", userId, result1.message);
        }
      } else {
        logger.info("getLastUpdatedDateChForUser-LastUpdatedOn: %s",result.message +" for table name "+tableName+ " Query "+ queryText);
      }
    } else {
      if (result.result.length == 0){
        if (chCollectorUidLUpd['str'+userId] == undefined) chCollectorUidLUpd['str'+userId] = {};
      }
      result.result.map(row => {
        if (chCollectorUidLUpd['str'+userId] == undefined) chCollectorUidLUpd['str'+userId] = {};
        chCollectorUidLUpd['str'+userId]['str'+row.uid] = row.last_received_on;
      });
    }
  } catch (e){
    logger.error("getLastUpdatedDateChForUser Exception: User: %s, errMsg: %s, errStack: %s", userId, e.message, e.stack);
  }
}

async function execShCmdForColl(userId, uid, last_received_on){
  try {
    let psqlTable = "collector_"+uid;
    let psqlCMTable = "counter_master_"+uid;
    let chTable = "collector_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdForColl's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'"
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdForColl -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdForColl - No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.info("execShCmdForColl - processing %s Rows for User:%s UID: %s", rowCnt, userId, uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdForColl executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdForColl executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdForColl - No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.error("execShCmdForColl - Error Received for User:%s UID: %s ErrMsg: %s", userId, uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (select c.uid,to_char(c.received_on,'yyyy-MM-dd HH24:mi:SS') as received_on, to_char(c.appedo_received_on,'yyyy-MM-dd HH24:mi:SS') as appedo_received_on, c.counter_type,cm.counter_name, c.exception,c.counter_value, c.process_name from "+psqlTable+" as c JOIN "+psqlCMTable+" as cm ON cm.counter_id=c.counter_type where received_on > '"+last_received_on+"' offset "+offset+" limit "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, counter_type, counter_name, exception, counter_value, process_name) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (select c.uid,to_char(c.received_on,'yyyy-MM-dd HH24:mi:SS') as received_on, to_char(c.appedo_received_on,'yyyy-MM-dd HH24:mi:SS') as appedo_received_on, c.counter_type,cm.counter_name, c.exception,c.counter_value, c.process_name from "+psqlTable+" as c JOIN "+psqlCMTable+" as cm ON cm.counter_id=c.counter_type OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, counter_type, counter_name, exception, counter_value, process_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = false;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdForColl No Rows Found for User:%s UID: %s", userId, uid);
        } else {
          logger.error("execShCmdForColl - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdForColl Successfully inserted data for Batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt,userId, uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdForColl Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uid, last_received_on, e.message, e.stack );
  }
}

/* The below service is used for testing this data migration service from AVM module */
router.get('/testDataMigService', async(req,res)=>{
  try {
    res.json({success:true, message:"Success", result : "Data Migration Service running with pid:"+process.pid});
  } catch (e) {
    logger.error(process.pid,e.stack, 'testDataMigService');
    res.json({success:false, error:true, message: e.stack});
  }
})

/** Network Data Migration code */
let networkPsqluid = {};
let networkChUidLUpd = {};
let networkProcessedCnt = 0;
let networkUidCnt = 0;
let networkUserIdArr = [];

async function getNetworkUIDsPSQL(){
  try {
    networkPsqluid = {};
    networkChUidLUpd = {};
    networkProcessedCnt = 0;
    networkUidCnt = 0;
    networkUserIdArr = [];
    let queryText = "SELECT user_id, system_id, uid, last_appedo_received_on FROM module_master WHERE module_code IN ('NETWORK') AND last_appedo_received_on is not null ORDER BY user_id, uid;"
    //last_appedo_received_on >='"+dt+"' AND
    let result = await psqlAPM.fnDbQuery("getNetworkUIDsPSQL",queryText,[]);
    if (result.success && result.rowCount > 0){
      networkUidCnt = result.rowCount;
      result.rows.map(row => {
        if (networkPsqluid['str'+row.user_id] == undefined) networkPsqluid['str'+row.user_id] = {};
        networkPsqluid['str'+row.user_id]['str'+row.uid] = row.last_appedo_received_on;
      });
      networkUserIdArr = [...new Set(result.rows.map(item => item.user_id))]; //to get the unique user_id
        //   
      for (let i = 0; i < networkUserIdArr.length; i++){
        let user = networkUserIdArr[i];
        await getNetworkLUDChForUserUid(user);
      }
      processNetworkExecCmd(networkUidCnt, networkUserIdArr);
      // networkUserIdArr.map(user => {
      //   getNetworkLUDChForUserUid(user);
      // });
    } else {
      if (result.rowCount == 0){
        logger.info("No data found in Module Master.")
      } else {
        logger.error("Failed to get data from module master %s", result.message)
      }
    }
  } catch(e){
    logger.error("getCollectorUIDsPSQL Exception: errMsg: %s, errStack: %s", e.message, e.stack)
  }
}

async function getNetworkLUDChForUserUid(userId){
  try{
    let tableName = "network_"+userId;
    let queryText = " SELECT concat(network_type,'_',toString(uid)) as uid, MAX(received_on) as last_received_on FROM "+tableName+" GROUP BY uid  ORDER BY uid "
    let result = await chAPM.chDbQuery("getNetworkLUDChForUserUid-LastUpdatedOn",queryText);
    if (result.error){
      if (result.message.includes("404: Code: 60")){
        let tName = "network_"+userId;
        let crQuery = "CREATE TABLE IF NOT EXISTS "+tName+"(system_id UInt16, uid UInt32, received_on DateTime, appedo_received_on DateTime,destination_ip Nullable(String), client_ip Nullable(String), source_ip Nullable(String), event_category Nullable(String), event_duration Nullable(UInt32), network_protocol Nullable(String), network_bytes Nullable(UInt32), network_type String, network_direction Nullable(String), network_transport Nullable(String), status Nullable(String), req_status Nullable(String), req_msg Nullable(String), req_status_code smallint, res_msg Nullable(String), res_status_code smallint, res_status Nullable(String), created_on DateTime DEFAULT now()) ENGINE = MergeTree() PARTITION BY toYYYYMM(appedo_received_on) ORDER BY (network_type, uid)";
        let result1 = await chAPM.chDbQuery("getNetworkLUDChForUserUid--createTable", crQuery);
        if (result1.success){
          if (networkChUidLUpd['str'+userId] == undefined) networkChUidLUpd['str'+userId] = {};
          logger.info("getNetworkLUDChForUserUid - Created table for network in Clickhouse for User: %d", userId);
        } else {
          logger.error("getNetworkLUDChForUserUid-CreateTable %d, Error:%s", userId, result1.message);
        }
      } else {
        logger.error("dataMig.getNetworkLUDChForUserUid-LastUpdatedOn - %d, Error: %s", userId, result.message);
      }
    } else {
      if (result.result.length == 0){
        if (networkChUidLUpd['str'+userId] == undefined) networkChUidLUpd['str'+userId] = {};
      }
      result.result.map(row => {
        if (networkChUidLUpd['str'+userId] == undefined) networkChUidLUpd['str'+userId] = {};
        networkChUidLUpd['str'+userId][row.uid] = row.last_received_on;
      });
      // console.log("networkChUidLUpd",networkChUidLUpd);
    }
  } catch (e){
    logger.error("getNetworkLUDChForUserUid Exception: User: %s, errMsg: %s, errStack: %s", userId, e.message, e.stack);
  }
}

async function processNetworkExecCmd(rowCount, userIdArr) {
  try {
    logger.info("processNetworkExecCmd - Total UID to be processed  for Network %d", rowCount);
    for (let i = 0; i < userIdArr.length; i++){
      let user = userIdArr[i];
      let uids = Object.keys(networkPsqluid['str' + user]);
      for (let j = 0; j < uids.length; j++ ){
        let uid = uids[j];
        let htmlUid = "http_"+uid.replace('str','');
        let icmpUid = "icmp_"+uid.replace('str','');
        if (networkChUidLUpd['str' + user] == undefined) {
          logger.error("processNetworkExecCmd - User undefined in networkChUidLUpd array %s", user);
        }
        else {
          if (networkChUidLUpd['str' + user][htmlUid] != undefined) {
            if (new Date(networkPsqluid['str' + user][uid]).getTime() > new Date(networkChUidLUpd['str' + user][htmlUid]).getTime()) {
              await execShCmdForNetworkHTTP(user, uid.replace('str', ''), networkChUidLUpd['str' + user][htmlUid]);
            }
            else {
              if (new Date(networkChUidLUpd['str' + user][htmlUid]).toString() == "Invalid Date") {
                logger.info("HTTP - User: %s, Uid: %s, last received on: %s, is invalid date, hence collecting data without date filter.", user, uid.replace('str', ''), new Date(networkChUidLUpd['str' + user][htmlUid]));
                await execShCmdForNetworkHTTP(user, uid.replace('str', ''), null);
              }
              else {
                logger.info("HTTP - User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(networkPsqluid['str' + user][uid]), new Date(networkChUidLUpd['str' + user]['http_'+(uid.replace('str',''))]));
              }
            }
          }
          else {
            // logger.info("User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(networkPsqluid['str' + user][uid]), new Date(networkChUidLUpd['str' + user]['http_'+(uid.replace('str',''))]));
            await execShCmdForNetworkHTTP(user, uid.replace('str', ''), null);
          }
          if (networkChUidLUpd['str' + user][icmpUid] != undefined) {
            if (new Date(networkPsqluid['str' + user][uid]).getTime() > new Date(networkChUidLUpd['str' + user][icmpUid]).getTime()) {
              await execShCmdForNetworkICMP(user, uid.replace('str', ''), networkChUidLUpd['str' + user][icmpUid]);
            }
            else {
              if (new Date(networkChUidLUpd['str' + user][icmpUid]).toString() == "Invalid Date") {
                logger.info("ICMP - User: %s, Uid: %s, last received on: %s, is invalid date, hence collecting data without date filter.", user, uid.replace('str', ''), new Date(networkChUidLUpd['str' + user][icmpUid]));
                await execShCmdForNetworkICMP(user, uid.replace('str', ''), null);
              }
              else {
                logger.info("ICMP - User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(networkPsqluid['str' + user][uid]), new Date(networkChUidLUpd['str' + user][icmpUid]));
              }
            }
          }
          else {
            // logger.info("User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.replace('str', ''), new Date(networkPsqluid['str' + user][uid]), new Date(networkChUidLUpd['str' + user]['http_'+(uid.replace('str',''))]));
            await execShCmdForNetworkICMP(user, uid.replace('str', ''), null);
          }
        }
      }
    }
  } catch(e) {
    logger.error("processNetworkExecCmd exception received Emsg: %s, stack: %s", e.message, e.stack);
  }
}

async function execShCmdForNetworkHTTP(userId, uid, last_received_on){
  try {
    let psqlTable = "network_http_v7_"+userId;
    let chTable = "network_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdForNetworkHTTP's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s", maxRunTimeForBatch/(60*1000), userId, uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"' AND uid="+uid;
    } else {
      query = "select count(*) from "+psqlTable+" WHERE appedo_received_on > '"+curDate+"' AND uid="+uid;
    }
    console.log("execShCmdForNetworkHTTP", query);
    let result = await psqlAPM.fnDbQuery("execShCmdForNetworkHTTP -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdForNetworkHTTP No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.info("execShCmdForNetworkHTTP processing %s Rows for User:%s UID: %s", rowCnt, userId, uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdForNetworkHTTP executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
        batchCnt += 1;
      }
      logger.info("execShCmdForNetworkHTTP executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdForNetworkHTTP No UID Found for User:%s UID: %s", userId, uid);
      } else {
        logger.error("execShCmdForNetworkHTTP Error Received for User:%s UID: %s ErrMsg: %s", userId, uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on, appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_protocol, network_bytes, 'http' as network_type, network_direction, network_transport, status, status as req_status, COALESCE(url_domain,'')||':'||COALESCE(url_port::varchar,'')||'//'||url_path||url_query as req_msg, 200 as req_status_code, http_res_status_code, http_res_status from "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' AND uid="+uid+" OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_protocol, network_bytes, network_type, network_direction, network_transport, status, req_status, req_msg, req_status_code, res_status_code, res_status) FORMAT CSV'";
      } else {
        // let dt = new Date(new Date().getTime()-1*60*60*1000).toISOString(); //getting data for last one hour only.
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on, appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_protocol, network_bytes, 'http' as network_type, network_direction, network_transport, status, status as req_status, COALESCE(url_domain,'')||':'||COALESCE(url_port::varchar,'')||'//'||url_path||url_query as req_msg, 200 as req_status_code, http_res_status_code, http_res_status from "+psqlTable+" WHERE uid="+uid+" OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_protocol, network_bytes, network_type, network_direction, network_transport, status, req_status, req_msg, req_status_code, res_status_code, res_status) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = false;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdForNetworkHTTP No Rows Found for User:%s UID: %s", userId, uid);
        } else {
          logger.error("execShCmdForNetworkHTTP - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdForNetworkHTTP Successfully inserted data for Batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdForNetworkHTTP Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdForNetworkICMP(userId, uid, last_received_on){
  try {
    let psqlTable = "network_icmp_v7_"+userId;
    let chTable = "network_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let batchTerminate = false;
    let execContinue = true;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdForNetworkICMP's further execution as batch process exceeded %s Min. Next schedule will continue to process the data. for userId: %s, uid: %s",execShCmdForNetworkHTTP/(60*1000), userId, uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"' AND uid="+uid;
    }
    else 
      query = "select count(*) from "+psqlTable+" WHERE appedo_received_on > '"+curDate+"' AND uid="+uid;
    let result = await psqlAPM.fnDbQuery("execShCmdForNetworkICMP -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdForNetworkICMP No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.info("execShCmdForNetworkICMP processing %s Rows for User:%s UID: %s", rowCnt, userId, uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdForNetworkICMP executing shell for batch: %s, User: %s, UID: %s", batchCnt, userId, uid);
        await executeShell();
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
        batchCnt += 1;
      }
      logger.info("execShCmdForNetworkICMP executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdForNetworkICMP No Rows Found for User:%s UID: %s", userId, uid);
      } else {
        logger.error("execShCmdForNetworkICMP Error Received for User:%s UID: %s ErrMsg: %s", userId, uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(appedo_received_on, created_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, 'icmp' as network_type, network_direction,  status, req_msg, req_code, res_code, res_msg from "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' AND uid="+uid+" OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_type, network_direction, status, req_msg, req_status_code, res_status_code, res_msg) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(appedo_received_on, created_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, 'icmp' as network_type, network_direction,  status, req_msg, req_code, res_code, res_msg from "+psqlTable+" WHERE appedo_received_on > '"+curDate+"' AND uid="+uid+" OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, destination_ip, client_ip, source_ip, event_category, event_duration, network_type, network_direction, status, req_msg, req_status_code, res_status_code, res_msg) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(err).includes("\"code\":108")){
          logger.info("execShCmdForNetworkICMP No Rows Found for User:%s UID: %s", userId, uid);
        } else {
          logger.error("execShCmdForNetworkICMP - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdForNetworkICMP Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdForNetworkICMP Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uid, last_received_on, e.message, e.stack );
  }
}

/** Log Data Migration code covers log_iis_access_v7_, log_iis_error_v7_, log_windows_event_v7_, log_custom_log_v7_, log_syslog_ */
let logPsqlUid = {};
let logChUidLUpd = {};
let logProcessedCnt = 0;
let logUidCnt = 0;
let logUserIdArr = [];
let logArray = {log_iis_access_v7_:"iis_access_v7", log_iis_error_v7_:"iis_error_v7", log_windows_event_v7_:"windows_event_v7", log_custom_log_v7_:"custom_log_v7", log_syslog_:"LINUX_SYSLOG", log_tomcat_access_:"TOMCAT_ACCESS",log_log4j_error_:"LOG4J_ERROR"};
let chProcessedLUpdCnt = 0;

async function getLogUIDsPSQL(){
  try {
    logPsqlUid = {};
    logChUidLUpd = {};
    logProcessedCnt = 0;
    logUidCnt = 0;
    logUserIdArr = [];
    let queryText = "select ser.system_id, ser.system_name, mm.user_id, mm.uid, mm.last_appedo_received_on from module_master mm left join server_information ser ON ser.system_id = mm.system_id where module_code ='LOG' and last_appedo_received_on is not null order by user_id,uid;";
    let result = await psqlAPM.fnDbQuery("getLogUIDsPSQL",queryText,[]);
    if (result.success && result.rowCount > 0){
      logUidCnt = result.rowCount;
      for (let i = 0; i < logUidCnt; i++){
        let row = result.rows[i];
        await getLogTypeTable(row, logUidCnt);
      }
      logUserIdArr = [...new Set(result.rows.map(item => item.user_id))]; //to get the unique user_id
      for (let i = 0; i < logUserIdArr.length; i++){
        let user = logUserIdArr[0];
        await getLogLastUpdatedDateChForUser(user);
      }
      processLog();
    } else {
      if (result.rowCount == 0){
        logger.info("getLogUIDsPSQL - No data found in Module Master.")
      } else {
        logger.error("getLogUIDsPSQL - Failed to get data from module master %s", result.message)
      }
    }
  } catch(e){
    logger.error("getLogUIDsPSQL Exception: errMsg: %s, errStack: %s", e.message, e.stack)
  }
}

async function getLogTypeTable(rowData){
  let query = "SELECT relname from pg_class where relname like ('log\_%\_"+rowData.uid+"') AND relhassubclass";
  let result = await psqlAPM.fnDbQuery("getLogTypeTable", query, []);
  if (result.success && result.rowCount > 0){
    result.rows.map(row => {
      if (logPsqlUid['str'+rowData.user_id] == undefined) logPsqlUid['str'+rowData.user_id] = [];
      logPsqlUid['str'+rowData.user_id].push({uid: rowData.uid, table_name: row.relname, last_appedo_received_on: rowData.last_appedo_received_on, grok_name:logArray[row.relname.replace(rowData.uid,'')], system_name: rowData.system_id+"-"+rowData.system_name});
    });
  }
  logProcessedCnt++;
  // if (logUidCnt == logProcessedCnt ){
  //   processLog();
  // }
}

function processLog(){
  //to ensure last updated date from ch and grok name from psql are received for each of the uid or user id.
  // if (logUidCnt == logProcessedCnt && logUserIdArr.length == chProcessedLUpdCnt){
    processLogExecCmd(logUidCnt,logUserIdArr);
  // }
  logger.info("processLog - logUserIdArr: %s", JSON.stringify(logUserIdArr));
  logger.info("processLog - logChUidLUpd: %s", JSON.stringify(logChUidLUpd));
}

async function getLogLastUpdatedDateChForUser(userId){
  try{
    let tableName = "log_"+userId;
    let queryText = "SELECT uid, psql_tbl_name, log_type, MAX(received_on) as last_received_on FROM "+tableName+" where received_on <= now() GROUP BY uid,psql_tbl_name, log_type ORDER BY uid, psql_tbl_name, log_type"
    let result = await chAPM.chDbQuery("getLogLastUpdatedDateChForUser-LastUpdatedOn",queryText);
    if (result.error){
      if (result.message.includes("404: Code: 60")){
        let tName = "log_"+userId;
        let crQuery = "CREATE TABLE IF NOT EXISTS "+tName+"(uid UInt32, system_name Nullable(String), received_on DateTime, appedo_received_on DateTime, psql_tbl_name String, log_type String, destination_ip Nullable(String), client_ip Nullable(String), source_ip Nullable(String), duration_ms Nullable(UInt32), status_code Nullable(UInt32), status_text Nullable(String), message String, remarks Nullable(String), created_on DateTime DEFAULT now()) ENGINE = MergeTree() PARTITION BY toYYYYMM(appedo_received_on) ORDER BY (log_type, uid)";
        let result1 = await chAPM.chDbQuery("getLastUpdatedDateChForUser--createTable", crQuery);
        if (result1.success){
          if (logChUidLUpd['str'+userId] == undefined) logChUidLUpd['str'+userId] = {};
          logger.info("dataMig.getLogLastUpdatedDateChForUser Created table %s for User: %d in clickhouse", tName, userId);
        } else {
          logger.error("dataMig.getLogLastUpdatedDateChForUser Create Table failed in clickhouse for User: %d, Error: %s", userId, result1.message);
        }
      } else {
        if (result.message.includes("socket hang up")){
          logger.error("dataMig.getLogLastUpdatedDateChForUser-LastUpdatedOn userId- %d, Error: %s", userId, result.message+", hence will be retried after 50 sec" );
          setTimeout(() => {
            getLogLastUpdatedDateChForUser(userId);
          }, 50000);
        } else {
          logger.error("dataMig.getLogLastUpdatedDateChForUser-LastUpdatedOn - %d, Error: %s", userId, result.message+" table Name "+tableName);
        }
      }
    } else {
      if (result.result.length == 0){
        if (logChUidLUpd['str'+userId] == undefined) logChUidLUpd['str'+userId] = {};
      }
      result.result.map(row => {
        if (logChUidLUpd['str'+userId] == undefined) logChUidLUpd['str'+userId] = {};
        logChUidLUpd['str'+userId][row.psql_tbl_name] = row.last_received_on;
      });
    }
  } catch (e){
    logger.error("getLastUpdatedDateChForUser Exception: User: %s, errMsg: %s, errStack: %s", userId, e.message, e.stack);
  }
  chProcessedLUpdCnt++;
}

async function processLogExecCmd(rowCount, userIdArr) {
  try {
    logger.info("processLogExecCmd - Total UID to be processed for Log %d", rowCount);
    for (let i = 0; i<userIdArr.length; i++){
      let user = userIdArr[i];
      let uids = logPsqlUid['str' + user];
      // console.log("user", user, "logChUidLUpd",logChUidLUpd['str' + user]);
      if (uids == undefined) {
        logger.info("processLogExecCmd No uids found for this user %s",user);
      } else {
        for (let k = 0; k < uids.length; k++){
          let uid = uids[k];
          // console.log("UID DETAILS",uid);
          if (logChUidLUpd['str' + user] == undefined) {
            logger.error("processLogExecCmd - User undefined in logChUidLUpd array %s", user);
          }
          else {
            if (logChUidLUpd['str' + user][uid.table_name] != undefined) {
              if (new Date(uid.last_appedo_received_on).getTime() > new Date(logChUidLUpd['str' + user][uid.table_name]).getTime()) {
                if (uid.grok_name == "windows_event_v7"){
                  await execShCmdLogWinEvent(user, uid, logChUidLUpd['str' + user][uid.table_name]);
                } 
                else if (uid.grok_name == "iis_access_v7"){
                  await execShCmdLogIISAccess(user, uid, logChUidLUpd['str' + user][uid.table_name]);
                }
                else if (uid.grok_name == "iis_error_v7"){
                  await execShCmdLogIISError(user, uid, logChUidLUpd['str' + user][uid.table_name]); 
                }
                else if (uid.grok_name == "custom_log_v7"){
                  await execShCmdLogCustom(user, uid, logChUidLUpd['str' + user][uid.table_name]);
                }
                else if (uid.grok_name == "LINUX_SYSLOG"){
                  await execShCmdLogLinuxSyslog(user, uid, logChUidLUpd['str' + user][uid.table_name]);
                }
                else if (uid.grok_name == "TOMCAT_ACCESS"){
                  await execShCmdLogTomcatAccess(user, uid, logChUidLUpd['str' + user][uid.table_name]);
                }
                else if (uid.grok_name == "LOG4J_ERROR"){
                  await execShCmdLogLog4j(user, uid, logChUidLUpd['str' + user][uid.table_name]) 
                }
              }
              else {
                if (new Date(logChUidLUpd['str' + user][uid.table_name]).toString() == "Invalid Date") {
                  if (uid.grok_name == "windows_event_v7"){
                    await execShCmdLogWinEvent(user, uid, null);
                  } 
                  else if (uid.grok_name == "iis_access_v7"){
                    await execShCmdLogIISAccess(user, uid, null);
                  }
                  else if (uid.grok_name == "iis_error_v7"){
                    await execShCmdLogIISError(user, uid, null); 
                  }
                  else if (uid.grok_name == "custom_log_v7"){
                    await execShCmdLogCustom(user, uid, null);
                  }
                  else if (uid.grok_name == "LINUX_SYSLOG"){
                    await execShCmdLogLinuxSyslog(user, uid, null);
                  }
                  else if (uid.grok_name == "TOMCAT_ACCESS"){
                    await execShCmdLogTomcatAccess(user, uid, null);
                  }
                  else if (uid.grok_name == "LOG4J_ERROR"){
                    await execShCmdLogLog4j(user, uid, null) 
                  }
                  logger.info("User: %s, Uid: %s, last received on: %s, is invalid date", user, uid.uid, new Date(uid.last_appedo_received_on));
                }
                else {
                  logger.info("User: %s, Uid: %s, last received on: %s, is upto date with Clickhouse date %s ", user, uid.uid, new Date(uid.last_appedo_received_on), new Date(logChUidLUpd['str' + user][uid.table_name]));
                }
              }
            }
            else {
              if (uid.grok_name == "windows_event_v7"){
                await execShCmdLogWinEvent(user, uid, null);
              } 
              else if (uid.grok_name == "iis_access_v7"){
                await execShCmdLogIISAccess(user, uid, null);
              }
              else if (uid.grok_name == "iis_error_v7"){
                await execShCmdLogIISError(user, uid, null); 
              }
              else if (uid.grok_name == "custom_log_v7"){
                await execShCmdLogCustom(user, uid, null);
              }
              else if (uid.grok_name == "LINUX_SYSLOG"){
                await execShCmdLogLinuxSyslog(user, uid, null);
              }
              else if (uid.grok_name == "TOMCAT_ACCESS"){
                await execShCmdLogTomcatAccess(user, uid, null);
              }
              else if (uid.grok_name == "LOG4J_ERROR"){
                await execShCmdLogLog4j(user, uid, null) 
              }
            }
          }
        }
      }
    }
  } catch(e) {
    logger.error("processNetworkExecCmd exception received Emsg: %s, stack: %s", e.message, e.stack);
  }
}

async function execShCmdLogWinEvent(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_windows_event_v7_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogWinEvent's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",execShCmdForNetworkHTTP/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogWinEvent -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogWinEvent No Rows Found for User:%s UID: %s last_received_on: %s", userId, uidDetails.uid, last_received_on);
      } else {
        logger.info("execShCmdLogWinEvent processing %s Rows for User:%s UID: %s last_received_on: %s",rowCnt, userId, uidDetails.uid, last_received_on);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogWinEvent executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogWinEvent executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.error("Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on, appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message, level, channel, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, status_text, remarks,system_name) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on, appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message, level, channel, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, status_text, remarks,system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogWinEvent No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogWinEvent - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdLogWinEvent Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogWinEvent Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogIISAccess(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_iis_access_v7_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogIISAccess's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s", execShCmdForNetworkHTTP/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogIISAccess -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogIISAccess No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogIISAccess processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogIISAccess executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogIISAccess executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogIISAccess - No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogIISAccess - Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message,duration, response_status_code, source_address, request_method||COALESCE(url_path,'') as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, duration_ms, status_code,client_ip, remarks,system_name) FORMAT CSV'";
      } else {
        // let dt = new Date(new Date().getTime()-1*60*60*1000).toISOString(); //getting data for last one hour only.
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message,duration, response_status_code, source_address, request_method||COALESCE(url_path,'') as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, duration_ms, status_code,client_ip, remarks,system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogIISAccess No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogIISAccess - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdLogIISAccess Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogIISAccess Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogIISError(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_iis_error_v7_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogIISError's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",execShCmdForNetworkHTTP/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogIISError -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogIISError No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogIISError processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogIISError executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogIISError executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogIISError - %s has no Rows for User:%s UID: %s",psqlTable, userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogIISError - Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message,remote_ip, reason_phrase, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, client_ip, status_text, system_name) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message,remote_ip, reason_phrase, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, client_ip, status_text, system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogIISError No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogIISError - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      }
      else {
        logger.info("execShCmdLogIISError Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogIISError Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogCustom(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_custom_log_v7_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogCustom's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogCustom -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogCustom No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogCustom processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogCustom executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogCustom executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogCustom - %s has no Rows for User:%s UID: %s",psqlTable, userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogCustom - Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"', log_message, file_path as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, remarks, system_name) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"', log_message, file_path as remarks,'"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, remarks, system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogCustom No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogCustom - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdLogCustom Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogCustom Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogLinuxSyslog(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_syslog_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogLinuxSyslog's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogLinuxSyslog -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogLinuxSyslog No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogLinuxSyslog processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogLinuxSyslog executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogLinuxSyslog executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogLinuxSyslog %s has no Rows for User:%s UID: %s",psqlTable, userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogLinuxSyslog Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"', message, host, source as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, remarks, system_name) FORMAT CSV'";
      } else {
        // let dt = new Date(new Date().getTime()-1*60*60*1000).toISOString(); //getting data for last one hour only.
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message, host, source as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"'  OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, remarks, system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      // console.log("LOG Query", query);
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogLinuxSyslog - No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogLinuxSyslog - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdLogLinuxSyslog Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogLinuxSyslog Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogTomcatAccess(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_tomcat_access_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogTomcatAccess's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogTomcatAccess -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogTomcatAccess No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogTomcatAccess processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogTomcatAccess executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogTomcatAccess executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogTomcatAccess - %s has no Rows for User:%s UID: %s",psqlTable, userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogTomcatAccess - Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"', message, ip_or_host, resptime_ms, status as status_text, method||path as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, duration_ms, status_text, remarks, system_name) FORMAT CSV'";
      } else {
        query = psqlDMConnStr +"   -c \"copy (SELECT uid,  to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message, ip_or_host, resptime_ms, status as status_text, method||path as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, duration_ms, status_text, remarks, system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      let result = execSync(query);
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(err).includes("\"code\":108")){
          logger.info("execShCmdLogTomcatAccess No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogTomcatAccess - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        let duration = new Date().getTime() - stTime;
        logger.info("execShCmdLogTomcatAccess Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogTomcatAccess Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}

async function execShCmdLogLog4j(userId, uidDetails, last_received_on){
  try {
    let uid = uidDetails.uid;
    let psqlTable = "log_log4j_error_"+uidDetails.uid;
    let chTable = "log_"+userId;
    let query;
    let limit = 100000; // batch limit
    let offset = 0; 
    let rowCnt = 0;
    let batchCnt = 1;
    let execContinue = true;
    let batchTerminate = false;
    let dt = new Date();
    let curDate = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" 00:00:00";
    let timeOut = setTimeout(() => {
      logger.info("Cancelling execShCmdLogLog4j's further execution as batch process exceeded %s Min. Next schedule will continue to process the remaining data for userId: %s, uid: %s",settings.scheduleEvery/(60*1000), userId, uidDetails.uid);
      execContinue = false;
    }, maxRunTimeForBatch);
    //Gets the row count to enable the batch run
    if (last_received_on != undefined){
      let dtLRO = new Date(last_received_on);
      let lro = last_received_on;
      if (dtLRO < new Date(curDate)) lro = curDate;
      query = "select count(*) from "+psqlTable+" where appedo_received_on > '"+lro+"'";
    }
    else 
      query = "select count(*) from "+psqlTable;
    let result = await psqlAPM.fnDbQuery("execShCmdLogLog4j -- count of rows",query, []);
    if (result.success && result.rowCount > 0){
      rowCnt = result.rows[0].count;
      if (rowCnt == 0){
        logger.info("execShCmdLogLog4j No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
      } else {
        logger.info("execShCmdLogLog4j processing %s Rows for User:%s UID: %s",rowCnt, userId, uidDetails.uid);
      }
      while (offset < rowCnt && rowCnt > 0 && !batchTerminate && execContinue){ 
        //Execute only if the row exist and batch is available for execution.
        logger.info("execShCmdLogLog4j executing shell for batch %s User:%s UID: %s", batchCnt, userId, uid);
        await executeShell();
        batchCnt += 1;
        if (rowCnt < limit+offset){
          offset += rowCnt-offset;
        } else {
          offset += limit;
        }
      }
      logger.info("execShCmdLogLog4j executing shell compleated for User: %s, UID: %s in %s batches", userId, uid, batchCnt-1);
    } else {
      if (result.rowCount == 0 ){
        logger.info("execShCmdLogLog4j - %s has no Rows for User:%s UID: %s",psqlTable, userId, uidDetails.uid);
      } else {
        logger.error("execShCmdLogLog4j - Error Received for User:%s UID: %s ErrMsg: %s", userId, uidDetails.uid, result.message )
      }
    }
    clearTimeout(timeOut);
    async function executeShell(){
      let dt = new Date();
      let strDt = dt.getFullYear()+"-"+(dt.getMonth()+1)+"-"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
      if (last_received_on != undefined){
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"', message, host, level as status_text, source as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on > '"+last_received_on+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, status_text, remarks, system_name) FORMAT CSV'";
      } else {
        // let dt = new Date(new Date().getTime()-1*60*60*1000).toISOString(); //getting data for last one hour only.
        query = psqlDMConnStr +"   -c \"copy (SELECT uid, to_char(COALESCE(created_on,appedo_received_on),'yyyy-MM-dd HH24:mi:SS') received_on, to_char(appedo_received_on,'yyyy-MM-dd HH24:mi:SS') appedo_received_on,'"+uidDetails.table_name+"','"+uidDetails.grok_name+"',message, host, level as status_text, source as remarks, '"+uidDetails.system_name+"' FROM "+psqlTable+" WHERE appedo_received_on <= '"+strDt+"' OFFSET "+offset+" LIMIT "+limit+") to STDOUT with CSV DELIMITER ',';\" | "+chDMConnStr+"  --query 'INSERT INTO "+chTable+"(uid, received_on, appedo_received_on, psql_tbl_name, log_type, message, source_ip, status_text, remarks, system_name) FORMAT CSV'";
      }
      let stTime = new Date().getTime();
      // console.log("LOG Query", query);
      let result = execSync(query);
      let duration = new Date().getTime() - stTime;
      if (result.err){
        batchTerminate = true;
        if (JSON.stringify(result.err).includes("\"code\":108")){
          logger.info("execShCmdLogLog4j No Rows Found for User:%s UID: %s", userId, uidDetails.uid);
        } else {
          logger.error("execShCmdLogLog4j - Terminating the job as error Received for User: %s, uid: %s, Err: %s, ErrMsg: %s", userId, uid, JSON.stringify(result.err), JSON.stringify(result.stderr));
        }
      } else {
        logger.info("execShCmdLogLog4j Successfully inserted data for batch: %s, user: %s, uid: %s, datetime: %s in %d ms", batchCnt, userId, uidDetails.uid, last_received_on, duration);
      }
    }
  } catch (e) {
    logger.error("execShCmdLogLog4j Exception: user: %s, uid: %s, Date: %s, eMsg: %s, eStack: %s", userId, uidDetails.uid, last_received_on, e.message, e.stack );
  }
}
