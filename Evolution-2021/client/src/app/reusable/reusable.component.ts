import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../_directives/alert.component';

@Component({
  selector: 'app-reusable',
  templateUrl: './reusable.component.html',
  styleUrls: ['./reusable.component.scss']
})

export class ReusableComponent implements OnInit {

  constructor(
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  public openAlertMsg(msg,type){
    this._snackBar.openFromComponent(AlertComponent,{data: {message:msg, type:type}});
  }

  public getIconName(module: string){
    let iconName: string;
    let clrClass: string;
    switch (module){
      case 'server':
        iconName = 'airplay';
        clrClass = 'apd-blue';
      break;
      case 'application':
        iconName = 'developer_board';
        clrClass = 'apd-brown';
      break;
      case 'database':
        iconName = 'storage';
        clrClass = 'apd-blue';
      break;
      case 'profiler':
        iconName = 'format_textdirection_r_to_l';
        clrClass = 'apd-brown';
      break;
      case 'log':
        iconName = 'format_line_spacing';
        clrClass = 'apd-brown';
      break;
      case 'visualizer':
        iconName = 'pie_chart';
        clrClass = 'apd-brown';
      break;
      default :
        iconName = 'airplay';
        clrClass = 'apd=blue';
    }
    return {iconName: iconName, clrClass:clrClass};
  }
}
