import { Component, OnInit, HostListener,ChangeDetectionStrategy, ElementRef } from '@angular/core';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { environment } from '../../environments/environment';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SidenavComponent implements OnInit {
  opened=true;
  ht; userDet;
  constructor(
    private authService: AuthenticationService,
    private element : ElementRef,
    private reusable: ReusableComponent
  ) { }

  ngOnInit() {
    this.authService.dispOnRequest.next(false);
    this.authService.screenChange.subscribe(res => {
      if (res['height'] == 0){
        this.ht = window.innerHeight-65;
      } else {
        this.ht = res['height'];
      }
    });
    this.authService.toggleMenu.subscribe(toggle => {
      this.opened = toggle;
      this.onToggleScreenSize(this.opened);
    });
    sessionStorage.setItem("currentRoute", 'home');
    this.userDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.ht = event.target.innerHeight - 70;
    let width = event.target.width - 20;
    let element = document.getElementById('sidemain');
    if (element != null){
      let positionInfo = element.getBoundingClientRect();
      let height = positionInfo.height;
      let width = positionInfo.width;
      this.authService.screenChange.next({height:this.ht, width:width});
    } else {
      this.authService.screenChange.next({height:this.ht, width:width});
    }
  }

  downloadAgent(type) {
    let url;
    let selEntObj = JSON.parse(sessionStorage.getItem('entItem'));
    if (!selEntObj.is_owner) {
      this.reusable.openAlertMsg("Only "+selEntObj.e_name+"'s owner can add module.","error");
      return;
    }

    if (type == 'AVM')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&guid=&encryptedUserId='+this.userDet.encryptedUserId+'&location=country##city##';
    else if (type == 'AVM_Node')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&guid=&encryptedUserId='+this.userDet.encryptedUserId+'&location=country##city##';
    else if (type == 'LINUX_MONITOR_AGENT_INSTALLER')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);
    else if (type == 'WINDOWS_MONITOR_AGENT_INSTALLER')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);
    else if (type == 'WINDOWS_AGENT_DOWNLOADER')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);
    else if (type == 'JAVA_AGENT')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);

    if (url != undefined || url.length>0){
      //window.location.href=url;
      this.authService.downloadAgent(url).then(blob =>{
        saveAs(blob, type+'.zip');
      });
    }
      
  }

  onToggleScreenSize(isOpen){
    //The below code is used to change the width when sidemenu is opened or closed, timeout is used to ensure the element reflects the change
    //timeout is used to ensure the element position reflects the changes
    setTimeout(()=>{
      let element = document.getElementById('sidemain');
      if (element != null){
        let positionInfo = element.getBoundingClientRect();
        let width = positionInfo.width;
        this.authService.screenChange.next({height:this.ht, width:width, menuIsOpen:isOpen});
        
      }
    },500);
    // setTimeout(()=>{
    //   let elt = document.getElementById('sidemain').getBoundingClientRect();
    //   let elt1 = document.getElementById('watermark').getBoundingClientRect();
    // },2000)
  }
}
