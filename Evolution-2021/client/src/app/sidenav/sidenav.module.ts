import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SidenavRoutingModule } from './sidenav-routing.module';
import { SidenavComponent } from './sidenav.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderModule } from '../login-header/login-header.module'

@NgModule({
  imports: [
    CommonModule,
    SidenavRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // MatDatetimepickerModule,
    // MatNativeDatetimeModule,
    LoginHeaderModule
  ],
  declarations: [
    SidenavComponent,
  ],
})

export class SidenavModule { }

