import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav.component';
import { AuthGuard } from '../_guards/index';

const routes: Routes = [
  {
    path: '',
    component: SidenavComponent,
    children: [
        {
            path: 'cp', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../change-password/change-password.module').then(m => m.ChangePasswordModule)
        },
        {
            path: 'profile', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule)
        },
        {
            path: 'loadTest', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../load-test/load-test.module').then(m => m.LoadTestModule)
        },
        {
            path: 'enterprise', 
            canActivate:[AuthGuard], 
            loadChildren:() => import('../enterprise/enterprise.module').then(m => m.EnterpriseModule)
        },
        {
            path: 'user', 
            canActivate:[AuthGuard], 
            loadChildren:() => import('../user/user.module').then(m => m.UserModule)
        },
        {
            path: 'alertconfig', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../alert-config/alert-config.module').then(m => m.AlertConfigModule)
        },
        {
            path: 'drilldown', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../drilldown/drilldown.module').then(m => m.DrilldownModule)
        },
        {
            path: 'profiler', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../profiler/profiler.module').then(m => m.ProfilerModule)
        },
        {
            path: 'netstackcharts', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../netstack-charts/netstack-charts.module').then(m => m.NetstackChartsModule)
        },
        {
            path: 'sum', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../sum/sum.module').then(m => m.SumModule)
        },
        {
            path: 'dashboard', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule)
        },
        {
            path: 'systemdetails', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../system-details/system-details.module').then(m => m.SystemDetailsModule)
        },
        {
            path: 'moduledetails', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../module-details/module-details.module').then(m => m.ModuleDetailsModule)
        },
        {
            path: 'logmodule', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../log-module/log-module.module').then(m => m.LogModuleModule)
        },
        {
            path: 'extmonitors', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../external-monitors/external-monitors.module').then(m => m.ExternalMonitorsModule)
        },
        {
            path: 'visualizer', 
            canActivate:[AuthGuard], 
            loadChildren: () => import('../visualizer/visualizer.module').then(m => m.VisualizerModule)
        },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SidenavRoutingModule { }

