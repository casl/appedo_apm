import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SystemDetailsRoutingModule } from './system-details-routing.module';
import { SystemDetailsComponent,systemDetailsDialog } from './system-details.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';

@NgModule({
  imports: [
    CommonModule,
    SystemDetailsRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
  ],
  declarations: [
    SystemDetailsComponent,
    systemDetailsDialog,
  ],
  entryComponents: [
    systemDetailsDialog,
  ],
})

export class SystemDetailsModule { }

