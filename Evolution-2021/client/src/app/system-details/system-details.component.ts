import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatPaginator } from '@angular/material/paginator';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, forkJoin } from 'rxjs';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

export interface DialogData {
  system_id: number;
  system_name: string;
  manufacturer: string;
}

@Component({
  selector: 'app-system-details',
  templateUrl: './system-details.component.html',
  styleUrls: ['./system-details.component.css']
})

export class SystemDetailsComponent implements OnInit {
  cardTitle: string;
  isLoading: boolean;
  offset = 0; 
  limit = 20;
  isFetched = false;
  lastScrolled = 0;
  scrollDirection ={eg:'down', umg: 'down'};
  scrollOffset = 1000;
  @ViewChild(CdkScrollable, { static: true }) virtualScroll: CdkScrollable;
  setIntervalId;
  systemDetailsColl = new MatTableDataSource([]);
  systemSubTotal = []; totalAgents = 0;
  selectedRow; 
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  handleScroll = (scrolled: boolean) => {
    if (scrolled) {
      this.offset += this.limit; 
      this.getSystemCard(this.offset, this.limit);
    }
  }
  obsEnt: any;
  theme: any;
  screenParam: any;
  prevEid:number;
  setIntervalHandleAlert;

  hasMore(){
    return this.isFetched;
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog
  ) { }


  ngOnInit() {
    sessionStorage.setItem("currentRoute", "/home/systemdetails");
    this.getSystemCard();
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      if (data && entItem != null && this.prevEid != entItem.e_id){
        this.getSystemCard();
      }
    });
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    });
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);  
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    clearInterval(this.setIntervalHandleAlert);
    if (this.obsEnt != undefined)
    this.obsEnt.unsubscribe();
  }

  gotoLogModule(modType, row){
    let setData = {moduleType:modType, selRow: row};
    sessionStorage.setItem("logmodule",JSON.stringify(setData));
    this.router.navigate(['home/logmodule']);
  }

  moduleDetails(modType,row){
    let setData = {moduleType:modType, selRow: row};
    sessionStorage.setItem("moduleDetails",JSON.stringify(setData));
    this.router.navigate(['home/moduledetails']);
  }  

  openDialog(): void {
    clearInterval(this.setIntervalHandleAlert);
    const dialogRef = this.dialog.open(systemDetailsDialog, {
      width: '40%',
      data: {system_id: this.selectedRow.system_id, system_name: this.selectedRow.system_name, manufacturer: this.selectedRow.owner}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined){
        this.saveModifiedRow(result);
      }
    });

  }

  async saveModifiedRow(data){
    let result = await this.authService.updateSystemInfo(data);
    if (result.success){
      this.startAlertTimer();
      this.reusable.openAlertMsg(result.message,'info');
      let row = this.systemDetailsColl.data.find(x => x.system_id == data.system_id);
      row.system_name = data.system_name;
      row.owner = data.manufacturer;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  editSystem(row){
    this.selectedRow = row;
    this.openDialog();
  }

  getSubTotal(){
    let transactions = this.systemDetailsColl.data;
    this.totalAgents = 0;
    this.subTotalColumns.map(column => {
      this.systemSubTotal[column] = 0;
      transactions.map(t =>{
        if (t[column] != undefined){
          this.systemSubTotal[column] += Number(t[column]);
          this.totalAgents += Number(t[column]);
        }
      })
      if (this.systemSubTotal[column] == 0) this.systemSubTotal[column] = undefined;
    });
  }
  displayedColumns=['edit','system_name','owner', 'server','application','database','log','network', 'netstack','profiler','alerts','created_on'];
  subTotalColumns = ['server','application','database','log', 'netstack','network','profiler'];
  
  async getSystemCard(offset?:number, limit?:number) {
    this.cardTitle = '';
    sessionStorage.setItem("allModCardVisitedModule","system");
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 20;
    this.isLoading = true;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    this.prevEid = entItem == undefined ? 0 : entItem.e_id;
    let entName = entItem != undefined && entItem.owner_id != null ? ' for enterprise "'+entItem.e_name+'"':''
    this.cardTitle = "Server/System Information" +entName;
    let userData = {
      entUserId: entItem != undefined ? entItem.owner_id : null, 
      e_id: entItem != undefined ? entItem.e_id : null,
      offset: offset,
      limit: limit
    };
    let result = await this.authService.getSystemContent(userData);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      this.isFetched = true;
      if (this.systemDetailsColl.data.length == 0 || reset){
        this.systemDetailsColl = new MatTableDataSource(result.result);
      } else {
        let newData = this.systemDetailsColl.data.concat(result.result);
        this.systemDetailsColl = new MatTableDataSource(newData);
      }
      this.systemDetailsColl.sort = this.sort;
      this.getAllModCodeForSystem(result.result);
      this.getAlerts(userData);
      this.startAlertTimer();
    } else if (result.rowCount == 0){
      if (reset){
        this.systemDetailsColl = new MatTableDataSource([]);
        this.getSubTotal();
      }
      this.isFetched = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  private startAlertTimer() {
    this.setIntervalHandleAlert = setInterval(() => {
      let entItem = JSON.parse(sessionStorage.getItem("entItem"));
      let userData = {
        entUserId: entItem != undefined ? entItem.owner_id : null,
        e_id: entItem != undefined ? entItem.e_id : null,
        offset: this.offset,
        limit: this.limit
      };
      this.getAlerts(userData);
    }, 60000);
  }

  async getAlerts(param){
    let alertsAPM = this.authService.getAPMAlerts(param);
    let alertsLog = this.authService.getLogAlerts(param);
    forkJoin([alertsLog, alertsAPM]).subscribe(results =>{
      let alerts;
      if (results[0].success) alerts = results[0].result;
      if (results[1].success && alerts != undefined) {
          results[1].result.map(row => {
            let findRow = alerts.find(x => x.system_id == row.system_id && x.breached_severity == row.breached_severity);
            if (findRow != undefined){
              findRow.count = Number(findRow.count)+Number(row.count);
            }
          });
      } else if (results[1].success){
        alerts = results[1].result;
        alerts.map(alert => {
          let row = this.systemDetailsColl.data.find(x => x.system_id == alert.system_id);
          if (row.alerts == undefined) row['alerts'] = {};
          if (alert.breached_severity == 'critical') {
            row['alerts']['critical'] = alert.count;
          } else {
            row['alerts']['warning'] = alert.count;
          }
        });
      }
    });
  }
  async getModCodeCount(data, row){
    let result = await this.authService.getModCodeCount(data);
    if (result.success){
      result.result.map(ele => {
        row[ele.module_code] = ele.count;
        this.setLastReceivedOn(ele,row);
      });
    } else {
      this.authService.invalidSession(result);
    }
    this.getSubTotal();
  }

  setLastReceivedOn(ele, row){
    let arrCnt = 0;
    if (row.status == undefined){
      row['status'] = {};
    }
    ele.last_received_on.map(arrEle => {
      if (arrEle != null){
        let received_on = new Date(arrEle).getTime();
        if (received_on <= (new Date().getTime()-300000)){
          arrCnt++;
        }
      } else {
        arrCnt++;
      }
    });
    if (arrCnt==0) {
      row['status'][ele.module_code] = "Running";
    } else if (arrCnt == ele.last_received_on.length){
      row['status'][ele.module_code] = "Down";
    } else {
      row['status'][ele.module_code] = "Partial";
    }
  }

  async getAllModCodeForSystem(rows){
    rows.map((mmCard, idx) => {
      let systemData = {systemId: mmCard.system_id};
      this.getModCodeCount(systemData, mmCard);
    });
  }
}

/* Dialog screen ts file for system-details-dialog.html */
@Component({
  selector: 'system-details-dialog',
  templateUrl: 'system-details-dialog.html',
})
export class systemDetailsDialog {

  btnDisable = false;
  constructor(
    public dialogRef: MatDialogRef<systemDetailsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  validateStringEmpty(){
    if (this.data.system_name == undefined || this.data.system_name.trim() == '' || this.data.manufacturer == undefined || this.data.manufacturer.trim() == '' ){
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }
  }  
}