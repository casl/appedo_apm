import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemDetailsComponent } from './system-details.component';

const routes: Routes = [
  {
    path: '',
    component: SystemDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SystemDetailsRoutingModule { }

