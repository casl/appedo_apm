import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  isLoading = false;
  userId;  firstName; lastName;  emailId;  mobileNo ='+'; password
  licenseType;  dataSize; dataRetentionDays; checkEmail = true;
  returnUrl;
  form: FormGroup;

  modPassword;
	createForm() {
		this.form = this.formBuilder.group({
			email:[this.emailId,Validators.compose([
				Validators.required,
        Validators.email
			])],
			fname:[this.firstName,Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(50)
			])],
			lname:[this.lastName,Validators.compose([
				Validators.required,
				Validators.minLength(1),
				Validators.maxLength(50)
			])],
			mobile_no:[this.mobileNo,Validators.compose([
				Validators.minLength(13),
				Validators.maxLength(15),
				this.validateMobile
			  ])]
		});	
	}

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private formBuilder: FormBuilder) {
      this.createForm();
    }

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='email') {msg += control.hasError('email') ? 'Not a valid email' :''}
    if (controlName =='fname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 3 & 50 char length': ''}
    if (controlName =='lname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 1 & 50 char length': ''}
    if (controlName =='mobile_no') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 13 & 15 length includes country code': ''}
    if (controlName =='mobile_no') {msg += !control.value.startsWith('+') ? 'Must start with +': ''}
    if (controlName =='mobile_no') {msg += control.hasError('validateMobile') ? 'Only Numeric Fields Allowed' :''}
    return msg;
  }

  ngOnInit() {
    this.authService.dispOnRequest.next(false);
    this.returnUrl = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/profile");
    this.getProfile();
  }

  async getProfile(){
    let profile = await this.authService.getProfile();
      if (profile.success){
        let user = this.authService.decrypt(profile.result);
        // this.authService.decrypt(profile.result).then(user => {
          let qryData = JSON.parse(user);
          this.form.setValue({
            fname: qryData.first_name,
            lname: qryData.last_name,
            mobile_no: qryData.mobile_no !=null && qryData.mobile_no !="" ? qryData.mobile_no :"+",
            email: qryData.email_id,
          });
          this.userId = qryData.user_id;
          this.emailId = qryData.email_id;
        // });
      }
      else {
        this.reusable.openAlertMsg(this.authService.invalidSession(profile),"error");
      }
  }

  validateMobile(controls){
		const reqExp = new RegExp(/^[+][0-9]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateMobile' : true};
		}
  }
  
  onClickCancel(){
    if (this.returnUrl != undefined && this.returnUrl != '/home/profile')
      this.router.navigate([this.returnUrl]);
    else
      this.router.navigate(['/home']);
  }

  checkEmailAvl(){
    if (!this.form.get("email").valid){
      this.checkEmail = false;
      return;
    }
    if (this.emailId == this.form.get("email").value) {
      this.checkEmail = true;
      return;
    }

    this.isLoading = true;  
    let emailData = {email: this.form.get("email").value};
    this.authService.checkEmailAvl(emailData).subscribe( result => {
      this.isLoading = false;
      if (result.success) this.checkEmail = true;
      else {
        this.checkEmail = false;
        this.reusable.openAlertMsg("Email already Exists", "info");
      }
    });
  }

  profileUpdate() {
    if (this.form.valid && this.checkEmail){
      this.isLoading = true;
      const profile = {
        user_id: this.userId,
        first_name: this.form.get('fname').value,
        last_name: this.form.get('lname').value,
        mobile_no: this.form.get('mobile_no').value,
        email_id: this.form.get('email').value
      };
      const prof = {data:this.authService.encrypt(JSON.stringify(profile))};
      this.authService.profileUpdate(prof).subscribe(data => {
        this.isLoading = false;
        if (data.success){
            this.reusable.openAlertMsg("Profile updated successfully","info");
            if (this.returnUrl != undefined && this.returnUrl != 'profile')
              this.router.navigate([this.returnUrl]);
            else
              this.router.navigate(['home']);
          } else {
            this.reusable.openAlertMsg(data.message,"error");
          }
        },
        error => {
          this.isLoading = false;
          this.reusable.openAlertMsg(error.message,"error");
        }
      );
    } else {
      this.reusable.openAlertMsg("Form is not valid, please check the error messages","error");
    }
  }
}
