import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { LoadTestRoutingModule } from './load-test-routing.module';
import { LoadTestComponent, configureSettingDialog, loadTestRunningDialog, loadTestReports } from './load-test.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';

@NgModule({
  imports: [
    CommonModule,
    LoadTestRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
  ],
  declarations: [
    LoadTestComponent,
    configureSettingDialog,
    loadTestRunningDialog,
    loadTestReports
  ],
  entryComponents: [
    configureSettingDialog,
    loadTestRunningDialog,
    loadTestReports
  ],
})

export class LoadTestModule { }

