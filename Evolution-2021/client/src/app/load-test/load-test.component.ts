import { Component, OnInit, Inject, Renderer2, ViewChild,ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReusableComponent } from '../reusable/reusable.component';
import { Observable, Unsubscribable, timer } from 'rxjs';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { throttle as _throttle, noop as _noop } from 'lodash';
import { filter } from 'rxjs/operators';

export interface DialogData {
  uid: string,
  modType: string,
  modName: string,
  modCode: string
}

@Component({
  selector: 'app-load-test',
  templateUrl: './load-test.component.html',
  styleUrls: ['./load-test.component.scss']
})

export class LoadTestComponent implements OnInit {
  isLoading: boolean;
  ltScenRunDet = [];
  selectedIdx: number;
  LtScriptsCollDispColumn = ['script_name', 'count','mapped_scenarios'];
  LtScenariosCollDispColumn = ['delete', 'Configure', 'scenario_name','count','mapped_scripts', 'chart','modified_on','last_run', 'active'];
  
  obsEnt: any;
  entItem: any;
  configParam;
  screenParam: any;
  theme: string;

  ltScenCard = new MatTableDataSource([]); 
  ltScriptCard = new MatTableDataSource([]); 
  //scroll variables
  offset = {script:0, scen:0}; //eg = enterprise grid, umg - user map grid
  limit = {script:20, scen:20};
  lastScrolled = {script:0, scen:0};
  isFetched = {script:false, scen:false};
  scrollDirection ={script:'down', scen: 'down'};
  scrollOffset = 1000;
  setIntervalId;

  @ViewChild(MatSort) scriptSort: MatSort;
  @ViewChild(MatSort) scenSort: MatSort;
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;

  handleScroll = (scrolled: boolean, origin) => {
    if (scrolled) {
      if(origin=="script") this.offset['script'] += this.limit['script']; 
      if(origin=="scen") this.offset['scen'] += this.limit['scen']; 
    }
    scrolled && origin=='script' && this.isFetched['script']? this.getLTScriptCard('JMETER', this.offset['script'], this.limit['script']) : _noop();
    scrolled && origin=='scen' && this.isFetched['scen']? this.getLTScenCard('JMETER', this.offset['scen'], this.limit['scen']) : _noop();
  }

  hasMore(origin){
    return this.isFetched[origin];
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    public dialog: MatDialog,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    sessionStorage.setItem("currentRoute","/home/loadTest");
    this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let curIndex = sessionStorage.getItem("extCurIndex");
    this.setIndex(curIndex);
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
        let curIndex = sessionStorage.getItem("extCurIndex");
        this.ltScriptCard = new MatTableDataSource([]);
        this.ltScenCard = new MatTableDataSource([]);
        this.setIndex(curIndex);
      }
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    });
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      let id = ele['elementRef']['nativeElement'].id;
      // console.log("scrolldispatch", id, this.hasMore(id));
      if(this.hasMore(id))
        this.handleScroll(true, id);
    });
    this.setIntervalId = setInterval(()=>{
      this.cd.detectChanges();
    }, 2000);
    // this.cd.detectChanges();
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.obsEnt != undefined)
      this.obsEnt.unsubscribe();
  }

  private setIndex(curIndex: string) {
    if (curIndex != undefined) {
      if (Number(curIndex) == 0) {
        this.onTabChange(0);
        this.selectedIdx = 0;
      }
      else if (Number(curIndex) == 1) {
        this.onTabChange(1);
        this.selectedIdx = 1;
      }
    }
    else {
      this.onTabChange(0);
    }
    // this.cd.detectChanges();
  }

  onTabChange(tabIdx){
    if (tabIdx == 0){
      sessionStorage.setItem("extCurIndex","0");
      this.getLTScriptCard('JMETER');
    } else if (tabIdx == 1){
      sessionStorage.setItem("extCurIndex","1");
      this.getLTScenCard('JMETER');
    }
  }

  async getLTScriptCard(type,offset?:number, limit?:number){
    this.isLoading = true;
    // this.cd.detectChanges();
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['script'] = 0; reset = true; } 
    if (limit == undefined) limit = 20;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let entName = entItem.owner_id != null ? ' for enterprise "'+entItem.e_name+'"':''
    let param = {
        entId: entItem == undefined || entItem.owner_id == undefined ? null : entItem.e_id,
        type: type,
        offset: offset,
        limit: limit
    };
    let result = await this.authService.getLTScriptCard(param).toPromise();
    this.isLoading = false;
    if (result.success){
      this.isFetched['script'] = true;
      if (this.ltScriptCard.data.length == 0 || reset){
        this.ltScriptCard = new MatTableDataSource(result.result);
      } else {
        let newData = this.ltScriptCard.data.concat(result.result);
        this.ltScriptCard = new MatTableDataSource(newData);
      }
      // console.log(this.ltScriptCard.data);
      this.ltScriptCard.sort = this.scriptSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.ltScriptCard = new MatTableDataSource([]);
      }
      this.isFetched['script'] = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  /* LT Module Code starts*/
  async getLTScenCard(type, offset?:number, limit?:number){
    this.isLoading = true;
    // this.cd.detectChanges();
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['scen'] = 0; reset = true; } 
    if (limit == undefined) limit = 20;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let entName = entItem.owner_id != null ? ' for enterprise "'+entItem.e_name+'"':''
    let param = {
        entId: entItem == undefined || entItem.owner_id == undefined ? null : entItem.e_id,
        type: type,
        offset: offset,
        limit: limit
    };
    let result = await this.authService.getLTScenCard(param).toPromise();
    this.isLoading = false;
    // console.log(result);
    if (result.success){
      this.isFetched['scen'] = true;
      if (this.ltScenCard.data.length == 0 || reset){
        this.ltScenCard = new MatTableDataSource(result.result);
        this.ltScenCard.data.map((ele,idx) =>{
          this.ltScenRunDet[idx] = [];
          this.getLTRunDetails(ele, idx);
        });
      } else {
        let newData = result.result;
        newData.map((ele,idx) =>{
          this.ltScenRunDet[idx] = [];
          this.getLTRunDetails(ele, idx);
        });
        newData = this.ltScenCard.data.concat(newData);
        this.ltScenCard = new MatTableDataSource(newData);
      }
      this.ltScenCard.sort = this.scenSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.ltScenCard = new MatTableDataSource([]);
      }
      this.isFetched['scen'] = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  async getLTRunDetails(ele,idx){
    let param = {
      scenario_id : ele.scenario_id
    };
    let result = await this.authService.getScenRunDetails(param).toPromise();
    if (result.success){
      this.ltScenRunDet[idx] = result.result;
      ele.active = result.result[0].active;
      ele.run_id = result.result[0].last_run_id;
      ele.status = result.result[0].status;
      ele.last_run = result.result[0].last_run;
      this.ltScenRunDet[idx][0].count = result.resultScenRunCount[0].count;
    } else if (result.message) {
      this.ltScenRunDet[idx].push({count:'No Run', last_report_id:'Not Available', last_run:'Not Available'});
    } else {
      this.ltScenRunDet[idx].push({count:'No Run', last_report_id:'Not Available', last_run:'Not Available'});
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async deleteLtScen(ele){
    let confirmMessage = confirm("Please confirm the deletion of LT Module - "+ele.scenario_name+" ("+ele.scenario_id+")");
    if(confirmMessage){
      this.isLoading = true;
      // this.cd.detectChanges();
      let param = {
        scenarioId: ele.scenario_id,
        scenarioName: ele.scenario_name,
        testTypeScript: ele.scenario_type,
        mappedScripts: ele.mapped_scripts != null ? ele.mapped_scripts : '',
        count: ele.count
      }
      let result = await this.authService.deleteJmeterScenarios(param);
      if (result.success){
        this.isLoading = false;
        this.reusable.openAlertMsg(result.message, "info");
      } else {
        this.isLoading = false;
        this.authService.invalidSession(result);
      }
      this.getLTScenCard('JMETER');
    } else {
      this.reusable.openAlertMsg("Delete operation cancelled by User.", "info");
    }
    // this.cd.detectChanges();
  };

  RunLTScenario(ltData, ltOperation){
    if(ltData.active && ltData.count > 0){
      let operationName = ltData.scenario_type == 'APPEDO_LT' ? 'LTLiveReport' : 'JmeterLiveReport';
      sessionStorage.setItem("ltLocData",JSON.stringify(ltData));
      sessionStorage.setItem("ltOperation", operationName);
      this.router.navigate(['home/loadTest/liveReports']);
      this.authService.toggleMenu.next(false);
    } else {
      this.openLTRunConfigDialog(ltData);
      sessionStorage.setItem("ltOperation", ltOperation);
    }
    // this.cd.detectChanges();
  }


  openConfigSetting(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.openConfigDialog(row);
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can change the configurations","info");
    }
    // this.cd.detectChanges();
  }

  openConfigDialog(row): void {
    const dialogRef = this.dialog.open(configureSettingDialog, {
      width: '50%',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    // this.cd.detectChanges();
  }

  openLTRunConfigDialog(row): void {
    const dialogRef = this.dialog.open(loadTestRunningDialog, {
      width: '50%',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
    });
    // this.cd.detectChanges();
  }

  drawLTChart (ltData, index, ltOperation) {
    if (this.ltScenRunDet[index][0].count == 'No Run') {
      this.reusable.openAlertMsg("No Run Executed.","info");
    }else {
      sessionStorage.setItem("ltLocData",JSON.stringify(ltData));
      sessionStorage.setItem("ltOperation", ltOperation);
      this.router.navigate(['home/loadTest/reports']);
      this.authService.toggleMenu.next(false);
    }   
    // this.cd.detectChanges();
  }

  async uploadJmeter(event) {
    this.isLoading = true;
    // this.cd.detectChanges();
    let files: FileList = event.target.files;
    let formData = new FormData(); 
    if(files.length > 0) {
      for(var i= 0; i < files.length; i++){
        formData.append('file_'+i, files[i], files[i].name); 
      }
      let filecount = files.length.toString();
      formData.append('upload_file_count', filecount);
      formData.append('ent_id', this.entItem.e_id);
    }

    let data = await this.authService.uploadJmeterScripts(formData).toPromise();
    this.isLoading = false;
    if (data.success) {
      this.reusable.openAlertMsg(data.message, "info");
      this.getLTScriptCard('JMETER');
    } else {
      this.reusable.openAlertMsg(data.message, "error");
    }
    // this.cd.detectChanges();
  }
}

//Load Test All Reports Details compontent controller.
@Component({
  selector: 'load-test-reports',
  templateUrl: 'load-test-reports.html',
  styleUrls: ['./load-test.component.scss']
})
export class loadTestReports implements OnInit {

  private timerSubscription: Unsubscribable;
  titleIcon; title; isLoading; ltData; ltOperation;
  ltRunData; selRunData; AutoStartTime; ltLiveRunData;
  ltSelRunData = []; saveButtonTitle;
  ltScenariosReportColumn = ['Report ID', 'Report Name', 'Start Time', 'End Time', 'Run Duration'];
  ltScenariosLiveReportColumn = ['Report ID', 'Report Name', 'Run Start Time', 'Elapsed Time'];
  
  constructor(
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private router: Router,
    public dialog: MatDialog,
  ) {}

  ngOnInit(){
    sessionStorage.getItem("ltLocData");
    this.titleIcon = "view_agenda";
    let ltLocData = sessionStorage.getItem('ltLocData');
    this.ltOperation = sessionStorage.getItem('ltOperation');
    
    if (ltLocData == undefined) {
      this.isLoading = false;
      this.router.navigate(['home/loadTest/']);
    }else{
      this.ltData = JSON.parse(ltLocData);
      this.title = "Load Test :: "+this.ltData.scenario_name;
      if(this.ltOperation == 'JmeterLiveReport' || this.ltOperation == 'LTLiveReport'){
        this.saveButtonTitle = 'View Live Report';
        this.ltOperation = sessionStorage.getItem('ltOperation');
        this.getLoadTestLiveReport(this.ltData);
      }else{
        this.saveButtonTitle = 'Click here to view report';
        this.getAllJMeterRunDetail(this.ltData);
      }

    }
  }

  getAllJMeterRunDetail(ltData) {
    this.isLoading = true;
    let qryData = {scenario_id: ltData.scenario_id};
    this.authService.getAllJMeterRunDetail(qryData).subscribe( result => {
      this.isLoading = false;
      if(result.success){
        this.ltRunData = result.result;
        if (this.ltRunData.length > 0)
          this.onLoadReport(this.ltRunData[0]);
        else {
          this.reusable.openAlertMsg("No Run executed.","info");
          this.goBack();
        }
      } else {
        this.authService.invalidSession(result);
        this.goBack();
      }
    }, error => {
      this.isLoading = false;
      this.authService.invalidSession(error);
      this.goBack();
    });
  }
  
  onLoadReport(selRunData) {
    this.ltSelRunData = [];
    this.ltSelRunData.push(selRunData);
    this.selRunData = selRunData;
    if(this.ltOperation == 'JmeterLiveReport' || this.ltOperation == 'LTLiveReport'){
      this.AutoStartTime = this.selRunData.diff_sec;
      this.startTime();
    }
    //go to LT status page
    if(this.ltData.scenario_type == 'APPEDO_LT'){
      console.log('go to LT status page implementation');
      //this.getScenarioStatusDetails();
    }else if (!this.selRunData.is_url_exist) {
      this.reusable.openAlertMsg("Execution failed/not completed, hence no report found.","error");
    }
  }

  startTime(){
    if( this.timerSubscription != undefined){
      this.timerSubscription.unsubscribe();
    }
    this.timerSubscription = timer(1000,1000).subscribe(()=>this.incrTimer());
  }

  incrTimer(){
    this.AutoStartTime++;
  }

  getLoadTestLiveReport(ltData){
    this.isLoading = true;
    let qryData = {
      scenario_type: ltData.scenario_type,
      run_id: ltData.run_id
    };
    this.authService.getLTLiveRunDetail(qryData).subscribe( result => {
      this.isLoading = false;
      if(result.success){
        this.ltLiveRunData = result.result;
        if (this.ltLiveRunData.length > 0){
          //this.selRunData = this.ltLiveRunData[0];
          this.onLoadReport(this.ltLiveRunData[0]);
        }else {
          this.reusable.openAlertMsg("No Run executed.","info");
          this.goBack();
        }
      }else {
        this.authService.invalidSession(result);
        this.goBack();
      }
    }, error => {
      this.isLoading = false;
      this.authService.invalidSession(error);
      this.goBack();
    });
  }

  openReport() {
    if (this.selRunData.is_url_exist) {
      //this.selRunData.grafana_report_url = this.sanitizer.bypassSecurityTrustResourceUrl(selRunData.grafana_report_url);
      window.open(this.selRunData.grafana_report_url, '_blank');
    } else {
      this.reusable.openAlertMsg("Execution failed/not completed, hence no report found.","error");
    }
  }

  stopJMeterTest(){
    this.isLoading = true;
    let qryData = {
      runId: this.ltData.run_id
    };
    this.authService.stopJMeterTest(qryData).subscribe( result => {
      if(result.success){
        this.isLoading = false;
        this.reusable.openAlertMsg(result.message,"info");
      }else{
        this.reusable.openAlertMsg(result.message,"error");
        this.isLoading = false;
      }
    });
  }

  convertSecToTime(intSec){
    return this.authService.convertSecToTime(intSec);
  }

  goBack(){
    this.router.navigate(['home/loadTest']);
  }
}

@Component({
  selector: 'load-test-running-dialog',
  templateUrl: 'load-test-run-dialog.html',
  styleUrls: ['./load-test.component.scss']
})
export class loadTestRunningDialog implements OnInit {
  
  configTitle; JmeterRegionData; JmeterScenarioData; isLoading; 
  isShowRegionData; ReportName; ltData; selectedUserAgent;

  constructor(
    public dialogRef: MatDialogRef<loadTestRunningDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService : AuthenticationService,
    private reusable: ReusableComponent
  ) {}

  ngOnInit(){
    this.configTitle = 'Assign Load Generator';
    this.ltData = this.data;
    this.agentLoadGenrator(this.data);
  }

  onClose(): void {
    this.dialogRef.close();
  }

  agentLoadGenrator(ltData) {
    this.isLoading = true;
    let qryData = {scenarioName: ltData.scenario_name, testTypeScript: ltData.scenario_type, licenseLevel: 'level3'};
    this.authService.getRegionDetails(qryData).subscribe( result => {
      if(result.success){
        this.JmeterRegionData = result.message.regionData;
        this.JmeterScenarioData = result.message.scenarioData;
        this.isLoading = false;
        this.isShowRegionData = true;
        let dd = new Date();
        this.ReportName = 'Rpt_'+ltData.scenario_name+'_'+dd.getFullYear().toString()+(dd.getMonth()+1).toString()+dd.getDate().toString()+'_'+dd.getHours().toString()+dd.getMinutes().toString()+dd.getSeconds().toString();
      }
    });
  }

  runScenario(){
    if(this.ReportName == undefined){
      this.reusable.openAlertMsg("ReportName is mandatory.","info");
    }else{
      let totalUser = 0;
      let regions = [];
      let distributions = [];
      let distribution = 0;
      let isVaildFrom = true;
      for(let i = 0; i<this.JmeterScenarioData.length; i++ ){
        totalUser = totalUser + this.JmeterScenarioData[i].userCount;
      }

      for(let region of this.JmeterRegionData){
        if(region.showRegion){
          distribution = distribution + region.distribution;
          let preIndex = region.region.indexOf('(')+1;
          let postIndex = region.region.indexOf(')');
          let regionName = region.region.substring(preIndex, postIndex);
          if(region.distribution >0){
            distributions.push(region.distribution);
            regions.push(regionName);
          }else{
            this.reusable.openAlertMsg("Distribution value shoule be greater than 0 for "+ regionName,"info");
            isVaildFrom = false;  
          }
        }
      }

      let params = {
        distributions : distributions.join(),
        endMonitor: 0,
        guids: '',
        maxUserCount: this.ltData.scenario_type == 'APPEDO_LT' ? this.ltData.v_users : totalUser,
        regions: regions.join(),
        reportName: this.ReportName,
        runType: this.ltData.runtype,
        scenarioId: this.ltData.scenario_id,
        scenarioName: this.ltData.scenario_name,
        startMonitor: 0,
        testType: this.ltData.scenario_type,
        userAgent: this.selectedUserAgent != undefined ? this.selectedUserAgent.user_agent_value : 'Recorded Agent',
        licenseLevel: 'level3'
      }
      
      if(isVaildFrom){
        if(regions.length == 0){
          this.reusable.openAlertMsg("Select atleast one Region.","info");
        }else if(distribution < 100 || distribution > 100) {
          this.reusable.openAlertMsg("Sum of distribution is 100% only.","info");
        }else if(this.ltData.maxusers < regions.length) {
          this.reusable.openAlertMsg("Selected region should not be greater than max user count.","info");
        }else{
          this.isLoading = true;
          this.authService.runScenario(params).subscribe( res => {
            this.isLoading = false;
            if(res.success){
              this.reusable.openAlertMsg(res.message,"info");
              this.isLoading = false;
              this.onClose();
            }else{
              this.reusable.openAlertMsg(res.message,"error");  
              this.authService.invalidSession(res);
            }
          },()=>{
            this.reusable.openAlertMsg("could not connect to server.","error");
          });
        }
      }
    } 
  }

}

/* Dialog screen ts file for configure settings html*/
@Component({
  selector: 'configure-setting-dialog',
  templateUrl: 'configure-setting-dialog.html',
  styleUrls: ['./load-test.component.scss']
})
export class configureSettingDialog implements OnInit {

  JMeterScriptData; totalUsers; jmeterErrMsg=[]; jmeterScnName;
  jMeterScrData = {}; scenarioName; configTitle; canConfigScenario;
  isLoading;
  validForm = true;

  constructor(
    public dialogRef: MatDialogRef<configureSettingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService : AuthenticationService,
    private reusable: ReusableComponent,
    private rend: Renderer2,
  ) {}

  ngOnInit(){
    this.configTitle = 'Configure Scenario | Name the Scenario and Map VUser Scripts ';
    this.JMeterScriptData = this.data;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.getScriptSettings();
    }
  }

  async getScriptSettings(){
    this.jmeterErrMsg = [];
    //this.JMeterScriptData = ele;
    this.isLoading = true;
    let param = {
      scenarioId: this.JMeterScriptData.scenario_id,
      scenarioName: this.JMeterScriptData.scenario_name,
      testTypeScript: this.JMeterScriptData.scenario_type
    }
    let result = await this.authService.getScriptSettings(param);
    this.totalUsers = 0;
    if (result.success){
      this.jmeterScnName = this.JMeterScriptData.scenario_name;
      this.jMeterScrData = JSON.parse(result.result);
      for(var key in this.jMeterScrData){
        this.scenarioName = key;
        this.jMeterScrData[key]['rampTimeFormat'] = this.convertSecToTime(this.jMeterScrData[key].rampUpTime);
        this.jMeterScrData[key]['durationTimeFormat'] = this.convertSecToTime(this.jMeterScrData[key].duration);
        this.jMeterScrData[key].loop = (this.jMeterScrData[key].loop == -1 ? 0 : this.jMeterScrData[key].loop);
        this.totalUsers += this.jMeterScrData[key].runThreads;
      }
      this.isLoading = false;
      this.canConfigScenario = true;
    } else {
      this.isLoading = false;
      this.jMeterScrData = [];
      this.reusable.openAlertMsg(result.message, "error");
      this.authService.invalidSession(result);
    }
  }

  onChangeThreads(scenarioName){
    this.totalUsers = 0;
    for(var key in this.jMeterScrData){
      this.totalUsers += this.jMeterScrData[key].runThreads;
    }
  }

  onChange(runType){
    if(runType == 'scheduler'){
      this.jMeterScrData[this.scenarioName].forever = false;
    }else{
      this.jMeterScrData[this.scenarioName].scheduler = false;
    }
  };

  setScenario(obj){
    let res = this.validateTime(this.jMeterScrData[this.scenarioName]['rampTimeFormat'],'#rampTimeFormat')
    if (!res){
      this.jMeterScrData[this.scenarioName]['rampTimeFormat'] ="00:00:00" ;
    }
    this.scenarioName = obj;
  }

  async jmeterScnUpdate(){
    //each script must have threads >0
    let isScenarioValid = true;
    this.jmeterErrMsg = [];
    for(var key in this.jMeterScrData){
      this.jMeterScrData[key].rampUpTime = this.convertTimeToSec(this.jMeterScrData[key].rampTimeFormat);
      if (this.jMeterScrData[key].runThreads<1){isScenarioValid = false; this.reusable.openAlertMsg(key +" threads count must be >0", "error");}
      if (this.jMeterScrData[key].scheduler){
        this.jMeterScrData[key].loop = -1;
        this.jMeterScrData[key].duration = this.convertTimeToSec(this.jMeterScrData[key].durationTimeFormat);
        if (this.jMeterScrData[key].duration < 1) {isScenarioValid = false; this.reusable.openAlertMsg(key +" duration must be >0, when scheduler is enabled", "error")}
      } else if (!this.jMeterScrData[key].forever) {
        this.jMeterScrData[key].duration = 0;
        if (this.jMeterScrData[key].loop < 1) {isScenarioValid = false; this.reusable.openAlertMsg(key+" loop count must be >0, when scheduler and forever is disabled", "error")}
      } else {
        this.jMeterScrData[key].loop = -1;
        this.jMeterScrData[key].duration = 0;
      }
    } 
    if (isScenarioValid) {
      this.isLoading = true;
      let param = {
        scenarioId: this.JMeterScriptData.scenario_id,
        scenarioName: this.JMeterScriptData.scenario_name,
        testTypeScript: this.JMeterScriptData.scenario_type,
        scriptData: JSON.stringify(this.jMeterScrData)
      }
      let result = await this.authService.updateJMeterScenarioSettings(param);
      if (result.success){
        this.isLoading = false;
        this.reusable.openAlertMsg("Scenario updated successfully","info");
        this.onClose();
      }else {
        this.isLoading = false;
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }

  validateTime(time, control){
		const reqExp = new RegExp(/^(?:\d|[01]\d|2[0-3]):[0-5]\d:[0-5]\d$/);
		if (reqExp.test(time)){
			return true;
		} else{
      this.reusable.openAlertMsg("Valid between 00:00:00 to 23:59:59 hours Only", "error");
      time = "00:00:00";
      const element = this.rend.selectRootElement(control);
      setTimeout(() => element.focus(), 0);     
			return false;
		}
  }

  onClose(): void {
    this.dialogRef.close();
  }

  convertSecToTime(seconds){
    let hr = Math.floor(seconds/3600);
    let strHr;
    if (hr<10) { strHr ='0'+hr;}
    else {strHr = hr.toString();}
    let remainder = seconds%3600;
    let min; 
    let strMin;
    let sec;
    let strSec;
    if (remainder<60){
      min = 0;
      strMin = '00';
      sec = remainder;
      if (sec<10){strSec = '0'+sec;}
      else {strSec = sec.toString();}
    } else {
      min = Math.floor(remainder/60);
      sec = remainder%60;
      if (min<10){strMin = '0'+min;}
      else {strMin = min.toString();}
      if (sec<10){strSec = '0'+sec;}
      else {strSec = sec.toString();}
    }
    return strHr+':'+strMin+':'+strSec;
  }

  convertTimeToSec(time){
    let splitTime = time.toString().split(':');
    let returnVal;
    if (splitTime.length<3) returnVal = Number(splitTime[0])*3600+ Number(splitTime[1])*60;
    else returnVal = Number(splitTime[0])*3600+ Number(splitTime[1])*60+Number(splitTime[2]);
    return returnVal;
  }
}