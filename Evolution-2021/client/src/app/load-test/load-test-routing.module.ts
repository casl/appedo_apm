import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadTestComponent, loadTestReports } from './load-test.component';

const routes: Routes = [
  {
    path: '',
    component: LoadTestComponent
  },
  {
    path: 'reports',
    component: loadTestReports
  },
  {
    path: 'liveReports',
    component: loadTestReports
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class LoadTestRoutingModule { }

