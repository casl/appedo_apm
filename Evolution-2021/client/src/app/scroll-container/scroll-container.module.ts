import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// import { MaterialModule } from '../material'
// import { FlexLayoutModule } from '@angular/flex-layout';

import { ScrollContainerComponent } from './scroll-container.component';


@NgModule({
  imports: [
    CommonModule,
    // FormsModule, 
    // ReactiveFormsModule,
    // MaterialModule,
    // FlexLayoutModule,
  ],
  declarations: [ScrollContainerComponent],
  exports: [ScrollContainerComponent]
})
export class ScrollContainerModule { }

