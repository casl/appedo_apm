import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DrilldownRoutingModule } from './drilldown-routing.module';
import { DrilldownComponent } from './drilldown.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ModuleDetailsModule } from '../module-details/module-details.module';
import { D3ChartsModule } from '../d3Directive/d3Directive.module';

@NgModule({
  imports: [
    CommonModule,
    DrilldownRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    ModuleDetailsModule,
    D3ChartsModule,
  ],
  declarations: [
    DrilldownComponent
  ],
  entryComponents: [
    
  ],
})

export class DrilldownModule { }

