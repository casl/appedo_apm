import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AuthenticationService } from '../_services/index';
import { Router, NavigationExtras } from '@angular/router';
import * as _ from 'lodash';
import { ReusableComponent } from '../reusable/reusable.component'
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-drilldown',
  templateUrl: './drilldown.component.html',
  // styleUrls: ['../dashboard/dashboard.component.css'],
  styleUrls: ['./drilldown.component.css']
})
export class DrilldownComponent implements OnInit {
  objectKeys = Object.keys; //used in display of table content json objec to html table
  objectValues = Object.values; //used in display of table content json objec to html table

  isLoading = false;
  d3ChartData=[];chartWidth=[]; chartHt=[];d3RootChartType=[]; chartTitle =[];
  isTable = []; isStamp = []; stDateTime =[]; endDateTime=[]; isRumTimeParam = false;
  dispColumns = [];
  tableDataSource = [];
  theme;
  //Filter variables for table
  filterText = []; unFilterData = []; prvFilterTxt =[]; dispChartsIds = []; varFilter =[]; 

  //Embed Tokenization changes
  fullTokenData; token; rawToken; encUserId;
  showPage=false; displayMessage='';
  is_embed = false;
  screenParam:any;

  getQryDataParam = [];
  @ViewChild(MatSort) sort: MatSort;

  //scroll variables
  offset = []; 
  limit = [];
  isFetched = []
  lastScrolled = [];
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;
  setIntervalId;

  handleScroll = (index, scrolled: boolean) => {
    if (scrolled) { this.offset[index] += this.limit[index];}
    let idx = Number(index.replace('idx',''));
    scrolled && this.isFetched[index]? this.getScrollDataProcess(this.d3ChartData[idx], this.getQryDataParam[idx], this.offset[index], this.limit[index], idx) : _.noop();
  }

  hasMore(id){
    return this.isFetched[id];
  }

  constructor(
    private authService: AuthenticationService,
    private route: Router,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    private reusable: ReusableComponent
  ) { }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
  }

  ngOnInit() {
    sessionStorage.setItem("currentRoute","home/drilldown");
    let d3DrillDownContent = JSON.parse(sessionStorage.getItem("d3DrillDownEnt"));
    if (d3DrillDownContent.is_embed) {
      this.is_embed = true;
      this.encUserId = d3DrillDownContent.encUserId;
      this.fullTokenData = d3DrillDownContent.fullTokenData;
      this.token = d3DrillDownContent.token;  
    }
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
      if (this.screenParam.height != 0 && this.screenParam.width != 0  ){
        setTimeout(() => {
          this.chartWidth.map((w,ix) => {
            if (this.screenParam.width < 850){
              this.chartWidth[ix] = (this.screenParam.width-30);
            } else {
              this.chartWidth[ix] = (this.screenParam.width-50)/2;
            }
          });
        }, 2000);
      }
    });
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    })
    let pryChData = d3DrillDownContent.chartData;
    let idx = 0
    this.stDateTime[idx] = d3DrillDownContent.startDt;
    this.endDateTime[idx] = d3DrillDownContent.endDt;
    this.isRumTimeParam = d3DrillDownContent.chartData.module_type == 'RUM' || d3DrillDownContent.chartData.module_type == 'LOG' ? true : false;
    this.drawChart(pryChData, d3DrillDownContent, idx);
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => {
        if (event && this.virtualScroll)
          return this.virtualScroll.measureScrollOffset('bottom') === 0;
        else 
          return null;
      })
     ).subscribe(ele =>{
        let id = ele['elementRef']['nativeElement'].id;
        if(this.hasMore(id)){
          this.handleScroll(id,true);
        }
     });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
  }

  tableRow(selRow:any, chartData, chartIndex){
    if (chartData.dd_chart_id == null){
      this.reusable.openAlertMsg("Drill down not configured for this Chart.","info");
    }
    let event = {
      chartData: chartData,
      ddMetricId: chartData.dd_chart_id,
      idx: chartIndex,
      label: selRow[Object.keys(selRow)[0]],
      xAxisTimeScale: chartData.xaxis_time_scale,
      selRowData: selRow
    };
    this.ViewChartVisual(event, chartIndex+1);
  }

  async getScrollDataProcess(chart, param, offset, limit, idx){
    if (chart.engine_name == "postgresql") {
      let qry = chart.execQry+" OFFSET "+offset+" LIMIT "+limit;
      let qParam = {
        query: qry,
        connection_details: chart.connection_details
      }
      let encryptData = this.authService.encrypt(JSON.stringify(qParam));
      let qryParam ={ data: encryptData};
      let result = await this.authService.getChartDataPsql(qryParam).toPromise();
      if (result.message>0){
        chart.data = chart.data.concat(result.result);
        let newData = chart.data;
        this.tableDataSource[idx] = new MatTableDataSource(newData);
        this.tableDataSource[idx].sort = this.sort;
      }
    }
  }

  private drawChart(pryChData: any, d3DrillDownContent: any, idx: number) {
    if (pryChData.root_chart_type =='table'){
      this.filterText[idx] = [];
      this.isTable[idx] = true;
    } else {
      this.isTable[idx] = false;
    }
    this.isStamp[idx] = false;
    this.d3ChartData[idx] = {
      h_display: pryChData.h_display,
      enable_yaxis: true,
      xaxis_time_scale: pryChData.xaxis_time_scale,
      chart_types_json: pryChData.chart_types_json,
      col_units_json: pryChData.col_units_json,
      dd_param: pryChData.dd_param,
      data: pryChData.data,
      col_critical: pryChData.col_critical,
      col_warning: pryChData.col_warning,
      root_chart_type: pryChData.root_chart_type,
      dd_chart_id: d3DrillDownContent.ddMetricId,
      from_new: pryChData.from_new
    };
    this.chartWidth[idx] = window.screen.availWidth >=750 ? (window.screen.availWidth - 50) / 2 : window.screen.availWidth - 50;
    this.chartHt[idx] = pryChData.chart_param.height == undefined ? 250 :   pryChData.chart_param.height  ;
    this.chartTitle[idx] = pryChData.chart_title + " "+ pryChData.category +" ("+pryChData.chart_id+")";
    this.d3RootChartType[idx] = pryChData.root_chart_type;
    this.ViewChartVisual(d3DrillDownContent, idx+1);
    setTimeout(()=>{
      this.authService.toggleMenu.next(false);
    },1000);
  }

  onBackFromCard(){
    sessionStorage.removeItem("d3DrillDownEnt");
    if (this.is_embed) {
      let navigationExtras: NavigationExtras = {
        queryParams : {encUserId: this.encUserId}
      };
      this.route.navigate(['embedDashboard/', this.token], navigationExtras);
    } else {
      this.route.navigate(['home/dashboard']);
    }
  }

  fromD3chart(event,idx){
    this.ViewChartVisual(event, idx+1);
  }

  ViewChartVisual (drillDownContent, idx){
    this.isTable[idx] = false;
    this.isStamp[idx] = false;
    this.stDateTime[idx] = this.stDateTime[idx-1];
    this.endDateTime[idx] = this.endDateTime[idx-1];
    let ddparams = {};
    let chartId = drillDownContent.ddMetricId
    if (this.offset[idx] == undefined){
      this.offset['idx'+idx] = 0;
      this.limit['idx'+idx] = 20;
      this.lastScrolled['idx'+idx] = 0;
      this.isFetched['idx'+idx] = true;
    }
    if (chartId == undefined) {
      this.reusable.openAlertMsg("Drill down not configured for this Chart.","info");
      return;
    }
    try {
      drillDownContent.chartData.dd_param = JSON.parse(drillDownContent.chartData.dd_param);
    } 
    catch {
      //nothing to be done.
    }
    // console.log(drillDownContent.chartData.dd_param, Object.values(drillDownContent.chartData.dd_param));
    Object.values(drillDownContent.chartData.dd_param).filter((item, i) => {
      if (item != 'none') {
        ddparams[item.toString()] = Object.keys(drillDownContent.chartData.dd_param)[i] ;
      }
    });
    if (!this.isLoading){
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let chartData;
      this.isLoading = true;
      if (drillDownContent.xAxisTimeScale){
        let selTime = drillDownContent[Object.keys(drillDownContent)[0]];
        let endTime = selTime + drillDownContent.timeDiff;
        let param = this.authService.pubSetDTFormat(selTime,endTime);
        //chartData = drillDownContent.selRowData.ddparams;
        chartData = {
          entUserId: this.is_embed ? null : entItem.owner_id,
          chartId: chartId,
          startTime: selTime/1000,
          endTime: endTime/1000,
          dtFormat: param.dtFormat,
          qryInterval: param.qryInterval,
          qryType: param.qryType,
          aggQryTableName: param.aggQryTableName,
          dateTrunc_ms: param.dateTrunc_ms,
          entId: entItem != undefined && entItem.e_id != undefined ? entItem.e_id == 0 ? null: entItem.e_id : null,
          param1: this.isRumTimeParam ? selTime/1000 : drillDownContent.selRowData[ddparams['param1']],
          param2: this.isRumTimeParam ? endTime/1000 : drillDownContent.selRowData[ddparams['param2']],
          param3: drillDownContent.selRowData[ddparams['param3']],
          param4: drillDownContent.selRowData[ddparams['param4']],
          param5: drillDownContent.selRowData[ddparams['param5']],
          offset: this.offset['idx'+idx],
          limit: this.limit['idx'+idx],
          from_new: drillDownContent.chartData.from_new,
        } 
      } else {
        //when apddtparent is found as ddparam in the query,then date time is filtered between +5 and -5 min. instead of default time
        Object.keys(ddparams).map(p =>{
          if (ddparams[p] == "apddtparent") {
            this.endDateTime[idx] = new Date(drillDownContent.selRowData[ddparams[p]]).getTime()+5*60*1000; //adding 5 min to end time
            this.stDateTime[idx] = this.endDateTime[idx]-10*60*1000;
          }
        });
        let param = this.authService.pubSetDTFormat(this.stDateTime[idx],this.endDateTime[idx]);
        chartData = {
          entUserId: this.is_embed ? null : entItem.owner_id,
          chartId: chartId,
          startTime: this.stDateTime[idx]/1000,
          endTime: this.endDateTime[idx]/1000,
          dtFormat: param.dtFormat,
          qryInterval: param.qryInterval,
          qryType: param.qryType,
          aggQryTableName: param.aggQryTableName,
          dateTrunc_ms: param.dateTrunc_ms,
          entId: entItem != undefined && entItem.e_id != undefined ? entItem.e_id == 0 ? null: entItem.e_id : null,
          param1: drillDownContent.selRowData[ddparams['param1']],
          param2: drillDownContent.selRowData[ddparams['param2']],
          param3: drillDownContent.selRowData[ddparams['param3']],
          param4: drillDownContent.selRowData[ddparams['param4']],
          param5: drillDownContent.selRowData[ddparams['param5']],
          offset: this.offset['idx'+idx],
          limit: this.limit['idx'+idx],
          from_new: drillDownContent.chartData.from_new,
        }
      }
      //Added following to bypass authorization check.
      chartData['is_embed'] = this.is_embed;
      chartData['id'] = this.is_embed ? this.fullTokenData.id : null;
      chartData['embedParams'] = this.is_embed && this.fullTokenData.tokenData.hasOwnProperty('params') ? this.fullTokenData.tokenData.params : null;
      this.getQryDataParam[idx] = chartData;
      this.authService.getChartDataByChartId(chartData).subscribe(cvEncryptData => {
        this.isLoading = false;
        if (cvEncryptData.success){
          let cvEncryptData1 = this.authService.decrypt(cvEncryptData.result);
          // this.authService.decrypt(cvEncryptData.result).then(cvEncryptData => {
            let cvData = JSON.parse(cvEncryptData1);
            try {
              cvData.chart_types_json = JSON.parse(cvData.chart_types_json);
            } catch {
              //if failed do nothing. this is requried for new chart visual.
            }
            try {
              cvData.col_units_json = JSON.parse(cvData.col_units_json);
            } catch {
              //if failed do nothing. This is required for new chart visual 
            }
            this.d3ChartData[idx] = cvData; 
            this.chartWidth[idx] = this.chartWidth[idx-1]
            this.chartHt[idx] = this.chartHt[idx-1]; 
            this.d3RootChartType[idx] = cvData.root_chart_type;
            this.filterText[idx] = [];
            this.prvFilterTxt[idx] = [];
            this.unFilterData[idx] = [];
            if (this.d3RootChartType[idx] == 'table') {
              this.isTable[idx] = true;
              this.isStamp[idx] = false;
              this.unFilterData[idx] = this.d3ChartData[idx].data;
              this.tableDataSource[idx] = new MatTableDataSource(this.d3ChartData[idx].data);
              this.tableDataSource[idx].sort = this.sort
              if (this.tableDataSource[idx].data.length>0){
                this.dispColumns[idx] = Object.keys(this.tableDataSource[idx].data[0]); 
              } else {
                this.dispColumns[idx] = [];
              }
            } else if (this.d3RootChartType[idx] == 'stamp') {
              this.tableDataSource[idx] = [];
              this.isTable[idx] = false;
              this.isStamp[idx] = true;
            } else {
              this.tableDataSource[idx] = [];
              this.isTable[idx] = false;
              this.isStamp[idx] = false;
            }
            let dateField = drillDownContent[Object.keys(drillDownContent)[0]];
            let repStrDate = dateField.toString().replace(' ','');
            if (!isNaN(repStrDate)) repStrDate = Number(repStrDate);
            let date = new Date(repStrDate);
            if (date.toString() == "Invalid Date") {repStrDate = drillDownContent.selRowData[Object.keys(drillDownContent.selRowData)[0]]}
            else repStrDate = this.authService.convertDateTime(repStrDate);
            this.chartTitle[idx] = this.d3ChartData[idx].chart_title + " "+ this.d3ChartData[idx].category +" - "+repStrDate+" ("+chartId+")";
          // });
        } else {
          if (this.is_embed) {
            this.reusable.openAlertMsg(cvEncryptData.message,"error");
          } else {
            this.d3ChartData[idx] =[]; 
            this.tableDataSource[idx] = new MatTableDataSource([]);
            this.reusable.openAlertMsg(this.authService.invalidSession(cvEncryptData),"error");
          }
        }
      }, error => {
        this.isLoading = false;
        this.reusable.openAlertMsg(error.message,"error");
      })
    } 
  }

  tableFilter(rowheader, index, parentIdx,leng,filterType) {
    let unfilteredData = this.unFilterData[parentIdx];
    this.varFilter[parentIdx] = {
      rowheader: rowheader,
      idx: index, 
      parentIdx: parentIdx,
      leng: leng,
      filterType:filterType
    };
    let itr = 1;
    let filterIdx = [];
    //clear filter
    if (filterType == "cf"){
      while(itr<=leng){
        this.filterText[parentIdx][itr-1] = '';
        itr++;
      }
      this.varFilter[parentIdx] = undefined;
      this.d3ChartData[parentIdx].data = unfilteredData
      return;
    }

    //check for existence of previous filters
    while(itr<=leng){
      if(this.filterText[parentIdx][itr-1] != undefined && this.filterText[parentIdx][itr-1] != '') {
        filterIdx.push(itr-1);
      }
      itr++;
    }
    if (filterIdx.length==0) {
      //if no filter content exist, will reset to unfiltered data;
      this.d3ChartData[parentIdx].data = unfilteredData
    } else {
      //actual filter operation
      let filterText = this.filterText[parentIdx][index];
      let filData = [];
      if(filterIdx.length>1){
        filData =  this.d3ChartData[parentIdx].data;
      } else {
        filData = unfilteredData 
      }
      let filterData =[];
      if(filterType =='sc'){
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader] == null? '' : row[rowheader];
          if (_.includes(txt.toString().toLowerCase(), filterText.toLowerCase())) {
            return txt;
          }
        });
      }
      if(filterType =='se'){
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader] == null? '' : row[rowheader];
          if (txt.toString().toLowerCase() == filterText.toLowerCase()) {
            return txt;
          }
        });
      }
      if(filterType =='nr'){
        let range = filterText.split('-')
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          if (!isNaN(txt) && (txt>=Number(range[0]) && txt <= Number(range[1]))) {
            return txt;
          }
        });
      }
      if(filterType =='ng' && !isNaN(filterText)){
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          if (!isNaN(txt) && txt>=Number(filterText)) {
            return txt;
          }
        });
      }
      if(filterType =='nl' && !isNaN(filterText)){
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          if (!isNaN(txt) && txt<=Number(filterText)) {
            return txt;
          }
        });
      }
      if(filterType =='ne' && !isNaN(filterText)){
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          if (!isNaN(txt) && txt==Number(filterText)) {
            return txt;
          }
        });
      }
      if(filterType =='de'){
        let filText = (new Date(filterText)).toLocaleDateString();
        if (filText.toString() == "Invalid Date") {
          this.filterText[parentIdx][index]= '';
          //this.alertService.info("Invalid Date format used");
          return;
        }
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          let filDate = (new Date(txt).toLocaleDateString());
          if ( filText==filDate) {
            return txt;
          }
        });
      }

      if(filterType =='dg'){
        let filText = (new Date(filterText+"T00:00:00"))
        if (filText.toString() == "Invalid Date") {
          this.filterText[parentIdx][index]= '';
          //this.alertService.info("Invalid Date format used");
          return;
        }
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          let filDate = new Date(txt);
          if ( filDate>=filText) {
            return txt;
          }
        });
      }
      if(filterType =='dl'){
        let filText = (new Date(filterText+"T23:59:59"))
        if (filText.toString() == "Invalid Date") {
          this.filterText[parentIdx][index]= '';
          //this.alertService.info("Invalid Date format used");
          return;
        }
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          let filDate = new Date(txt);
          if ( filDate<=filText) {
            return txt;
          }
        });
      }
      if(filterType =='dr'){
        let splitData = filterText.split("|");
        let startDate = new Date(splitData[0]+"T00:00:00");
        let endDate = new Date(splitData[1]+"T23:59:59");
        if (startDate.toString() == "Invalid Date") {
          this.filterText[parentIdx][index]= '';
          //this.alertService.info("Invalid startDate format used");
          return;
        }
        if (endDate.toString() == "Invalid Date") {
          this.filterText[parentIdx][index]= '';
          //this.alertService.info("Invalid endDate format used");
          return;
        }
        
        filterData = _.filter(filData, function(row){
          let txt = row[rowheader];
          let filDate = new Date(txt);
          if ( filDate>=startDate && filDate<=endDate) {
            return txt;
          }
        });
      }

      if (filterData.length >0 ){
        this.d3ChartData[parentIdx].data  = filterData;
        if(this.prvFilterTxt[parentIdx] == undefined ) this.prvFilterTxt[parentIdx] = [];
        this.prvFilterTxt[parentIdx][index] = this.filterText[parentIdx][index];
      } else {
        this.filterText[parentIdx][index]= this.prvFilterTxt[parentIdx][index];
        //this.alertService.info("No Data to Filter, retaining last filter data");
      }
    }
  }

  getRowHeader(rhead){
    return rhead.replace(/_/gi,' ');
  }

  convertSecToTime(intSec){
    return this.authService.convertSecToTime(intSec);
  }

  convertMstoTime(intMs){
    return this.authService.convertMstoTime(intMs);

  }

  convertDate(strDate) {
    return this.authService.convertDate(strDate);
  }

  convertDateTime(strDate) {
    return this.authService.convertDateTime(strDate);
  }

  convertTime(strDate) {
    return this.authService.convertTime(strDate);
  }

  drillDownFromTbl(tblRow, chart,idx){
    let event = {};
    event['chartData']= chart;
    event['selRowData'] = tblRow;
    event['startDt'] = this.stDateTime[idx];
    event['endDt'] = this.endDateTime[idx];
    event['timeDiff'] = chart.timeDiff;
    event['xAxisTimeScale'] = chart.xaxis_time_scale;
    event['idx'] = chart.chart_id;
    event['ddMetricId'] = chart.dd_chart_id;
    this.fromD3chart(event,idx);
  }
}
