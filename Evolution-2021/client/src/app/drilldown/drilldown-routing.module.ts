import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrilldownComponent } from './drilldown.component';

const routes: Routes = [
  {
    path: '',
    component: DrilldownComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class DrilldownRoutingModule { }

