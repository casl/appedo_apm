import { Component, OnInit, ViewChild, Inject, Pipe, PipeTransform, Injectable, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray, FormControlName} from '@angular/forms';
import { filter as _filter, find as _find, findIndex as _findIndex, noop as _noop } from 'lodash';
import { environment } from 'src/environments/environment';
import { saveAs } from 'file-saver';
import { filter } from 'rxjs/operators';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { AlertDialog, LogDetailsComponent } from '../log-module/log-module.component'
declare const Buffer;

export interface DialogSumAddEdit {
  testId: string,
}

export interface DialogAddEdit {
  uid: string,
  modName: string,
  modDesc: string,
  respTimeAlert: boolean,
  warning: number,
  critical: number,
  breachCount: number,
  guid: string
}

@Component({
  selector: 'app-external-monitors',
  templateUrl: './external-monitors.component.html',
  styleUrls: ['./external-monitors.component.css'],
})

export class ExternalMonitorsComponent implements OnInit {
  modCode: string;
  entItem: any;
  usrDet: any;
  modRumColl = new MatTableDataSource([]);
  modRumCollDispColumn = ['delete','edit','download','uid', 'module_name','alert','chart', 'critical', 'warning','active'];
  noRowsMsg: string;
  selectedIdx:number;
  modSumColl = new MatTableDataSource([]);
  modSumCollDispColumn = ['delete','edit','test_id', 'testname','testtype', 'runevery', 'status', 'chart'];
  modAvmColl = new MatTableDataSource([]);
  modAvmCollDispColumn = ['delete', 'edit', 'avm_test_id', 'testname', 'testurl', 'frequency', 'status_text','alert', 'chart', 'details','breach_cnt','status'];
  avmSelectedCities = [];
  modAvlColl = new MatTableDataSource([]);
  modAvlCollDispColumn = ['delete', 'alert','agent_id', 'country','state', 'city', 'ip_address', 'latitude', 'longitude', 'last_requested_on', 'details','status'];

  @ViewChild(MatSort) tblSort: MatSort;
  @ViewChild(MatSort) tblSumSort: MatSort;
  @ViewChild(MatSort) tblAvmSort: MatSort;
  @ViewChild(MatSort) tblAvlSort: MatSort;

  isLoading: boolean;
  modParam: { uid: string; modName: string; modDesc: string; respTimeAlert: boolean; warning: number; critical: number; breachCount: number; guid: string; };
  modSumParam: {testId:number};
  obsEnt: any;
  theme: any;
  screenParam:any;
  modAvmParam: { testId: any; };
  modAvlParam: { testId: any; };
  modAlertParam: { testId: any; };
  breachAVMHandler;

  //scroll variables
  offset = {rum:0, sum:0, avm:0, avl:0}; //eg = enterprise grid, umg - user map grid
  limit = {rum:20, sum:20, avm:20, avl:20};
  lastScrolled = {rum:0, sum:0, avm:0, avl:0};
  isFetched = {rum:false, sum:false, avm:false, avl:false};
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;

  setIntervalId;

  handleScroll = (scrolled: boolean, origin) => {
    if (scrolled) {
      if(origin=="rum") this.offset['rum'] += this.limit['rum']; 
      if(origin=="sum") this.offset['sum'] += this.limit['sum']; 
      if(origin=="avm") this.offset['avm'] += this.limit['avm']; 
      if(origin=="avl") this.offset['avl'] += this.limit['avl']; 
    }
    scrolled && origin=='rum' && this.isFetched['rum']? this.getModuleDetails(this.offset['rum'], this.limit['rum']) : _noop();
    scrolled && origin=='sum' && this.isFetched['sum']? this.getSumCardDetails(this.offset['sum'], this.limit['sum']) : _noop();
    scrolled && origin=='avm' && this.isFetched['avm']? this.getAVMonCard(this.offset['avm'], this.limit['avm']) : _noop();
    scrolled && origin=='avl' && this.isFetched['avl']? this.getAVLocCard(this.offset['avl'], this.limit['avl']) : _noop();
  }
  alertData: { uid: any; log_grok: any; log_table_name: any; module_code: string; module_name: any; };

  hasMore(origin){
    return this.isFetched[origin];
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    
    sessionStorage.setItem("currentRoute","/home/extmonitors");
    this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
    this.usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    let curIndex = sessionStorage.getItem("extCurIndex");
    this.setIndex(curIndex);
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
        let curIndex = sessionStorage.getItem("extCurIndex");
        this.setIndex(curIndex);
      }
    });
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if (ele != undefined){
        let id = ele['elementRef']['nativeElement'].id;
        if(this.hasMore(id))
          this.handleScroll(true, id);
      }
    });
    this.setIntervalId = setInterval(()=>{
      this.cd.detectChanges();
    },2000);
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.obsEnt != undefined)
    this.obsEnt.unsubscribe();
    clearInterval(this.breachAVMHandler);
  }

  private setIndex(curIndex: string) {
    if (curIndex != undefined) {
      if (Number(curIndex) == 0) {
        this.onTabChange(0);
        this.selectedIdx = 0;
      }
      else if (Number(curIndex) == 1) {
        this.onTabChange(1);
        this.selectedIdx = 1;
      }
      else if (Number(curIndex) == 2) {
        this.onTabChange(2);
        this.selectedIdx = 2;
      }
      else if (Number(curIndex) == 3) {
        this.onTabChange(3);
        this.selectedIdx = 3;
      }
    }
    else {
      this.onTabChange(1);
      this.selectedIdx = 1;
    }
  }

  onTabChange(tabIdx){
    if (tabIdx == 0){
      this.modCode = 'rum';
      sessionStorage.setItem("extCurIndex","0");
      clearInterval(this.breachAVMHandler);
      this.getModuleDetails();
    } else if (tabIdx == 1){
      sessionStorage.setItem("extCurIndex","1");
      this.modCode = 'sum';
      clearInterval(this.breachAVMHandler);
      this.getSumCardDetails();
    } else if (tabIdx == 2){
      sessionStorage.setItem("extCurIndex","2");
      this.modCode = 'avm';
      this.getAVMonCard();
    } else {
      sessionStorage.setItem("extCurIndex","3");
      this.modCode = 'avl';
      clearInterval(this.breachAVMHandler);
      this.getAVLocCard();
    }
  }

  openAVMBreachDetails(ele){
    ele['module_name'] = 'AVM Breach-'+ele.testname;
    ele["log_grok"] = "AVM";
    ele["uid"] = ele.avm_test_id;
    ele["log_table_name"] = "so_log_threshold_breach_";
    sessionStorage.setItem("logDetails", JSON.stringify(ele));
    this.router.navigate(['/home/logmodule/logdetails']);
  }

  openAgentDetails(ele){
    ele['module_name'] = 'Agent-'+ele.registered_location;
    ele["log_grok"] = "AVM";
    ele["uid"] = ele.agent_id;
    ele["log_table_name"] = "avm_test_run_details";
    sessionStorage.setItem("logDetails", JSON.stringify(ele));
    this.router.navigate(['/home/logmodule/logdetails']);
  }

  openAVMDetails(ele){
    ele['module_name'] = ele.testname;
    ele["log_grok"] = "AVM";
    ele["uid"] = ele.avm_test_id;
    ele["log_table_name"] = "log_avm_"
    sessionStorage.setItem("logDetails", JSON.stringify(ele));
    this.router.navigate(['/home/logmodule/logdetails']);
  }

  openAVMAlertModule(ele){
    this.alertData = {uid:ele.avm_test_id, log_grok:'AVM', log_table_name: 'log_avm_', module_code:'AVM', module_name: ele.testname};
    this.openAVMAlertDialog();
  }

  openAVMAlertDialog(): void {
    const dialogRef = this.dialog.open(AlertDialog, {
      width: '60%',
      data: {uid:this.alertData.uid, log_grok:this.alertData.log_grok, log_table_name: this.alertData.log_table_name, module_code:this.alertData.module_code, module_name: this.alertData.module_name}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined)
        this.getAVMonCard();
    });
  }

  openAlert(row){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modAlertParam = {testId: row.agent_id};
      this.openAlertDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Add AVL Alerts","info");
    }
  }

  openAlertDialog(): void {
    const dialogRef = this.dialog.open(avlAlertDialog, {
      width: '50%',
      data: {testId: this.modAlertParam.testId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getAVLocCard();
      }
    });
  }

  openAvlModuleAdd(){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modAvlParam = {testId: null};
      this.openAvlModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Add AVM Module","info");
    }
  }

  openAvlModuleDialog(): void {
    const dialogRef = this.dialog.open(avlAddEditDialog, {
      width: '50%',
      data: {testId: this.modAvlParam.testId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getAVLocCard();
      }
    });
  }

  async getAVLocCard(offset?:number, limit?:number){
    if(!this.isLoading){
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let entName = entItem.owner_id != null ? ' for enterprise "'+entItem.e_name+'"':''
      let eId = entItem.owner_id != null ? entItem.e_id : null;
      let eUserId = entItem.owner_id;
      let reset = false;
      if (offset == undefined){offset = 0; this.offset['avl'] = 0; reset = true; } 
      if (limit == undefined) limit = 20;
      this.isLoading = true;
      let eData = {
        e_id: eId,
        e_user_id: eUserId,
        offset: offset,
        limit: limit
      }
      let result = await this.authService.getAVMLocationCard(eData);
      this.isLoading = false;
      if (result.success && result.rowCount>0){
        this.isFetched['avl'] = true;
        result.result.map(card =>{
          this.splitLocation(card);
        })
        if (this.modAvlColl.data.length == 0 || reset){
          this.modAvlColl = new MatTableDataSource(result.result);
        } else {
          let newData = this.modAvlColl.data.concat(result.result);
          this.modAvlColl = new MatTableDataSource(newData);
        }
        this.modAvlColl.sort = this.tblAvlSort;
      } else if (result.rowCount == 0){
        if (reset){
          this.modAvlColl = new MatTableDataSource([]);
        }
        this.isFetched['avl'] = false;
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }

  async splitLocation(avlcard){
    let location = avlcard.location.split('#');
    avlcard['country'] = location[0];
    avlcard['state'] = location[1];
    avlcard['city'] = location[2];
    avlcard['region'] = location[3];
    avlcard['zone'] = location[4];
    if (avlcard.last_requested_on <= new Date().getTime()-10*60*1000){
      avlcard['status'] = "down";
    }
  }
  async deleteAvLoc(agentId){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of AVM location");
      if(confirmMessage) {
        this.isLoading = true;
        let qryData = {agent_id: agentId};
        let result = await this.authService.deleteAvmLoc(qryData)
        this.isLoading = false;
        if(result.success){
          this.reusable.openAlertMsg(result.message, "info");
          this.getAVLocCard();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
        }
      } 
    } else {
      this.reusable.openAlertMsg("Sorry, Only enterprise owner of enterprise '"+entItem.e_name+"' can delete this location","error");
    }
  }

  async deleteAvm(element){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of AVM Test "+element.testname);
      if(confirmMessage) {
        this.isLoading = true;
        let qryData = {avm_test_id: element.avm_test_id};
        let result = await this.authService.deleteAvm(qryData);
        this.isLoading = false;
        if(result.success){
          this.reusable.openAlertMsg(result.message,"info");
          this.authService.updAVMAgents();
          this.getAVMonCard();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
        }
      }
    } else {
      this.reusable.openAlertMsg("Sorry, Only enterprise owner of enterprise '"+entItem.e_name+"' can delete this AVM", "error");
    }
  }

  openAvmModuleAdd(){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modAvmParam = {testId: null};
      sessionStorage.setItem("modAvmParam",JSON.stringify(this.modAvmParam));
      this.router.navigate(['home/extmonitors/avm'])
      // this.modAvmParam = {testId: null};
      // this.openAvmModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Add AVM Module","info");
    }
  }

  openAvmModuleEdit(row){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modAvmParam = {testId: row.avm_test_id};
      sessionStorage.setItem("modAvmParam",JSON.stringify(this.modAvmParam));
      this.router.navigate(['home/extmonitors/avm'])
      // this.openAvmModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit AVM Module","info");
    }
  }

  openAvmModuleDialog(): void {
    const dialogRef = this.dialog.open(avmAddEditDialog, {
      width: '50%',
      data: {testId: this.modAvmParam.testId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getAVMonCard();
      }
    });
  }
  async getAVMonCard(offset?:number, limit?:number){
    if(!this.isLoading){
      let reset = false;
      if (offset == undefined){offset = 0; this.offset['avm'] = 0; reset = true; } 
      if (limit == undefined) limit = 20;
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let entName = entItem.owner_id != null ? ' for enterprise "'+entItem.e_name+'"':''
      let eId = entItem.owner_id != null ? entItem.e_id : null;
      this.isLoading = true;
      let eData = {
        e_id: eId,
        e_user_id: entItem.owner_id,
        offset: offset,
        limit: limit
      }
      let result = await this.authService.getAVLMonCard(eData);
      this.isLoading = false;
      if (result.success && result.rowCount > 0) {
        if (this.modAvmColl.data.length == 0 || reset){
          this.isFetched['avm'] = true;
          this.modAvmColl = new MatTableDataSource(result.result);
          this.modAvmColl.data.map((avmCard,idx) => {
            this.getAVMTestLocByTestId(avmCard,idx)
            this.getAVMPolicyCountByTestId(avmCard);
            this.getBreachCntByModuleUid(avmCard);
          });
        } else {
          let newData = this.modAvmColl.data.concat(result.result);
          this.modAvmColl = new MatTableDataSource(newData);
          result.result.map((card, idx) => {
            this.getAVMTestLocByTestId(card,idx);
            this.getAVMPolicyCountByTestId(card);
            this.getBreachCntByModuleUid(card);
          });
        }
        this.breachAVMHandler = setInterval(()=>{
          this.startAutoUpdAVMBreach();
        }, 60000);
        this.modAvmColl.sort = this.tblAvmSort;
      } else if (result.rowCount == 0){
        if (reset){
          this.modAvmColl = new MatTableDataSource([]);
        }
        this.isFetched['avm'] = false;
      } else {
        this.modAvmColl = new MatTableDataSource([]);
        this.reusable.openAlertMsg(this.authService.invalidSession(result), "error");
      }
    }
  }

  async getBreachCntByModuleUid(avmCard){
    let param = {module: 'AVM', uid:avmCard.avm_test_id};
    let res = await this.authService.getBreachCntByModuleUid(param).toPromise();
    if (res.success){
      avmCard.breach_cnt = res.result[0].count;
    } else {
      this.authService.invalidSession(res);
    }
  }

  startAutoUpdAVMBreach(){
    this.modAvmColl.data.map(avmCard=>{
      this.getBreachCntByModuleUid(avmCard);
    })
  }

  async getAVMPolicyCountByTestId(avmCard){
    let avmTestData = {
      avmTestId : avmCard.avm_test_id
    }
    let res = await this.authService.getAVMPolicyCountByTestId(avmTestData).toPromise();
    if (res.success){
      avmCard.policy_cnt = res.result[0].count;
    } else {
      this.authService.invalidSession(res);
    }
  }

  async getAVMTestLocByTestId(avmCard,idx){
    let avmTestData = {
      avmTestId : avmCard.avm_test_id
    }
    let res = await this.authService.getAVMTestLocByTestId(avmTestData).toPromise();
    if (res.success){
      this.avmSelectedCities[idx] = res.result;
      res.result.map((loc,i) => {
        if (avmCard.locations == undefined) avmCard.locations = '';
        if (i != 0) avmCard.locations += ", ";
        avmCard.locations += loc.city ;
      });
    } else {
      this.avmSelectedCities[idx] = [];
      this.avmSelectedCities[idx].push({avm_test_id:avmCard.avm_test_id,city:"No city mapped"});
      avmCard.locations = "No city mapped";
    };
  }

  async getSumCardDetails(offset?:number, limit?:number){
    this.isLoading = true;
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['sum'] = 0; reset = true; } 
    if (limit == undefined) limit = 20;
    let sumCard = {
      eId : this.entItem != undefined && this.entItem.e_id != undefined && this.entItem.e_id !=0 ? this.entItem.e_id : null,
      offset: offset,
      limit: limit
    };
    let result = await this.authService.getSumCardData(sumCard);
    this.isLoading = false;
    if (result.success && result.rowCount>0){
      this.isFetched['sum'] = true;
      if (this.modSumColl.data.length == 0 || reset){
        this.modSumColl = new MatTableDataSource(result.result);
      } else {
        let newData = this.modSumColl.data.concat(result.result);
        this.modSumColl = new MatTableDataSource(newData);
      }
      this.modSumColl.sort = this.tblSumSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.modSumColl = new MatTableDataSource([]);
      }
      this.isFetched['sum'] = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async deleteSUMCard(ele) {
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of SUM Test - '"+ele.testname+" ("+ele.test_id+")'");
      if(confirmMessage) {
        this.isLoading = true;
        let reqData = {testId : +ele.test_id, moduleCode: 'SUM', entId : this.entItem.e_id == 0 ? null : this.entItem.e_id };
        let res = await this.authService.deleteSumModule(reqData);
        this.isLoading=false;
        if (res.success) {
          this.reusable.openAlertMsg(res.message, "info");
          this.getSumCardDetails();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
        }
      }
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can delete this card","info");
    }
  }

  async deleteRumCard(row){
    let confirmMessage = confirm("Please confirm the deletion of RUM Module - "+row.module_name+" ("+row.uid+")");
    if(confirmMessage) {
      this.isLoading = true;
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let reqData = {uid : +row.uid, moduleCode: 'RUM', entId : entItem.e_id == 0 ? null : entItem.e_id };
      let res = await this.authService.deleteRumModule(reqData);
      this.isLoading = false;
      if (res.success) {
        this.reusable.openAlertMsg(res.message,"info");
        if (this.modCode != undefined){
          this.getModuleDetails();
        }
      } else {
        this.authService.invalidSession(res);
      }
    } else {
      this.reusable.openAlertMsg("Delete operation cancelled by User.","info");
    }
  }

  downloadUrl(element) {
    let url = environment.domain+'/downloader/downloadAgent?type=RUM&guid='+element.guid;
    this.authService.downloadAgent(url).then(blob =>{
      saveAs(blob, 'appedo_rum_script.zip');
    });
    //window.open(url,'_blank');
  }

  openSumModuleAdd(){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modSumParam = {testId: null};
      this.openSumModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Add SUM Module Attributes","info");
    }
  }
  openSumModuleEdit(row){
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || usrDet.unq_id == this.entItem.owner_id){
      this.modSumParam = {testId: row.test_id};
      this.openSumModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit SUM Module Attributes","info");
    }
  }
  openSumModuleDialog(): void {
    const dialogRef = this.dialog.open(sumAddEditDialog, {
      width: '50%',
      data: {testId: this.modSumParam.testId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getSumCardDetails();
      }
    });
  }

  openModuleAdd(){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.modParam = {uid:null, modName:null, modDesc: null, respTimeAlert: true, warning:5, critical:10, breachCount: 10, guid:null};
      this.openModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Add RUM Module Attributes","info");
    }
  }

  openModuleEdit(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.modParam = {uid:row.uid, modName:row.module_name, modDesc: row.description, respTimeAlert: row.resTimeAlert, warning:row.warning_threshold_value, critical: row.critical_threshold_value, breachCount: row.min_breach_count, guid: row.guid};
      this.openModuleDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit RUM Module Attributes","info");
    }
  }
  
  openModuleDialog(): void {
    const dialogRef = this.dialog.open(rumAddEditDialog, {
      width: '40%',
      data: {uid:this.modParam.uid, modName:this.modParam.modName, modDesc: this.modParam.modDesc, respTimeAlert: this.modParam.respTimeAlert, warning:this.modParam.warning, critical: this.modParam.critical, breachCount: this.modParam.breachCount, guid:this.modParam.guid}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getModuleDetails();
      }
    });
  }

  async getModuleDetails(offset?:number, limit?:number){
    this.isLoading = true;
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['rum'] = 0; reset = true; } 
    if (limit == undefined) limit = 20;
    let param = {
      entUserId: this.entItem == undefined || this.entItem.owner_id == undefined ? null : this.entItem.owner_id , 
      entId: this.entItem == undefined || this.entItem.owner_id == undefined ? 0 : this.entItem.e_id,
      modCode: this.modCode,
      offset: offset,
      limit: limit
    };
    let result = await this.authService.getMMCardRum(param);
    this.isLoading = false;
    if (result.success){
      this.isFetched['rum'] = true;
      if (this.modRumColl.data.length == 0 || reset){
        this.modRumColl = new MatTableDataSource(result.result);
        result.result.map(row => {
          this.getRumAlertsForUser(row, param);
          this.getRumBreachSettings(row, param);
        })
      } else {
        let newData = this.modRumColl.data.concat(result.result);
        result.result.map(row => {
          this.getRumAlertsForUser(row, param);
          this.getRumBreachSettings(row, param);
        });
        this.modRumColl = new MatTableDataSource(newData);
      }
      this.modRumColl.sort = this.tblSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.modRumColl = new MatTableDataSource([]);
      }
      this.isFetched['rum'] = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async getRumAlertsForUser(rumCard, parentParam){
    let param = {uid: rumCard.uid,entId: parentParam.entId, entUserId:parentParam.entUserId}
    let result = await this.authService.getRumAlertsForUser(param).toPromise();
    if (result.success){
      rumCard['critical'] = result.result[0].critical;
      rumCard['warning'] = result.result[0].warning
    }
  }

  async getRumBreachSettings(rumCard, parentParam){
    let param = {uid: rumCard.uid,entId: parentParam.entId, entUserId:parentParam.entUserId}
    let result = await this.authService.getRumBreachSettings(param);
    if (result.success){
      let row = result.result[0];
      rumCard['sla_id'] = row.sla_id;
      rumCard['warning_threshold_value'] = row.warning_threshold_value/1000; //converting to seconds
      rumCard['critical_threshold_value'] = row.critical_threshold_value/1000;
      rumCard['min_breach_count'] = row.min_breach_count;
      rumCard['is_above_threshold'] = row.is_above_threshold;
    }
  }

  async drawChart(uidData,modCode){
    uidData['module_code'] = modCode;
    sessionStorage.setItem("drawChartUid",JSON.stringify(uidData));
    sessionStorage.removeItem("dashSelVal");
    this.router.navigate(['/home/dashboard']);
  }
}

/* Dialog screen ts file for RUM-AddEdit-dialog.html */
@Component({
  selector: 'rum-addedit-dialog',
  templateUrl: 'rum-addedit-dialog.html',
})
export class rumAddEditDialog implements OnInit {
  btnDisable = false;
  isLoading: boolean;
  modCode: string;
  form: FormGroup;
  rumName: string;
  rumDesc: string;
  warnThreshold: number = 5;
  criticalThreshold: number = 10;
  minBreachCnt: number = 10;
  isAboveThreshold: boolean = true;
  titleIcon: string;
  title: string;
  showDownloadIcon: boolean;
  showSave:boolean = true;

  constructor(
    public dialogRef: MatDialogRef<rumAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogAddEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(){
    if (this.data.uid == undefined) {
      this.titleIcon = "supervisor_account"
      this.title = "Add RUM Module";
    } else {
      this.titleIcon = "edit"
      this.title = "Edit Module Details ("+ this.data.uid +")";
    }
    this.rumName = this.data.modName;
    this.rumDesc = this.data.modDesc;
    this.warnThreshold = this.data.warning;
    this.criticalThreshold = this.data.critical;
    this.isAboveThreshold = true;
    this.minBreachCnt = this.data.breachCount;
    this.createAlertForm();
  }

  createAlertForm(){
    this.form = this.formBuilder.group({
     RumName:[this.rumName, Validators.compose([
       Validators.required,
     ])],
     RumDesc:[this.rumDesc, Validators.compose([
       Validators.required,
     ])],
     WarnThreshold:[this.warnThreshold,Validators.compose([
       Validators.required,
     ])],
     CriticalThreshold:[this.criticalThreshold,Validators.compose([
       Validators.required
     ])],
     MinBreachCount:[this.minBreachCnt,Validators.compose([
       Validators.required,
     ])],
     IsAboveThreshold:[this.isAboveThreshold],
   });	
  }

  getErrorMessage(control) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    return msg;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }

  validateStringEmpty(){
    if (this.data.modName == undefined || this.data.modName.trim() == ''){
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }
  }
  
  async addUpdateRum(){ 
    this.isLoading = true;
    let rumData = {};
    let entItem = {};
    if (sessionStorage.getItem('entItem') != undefined) {
      entItem = JSON.parse(sessionStorage.getItem('entItem'));
    } 
    if (this.form.valid){
      rumData = {
        uid: this.data.uid,
        moduleName: this.form.get('RumName').value.trim(),
        moduleCode: 'RUM',
        description: this.form.get('RumDesc').value.trim(),
        respTimeAlert: true,
        eid: entItem['is_owner'] && entItem['e_id'] > 0 ? entItem['e_id'] : null,
        warning: this.form.get('WarnThreshold').value * 1000, //converting to ms from sec
        critical: this.form.get('CriticalThreshold').value * 1000,
        breachCount: this.form.get('MinBreachCount').value
      }
      let ack = await this.authService.addRUM(rumData)
      this.isLoading = false;
      if (ack.success) {
        if(this.data.uid == undefined) {
          this.data.guid = ack.result[0].add_rum;
        }           
        this.reusable.openAlertMsg(ack.message, "info");
        this.showDownloadIcon = true;
        this.form.disable();
        this.showSave = false;
      } else {
        this.isLoading = false;
        this.reusable.openAlertMsg(this.authService.invalidSession(ack),"error");
      }
    } else {
      this.isLoading = false;
      this.reusable.openAlertMsg('Please enter all required fields.',"error");
    }
  }

  downloadUrl(type) {
    let url = environment.domain+'/downloader/downloadAgent?type='+type+'&guid='+this.data.guid;
    this.authService.downloadAgent(url).then(blob =>{
      saveAs(blob, 'appedo_rum_script.zip');
    });
    //window.open(url,'_blank');
  }
}

/* Dialog screen ts file for SUM-AddEdit-dialog.html */
@Component({
  selector: 'sum-addedit-dialog',
  templateUrl: 'sum-addedit-dialog.html',
})
export class sumAddEditDialog implements OnInit {
  btnDisable = false;
  isLoading: boolean;
  modCode: string;
  form: FormGroup;
  warning: number = 5;
  critical: number = 10;
  minBreachCnt: number = 10;
  isAboveThreshold: boolean = true;
  titleIcon: string;
  title: string;
  showSave:boolean = true;
  testName: string;
  testType: string = "URL";
  testUrl: string;
  startDate: Date = new Date(new Date().getTime()+5*60*1000);
  endDate: Date = new Date(new Date().getTime()+1*60*60*1000+5*60*1000);
  runEvery: number;
  connection: string;
  deviceType: string;
  osType: string;
  browser: string;
  repeatView: boolean = true;
  isActive: boolean = true;
  selectedCities: [];
  testTransaction ="import java.io.*;\nimport java.util.concurrent.*;\nimport java.lang.*;\nimport java.util.logging.Level;\nimport java.net.*;\nimport org.json.*;\nimport org.openqa.selenium.*;\nimport org.openqa.selenium.chrome.*;\nimport org.openqa.selenium.logging.*;\nimport org.openqa.selenium.remote.*;\n";
  testTrans2 = "driver.get(\"http://learn.openmentor.net:8080/openmentor/view/user/\");\nThread.sleep(5000);\ndriver.findElement(By.linkText(\"Recorded Videos\")).click();\nThread.sleep(5000);\ndriver.findElement(By.linkText(\"Very Basics of Software\")).click();\nThread.sleep(5000);\n";
  transStatic ="public class MyWebsite {\n\t public static void main(String args[]) {\n\t\t WebDriver driver = AppedoUtils.getWebDriver();";
  transStatic2 ="\t}\n}";
  script: string;
  allDeviceOsBrowserDet: any;
  sumDeviceTypes: any;
  deviceTypeSelVal: any;
  runEverySelVal: any;
  connectionSelVal: any;
  osSelVal: any;
  browserSelVal: any;
  showNodeSelection: boolean;
  dobIds: any;
  test_id: any;
  nodeList: any;
  countryWise: any;
  selectedCityList=[];
  countrySelVal: any;
  avlCityList = [];
  deviceWiseConnectivities: any;
  testStatus: any;
  runEveryTypes = [{disp:'15 Mins', value:15}, {disp:'30 Mins', value:30}, {disp:'45 Mins', value:45}, {disp:'1 Hr', value:60}, {disp:'2 Hrs', value:120}, {disp:'4 Hrs', value:240},{disp:'8 Hrs', value:480}, {disp:'12 Hrs', value:720}, {disp:'24 Hrs', value:1440}];  sumConnectivities: any;
  osList: any;
  browserList: any;
  maxDateTime:string = new Date(new Date().getTime()+75*24*60*60*1000).toISOString();
  beforeEditStartEndDate: { sDate: Date; eDate: Date; };
  theme:any;

  constructor(
    public dialogRef: MatDialogRef<sumAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogSumAddEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {  
    this.createForm();
  }

  ngOnInit(){
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    
    this.beforeEditStartEndDate ={sDate: new Date(), eDate: new Date(new Date().getTime()+60*60*1000) };
    this.runEverySelVal = this.runEveryTypes.find(x=>x.value==60);
    this.form.get("RunEvery").setValue(this.runEverySelVal);
    this.getSumDeviceTypes();
    this.getSumConnectivity();
    if (this.data.testId == undefined) {
      this.titleIcon = "android"
      this.title = "Add SUM Module";
    } else {
      this.titleIcon = "edit"
      this.title = "Edit Module Details ("+ this.data.testId +")";
      this.loadEditData();
    }
  }

  async loadEditData() {
    let reqData={testId: this.data.testId};
    let respData = await this.authService.getEditSumData(reqData);
    if (respData.success){
      this.populateSumData(respData.result[0]);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(respData),"error");
    }
  }

  populateSumData(sumData) {
    this.startDate = new Date(sumData.start_date);
    this.endDate = new Date(sumData.end_date);
    this.beforeEditStartEndDate = {sDate: new Date(sumData.start_date), eDate: new Date(sumData.end_date)}
    this.runEverySelVal = this.runEveryTypes.find(x=>x.value == sumData.runevery);
    this.connectionSelVal = _find(this.sumConnectivities, {'connection_id': sumData.connection_id});
    this.testStatus = sumData.teststatus;
    this.testType  = sumData.testtype;
    this.testType = sumData.testtype;
    this.testTypeSel(this.testType);
    let dobIds = sumData.dob_ids.split(',');
    this.getSumNodes(dobIds);
    dobIds.forEach( (dobId) => {
      let deviceOsBrowser = _find(this.allDeviceOsBrowserDet, {dob_id : dobId});
      this.osSelVal = deviceOsBrowser['os_name'];
      this.deviceTypeSelVal = deviceOsBrowser['device_type'];
      this.browserSelVal = deviceOsBrowser['browser_name'];
    });
    this.onChangeDevice(this.deviceTypeSelVal);
    this.form.get("Connection").setValue(this.connectionSelVal);
    this.form.get("RunEvery").setValue(this.runEverySelVal);
    this.form.get("StartDate").setValue(this.startDate);
    this.form.get("EndDate").setValue(this.endDate);
    this.form.get("TestName").setValue(sumData.testname);
    this.form.get("TestUrl").setValue(sumData.testurl);
    this.form.get("TestType").setValue(sumData.testtype);
    this.form.get("TestTransaction").setValue(sumData.trasnaction_imports);
    this.form.get("TestTrans2").setValue(sumData.testtransaction);
    this.form.get("TransStatic").setValue(this.transStatic);
    this.form.get("TransStatic2").setValue(this.transStatic2);
    this.form.get("Script").setValue(sumData.script);
    this.form.get("Active").setValue(sumData.status);
    this.form.get("Device").setValue(this.sumDeviceTypes[this.sumDeviceTypes.findIndex(x=>x == this.deviceTypeSelVal)]);
    this.form.get("Os").setValue(this.osList[this.osList.findIndex(x=>x==this.osSelVal)]);
    this.form.get("Browser").setValue(this.browserList[this.browserList.findIndex(x=>x==this.browserSelVal)]);
    this.form.get("Warning").setValue(sumData.warning);
    this.form.get("Critical").setValue(sumData.error);
    this.form.get("MinBreachCnt").setValue(sumData.rm_min_breach_count);
    this.form.get("Country").setValue(this.countrySelVal);
    this.form.controls['TestType'].disable();
    this.form.controls['TestTransaction'].disable();
    this.form.controls['TestTrans2'].disable();
    this.form.controls['TestUrl'].disable();
  }

  testTypeSel(selType){
    this.testType = selType;
    if (this.testType == 'URL' || this.testType == 'SCRIPT') {
      if (this.data.testId == null){
        this.deviceTypeSelVal = this.sumDeviceTypes[0];
        this.connectionSelVal = this.sumConnectivities[0];
        this.form.get("Device").setValue(this.deviceTypeSelVal);
        this.form.get("Connection").setValue(this.connectionSelVal);
      }
    } else {
      if (this.data.testId == null){
        this.connectionSelVal = this.sumConnectivities[10];
        this.form.get("Connection").setValue(this.connectionSelVal);
      }
    }
  }

  createForm() {
		this.form = this.formBuilder.group({
      TestName:[this.testName, Validators.compose([
				Validators.required,
				Validators.minLength(3),
        Validators.maxLength(30),
        this.validateName
      ])],
      TestUrl:[this.testUrl, Validators.compose([
				Validators.minLength(3),
        Validators.maxLength(50),
        this.validateURL
      ])],
      TestType: [this.testType],
      TestTransaction: [this.testTransaction],
      Script: [this.script],
      TransStatic: [{value:this.transStatic, disabled:true}],
      TestTrans2: [this.testTrans2],
      TransStatic2: [{value:this.transStatic2, disabled:true}],
      StartDate:[this.startDate],
      EndDate:[this.endDate],
      RunEvery:[this.runEverySelVal],
      Connection:[this.connectionSelVal],
      Device:[this.deviceTypeSelVal],
      Os: [this.osSelVal],
      Browser:[this.browserSelVal],
      Warning: [this.warning, Validators.compose([
        this.validateNumber
      ])],
      Critical: [this.critical, Validators.compose([
        this.validateNumber
      ])],
      MinBreachCnt: [this.minBreachCnt, Validators.compose([
        this.validateNumber
      ])],
      Active: [this.isActive],
      RepeatView: [this.repeatView],
      IsAboveThreshold: [true],
      Country: [this.countrySelVal],
      SelectedCities: [this.selectedCityList]
     });	
  }

  onToggleSelCities(eleColl){
    let country = this.countrySelVal;
    let filCountry = _filter(this.selectedCityList, function(o){ return o.country != country});
    this.selectedCityList = filCountry;
    if (eleColl.length > 0){
      eleColl.map(row => {
        let idx = _findIndex(this.nodeList,{'country':this.countrySelVal,'city':row.city});
        this.nodeList[idx].is_selected = true;
        this.selectedCityList.push(this.nodeList[idx]);
      });     
    }
  }

  getErrorMessage(control) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    return msg;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }

  async getSumNodes(dobIds) {
    this.showNodeSelection = true;
    if (dobIds == null) {
      this.dobIds = this.allDeviceOsBrowserDet.filter(obj => obj.device_type === this.deviceTypeSelVal && obj.os_name === this.osSelVal && obj.browser_name === this.browserSelVal)
            .map(item => item.dob_id).filter((value, index, self) => self.indexOf(value) === index);
    } else {
      this.dobIds = dobIds;
    }
    let reqData={dobId:this.dobIds.join(), testId: this.data.testId == undefined ? -1 : this.data.testId};
    let respData = await this.authService.getSumNodes(reqData);
    if (respData.success){
      this.nodeList = respData.result;
      this.countryWise = this.nodeList.map(item => item.country)
            .filter((value, index, self) => self.indexOf(value) === index);
      this.selectedCityList = _filter(this.nodeList,['is_selected',true]);
      this.form.get("SelectedCities").setValue(this.selectedCityList);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(respData),"error");
    }
  }

  onCountryChange(country) {
    this.countrySelVal = country;
    this.form.get("SelectedCities").setValue(this.selectedCityList);
  }

  onChangeDevice(changedDevice){
    this.deviceTypeSelVal = changedDevice;
    this.processSumDOBs();
  }

  async getSumDeviceTypes() {
    let sumDeviceTypes = await this.authService.getSumDeviceTypes();
    if (sumDeviceTypes.success){
      this.allDeviceOsBrowserDet = sumDeviceTypes.result;
      this.sumDeviceTypes = this.allDeviceOsBrowserDet.map(item => item.device_type)
          .filter((value: any, index: any, self: { indexOf: (arg0: any) => void; }) => self.indexOf(value) === index);
      this.deviceTypeSelVal = this.sumDeviceTypes[0];
      this.form.get("Device").setValue(this.deviceTypeSelVal);
      this.processSumDOBs();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(sumDeviceTypes),'error');
    }
  }
  
  processSumDOBs() {
    this.osList = this.allDeviceOsBrowserDet.filter(os => os.device_type === this.deviceTypeSelVal)
        .map(item => item.os_name).filter((value, index, self) => self.indexOf(value) === index);
    this.osSelVal = this.osList[0];
    this.form.get("Os").setValue(this.osSelVal);
    this.browserList = this.allDeviceOsBrowserDet.filter(os => os.device_type === this.deviceTypeSelVal && os.os_name === this.osSelVal)
        .map(item => item.browser_name).filter((value, index, self) => self.indexOf(value) === index);
    this.browserSelVal = this.browserList[0];
    this.form.get('Browser').setValue(this.browserSelVal);
    //Getting sum Node for the selection
    if (this.test_id == null)
      this.getSumNodes(null);
  }

  async getSumConnectivity() {
    let sumConnectivity = await this.authService.getSumConnectivity();
    if (sumConnectivity.success){
      this.sumConnectivities = sumConnectivity.result;
      this.connectionSelVal = this.sumConnectivities[0];
      this.form.get("Connection").setValue(this.connectionSelVal);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(sumConnectivity),"error");
    }
  }

  validateName(controls){
		const reqExp = new RegExp(/^[a-zA-Z0-9_ ]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
  }
  
  validateURL(controls){
		const reqExp = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/);
    if ((controls.value == null || controls.value == '') || reqExp.test(controls.value)){
      return null;
		} else{
			return { 'validateURL' : true};
		}
  }

  validateNumber(controls){
    const reqExp = new RegExp(/^[0-9]+$/);
		if (reqExp.test(controls.value)){
			return null;
		} else {
			return { 'validateNumber' : true};
		}
  }

  changeDateTime(){
    let stDateTime;
    let endDateTime;
    this.startDate = this.form.get("StartDate").value;
    this.endDate = this.form.get("EndDate").value;
    if (this.startDate == undefined) {
      this.startDate = new Date(endDateTime-1*60*60*1000); 
    } else {
      stDateTime = new Date(this.startDate).getTime();
      if (stDateTime < new Date().getTime() && this.data.testId == null) {
        this.startDate = new Date(stDateTime); 
        this.form.get("StartDate").setValue(this.startDate);
        this.reusable.openAlertMsg("Start date cannot be earlier than current date and time","error"); 
        return false;
      } else if (this.beforeEditStartEndDate.sDate != undefined && this.data.testId != null){
        if(stDateTime < this.beforeEditStartEndDate.sDate.getTime()) {
          this.startDate = this.beforeEditStartEndDate.sDate;
          this.form.get("StartDate").setValue(this.startDate);
          this.reusable.openAlertMsg("Start date cannot be earlier than start date and time","error"); 
          return false;
        }
      }
    }
    if (this.endDate == undefined) return false;
    else {
      endDateTime = new Date(this.endDate).getTime();
      if (endDateTime < stDateTime && this.data.testId == null) {
        this.endDate = new Date(stDateTime + 1*60*60*1000); 
        endDateTime = new Date(this.endDate).getTime();
        this.form.get("EndDate").setValue(this.endDate);
        this.reusable.openAlertMsg("End date cannot be earlier than start date", 'error'); 
        return false;
      } else if (this.data.testId !=null) {
        if (endDateTime != this.beforeEditStartEndDate.eDate.getTime() && endDateTime < new Date().getTime()+30*60*1000){
          this.endDate = this.beforeEditStartEndDate.eDate;
          this.form.get("EndDate").setValue(this.endDate);
          endDateTime = new Date(this.endDate).getTime();
          this.reusable.openAlertMsg("Edited end date has to be atleast 30 min latter from now", 'error'); 
          return false;
        } else if (endDateTime <= stDateTime) {
          this.endDate = new Date(stDateTime+1*60*60*1000);
          this.form.get("EndDate").setValue(this.endDate);
          this.reusable.openAlertMsg("End date cannot be less than start date.","error");
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  }

  async addUpdateSum() {
    var allowSave = true;
    let sumData = {};
    let entItem={};
    if (sessionStorage.getItem('entItem') != undefined) {
      entItem = JSON.parse(sessionStorage.getItem('entItem'));
    } 
    allowSave = this.changeDateTime();
    if (this.testType == 'TRANSACTION' && (this.form.get("TestTrans2") == null || this.form.get("TestTrans2").value == null || this.form.get("TestTrans2").value == '' || this.form.get('TestTransaction') == null || this.form.get('TestTransaction').value == null ||this.form.get('TestTransaction').value == '') ){
      this.reusable.openAlertMsg("Must have Transaction configured", "error");
      return;
    }
    if (this.testType == 'SCRIPT' && (this.form.get("Script") == null || this.form.get("Script").value == null || this.form.get("Script").value == '')){
      this.reusable.openAlertMsg("Must have Script configured", "error");
      return;
    }
    if (this.testType == 'URL' && (this.form.get("TestUrl") == null || this.form.get("TestUrl").value == null || this.form.get("TestUrl").value == '')){
      this.reusable.openAlertMsg("Must have URL configured", "error");
      return;
    }
    if (this.selectedCityList.length == 0) {
      allowSave=false;
      this.reusable.openAlertMsg('Select city nodes to continue.',"error");
    } else if(_filter(this.selectedCityList,['can_agent_accept_test',false]).length > 0) {
      allowSave=false;
      this.reusable.openAlertMsg('Select active city nodes.',"error");
    } else {
      sumData = {
        test_id : this.data.testId != undefined ? this.data.testId : null,
        testname: this.form.get('TestName').value.trim(),
        testtype: this.form.get('TestType').value.trim(),
        testurl: this.testType == 'URL' ? this.form.get('TestUrl').value : '',
        e_id: entItem['is_owner'] && entItem['e_id'] > 0 ? entItem['e_id'] : null,
        trasnaction_imports: this.testType == 'TRANSACTION' ? this.form.get('TestTransaction').value : null,
        testtransaction: this.testType == 'TRANSACTION' ? this.form.get('TestTrans2').value : null,
        script: this.testType == 'SCRIPT'? this.form.get('Script').value : null,
        start_date : new Date(this.form.get('StartDate').value).getTime(),
        end_date : new Date(this.form.get('EndDate').value).getTime(),
        runevery : this.form.get('RunEvery').value['value'],
        status : this.form.get('Active').value,
        connection_id : this.form.get('Connection').value['connection_id'],
        download : this.form.get('Connection').value['download'],
        upload : this.form.get('Connection').value['upload'],
        latency : this.form.get('Connection').value['latency'], 
        packet_loss : this.form.get('Connection').value['packet_loss'],
        resp_time_alert : true,
        sms_alert : true,
        email_alert: true,
        warning: this.form.get('Warning').value,
        error : this.form.get('Critical').value,
        rm_min_breach_count : this.form.get('MinBreachCnt').value,
        repeat_view : this.form.get('RepeatView').value,
        last_run_detail: new Date().getTime() - (this.form.get('RunEvery').value['value']) * 60 * 1000,
        dob_id : this.testType == 'URL' ? this.dobIds.join() : 1,
        selected_cities : this.processSumNodes()
      } 
      if (allowSave) {
        this.isLoading = true;
        let ack = await this.authService.addUpdateSum(sumData);
        this.isLoading = false;
        if (ack.success) {
          this.reusable.openAlertMsg(ack.message, "info");
          this.closeWithReturn();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(ack), "error");
        }
      } else {
        //this.reusable.openAlertMsg('Fill all mandatory fields',"error");
      }
    }
  }

  processSumNodes() {
    let nodeList:String[] = [];
    this.selectedCityList.forEach(obj => {
      nodeList.push(obj.country+'--'+obj.city);
    });
    return nodeList.join();
  }

}

/**
 * For pipe filter at html level the below code is added. 
 * currently used for nodeList variable
*/
@Pipe({
  name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
  transform(items: any[], field : string, value : string): any[] {  
    if (!items) return [];
    // if (!value || value.length == 0) return items;
    if (!value || value.length == 0) return [];
    return items.filter(it => 
    it[field].toLowerCase().indexOf(value.toLowerCase()) !=-1);
  }
}

/* Dialog screen ts file for AVM-AddEdit-dialog.html */
@Component({
  selector: 'avm-addedit-dialog',
  templateUrl: 'avm-addedit-dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class avmAddEditDialog implements OnInit {
  objectKeys = Object.keys; //used in display of table content json objec to html table
  objectValues = Object.values; //used in display of table content json objec to html table

  runEveryTypes = [1,3,5,10,15,30,60];
  beforeEditStartEndDate: { sDate: Date; eDate: Date; };
  theme: string;
  title: string;
  titleIcon: string;
  reqMethodColl = ['GET','POST','HEAD','PUT','WSDL'];
  max: Date;
  min = new Date();
  avmTestId: string;
  startDate: Date = new Date(new Date().setMinutes(new Date().getMinutes()+5));
  endDate: Date = new Date(new Date().setHours(23,59,59));
  arrReqHeader = [];
  isPostOrPut = false;
  reqHeadCollapse = false;
  reqParamCollapse = false;
  isWSDL = false;
  isLoading: boolean;
  startTime: any;
  endTime: any;
  nodeList = [];
  countryWise = [];
  selectedCityList = [];
  countrySelVal: any;
  bodyType ='raw';
  contentTypeColl = ['text/plain','application/json','application/xml', 'text/xml', 'text/html'];
  isBodyDisabled = true;
  authorizationColl = ['No Auth','Basic Auth','JWT Token'];
  data:any;
  screenParam: any;
  headerTotal:string;
  hide = true;
  setIntervalId:any;
  selectedSubTabIdx = 0;
  respContent: any;
  selectedTabIndex: number = 0;
  respTime: number;
  bandwidth: any;

  constructor(
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
    private router: Router,
    private cd: ChangeDetectorRef,
  ) {}

  form = this.formBuilder.group({
    TestName: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    IsActive: [true],
    TestUrl: ['', Validators.compose([
      Validators.required,
      this.validateURL
    ])],
    ReqMethod:[this.reqMethodColl[0], Validators.required],
    TestHeadMethodFirst: [true],
    StartDate: [this.startDate, Validators.required],
    EndDate: [this.endDate, Validators.required],
    BodyType:[this.bodyType, Validators.required],
    ContentType:['*', Validators.required],
    Authorization:[this.authorizationColl[0], Validators.required],
    UserName:[null],
    Password:[null],
    Token: [null],
    ReqBody: [''],
    RunEvery: [this.runEveryTypes[2]],
    StartTime: [null],
    EndTime: [null],
    MinBreachCnt: [1, Validators.compose([
      Validators.required,
      this.validateNumber
    ])],
    ReqHeaders: this.formBuilder.array([]),
    ReqParams: this.formBuilder.array([]),
    Variables: this.formBuilder.array([]),
    Country: [this.countrySelVal],
    SelectedCities: [this.selectedCityList]
  })

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
  }

  ngOnInit(){
    this.data = JSON.parse(sessionStorage.getItem("modAvmParam"));
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    this.max =new Date(this.min);
    this.max.setMonth(this.min.getMonth()+6);
    this.avmTestId = this.data.testId;
    this.startDate = new Date();
    this.endDate = new Date(this.startDate);
    this.endDate.setHours(this.startDate.getHours()+24);
    this.beforeEditStartEndDate = {sDate: new Date(), eDate: new Date(new Date().getTime() + 60*60*1000) };
    this.selectLocation();
    if (this.data.testId == undefined) {
      this.titleIcon = "favorite"
      this.title = "Add AVM Test";
      this.arrReqHeader[0] = {name:'Accept',value:'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'};
      this.arrReqHeader[1] = {name:'Accept-Language',value:'en-US,en;q=0.5'};
      this.arrReqHeader[2] = {name:'Connection',value:'close'};
      this.arrReqHeader[3] = {name:'User-Agent',value:'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0'};
      this.addDefaultReqHeaders();
    } else {
      this.titleIcon = "edit"
      this.title = "Edit AVM Test Details ("+ this.data.testId +")";
      //this.loadEditData();
    }
    this.setIntervalId = setInterval(()=>{
      this.cd.detectChanges();
    },2000);
  }

  getReqHeadersControls(){
    return (this.form.get('ReqHeaders') as FormArray).controls;
  }

  getReqParamsControls(){
    return (this.form.get('ReqParams') as FormArray).controls;
  }

  onChangeBodyType(bodyType){
    if (bodyType == "raw"){
      if (this.form.get('ContentType').value == null){
        this.form.get('ContentType').setValue(this.contentTypeColl[1])
      }
      this.onChangeContentType(this.form.get('ContentType').value);
    } else {
      this.removeReqHeader("Content-Type");
      this.reqHeaders.push(this.formBuilder.group({
        Name:['Content-Type', Validators.required],
        Value: ['application/x-www-form-urlencoded', Validators.required]
      }));
      if (this.reqParam.length == 0){
        this.addArrReqParam();
        this.reqParamCollapse = false;
      }
      if(this.form.get('ReqBody').hasError("required")){
        this.form.get('ReqBody').setErrors(null);
      }
    }
  }

  onChangeContentType(contentType){
    this.removeReqHeader("Content-Type");
    if (contentType.includes('xml')){
      //this.form.get('ReqBody').setValidators([Validators.required,this.validateXML, Validators.maxLength(6000)]);
      this.form.get('ReqBody').setValidators([Validators.required,this.validateXML]);
      this.form.get('ReqBody').setValue(null);
    } else if (contentType.includes('json')){
      this.form.get('ReqBody').setValidators([Validators.required,this.validateJson, Validators.maxLength(6000)]);  
      this.form.get('ReqBody').setValue(null);
    } else {
      this.form.get('ReqBody').setValidators([Validators.required,, Validators.maxLength(6000)]);  
      this.form.get('ReqBody').setValue(null);
    }
    this.reqHeaders.push(this.formBuilder.group({
      Name:['Content-Type', Validators.required],
      Value: [contentType, Validators.required]
    }));
    this.form.get('ReqBody').markAsDirty();
  }

  updateReq(){
    this.removeReqHeader("Authorization");
    if (this.form.get("Authorization").value == "No Auth"){
      this.form.get("UserName").setValue(null);
      this.form.get("Password").setValue(null);
      this.form.get("Token").setValue(null);
    } else if (this.form.get("Authorization").value == "Basic Auth"){
      if (this.form.get("UserName").value == null || this.form.get("UserName").value == ''){
        this.reusable.openAlertMsg("For Basic Authentication, username is mandatory","error");
        return ;
      } else if (this.form.get("Password").value == null || this.form.get("Password").value == ''){
        this.reusable.openAlertMsg("For Basic Authentication, password is mandatory","error");
        return;
      }
      this.form.get("Token").setValue(null);
      let auth = "Basic " + new Buffer(this.form.get("UserName").value+':'+this.form.get("Password").value).toString("base64");
      this.reqHeaders.push(this.formBuilder.group({
        Name:['Authorization', Validators.required],
        Value: [auth, Validators.required]
      }));
    } else {
      if (this.form.get("Token").value == null || this.form.get("Token").value == ''){
        this.reusable.openAlertMsg("For JWT Token, token is mandatory","error");
        return;
      }
      this.form.get("UserName").setValue(null);
      this.form.get("Password").setValue(null);
      this.reqHeaders.push(this.formBuilder.group({
        Name:['Authorization', Validators.required],
        Value: [this.form.get("Token").value, Validators.required]
      }));
    }
  }

  removeReqHeader(headerName:string){
    let length = this.reqHeaders.length;
    let i = 0;
    while (i < length){
      if (this.reqHeaders.controls[i] != undefined){
        if (this.reqHeaders.controls[i].get("Name").value == headerName){
          this.reqHeaders.removeAt(i);
          length = this.reqHeaders.length;
          i = 0;
        } else {
          i++;
        }
      } else {
        i++;
      }
    }
  }

  getErrorMessage(control, controlName) {
    let msg =' ';
    msg += control.hasError('required') ? 'You must enter a value. ' :'';
    if (controlName =='TestName') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='TestName') {msg += control.hasError('validateName') ? 'AlphaNumeric,Space,_,- are allowed. ' :''}
    if (controlName =='TestUrl') {msg += control.hasError('validateURL') ? 'Not a valid URL. ' :''}
    if (controlName =='MinBreachCnt') {msg += control.hasError('validateNumber') ? 'Must be a Number. ' :''}
    if (controlName =='ReqBody') {msg += control.hasError('validateJson') ? 'Must be a valid Json String. ' :''}
    if (controlName =='ReqBody') {msg += control.hasError('validateXML') ? 'Must be a valid XML String. ' :''}
    if (controlName =='ReqBody') {msg += (control.errors != undefined && control.errors.maxlength) ? 'Must be less than 6000 char length. ': ''}
    return msg;
  }

  get variables(){
    return this.form.get('Variables') as FormArray;
  }

  addArrVariables(){
    this.variables.push(this.formBuilder.group({
      Name:['', Validators.required],
      Script: ['', Validators.required],
      Result:['',[Validators.required, this.EvalError]],
    }));
  }

  EvalError(controls){

  }
  evaluate(idx){
    try {
      let res = eval(this.variables.controls[idx].get('Script').value);
      this.variables.controls[idx].get('Result').setValue(res);
      this.variables.controls[idx].setErrors(null)
    } catch(e){
      this.variables.controls[idx].get('Result').setValue(e.message);
      this.variables.controls[idx].setErrors({'EvalError':true});
    }
  }

  removeVariableElement(idx){
    this.variables.removeAt(idx);
  }

  get reqParam(){
    return this.form.get('ReqParams') as FormArray;
  }

  addArrReqParam(){
    this.reqParam.push(this.formBuilder.group({
      ParamName:['', Validators.required],
      ParamValue: ['', Validators.required]
    }));
  }

  removeReqParamElement(idx){
    this.reqParam.removeAt(idx);
  }

  onChangeReqMethod(method, mode?:string){
    this.form.get('ReqBody').setValidators(null);
    this.form.get("ReqBody").setValue(null);
    if (method == "POST" || method == 'PUT') {
      this.selectedSubTabIdx = 2;
      this.isPostOrPut = true;
      this.reqHeadCollapse = true;
      this.isWSDL = false;
      this.isBodyDisabled = false;
      if (mode == undefined){
        this.form.get("ContentType").setValidators(Validators.required);
        this.form.get("ContentType").setValue(this.contentTypeColl[1]);
        this.form.get('ReqBody').setValidators([Validators.required,this.validateJson,Validators.maxLength(6000)]);
        this.form.get("BodyType").enable();
        this.onChangeContentType(this.contentTypeColl[1]);
      }
    } else if (method == "WSDL") {
      this.selectedSubTabIdx = 2;
      this.isWSDL = true;
      this.isPostOrPut = false;
      this.reqHeadCollapse = true;
      this.isBodyDisabled = false;
      if (mode == undefined){
        this.onChangeContentType(this.contentTypeColl[3]);
        this.form.get("ContentType").setValidators(Validators.required);
        this.form.get('ReqBody').setValidators([Validators.required,this.validateXML,Validators.maxLength(6000)]);
        this.form.get("BodyType").setValue('raw');
        this.form.get("BodyType").disable();
      }
    } else {
      this.selectedSubTabIdx = 1;
      this.isWSDL = false;
      this.isPostOrPut = false;
      this.reqHeadCollapse = false;
      this.isBodyDisabled = true;
      if (mode == undefined){
        this.removeReqHeader("Content-Type");
        this.form.get("ContentType").setValidators(null);
      }
    }
  }

  async sendReq(){
    this.isLoading = true;
    this.respTime = undefined;
    this.bandwidth = undefined;
    let reqMethod = this.form.get('ReqMethod').value;
    let reqHeaders = {};
    let reqParams = {};
    let bodyType = null;
    if (this.form.get("ReqMethod").value == "GET" || this.form.get("ReqMethod").value == "HEAD" ){
      bodyType = null;
    } else {
      bodyType = this.form.get("BodyType").value;
    }
    let headerLength = this.reqHeaders.length;
    for (let i = 0; i< headerLength; i++){
      reqHeaders[this.reqHeaders.controls[i].get('Name').value] = this.reqHeaders.controls[i].get('Value').value;
    }
    
    let param = {
      url: this.form.get('TestUrl').value,
      headers: JSON.stringify(reqHeaders),
      bodyType: bodyType,
      method: reqMethod == 'WSDL' ? 'POST' : reqMethod
    }
    if (reqMethod == 'POST' || reqMethod == 'PUT'){
      if(this.form.get('BodyType').value == "raw" ){
        if (this.form.get('ReqBody').value != null && this.form.get('ReqBody').value != ''){
          param['body'] = this.form.get('ReqBody').value;
          let contentType = this.form.get("ContentType").value;
          param['contentType'] = contentType.includes('json') ? 'json' : 'text';
        } else {
          this.reusable.openAlertMsg("For Post Method and body type raw, Body cannot be empty","error");
          this.isLoading = false;
          return;
        }
      } else {
        let paramLength = this.reqParam.length;
        if (paramLength == 0){
          this.reusable.openAlertMsg("For Post and body type form-data, Parameter must be defined", "error");
          this.isLoading = false;
          return;
        }
        let paramExist = false;
        for (let i = 0; i< paramLength; i++){
          let paramName = this.reqParam.controls[i].get('ParamName').value;
          let paramVal = this.reqParam.controls[i].get('ParamValue').value;
          if (paramName != "" && paramVal != "" && paramName != undefined && paramVal != undefined){
            reqParams[paramName] = paramVal;
            paramExist = true;
          }
        }
        if (!paramExist){
          this.reusable.openAlertMsg("For Post and body type form-data, Parameter must be defined", "error");
          this.isLoading = false;
          return;
        } else {
          param['body'] = JSON.stringify(reqParams);
        }
      }
    } else if (reqMethod == 'WSDL'){
      if (this.form.get('ReqBody').value != null && this.form.get('ReqBody').value != ''){
        param['body'] = this.form.get('ReqBody').value;
      } else {
        this.reusable.openAlertMsg("For WSDL Method, Body cannot be empty","error");
        this.isLoading = false;
        return;
      }
    }
    let varLength = this.variables.length;
    let variables = {};
    for (let i = 0; i< varLength; i++){
      variables[this.variables.controls[i].get('Name').value] = eval(this.variables.controls[i].get('Script').value);
    }
    if (varLength > 0){
      let strParam = JSON.stringify(param);
      let varKeys = Object.keys(variables);
      varKeys.map(key => {
        let vKey = '@@'+key+'@@';
        strParam = strParam.replace(vKey,variables[key]);
      });
      param = JSON.parse(strParam)
    }
    let startTime = new Date().getTime();
    this.respContent = await this.authService.getURL(param);
    this.respTime = new Date().getTime() - startTime
    this.isLoading = false;
    if (this.respContent.invalidToken){
      this.authService.invalidSession(this.respContent);
    } else {
      if (this.respContent.success){
        if (varLength >0)
          this.respContent['reqVariables'] = variables;
        if (this.respContent.headers['content-length'] != undefined){
          let contLength = this.respContent.headers['content-length'][0];
          this.bandwidth = (Math.round(Number(contLength)/Number(this.respTime)*100)/100).toString();
          this.bandwidth += this.respContent.headers['accept-ranges'] == undefined ? ' bytes per sec' :' '+ this.respContent.headers['accept-ranges'][0]+' per sec';
        } else {
          this.bandwidth = 'NA';
        }
      }
      this.selectedTabIndex = 2;
    }
  }

  addDefaultReqHeaders(){
    this.arrReqHeader.map(head =>{
      this.reqHeaders.push(this.formBuilder.group({
        Name:[head.name, Validators.required],
        Value: [head.value, Validators.required]
      }));
    })
  }

  get reqHeaders(){
    return this.form.get('ReqHeaders') as FormArray;
  }

  addArrReqHeader(){
    this.reqHeaders.push(this.formBuilder.group({
      Name:['', Validators.required],
      Value: ['', Validators.required]
    }));
  }

  removeReqHeaderElement(idx){
    this.reqHeaders.removeAt(idx);
  }
 
  changeDateTime(){
    let stDateTime;
    let endDateTime;
    this.startDate = this.form.get("StartDate").value;
    this.endDate = this.form.get("EndDate").value;
    if (this.startDate == undefined) {
      this.startDate = new Date(endDateTime-1*60*60*1000); 
    } else {
      stDateTime = new Date(this.startDate).getTime();
      if (stDateTime < new Date().getTime() && this.data.testId == null) {
        this.startDate = new Date(stDateTime); 
        this.form.get("StartDate").setValue(this.startDate);
        this.reusable.openAlertMsg("Start date cannot be earlier than current date and time","error"); 
        return false;
      } else if (this.beforeEditStartEndDate.sDate != undefined && this.data.testId != null){
        if(stDateTime < this.beforeEditStartEndDate.sDate.getTime()) {
          this.startDate = this.beforeEditStartEndDate.sDate;
          this.form.get("StartDate").setValue(this.startDate);
          this.reusable.openAlertMsg("Start date cannot be earlier than start date and time","error"); 
          return false;
        }
      }
    }
    if (this.endDate == undefined) return false;
    else {
      endDateTime = new Date(this.endDate).getTime();
      if (endDateTime < stDateTime && this.data.testId == null) {
        this.endDate = new Date(stDateTime + 1*60*60*1000); 
        endDateTime = new Date(this.endDate).getTime();
        this.form.get("EndDate").setValue(this.endDate);
        this.reusable.openAlertMsg("End date cannot be earlier than start date", 'error'); 
        return false;
      } else if (this.data.testId !=null) {
        if (endDateTime != this.beforeEditStartEndDate.eDate.getTime() && endDateTime < new Date().getTime()+30*60*1000){
          this.endDate = this.beforeEditStartEndDate.eDate;
          this.form.get("EndDate").setValue(this.endDate);
          endDateTime = new Date(this.endDate).getTime();
          this.reusable.openAlertMsg("Edited end date has to be atleast 30 min latter from now", 'error'); 
          return false;
        } else if (endDateTime <= stDateTime) {
          this.endDate = new Date(stDateTime+1*60*60*1000);
          this.form.get("EndDate").setValue(this.endDate);
          this.reusable.openAlertMsg("End date cannot be less than start date.","error");
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  }

  validateXML(controls){
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(controls.value, "text/xml");
    let parsererrorColl = xmlDoc.getElementsByTagName("parsererror");
    if (parsererrorColl.length>0){
      return {'validateXML': true}
    } else {
      return null;
    }
  }

  validateJson(controls){
    try{
      let jsonParse = JSON.parse(controls.value);
      return null;
    } catch {
      return {'validateJson': true}
    }
  }
  validateName(controls){
		const reqExp = new RegExp(/^[a-zA-Z0-9 \-_\&]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
  }

  validateNumber(controls){
		const reqExp = new RegExp(/^[0-9]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateNumber' : true};
		}
  }

  validateURL(controls){
		const reqExp = new RegExp(/^https?:\/\/[\w.?-]+(?:\.?[\w\.?-]+)+[\w\-\.?_~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateURL' : true};
		}
  }

  async saveAVM(){
    if (this.selectedCityList.length == 0){
      this.reusable.openAlertMsg("Atleast one city must be selected to proceed","error");
      return;
    }
    let entItem = JSON.parse(sessionStorage.getItem('entItem'))
    this.startTime = this.form.get('StartTime').value;
    this.endTime = this.form.get('EndTime').value;
    let arrReqParam = [];
    let arrReqHeader = [];
    let bodyType = null;
    if (this.form.get("ReqMethod").value == "GET" || this.form.get("ReqMethod").value == "HEAD" ){
      bodyType = null;
    } else {
      bodyType = this.form.get("BodyType").value;
    }
    if (this.form.get("ReqMethod").value == 'POST' || this.form.get("ReqMethod").value == 'PUT'){
      for (let i=0; i<this.reqParam.length; i++){
        arrReqParam.push({name: this.reqParam.controls[i].get("ParamName").value, value:this.reqParam.controls[i].get("ParamValue").value });
      }
    }
    for (let i=0; i<this.reqHeaders.length; i++){
      arrReqHeader.push({name: this.reqHeaders.controls[i].get("Name").value, value:this.reqHeaders.controls[i].get("Value").value });
    }
    let authType = this.form.get("Authorization").value;
    let authParam = null;
    if (authType == 'Basic Auth'){
      authParam = {user_name:this.form.get('UserName').value, password: this.form.get('Password').value};
    } else if (authType == 'JWT Token'){
      authParam = {jwttoken: this.form.get('Token').value};
    }
    let varLength = this.variables.length;
    let variables = {};
    for (let i = 0; i< varLength; i++){
      variables[this.variables.controls[i].get('Name').value] = this.variables.controls[i].get('Script').value;
    }
    this.isLoading = true;
    let avmData = {
      avm_test_id: this.data.testId,
      testname: this.form.get('TestName').value,
      testurl: this.form.get('TestUrl').value,
      start_date: new Date(this.form.get('StartDate').value).getTime(),
      end_date: new Date(this.form.get('EndDate').value).getTime(),
      is_active: this.form.get('IsActive').value,
      frequency: this.form.get('RunEvery').value,
      start_time: this.form.get("StartTime").value == '' || this.form.get("StartTime").value == undefined ? null :this.form.get("StartTime").value,
      end_time:this.form.get("EndTime").value == '' || this.form.get("EndTime").value == undefined ? null :this.form.get("EndTime").value,
      request_method: this.form.get("ReqMethod").value,
      test_head_method_first: this.form.get("TestHeadMethodFirst").value,
      request_headers: JSON.stringify(arrReqHeader),
      request_parameters: JSON.stringify(arrReqParam),
      request_body: this.form.get('ReqBody').value == null ? '' : this.form.get('ReqBody').value,
      min_breach_count: this.form.get('MinBreachCnt').value == undefined ? 0 : this.form.get('MinBreachCnt').value,
      eId : entItem != undefined && entItem.e_id != undefined && entItem.e_id !=0 && entItem.is_owner ? entItem.e_id : null,
      selectedCities: JSON.stringify(this.selectedCityList),
      authorize_type: authType,
      authorize_param: authParam,
      req_body_type: bodyType,
      variables: varLength > 0 ? variables : null
    };
    let result = await this.authService.saveAVM(avmData);
    this.isLoading = false;
    if(result.success){
      this.authService.updAVMAgents();
      this.reusable.openAlertMsg(result.message,"info");
      this.closeWithReturn();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  onCountryChange(country) {
    this.countrySelVal = country;
    this.form.get("SelectedCities").setValue(this.selectedCityList);
  }

  onToggleSelCities(eleColl){
    let country = this.countrySelVal;
    let filCountry = _filter(this.selectedCityList, function(o){ return o.country != country});
    this.selectedCityList = filCountry;
    if (eleColl.length > 0){
      eleColl.map(row => {
        let idx = _findIndex(this.nodeList,{'country':this.countrySelVal,'city':row.city});
        this.nodeList[idx].is_selected = true;
        this.selectedCityList.push(this.nodeList[idx]);
      });     
    }
  }

  async selectLocation(){
    this.isLoading = true;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let entData = {
      entUserId : entItem.owner_id,
      entId : entItem.e_id == 0 ? null : entItem.e_id 
    };

    let result = await this.authService.getAVMLocation(entData);
    this.isLoading = false;
    if (result.success){
      result.result.map(res =>{
        let location = res.location.split("#");
        res['country'] = location[0];
        res['state'] = location[1];
        res['city'] = location[2];
        res['region'] = location[3];
        res['zone'] = location[4];
        res['status'] = new Date(res.appedo_received_on).getTime() <= (new Date().getTime() - 10*60*1000) ? 'down':'active';
      });
      this.nodeList = result.result;
      this.countryWise = this.nodeList.map(item => item.country)
            .filter((value, index, self) => self.indexOf(value) === index);
      this.selectedCityList = _filter(this.nodeList,['is_selected',true]);
      this.form.get("SelectedCities").setValue(this.selectedCityList);
      if (this.data.testId != undefined) {
        this.loadEditData();
      }
      
    }
  }

  onClose(): void {
    this.router.navigate(['/home/extmonitors']);
    // this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.router.navigate(['/home/extmonitors']);
    // this.dialogRef.close(this.data);
  }

  async loadEditData(){
    this.isLoading = true;
    let avmTestData = {
      avmTestId : this.avmTestId
    }
    let res = await this.authService.getAVMTestLocByTestId(avmTestData).toPromise();
    if (res.success){
      let selectedCities = res.result;
      selectedCities.map(city => {
        city.country = city.country.toLowerCase();
        city.state = city.state == ''? '' : city.state.toLowerCase();
        city.region = city.region == ''? '' : city.region.toLowerCase();
        city.zone = city.zone == ''? '' : city.zone.toLowerCase();
        city.city = city.city == ''? '' : city.city.toLowerCase();
      });
      selectedCities.map(city => {
        let nodeIdx = this.nodeList.findIndex(x=>x.country == city.country && x.state == city.state && x.region == city.region && x.zone == city.zone && x.city == city.city);
        if (nodeIdx >=0 ){
          this.nodeList[nodeIdx].is_selected = true;
        }
      });
      this.selectedCityList = _filter(this.nodeList,['is_selected',true]);
      this.form.get("SelectedCities").setValue(this.selectedCityList);
      if (this.selectedCityList.length >= 1){
        this.form.get("Country").setValue(this.countryWise[this.countryWise.findIndex(x=>x==this.selectedCityList[0].country)]);
        this.countrySelVal = this.selectedCityList[0].country;
      }
    } else {
      this.selectedCityList = [];
      this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
    };
    let avmData={avm_test_id: this.avmTestId};
    let result =  await this.authService.getAvmEditById(avmData);
    this.isLoading = false;
    if (result.success){
      let avmEditResult = result.result;
      this.form.get("StartDate").setValue(new Date(avmEditResult.start_date));
      this.form.get("EndDate").setValue(new Date(avmEditResult.end_date));
      this.form.get("ReqMethod").setValue(this.reqMethodColl[this.reqMethodColl.findIndex(x=>x == avmEditResult.request_method)]);
      this.onChangeReqMethod(this.form.get("ReqMethod").value,'edit');
      let arrReqHeader = avmEditResult.request_headers;
      let arrReqParam = avmEditResult.request_parameters;
      this.form.get("RunEvery").setValue(this.runEveryTypes[this.runEveryTypes.findIndex(x=>x == avmEditResult.frequency)]);
      this.form.get("StartTime").setValue(avmEditResult.start_time);
      this.form.get("EndTime").setValue(avmEditResult.end_time);
      this.form.get("TestName").setValue(avmEditResult.testname);
      this.form.get("TestUrl").setValue(avmEditResult.testurl);
      this.bodyType = avmEditResult.req_body_type == null ? 'raw' :avmEditResult.req_body_type;
      this.form.get('BodyType').setValue(this.bodyType);
      this.form.get("MinBreachCnt").setValue(avmEditResult.min_breach_count);
      this.form.get('Authorization').setValue(this.authorizationColl[this.authorizationColl.findIndex(x=>x == avmEditResult.authorize_type)]);
      if (avmEditResult.authorize_type == 'Basic Auth'){
        this.form.get("UserName").setValue(avmEditResult.authorize_param['user_name']);
        this.form.get("Password").setValue(avmEditResult.authorize_param['password']);
      } else if (avmEditResult.authorize_type == 'JWT Token') {
        this.form.get("Token").setValue(avmEditResult.authorize_param['jwttoken']);
      }
      arrReqHeader.map(head =>{
        this.reqHeaders.push(this.formBuilder.group({
          Name:[head.name, Validators.required],
          Value: [head.value, Validators.required]
        }));
        if (head.name == "Content-Type" && this.bodyType =="raw"){
          let splitCT = head.value.split(';');
          splitCT.map(ct =>{
            if (this.form.get("ContentType").value == undefined || this.form.get("ContentType").value == '*' ){
              this.form.get("ContentType").setValue(this.contentTypeColl[this.contentTypeColl.findIndex(x=>x.includes(ct))]);
            } 
          });
          if (this.form.get("ContentType").value != undefined){
            this.onChangeContentType(this.form.get("ContentType").value);
          }
        }
      })
      arrReqParam.map(param =>{
        this.reqParam.push(this.formBuilder.group({
          ParamName:[param.name, Validators.required],
          ParamValue: [param.value, Validators.required]
        }));
      })
      if (avmEditResult.variables != null){
        let variables = avmEditResult.variables;
        let varKeys = Object.keys(variables);
        varKeys.map(key =>{
          this.variables.push(this.formBuilder.group({
            Name: [key, Validators.required],
            Script: [variables[key], Validators.required],
            Result: [eval(variables[key]), [Validators.required, this.EvalError]]
          }));
        })
      }
      this.form.get("ReqBody").setValue(avmEditResult.request_body);
    } else {
      this.authService.invalidSession(result);
    }
  }
}

/* Dialog screen ts file for AVL-AddEdit-dialog.html */
@Component({
  selector: 'avl-add-dialog',
  templateUrl: 'avl-add-dialog.html',
})
export class avlAddEditDialog implements OnInit {
  isLoading = false;
  form = this.formBuilder.group({
    Country: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    State: ['', Validators.compose([
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    Region: ['', Validators.compose([
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    Zone: ['', Validators.compose([
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    City: ['', Validators.compose([
      Validators.minLength(3),
      Validators.maxLength(50),
      this.validateName
    ])],
    IsPrivate:[true]
  });
  theme: string;
  titleIcon: string;
  title: string;

  constructor(
    public dialogRef: MatDialogRef<avlAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogSumAddEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {}

  

  ngOnInit(){
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    if (this.data.testId == undefined) {
      this.titleIcon = "language"
      this.title = "Add Availability Location";
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }

  getErrorMessage(control, controlName) {
    let msg =' ';
    msg += control.hasError('required') ? 'You must enter a value. ' :'';
    if (controlName =='Country') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='Country') {msg += control.hasError('validateName') ? 'Only Characters are allowed. ' :''}
    if (controlName =='City') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='City') {msg += control.hasError('validateName') ? 'Only Characters are allowed. ' :''}
    if (controlName =='State') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='State') {msg += control.hasError('validateName') ? 'Only Characters are allowed. ' :''}
    if (controlName =='Region') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='Region') {msg += control.hasError('validateName') ? 'Only Characters are allowed. ' :''}
    if (controlName =='Zone') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 50 char length. ': ''}
    if (controlName =='Zone') {msg += control.hasError('validateName') ? 'Only Characters are allowed. ' :''}
    return msg;
  }

  validateName(controls){
		const reqExp = new RegExp(/^[a-zA-Z]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
  }

  async saveAVLoc(){
    if (this.form.valid){
      this.isLoading = true;
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
      if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
        let avmLocData = {
          e_id: entItem.owner_id == null ? null : entItem.e_id,
          country: this.form.get('Country').value.toLowerCase(),
          state: this.form.get('State').value == null ? '':this.form.get('State').value.toLowerCase(),
          region: this.form.get('Region').value == null ? '':this.form.get('Region').value.toLowerCase(),
          zone: this.form.get('Zone').value == null ? '':this.form.get('Zone').value.toLowerCase(),
          city: this.form.get('City').value.toLowerCase(),
          private: this.form.get('IsPrivate').value,
          status: 'JUST_ADDED'
        };
        let result = await this.authService.insAVMLocation(avmLocData);
        this.isLoading = false;
        if (result.success){
          this.reusable.openAlertMsg(result.message,"info");
          this.closeWithReturn();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
        }
      } else {
        this.reusable.openAlertMsg("Sorry, Only owner of enterprise '"+entItem.e_name+"' can add AVM Location","error");
      }
    }
  }
}

/* Dialog screen ts file for AVM-Alert-dialog.html */
@Component({
  selector: 'avl-alert-dialog',
  templateUrl: 'avl-alert-dialog.html',
})
export class avlAlertDialog implements OnInit {
  isLoading = false;
  form = this.formBuilder.group({
      alertType:['email'],
      email:[null,Validators.compose([
        Validators.email
      ])],
      sms:[null,Validators.compose([
        Validators.minLength(13),
        Validators.maxLength(15),
        this.validateMobile
      ])]
    }, 
      {validator: this.validateEmailOrSMS('email', 'sms','alertType')}
  );
  theme: string;
  titleIcon: string;
  title: string;
  alertColl = new MatTableDataSource([]);
  displayedColumns=['edit','delete','alert_type','email_mobile','validated_on'];
  showGrid:boolean = true;

  @ViewChild(MatSort) tblSort: MatSort;
  avmAlertId: any;

  constructor(
    public dialogRef: MatDialogRef<avlAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogSumAddEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(){
    this.showGrid = true;
    this.getAlertForAgentId();
  }

  async removeAlert(element){
    this.isLoading = true;
    let rmData = {
      a_a_m_id : element.a_a_m_id
    }
    let result = await this.authService.remAVMAgentAlertMapping(rmData);
    this.isLoading=false;
    if (result.success){
      this.reusable.openAlertMsg(result.message,"info");
      this.showGrid = true;
      this.getAlertForAgentId();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  editAlert(element){
    this.showGrid = false;    
    this.avmAlertId = element.a_a_m_id;
    this.form.setValue({
      alertType: element.alert_type.toLowerCase(),
      email: element.alert_type.toLowerCase() == 'email' ? element.email_mobile : null,
      sms: element.alert_type.toLowerCase() == 'sms' ? element.email_mobile : null,
    });
  }
  
  cancelAlert(){
    this.showGrid = true;
  }

  async saveAlert(nxtAct){
    this.isLoading = true;
    if (this.avmAlertId == null){
      let insData = {
        agent_id : this.data.testId,
        alert_type : (this.form.get('alertType').value).toUpperCase(),
        email_mobile : this.form.get('alertType').value == 'email' ? this.form.get('email').value : this.form.get('sms').value
      };
      let result = await this.authService.insAVMAgentAlertMapping(insData);
        this.isLoading = false;
        if (result.success){
          if(nxtAct == 'finish'){
            this.reusable.openAlertMsg(result.message,"info");
            this.showGrid = true;
            this.getAlertForAgentId();
          } else {
            this.reusable.openAlertMsg(result.message+", Add another alert","info");
          }
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
        }
    } else {
      let updData = {
        a_a_m_id : this.avmAlertId,
        agent_id : this.data.testId,
        alert_type : (this.form.get('alertType').value).toUpperCase(),
        email_mobile : this.form.get('alertType').value == 'email' ? this.form.get('email').value : this.form.get('sms').value
      };
      let result = await this.authService.updAVMAgentAlertMapping(updData);
      this.isLoading = false;
      if (result.success){
        this.reusable.openAlertMsg(result.message, "info");
        this.showGrid = true;
        this.avmAlertId = null;
        this.getAlertForAgentId();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }

  addAlerts(){
    this.showGrid = false;
    this.avmAlertId = undefined;
  }

  async getAlertForAgentId(){
    this.isLoading = true;
    let agentData = {
      agent_id : this.data.testId
    };
    let result = await this.authService.getAlertForAVMAgentId(agentData)
    this.isLoading = false;
    if (result.success){
      this.alertColl = new MatTableDataSource(result.result);
      this.alertColl.sort = this.tblSort;
    } else {
      this.alertColl = new MatTableDataSource([]);
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  selectEmailOrSMS(){
    let varEmail = this.form.get('email').value;
    let varSMS = this.form.get('sms').value;
    if (this.form.get('alertType').value == 'email'){
      this.form.setValue({'sms':null,'alertType':'email', 'email':varEmail});
    } else {
      this.form.setValue({'email':null,'alertType':'sms','sms': varSMS});
    }
  }

  validateEmailOrSMS(email,sms,alertType){
    return (group: FormGroup) => {
      if (group.controls[alertType].value == 'email' && group.controls[email].value != null && group.controls[email].value != ''){
        return null;
      } else if (group.controls[alertType].value == 'sms' && group.controls[sms].value != null){
        return null;
      } else {
        return {'validateEmailOrSMS' : true};
      }
    }
  }

  validateMobile(controls){
    if (controls.value != null){
      if (controls.value.substring(0,1) != '+' || controls.value.length < 12 || controls.value.length>20){
        return { 'validateMobile' : true};
      }
      const reqExp = new RegExp(/^[+][0-9]*$/);
      if (reqExp.test(controls.value)){
        return null;
      } else{
        return { 'validateMobile' : true};
      }
    } else {
      return null;
    }
	}
  getErrMsgAlert(control, controlName) {
    let msg =' ';
    msg += control.hasError('required') ? 'You must enter a value. ' :'';
    if (controlName =='email') {msg += control.errors.email ? 'Invalid Email': ''}
    if (controlName =='sms') {msg += control.hasError('validateMobile') ? 'Only Numbers Allowed' :''}
    if (controlName =='sms') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 12 & 20 digit length. ': ''}
    return msg;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }
}