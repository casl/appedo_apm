import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ExternalMonitorsRoutingModule } from './external-monitors-routing.module';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ExternalMonitorsComponent, rumAddEditDialog, sumAddEditDialog, FilterPipe, avmAddEditDialog,avlAddEditDialog, avlAlertDialog } from './external-monitors.component';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LogModuleModule } from '../log-module/log-module.module'

@NgModule({
  imports: [
    CommonModule,
    ExternalMonitorsRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
    LogModuleModule
  ],
  declarations: [
    ExternalMonitorsComponent,
    rumAddEditDialog,
    sumAddEditDialog,
    FilterPipe,
    avmAddEditDialog,
    avlAddEditDialog,
    avlAlertDialog,
    // AlertDialog
  ],
  entryComponents: [
    rumAddEditDialog,
    sumAddEditDialog,
    // avmAddEditDialog,
    avlAddEditDialog,
    avlAlertDialog,
    // AlertDialog
  ],
})

export class ExternalMonitorsModule { }

