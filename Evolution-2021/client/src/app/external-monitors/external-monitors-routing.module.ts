import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExternalMonitorsComponent, avmAddEditDialog } from './external-monitors.component';

const routes: Routes = [
  {
    path: '',
    component: ExternalMonitorsComponent
  },
  {
    path: 'avm',
    component: avmAddEditDialog
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ExternalMonitorsRoutingModule { }

