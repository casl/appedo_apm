import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;

  isLoading = false;
  userColl = new MatTableDataSource([]);
  theme: string;
  screenParam: any;
  displayedColumns = ["is_selected","user_id", "email_id", "first_name", "created_on", "last_access_on", "email_verified_on"];
  dispDelButton = false;
  constructor(
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
  ) { }

  ngOnInit(): void {
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    });
    this.getUserList();
  }

  async getUserList(){
    this.isLoading = true;
    let result = await this.authService.getUserList();
    this.isLoading = false;
    if (result.success){
      this.userColl = new MatTableDataSource(result.result);
      this.userColl.sort = this.sort;
    } else {
      this.authService.invalidSession(result);
    }
  }

  selUsr(row){
    row.is_selected = !row.is_selected;
    let filterIsSel = this.userColl.data.filter(x => x.is_selected);
    if (filterIsSel.length > 0){
      this.dispDelButton = true;
    } else {
      this.dispDelButton = false;
    }
  }

  async delUsr(){
    let delUsr = this.userColl.data.filter(x => x.is_selected);
    let msg ='';
    if (delUsr.length == 1){
      msg = " user is selected for deletion, Once deleted, all data will be lost for ever. Please confirm";
    } else {
      msg = " users are selected for deletion. Once deleted, all data will be lost for ever. Please confirm";
    }
    let con = confirm(delUsr.length + msg);
    if (con){
      let userArr = [];
      delUsr.map(usr =>{
        userArr.push(usr.user_id);
      });
      let param = {userIds: userArr};
      let result = await this.authService.delUser(param);
      if (result.success){
        this.reusable.openAlertMsg(result.message,"info");
        let users = this.userColl.data.filter(x => !x.is_selected);
        this.userColl = new MatTableDataSource(users);
        this.dispDelButton = false;
      } else {
        this.authService.invalidSession(result);
      }
    }
  }
}
