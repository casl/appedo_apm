import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleDetailsComponent } from './module-details.component';

const routes: Routes = [
  {
    path: '',
    component: ModuleDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ModuleDetailsRoutingModule { }

