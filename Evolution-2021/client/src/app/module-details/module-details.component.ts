import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, forkJoin } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { filter as _filter, groupBy as _groupBy, noop as _noop} from 'lodash';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

export interface DialogData {
  uid: string,
  modType: string,
  modName: string,
  modCode: string
}

export interface DialogDataCM {
  uid: string,
  guid: string,
  modType: string,
  modName: string,
  modCode: string
}

export interface DialogEdit {
  uid: string,
  modName: string,
  modDesc: string
  modCode: string
}

@Component({
  selector: 'app-module-details',
  templateUrl: './module-details.component.html',
  styleUrls: ['./module-details.component.css']
})

export class ModuleDetailsComponent implements OnInit {
  modIcon:{iconName:string;clrClass:string};
  moduleTitle1: string;
  moduleTitle2: string;
  modDetails = {};
  modCode: string;
  modColl = new MatTableDataSource([]);
  modCollDispColumn = ['delete','edit', 'uid', 'module_type', 'module_name', 'configure', 'copy', 'paste','custom_metrics', 'chart', 'critical', 'warning','active'];
  dispCopyPaste: boolean;
  isLoading: boolean;
  copiedMetrics = []; copiedUid: string; copiedModType: string;
  configParam: {uid: any; modType:string, modName:string, modCode:string};
  modEditParam: { uid: string; modName: string; modDesc: string; modCode: string; };
  @ViewChild(MatSort, { static: true }) tblSort: MatSort;
  obsEnt: any;
  theme:any;
  screenParam: any;
  customMetricData:any;

  //scroll variables
  offset = 0; 
  limit = 10;
  isFetched = false;
  lastScrolled = 0;
  @ViewChild(CdkScrollable, { static: true }) virtualScroll: CdkScrollable;
  setIntervalId;
  
  handleScroll = (scrolled: boolean) => {
    if (scrolled) { this.offset += this.limit;}
    scrolled && this.isFetched? this.getModuleDetails(this.offset, this.limit) : _noop();
  }

  hasMore(){
    return this.isFetched;
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
   public dialog: MatDialog
  ) { }

  ngOnInit() {
    sessionStorage.setItem("currentRoute","/home/moduledetails");
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.goBack();
      }
    });
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    let modDetails = this.modDetails = JSON.parse(sessionStorage.getItem("moduleDetails"));
    if (modDetails == undefined){
      this.goBack();
    } else {
      this.modCode = modDetails.moduleType.toLowerCase();
      let selRow = modDetails.selRow;
      this.getModuleIcon(this.modCode);
      this.moduleTitle1 = this.modCode+" details of ";
      this.moduleTitle2 = selRow.system_name+" "+selRow.owner+"("+selRow.system_id+")";
      this.getModuleDetails();
    }
    if (this.modCode == 'server' || this.modCode == 'application' || this.modCode == 'database' ){
      this.dispCopyPaste = true;
      let sessionCpyMetrics = JSON.parse(sessionStorage.getItem("copiedMetrics"));
      if (sessionCpyMetrics != undefined){
        this.copiedMetrics = sessionCpyMetrics.copiedMetrics;
        this.copiedModType = sessionCpyMetrics.copiedModType;
        this.copiedUid = sessionCpyMetrics.copiedUid;
      }
    } else {
      this.dispCopyPaste = false;
    }
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);

  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.obsEnt != undefined)
    this.obsEnt.unsubscribe();
  }

  async drawChart(uidData){
    uidData['module_code'] = this.modCode;
    if (uidData.module_code == 'profiler') {
      sessionStorage.setItem("profilerLocData",JSON.stringify(uidData));
      this.router.navigate(['/home/profiler']);
    } else if (uidData.module_code == 'netstack' || uidData.module_code == 'nettrace' ){
      sessionStorage.setItem("netstackChart",JSON.stringify(uidData));
      this.router.navigate(['/home/netstackcharts']);
    } else {
      sessionStorage.setItem("drawChartUid",JSON.stringify(uidData));
      sessionStorage.removeItem("dashSelVal");
      this.router.navigate(['/home/dashboard']);
    }
  }

  async deleteMMCard(ele){
    let delRes = await this.deleteMMCardReusable(ele);
    this.isLoading = false;
    if (delRes['success']) {
      this.reusable.openAlertMsg(delRes['message'],"info");
      if (this.modCode != undefined){
        this.getModuleDetails();
      }
    } else if (delRes['error']) {
      this.reusable.openAlertMsg(this.authService.invalidSession(delRes),"error");
    }
  }
  //made it as public as it is used in rum module card in external monitors
  public async deleteMMCardReusable(ele) {
    let modCode = ele.modCode == undefined ? this.modCode : ele.modCode; 
    let module = ele.module_type == undefined? modCode : ele.module_type;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    let res = {};
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of Module - "+module+" :: "+ele.module_name+" ("+ele.uid+")");
      this.isLoading = true;
      if(confirmMessage) {
        let entDet = JSON.parse(sessionStorage.getItem('entItem'));
        let reqData = {uid : +ele.uid, moduleCode: modCode, entId : entDet.e_id == 0 ? null : entDet.e_id };
        res = await this.authService.deleteModule(reqData);
        this.isLoading=false;
        return res;
      } else {
        this.isLoading=false;
        return {cancelled:true};
      }
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can delete this card","error");
      return res;
    }
  }

  openAlertDetails(ele, alert){
    console.log(ele, alert);
    let details = JSON.parse(JSON.stringify(ele));
    details.module_name = details.module_name+"-"+details.module_type;
    details.log_grok = alert;
    details.log_table_name ='so_threshold_breach_';
    sessionStorage.setItem("logDetails", JSON.stringify(details));
    this.router.navigate(['/home/logmodule/logdetails']);
  }
  
  openCustomMetric(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.customMetricData = {uid:row.uid, modName:row.module_name, modCode: this.modCode, modType:row.module_type, guid:row.guid };
      this.openCustomMetricDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit Module Attributes","info");
    }
  }
  
  openCustomMetricDialog(): void {
    const dialogRef = this.dialog.open(customMetricDialog, {
      width: '50%',
      data: {uid:this.customMetricData.uid, modName:this.customMetricData.modName, modCode: this.customMetricData.modCode, modType:this.customMetricData.modType, guid:this.customMetricData.guid}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined)
        this.updLocalVariables(result);
    });
  }
 
  openConfigSetting(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.configParam = {uid: row.uid, modType: row.module_type, modName: row.module_name, modCode: this.modCode};
      this.openConfigDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can change the configurations","info");
    }
  }

  openConfigDialog(): void {
    const dialogRef = this.dialog.open(configureSettingDialog, {
      width: '50%',
      data: {uid: this.configParam.uid, modType: this.configParam.modType, modName:this.configParam.modName, modCode:this.configParam.modCode}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  
  openModuleEdit(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.modEditParam = {uid:row.uid, modName:row.module_name, modDesc: row.description, modCode: this.modCode};
      this.openModuleEditDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit Module Attributes","info");
    }
  }
  
  openModuleEditDialog(): void {
    const dialogRef = this.dialog.open(moduleEditDialog, {
      width: '40%',
      data: {uid:this.modEditParam.uid, modName:this.modEditParam.modName, modDesc: this.modEditParam.modDesc, modCode: this.modEditParam.modCode}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined)
        this.updLocalVariables(result);
    });
  }
  
  private updLocalVariables(element: any) {
    let updColl = this.modColl.data;
    let findRow = updColl.find(x => x.uid == element.uid);
    if (findRow != undefined) {
      findRow['module_name'] = element.modName;
      findRow['description'] = element.modDesc;
    }
  }

  getModuleIcon(moduleName) {
    this.modIcon = this.reusable.getIconName(moduleName);
  }

  goBack(){
    sessionStorage.removeItem("moduleDetails");
    this.router.navigate(['home/systemdetails']);
  }

  async getModuleDetails(offset?:number, limit?:number){
    let selRow = this.modDetails['selRow'];
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 10;
    let param = {
      modCode: this.modCode,
      systemId: selRow.system_id,
      offset: offset,
      limit: limit
    };
    let result = await this.authService.getMMCardV2(param);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      this.isFetched = true;
      if (this.modColl.data.length == 0 || reset){
        this.modColl = new MatTableDataSource(result.result);
        result.result.map(row => {
          this.getAPMAlertsForModule(row);
        });
      } else {
        let newData = this.modColl.data.concat(result.result);
        result.result.map(row => {
          this.getAPMAlertsForModule(row);
        });
        this.modColl = new MatTableDataSource(newData);
      }
      this.modColl.sort = this.tblSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.modColl = new MatTableDataSource([]);
      }
      this.isFetched = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async getAPMAlertsForModule(module){
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let param = {
      entUserId: entItem != undefined ? entItem.owner_id : null, 
      uid: module.uid
    };
    let result = await this.authService.getAPMAlertsForModule(param).toPromise();
    if (result.success){
      module['critical'] = result.result[0].critical;
      module['warning'] = result.result[0].warning;
    }
  }

  async copyMetrics(element){
    this.isLoading = true;
    let configData = {uid: element.uid};
    if (this.modCode == 'server' || this.modCode == 'application' || this.modCode == 'database'){
      let res = await this.authService.copyConfigByUid(configData);
      this.isLoading = false;
      if (res.success){
        this.copiedMetrics = res.result;
        this.copiedModType = element.module_type;
        this.copiedUid = element.uid;
        sessionStorage.setItem("copiedMetrics", JSON.stringify({copiedMetrics:this.copiedMetrics,copiedModType:this.copiedModType,copiedUid:this.copiedUid}));
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
      }
    } else {
      this.reusable.openAlertMsg("Available only for application, server, database", "info");
    }
  }

  async cancelCopyMetrics(){
    this.copiedMetrics = [];
    this.copiedUid = undefined;
    this.copiedModType = undefined;
    sessionStorage.removeItem("copiedMetrics");
  }

  async pasteMetrics(element){
    this.isLoading = true;
    let configData = {uid: element.uid, copiedUid: this.copiedUid};
    let res = await this.authService.pasteConfigByUid(configData);
    if (res.success){
      let metrics = [];
      res.result.map((ele)=>{
        metrics.push(ele.counter_id);
      });
      let configData = {
        uid: this.copiedUid, 
        metricIds:metrics, 
        moduleCode: this.modCode,
        moduleName: element.module_name
      };
      //to update the chart visual for newly updated metrics
      let updCVRes = this.authService.updateConfigCV(configData);
      //to set the user_status to restart so that agent will start collecting metrics for the newly selected metrics
      let updMMRes = this.authService.updateConfigMM(configData);
      forkJoin([updCVRes,updMMRes]).subscribe(results =>{
        this.isLoading = false;
        if(results[0].success && results[1].success){
          this.reusable.openAlertMsg("Copied metrics pasted successfully. Updated all other dependent tables.","info");
        } else {
          if (!results[0].success)  this.reusable.openAlertMsg(this.authService.invalidSession(results[0]),"error");
          else  this.reusable.openAlertMsg(this.authService.invalidSession(results[1]),"error");
        }
      });
    } else {
      this.isLoading = false;
      this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
    }
  }
}

/* Dialog screen ts file for configure settings html*/
@Component({
  selector: 'configure-setting-dialog',
  templateUrl: 'configure-setting-dialog.html',
  styleUrls: ['./module-details.component.css']
})
export class configureSettingDialog implements OnInit {
  categoryWise: any;
  selectedMetrics: any;
  arrCategory: string[];
  canConfigure: boolean;
  configTitle: string;
  configUid: any;
  subModName: any;
  configSelCategory: string = "--select--";
  avlMetrics: any;
  themeClass; isLoading: boolean;
  theme:any;
  screenParam: any;
  constructor(
    public dialogRef: MatDialogRef<configureSettingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService : AuthenticationService,
    private reusable: ReusableComponent,
  ) {}

  ngOnInit(){
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    })
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    });
    this.configTitle = 'Configure Metrics of '+this.data.modName +' ('+this.data.uid+')' ;
    this.configUid = this.data.uid;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.getConfigByUid();
    }
  }

  async getConfigByUid(){
    let res = await this.authService.getConfigByUid(this.data);
    if (res.success){
      this.categoryWise = _groupBy(res.result,'category')
      this.selectedMetrics = _filter(res.result,['is_selected',true]);
      this.arrCategory = Object.keys(this.categoryWise);
      this.arrCategory.unshift("--select category--");
      this.onCategoryChange("--select category--");
      this.canConfigure = true;
      this.subModName = this.data.modName;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onChange(ele){
    if (ele.is_selected)
      this.selectedMetrics.push(ele);
    else
      this.selectedMetrics = _filter(this.selectedMetrics,['is_selected',true]);
  }

  onCategoryChange(selCategory){
    this.configSelCategory = selCategory;
    this.avlMetrics = this.categoryWise[selCategory];
  }

  async updateConfigureMetrics(){
    let metrics =[] ;
    if (this.selectedMetrics.length == 0){
      this.reusable.openAlertMsg("At least one metric should be selected","error");
      return;
    }
    this.isLoading = true;
    this.selectedMetrics.map(ele => {
       metrics.push(Number(ele.counter_id));
    });
    let configData = {
      uid: this.configUid, 
      metricIds:metrics, 
      moduleCode: this.data.modCode,
      moduleName: this.data.modName
    };
    let result = await this.authService.updateConfigureMetrics(configData);
    if (result.success){
      //to update the chart visual for newly updated metrics
      let updCVRes = this.authService.updateConfigCV(configData);
      //to set the user_status to restart so that agent will start collecting metrics for the newly selected metrics
      let updMMRes = this.authService.updateConfigMM(configData);
      forkJoin([updCVRes,updMMRes]).subscribe(results =>{
        this.isLoading = false;
        if(results[0].success && results[1].success){
          this.reusable.openAlertMsg("Metrics updated successfully","info");
          this.onClose();
        } else {
          if (!results[0].success) this.reusable.openAlertMsg(this.authService.invalidSession(results[0]),"error");
          else         this.reusable.openAlertMsg(this.authService.invalidSession(results[1]),"error");
        }
      });
    } else {
      this.isLoading = false;
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
}

/* Dialog screen ts file for Module-edit-dialog.html */
@Component({
  selector: 'module-edit-dialog',
  templateUrl: 'module-edit-dialog.html',
})
export class moduleEditDialog {
  btnDisable = false;
  isLoading: boolean;
  modCode: string;

  constructor(
    public dialogRef: MatDialogRef<moduleEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  validateStringEmpty(){
    if (this.data.modName == undefined || this.data.modName.trim() == ''){
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }
  }
  
  async updateEditMMCard(element){
    this.isLoading = true;
    this.modCode = element.modCode;    
    let updData = {
      uid: element.uid,
      modName: element.modName,
      description: element.modDesc,
      modCode: this.modCode
    };
    if (this.modCode.toLowerCase() != 'network' ){
      let updMMCVRes = this.authService.updateMMCardCV(updData);
      let updMMEditRes = this.authService.updateEditMMCard(updData);
      forkJoin([updMMEditRes,updMMCVRes]).subscribe(results => {
        this.isLoading = false;
        if (results[0].success){
          this.dialogRef.componentInstance.data = element;
          this.reusable.openAlertMsg(results[0].result,"info");
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(results[0]),"error");
        }
        this.dialogRef.close(this.data);
      });
    } else {
      let updMMEditRes = await this.authService.updateEditMMCard(updData);
      this.isLoading = false;
      if (updMMEditRes.success){
        this.dialogRef.componentInstance.data = element;
        this.reusable.openAlertMsg(updMMEditRes.result,"info");
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(updMMEditRes),"error");
      }
      this.dialogRef.close(this.data);
    }
  }
}


/* Dialog screen - ts file for custom-metrics-dialog.html */
@Component({
  selector: 'custom-metric-dialog',
  templateUrl: 'custom-metric-dialog.html',
})

export class customMetricDialog implements OnInit {
  btnDisable = false;
  isLoading: boolean;
  modCode: string;
  form: FormGroup;
  counterCategory = [];
  isNewCategory: boolean;
  counterUnits = ['numbers','text','unit','rows','%','datetime','sec','ms','min','hour','bps','KBps','MBps','GBps','bytes','KB','MB','GB','TB','Rs','$'];
  execTypes = ['cmd'];

  constructor(
    public dialogRef: MatDialogRef<customMetricDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDataCM,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {  }

  ngOnInit(){
    this.getCounterCategory();
    this.createCMForm();
  }

  onClose(): void {
    this.dialogRef.close();
  }

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    return msg;
  }

  createCMForm(){
    this.form = this.formBuilder.group({
      categoryNameCM:[null,Validators.compose([
        Validators.required,
      ])],
      categoryNewNameCM:[null],
      CounterNameCM:[null, Validators.compose([
        Validators.required,
      ])],
      CounterDescCM:[null,Validators.compose([
        Validators.required,
      ])],
      QryStringCM:[null,Validators.compose([
        Validators.required
      ])],
      UnitCM:[null,Validators.compose([
      Validators.required
      ])],
      ExecTypeCM:[this.execTypes[0],Validators.compose([
        Validators.required
      ])]
   });	
  }


  validateStringEmpty(){
    if (this.data.modName == undefined || this.data.modName.trim() == ''){
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }
  }
  
  onChangeCategory(selection){
    if (selection.category != "--Create New--") this.isNewCategory = false;
    else this.isNewCategory = true;
  }

  async getCounterCategory() {
    let qryData = {uid: this.data.uid};
    let result = await this.authService.getCounterCategoryNames(qryData);
      this.isLoading = false;
      if(result.success){
        this.counterCategory = result.result;
        this.counterCategory.unshift({category: '--Create New--'}); 
        this.form.get("categoryNameCM").setValue(this.counterCategory[0]);
        this.isNewCategory = true;
      } else {
        this.reusable.openAlertMsg(result.message, "error");
        this.authService.invalidSession(result);
      }
  }

  async addCustomMetric(){
    let categoryName;
    if (this.form.get("categoryNameCM").value.category == "--Create New--" && (this.form.get("categoryNewNameCM").value == undefined || this.form.get("categoryNewNameCM").value.trim() == "")){
      this.reusable.openAlertMsg("Category name is Mandatory","error");
      return;
    } else {
      if (this.form.get("categoryNameCM").value.category == "--Create New--"){
        categoryName = this.form.get("categoryNewNameCM").value.trim();
      } else {
        categoryName = this.form.get("categoryNameCM").value.category;
      }
    }
    this.isLoading = true;
    let cusMetData = {
      uid: this.data.uid,
      guid: this.data.guid,
      categoryName: categoryName,
      isNewCategory: this.isNewCategory,
      counterName: this.form.get('CounterNameCM').value.toLowerCase().trim(),
      counterDesc: this.form.get('CounterDescCM').value.trim(),
      queryStr: this.form.get('QryStringCM').value.trim(),
      unit: this.form.get("UnitCM").value,
      execType: this.form.get("ExecTypeCM").value
    };
    let result = await this.authService.addCustomMetric(cusMetData);
    this.isLoading = false;
    if (result.success) {
      this.reusable.openAlertMsg(result.message,"info");
      this.onClose();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
}