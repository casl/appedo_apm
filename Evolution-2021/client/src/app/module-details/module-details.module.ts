import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ModuleDetailsRoutingModule } from './module-details-routing.module';
import { ModuleDetailsComponent, configureSettingDialog, moduleEditDialog,customMetricDialog } from './module-details.component';

@NgModule({
  imports: [
    CommonModule,
    ModuleDetailsRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
  ],
  declarations: [
    ModuleDetailsComponent,
    configureSettingDialog,
    moduleEditDialog,
    customMetricDialog
  ],
  entryComponents: [
    configureSettingDialog,
    moduleEditDialog,
    customMetricDialog
  ],
})

export class ModuleDetailsModule { }

