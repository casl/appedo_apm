﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  domain = environment.domain;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  uploaderResp:string;

  private sessionStorage = new Subject<boolean>();
  themeChange: BehaviorSubject<{}> = new BehaviorSubject({themeClass:"lightindigotheme",themeName:"Light Indigo"});
  toggleMenu: BehaviorSubject<boolean> = new BehaviorSubject(true);
  screenChange: BehaviorSubject<{}> = new BehaviorSubject({height:0, width:0});
  dateEpoch:  BehaviorSubject<{}> = new BehaviorSubject({stDate:new Date().getTime()-59*60*1000, endDate:new Date().getTime()});
  dispOnRequest: BehaviorSubject<boolean> = new BehaviorSubject(false);
  myDashSelVal: BehaviorSubject<string> = new BehaviorSubject("--my dashboard--");
  newMyChart: BehaviorSubject<boolean> = new BehaviorSubject(false);
  entChange: BehaviorSubject<boolean> = new BehaviorSubject(false);
  dispEnt: BehaviorSubject<boolean> = new BehaviorSubject(true);
  dispMyChart: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private http: HttpClient,
    private router: Router,
    public jwtHelper: JwtHelperService,
  ) { }

  convertMstoTime(intMs){
    if (isNaN(intMs)) return intMs;
    let ms = intMs%1000;
    let Sec = Math.floor(intMs/1000);
    let convSec = this.convertSecToTime(Sec);
    return convSec+"."+ms;
  }

  convertSecToTime(intSec) {
    if (isNaN(intSec)) return intSec; //98402
    let sec = 0;
    let min = 0;
    let hr = 0;
    let days = 0;
    let balMin;
    let balHr;
    let balDay;
    if (intSec / 60 >= 1) {
        sec = intSec % 60; //0sec 2
        balMin = Math.floor(intSec / 60); //60min //200 1640
        //min = balMin % 60;
        if (balMin / 60 >= 1) {
            min = balMin%60 //0 20 20
            balHr = Math.floor(balMin / 60); //1 3 27
            if (balHr/24 >= 1){ 
                hr = balHr % 24;
                balDay = Math.floor(balHr / 24); 
                days = Math.floor(balDay);
            } else {
                hr = Math.floor(balHr);
            }
        } else {
            min = Math.floor(balMin)
        }
    } else {
        sec = Math.floor(intSec);
    }
    if (days>0) return days + " Days:" + hr + ":" + min + ":" + sec ;
    else if (hr>0) return hr + ":" + min + ":" + sec ;
    else return min + ":" + sec ;
  }

  convertDate(strDate){
    let date = new Date(strDate);
    if (date.toString() == "Invalid Date") return strDate;
    return date.toLocaleDateString([],{year:'numeric',month:'short', day:'numeric'});
  }

  convertDateTime(strDate){
    let repStrDate = strDate.toString().replace(' ','');
    if (!isNaN(repStrDate)) repStrDate = Number(repStrDate);
    let date = new Date(repStrDate);
    if (date.toString() == "Invalid Date") return strDate;
    return date.toLocaleString([],{year:'numeric',month:'short', day:'numeric', hour: '2-digit', minute: '2-digit', hour12: false});
  }

  convertTime(strDate){
    let date = new Date(strDate);
    if (date.toString() == "Invalid Date") return strDate;
    return date.toLocaleTimeString([],{hour: '2-digit', minute: '2-digit', hour12: false});
  }

  public randomString(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  encrypt(val){
    var key = CryptoJS.enc.Utf8.parse(environment.keyEncryptDecrypt);
    var iv = CryptoJS.enc.Hex.parse(this.randomString(32));
    var encrypted = CryptoJS.AES.encrypt(val, key, { 
      iv: iv, 
      mode: CryptoJS.mode.CBC
    });   
    var output = encrypted.ciphertext.toString();
    return iv+":"+output;
  }
   
  decrypt(val){
    try {
      var key = CryptoJS.enc.Utf8.parse(environment.keyEncryptDecrypt);
      let keyVal = val.split(":");
      var iv = CryptoJS.enc.Hex.parse(keyVal[0]);
      var cipherText = CryptoJS.enc.Hex.parse(keyVal[1]);
      var options = { mode: CryptoJS.mode.CBC, iv: iv};
      var decrypted = CryptoJS.AES.decrypt({ciphertext: cipherText, iv:iv, salt:undefined}, key, options);
      let retVal = decrypted.toString(CryptoJS.enc.Utf8);
      return retVal
    } catch (err) {
      console.error(err);
      return "";
    } 
  }

  getScenRunDetails(param) {
    return this.http.post<any>(this.domain + '/externalMonitor/getScenRunDetails', param);
  }
  getLTScenCard(param) {
      return this.http.post<any>(this.domain + '/externalMonitor/getLTScenCard', param);
  }

  getLTScriptCard(param) {
      return this.http.post<any>(this.domain + '/externalMonitor/getLTScriptCard', param);
  }

  getLTScenStat(param) {
      return this.http.post<any>(this.domain + '/externalMonitor/getLTScenStat', param);
  }

  getLTScriptStat(param) {
      return this.http.post<any>(this.domain + '/externalMonitor/getLTScriptStat', param);
  }

//   downloadAgent(req) {
//       return this.http.get<any>(this.domain+req.url, req);
//   }

  async downloadAgent(req) {
    //return this.http.get<any>(this.domain+req.url, req).toPromise();
    return this.http.post(req, req, {responseType:'blob'}).toPromise();
  }

  uploadJmeterScripts(param) {
      return this.http.post<any>(this.domain+ '/pgDbAuth/uploadJmeterScripts', param);
  }

  checkValidHarURL(reqData) {
      //return this.http.get<any>(harURL);
      return this.http.post<any>(this.domain + '/externalMonitor/checkValidURL', reqData);
  }
  //checkValidURL
  async mapUsrToEnt(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/mapUsrToEnt', entData).toPromise();
  }

  async delMappedUserFromEnt(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/delMappedUserFromEnt', entData).toPromise();
  }

  async getUsrEntMap(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/getUsrEntMap', entData).toPromise();
  }

  async unDelEnterprise(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/unDelEnterprise', entData).toPromise();
  }

  async delEnterprise(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/delEnterprise', entData).toPromise();
  }

  async getEntByUser(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getEntByUser', param).toPromise();
  }

  async addUpdEnterprise(entData){
      return this.http.post<any>(this.domain + '/pgDbAuth/addUpdEnterprise', entData).toPromise();
  }

  async deleteLogAlert(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/deleteLogAlert', reqData).toPromise();
  }

  async getAvmEditById(avmData){
      return this.http.post<any>(this.domain + '/externalMonitor/getAvmEditById', avmData).toPromise();
  }

  async saveAVM(avmData){
      return this.http.post<any>(this.domain + '/externalMonitor/saveAVM', avmData).toPromise()
  }

  async updAVMAgents(){
      return this.http.get<any>(this.domain + '/avmScheduler/updAVMAgents').toPromise()
  }

  async getAVLMonCard(eData){
      return this.http.post<any>(this.domain + '/externalMonitor/getAVLMonCard', eData).toPromise();
  }

  getBreachCntByModuleUid(param){
      return this.http.post<any>(this.domain + '/externalMonitor/getBreachCntByModuleUid',param);
  }

  getAVMPolicyCountByTestId(avmTestData){
      return this.http.post<any>(this.domain + '/externalMonitor/getAVMPolicyCountByTestId',avmTestData);
  }

  getAVMTestLocByTestId(avmTestData){
      return this.http.post<any>(this.domain + '/externalMonitor/getAVMTestLocByTestId',avmTestData);
  }

  async insAVMLocation(avmLocData){
      return this.http.post<any>(this.domain + '/externalMonitor/insAVMLocation',avmLocData).toPromise();
  }
  async deleteAvm(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/deleteAvm', reqData).toPromise();
  }

  async deleteAvmLoc(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/deleteAvmLoc', reqData).toPromise();
  }

  getAllJMeterRunDetail(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getAllJMeterRunDetail', reqData);
  }

  getLTLiveRunDetail(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getLTLiveRunDetail', reqData);
  }

  getRegionDetails(reqData){
      return this.http.post<any>(this.domain + '/pgDbAuth/getRegionDetails', reqData);
  }

  getUserAgentDetails(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getUserAgentDetails', reqData);
  }

  stopJMeterTest(reqData){
      return this.http.post<any>(this.domain + '/pgDbAuth/stopJMeterTest', reqData);
  }

  runScenario(reqData){
      return this.http.post<any>(this.domain + '/pgDbAuth/runScenario', reqData);
  }

  getScenarioStatusDetails(reqData){
      return this.http.post<any>(this.domain + '/pgDbAuth/getScriptwiseData', reqData);
  }

  async getAVMLocationCard(eData){
      return this.http.post<any>(this.domain+'/externalMonitor/getAVMLocationCard',eData).toPromise();
  }

  async getAVMLocation(entData){
      return this.http.post<any>(this.domain+'/externalMonitor/getAVMLocation',entData).toPromise()
  }

  getAlertForAVMAgentId(agentData){
      return this.http.post<any>(this.domain+'/externalMonitor/getAlertForAVMAgentId',agentData).toPromise();
  }

  async insAVMAgentAlertMapping(insData){
      return this.http.post<any>(this.domain+'/externalMonitor/insAVMAgentAlertMapping',insData).toPromise();
  }

  async updAVMAgentAlertMapping(updData){
      return this.http.post<any>(this.domain+'/externalMonitor/updAVMAgentAlertMapping',updData).toPromise();
  }

  async remAVMAgentAlertMapping(rmData){
      return this.http.post<any>(this.domain+'/externalMonitor/remAVMAgentAlertMapping',rmData).toPromise();
  }

  getSumConnectionById(data){
      return this.http.post<any>(this.domain + '/pgDbAuth/getSumConnectionById', data);
  }
  getSumBrowsersById(data){
      return this.http.post<any>(this.domain + '/pgDbAuth/getSumBrowsersById', data);

  }
  getSumLocationById(data){
      return this.http.post<any>(this.domain + '/pgDbAuth/getSumLocationById', data);

  }
  async getSumCardData(sumCard){
      return this.http.post<any>(this.domain + '/pgDbAuth/getSumCardData', sumCard).toPromise();
  }

  getSumMeasurementData(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getSumMeasurementData', reqData);
  }

  getFirstByte(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getFirstByte', reqData);
  }

  fetchScreen(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/fetchScreen', reqData);
  }

  async getEditSumData(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getEditSumData', reqData).toPromise();
  }

  getEditRumData(reqData){
      return this.http.post<any>(this.domain + '/externalMonitor/getEditRumData', reqData);
  }

  async pasteConfigByUid(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/pasteConfigByUid', configData).toPromise();
  }

  async copyConfigByUid(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/copyConfigByUid', configData).toPromise();
  }

  async updateConfigMM(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateConfigMM', configData).toPromise();
  }

  async updateConfigCV(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateConfigCV', configData).toPromise();
  }

  async updateMMCardCV(updData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateMMCardCV', updData).toPromise();
  }

  async updateEditMMCard(updData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateEditMMCard', updData).toPromise();
  }

  async deleteModule(deleteData) {
      return this.http.post<any>(this.domain + '/internalMonitor/deleteModule', deleteData).toPromise();
  }

  getProfilerResources(profilerData) {
      return this.http.post<any>(this.domain + '/internalMonitor/getProfilerResources', profilerData);
  }

  async getCounterCategoryNames(data) {
      return this.http.post<any>(this.domain + '/internalMonitor/getCounterCategoryNames', data).toPromise();
  }

  async deleteSumModule(deleteData) {
      return this.http.post<any>(this.domain + '/externalMonitor/deleteSumModule', deleteData).toPromise();
  }

  async deleteRumModule(deleteData) {
      return this.http.post<any>(this.domain + '/externalMonitor/deleteRumModule', deleteData).toPromise()
  }

  async updateConfigureMetrics(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateConfigureMetrics', configData).toPromise();
  }

  async getConfigByUid(configData){
      return this.http.post<any>(this.domain + '/pgDbAuth/getConfigByUid', configData).toPromise();
  }

  //Custom DB Connector Query Starts
  customQuery(qryData){
      return this.http.post<any>(this.domain + '/pgDbAuth/customQuery', qryData);
  }
  //Custom DB connector Query Ends
  addChartVisual(chartData){
      return this.http.post<any>(this.domain + '/pgDbAuth/addChartVisual', chartData);
  }
  updateCVbyChartId (chartData){
      return this.http.post<any>(this.domain + '/pgDbAuth/updateCVbyChartId', chartData);

  }
  watchStorage(): Observable<any> {
      return this.sessionStorage.asObservable();
  }
  setItem(key: string, data: any) {
      sessionStorage.setItem(key, data);
      this.sessionStorage.next(true);
  }

  removeItem(key) {
  sessionStorage.removeItem(key);
  this.sessionStorage.next(false);
  }

  async registerUser(user){
  return this.http.post<any>(this.domain + '/pgDbAuth/register', user).toPromise();
  }
  async validateEmailSignUp(qryData){
  return this.http.post<any>(this.domain + '/pgDbAuth/validateEmailSignUp', qryData).toPromise();
  }

  checkEmailAvl(qryData){
  return this.http.post<any>(this.domain + '/pgDbAuth/checkEmailAvl', qryData);
  }

  //Added this exclusively for validating enterprise user as it failed when adding owner itself.
  checkEmailAvlInclOwner(qryData){
  return this.http.post<any>(this.domain + '/pgDbAuth/checkEmailAvlInclOwner', qryData);
  }

  profileUpdate(profile){
  return this.http.post<any>(this.domain + '/pgDbAuth/profileUpdate', profile);
  }

  async chgPassword(pass){
  return this.http.post<any>(this.domain + '/pgDbAuth/chgPassword', pass).toPromise();
  }
  //changeForgottenPassword
  async changeForgottenPassword(param) {
  return this.http.post<any>(this.domain + '/pgDbAuth/changeForgottenPassword', param).toPromise();
  }

  verifySignUp(param) {
  return this.http.post<any>(this.domain + '/pgDbAuth/verifySignUp', param);
  }

  verifySlaEmail(param) {
  return this.http.post<any>(this.domain + '/pgDbAuth/verifySlaEmail', param);
  }

  async forgotPassword(param){
  return this.http.post<any>(this.domain + '/pgDbAuth/forgotPassword', param).toPromise();
  }

  async dbConnectorUpdate(dbConnData){
  return this.http.post<any>(this.domain + '/pgDbAuth/dbConnectorUpdate', dbConnData).toPromise();
  }

  async login(user){
      return this.http.post<any>( this.domain +'/pgDbAuth/login',user).toPromise();
  }

  chkDDChartId(chkData){
      return this.http.post<any>(this.domain +'/pgDbAuth/chkDDChartId',chkData);
  }

  async addToMyChart(myChartData){
      return this.http.post<any>(this.domain +'/pgDbAuth/addToMyChart',myChartData).toPromise();
  }

  removeChart(rcData){
      return this.http.post<any>(this.domain +'/pgDbAuth/removeChart',rcData);
  }

  storeSessionData(token,user){
      this.setItem('token',token);
      this.setItem('apdUser', JSON.stringify(user.name));
      this.setItem('apdUserDet', JSON.stringify(user));
  }

  getCVSubCategoryStat(cvSCData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getCVSubCategoryStat',cvSCData);
  }

  getCVCardByRefId(cvData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getCVCardByRefId',cvData);
  }

  getAvlConfigCntForUid(msData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getAvlConfigCntForUid',msData);
  }

  getRumPageViews(pvData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getRumPageViews',pvData);
  }
  getMetricStatusForUid(msData) {
      return this.http.post<any>(this.domain+'/pgDbAuth/getMetricStatusForUid',msData);
  }

  getMMCard(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getMMCard', mmData);
  }

  getModuleMasterStat(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getModuleMasterStat', mmData);
  }

  getChartVisualStat(cvData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartVisualStat', cvData);
  }

  getChartMappedCnt(dbData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartMappedCnt',dbData);
  }

  async getDBConnectorForDbId(dbData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getDBConnectorForDbId',dbData).toPromise();
  }
  getChartDataByChartId(chartData) {
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataByChartId',chartData);
  }

  getChartVisualForChartId(chartData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartVisualForChartId',chartData);
  }

  delCVbyChartId(chartData){
      return this.http.post<any>(this.domain+'/pgDbAuth/delCVbyChartId',chartData);
  }

  getChartVisualCard(userData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartVisualCard',userData);
  }

  async getDatabase(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getDatabase',param).toPromise();
  }

  getDbConnector(){
      return this.http.get<any>(this.domain+'/pgDbAuth/getDbConnector');
  }

  getDataFromToken(reqData) {
      return this.http.post<any>(this.domain + '/pgDbAuth/getDataFromToken',reqData);
  }

  async getColDef(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getColDef',param).toPromise();
  }

  async getPsqlTables(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getPsqlTables',param).toPromise();
  }
  async getProfile(){
      return this.http.get<any>(this.domain + '/pgDbAuth/profile').toPromise();
  }

  async getDashboard(){
      return this.http.get<any>(this.domain + '/pgDbAuth/getDashboard').toPromise();
  }

  async validateConnection(dbConnData){
      return this.http.post<any>(this.domain + '/pgDbAuth/validateConnection',dbConnData).toPromise();
  }

  async getEnterprise(){
      return this.http.get<any>(this.domain + '/pgDbAuth/getEnterprise').toPromise();
  }

  getCustomQueryById(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getCustomQueryById', param).toPromise();
  }

  async runRowCountQuery(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/runRowCountQuery', param).toPromise();
  }
  async runQuery(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/runQuery', param).toPromise();
  }

  async delCustomQry(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/delCustomQry', param).toPromise();
  }

  async saveQuery(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/saveQuery', param).toPromise();
  }

  async getSumNodes(sumData){
      return this.http.post<any>(this.domain + '/externalMonitor/getSUMNodes', sumData).toPromise();
  }

  async addUpdateSum(sumData){
      return this.http.post<any>(this.domain + '/externalMonitor/addUpdateSum', sumData).toPromise();
  }

  async addCustomMetric(cusMetData){
      return this.http.post<any>(this.domain + '/internalMonitor/addCustomMetric', cusMetData).toPromise();
  }

  async getSumDeviceTypes(){
      return this.http.get<any>(this.domain + '/externalMonitor/getSumDeviceTypes').toPromise();
  } 

  async getSumConnectivity(){
      return this.http.get<any>(this.domain + '/externalMonitor/getSumConnectivity').toPromise();
  }
  async addRUM(rumData){
      return this.http.post<any>(this.domain + '/externalMonitor/addUpdateRum', rumData).toPromise();
  }    
  getChartDataPsql(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataPsql',param);
  }
  getChartDataMssql(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataMssql',param);
  }
  getChartDataChsql(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataChsql',param);
  }
  getChartQuery(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartQuery',param);
  }
  getDashCharts(myDash){
      return this.http.post<any>(this.domain+'/pgDbAuth/getDashCharts',myDash);
  }    
  updateReportEmail(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updateReportEmail',param).toPromise()
  }
  updateSchedule(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updateSchedule',param).toPromise()
  }
  getConnectorCustomQry(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getConnectorCustomQry',param).toPromise()
  }
  postChartParam(arrChartParam){
      return this.http.post<any>(this.domain + '/pgDbAuth/saveChartParam',arrChartParam);
  }
  async getSlaPoliciesByMetric(alrtData){
      return this.http.post<any>(this.domain+'/internalMonitor/getSlaPoliciesByMetric',alrtData).toPromise();
  }
  async getProcessNameByMetric(procData){
      return this.http.post<any>(this.domain+'/internalMonitor/getProcessNameByMetric',procData).toPromise();
  }
  async getSystemContent(userData){
      return this.http.post<any>(this.domain+'/internalMonitor/getSystemContent',userData).toPromise();
  }
  async getModCodeCount(systemData){
      return this.http.post<any>(this.domain+'/internalMonitor/getModCodeCount',systemData).toPromise();
  } 
  async updateSystemInfo(systemData){
      return this.http.post<any>(this.domain+'/internalMonitor/updateSystemInfo',systemData).toPromise();
  }
  getMappedModuleDetails(systemData){
      return this.http.post<any>(this.domain+'/internalMonitor/getMappedModuleDetails',systemData);
  }
  async insSLA(procData){
      return this.http.post<any>(this.domain+'/internalMonitor/insSLA',procData).toPromise();
  }
  async updSLA(procData){
      return this.http.post<any>(this.domain+'/internalMonitor/updSLA',procData).toPromise();
  }
  getGrokPatternByUid(gpData){
      return this.http.post<any>(this.domain+'/internalMonitor/getGrokPatternByUid',gpData);
  }
  getLogStatusByPattern(logStatusData){
      return this.http.post<any>(this.domain+'/internalMonitor/getLogStatusByPattern',logStatusData);
  }
  getLogLROByPattern(logStatusData){
      return this.http.post<any>(this.domain+'/internalMonitor/getLogLROByPattern',logStatusData);
  }
  async getNumericColumnForTable(tableName){
      return this.http.post<any>(this.domain+'/internalMonitor/getNumericColumnForTable',tableName).toPromise();
  }

  async getSLACntForUsr(usrData){
      return this.http.post<any>(this.domain+'/internalMonitor/getSLACntForUsr',usrData).toPromise();
  }

  async getThresholdBreachMetrics(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getThresholdBreachMetrics',metData).toPromise();
  }
  async getAVMThresholdBreachMetrics(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getAVMThresholdBreachMetrics',metData).toPromise();
  }
  async getLogThresholdBreachMetrics(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getLogThresholdBreachMetrics',metData).toPromise();
  }
  async getSUMThresholdBreachMetrics(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getSUMThresholdBreachMetrics',metData).toPromise();
  }
  async getRUMThresholdBreachMetrics(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getRUMThresholdBreachMetrics',metData).toPromise();
  }
  async getAlertSLADetails(metData){
      return this.http.post<any>(this.domain+'/internalMonitor/getAlertSLADetails',metData).toPromise();
  }
  async logoutUser(data) {
      return this.http.post<any>(this.domain+'/pgDbAuth/logoutUser',data).toPromise();
  }

  logout() {
      sessionStorage.clear();
  }

  loggedIn() {
      const token = this.jwtHelper.tokenGetter();
      if (!token) return false;
      return !this.jwtHelper.isTokenExpired();    
  }

  sortByKey(array, key) {
      return array.sort(function(a, b) {
          var x = a[key]; var y = b[key];
          return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      });
  }

  invalidSession(res: any, dispErr?: any) {
      let msg;
      if (res.invalidToken) {
          msg = 'Session expired, please login again.';
          this.logout();
          this.router.navigate(['/login']);
      } else if (!res.success) {
          if (res.message != undefined){
              if (res.message.includes("duplicate")){
                  msg = "Already Exists, use different name and try again";
              } else {
                  let pos = res.message.indexOf(' at ');
                  msg = pos > 0 ? res.message.substring(0,pos) : res.message; 
              }
          } else {
              msg ="Some of the operation failed. please see the server log for more details";
          }
      } else {
          msg = res.message
      }
      return msg;
  }

  pubSetDTFormat(stDate: any, endDate: any) {
      let timeDiff =  endDate - stDate;
      if (timeDiff <= 3*60*60*1000){
          let setParam = {qryInterval: '1 MINUTES',dtFormat:'MINUTES', qryType:'query',aggQryTableName:'_',dateTrunc_ms:60};
          return setParam;
      } else if (timeDiff > 3 * 60 * 60 * 1000 && timeDiff <= 12 * 60 * 60 * 1000) {
        //bet 3hours and 12 hours
        let setParam = {qryInterval: '5 MINUTES',dtFormat:'MINUTES', qryType:'aggr_query',aggQryTableName:'_aggr_5min_',dateTrunc_ms:300};
        return setParam;
      } else if (timeDiff > 12 * 60 * 60 * 1000 && timeDiff <= 48 * 60 * 60 * 1000) {
          //between 12 hours and 48 hours
          let setParam = {qryInterval: '15 MINUTES',dtFormat:'MINUTES', qryType:'aggr_query',aggQryTableName:'_aggr_15min_',dateTrunc_ms:900};
          return setParam;
      } else if (timeDiff > 48 * 60 * 60 * 1000 && timeDiff <= 96 * 60 * 60 * 1000) {
          //between 48 hours and 96 hours
          let setParam = {qryInterval: '30 MINUTES',dtFormat:'MINUTES', qryType:'aggr_query',aggQryTableName:'_aggr_30min_',dateTrunc_ms:180};
          return setParam;
      } else {
          //greater than 96 hours
          let setParam = {qryInterval: '1 HOUR',dtFormat:'HOUR', qryType:'aggr_query',aggQryTableName:'_aggr_1hr_',dateTrunc_ms:3600};
          return setParam;
      }
  }

  async getMetricUnit(param){
      return this.http.post<any>(this.domain+'/internalMonitor/getMetricUnit',param).toPromise();
  }

  async updateAddAlertsEmailSms(param){
      return this.http.post<any>(this.domain+'/internalMonitor/updateAddAlertsEmailSms',param).toPromise();
  }

  async resendOTP(param){
      return this.http.post<any>(this.domain+'/internalMonitor/resendOTP',param).toPromise();
  }

  async removeAlertsEmailSms(param){
      return this.http.post<any>(this.domain+'/internalMonitor/removeAlertsEmailSms',param).toPromise();
  }

  async validateOTP(param){
      return this.http.post<any>(this.domain+'/internalMonitor/validateOTP',param).toPromise();
  }

  async getAlertsEmailSms(param){
      return this.http.post<any>(this.domain+'/internalMonitor/getAlertsEmailSms',param).toPromise();
  }

  async addUpdateAlertSettings(param){
      return this.http.post<any>(this.domain+'/internalMonitor/addUpdateAlertSettings',param).toPromise();
  }

  async getScriptSettings(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getScriptSettings',param).toPromise();
  }

  async deleteJmeterScenarios(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/deleteJmeterScenarios',param).toPromise();
  }

  async updateJMeterScenarioSettings(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updateJMeterScenarioSettings',param).toPromise();
  }

  async getAlertSettings(param){
      return this.http.post<any>(this.domain+'/internalMonitor/getAlertSettings',param).toPromise();
  }

  async pubGetChartQuery(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartQuery',param).toPromise();
  }

  async pubGetQueryData(param,chart){
      let query;
      let connectionDetails;
      let engineName;
      if (param.qryType == "query"){
        query = chart.query;
        connectionDetails = chart.qry_connection_details;
        engineName = chart.qry_engine_name;
      } else {
        if (chart.aggr_query != null && chart.aggr_query.length>0){
          query = chart.aggr_query;
          connectionDetails = chart.aggr_connection_details;
          engineName = chart.aggr_engine_name;
        } else {
          query = chart.query;
          connectionDetails = chart.qry_connection_details;
          engineName = chart.qry_engine_name;
        }
      }
      query = query.replace(/@dateTrunc@/gi, param.dtFormat).replace(/@timeInterval@/gi,param.qryInterval).replace(/@startDate@/gi,param.stTime).replace(/@endDate@/gi, param.endTime).replace(/@counterId@/gi, chart.ref_table_pkey_id).replace(/@aggr_date@/gi,param.aggQryTableName).replace(/@dateTrunc_ms@/gi,param.dateTrunc_ms/1000);
      let data = {
          query: query,
          connection_details: connectionDetails,
      };
      let res = this.encrypt(JSON.stringify(data));
      let qryParam = {data : res};
      if (engineName == 'postgresql'){
          return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataPsql',qryParam).toPromise();
      } else if (engineName == 'mssql') {
          return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataMssql',qryParam);
      } else {
          return false;
      }
  }

  async pubGetChartDataByChartId(chartData) {
      return this.http.post<any>(this.domain+'/pgDbAuth/getChartDataByChartId',chartData).toPromise();
  }

  async getASMetricDetails(param) {
      return this.http.post<any>(this.domain+'/internalMonitor/getASMetricDetails',param).toPromise();
  }

  async getUIDsForBreach(param) {
      return this.http.post<any>(this.domain+'/internalMonitor/getUIDsForBreach',param).toPromise();
  }

  async removeAlert(param) {
      return this.http.post<any>(this.domain+'/externalMonitor/removeAlert',param).toPromise();
  }

  async getProtocolByUserid(param) {
      return this.http.post<any>(this.domain+'/internalMonitor/getProtocolByUserid',param).toPromise();
  }

  async getNetworkStatusByProtocol(param) {
      return this.http.post<any>(this.domain+'/internalMonitor/getNetworkStatusByProtocol',param).toPromise();
  }

  async getNetworkPatternByUid(gpData){
      return this.http.post<any>(this.domain+'/internalMonitor/getNetworkPatternByUid',gpData).toPromise();;
  }

  async getLogAlerts(param){
      return this.http.post<any>(this.domain+'/internalMonitor/getLogAlerts', param).toPromise();;
  }

  async getAPMAlerts(param){
      return this.http.post<any>(this.domain+'/internalMonitor/getAPMAlerts',param).toPromise();;
  }

  async getRumBreachSettings(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getRumBreachSettings', mmData).toPromise();
  }

  async getMMCardRum(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getMMCardRum', mmData).toPromise();
  }

  async getLogAlertsByUid(mmData){
      return this.http.post<any>(this.domain+'/internalMonitor/getLogAlertsByUid', mmData).toPromise();
  }
      
  async getLogCard(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getLogCard', mmData).toPromise();
  }

  async deleteDBConn(dbData){
      return this.http.post<any>(this.domain+'/pgDbAuth/deleteDBConn', dbData).toPromise();
  }

  async getMMCardV2(mmData){
      return this.http.post<any>(this.domain+'/pgDbAuth/getMMCardV2', mmData).toPromise();
  }

  getAPMAlertsForModule(mmData){
      return this.http.post<any>(this.domain+'/internalMonitor/getAPMAlertsForModule', mmData);
  }

  getRumAlertsForUser(mmData){
      return this.http.post<any>(this.domain+'/internalMonitor/getRumAlertsForUser', mmData);
  }

  getAlertsCntByTable(mmData){
      return this.http.post<any>(this.domain+'/internalMonitor/getAlertsCntByTable', mmData);
  }

  async getMyEnterprises(){
      return this.http.get<any>(this.domain+'/pgDbAuth/getMyEnterprises').toPromise();
  }

  async updCustQryEntMap(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updCustQryEntMap', param).toPromise();
  }

  async updDBEntMap(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updDBEntMap', param).toPromise();
  }

  async getMappedEntName(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getMappedEntName', param).toPromise();
  }

  async updMyChartGridParam(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/updMyChartGridParam', param).toPromise();
  }

  async getPolicyCntByUid(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getPolicyCntByUid', param).toPromise();
  }

  async getNetstackChart(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getNetstackChart', param).toPromise();
  }

  async getNetstackChartByChartId(param){
      return this.http.post<any>(this.domain+'/pgDbAuth/getNetstackChartByChartId', param).toPromise();
  }

  async processTimeSeriesData(ptData){
      if (ptData.chData.length == 0){
          return ptData.chData;
      }
      let arrData = ptData.chData;
      let keyArr = Object.keys(arrData[0]);
      let dateTimeArr = arrData.map((data)=>{
          //added this line to ensure even when date is in format other than epoch //13-Nov-2019 2pm. SK
          data[keyArr[0]] = new Date(data[keyArr[0]]).getTime(); 
          return data[keyArr[0]];
      })
      let stTime = ptData.stTime*1000;
      let endTime = ptData.endTime*1000;
      let interval = ptData.interval*1000;
      let missingArr = [];
      let i = stTime;
      while (i<endTime){
          if (dateTimeArr.indexOf(i)==-1){
              missingArr.push(i)
          }
          i = i+interval;
      }
      missingArr.map(ele => {
          let newEle = JSON.parse(JSON.stringify(arrData[0])); //to ensure no reference is kept (clone)
          // let newEle = _.clone(arrData[0]);
          // let newEle = arrData.slice(0,1)[0];
          arrData.push(newEle);
          keyArr.map((key,j) => {
              if (j==0){
                  arrData[arrData.length-1][key] = ele;
              } else 
              arrData[arrData.length-1][key] = null;
          })
      });
      // console.log("missing element count "+ missingArr.length);
      // arrData = _.sortBy(arrData,[keyArr[0]]); loadash removed from the package
      arrData = arrData.sort((a,b) => 0 - (a[keyArr[0]]>b[keyArr[0]] ? 1 : -1));
      return arrData;
  }

  getDbConnectorForCV(){
      return this.http.get<any>(this.domain+'/pgDbAuth/getDbConnectorForCV');
  }

  async getLogDetailsRowCount(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getLogDetailsRowCount', param).toPromise();
  }

  async delUser(param){
    return this.http.post<any>(this.domain + '/pgDbAuth/delUser', param).toPromise();
}

  async getUserList(){
    return this.http.get<any>(this.domain + '/pgDbAuth/getUserList').toPromise();
}

  async getLogDetails(param){
      return this.http.post<any>(this.domain + '/pgDbAuth/getLogDetails', param).toPromise();
  }

  getURL(param){
      //{responseType: "text" as 'json'}
      return this.http.post<any>(this.domain + '/extReq/getUrl', param).toPromise();
  } 

  //used in netstack-charts.components.ts
  processTimeSeriesDataModified(chData, startTimeInSec, endTimeInSec, timeIntervalInSec){
    if (chData.length == 0){
        return chData;
    }
    let arrData = chData;
    let keyArr = Object.keys(arrData[0]);
    let dateTimeArr = arrData.map((data)=>{
        //added this line to ensure even when date is in format other than epoch //13-Nov-2019 2pm. SK
        data[keyArr[0]] = new Date(data[keyArr[0]]).getTime(); 
        return data[keyArr[0]];
    })
    let stTime = (startTimeInSec -  startTimeInSec%timeIntervalInSec)*1000
    let endTime = (endTimeInSec - endTimeInSec%timeIntervalInSec)*1000;
    let interval = timeIntervalInSec*1000;
    let missingArr = [];
    let i = stTime;
    while (i<endTime){
        if (dateTimeArr.indexOf(i)==-1){
            missingArr.push(i)
        }
        i = i+interval;
    }
    missingArr.map(ele => {
        let newEle = JSON.parse(JSON.stringify(arrData[0])); //to ensure no reference is kept (clone)
        arrData.push(newEle);
        keyArr.map((key,j) => {
            if (j==0){
                arrData[arrData.length-1][key] = ele;
            } else 
            arrData[arrData.length-1][key] = null;
        })
    });
    let newData = arrData.sort((a,b) => 0 - (a[keyArr[0]]>b[keyArr[0]] ? 1 : -1));
    // let newData = _.sortBy(arrData,[function(o){return o[keyArr[0]]}]); old loadash model
    return newData;
  }
}
