import { Injectable } from '@angular/core';
import {Subject} from 'rxjs'; 

@Injectable()
export class CommonService {
  invokeEvent: Subject<any> = new Subject(); 

  callHeader() { 
      console.log("callHeader called");
    this.invokeEvent.next('header') ;     
  }
}