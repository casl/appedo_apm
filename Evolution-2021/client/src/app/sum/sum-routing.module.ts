import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SumComponent } from './sum.component';

const routes: Routes = [
  {
    path: '',
    component: SumComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class SumRoutingModule { }

