import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { SumRoutingModule } from './sum-routing.module';
import { SumComponent } from './sum.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ModuleDetailsModule } from '../module-details/module-details.module';

@NgModule({
  imports: [
    CommonModule,
    SumRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    ModuleDetailsModule,
  ],
  declarations: [
    SumComponent
  ],
  entryComponents: [
    
  ],
})

export class SumModule { }

