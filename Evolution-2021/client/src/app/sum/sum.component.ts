import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators, FormControlName} from '@angular/forms';
import { AuthenticationService } from '../_services/index';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import * as _ from 'lodash';
import { ReusableComponent } from '../reusable/reusable.component';


@Component({
  selector: 'app-sum',
  templateUrl: './sum.component.html',
  styleUrls: ['./sum.component.scss']
})
export class SumComponent implements OnInit {
  isLoading = false;
  pageTitle:String;
  test_id;
  sumData; runEveryTypes;
  iframeLoaded = false; dispIframe = "none";
  returnURL:string;
  screenChange:any;
  toggleKeyMetrics:boolean=true
  startDt: Date; endDt: Date; 

  constructor(
    private route: Router,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private reusable: ReusableComponent,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.authService.screenChange.subscribe(res =>{
      this.screenChange = res;
    });
    this.returnURL = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/sum");
    this.isLoading = true;
    this.pageTitle = 'Sum Details';
    this.sumData = {};
    let sumLocalData = sessionStorage.getItem('sumData');
    this.runEveryTypes=[{disp:'15 Mins', value:15}, {disp:'30 Mins', value:30}, {disp:'45 Mins', value:45}, {disp:'1 Hr', value:60}, {disp:'2 Hrs', value:120}, {disp:'4 Hrs', value:240},{disp:'8 Hrs', value:480}, {disp:'12 Hrs', value:720}, {disp:'24 Hrs', value:1440}];
    if (sumLocalData == undefined) {
      this.isLoading = false;
      if (this.returnURL != undefined){
        this.route.navigate([this.returnURL]);
      } else {
        this.route.navigate(['home'])
      }
    } else {
      this.sumData = JSON.parse(sumLocalData);
      this.sumData.sumCardData.runEveryDispVal = _.find(this.runEveryTypes, {'value': this.sumData.sumCardData.runevery}).disp;
      this.startDt = this.sumData.dateTime.startDt;
      this.endDt = this.sumData.dateTime.endDt;
      this.loadSumMeasurementData();
      this.isLoading = false;
      this.onDataPointClick(this.sumData.selRowData.harfilename, this.sumData.selRowData.date_time, this.sumData.selRowData[0], location, this.sumData.selRowData.id, this.sumData.sumCardData.testtype)
    }
  }

  getFirstByte(harId, testType) {
    let reqData = {
      harId: harId,
      testType: testType
    };
    this.authService.getFirstByte(reqData).subscribe( respData => {
      if (respData.success){
        this.sumData.firstByte = respData.result;
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(respData),"error");
      }
    },
    error => {
      this.reusable.openAlertMsg(this.authService.invalidSession(error),"error");
    });
  }

  fetchScreen(harId) {
    let reqData={harId: harId};
    this.authService.fetchScreen(reqData).subscribe( respData => {
      if (respData.success){
        let urlData = respData.result;
        for (let i=0; i<Object.keys(urlData).length; i++) {
          var key = Object.keys(urlData)[i];
          this.sumData.urls[key] = this.sanitizer.bypassSecurityTrustResourceUrl(urlData[key]);
        }
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(respData),"error");
      }
    },
    error => {
      this.reusable.openAlertMsg(this.authService.invalidSession(error),"error");
    });
  }

  onDataPointClick(harFileName, time, respTime, location, id, testType) {
    this.sumData.urls = {};
    this.sumData.firstByte = {};
    let param = {
      test_id : this.sumData.sumCardData.test_id,
      harFileName: harFileName
    }
    this.authService.checkValidHarURL(param).subscribe(result => {
      this.authService.toggleMenu.next(false);
      this.isLoading = false;
      if (result.success){
        this.sumData.urls.harUrl=this.sanitizer.bypassSecurityTrustResourceUrl(result.url);
        this.iframeLoaded = false;
        this.dispIframe = "none";
        this.getFirstByte(id, testType.toLowerCase());
        this.fetchScreen(id);
        setTimeout(() => {
          this.iframeLoaded = true;
          this.dispIframe = "block";
        }, 10000);
      } else {
        this.reusable.openAlertMsg(result.message,"error");
      }
    }, error => {
        this.isLoading=false;
        this.reusable.openAlertMsg(error.message,"error");
    })
  }

  goBack() {
    sessionStorage.removeItem("sumData");
    this.route.navigate([this.returnURL]);
  }

  loadSumMeasurementData() {
    let entObj= JSON.parse(sessionStorage.getItem('entItem'));
    let ownerId=entObj.is_owner? undefined : entObj.owner_id;
    let reqData={owner_id: ownerId, testId: this.sumData.sumCardData.test_id, startDateSec: this.startDt, endDateSec: this.endDt};
    this.authService.getSumMeasurementData(reqData).subscribe( respData => {
      if (respData.success){
        this.sumData.measurementData = respData.result[0];
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(respData),"error");
      }
    },error => {
      this.reusable.openAlertMsg(this.authService.invalidSession(error),"error");
    });
  }
}
