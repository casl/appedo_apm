import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../_services/index';
import { ReusableComponent } from '../../reusable/reusable.component';
import { LoginHeaderComponent } from '../../login-header/login-header.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [LoginHeaderComponent]
})
export class RegisterComponent implements OnInit {
  isLoading = false; checkEmail = false; hide=true;
	showRegisterForm = false;
	passwordReqData = {};
	SlaReqData = {};

	form: FormGroup;
  modPassword;

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='email') {msg += control.hasError('email') ? 'Not a valid email' :''}
    if (controlName =='fname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 3 & 50 char length': ''}
    if (controlName =='lname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 1 & 50 char length': ''}
    if (controlName =='lname') {msg += (control.errors.validateName) ? 'Must be valid characters. Special Characters/Numbers are not allowed': ''}
    if (controlName =='fname') {msg += (control.errors.validateName) ? 'Must be valid characters. Special Characters/Numbers are not allowed': ''}
    if (controlName =='mobile_no') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 13 & 15 length includes country code': ''}
    if (controlName =='mobile_no') {msg += !control.value.startsWith('+') ? 'Must start with +': ''}
    if (controlName =='mobile_no') {msg += control.hasError('validateMobile') ? 'Only Numeric Fields Allowed' :''}
	if (controlName =='form') {msg += control.hasError('matchingPasswords') ? 'Password, confirm password must be same' :''}
    return msg;
	}

	createForm() {
		this.form = this.formBuilder.group({
			email:['',Validators.compose([
        Validators.required,
        Validators.email
			])],
			fname:['',Validators.compose([
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(50),
				this.validateName
			])],
			lname:['',Validators.compose([
				Validators.required,
				Validators.minLength(1),
				Validators.maxLength(50),
				this.validateName
			])],
			password:['',Validators.compose([
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(20)
			])],
			confirm:['',Validators.required],
			mobile_no:['+',Validators.compose([
				Validators.required,
				Validators.minLength(13),
				Validators.maxLength(15),
				this.validateMobile
			])]
		},
			{validator: this.matchingPasswords('password', 'confirm')}
		);	
	}

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private authService: AuthenticationService,
		private reusable: ReusableComponent,
		private formBuilder: FormBuilder) {
		this.createForm();
	}

	ngOnInit() {
		if(this.activatedRoute.snapshot.paramMap.has('query')){
			this.SlaReqData = this.activatedRoute.snapshot.paramMap.get('query');
			this.VerifySlaEmailAddress(this.SlaReqData);
		}else if (this.activatedRoute.snapshot.paramMap.has('data')) {
			this.showRegisterForm = true;
			this.isLoading = true;
			this.passwordReqData = this.activatedRoute.snapshot.paramMap.get('data');
			this.verifySignUp(this.passwordReqData);
		} else {
		this.showRegisterForm = true;
		}
	}

	VerifySlaEmailAddress(query){
		let paramData={
			data: this.SlaReqData
		};
		this.authService.verifySlaEmail(paramData).subscribe(data => {
			this.isLoading = false;
			if (data.success) {
        this.reusable.openAlertMsg(data.message,"info");
				this.router.navigate(['/login']);
			} else {
        this.reusable.openAlertMsg(data.message,"error");
				this.router.navigate(['/login']);
			}
		},
		error => {
			this.isLoading = false;
	    	this.reusable.openAlertMsg(error.message,"error");
			// this.router.navigate(['/login']);
		});
	}

	verifySignUp(data) {
		let paramData={
			data: this.passwordReqData
		};
		this.authService.verifySignUp(paramData).subscribe(data => {
			this.isLoading = false;
			if (data.success) {
        this.reusable.openAlertMsg(data.message,"info");
				this.router.navigate(['/login']);
			} else {
        this.reusable.openAlertMsg(data.message,"info");
				this.router.navigate(['/login']);
			}
		},error => {
			this.isLoading = false;
	       this.reusable.openAlertMsg(error.message,"error");
			this.router.navigate(['/login']);
		});
	}

	validateName(controls){
		const reqExp = new RegExp(/[a-z][A-Z]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
	}

	validateEmail(controls){
		const reqExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateEmail' : true};
		}
	}

	matchingPasswords(pass,conf){
		return (group: FormGroup) => {
			if(group.controls[pass].value === group.controls[conf].value){
				return null;
			} else{
				return {'matchingPasswords' : true};
			}
		}
	}
	validateMobile(controls){
		const reqExp = new RegExp(/^[+][0-9]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateMobile' : true};
		}
	}
	  
	async validateEmailSignUp(){
		if (this.form.get("email").status == "VALID"){
			let emailData = {email: this.form.get("email").value};
			let result = await this.authService.validateEmailSignUp(emailData);
			if (result.success) {this.checkEmail = true; return true;}
			else {this.checkEmail = false; return false;}
		}
	}
	
  async register() {
	if (this.form.valid && this.checkEmail){
		this.isLoading = true;
		const user = {
			email:this.form.get('email').value,
			first_name: this.form.get('fname').value,
			last_name: this.form.get('lname').value,
			password: this.form.get('password').value,
			license_level: 'level0',
			mobile: this.form.get('mobile_no').value,
			operation: 'signUpVerificationMail',
			telephone_code: '+91'
		};
		let res = await this.validateEmailSignUp();
		if (res){
			const usr = {data:this.authService.encrypt(JSON.stringify(user))};
			let data = await this.authService.registerUser(usr);
			this.isLoading = false;
			if (data.success){
				this.reusable.openAlertMsg(data.message,"info");
					this.router.navigate(['/login',{ skipLocationChange: true }]);
			} else {
				this.reusable.openAlertMsg(this.authService.invalidSession(data),"error");
			}
		} else {
			this.isLoading = false;
			this.reusable.openAlertMsg("Email Already Exists","info");
		}
	} else {
		this.reusable.openAlertMsg("Form is not valid, please check for errors","info");
	}
  }
}
