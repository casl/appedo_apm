import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register/register.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderModule } from '../login-header/login-header.module'
// import { MaterialModule } from '../material';


@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
    LoginHeaderModule
  ],
  declarations: [
    RegisterComponent,
    // LoginHeaderComponent,
    // MaterialModule
  ],
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class RegisterModule { }

