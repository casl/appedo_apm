import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { Unsubscribable, timer } from 'rxjs';
import { filter as _filter, noop as _noop, find as _find } from 'lodash';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {NestedTreeControl} from '@angular/cdk/tree';

interface FunctionTree{
  id : number;
  hierarchy: number;
  data: any;
  children: FunctionTree[];
}

@Component({
  selector: 'app-netstack-charts',
  templateUrl: './netstack-charts.component.html',
  styleUrls: ['./netstack-charts.component.css']
})
export class NetstackChartsComponent implements OnInit {
  isLoading = false;
  returnURL:string;
  screenWidthHt:any;
  stEndDate={};
  callFromSubscribe = false;
  uid: BigInteger;
  uidLabel: string;
  isRefreshing = false; enableRefresh = true; refreshTime = 0; autoRefreshEnabled = true; startRefreshTime= false; setRefreshTime = 60;
  private timerSubscription: Unsubscribable;
  private secondsTimer: Unsubscribable;
  screenAttrib: Unsubscribable;
  setIntervalId;
  d3ChartData=[];chartWidth=[]; chartHt=[];d3RootChartType=[]; chartTitle =[];
  screenParam: any;
  theme: string;
  treeCollTitle; secondDDColl=[];
  treeDataColl : FunctionTree[];
  treeControl = new NestedTreeControl<FunctionTree>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FunctionTree>();
  hasChild = (_: number, node: FunctionTree) => !!node.children && node.children.length > 0;

  constructor(
    private route: Router,
    private authService: AuthenticationService,
  ) { 
    this.dataSource.data = this.treeDataColl;
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.timerSubscription != undefined) {
        this.timerSubscription.unsubscribe();
    }

    if (this.secondsTimer != undefined) {
      this.secondsTimer.unsubscribe();
    }
    if (this.screenAttrib != undefined){
      this.screenAttrib.unsubscribe();
    }
    this.authService.myDashSelVal.next("--my dashboard--");
    this.authService.dispOnRequest.next(false);
    this.authService.dispMyChart.next(false);
  }

  ngOnInit() {
    this.authService.dispOnRequest.next(true);
    this.authService.dispMyChart.next(false);
    this.authService.dispEnt.next(false);
    this.returnURL = sessionStorage.getItem("currentRoute");

    setTimeout(() => {
      this.authService.toggleMenu.next(false);
    }, 2000);

    if(!this.returnURL.includes('sum') && !this.returnURL.includes('drilldown')){
      sessionStorage.setItem("parentRoute",this.returnURL);
    }
    sessionStorage.setItem("currentRoute","/home/netstackcharts");
    let uidData = sessionStorage.getItem('netstackChart') != undefined ? JSON.parse(sessionStorage.getItem('netstackChart')) :undefined;
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    })
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
      if (this.screenParam.height != 0 && this.screenParam.width != 0  ){
        setTimeout(() => {
          this.chartWidth.map((w,ix) => {
            if (this.screenParam.width < 750){
              this.chartWidth[ix] = (this.screenParam.width-30);
            } else {
              if (ix != 2){
                this.chartWidth[ix] = (this.screenParam.width-60)/2;
              }
              else {
                this.chartWidth[ix] = (this.screenParam.width-30);
              }
            }
          });
        }, 2000);
      }
    });
    this.authService.dateEpoch.subscribe(dtColl => {
      this.stEndDate = dtColl;
      if (!this.callFromSubscribe && this.uid != undefined){
        this.callFromSubscribe = true;
        this.callGetDashboard();
      }
    });
    let now = new Date();
    this.stEndDate = {stDate: new Date(now.getFullYear(), now.getMonth(),now.getDate(),now.getHours()-1, now.getMinutes(), now.getSeconds()), endDate: now};
    if (uidData != undefined){
      this.uidLabel = uidData.uid +"::"+ uidData.module_name;
      this.uid = uidData.uid;
    }
    if (!this.callFromSubscribe){
      this.callFromSubscribe = true;
      this.callGetDashboard();
    }
    this.startAutoRefresh();
  }
  ddparams = [];
  drillDownEvent = []
  async fromD3chart(event,idx){
    this.dataSource.data = [];
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let ddparams = {};
    try {
      event.chartData.dd_param = JSON.parse(event.chartData.dd_param);
    } 
    catch {
      //nothing to be done.
    }
    this.drillDownEvent[0] = [];
    this.drillDownEvent[idx+1] = event;
    if (this.ddparams[idx+1] == undefined) this.ddparams[idx+1] = {};
    Object.values(event.chartData.dd_param).filter((item, i) => {
      if (item != 'none') {
        this.ddparams[idx+1][item.toString()] = Object.keys(event.chartData.dd_param)[i] ;
      }
    });
    if (idx == 0){
      await this.netstackResponseTimeChart(event, this.ddparams[idx+1], entItem, idx);
    } else {
      await this.callTreeView(event, this.ddparams[idx+1], entItem, idx);
    }
  }
  
  private  async callTreeView(event:any, ddparams:{}, entItem:any, idx:number ){
    //When working with 1 day and more with high data volume, this processing is consuming cpu of browser and time. This is one of the reason isLoading is made to false only at the end. 
    let param = {
      functionName: event.selRowData.function_name,
      chartId: event.ddMetricId,
      startTime: (event.selRowData.date_time - event.timeDiff),
      endTime: event.selRowData.date_time,
      dateTrunc_ms: event.timeDiff,
      ownerId: entItem.owner_id == null ? -1 : entItem.owner_id
    };
    this.isLoading = true;
    let result = await this.authService.getNetstackChartByChartId(param);
    if (result.success && result.rowCount > 0 ) {
      let res = await this.authService.decrypt(result.result);
      this.secondDDColl[0] = JSON.parse(res);
      this.treeDataColl = [];
      let whereInserted = '';
      let parentNode; 
      let childNode;
      let collCountPerArr=0;
      this.secondDDColl[0].data.map((ele,idx)=>{
        collCountPerArr++;
        if (idx == 0 || ele.r_hierarchy == 0 ){
          this.treeDataColl.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
          whereInserted = 'root';
          if (idx!=0){
            collCountPerArr = 1;
          }
        } else {
          let searchEle;
          let i = 0;
          if (this.secondDDColl[0].data[idx-1].r_hierarchy == ele.r_hierarchy-1){
            if (whereInserted == 'root'){
              searchEle = _find(this.treeDataColl,{'id':idx-1});
              searchEle.children.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
              let parentLength = this.treeDataColl.length;
              let childrenLength = this.treeDataColl[parentLength-1].children.length;
              childNode = this.treeDataColl[parentLength-1].children[childrenLength-1].children;
              parentNode = this.treeDataColl[this.treeDataColl.length-1].children;
              whereInserted = 'children';
            } else {
              try{
                searchEle = _find(parentNode,{'id':idx-1});     
                searchEle.children.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
                parentNode = childNode;
                childNode = childNode[childNode.length-1].children;
              } catch {}
            }
          } else if (this.secondDDColl[0].data[idx-1].r_hierarchy == ele.r_hierarchy) {
            parentNode.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
            childNode = parentNode[parentNode.length-1].children;
            whereInserted = 'children';
          } else if (this.secondDDColl[0].data[idx-1].r_hierarchy != ele.r_hierarchy){
            whereInserted = 'children';
            let parentLength = this.treeDataColl.length;
            searchEle = _find(this.treeDataColl, {'id':idx-1});
            let proceed = true;
            if (searchEle == undefined){
              if (proceed){
                parentNode = this.treeDataColl[parentLength-1].children;
                let searchSuccess = true;
                for (let j = 1; j < collCountPerArr-1; j++){
                  // console.log('idx',idx,'parentNode',parentNode, j, collCountPerArr, ele.r_hierarchy, this.secondDDColl[0].data[idx-1].r_hierarchy, parentLength,this.treeDataColl);
                  searchEle = _find(parentNode, {'hierarchy':ele.r_hierarchy-1});
                  if (searchEle == undefined){
                    searchSuccess = false;
                    try{
                      parentNode = parentNode[parentNode.length-1].children == undefined ? parentNode : parentNode[parentNode.length-1].children;
                    } catch{}
                  } else {
                    searchSuccess = true;
                    try {
                      searchEle.children.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
                      parentNode = parentNode[parentNode.length-1].children;
                      childNode = parentNode[parentNode.length-1].children;
                    } catch {}
                    break;
                  }
                } 
                if (!searchSuccess){
                  parentNode = this.treeDataColl[parentLength-1].children;
                  parentNode.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy})
                  parentNode = parentNode;
                  childNode = parentNode[parentNode.length-1].children;
                  proceed = false
                }
              }
            } else {
              searchEle.children.push({id: idx, data: ele, children: [], hierarchy:ele.r_hierarchy});
              let childrenLength = this.treeDataColl[parentLength-1].children.length;
              childNode = this.treeDataColl[parentLength-1].children[childrenLength-1].children;
              parentNode = this.treeDataColl[this.treeDataColl.length-1].children;
            }
          } else {
            console.log(idx,this.secondDDColl[0].data[idx-1].r_hierarchy,ele.r_hierarchy,'inside r_hierarchy above 1', parentNode, childNode);
            console.log("No code written for this condition");
          }
        }
      });
      let stDate = new Date(param.startTime);
      let endDate = new Date(param.endTime);
        let fromDateTime = new Date().getDate() - stDate.getDate() == 0 ? stDate.toLocaleTimeString() : stDate.getFullYear() + "-" + stDate.getMonth() + 1 + "-" + stDate.getDate() + " " + stDate.getHours() + ":" + stDate.getMinutes();
      let toDateTime = new Date().getDate() - endDate.getDate() == 0 ? endDate.toLocaleTimeString() : endDate.getFullYear() + "-" + endDate.getMonth() + 1 + "-" + endDate.getDate() + " " + endDate.getHours() + ":" + endDate.getMinutes();
        this.treeCollTitle = "Tree view between "+fromDateTime+" and "+toDateTime+" for "+param.functionName;
      this.dataSource.data = this.treeDataColl;
      console.log(this.dataSource.data);
      this.chartWidth[idx + 1] = this.screenParam == undefined || this.screenParam.width == 0 ? window.screen.availWidth - 60 :this.screenParam.width-30;
    } else if (result.rowCount == 0){
      this.dataSource.data = [];
    } else {
      this.authService.invalidSession(result);
    }
    this.isLoading = false;
  }

  private async netstackResponseTimeChart(event: any, ddparams: {}, entItem: any, idx: any) {
    let dateParam = this.authService.pubSetDTFormat(new Date(this.stEndDate["stDate"]).getTime(), new Date(this.stEndDate["endDate"]).getTime());
    let param = {
      functionName: event.function_name,
      chartId: event.ddMetricId,
      startTime: this.stEndDate['stDate'],
      endTime: this.stEndDate['endDate'],
      param1: ddparams['param1'],
      dateTrunc_ms: dateParam.dateTrunc_ms,
      ownerId: entItem.owner_id == null ? -1 : entItem.owner_id
    };
    this.isLoading = true;
    let result = await this.authService.getNetstackChartByChartId(param);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      let res = await this.authService.decrypt(result.result);
      let cvData = JSON.parse(res);
      let stDate = new Date(this.stEndDate["stDate"]);
      let endDate = new Date(this.stEndDate["endDate"]);
      //let fromDateTime = new Date().getDate() - stDate.getDate() == 0 ? stDate.toLocaleTimeString() : stDate.getFullYear() + "-" + stDate.getMonth() + 1 + "-" + stDate.getDate() + " " + stDate.getHours() + ":" + stDate.getMinutes();
      //let toDateTime = new Date().getDate() - endDate.getDate() == 0 ? endDate.toLocaleTimeString() : endDate.getFullYear() + "-" + endDate.getMonth() + 1 + "-" + endDate.getDate() + " " + endDate.getHours() + ":" + endDate.getMinutes();
      //this.chartTitle[1] = event.function_name + " From " + fromDateTime + " To " + toDateTime;
      this.chartTitle[1] = event.function_name;
      try {
        cvData.chart_types_json = JSON.parse(cvData.chart_types_json);
      }
      catch {
        //if failed do nothing. this is requried for new chart visual.
      }
      try {
        cvData.col_units_json = JSON.parse(cvData.col_units_json);
      }
      catch {
        //if failed do nothing. This is required for new chart visual 
      }
      //process time series data for missing node elements
      let timeFormat = this.authService.pubSetDTFormat(stDate.getTime(), endDate.getTime());
      let convData = this.authService.processTimeSeriesDataModified(cvData.data, stDate.getTime() / 1000, endDate.getTime() / 1000, timeFormat.dateTrunc_ms);
      cvData.data = convData;
      this.d3ChartData[idx + 1] = cvData;
      this.chartHt[idx + 1] = 250 + 40;
      this.d3RootChartType[idx + 1] = cvData.root_chart_type;
      this.chartWidth[idx + 1] = this.screenParam == undefined || this.screenParam.width == 0 ? window.screen.availWidth > 750 ?(window.screen.availWidth - 60) / 2 : window.screen.availWidth - 60 :this.screenParam.width > 750 ? (this.screenParam.width-60)/2 : this.screenParam.width-30;
    } else if (result.rowCount == 0){
      this.d3ChartData[idx + 1] = undefined;
    } else {
      this.authService.invalidSession(result);
    }
  }

  async callGetDashboard(){
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let param = {
      startTime: this.stEndDate['stDate'],
      endTime: this.stEndDate['endDate'],
      uid: this.uid,
      moduleType: "netstack",
      ownerId: entItem.owner_id == null ? -1 : entItem.owner_id
    };
    this.isLoading = true;
    let result = await this.authService.getNetstackChart(param);
    this.isLoading = false;
    this.callFromSubscribe = false;
    if (result.success && result.rowCount > 0){
      let res = await this.authService.decrypt(result.result);
      let cvData = JSON.parse(res);
      this.chartTitle[0] = cvData.chart_title + " "+ cvData.category +" ("+cvData.chart_id+")";
      try {
        cvData.chart_types_json = JSON.parse(cvData.chart_types_json);
      } catch {
        //if failed do nothing. this is requried for new chart visual.
      }
      try {
        cvData.col_units_json = JSON.parse(cvData.col_units_json);
      } catch {
        //if failed do nothing. This is required for new chart visual 
      }
      this.d3ChartData[0] = cvData; 
      // this.chartWidth[idx] = this.chartWidth[idx-1]
      this.chartHt[0] = result.rowCount * 28 + 40 < 280 ? 280 : result.rowCount * 28 + 40;
      this.d3RootChartType[0] = cvData.root_chart_type;
      this.chartWidth[0] = this.screenParam == undefined || this.screenParam.width == 0 ? window.screen.availWidth > 750 ?(window.screen.availWidth - 60) / 2 : window.screen.availWidth - 60 :this.screenParam.width > 750 ? (this.screenParam.width-60)/2 : this.screenParam.width-30;
      console.log("ChartHeight", this.chartHt, this.chartWidth, this.screenParam,window.screen.availWidth);
      let dateFormat = this.authService.pubSetDTFormat(new Date(this.stEndDate['stDate']).getTime(), new Date(this.stEndDate['endDate']).getTime() );
      this.setRefreshTime = dateFormat.dateTrunc_ms;
      this.startAutoRefresh();
    } else if (result.rowCount == 0 ){
      this.d3ChartData[0] = undefined;
    } else {
      this.authService.invalidSession(result);
    }
  }

  callCnt:number=0;

  convertSecToTime(intSec){
    return this.authService.convertSecToTime(intSec);
  }

  decrTimer(){
    this.refreshTime--;
  }

  startAutoRefresh(){
    if (this.autoRefreshEnabled) {
      this.startRefreshTime = true;
      this.refreshTime = this.setRefreshTime;
      if( this.timerSubscription != undefined){
        this.timerSubscription.unsubscribe();
      }
      if (this.secondsTimer != undefined){
        this.secondsTimer.unsubscribe();
      }
      this.timerSubscription = timer(this.refreshTime*1000,this.refreshTime*1000).subscribe(() => this.refreshData());
      this.secondsTimer = timer(1000,1000).subscribe(()=>this.decrTimer());
    }
  }

  refreshData(){
    if(!this.isLoading){
      this.stopRefTime();
      let now = new Date();
      let timeDiff = new Date(this.stEndDate["endDate"]).getTime() - new Date(this.stEndDate["stDate"]).getTime()
      this.stEndDate["endDate"] = now;
      this.stEndDate["stDate"] = new Date(now.getTime()-timeDiff);
      this.callGetDashboard();
      let entItem = JSON.parse(sessionStorage.getItem("entItem"));
      if (this.drillDownEvent[1] != undefined){
        this.netstackResponseTimeChart(this.drillDownEvent[1],this.ddparams[1],entItem,0);
      }
      this.refreshTime = this.setRefreshTime;
    }
  }

  stopRefTime() {
    this.refreshTime = 0;
    if( this.timerSubscription != undefined){
      this.timerSubscription.unsubscribe();
    }
    if (this.secondsTimer != undefined){
      this.secondsTimer.unsubscribe();
    }
    this.startRefreshTime = false;
  }

  goBack(){
    let parentRoute = sessionStorage.getItem("parentRoute");
    sessionStorage.removeItem('netstackChart');
    if (parentRoute != undefined){
      this.route.navigate([parentRoute]);  
      sessionStorage.removeItem('parentRoute');
    } else {
      this.route.navigate([this.returnURL]);
    }
  }
}
