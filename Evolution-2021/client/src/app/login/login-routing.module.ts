import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { AuthGuard } from '../_guards/index';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  // { 
  //   path: 'home', 
  //   canActivate:[AuthGuard], 
  //   loadChildren: () => import('../sidenav/sidenav.module').then(m => m.SidenavModule)
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }

