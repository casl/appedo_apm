import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthGuard } from '../_guards/auth.guard';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
	isLoading = false;
	returnUrl: string;
	hide = true;
	form: FormGroup;
	themeChange;

	email = new FormControl('', [Validators.required, Validators.email]);
	password = new FormControl('');

	getErrorMessage() {
	return this.email.hasError('required') ? 'You must enter a value' :
		this.email.hasError('email') ? 'Not a valid email' : '';
	}
	
	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authGuard: AuthGuard,
		private authenticationService: AuthenticationService,
		private reusable: ReusableComponent,
	){} 

	ht:number;
  	ngOnInit() {
		this.ht = window.innerHeight-64;
		if (this.authGuard.redirectUrl){
			this.returnUrl = this.authGuard.redirectUrl;
			this.authGuard.redirectUrl = undefined;
		} else {
			if(this.authenticationService.loggedIn()){
				this.navigation();
			}
		}
  	}
	
	private navigation() {
		let lastVisitedURL = sessionStorage.getItem("currentRoute");
		switch (lastVisitedURL) {
			case "home" :
				this.router.navigate(['home']);
			break
			case "home/dashboard":
				this.router.navigate(['home/dashboard']);
			break;
			case "home/moduledetails":
				this.router.navigate(['home/moduledetails']);
			break;
			case "home/profile":
				this.router.navigate(['home/profile']);
			break;
			case "home/alertconfig":
				this.router.navigate(['home/alertconfig']);
			break;
			case "home/enterprise":
				this.router.navigate(['home/enterprise']);
			break;
			case "home/cp":
				this.router.navigate(['home/cp']);
			break;
			case "home/extmonitors":
				this.router.navigate(['home/extmonitors']);
			break;
			case "home/visualizer":
				this.router.navigate(['home/visualizer']);
			break;
			case "home/systemdetails":
				this.router.navigate(['home/systemdetails']);
			break;
			default:
				console.log("inside home navigation");
				this.router.navigate(['home/systemdetails']);
		}
	}

  async login() {
	if (this.email.valid && this.password.valid){
		this.isLoading = true;
		const user ={
			email: this.email.value,
			password: this.password.value
		};
		const usr = {data:this.authenticationService.encrypt(JSON.stringify(user))};
		let data = await this.authenticationService.login(usr)
		if (data.success){
			this.authenticationService.storeSessionData(data.token, data.user);
			if (this.returnUrl){
				console.log("successfully logged in");
				this.router.navigate([this.returnUrl]);
			} else {
				console.log("successfully logged in");
				this.navigation();
			}
		} else {
			this.reusable.openAlertMsg(data.message,'error');
		}
		setTimeout(()=>{this.isLoading = false},2000); //to avoid login button getting enabled by the time navigation completes
	} else {
		this.reusable.openAlertMsg('Wrong input','warn');
	}
  }
}
