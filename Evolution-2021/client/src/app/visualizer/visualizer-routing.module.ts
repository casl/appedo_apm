import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisualizerComponent,dbAddEditQueryDialog, QryGridDialog} from './visualizer.component'
const routes: Routes = [
  {
    path: '',
    component: VisualizerComponent
  },
  {
    path: 'query',
    component: dbAddEditQueryDialog
  },
  {
    path: 'grdqry',
    component: QryGridDialog
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizerRoutingModule { }
