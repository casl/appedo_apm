import { Component, OnInit, ViewChild, Inject, ElementRef,ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, forkJoin, BehaviorSubject } from 'rxjs';
import { filter, map, distinctUntilChanged, mergeMap, debounceTime, catchError } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormBuilder, FormGroup, Validators, FormControlName, FormArray} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { filter as _filter, noop as _noop, uniqBy as _uniqBy } from 'lodash';
import { myChartDialog } from '../dashboard/dashboard.component';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';

export interface DialogDbAddEdit {
  dbId: string
}

export interface classDbEntMap {
  db_id: string,
  database: string,
  arr_eid: any,
  engine_name: string,
}

@Component({
  selector: 'app-visualizer',
  templateUrl: './visualizer.component.html',
  styleUrls: ['./visualizer.component.css']
})

export class VisualizerComponent implements OnInit {
  dbColl = [];
  entItem:any;
  usrDet:any;
  connDBParam: { dbId: any; };
  obsEnt:any;
  isLoading: any;
  mapDbEnt: classDbEntMap;
  theme:any;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    sessionStorage.setItem("currentRoute","/home/visualizer");
    this.usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
    //Whenever changes noticed to the enterprise selected item, the below service will get triggered. 
    this.obsEnt = this.authService.watchStorage().subscribe(data =>{
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
        this.getDatabase();
      }
    });
    this.getDatabase();
  }

  ngOnDestroy(){
    if (this.obsEnt != undefined)
      this.obsEnt.unsubscribe();
  }

  openMapEnt(db){
    this.mapDbEnt  = {db_id: db.db_id, arr_eid: db.arr_eid, database: db.database, engine_name:db.engine_name};
    this.openDBEntMapDialog();
  }

  openDBEntMapDialog(): void {
    const dialogRef = this.dialog.open(DBEntMapDialog, {
      width: '40%',
      data: {db_id: this.mapDbEnt.db_id, arr_eid: this.mapDbEnt.arr_eid, database: this.mapDbEnt.database, engine_name:this.mapDbEnt.engine_name}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getDatabase();
      }
    });
  }
  
  openQuery(db){
    sessionStorage.setItem("dbQuery",JSON.stringify(db));
    this.router.navigate(['home/visualizer/query']);
    this.authService.toggleMenu.next(false);
  }

  openGrdQuery(db){
    sessionStorage.setItem("dbQuery",JSON.stringify(db));
    this.router.navigate(['home/visualizer/grdqry']);
    this.authService.toggleMenu.next(false);
  }

  openDBConnAdd(){
    this.connDBParam = {dbId: null};
    this.openDBConnDialog();
  }

  openDBConnEdit(row){
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || this.usrDet.unq_id == this.entItem.owner_id){
      this.connDBParam = {dbId: row.db_id};
      this.openDBConnDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit DB Module Attributes","info");
    }
  }

  openDBConnDialog(): void {
    const dialogRef = this.dialog.open(dbAddEditDialog, {
      width: '40%',
      data: {dbId: this.connDBParam.dbId}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getDatabase();
      }
    });
  }

  async getDatabase(){
    let eId =  this.entItem.e_id == 0 ? null : this.entItem.e_id;
    let param = {e_id: eId};
    let dbColl = await this.authService.getDatabase(param);
    if (dbColl.success){
      dbColl.result.map(db => {
        if (db.engine_name.toLowerCase() == 'postgresql'){
          db.imgUrl = '../../assets/images/psql.png';
          db.title ="PostgreSQL"
        } else if (db.engine_name.toLowerCase() == 'mssql'){
          db.imgUrl = '../../assets/images/mssql.png';
          db.title ="MSSQL"
        } else if (db.engine_name.toLowerCase() == 'clickhouse'){
          db.imgUrl = '../../assets/images/clickhouse.png';
          db.title ="ClickHouse"
        }
      });
      this.dbColl = dbColl.result;
    } else {
      this.dbColl = [];
      this.reusable.openAlertMsg(this.authService.invalidSession(dbColl),"error");
    }
  }

  async delDBConn(dbDetails){
    if (this.entItem.e_id == 0 || this.entItem.e_id == null || this.usrDet.unq_id == this.entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of DB Connector "+dbDetails.database+"("+dbDetails.db_id+") \nDependent queries ("+dbDetails.cnt_qry+") will be deleted as well.");
      if (!confirmMessage){
        return;
      }
      this.isLoading = true;
      let param = {db_id: dbDetails.db_id}
      let result = await this.authService.deleteDBConn(param);
      this.isLoading = false;
      if (result.success){
        this.reusable.openAlertMsg(result.message, "info");
        this.getDatabase();      
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    } else {
      this.reusable.openAlertMsg("Only owner of this DB connector can delete","info");
    }
  }
}

/* Dialog screen ts file for DB-AddEdit-dialog.html */
@Component({
  selector: 'db-addedit-dialog',
  templateUrl: 'db-addedit-dialog.html',
})
export class dbAddEditDialog implements OnInit {
  form: FormGroup;
  titleIcon: string;
  title: string;
  engineColl = [{engine_name:'postgresql',disp_name:'PostgreSQL'},{engine_name:'clickhouse',disp_name:'ClickHouse'} ,{engine_name:'mssql',disp_name:'MSSQL'}];
  //,{engine_name:'oracle',disp_name:'Oracle'} ,{engine_name:'mysql',disp_name:'MySQL'}
  readOnly = true;
  isLoading: boolean;
  dbConnData: any;
  obsEnt: any;
  entItem:any;
  theme:any;
  constructor(
    public dialogRef: MatDialogRef<dbAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDbAddEdit,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder,
  ) {
    this.createForm();
  }

  ngOnInit(){
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    if (this.data.dbId == undefined) {
      this.titleIcon = "view_agenda"
      this.title = "Add Database";
    } else {
      this.titleIcon = "edit"
      this.title = "Edit Database ("+ this.data.dbId +")";
      this.loadEditData();
    }
    //enterprise change observable
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.entItem = JSON.parse(sessionStorage.getItem("entItem"))
      }
    });
  }

  ngOnDestroy(){
    if (this.obsEnt != undefined)
    this.obsEnt.unsubscribe();
  }

  
  onClose(): void {
    this.dialogRef.close();
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }

  getErrorMessage(control) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    return msg;
  }

  async loadEditData(){
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let usrDet = JSON.parse(sessionStorage.getItem("apdUserDet"));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      let param = {dbId: this.data.dbId};
      let result = await this.authService.getDBConnectorForDbId(param);
      if (result.success){
        let editRecord = JSON.parse(await this.authService.decrypt(result.result));
        if (editRecord.engine_name.toLowerCase() == "postgresql"){
          let engineIdx = this.engineColl.findIndex(x => x.engine_name == 'postgresql');
          this.form.get("EngineName").setValue(this.engineColl[engineIdx]);
          this.form.get("Host").setValue(editRecord.connection_details.host);
          this.form.get('Port').setValue(editRecord.connection_details.port);
          this.form.get('Database').setValue(editRecord.connection_details.database);
          this.form.get('DBUser').setValue(editRecord.connection_details.user);
          this.form.get('DBPassword').setValue(editRecord.connection_details.password);
        } else if (editRecord.engine_name.toLowerCase() == "mssql"){
          let engineIdx = this.engineColl.findIndex(x => x.engine_name == 'mssql');
          this.form.get("EngineName").setValue(this.engineColl[engineIdx]);
          this.form.get("Host").setValue(editRecord.connection_details.server);
          this.form.get('Port').setValue(editRecord.connection_details.port);
          this.form.get('Database').setValue(editRecord.connection_details.database);
          this.form.get('DBUser').setValue(editRecord.connection_details.user);
          this.form.get('DBPassword').setValue(editRecord.connection_details.password);
        } else if (editRecord.engine_name.toLowerCase() == "clickhouse"){
          let engineIdx = this.engineColl.findIndex(x => x.engine_name == 'clickhouse');
          this.form.get("EngineName").setValue(this.engineColl[engineIdx]);
          this.form.get("Host").setValue(editRecord.connection_details.host);
          this.form.get('Port').setValue(editRecord.connection_details.port);
          this.form.get('Database').setValue(editRecord.connection_details.database);
          this.form.get('DBUser').setValue(editRecord.connection_details.user);
          this.form.get('DBPassword').setValue(editRecord.connection_details.password);
        } else {
          console.log("Yet to develop for engine_name")
        }
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    } else {
      this.reusable.openAlertMsg("Only owner can edit this DB Connector","info");
    }
  }

  createForm() {
		this.form = this.formBuilder.group({
      DBUser:[null, Validators.compose([
				Validators.required,
      ])],
      DBPassword:[null,Validators.compose([
				Validators.required,
      ])],
      Database:[null,Validators.compose([
				Validators.required,
      ])],
      Host:[null,Validators.compose([
				Validators.required
      ])],
      Port:[null,Validators.compose([
				Validators.required,
      ])],
      EngineName:[null,Validators.compose([
				Validators.required,
      ])]
     },
		);	
  }

  async validateConn() {
    this.isLoading = true;
    let engine = this.form.get("EngineName").value.engine_name;
    if(engine.trim().toLowerCase()=='postgresql') {
      this.dbConnData = {
        engine_name:  engine.toLowerCase().trim(),
        connection_details: {
          host: this.form.get('Host').value.trim(),
          port: this.form.get('Port').value,
          database: this.form.get('Database').value.trim(),
          user: this.form.get('DBUser').value.trim(),
          password: this.form.get('DBPassword').value.trim(),
          max: 20
        }
      }
    } else if (engine.trim().toLowerCase()=='mssql') {
      this.dbConnData = {
        engine_name:  engine.toLowerCase().trim(),
        connection_details: {
          server: this.form.get('Host').value.trim(),
          port: this.form.get('Port').value,
          database: this.form.get('Database').value.trim(),
          user: this.form.get('DBUser').value.trim(),
          password: this.form.get('DBPassword').value.trim(),
          options:{encrypt:false}
        }
      }
    } else if (engine.trim().toLowerCase()=='clickhouse') {
      this.dbConnData = {
        engine_name:  engine.toLowerCase().trim(),
        connection_details: {
          host: this.form.get('Host').value.trim(),
          port: this.form.get('Port').value,
          database: this.form.get('Database').value.trim(),
          user: this.form.get('DBUser').value.trim(),
          password: this.form.get('DBPassword').value.trim(),
          options:{encrypt:false}
        }
      }
    } else {
      return ("yetto");
    }
    let result = await this.authService.validateConnection(this.dbConnData);
    this.isLoading = false;
    if (result.success) {
      return ("success");
    } else if (result.connection != undefined && !result.connection){ 
      return ("failed");
    } else if (result.message != undefined && result.message == "Yet to be implemented"){
      this.reusable.openAlertMsg(result.message,"info");
      return ("yetto");
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      return (result.message);
    }
  }

  async addUpdateDb(){
    let validation = await this.validateConn();
    if(validation != 'success'){
      if (validation == "failed")
        this.reusable.openAlertMsg("Could not establish connection to db. Validation failed", "error");
      else if (validation == "yetto")
        this.reusable.openAlertMsg("DB Connection not yet implemented, contact service provider for more details", "error");
      return;
    } else {
      this.isLoading = true;
      this.dbConnData.db_id = this.data.dbId != undefined ? this.data.dbId : null ;
      this.dbConnData.connector_name = this.form.get('Database').value.trim();
      const dbData = {data:this.authService.encrypt(JSON.stringify(this.dbConnData))};
      let result = await this.authService.dbConnectorUpdate(dbData);
      this.isLoading = false;
      if (result.success){
        this.reusable.openAlertMsg(result.message,"info");
        this.closeWithReturn();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }
}

/* Dialog screen ts file for add-query */
export interface TableName {
  table_name: string;
}

export interface ColDefClass {
  column_name: string,
  data_type: string,
  is_numeric: boolean
}

export interface QrySaveClass {
  qry_name: string,
  qry_desc: string
}


@Component({
  selector: 'db-addedit-query',
  templateUrl: 'db-addedit-query-dialog.html',
  styleUrls: ['./visualizer.component.css']
})

export class dbAddEditQueryDialog implements OnInit {
  openpopup:boolean = false;
  IsPartitioned = new FormControl();
  connectionString;
  readOnlySelect = true;
  placeholder = "Fetching tables...."
  form: FormGroup;
  titleIcon: string;
  title: string;
  isLoading: boolean = false;
  dbId: number;
  dbDet: any;
  visualization: string;
  visualColl =['table','stamp', 'pie', 'pie3d','donut','donut3d', 'hbar','others'];
  //scroll variables
  offset = 0; 
  limit = 50;
  isFetched = false;
  lastScrolled = 0;
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;
  setIntervalId;

  dispColumns = [];
  //Autocomplete variables
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  TableSel = new FormControl();
  ColumnSel = new FormControl();
  DDChartId = new FormControl();
  filteredColumns: Observable<ColDefClass[]>;
  tableCollection: TableName[];
  filteredTable: Observable<TableName[]>;
  columnDef: ColDefClass[] = [];
  totalRows:number = 0;
  level:string ="colFilter";
  operatorColl = {text:['Start with', 'Ends with', 'Contains', 'Equals', 'Not Equals'], number:['Greater Than Equals', 'Greater Than', 'Less Than Equals','Less Than','Equals','Not Equals','Between'], bool:['Equals', 'Not Equals'], timestamp:['Last 1 Hr','Current Day','Week to Date','Month to Date']};
  dispOperator = [];
  OperatorSel = new FormControl();
  FilterVal1 = new FormControl();
  FilterVal2 = new FormControl();
  fieldType:string;
  qryFilterColl = [];
  JoinOperator = new FormControl();
  operatorMap = {"Start with":"LIKE ('@@var@@%')", "Ends with":"LIKE ('%@@var@@')", Contains:"LIKE ('%@@var@@%')", Equals:"= '@@var@@'","Not Equals":"!= '@@var@@'", "Greater Than":"> '@@var@@'", "Greater Than Equals":">= '@@var@@'", "Less Than":"< '@@var@@'", "Less Than Equals":"<= '@@var@@'", Between:"BETWEEN '@@var@@' AND '@@var1@@'","Last 1 Hr":"BETWEEN to_timestamp(@startDate@) AND to_timestamp(@endDate@)","Current Day":"BETWEEN '@startDay@' AND '@endDay@'","Week to Date":"BETWEEN '@startWeek@' AND '@endWeek@'","Month to Date":"BETWEEN '@startMonth@' AND '@endMonth@'"}
  //Group collection
  qryGrpColl = []; openGrpPopup:boolean = false; openOrderByPopup:boolean = false;
  GrpSel = new FormControl();
  theme = "light";
  timestampGroup = ['Minute','Hour','Day','Week','Month','Quarter','Year'];
  TimeSubGroup = new FormControl();
  aggregatorColl = [{name:'COUNT', disp_name:'Count of Rows',help:'Count of rows returned'},{name:'SUM', disp_name:'SUM of...', help:'Sum of values'},{name:'SUMDistinct',disp_name:'Sum of distinct values', help:'the SUM function only calculates the values of distinct values'},{name:'AVG', disp_name:'Avg of...',help:'average value of a  numeric column.'},{name:'AVGDistinct', disp_name:'Avg of distinct values...',help:'the AVG function only calculates the values of distinct values'},{name:'MIN', disp_name:'Min of..',help:'Returns minimum value'}, {name:'MAX', disp_name:'Max of..',help:'Returns maximum value'},{name:'PERCENTILE',disp_name:'Percentile of..', help:'will return a value from the input set closest to the percentile you request'}];
  aggMap = {COUNT:[],DISTINCT:['text','number','bool'], SUM:['number'], SUMDistinct:['number'], AVG:['number'], AVGDistinct:['number'],MIN:['number','timestamp'], MAX:['number','timestamp'],PERCENTILE:['number']};
  AggSel = new FormControl();
  PercValFC = new FormControl();
  SelOrderByCol = new FormControl();
  SelOrderBySort = new FormControl('ASC');
  dateFormat = {dateTime:'dd-MMM-yyyy HH:mm:ss', date:'dd-MMM-yyyy', time:'HH:mm:ss'};
  selRawColColl = [];
  orderByColl = [];
  openSelColPopup:boolean = false;
  SelColFC = new FormControl();
  dispCols = [];
  dispFormat:string;
  qrySave:QrySaveClass;
  query_name: string;
  query_desc: string;
  chart = {data:[]};
  chart_types_json = {};
  stack_cols:string;
  h_display:boolean = false;
  enable_yaxis: boolean = true;
  col_units_json = {};
  svgData = [];
  id:number;
  paramArr = [];
  Param1 = new FormControl();
  Param2 = new FormControl();
  Param3 = new FormControl();
  Param4 = new FormControl();
  Param5 = new FormControl();

  @ViewChild('tableInput') tableInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild(MatSort) sort: MatSort;
  qryResultSet;
  qryId;
  myChart: boolean = false;

  tblCollected: BehaviorSubject<boolean> = new BehaviorSubject(false);
  tblVar = false;
  qryDet:any;
  screenParam:any;
  hideQryParam: boolean = false;
  isSQL: boolean = false;
  SqlQry = new FormControl();
  columnAndTypeGlobal:any;
  chartToDisplay = {data:[]};
  dataTypeColl=[]; qryText:string;
  chartParamForSave:any;

  handleScroll = (scrolled: boolean) => {
    if (scrolled) { this.offset += this.limit;}
    scrolled && this.isFetched? this.runQry(this.isSQL,this.offset, this.limit) : _noop();
  }

  hasMore(){
    return this.isFetched;
  }

  constructor(
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private router: Router,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog,
    public fb: FormBuilder
  ) {}

  ngOnInit(){
    this.tblCollected.subscribe(res => {
      if (res){
        this.qryId = sessionStorage.getItem("qryId");
        if (this.qryId != undefined){
          this.getCustomQueryById(this.qryId);
        }
      }
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    });
    this.dispFormat = this.dateFormat.dateTime;
    this.dbDet = JSON.parse(sessionStorage.getItem("dbQuery"));
    this.visualization = this.visualColl[0];
    if (this.dbDet == undefined){
      this.goBack();
      return;
    }
    this.dbId = this.dbDet.db_id;
    this.IsPartitioned.setValue(false);
    this.titleIcon = "view_agenda"
    this.title = "New Query for Database "+this.dbDet.database+" ("+this.dbDet.engine_name+")";
    this.getDatabaseById();
    this.filteredTable = this.TableSel.valueChanges.pipe(
      debounceTime(1000),
      map(x => x == undefined? x='a' : x),
      filter(x => x.length > 2 ),
      map(value => typeof value === 'string' ? value : value.table_name),
      map(tName => tName ? this._filter(tName) : this.tableCollection.slice(0,100))
    )
    this.filteredColumns = this.ColumnSel.valueChanges.pipe(
      debounceTime(1000),
      map(value => typeof value === 'string' ? value : value.column_name),
      map(colName => colName ? this._filterCol(colName) : this.columnDef.slice(0,20))
    )
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('light') ?'light':'dark';
    })
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => {
        if (this.virtualScroll != undefined)
          return this.virtualScroll.measureScrollOffset('bottom') === 0
        else 
          return false;
      })
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    sessionStorage.removeItem("dbQuery");
    sessionStorage.removeItem("qryId");
  }

  drawChart(chartType){
    if (this.chart.data.length == 0){
      this.reusable.openAlertMsg("No Data to display, click Run/Refresh query to get the data","info");
      return;
    }
    if (chartType != 'others'){
      if (this.chart_types_json != undefined){
        let keys = Object.keys(this.chart_types_json);
        keys.map((key,ix) => {
          this.chart_types_json[key] = chartType;
          this.columnAndTypeGlobal[ix+1]['chart_type'] = chartType;
        });
      }
    } else {
      let keys = Object.keys(this.chart_types_json);
      let chType = ['line', 'area', 'vbar','hidden','toolTip'];
      keys.map((key,ix) => {
        if (!chType.includes(this.chart_types_json[key])){
          this.chart_types_json[key] = 'line';
          this.columnAndTypeGlobal[ix+1]['chart_type'] = chartType;
        }
      });
    }
    this.chart['root_chart_type'] = chartType;
    this.chart['chart_types_json'] = this.chart_types_json;
    this.chartParamForSave['root_chart_type'] = chartType;
    this.chartParamForSave['chart_types_json'] = this.chart_types_json;
    this.chartToDisplay = JSON.parse(JSON.stringify(this.chart));
  }

  fromD3chart(event){
    console.log(event);
  }

  async getCustomQueryById(qryId){
    this.title = "Edit Query for Database "+this.dbDet.database+" ("+this.dbDet.engine_name+")"+" for query id "+qryId;
    let param = {id:qryId};
    let result = await this.authService.getCustomQueryById(param);
    if (result.success){
      this.qryDet = result.result[0];
      if (this.tableCollection == undefined){
        this.reusable.openAlertMsg("No tables found for the Selected Query","error")
        return;
      }
      if (this.qryDet.is_advance_query){
        this.isSQL = true;
        this.SqlQry.setValue(this.qryDet.query_text);
      } else {
        let find = this.tableCollection.find(x=>x.table_name == this.qryDet.table_name.table_name)
        this.TableSel.setValue(find);
        await this.getColDef();
      }
      this.DDChartId.setValue(this.qryDet.custom_params.dd_chart_id);
      this.qryFilterColl = this.qryDet.filter_by == null ? [] : this.qryDet.filter_by ;
      this.qryGrpColl = this.qryDet.group_by == null ? [] : this.qryDet.group_by;
      this.orderByColl = this.qryDet.order_by == null ? [] : this.qryDet.order_by;
      this.selRawColColl = this.qryDet.column_names == null ? [] : this.qryDet.column_names;
      this.orderByColl = this.qryDet.order_by == null? [] : this.qryDet.order_by;
      this.visualization = this.visualColl[this.visualColl.indexOf(this.qryDet.chart_type)];
      this.columnAndTypeGlobal = this.qryDet.disp_columns == null? undefined : this.qryDet.disp_columns;
      this.chart_types_json = this.qryDet.custom_params.chart_types_json;
      this.col_units_json = this.qryDet.custom_params.col_units_json;
      this.h_display = this.qryDet.custom_params.h_display;
      this.enable_yaxis = this.qryDet.custom_params.enable_yaxis;
      this.stack_cols = this.qryDet.custom_params.stack_cols;
      this.chart['dd_param'] = this.qryDet.custom_params.dd_param;
      this.columnAndTypeGlobal.map((column,ix) => {
          this.dispColumns.push(column.column_name);
          this.dataTypeColl.push(column.data_type);
      });
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result), "error");
    }
  }

  openConfigColumn(){
    const dialogRef = this.dialog.open(configColumnDialog, {
      width: '60%',
      data: {dispColumns: this.columnAndTypeGlobal, chart:this.chart}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined){
        this.dispColumns = [];
        this.dataTypeColl = [];
        this.chart_types_json = {};
        this.col_units_json = {};
        this.columnAndTypeGlobal = result.dispColumns;
        result.dispColumns.map((column,ix) => {
          this.dispColumns.push(column.column_name);
          this.dataTypeColl.push(column.data_type);
          if (this.chart['dd_param'] == undefined) this.chart['dd_param'] = {};
          this.chart['dd_param'][column.column_name] = column.dd_param;
          if(ix != 0){
            this.chart_types_json[column.column_name] = column.chart_type;
            this.col_units_json[column.column_name] = column.col_unit;
          }
        });
        this.chart['chart_types_json'] = this.chart_types_json;
        this.chart['col_units_json'] = this.col_units_json;
        this.chartParamForSave = JSON.parse(JSON.stringify(this.chart));
        this.chartToDisplay = this.chart;
      }
    });
  }

  chartParam;
  addToMyChart(qryId){
    this.myChart = true;
    this.chartParam = {chartId: qryId, modType: 'custom', refId: '-1', isSystem: false, category:"Custom Chart", isNewVisualizer:true};
    this.openMyChartDialog();
  }

  openMyChartDialog(){
    const dialogRef = this.dialog.open(myChartDialog, {
      width: '50%',
      data: {refId: this.chartParam.refId, modType: this.chartParam.modType, chartId: this.chartParam.chartId, isSystem: this.chartParam.isSystem, category: this.chartParam.category, isNewVisualizer:true}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  
  getAggColumns(selAgg){
    this.dispCols = [];
    let dType = this.aggMap[selAgg.name];
    let dispCollection = [];
    dType.map(type => {
      if (type=='text'){
        dispCollection.push(this.columnDef.filter(x=>x.data_type.includes('char') || x.data_type.includes('text')));
      } else if (type=='number'){
        dispCollection.push(this.columnDef.filter(x=>x.data_type.includes('numer') || x.data_type.includes('int')));
      } else if (type=='bool'){
        dispCollection.push(this.columnDef.filter(x=>x.data_type.includes('bool')));
      } else if (type=='timestamp'){
        dispCollection.push(this.columnDef.filter(x=>x.data_type.includes('timest') || x.data_type.includes('date')));
      } else {
        dispCollection.push(this.columnDef.filter(x=>x.data_type.includes('char') || x.data_type.includes('text')));
      }
    });
    dispCollection.map(coll => {
      coll.map(col=>{
        this.dispCols.push(col);
      })
    });
  }

  removeOrderCol(ix){
    this.orderByColl.splice(ix,1);
  }
  
  removeAggCol(ix){
    this.selRawColColl.splice(ix,1);
  }
  
  addToOrderByCol(){
    if(this.SelOrderByCol.value == null) {
      return;
    }
    if(this.SelOrderBySort.value == null || this.SelOrderBySort.value == '') this.SelOrderBySort.setValue("ASC");
    this.orderByColl.push({column_index: this.SelOrderByCol.value, sort: this.SelOrderBySort.value});
  }

  addToCol(){
    if (this.AggSel.value == null || this.AggSel.value == '') return;
    if (this.AggSel.value.name != 'COUNT' && this.SelColFC.value == null) return;
    this.selRawColColl.push({column_name:this.SelColFC.value == null?'RowCount':this.SelColFC.value.column_name, aggr:this.AggSel.value.name, percentile:this.PercValFC.value == undefined ? -1 : this.PercValFC.value});
    this.openSelColPopup = false;
    this.SelColFC.setValue(null);
    this.AggSel.setValue(null);
  }

  addToGrp(){
    if (this.GrpSel.value == null || this.GrpSel.value == '') return;
    if (this.GrpSel.value.data_type.includes('timestamp') && this.TimeSubGroup.value == null ) return;
    let selectedValue = this.GrpSel.value;
    if (this.TimeSubGroup.value != null){
      selectedValue['subGroup'] = this.TimeSubGroup.value;
      selectedValue['timeOffset'] = new Date().getTimezoneOffset() * -1;
    }
    this.qryGrpColl.push(selectedValue);
    this.GrpSel.setValue(null);
    this.TimeSubGroup.setValue(null);
    this.openGrpPopup = false;
  }

  removeFilter(columnName:string){
    let ix = this.qryFilterColl.findIndex(x => x.column_name == columnName);
    this.qryFilterColl.splice(ix, 1);
  }

  removeGrpFilter(columnName:string){
    let ix = this.qryGrpColl.findIndex(x => x.column_name == columnName);
    this.qryGrpColl.splice(ix, 1);
  }

  onCancel(){
    this.openpopup = false;
    this.ColumnSel.setValue('');
    this.OperatorSel.setValue(null);
    this.FilterVal1.setValue(null);
    this.FilterVal2.setValue(null);
    this.JoinOperator.setValue(null);
  }

  addToFilter(){
    let opSelValIdx = this.operatorColl.timestamp.indexOf(this.OperatorSel.value);
    if (this.ColumnSel.value == null || this.ColumnSel.value == '' || ((this.FilterVal1.value == null|| this.FilterVal1.value == '') &&  opSelValIdx == -1) ) return;
    let utcTimeVal1 = null;
    let utcTimeVal2 = null;
    if (this.ColumnSel.value.data_type.includes("time") || this.ColumnSel.value.data_type.includes("date") ){
      if(opSelValIdx == -1){
        utcTimeVal1 = new Date(this.FilterVal1.value).toISOString();
        if (this.FilterVal2.value != null){
          utcTimeVal2 = new Date(this.FilterVal2.value).toISOString();
        }
      } 
    }
    this.qryFilterColl.push({joinOperator:this.JoinOperator.value, column_name: this.ColumnSel.value.column_name, operator:this.OperatorSel.value, filterVal1:this.FilterVal1.value, filterVal2: this.FilterVal2.value, utcTimeVal1:utcTimeVal1, utcTimeVal2:utcTimeVal2});
    this.ColumnSel.setValue('');
    this.OperatorSel.setValue(null);
    this.FilterVal1.setValue(null);
    this.FilterVal2.setValue(null);
    this.openpopup = false;
  }

  getOperator(selCol){
    this.FilterVal1.setValue(null);
    this.FilterVal2.setValue(null);
    this.OperatorSel.setValue('Equals');
    let dType = selCol.data_type;
    if (dType.includes('char') || dType.includes('text')){
      this.dispOperator = this.operatorColl['text'];
      this.fieldType = 'text';
    } else if (dType.includes('number') || dType.includes('int') || dType.includes('double') || dType.includes('real')){
      this.dispOperator = this.operatorColl['number'];
      this.fieldType = 'number';
    } else if (dType.includes('timestamp')){
      this.dispOperator = this.operatorColl['number'];
      this.operatorColl['timestamp'].map(col =>{
        this.dispOperator.push(col);
      });
      this.fieldType ='datetime-local';
    } else if (dType.includes('bool')){
      this.dispOperator = this.operatorColl['bool'];
      this.fieldType = 'radio';
    }
  }

  async customQry(offset?:number, limit?:number){
    let msg=''
    let queryText = this.SqlQry.value.toString();
    if (this.SqlQry.value.includes(';')){
      msg += "Semicolan is not allowed. Note Only single statement query is allowed.";
    } 
    if (!this.SqlQry.value.toLowerCase().trim().startsWith('select')){
      msg += " Only SELECT Statement is allowed and must start with SELECT";
    }
    if (this.SqlQry.value.toLowerCase().includes('offset')||this.SqlQry.value.toLowerCase().includes('limit')){
      msg += " System manages the offset and limits, hence remove the same.";
    }
    if (msg.length > 0){
      this.reusable.openAlertMsg(msg, "info");
      return;
    } 
    if (this.paramArr.length > 0){
      this.paramArr.map((val,ix) =>{
        while(queryText.includes("@@param"+(ix+1)+"@@")){
          let repVal = ix == 0 ? this.Param1.value : ix == 1 ? this.Param2.value : ix == 2 ? this.Param3.value : ix == 3 ? this.Param4.value : ix == 4 ? this.Param5.value : null;
          queryText = queryText.replace("@@param"+(ix+1)+"@@", repVal);
        }
      });
    }
    this.paramArr = [];
    for (let i = 1; i <= 5; i++){
      if (queryText.includes('@@param'+i+'@@')){
        this.paramArr.push(true);
      }
    }
    if (this.paramArr.length > 0) {
      this.reusable.openAlertMsg("Query has parameter, please replace with value", "info"); 
      return;
    }
    let cntQryText = "SELECT COUNT(*) FROM ("+queryText+") AS qry"
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 50;
    this.executeQuery(queryText,offset, limit, cntQryText, reset);
  }

  async runQry(sqlQry, offset?:number, limit?:number){
    this.id = Math.floor(Math.random()*100);
    this.chart = {data:[]};
    let columnAndType = [];
    if (sqlQry){
      this.customQry(offset, limit);
      return;
    }
    let opSelValIdx = this.operatorColl.timestamp.indexOf(this.OperatorSel.value)
    if (this.TableSel.value == undefined){
      return;
    }
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 50;
    let qryFilter = '';
    if (this.qryFilterColl.length>0){
      let fil = this.qryFilterColl;
      fil.map((filter,ix) => {
        if (ix==0){ qryFilter = 'WHERE '};
        if (filter.joinOperator != null) qryFilter += " "+filter.joinOperator+" ";
        qryFilter += filter.column_name + " "+this.operatorMap[filter.operator];
        if (opSelValIdx == -1){
          qryFilter = qryFilter.replace('@@var@@', filter.utcTimeVal1 == null ? filter.filterVal1: filter.utcTimeVal1);
          if (filter.filterVal2 != null){
            qryFilter = qryFilter.replace('@@var1@@', filter.utcTimeVal2 == null ? filter.filterVal2: filter.utcTimeVal2);
          }
        }
      })
    }
    let grpBy = '';
    let select = 'SELECT ';
    let cntQryText = ''
    if (this.qryGrpColl.length>0){
      let grpColl = this.qryGrpColl;
      grpColl.map((grp,ix)=>{
        if (ix==0) {
          grpBy ="GROUP BY ";
        }
        else {
          grpBy += ", ";
          select += ", ";
        }
        if (grp.data_type.includes('timestamp')){
          if(grp.subGroup != undefined) {
            grpBy += "date_trunc('"+grp.subGroup.toLowerCase()+"',"+grp.column_name +" +interval '"+grp.timeOffset+" minutes')";
            select += "date_trunc('"+grp.subGroup.toLowerCase()+"',"+grp.column_name +" +interval '"+grp.timeOffset+" minutes')::timestamp without time zone AS "+grp.column_name+"_"+grp.subGroup.toLowerCase();
            let colName = grp.column_name+"_"+grp.subGroup.toLowerCase();
            columnAndType.push({column_name:colName,data_type:grp.data_type,chart_type:'table',col_unit:'datetime'});
          }
        } else {
          grpBy += grp.column_name +" ";
          select += grp.column_name;
          let colUnit = grp.data_type.includes("int")?'numeric':grp.data_type.includes("char")?'text':grp.data_type;
          columnAndType.push({column_name:grp.column_name, data_type:grp.data_type,chart_type:'table',col_unit:colUnit});
        }
      });
    } else {
      if (this.selRawColColl.length == 0){
        select += " * ";
        cntQryText = "SELECT COUNT(*) FROM "+this.TableSel.value.table_name +" "+qryFilter;
        // this.processDispColumns(this.columnDef); // this line giving duplicate columns
      }
    }
    if (this.selRawColColl.length > 0){
      this.selRawColColl.map((column,ix) => {
        if (select.length > 8) select += ",";
        if (column.column_name == "RowCount"){
          select += " COUNT(*) as count";
          columnAndType.push({column_name:'count', data_type:'int',chart_type:'table',col_unit:'count'});
        } else {
          if (column.aggr.toLowerCase()=="sumdistinct"){
            select += " SUM(DISTINCT "+column.column_name+") as "+column.column_name+'_'+column.aggr.toLowerCase();
            columnAndType.push({column_name:column.column_name+'_'+column.aggr.toLowerCase(), data_type:'numeric',chart_type:'table',col_unit:'numeric'});
          } else if (column.aggr.toLowerCase()=="avgdistinct"){
            select += " AVG(DISTINCT "+column.column_name+") as "+column.column_name+'_'+column.aggr.toLowerCase();
            columnAndType.push({column_name:column.column_name+'_'+column.aggr.toLowerCase(), data_type:'numeric',chart_type:'table',col_unit:'numeric'});
          } else if (column.aggr.toLowerCase()=="percentile"){
            if (column.percentile < 0 || column.percentile > 1 ){column.percentile = 0.90}
            select += " PERCENTILE_DISC("+column.percentile+") WITHIN GROUP (ORDER BY "+column.column_name+") as "+column.column_name+"_perc"+column.percentile*100;
            columnAndType.push({column_name:column.column_name+"_perc"+column.percentile*100, data_type:'numeric',chart_type:'table',col_unit:'numeric'});
          }
          else {
            select += " "+column.aggr+"("+column.column_name+") as "+column.column_name+'_'+column.aggr.toLowerCase() ;
            columnAndType.push({column_name:column.column_name+'_'+column.aggr.toLowerCase(), data_type:'numeric',chart_type:'table',col_unit:'numeric'});
          }
        }
      });
    }
    let orderBy = '';
    if (this.orderByColl.length > 0){
      this.orderByColl.map((order,ix) => {
        if (ix == 0) orderBy = " ORDER BY "
        if (ix != 0) orderBy += ", ";
        orderBy += (order.column_index+1).toString() +" "+ order.sort;
      })
    }
    if (columnAndType.length > 0){
      if (this.columnAndTypeGlobal == undefined) {
        this.columnAndTypeGlobal = columnAndType;
      } else {
        //adding new column if there to the global collection
        columnAndType.map((column)=>{
          let findIdx = this.columnAndTypeGlobal.findIndex(x => x.column_name == column.column_name);
          if (findIdx == -1){
            this.columnAndTypeGlobal.push(column);
          }
        });
        //remove the columns that does not exist in the current columnAndType collection
        let oldCollection = JSON.parse(JSON.stringify(this.columnAndTypeGlobal)); // to remove the reference
        oldCollection.map((column) => {
          let findIdx = columnAndType.findIndex(x => x.column_name == column.column_name);
          if (findIdx == -1){
            let colIdx = this.columnAndTypeGlobal.findIndex(x=> x.column_name == column);
            this.columnAndTypeGlobal.splice(colIdx,1); //removing the column that does not exist in recent collection;
          }
        })
      }
      this.dispColumns = [];
      this.dataTypeColl = [];
      this.columnAndTypeGlobal.map((column,ix)=>{
        this.dispColumns.push(column.column_name);
        this.dataTypeColl.push(column.data_type);
        if (column['dd_param'] == undefined){
          column['dd_param'] = this.chart['dd_param'] != undefined ? this.chart['dd_param'][column] : 'none';
        }
        if (ix != 0){
          this.chart_types_json[column.column_name] = column.chart_type;
          this.col_units_json[column.column_name] = column.col_unit;
        }
      });
    }
    let qryText = select+" FROM "+this.TableSel.value.table_name + " "+qryFilter+" "+grpBy + orderBy;
    await this.executeQuery(qryText, offset, limit, cntQryText, reset);
  }  
  
  private async executeQuery(qryText: string, offset: number, limit: number, cntQryText: string, reset: boolean) {
    let param = {
      queryText: qryText + " OFFSET " + offset + " LIMIT " + limit,
      cntQryText: cntQryText == '' ? "SELECT COUNT(*) FROM (" + qryText + ") AS qry" : cntQryText,
      connectionString: this.connectionString,
      timeOffset: new Date().getTimezoneOffset() * -1
    };
    this.qryText = qryText;
    this.isLoading = true;
    let enParam = this.authService.encrypt(JSON.stringify(param));
    let qryParam = { data: enParam };
    this.getRowCount(qryParam);
    let result = await this.authService.runQuery(qryParam);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      let colColl = result.columns;
      this.hideQryParam = true;
      this.isFetched = true;
      if (this.offset == 0) {
        this.convertDatetimeToEpoch(result.result);
        if(this.columnAndTypeGlobal == undefined){
          this.dispColumns = [];
          this.dataTypeColl = [];
          this.columnAndTypeGlobal = [];
          if (this.chart['dd_param'] == undefined) this.chart['dd_param'] = {};
          colColl.map((column,ix) => {
            if (ix == 0){
              let colUnit; 
              if (this.qryDet != undefined){
                this.qryDet.custom_params.xaxis_time_scale ? 'datetime' : 'text';
              }
              this.columnAndTypeGlobal.push({column_name:column.name, data_type: column.format, chart_type:'axis-label', col_unit:colUnit, dd_param:'none'});
              this.dispColumns.push(column.name);
              this.dataTypeColl.push(column.format);
              this.chart['dd_param'][column.name] = 'none'
            } else {
              this.columnAndTypeGlobal.push({column_name:column.name, data_type: column.format, chart_type:this.chart_types_json[column.name], col_unit: this.col_units_json[column.name],dd_param:'none'});
              this.dispColumns.push(column.name);
              this.dataTypeColl.push(column.format);
              this.chart['dd_param'][column.name] = 'none'
            }
          });
        } else {
          colColl.map((column, ix) => {
            if (this.columnAndTypeGlobal[ix] == undefined){
              this.columnAndTypeGlobal.push({column_name:column.name, data_type: column.format, chart_type:this.visualization == 'others' ? 'line' : this.visualization, col_unit: 'text',dd_param:'none'});
            } else {
              if (this.columnAndTypeGlobal[ix].column_name != column.name){
                this.columnAndTypeGlobal[ix].column_name = column.name;
                this.columnAndTypeGlobal[ix].data_type = this.columnAndTypeGlobal[ix].col_unit = column.format;
                this.columnAndTypeGlobal[ix].dd_param = 'none';
              }
            }
          });
          if (colColl.length <= this.columnAndTypeGlobal.length){
            let length = this.columnAndTypeGlobal.length;
            for (let i = colColl.length; i < length; i++){
              this.columnAndTypeGlobal.splice(i,1);
            }
          }
          this.chart_types_json = {};
          this.col_units_json = {};
          this.dispColumns = [];
          this.dataTypeColl = [];
          this.columnAndTypeGlobal.map((column,ix) => {
            this.dispColumns.push(column.column_name);
            this.dataTypeColl.push(column.data_type);
            if (ix != 0){
              this.chart_types_json[column.column_name] = column.chart_type;
              this.col_units_json[column.column_name] = column.col_unit;
            }
          });
        }
        if(this.dataTypeColl[0].includes('timestamp') ||this.dataTypeColl[0].includes('datetime')||this.dataTypeColl[0].includes('date')){
          this.chart['xaxis_time_scale'] = true;
        } else {
          this.chart['xaxis_time_scale'] = false;
        }
        this.chart['stack_cols'] = this.stack_cols;
        this.chart['enable_yaxis'] = this.enable_yaxis;
        this.chart['chart_types_json'] = this.chart_types_json;
        this.chart["col_units_json"] = this.col_units_json;
        this.chart["h_display"] = this.h_display;
        this.chart["stack_cols"] = this.stack_cols;
        this.chart['enable_yaxis'] = this.enable_yaxis;
        this.chart['root_chart_type'] = this.visualization;
        this.chart['query'] = this.isSQL ? this.SqlQry.value : qryText;
        this.chart['data'] = result.result;
        this.chartParamForSave = JSON.parse(JSON.stringify(this.chart));
        this.svgData = result.result;
        this.qryResultSet = new MatTableDataSource(result.result);
        this.qryResultSet.sort = this.sort;
        this.chartToDisplay = JSON.parse(JSON.stringify(this.chart));
        if (this.visualization != 'table') {
          this.drawChart(this.visualization);
        }
      }
      else {
        this.svgData = this.chart.data = this.svgData.concat(result.result);
        this.qryResultSet.data = this.qryResultSet.data.concat(result.result);
        this.qryResultSet.sort = this.sort;
        this.chartToDisplay = JSON.parse(JSON.stringify(this.chart));
      }
    }
    else if (result.rowCount == 0) {
      if (reset) {
        this.qryResultSet = new MatTableDataSource([]);
        this.chart['data'] = [];
        this.svgData = [];
        this.chartToDisplay = JSON.parse(JSON.stringify(this.chart));
      }
      this.isFetched = false;
    }
    else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result), "error");
    }
  }

  convertDatetimeToEpoch(dataSet){
    if (this.dataTypeColl != undefined && this.dataTypeColl.length>0){
      this.dataTypeColl.map((type,ix) => {
        if(type.includes('timestamp') || type.includes('datetime') || type.includes('date')){
          dataSet.map(row => {
            row[this.dispColumns[ix]] = new Date(row[this.dispColumns[ix]]).getTime() != NaN ? new Date(row[this.dispColumns[ix]]).getTime(): row[this.dispColumns[ix]];
          });
        }
      })
    }
  }

  async getRowCount(qryParam){
    let result = await this.authService.runRowCountQuery(qryParam);
    if (result.success){
      this.totalRows = result.result;
    } else {
      this.totalRows = 0;
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
  openQrySave(){
    if (this.qryDet != undefined){
      this.qrySave = {qry_name: this.qryDet.qry_name, qry_desc: this.qryDet.qry_desc};
    } else {
      this.qrySave = {qry_name: null, qry_desc:null};
    }
    this.openQrySaveDialog();
  }

  openQrySaveDialog(): void {
    const dialogRef = this.dialog.open(QrySaveDialog, {
      width: '40%',
      data: {qry_name: this.qrySave.qry_name, qry_desc: this.qrySave.qry_desc}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.query_name = result.qry_name;
        this.query_desc = result.qry_desc;
        this.saveQuery();
      }
    });
  }

  async saveQuery(){
    delete this.chartParamForSave["data"];
    this.chartParamForSave['dd_chart_id'] = this.DDChartId.value;
    if (this.chartParamForSave['dd_param'] == undefined) this.chartParamForSave['dd_param'] = {};
    this.columnAndTypeGlobal.map(column => {
      this.chartParamForSave['dd_param'][column.column_name] = column.dd_param;
    });
    let param = {
      qry_id: this.qryDet != undefined ? this.qryDet.id: null,
      qry_name: this.query_name,
      qry_desc: this.query_desc,
      table_name : JSON.stringify(this.TableSel.value),
      group_by: this.qryGrpColl.length == 0 ? null : JSON.stringify(this.qryGrpColl),
      order_by: this.orderByColl.length == 0? null : JSON.stringify(this.orderByColl),
      filter_by: this.qryFilterColl.length == 0 ? null : JSON.stringify(this.qryFilterColl),
      column_names: this.selRawColColl.length == 0 ? null :JSON.stringify(this.selRawColColl),
      query_text: this.isSQL ? this.SqlQry.value : this.qryText,
      connector_id:this.dbDet.db_id,
      chart_type: this.visualization,
      custom_params: this.chartParamForSave,
      is_advance_query: this.isSQL,
      disp_columns: this.columnAndTypeGlobal == undefined ? null : JSON.stringify(this.columnAndTypeGlobal)
    };
    let result = await this.authService.saveQuery(param);
    if (result.success){
      this.reusable.openAlertMsg(result.message,"info");
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  processDispColumns(colDef:any){
    colDef.map(col => {
      this.dispColumns.push(col.column_name);
      this.dataTypeColl.push(col.data_type);
    });
  }

  private _filterCol(colName: string): ColDefClass[] {
    const filterValue = colName.toLowerCase();
    return this.columnDef.filter(option => option.column_name.toLowerCase().includes(filterValue));
  }

  private _filter(tName: string): TableName[] {
    const filterValue = tName.toLowerCase();
    return this.tableCollection.filter(option => option.table_name.toLowerCase().includes(filterValue));
  }

  displayFn(tName?: TableName): string | undefined {
    return tName ? tName.table_name : undefined;
  }

  displayColFn(colName?: ColDefClass): string | undefined {
    return colName ? colName.column_name : undefined;
  }

  async getColDef(){
    this.authService.toggleMenu.next(false);
    this.columnDef = [];
    this.openpopup = false;
    this.openSelColPopup = false;
    this.openGrpPopup = false;
    this.ColumnSel.setValue('');
    this.qryResultSet = null;
    this.qryResultSet = new MatTableDataSource([]);
    this.dispColumns = [];
    this.qryFilterColl = [];
    this.qryGrpColl = [];
    this.selRawColColl = [];
    this.totalRows = 0;
    this.chart = {data:[]};
    this.visualization = this.visualColl[0];
    let param = {
      connectionString: this.connectionString,
      table_name: this.TableSel.value.table_name
    };
    let enParam = this.authService.encrypt(JSON.stringify(param));
    let qryParam = {data:enParam};
    let result = await this.authService.getColDef(qryParam);
    if (result.success){
      this.columnDef = result.result;
      this.processDispColumns(this.columnDef);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  onChangePartition(){
    this.placeholder = "Fetching tables....";
    this.readOnlySelect = true;
    this.getPsqlTables(this.connectionString);
  }

  async getDatabaseById(){
    let param = {dbId: this.dbId};
    let result = await this.authService.getDBConnectorForDbId(param);
    if (result.success){
      let deResult = JSON.parse(await this.authService.decrypt(result.result));
      if (deResult.engine_name == 'postgresql'){
        this.connectionString = deResult.connection_details;
        this.getPsqlTables(deResult.connection_details);
      }
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async getPsqlTables(connectionString){
    let encryptParam = {isPartitioned: this.IsPartitioned.value, connectionString: connectionString};
    let param = this.authService.encrypt(JSON.stringify(encryptParam));
    let qryParam = {param : param}
    let result = await this.authService.getPsqlTables(qryParam);
    if (result.success){
      this.tableCollection = result.result;
      this.readOnlySelect = false;
      this.placeholder = "Select Table";
      this.tblCollected.next(true);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  goBack(){
    this.router.navigate(['home/visualizer']);
  }
}


/* Map Enterprise to Database connector */
@Component({
  selector: 'db-ent-map',
  templateUrl: 'db-ent-map-dialog.html',
  styleUrls: ['./visualizer.component.css']
})
export class DBEntMapDialog implements OnInit {
  form: FormGroup;
  titleIcon: string;
  title: string;
  isLoading: boolean;
  dbConnData: any;
  mappedEntColl = new MatTableDataSource([]);
  enterpriseColl: any;
  EnterpriseFC = new FormControl();
  removedEntIds = [];
  filterEntColl = [];
  displayEntCol = ['delete','e_id','e_name'];
  saveChanges = false;
  entItem:any;
  usrDet:any;
  enableRemove:false;
  theme:any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<DBEntMapDialog>,
    @Inject(MAT_DIALOG_DATA) public data: classDbEntMap,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
  ) { }

  ngOnInit(){
      this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    })
    if (this.data.engine_name == null && this.data.database == null){
      this.titleIcon = "share"
      this.title = "Map Enterprise to Custom Query ("+ this.data.db_id +")";
    } else {
      this.titleIcon = "share"
      this.title = "Map Enterprise to DB "+this.data.database+ "("+ this.data.db_id +")";
    }
    let mappedEnt = this.data.arr_eid;
    this.entItem = JSON.parse(sessionStorage.getItem("entItem"));
    if (mappedEnt != null && mappedEnt.length>0){
      let param = {ent_ids: mappedEnt.toString()};
      let resMappedEnt = this.authService.getMappedEntName(param);
      let resEnterprises = this.authService.getMyEnterprises();
      let errMsg = ''
      forkJoin([resMappedEnt,resEnterprises]).subscribe(results => {
        if (results[0].success){
          this.mappedEntColl = new MatTableDataSource(results[0].result);
          this.mappedEntColl.sort = this.sort;
        } else {
          errMsg += this.authService.invalidSession(results[0].result);
        }
        if (results[1].success){
          this.enterpriseColl = results[1].result;
          this.filterEnterprise();
        } else {
          errMsg += this.authService.invalidSession(results[1].result);
        }
        if (errMsg.length>0){
          this.reusable.openAlertMsg(errMsg,"error");
        }
      }, err=>{
        console.log(err);
      });
    } else {
      this.getMyEnterprises();
    }
  }

  async updDBEntMapping(){
    let param = {ent_ids: this.data.arr_eid, db_id: this.data.db_id};
    let result;
    if (this.data.engine_name == null && this.data.database == null){
      result = await this.authService.updCustQryEntMap(param);
    } else {
      result = await this.authService.updDBEntMap(param);
    }
    if (result.success){
      this.reusable.openAlertMsg(result.message,"info");
      this.closeWithReturn();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"eror");
    }
  }

  removeEnt(ent){
    let findIdx = this.mappedEntColl.data.findIndex(x=>x.e_id==ent.e_id);
    this.mappedEntColl.data.splice(findIdx,1);
    let dt = this.mappedEntColl.data;
    this.mappedEntColl = new MatTableDataSource(dt);
    let mapIdx = this.data.arr_eid.findIndex(x=>x==ent.e_id);
    this.data.arr_eid.splice(mapIdx,1);
    this.saveChanges = true;
    this.filterEnterprise();
  }

  mapEnt(){
    let dt = this.mappedEntColl.data;
    dt.push(this.EnterpriseFC.value);
    this.mappedEntColl = new MatTableDataSource(dt);
    if (this.data.arr_eid == null) this.data.arr_eid = [];
    this.data.arr_eid.push(this.EnterpriseFC.value.e_id);
    this.saveChanges = true;
    this.EnterpriseFC.setValue(null);
    this.filterEnterprise();
  }

  private filterEnterprise() {
    this.filterEntColl = [];
    let mappedIds = this.data.arr_eid;
    this.enterpriseColl.map(ent => {
      if (!mappedIds.includes(ent.e_id)) {
        this.filterEntColl.push(ent);
      }
    });
  }

  async getMyEnterprises(){
    let result = await this.authService.getMyEnterprises();
    if (result.success){
      this.enterpriseColl = this.filterEntColl = result.result;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result), "error");
    }
  }

  onClose(): void {
    if (this.saveChanges){
      let conMsg = confirm("Are you sure you want cancel without saving changes?");
      if (conMsg){
        this.dialogRef.close();
      }
    } else {
      this.dialogRef.close();
    }
  }

  closeWithReturn(): void {
    this.dialogRef.close(this.data);
  }
}

/* Open Save query Dialog for capturing query name and description */
@Component({
  selector: 'qry-save',
  templateUrl: 'qry-save-dialog.html',
})

export class QrySaveDialog {
  btnDisable: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<QrySaveDialog>,
    @Inject(MAT_DIALOG_DATA) public data: QrySaveClass,
  ) { }

  onClose(): void {
    this.dialogRef.close();
  }

  validateStringEmpty(){
    if (this.data.qry_name == undefined || this.data.qry_name.trim() == ''){
      this.btnDisable = true;
    } else {
      this.btnDisable = false;
    }
  }

}

/* Open query Grid Dialog to allow user to edit,delete query */
export interface DataScheduler {
  is_scheduled: boolean; 
  frequency: string; 
  enable_scheduler: boolean; 
  title: string; 
  description: string; 
  qry_name: string; 
  qry_desc: string; 
  send_as_attachment: boolean;
  id: number;
}

export interface DataEmail {
  email_arr:any;
  qry_name:string;
  id:number
}

@Component({
  selector: 'qry-grid',
  templateUrl: 'qry-grid-dialog.html',
  styleUrls: ['./visualizer.component.css']
})
export class QryGridDialog implements OnInit {
  titleIcon: string;
  title: string;
  isLoading: boolean = false;
  qryResultSet = new MatTableDataSource([]);
  dbDet: any;
  dbId: number;
  dispColumns = ['edit','delete', 'qry_name', 'qry_desc','map_ent','schedule_report','frequency','email_arr','last_email_sent_on'];
  screenParam: any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  //scroll variables
  offset = 0; 
  limit = 10;
  isFetched = false;
  lastScrolled = 0;
  @ViewChild(CdkScrollable, { static: true }) virtualScroll: CdkScrollable;
  setIntervalId;
  
  handleScroll = (scrolled: boolean) => {
    if (scrolled) { this.offset += this.limit; }
    scrolled && this.isFetched? this.getConnectorCustomQry(this.offset, this.limit) : _noop();
  }
  obsEnt: any;
  entItem: any;
  mapDbEnt: classDbEntMap;
  theme: any;
  schedulerData: DataScheduler;
  emailArrData: DataEmail;

  hasMore(){
    return this.isFetched;
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog,
  ) { }
  
  ngOnDestroy(){
    clearInterval(this.setIntervalId);
  }

  ngOnInit(){
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    this.dbDet = JSON.parse(sessionStorage.getItem("dbQuery"));
    this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
    if (this.dbDet == undefined){
      this.goBack();
      return;
    }
    this.dbId = this.dbDet.db_id;
    this.titleIcon = "view_agenda"
    this.title = "List of Custom Queries for "+this.dbDet.database+" ("+this.dbDet.engine_name+")";
    this.getConnectorCustomQry();

    this.obsEnt = this.authService.watchStorage().subscribe(data =>{
      if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
        this.entItem = JSON.parse(sessionStorage.getItem('entItem'));
        this.getConnectorCustomQry();
      }
    });

    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
  }

  openConfigEmail(row){
    this.emailArrData  = {email_arr:row.email_arr, qry_name: row.qry_name, id:row.id};
    this.openConfigEmailDialog();
  }

  openConfigEmailDialog(): void {
    const dialogRef = this.dialog.open(QryConfigEmailDialog, {
      width: '40%',
      data: {email_arr:this.emailArrData.email_arr, qry_name:this.emailArrData.qry_name, id:this.emailArrData.id}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getConnectorCustomQry();
      }
    });
  }

  openScheduler(row){
    this.schedulerData  = {is_scheduled:row.is_scheduled, frequency: row.frequency, enable_scheduler: row.enable_scheduler, title:row.report_title, description: row.report_description, qry_name:row.qry_name, qry_desc: row.qry_desc, send_as_attachment:row.send_as_attachment, id: row.id};
    this.openSchedulerDialog();
  }

  openSchedulerDialog(): void {
    const dialogRef = this.dialog.open(QrySchedulerDialog, {
      width: '40%',
      data: {is_scheduled:this.schedulerData.is_scheduled, frequency: this.schedulerData.frequency, enable_scheduler: this.schedulerData.enable_scheduler, title:this.schedulerData.title, description: this.schedulerData.description, qry_name:this.schedulerData.qry_name, qry_desc: this.schedulerData.qry_desc, send_as_attachment: this.schedulerData.send_as_attachment, id: this.schedulerData.id}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getConnectorCustomQry();
      }
    });
  }

  async getConnectorCustomQry(offset?:number, limit?:number){
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 10;
    let param = {
      eId: this.entItem.e_id == 0 ? null : this.entItem.e_id,
      offset: offset,
      limit: limit,
      db_id: this.dbDet.db_id
    };
    let result = await this.authService.getConnectorCustomQry(param);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      this.isFetched = true;
      if (this.qryResultSet.data.length == 0 || reset){
        this.qryResultSet = new MatTableDataSource(result.result);
      } else {
        let newData = this.qryResultSet.data.concat(result.result);
        this.qryResultSet = new MatTableDataSource(newData);
      }
      this.qryResultSet.sort = this.sort;
    } else if (result.rowCount == 0){
      if (reset){
        this.qryResultSet = new MatTableDataSource([]);
      }
      this.isFetched = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  configQuery(qryId){
    sessionStorage.setItem("qryId",qryId);
    this.router.navigate(['home/visualizer/query'])
  }

  async delCustomQry(qryId){
    let confirmMsg = confirm("Are you sure, you want to delete this query?");
    if (confirmMsg){
      let param = {id: qryId};
      let result = await this.authService.delCustomQry(param);
      if (result.success){
        this.reusable.openAlertMsg(result.message,"info");
        this.getConnectorCustomQry();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }

  openMapEnt(qryDet){
    this.mapDbEnt  = {db_id: qryDet.id, arr_eid: qryDet.arr_eid, database: null, engine_name:null};
    this.openDBEntMapDialog();
  }

  openDBEntMapDialog(): void {
    const dialogRef = this.dialog.open(DBEntMapDialog, {
      width: '40%',
      data: {db_id: this.mapDbEnt.db_id, arr_eid: this.mapDbEnt.arr_eid, database: this.mapDbEnt.database, engine_name:this.mapDbEnt.engine_name}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.getConnectorCustomQry();
      }
    });
  }

  goBack() {
    this.router.navigate(['home/visualizer']);
  }
}

/* Open query Scheduler Dialog to allow user to schedule reports 
* user is allowed to configure email for each report.
*/
@Component({
  selector: 'qry-scheduler',
  templateUrl: 'qry-scheduler-dialog.html',
  styleUrls: ['./visualizer.component.css']
})

export class QrySchedulerDialog implements OnInit {
  titleIcon: string;
  title: string;
  isLoading: boolean = false;
  frequencyColl =['Hourly','Daily','Weekly','Fortnightly','Monthly','Quarterly','Halfyearly','Yearly']

  form = this.fb.group({
    ReportTitle:[this.data.title, Validators.compose([
      Validators.required,
      Validators.maxLength(100)
    ])],
    ReportDesc:[this.data.description,Validators.compose([
      Validators.required,
    ])],
    EnableScheduler:[this.data.enable_scheduler,Validators.compose([
      Validators.required,
    ])],
    Frequency:[this.frequencyColl[1],Validators.compose([
      Validators.required
    ])],
    SendAsAttachment:[this.data.send_as_attachment,Validators.compose([
      Validators.required,
    ])],
   },
  );	

  constructor(
    public dialogRef: MatDialogRef<QrySchedulerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DataScheduler,
    private router: Router,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    public dialog: MatDialog,
    private fb: FormBuilder,
  ) { }
  
  ngOnInit(){
    if (!this.data.is_scheduled)
      this.title = "Add Schedule for \""+this.data.qry_name+'\"('+this.data.id+')'
    else 
      this.title = "Edit Schedule for \""+this.data.qry_name+'\"('+this.data.id+')'

    if (this.data.title == undefined){
      this.form.get('ReportTitle').setValue(this.data.qry_name);
    }
    if (this.data.description == undefined){
      this.form.get('ReportDesc').setValue(this.data.qry_desc);
    }
    if (this.data.frequency == undefined){
      this.form.get('Frequency').setValue(this.frequencyColl[1]);
    } else {
      let idx = this.frequencyColl.indexOf(this.data.frequency);
      this.form.get('Frequency').setValue(this.frequencyColl[idx]);
    }

  }

  getErrorMessage(control, controlName) {
    let msg =' ';
    msg += control.hasError('required') ? 'You must enter a value. ' :'';
    if (controlName =='ReportTitle') {msg += (control.errors != undefined || control.errors.maxlength) ? 'Must be less than 100 char length. ': ''}
    return msg;
  }

  async updateSchedule(){
    this.isLoading = true
    let param = { 
      frequency: this.form.get("Frequency").value,
      enable_scheduler: this.form.get("EnableScheduler").value,
      report_title: this.form.get("ReportTitle").value,
      report_description: this.form.get("ReportDesc").value,
      send_as_attachment: this.form.get('SendAsAttachment').value,
      id: this.data.id,
      time_offset: new Date().getTimezoneOffset()*-1
    };
    let result = await this.authService.updateSchedule(param);
    this.isLoading = false;
    if (result.success){
      this.reusable.openAlertMsg(result.message, "info");
      this.dialogRef.close(this.data);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  onClose() {
    this.dialogRef.close();
  }
}

/* Open query email Dialog to allow user configure email for each report*/
@Component({
  selector: 'qry-config-email',
  templateUrl: 'qry-config-email-dialog.html',
  styleUrls: ['./visualizer.component.css']
})

export class QryConfigEmailDialog implements OnInit {
  title: string;
  isLoading: boolean = false;
 
  form = this.fb.group({
    EmailArr: this.fb.array([]),
  });

  constructor(
    public dialogRef: MatDialogRef<QryConfigEmailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DataEmail,
    private router: Router,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    public dialog: MatDialog,
    private fb: FormBuilder,
  ) { }
  
  ngOnInit(){
    this.title = "Configure email for \""+this.data.qry_name+'\"('+this.data.id+')';
    if(this.data.email_arr != undefined && this.data.email_arr.length > 0){
      this.data.email_arr.map(email => {
        this.addArrEleEmail(email);
      })
    }
  }

  get emailArr(){
    return this.form.get('EmailArr') as FormArray;
  }

  addArrEleEmail(emailId?:string){
    this.emailArr.push(new FormControl(emailId,Validators.compose([
      Validators.required,
      Validators.email,
    ])));
  }

  removeEmailArrElement(idx){
    this.emailArr.removeAt(idx);
    this.form.markAsDirty();
  }

  getErrorMessage(control) {
    let msg =' ';
    msg += control.hasError('required') ? 'You must enter a value. ' :'';
    msg += control.hasError('email') ? 'Not a valid email' :'';
    return msg;
  }

  onClose() {
    this.dialogRef.close();
  }
  
  async updateEmail(){
    let msg = ''
    let old_email_arr = []
    for (let i=0; i<this.emailArr.length; i++){
      old_email_arr.push(this.emailArr.controls[i].value.toLowerCase());
    }
    let email_arr = _uniqBy(old_email_arr);
    if (old_email_arr.length != email_arr.length){
      msg = " Note: Duplicate emails found and removed.";
    }
    let param = {
      id: this.data.id,
      email_arr: email_arr
    }
    let result = await this.authService.updateReportEmail(param)
    if (result.success){
      this.reusable.openAlertMsg(result.message+msg,"info");
      this.dialogRef.close(this.data);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
}

/* ColumnConfigure to Custom Query */
@Component({
  selector: 'config-column',
  templateUrl: 'config-column-dialog.html',
})

export class configColumnDialog implements OnInit {
  title: string;
  isLoading: boolean = false;
  colUnitColl = (['text','numbers','unit','rows','%','timestamp','datetime','date','time','sec','secToTime','ms','msToTime', 'min','hour','bps','KBps','MBps','GBps','bytes','KB','MB','GB','TB','Rs','$','count']).sort();
  chartType = ['line', 'area', 'vbar','hidden','toolTip'];
  paramColl = ['none','param1', 'param2', 'param3', 'param4', 'param5'];

  form = this.fb.group({
    ColDef: this.fb.array([]),
  });

  constructor(
    public dialogRef: MatDialogRef<configColumnDialog>,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private fb: FormBuilder,
    private reusable: ReusableComponent
  ) { }

  ngOnInit(){
    if (this.data.chart.root_chart_type != 'others'){
      this.chartType = [];
      this.chartType.push(this.data.chart.root_chart_type, 'hidden', 'toolTip');
    }
    this.data.dispColumns.map((column) => {
      let type = this.colUnitColl[this.colUnitColl.indexOf(column.data_type)];
      let chartTy = this.chartType[this.chartType.indexOf(column.chart_type)];
      let param = this.paramColl[this.paramColl.indexOf(column.dd_param)];
      this.addArrColDef(column.column_name, type, chartTy, param)
    })
  }

  onChangeParam(param, idx){
    let colLength = this.colDef.length;
    for (let i = 0; i<colLength; i++){
      if (i != idx && this.colDef.controls[i].get('Param').value != "none"){
        if (param == this.colDef.controls[i].get('Param').value){
          param = this.paramColl[0];
          this.reusable.openAlertMsg(param+" is already in use", "info");
        }
      }
    }
  }

  get colDef(){
    return this.form.get('ColDef') as FormArray;
  }

  addArrColDef(defaultName:string, defaultVal:string, defaultChart:string, param:string){
    if (this.colDef.length ==0){
      this.colDef.push(this.fb.group({
        ColName:[{value:defaultName,disabled:true}, Validators.required],
        ColType: [defaultVal, Validators.required],
        ChartType: [{value:'axis-label',disabled:true}, Validators.required],
        Param:[param]
      }));
    } else {
      this.colDef.push(this.fb.group({
        ColName:[{value:defaultName,disabled:true}, Validators.required],
        ColType: [defaultVal, Validators.required],
        ChartType: [defaultChart, Validators.required],
        Param:[param]
      }));
    }
  }

  removeColDefElement(idx){
    this.colDef.removeAt(idx);
  }

  saveColUnits(){
    let colLength = this.colDef.length;
    this.data.dispColumns = [];
    for (let i = 0; i < colLength; i++){
      this.data.dispColumns.push({column_name:this.colDef.controls[i].get('ColName').value, data_type: this.colDef.controls[i].get('ColType').value, chart_type: this.colDef.controls[i].get('ChartType').value, col_unit: this.colDef.controls[i].get('ColType').value, dd_param:this.colDef.controls[i].get('Param').value});
    }
    this.dialogRef.close(this.data);
  }

  onClose(){
    this.dialogRef.close();
  }

}
