import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { VisualizerRoutingModule } from './visualizer-routing.module';
import { VisualizerComponent, dbAddEditDialog, dbAddEditQueryDialog, DBEntMapDialog, QrySaveDialog, QryGridDialog, QrySchedulerDialog,QryConfigEmailDialog,configColumnDialog} from './visualizer.component';
import { D3ChartsModule } from '../d3Directive/d3Directive.module'
import { DashboardModule } from '../dashboard/dashboard.module'
@NgModule({
  declarations: [
    VisualizerComponent,
    dbAddEditDialog,
    dbAddEditQueryDialog,
    DBEntMapDialog,
    QrySaveDialog,
    QryGridDialog,
    QrySchedulerDialog,
    QryConfigEmailDialog,
    configColumnDialog
  ],
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    VisualizerRoutingModule,
    MatAutocompleteModule,
    D3ChartsModule,
    DashboardModule
  ],
  entryComponents: [
    dbAddEditDialog,
    dbAddEditQueryDialog,
    DBEntMapDialog,
    QrySaveDialog,
    QrySchedulerDialog,
    QryConfigEmailDialog,
    configColumnDialog
  ],
})

export class VisualizerModule { }
