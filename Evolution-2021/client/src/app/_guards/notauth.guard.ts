﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from '../_services/index';


@Injectable()
export class NotAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthenticationService
    ) { }

    canActivate() {
        if(this.authService.loggedIn()){
            this.router.navigate(['/login']);
            return false;
        } else {
            return true;
        }
    }
}