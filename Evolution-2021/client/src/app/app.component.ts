import { Component } from '@angular/core';
import { AuthenticationService } from './_services/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Evolution-2021';
  themeChange;
  constructor(
    private authenticationService: AuthenticationService,
  ){}

  ngOnInit(){
    this.authenticationService.themeChange.subscribe(theme =>{
      this.themeChange=theme['themeClass'];
    });
  }

}


