import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotAuthGuard } from './_guards/notauth.guard';
import { AuthGuard } from './_guards/auth.guard';
const routes: Routes = [
  { 
    path: '', 
    canActivate:[NotAuthGuard],
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  { 
    path: 'login', 
    canActivate:[NotAuthGuard],
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  // { 
  //   path: '**', 
  //   loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  // },
  { 
    path: 'register', 
    canActivate:[NotAuthGuard],
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)
  },
  { 
    path: 'verifySignUp/:data', 
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)
  },
  { 
    path: 'verifySlaEmailAlerts/:query', 
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'forgotPassword',
    canActivate:[NotAuthGuard],
    loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
  },
  {
    path: 'resetPassword/:data',
    canActivate:[NotAuthGuard],
    loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
  },
  { path: 'home', 
    canActivate:[AuthGuard], 
    loadChildren: () => import('./sidenav/sidenav.module').then(m => m.SidenavModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
