import { Component, OnInit, HostListener, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { Observable, Unsubscribable, timer, fromEvent, forkJoin, of, from } from 'rxjs';
import { map, concatAll, mergeMap, switchMap, distinctUntilChanged, flatMap, debounce, debounceTime } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl, FormBuilder, FormGroup, Validators, FormControlName} from '@angular/forms';
import { filter as _filter, noop as _noop } from 'lodash';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

export interface DialogAlertManage {
  metricId: string,
  uid: string,
  modType: string,
  category: string,
}

export interface DialogMyChart {
  chartId: boolean,
  category: string,
  refId: string,
  modType: string,
  isSystem: boolean,
  isNewVisualizer: boolean
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})

export class DashboardComponent implements OnInit {
  objectKeys = Object.keys; //used in display of table content json object to html table
  objectValues = Object.values; //used in display of table content json object to html table
  /* Auto Refresh Timer */
  private timerSubscription: Unsubscribable;
  private secondsTimer: Unsubscribable;
  screenAttrib: Unsubscribable;
  isRefreshing = false; enableRefresh = true; refreshTime = 0; autoRefreshEnabled = true; startRefreshTime= false; setRefreshTime = 60;
  
  divCount=[]; isLoading=false;
  draggingWindow; resizer: Function; draggingCorner: boolean;
  idx: any;  px: number;   py: number;   actCord=[]; dispConfig=[];

  stEndDate={};
  moduleCode: any;  isFromMyChart: boolean = true;  dashSelVal: any;  uidLabel: string;  totRowCnt: number;  selDBMetricChart: string;  startDt: any;  endDt: any;
  dtFormat:String = 'MINUTES'; qryInterval:String = '1 MINUTE' ; qryType:string = 'query';  //key name of the type of query to be used. either aggr_query or query
  aggQryTableName ='_'; dateTrunc_ms = 60000;
  varFilterData: { myDashSelValue: any; stEUTCTime: any; endEUTCTime: any; dtFormat: any; qryInterval: any; qryType: any; aggQryTableName: any; dateTrunc_ms: any; mCode: any; };
  verticalScroll: boolean;   myChart = [];   start: number = 0;   scrollLimit: number = 16;   cnt: number = 0;   chartLoaded: number;   totalChartLoading: number;   chartDetails = [];   
  getQryDataParam = []; cvOffset = [];   
  drawChartData = [];
  openAlertTitleOrg: string;
  dispSmallGrid = "none";
  //Filter variables for table
  filterText = []; unFilterData = []; prvFilterTxt =[]; dispSmallGridsIds = []; varFilter =[]; svgData =[]; d3Width=[]; d3Height=[];
  theme;
  callFromSubscribe = false;
  divSize: number; colCount: number; chartPerRow: number = 2;
  returnURL:string;
  obsEnt: any;
  screenWidthHt:any;
  dispColumns = [];
  tableDataSource = [];
  parentRoute;
  @ViewChild(MatSort) sort: MatSort;

  //scroll variables
  offset = []; 
  limit = [];
  isFetched = []
  lastScrolled = [];
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;
  setIntervalId;
  

  handleScroll = (index, scrolled: boolean) => {
    if (scrolled) { this.offset[index] += this.limit[index];}
    let idx = Number(index.replace('idx',''));
    scrolled && this.isFetched[index]? this.getScrollDataProcess(this.chartDetails[idx], this.getQryDataParam[idx], this.offset[index], this.limit[index], idx) : _noop();
    
  }

  hasMore(id){
    return this.isFetched[id];
  }

  constructor(
    private route: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    public dialog: MatDialog
  ) { }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.obsEnt != undefined)
      this.obsEnt.unsubscribe();

    if (this.timerSubscription != undefined) {
        this.timerSubscription.unsubscribe();
    }

    if (this.secondsTimer != undefined) {
      this.secondsTimer.unsubscribe();
    }
    if (this.screenAttrib != undefined){
      this.screenAttrib.unsubscribe();
    }
    this.authService.myDashSelVal.next("--my dashboard--");
    this.authService.dispOnRequest.next(false);
    this.authService.dispMyChart.next(false);
  }
  
  ngOnInit() {
    this.authService.dispOnRequest.next(true);
    this.authService.dispMyChart.next(true);
    this.authService.dispEnt.next(true);
    this.returnURL = sessionStorage.getItem("currentRoute");
    if(!this.returnURL.includes('sum') && !this.returnURL.includes('drilldown')){
      sessionStorage.setItem("parentRoute",this.returnURL);
    }
    sessionStorage.setItem("currentRoute","/home/dashboard");
    let uidData = sessionStorage.getItem('drawChartUid') != undefined ? JSON.parse(sessionStorage.getItem('drawChartUid')) :undefined;
    this.authService.screenChange.subscribe(res => {
      this.screenWidthHt = res;
      this.fnScreenChange(res);
    });
    this.authService.dateEpoch.subscribe(dtColl => {
      this.stEndDate = dtColl;
      if (!this.callFromSubscribe && this.dashSelVal != undefined){
        this.callFromSubscribe = true;
        this.callGetDashboard();
      }
    });
    //my chart observable
    this.authService.myDashSelVal.subscribe(myDashSelVal => {
      if (myDashSelVal != "--my dashboard--"){
        this.dashSelVal = myDashSelVal;
        uidData = undefined;
        sessionStorage.removeItem('drawChartUid');
        this.uidLabel = "Dashboard of "+this.dashSelVal;
        this.isFromMyChart = true;
        if (!this.callFromSubscribe){
          this.callFromSubscribe = true;
          this.callGetDashboard();
        }
      } else {
        if (uidData != undefined){
          this.drawChartData = [];
          this.svgData = [];
          this.tableDataSource = [];
        }
      }
    });
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    })
    //enterprise change observable
    this.obsEnt = this.authService.watchStorage().subscribe(data => {
        if (data && JSON.parse(sessionStorage.getItem('entItem')) != null){
          this.start = 0;
          this.callGetDashboard();
        }
    });
    if (uidData != undefined ){
      this.moduleCode = uidData.module_code;
      let moduleName = uidData.module_name == undefined ? uidData.testname : uidData.module_name;
      this.isFromMyChart = false;
      this.dashSelVal = uidData.uid == undefined ? uidData.test_id : uidData.uid;
      this.uidLabel = this.dashSelVal+" :: "+moduleName;
      this.totRowCnt = Number(uidData.chartCnt);
      this.selDBMetricChart = "Metric Charts";
      this.callGetDashboard();
    } else {
      this.selDBMetricChart = "Metric Charts"
    }
    setTimeout(()=>{
      this.authService.toggleMenu.next(false);
    },2000);

    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => {
        if (event && this.virtualScroll)
          return this.virtualScroll.measureScrollOffset('bottom') === 0;
        else 
          return null;
      })
     ).subscribe(ele =>{
        let id = ele['elementRef']['nativeElement'].id;
        if(this.hasMore(id)){
          this.handleScroll(id,true);
        }
     });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
  }

  resizeId:any;
  private fnScreenChange(screen: {}) {
    /*
    To ensure the redrawing of chart is called if there is no change observed in 2 sec. if there are many changes within 2 sec, system will ignore the changes and wait for the final change before it calls the screen layout call
    */
    if (screen['height'] == 0 && screen['width'] == 0) return;
    clearInterval(this.resizeId);
    this.resizeId = setTimeout(() => {
        this.redrawDiv(screen);
    },1000);
  }

  chartTimeoutId:any;
  async redrawDiv(screen){
    if (screen['width'] > 1230)
      this.divSize = 85;
    else if (screen['width'] > 1100 && screen['width'] <= 1230)
      this.divSize = 65;
    else if (screen['width'] > 900 && screen['width'] <= 1100)
      this.divSize = 65;
    else
      this.divSize = 55;
    if (screen['width'] < 750)
      this.chartPerRow = 1;
    else
      this.chartPerRow = 2;
    let gridCntHt = Math.floor((screen['height'] - 10) / (this.divSize + 10));
    this.colCount = Math.floor((screen['width'] - 20) / (this.divSize + 10));
    let count = gridCntHt * this.colCount;
    this.divCount = [];
    for (let i = 0; i < count; i++) {
      this.divCount.push(i);
    }
    await this.drawDashboard();
    clearInterval(this.chartTimeoutId);
    this.chartTimeoutId = setTimeout(()=>{
      this.chartDetails.map((c,i)=>{
        let ele = document.getElementById("idx"+i);
        if (ele){
          let elRect = ele.getBoundingClientRect();
          this.d3Width[i] = elRect.width;
          this.d3Height[i] = elRect.height;
        }
      })
    },500)
  }

  convertSecToTime(intSec){
    return this.authService.convertSecToTime(intSec);
  }

  decrTimer(){
    this.refreshTime--;
  }

  startAutoRefresh(){
    if (this.autoRefreshEnabled) {
      this.startRefreshTime = true;
      this.refreshTime = this.setRefreshTime;
      if( this.timerSubscription != undefined){
        this.timerSubscription.unsubscribe();
      }
      if (this.secondsTimer != undefined){
        this.secondsTimer.unsubscribe();
      }
      this.timerSubscription = timer(this.refreshTime*1000,this.refreshTime*1000).subscribe(() => this.refreshData());
      this.secondsTimer = timer(1000,1000).subscribe(()=>this.decrTimer());
    }
  }

  refreshData(){
    this.stopRefTime();
    let chartRefresh = _filter(this.getQryDataParam, function(o){ return o.chart.xaxis_time_scale});
    if (chartRefresh.length == 0 || chartRefresh == undefined) return;
    this.verticalScroll = false;
    let etime = (new Date()).getTime();
    let param = chartRefresh[0].param;
    let timeDiff = param.endTime - param.startTime;
    param.endTime = Math.floor(etime/param.dateTrunc_ms)*param.dateTrunc_ms/1000;
    let isHeaderFound = false;
    let chartColl = [];
    let refreshFull = true;
    chartRefresh.map((row) => {
      //row.idx is to ensure, right data set is changed after refresh. using svgData[i] to directly refer the changes to d3
      row.chart.divIdx = row.idx; 
      chartColl.push(row.chart);
      if (row.chart.data != undefined ){
        if (row.chart.data.length > 5) {
          row.chart.data.splice(row.chart.data.length-5, 5);
          if (!isHeaderFound){
            param.startTime = row.chart.data[row.chart.data.length-1][Object.keys(row.chart.data[0])[0]]/1000 + this.dateTrunc_ms/1000;
            isHeaderFound = true;
            refreshFull = false;
          }
        } else {
          param.startTime = param.endTime - timeDiff;
          refreshFull = true;
        }
      } 
    });
    let querySet = this.formQry(chartColl, param);

    if (!refreshFull)
      this.getAndProcessPsqlQry(querySet,param,'refresh');
    else 
      this.getAndProcessPsqlQry(querySet,param,'mainCall');
  
    this.refreshTime = this.setRefreshTime;
  }

  goBack(){
    let parentRoute = sessionStorage.getItem("parentRoute");
    if (!this.isFromMyChart){
      sessionStorage.removeItem('drawChartUid');
      if (parentRoute != undefined){
        this.route.navigate([parentRoute]);  
        sessionStorage.removeItem('parentRoute');
      } else {
        this.route.navigate([this.returnURL]);
      }
    }
  }

  chartParam = {chartId: null, modType: null, refId: null,isSystem:null, category:null };
  enableAddMychart(chart){
    this.chartParam = {chartId: chart.chart_id, modType: chart.module_type, refId: chart.ref_id, isSystem: chart.is_system, category:chart.category};
    this.openMyChartDialog();
  }

  openMyChartDialog(){
    const dialogRef = this.dialog.open(myChartDialog, {
      width: '50%',
      data: {refId: this.chartParam.refId, modType: this.chartParam.modType, chartId: this.chartParam.chartId, isSystem: this.chartParam.isSystem, category: this.chartParam.category,isNewVisualizer:false}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  alertParam = {metricId: null, modType: null, uid: null, category: null};
  openAlertModal(metricId: string, modType: string, uid: string, category: string)
  {
    this.alertParam = {uid: uid, modType: modType, metricId:metricId, category: category};
    this.openAlertDialog();
  }

  openAlertDialog(){
    const dialogRef = this.dialog.open(manageAlertDialog, {
      width: '50%',
      data: {uid: this.alertParam.uid, modType: this.alertParam.modType, metricId: this.alertParam.metricId, category: this.alertParam.category}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  getConfigPos(idx){
    return this.d3Width[idx] <=275 ? (this.d3Width[idx]-165)+'px' : (this.d3Width[idx]-120)+'px';
  }

  onMouseOverConfig(idx){
    this.dispConfig[idx] = 1;
    setTimeout(() => {
      this.dispConfig[idx] = 0;
    }, 5000);
  }

  removeChart(chart){
    let confirmMsg = confirm("Please confirm the removal of chart ("+chart.divTitle+")");
    if (confirmMsg) {
      let rcData = {mcId: chart.mc_id};
      this.authService.removeChart(rcData).subscribe(ack => { 
        this.stopRefTime();
        if (ack.success) {
          this.drawChartData = [];
          this.chartDetails = [];
          this.uidLabel = undefined;
          this.authService.newMyChart.next(true);
          this.reusable.openAlertMsg("Successfully removed", "info");
          this.isLoading = false;
        } else {
          this.drawChartData = [];
          this.chartDetails = [];
          this.reusable.openAlertMsg(ack.message, "error");
          this.isLoading=false;
        }
      });
    }
  }
  
  private callGetDashboard() {
    let entItem = sessionStorage.getItem("entItem");
    if (entItem == undefined) return;
    let dtParams = this.authService.pubSetDTFormat(this.stEndDate['stDate'],this.stEndDate['endDate']);
    this.qryInterval = dtParams.qryInterval;
    this.qryType = dtParams.qryType;
    this.aggQryTableName = dtParams.aggQryTableName;
    this.dateTrunc_ms = dtParams.dateTrunc_ms*1000;
    this.dtFormat = dtParams.dtFormat;
    this.setRefreshTime = dtParams.dateTrunc_ms;

    let param = { myDashSelValue: this.dashSelVal, stEUTCTime: this.stEndDate['stDate'], endEUTCTime: this.stEndDate['endDate'], dtFormat: this.dtFormat, qryInterval: this.qryInterval, qryType: this.qryType, aggQryTableName: this.aggQryTableName, dateTrunc_ms: this.dateTrunc_ms, mCode: this.moduleCode };
    this.pvtGetCharts(param);
  }

  async pvtGetCharts(chParam){
    if (this.isLoading) return;
    if (chParam.myDashSelValue == "--my dashboard--" || chParam.myDashSelValue == undefined) {
      return;
    }
    let entItem = sessionStorage.getItem('entItem') != undefined ? JSON.parse(sessionStorage.getItem('entItem')) : null;

    this.dispSmallGrid ="none";
    this.stopRefTime();
    this.getQryDataParam = [];
    this.setFilterData(chParam);
    this.verticalScroll = true;
    if (!this.isRefreshing) this.isLoading=true;
    const param = {
      isFromMyChart: this.isFromMyChart,
      myDash: chParam.myDashSelValue,
      moduleCode:chParam.mCode,
      startTime: (chParam.stEUTCTime - chParam.stEUTCTime%chParam.dateTrunc_ms)/1000 ,
      endTime: (chParam.endEUTCTime - chParam.endEUTCTime%chParam.dateTrunc_ms)/1000,
      dtFormat: chParam.dtFormat,
      qryInterval: chParam.qryInterval,
      qryType: chParam.qryType,
      aggQryTableName: chParam.aggQryTableName,
      entId: entItem == null ? 0 : entItem.e_id,
      entName: entItem == null ? 'Select Enterprise' : entItem.e_name,
      dateTrunc_ms: chParam.dateTrunc_ms,
      offset:this.start,
      limit: this.scrollLimit,
      chartUsrId: entItem == null ? null : entItem.owner_id,
      timeOffset: new Date().getTimezoneOffset()
    };
    const chartQry = this.authService.getChartQuery(param).pipe(
      // distinctUntilChanged(),
      mergeMap(async encryptResult => {
        if (encryptResult.success){
          let res = await this.authService.decrypt(encryptResult['result']);
          return JSON.parse(res);
        } else {
          return encryptResult
        }
      }),
      map(decrptResult => {
        return decrptResult;
      }),
      map(charts => this.formQry(charts,param)),
    );
    chartQry.subscribe(results => {
      this.isLoading = false;
      if (results == undefined) {
        this.callFromSubscribe = false;
        return;
      }
      if (results.charts.length == 0){
        this.chartDetails = [];
        this.drawChartData = [];
      }
      if (results.charts.length >0){
        this.getAndProcessPsqlQry(results, param,'mainCall');
      }
      else {
        this.reusable.openAlertMsg("No Charts found for this selection","info");
        this.callFromSubscribe = false;
      } 
    });
  }

  async getScrollDataProcess(chart, param, offset, limit, idx){
    let engine = chart.execConn;
    if (chart.execEngine == "postgresql") {
      let qry = chart.execQry+" OFFSET "+offset+" LIMIT "+limit;
      let qParam = {
        query: qry,
        connection_details: chart.execConn,
        timeOffset: new Date().getTimezoneOffset()
      }
      let encryptData = this.authService.encrypt(JSON.stringify(qParam));
      let qryParam ={ data: encryptData};
      let result = await this.authService.getChartDataPsql(qryParam).toPromise();
      if (result.message>0){
        this.chartDetails[idx].data = this.chartDetails[idx].data.concat(result['result']);
        let newData = this.chartDetails[idx].data;
        this.tableDataSource[idx] = new MatTableDataSource(newData);
        this.tableDataSource[idx].sort = this.sort;
      }
    }
  }

  private getAndProcessPsqlQry(chartData:any, param:any, who:string) {
    if (who  == 'mainCall'){
      this.isLoading = true;
      this.chartDetails = chartData.charts;
    }
    let paramColl = chartData.paramColl;
    let engine = chartData.engine;
    let multiRes = [];
    paramColl.map((qParam, ix) => {
      let encryptData = this.authService.encrypt(JSON.stringify(qParam));
      let qryParam = {
        data: encryptData,
      };
      if (engine[ix] == "postgresql") {
        multiRes[ix] = this.authService.getChartDataPsql(qryParam);
      }
      else if (engine[ix] == "mssql") {
        multiRes[ix] = this.authService.getChartDataMssql(qryParam);
      }
      else if (engine[ix] == "clickhouse") {
        multiRes[ix] = this.authService.getChartDataChsql(qryParam);
      }
    });
    forkJoin(multiRes).subscribe(results => {
      this.startAutoRefresh();
      results.map((result, ix) => {
        if (result['success']){
          if (who == 'mainCall'){
            if (this.moduleCode != 'sum' && chartData.charts[ix].xaxis_time_scale && chartData.charts[ix].root_chart_type != "table"){
              // {qryInterval: '15 MINUTES',dtFormat:'MINUTES', qryType:'aggr_query',aggQryTableName:'_aggr_15min_',dateTrunc_ms:900}
              if (engine[ix] == "clickhouse" && param.qryInterval == "30 MINUTES"){
                param.qryInterval = '15 MINUTES';
                param.dtFormat  = "MINUTES";
                param.dateTrunc_ms = 900;
              }
              this.processPsqlData(result['result'], param, ix);
            } else {
              this.chartDetails[ix].data = result['result'];   
            }
            if (chartData.charts[ix].root_chart_type == "table"){
              setTimeout(() => {
                let ddata = chartData.charts[ix].data;
                this.tableDataSource[ix] = new MatTableDataSource(ddata);
                this.tableDataSource[ix].sort = this.sort
                if (this.tableDataSource[ix].data.length>0){
                  this.dispColumns[ix] = Object.keys(this.tableDataSource[ix].data[0]); 
                } else {
                  this.dispColumns[ix] = [];
                }
              }, 2000); //added as processTimeSeriesData inside processPsqlData is an async function, due to processing time for data, if timeout is not set data is set as undefined.
            } else {
              this.tableDataSource[ix] = [];
              this.dispColumns[ix] = [];
            }
          }
          else if (who == 'refresh'){
            if (this.moduleCode != 'sum'){
              this.processRefreshPsqlData(result['result'], chartData.charts[ix], param, ix);
            } else {
              this.svgData[chartData.charts[ix].divIdx] = chartData.charts[ix].data.concat(result['result']);
            }
          }
        } else {
          this.chartDetails[ix].data = [];
          this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
        }
      });
      if(who == 'mainCall'){
        this.afterGetQryDrawDashboard();
      }
      this.isLoading = false;
    },
    err=> {
      this.isLoading = false;
      this.reusable.openAlertMsg(err.message,"error");
   })
  }

  async processRefreshPsqlData(qryData, chart, param, idx){
    let endTime = param.endTime;
    let stTime = param.startTime;
    let interval = this.dateTrunc_ms/1000;
    let ptData = {
      stTime: Math.round(stTime/interval)*interval,
      endTime:Math.round(endTime/interval)*interval,
      interval:interval,
      chData: qryData
    };
    this.authService.processTimeSeriesData(ptData).then(modData=>{
      chart.data = chart.data.concat(modData);
      this.svgData[chart.divIdx] = chart.data;
    })
  }

  private processPsqlData(qryData, param, idx){
    let endTime = this.verticalScroll ? param.endTime : this.getQryDataParam[idx]['endDate'] ;
    let stTime = this.verticalScroll ? param.startTime : this.getQryDataParam[idx]['stDate'] ;
    let interval = param.dateTrunc_ms;
    let ptData = {
      stTime: Math.round(stTime/interval)*interval,
      endTime:Math.round(endTime/interval)*interval,
      interval:interval,
      chData: qryData
    };
    this.authService.processTimeSeriesData(ptData).then(modData => {
      this.chartDetails[idx].data = modData;
    });
  }

  private setFilterData(chParam: any) {
    this.varFilterData = {
      myDashSelValue: chParam.myDashSelValue,
      stEUTCTime: chParam.stEUTCTime,
      endEUTCTime: chParam.endEUTCTime,
      dtFormat: chParam.dtFormat,
      qryInterval: chParam.qryInterval,
      qryType: chParam.qryType,
      aggQryTableName: chParam.aggQryTableName,
      dateTrunc_ms: chParam.dateTrunc_ms,
      mCode: chParam.mCode
    };
  }

  stopRefTime() {
    this.refreshTime = 0;
    if( this.timerSubscription != undefined){
      this.timerSubscription.unsubscribe();
    }
    if (this.secondsTimer != undefined){
      this.secondsTimer.unsubscribe();
    }
    this.startRefreshTime = false;
  }
  
  formQry(charts,param){
    if ((charts.error != undefined && charts.error) || (charts.success != undefined && !charts.success)){
      this.reusable.openAlertMsg(this.authService.invalidSession(charts),"error");
      return;
    }
    let paramColl = [];
    let engineName = [];
    charts.map((chart,ix) => {
      if (this.offset[ix] == undefined){
        this.offset['idx'+ix] = 0;
        this.limit['idx'+ix] = 20;
        this.lastScrolled['idx'+ix] = 0;
        this.isFetched['idx'+ix] = true;
      }
      this.setQryDataParam(ix, chart, param);
      paramColl[ix] = {};
      let query;
      let connectionDetails;
      if (param.qryType == "query"){
        query =chart.query;
        connectionDetails = chart.qry_connection_details;
        engineName[ix] = chart.qry_engine_name;
      } else {
        if (chart.aggr_query != null && chart.aggr_query.length>0){
          query = chart.aggr_query;
          connectionDetails = chart.aggr_connection_details;
          engineName[ix] = chart.aggr_engine_name;
        } else {
          query = chart.query;
          connectionDetails = chart.qry_connection_details;
          engineName[ix] = chart.qry_engine_name;
        }
      }
      let endTime = param.endTime;
      let stTime =  param.startTime;
      if(engineName[ix]=='postgresql') {
        query = query.replace(/@dateTrunc@/gi, param.dtFormat).replace(/@timeInterval@/gi,param.qryInterval).replace(/@startDate@/gi,stTime).replace(/@endDate@/gi, endTime).replace(/@counterId@/gi, chart.ref_table_pkey_id).replace(/@aggr_date@/gi,param.aggQryTableName).replace(/@dateTrunc_ms@/gi,param.dateTrunc_ms/1000);
        chart.execQry = query.toString();
        chart.execConn = connectionDetails;
        chart.execEngine = engineName[ix];
        query += chart.root_chart_type == 'table'? ' OFFSET '+this.offset['idx'+ix]+' LIMIT '+this.limit['idx'+ix] : ''
      } else if(engineName[ix]=='clickhouse'){
        endTime = new Date(endTime*1000).toISOString().slice(0,19).replace('T',' ');
        stTime = new Date(stTime*1000).toISOString().slice(0,19).replace('T',' ');
        query = query.replace(/@dateTrunc@/gi, param.dtFormat).replace(/@startDate@/gi,stTime).replace(/@endDate@/gi, endTime).replace(/@counterId@/gi, chart.ref_table_pkey_id).replace(/@aggr_date@/gi,param.aggQryTableName).replace(/@dateTrunc_ms@/gi,param.dateTrunc_ms/1000);
        if (param.qryInterval == '1 MINUTE' || param.qryInterval == '1 MINUTES'){
          query = query.replace(/@timeInterval@/gi,"toStartOfMinute");
        } else if (param.qryInterval == '5 MINUTES'){
          query = query.replace(/@timeInterval@/gi,"toStartOfFiveMinute");
        } else if(param.qryInterval == '15 MINUTES' || param.qryInterval == '30 MINUTES'){
          query = query.replace(/@timeInterval@/gi,"toStartOfFifteenMinutes");
        } else {
          query = query.replace(/@timeInterval@/gi,"toStartOfHour");
        }
      }
      paramColl[ix] = {
        query: query,
        connection_details: connectionDetails,
        offset: 0,
        limit: 16,
        timeOffset: new Date().getTimezoneOffset()
      };
    });
    return {charts:charts,paramColl:paramColl, engine: engineName};
  }

  private setQryDataParam(ix: any, chart: any, param: any) {
    if (this.getQryDataParam[ix] == undefined) {
      this.getQryDataParam[ix] = {};
    }
    this.getQryDataParam[ix]['chart'] = chart;
    this.getQryDataParam[ix]['param'] = param;
    this.getQryDataParam[ix]['idx'] = ix;
    this.getQryDataParam[ix]['len'] = 0;
    this.getQryDataParam[ix]['endDate'] = this.getQryDataParam[ix]['endDate'] == undefined ? param.endTime : this.getQryDataParam[ix]['endDate'];
    this.getQryDataParam[ix]['stDate'] = this.getQryDataParam[ix]['stDate'] == undefined ? param.startTime : this.getQryDataParam[ix]['stDate'];
  }

  private afterGetQryDrawDashboard() {
    this.isLoading = false;
    this.chartLoaded = undefined;
    this.totalChartLoading = undefined;
    this.callFromSubscribe = false;
    this.fnScreenChange(this.screenWidthHt);
    // this.drawDashboard();
    this.refreshTime = this.setRefreshTime;
    this.startAutoRefresh();
  }

  async drawDashboard() {
    await this.setDashboardValues();
    await this.setChartValues();
    this.chartDetails.map((e,i) =>{
      let gridParam = e.chart_param.grid_param ;
      let cols = gridParam.gce-gridParam.gcs;
      let rows = gridParam.gre-gridParam.grs;
      e.divTitle = this.setDivTitleChartWidth(cols, rows, e.divTitle, i);
    });
    this.drawChartData = this.chartDetails;
  }

  async setChartValues() {
    this.draggingCorner = false;
    this.draggingWindow = false;
    this.chartDetails.map((e,i) =>{
      this.dispConfig[i] = 0.3;
      setTimeout(() => {
        this.dispConfig[i] = 0;
      }, 5000);
      this.dispSmallGridsIds[i] = e.mc_id;
      if (e.root_chart_type == 'table') {
        //this.unFilterData[i] = e.data;
        if (this.varFilter[i] != undefined){
          // this.tableFilter(this.varFilter[i].rowheader, this.varFilter[i].idx, this.varFilter[i].parentIdx,this.varFilter[i].leng,this.varFilter[i].filterType)
        } else {
          this.filterText[i] = [];
        }
      } else{
        this.unFilterData[i] = undefined;
        this.filterText[i] = [];
      }
    });
  }

  async setDashboardValues() {
    let colCnt = this.colCount == undefined ? 8 : Math.floor(this.colCount/this.chartPerRow);
    let initVal = {gcsNext:1, gceNext:colCnt+1, grsNext:1, greNext:4};
    for (let idx = 0; idx<this.chartDetails.length; idx++){
      let ele = this.chartDetails[idx];
      let divTitle = ele.chart_id+' - '+ele.category+' '+ele.chart_title; 
      if(ele.chart_param == null || ele.chart_param.grid_param == undefined) {
        ele.chart_param = {grid_param:{chart_id:ele.chart_id, edit:false, gcs:initVal.gcsNext,gce:initVal.gceNext,grs:initVal.grsNext,gre:initVal.greNext}};
        initVal.gcsNext = initVal.gceNext;
        initVal.gceNext = initVal.gcsNext+colCnt;
        if (initVal.gceNext > this.colCount+1){
          initVal.gcsNext = 1;
          initVal.gceNext = colCnt+1;
          initVal.grsNext = initVal.greNext;
          initVal.greNext = parseInt(initVal.grsNext.toString())+3;
        }
        let cols = ele.chart_param.grid_param.gce-ele.chart_param.grid_param.gcs;
        ele['divTitle'] = divTitle; 
      } else {
        let gridParam = ele.chart_param.grid_param ;
        ele['divTitle'] = divTitle; 
        initVal.gcsNext = gridParam.gce;
        initVal.gceNext = initVal.gcsNext+colCnt;
        if (initVal.gceNext > this.colCount+1){
          initVal.gcsNext = 1;
          initVal.gceNext = colCnt+1;
          initVal.grsNext = gridParam.gre+1;
          initVal.greNext = initVal.grsNext+3;
        }
      }
    }
    this.dispSmallGrid = "none";
  }

  private setDivTitleChartWidth(cols, rows,divTitle: string, idx: number) {
    let calWidth = cols * (this.divSize + 10) - 10;
    let charAllowed = Math.floor(calWidth / 8); //8 - per character 8 pixel for font_14
    let charEqForImgs = Math.ceil(146 / 8);
    let calHt = rows * (this.divSize + 10) - 10;
    let modDivTitle = divTitle.substring(0, (charAllowed-charEqForImgs-10)) ;
    if (modDivTitle.length < divTitle.length){
      modDivTitle += "...";
    }
    this.d3Width[idx] = calWidth;
    this.d3Height[idx] = calHt;
    return modDivTitle;
  }

  setEditMode(idx) {
    this.stopRefTime();
    if(this.idx != undefined){this.reusable.openAlertMsg("There is another chart in edit mode","info"); return;}
    this.dispSmallGrid = "block";
    this.reusable.openAlertMsg("Double Click to finish resizing","info");
    this.isLoading=true;
    this.idx = idx;
    let eleMain = document.getElementById('sidemain');
    let eleMainPos = eleMain.getBoundingClientRect();
    let ele = document.getElementById('idx'+idx);
    let pos = ele.getBoundingClientRect();
    this.actCord[idx]={editMode:true, x: Number(pos['x'])+Number(eleMain['scrollLeft'])-Number(eleMainPos['x']), y:Number(pos['y'])+Number(eleMain['scrollTop'])-Number(eleMainPos['y']), width:pos['width'], height:pos['height']};
    this.chartDetails[idx].chart_param.grid_param['edit']=true;
    // this.chartParamArr[idx]['edit']=true;
    //this to avoid doing multiple activity by the user
    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
  }
  
  getWidthHt(widthHt, idx){
    let ele = document.getElementById('idx'+idx);
    let pos = ele.getBoundingClientRect();
    return pos[widthHt];
  }

  fromD3chart(event){
    let selTime = event[Object.keys(event)[0]];
    let stTime;
    let eTime;
    if(event.chartData.module_type == "SUM"){
      event.sumCardData = JSON.parse(sessionStorage.getItem('drawChartUid'));
      event.dateTime = {startDt: this.stEndDate['stDate']/1000, endDt: this.stEndDate['endDate']
      /1000}
      //event.dateTime = {startDt: this.startDt.getTime()/1000, endDt: this.endDt.getTime()/1000}
      sessionStorage.setItem('sumData', JSON.stringify(event));
      this.route.navigate(['/home/sum']);
    } else {
      if (event.ddMetricId == null) {
        if (event.xAxisTimeScale){
          if (selTime < (new Date().getTime() - 1*60*60*1000) ){
            stTime = selTime - 30*60*1000;
            eTime = selTime + 30*60*1000;
            this.dtFormat = 'MINUTES';
            this.qryInterval = '1 MINUTE';
            this.qryType = 'query';
            this.aggQryTableName ='_';
            this.dateTrunc_ms = 60000;
            let param = { myDashSelValue: this.dashSelVal, stEUTCTime: stTime, endEUTCTime: eTime, dtFormat: this.dtFormat, qryInterval: this.qryInterval, qryType: this.qryType, aggQryTableName: this.aggQryTableName, dateTrunc_ms: this.dateTrunc_ms, mCode: this.moduleCode };

            this.pvtGetCharts(param);
          } else {
            this.reusable.openAlertMsg("Selected time must be less than  1 hour from current time.","info");
            return;
          }
        } else {
          this.reusable.openAlertMsg(event.chartData.chart_id +" does not have drilldown configuration. please configure drilldown from chart visual menu", "info");
        } 
      } else {
        event['startDt'] = this.stEndDate['stDate'];
        event["endDt"] = this.stEndDate['endDate'];
        sessionStorage.setItem("d3DrillDownEnt", JSON.stringify(event));
        this.route.navigate(['/home/drilldown']);
      }
    }
  }

  onWindowPress(event: MouseEvent) {
    this.draggingWindow = true;
    this.px = event.clientX;
    this.py = event.clientY;
  }

  onWindowDrag(event: MouseEvent) {
     if (!this.draggingWindow) return;
    let idx = this.idx;
    let offsetX = event.clientX - this.px;
    let offsetY = event.clientY - this.py;
    this.actCord[idx]['x'] += offsetX;
    this.actCord[idx]['y'] += offsetY;
    this.px = event.clientX;
    this.py = event.clientY;
  }

  bottomRightResize(offsetX: number, offsetY: number) {
    let idx=this.idx;
    this.actCord[idx]['width'] += offsetX;
    this.actCord[idx]['height'] += offsetY;
  }

  onCornerClick(event: MouseEvent, resizer?: Function) {
    this.draggingCorner = true;
    this.px = event.clientX;
    this.py = event.clientY;
    this.resizer = resizer;
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('window:mousemove', ['$event'])
  onCornerMove(event: MouseEvent) {
    if (!this.draggingCorner) return;
    let offsetX = event.clientX - this.px;
    let offsetY = event.clientY - this.py;
    this.resizer(offsetX, offsetY);
    this.px = event.clientX;
    this.py = event.clientY;
  }

  @HostListener('window:mouseup', ['$event'])
  onCornerRelease(_event: MouseEvent) {
    if (this.idx != undefined){
      let idx = this.idx;
      this.draggingWindow = false;
      this.draggingCorner = false;
    }
  }

  async releaseEdit(){
    //setting the grid column/row start and end postion
    let halfsize = Math.floor((this.divSize+10)/2);
    this.chartDetails[this.idx].chart_param.grid_param['gcs'] = Math.ceil((this.actCord[this.idx]['x']+halfsize)/(this.divSize+10));
    this.chartDetails[this.idx].chart_param.grid_param['grs'] = Math.ceil((this.actCord[this.idx]['y']+halfsize)/(this.divSize+10));
    this.chartDetails[this.idx].chart_param.grid_param['gce'] = Math.ceil((this.actCord[this.idx]['x']+this.actCord[this.idx]['width']+halfsize)/(this.divSize+10));
    this.chartDetails[this.idx].chart_param.grid_param['gre'] = Math.ceil((this.actCord[this.idx]['y']+this.actCord[this.idx]['height']+halfsize)/(this.divSize+10));
    this.actCord[this.idx]['editMode']=false;
    this.chartDetails[this.idx].chart_param.grid_param['edit'] = false;
    let cols = this.chartDetails[this.idx].chart_param.grid_param['gce'] - this.chartDetails[this.idx].chart_param.grid_param['gcs'];
    let rows = this.chartDetails[this.idx].chart_param.grid_param['gre'] - this.chartDetails[this.idx].chart_param.grid_param['grs'];
    let divTitle = this.chartDetails[this.idx].chart_id+' - '+this.chartDetails[this.idx].category+' '+this.chartDetails[this.idx].chart_title; 
    this.d3Width[this.idx] = cols*(this.divSize+10)-10;
    this.d3Height[this.idx] = rows*(this.divSize+10)-10;
    this.chartDetails[this.idx]['divTitle'] = this.setDivTitleChartWidth(cols, rows, divTitle, this.idx);
    let ack = confirm("Do you like to save this change?");
    if (ack){
      this.isLoading = true;
      let param = {
        mc_id : this.chartDetails[this.idx]['mc_id'],
        chart_param: JSON.stringify(this.chartDetails[this.idx].chart_param)
      };
      let result = await this.authService.updMyChartGridParam(param);
      this.isLoading = false;
      if (result.success){
        this.reusable.openAlertMsg(result.message,"info");
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
    this.idx = undefined;
    this.dispSmallGrid = "none";
  }
}

/* Dialog screen for alert setting module */
@Component({
  selector: 'manage-alert-dialog',
  templateUrl: 'manage-alert-dialog.html',
  styleUrls: ['./dashboard.component.css']
})

export class manageAlertDialog implements OnInit{
  isLoading:boolean = false;
  btnDisable:boolean = false;
  tableName: string;
  openAlertTitle: string;
  alertPolicyColl = new MatTableDataSource([]);
  processNameColl: any;
  dispProcessName: boolean;
  processName: string;
  dispAlertGrid:boolean = true;
  slaId;
  theme;
  form: FormGroup;
  policyName:string; policyDesc:string; warnThreshold:number; criticalThreshold:number; minBreachCnt:number; 
  isAboveThreshold:boolean = true;
  
  @ViewChild(MatSort) sort: MatSort;

  dispPolicyColumns = ['delete', 'edit', 'policy_name','critical_threshold_value','warning_threshold_value','min_breach_count']

  constructor (
    public dialogRef: MatDialogRef<manageAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogAlertManage,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private formBuilder: FormBuilder
  ) {}
  
  ngOnInit(){
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.policyName = this.data.uid+'::'+this.data.category;
    this.policyDesc = this.policyName;
    this.isAboveThreshold = true;
    this.manageAlert();
  }

  async manageAlert(){
    this.alertPolicyColl.data = [];
    this.processNameColl = [];
    this.dispAlertGrid = true;
    let modType = this.data.modType == undefined ? "Others" : this.data.modType;
    if (modType.toLowerCase()== 'application' || modType.toLowerCase()== 'server' || modType.toLowerCase()== 'database')
      this.tableName = 'so_sla_counter';
    else if (modType.toLowerCase() == 'sum') this.tableName = 'so_sla_sum'
    else if (modType.toLowerCase() == 'rum') this.tableName = 'so_sla_rum'
    else {
      this.reusable.openAlertMsg("Alert is not yet build for this module type "+modType,"info");
      return;
    }
    this.isLoading = true;    
    let alertData = {
      metricId: this.data.metricId,
      uid: this.data.uid,
      source: this.tableName   
    };
    if (this.tableName =='so_sla_counter'){
      let slaPolicyRes = this.authService.getSlaPoliciesByMetric(alertData);
      let proNameRes = this.authService.getProcessNameByMetric(alertData);
      forkJoin([slaPolicyRes,proNameRes]).subscribe(results => {
        this.isLoading = false;
        let res = results[0];
        this.processSLAPolicy(res, alertData);
        let res1 = results[1];
        if (res1.success){
          this.processNameColl = res1.result == undefined? [] : res1.result;
          if (this.processNameColl.length == 0) this.dispProcessName = false;
          else {
            this.processNameColl.unshift({process_name:"--select process--"});
            this.dispProcessName = true;
            this.onChangeProcessName("--select process--")
          }
        } else {
          this.dispProcessName = false;
          this.authService.invalidSession(res1, false);
        }
      });
    } else {
      let slaPolicyRes = await this.authService.getSlaPoliciesByMetric(alertData);
      this.processSLAPolicy(slaPolicyRes, alertData);
    }
  }

  private processSLAPolicy(res: any, alertData: { metricId: string; uid: string; source: string; }) {
    if (res.success) {
      this.alertPolicyColl = res.result == undefined ? new MatTableDataSource([]) :  new MatTableDataSource(res.result);
      this.alertPolicyColl.sort = this.sort;
      this.openAlertTitle = this.data.category + " (unit in " + this.alertPolicyColl.data[0].unit + ")";
    } else {
      this.getMetricUnit(alertData);
      this.authService.invalidSession(res, false);
    }
  }

  async getMetricUnit(alertData){
    let result = await this.authService.getMetricUnit(alertData);
    if(result.success){
      this.openAlertTitle = this.data.category +" (unit in "+result['result'][0].unit+")";
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  } 
  
  onClose(): void {
    if (!this.dispAlertGrid){
      this.dispAlertGrid = true;
    } else {
      this.dialogRef.close();
    }
  }

  validateStringEmpty(){
  }

  createPolicy(){
    this.dispAlertGrid = false;
    this.createAlertForm();
  }

  createAlertForm(){
    this.form = this.formBuilder.group({
     PolicyName:[this.policyName, Validators.compose([
       Validators.required,
     ])],
     PolicyDesc:[this.policyDesc, Validators.compose([
       Validators.required,
     ])],
     WarnThreshold:[this.warnThreshold,Validators.compose([
       Validators.required,
       Validators.min(1),
       this.validateNumber
     ])],
     CriticalThreshold:[this.criticalThreshold,Validators.compose([
       Validators.required,
       Validators.min(1),
       this.validateNumber
     ])],
     MinBreachCount:[this.minBreachCnt,Validators.compose([
       Validators.required,
       Validators.min(1),
       this.validateNumber
     ])],
     IsAboveThreshold:[this.isAboveThreshold],
     ProcessName:[this.processName],
   });	
  }

  onChangeProcessName(pName){
    this.processName = pName;
  }

  validateNumber(controls){
		const reqExp = new RegExp(/^[0-9]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateNumber' : true};
		}
  }
  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    msg += control.hasError('min') ? 'Must be a positive integer' :'';
    if (controlName =='WarnThreshold') {msg += control.hasError('validateNumber') ? 'Only Numeric Fields Allowed' :''}
    if (controlName =='CriticalThreshold') {msg += control.hasError('validateNumber') ? 'Only Numeric Fields Allowed' :''}
    if (controlName =='MinBreachCount') {msg += control.hasError('validateNumber') ? 'Only Numeric Fields Allowed' :''}
    return msg;
  }

  async alertPolicySubmit(){
    if (this.dispProcessName){
      if (this.processName == "--select process--") {
        this.reusable.openAlertMsg("please select the process to continue", "error");
        return;
      }
    }
    this.isLoading=true;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'))
    let eId = entItem != undefined && entItem.e_id != undefined && entItem.e_id !=0 ? entItem.e_id : null
    let policyData = {
      sla_id: this.slaId == undefined ? null : this.slaId,
      sla_name: this.form.get('PolicyName').value,
      sla_description: this.form.get('PolicyDesc').value,
      server_details:"",
      server_details_type:"",
      e_id: eId,
      sla_type: "Alert",
      metric_id: this.data.metricId,
      module_type: this.data.modType,
      process_name: this.processName == undefined ? null : this.processName,
      uid: this.data.uid,
      warn_threshold_value: this.form.get('WarnThreshold').value,
      critical_threshold_value: this.form.get('CriticalThreshold').value,
      is_above_threshold: this.form.get("IsAboveThreshold").value,
      min_breach_cnt: this.form.get('MinBreachCount').value,
      table_name: this.tableName
    };
    if (this.slaId == null){
      let res = await this.authService.insSLA(policyData);
      this.isLoading=false;
      if (res.success){
        this.reusable.openAlertMsg(res.message, "info");
        this.dispAlertGrid = true;
        this.manageAlert();
        this.form.reset();
        this.clearPolicyData();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
      }
    } else {
      let res = await this.authService.updSLA(policyData)
      this.isLoading=false;
      if (res.success){
        this.reusable.openAlertMsg(res.message, "info");
        this.dispAlertGrid = true;
        this.manageAlert();
        this.clearPolicyData();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
      }
    }
  }

  clearPolicyData(){
    this.slaId = undefined;
    this.policyName = undefined;
    this.policyDesc = undefined;
    this.isAboveThreshold = undefined;
    this.criticalThreshold = undefined;
    this.warnThreshold = undefined;
    this.minBreachCnt = undefined;
    this.processName = undefined;
  }

  editAlert(pol){
    this.slaId = pol.sla_id;
    this.policyName = pol.sla_name;
    this.policyDesc = pol.sla_description;
    this.isAboveThreshold = pol.is_above_threshold;
    this.criticalThreshold = pol.critical_threshold_value;
    this.warnThreshold = pol.warning_threshold_value;
    this.minBreachCnt = pol.min_breach_count;
    this.processName = pol.process_name == '' || pol.process_name == null ? undefined : pol.process_name;
    this.dispAlertGrid = false;
    this.createAlertForm();
  }

  async removeAlert(pol){
    let confirmMsg = confirm("Are you sure you want to remove the policy? Please confirm");
    if (confirmMsg){
      let entItem = JSON.parse(sessionStorage.getItem("entItem"));
      let param = {sla_id : pol.sla_id, uid: this.data.uid, owner_id:entItem.owner_id};
      this.isLoading = true;
      let result = await this.authService.removeAlert(param);
      this.isLoading = false;
      if (result.success){
        this.reusable.openAlertMsg(result.message, "info");
        this.manageAlert();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  }
}

/* Dialog for myChart-dialog.html */
@Component({
  selector: 'myChart-dialog',
  templateUrl: 'myChart-dialog.html',
})
export class myChartDialog  implements OnInit{
  dashboardColl = [];
  myChartSelVal:string;
  myChartName:string;
  errMsg:string;
  isLoading:boolean = false;
  title:string;

  constructor(
    public dialogRef: MatDialogRef<myChartDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogMyChart,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
  ) {}

  ngOnInit(){
    this.title = this.data.category+"("+this.data.chartId+")";
    this.getDashboard();
  }

  async getDashboard(){
    let result = await this.authService.getDashboard()
      if (result.success){
        this.dashboardColl = result['result'];
        this.dashboardColl.unshift({mc_name:'--my dashboard--'});
        this.onChangeDashName('--my dashboard--');
      } else {
        this.myChartSelVal = "--my dashboard--";
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
  }

  onChangeDashName(myChartName){
    this.myChartSelVal = myChartName;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onEditMyChartName(){
    const reqExp = new RegExp(/^[a-zA-Z0-9 :_-]*$/);
    if (reqExp.test(this.myChartName)){
      return true;
    } else {
      this.errMsg = " Name cannot be empty(A-Z,a-z,0-9,Space,:,_,- are allowed).";
      return false;
    }
  }

  async addToMyChart(){
    this.isLoading = true;
    this.errMsg = undefined;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let mcName = this.myChartSelVal=="--my dashboard--" && this.myChartName != undefined ? this.myChartName.trim() : this.myChartSelVal != "--my dashboard--" ? this.myChartSelVal : undefined;
    if (mcName == undefined || mcName == '' || !this.onEditMyChartName() ){
      this.errMsg = " Choose from My dashboard or Create new"
      this.isLoading = false;
      return ;
    }
    let myChartData = {
      mcName : mcName ,
      chartId : this.data.chartId,
      refId : this.data.refId == null ? -1 : this.data.refId,
      moduleType : this.data.modType,
      eId : entItem != undefined && entItem.e_id != undefined && entItem.e_id !=0 ? entItem.e_id : null,
      entOwnerId : entItem != undefined && entItem.owner_id != null && entItem.owner_id !=0 ? entItem.owner_id : null,
      modifiedOn : new Date(),
      isSystem : this.data.isSystem,
      isNewVisualizer: this.data.isNewVisualizer
    };
    let result = await this.authService.addToMyChart(myChartData);
    this.isLoading = false;
    if (result.success){
      this.authService.newMyChart.next(true);
      this.reusable.openAlertMsg(result.message,"info");
      this.onClose();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }  
}