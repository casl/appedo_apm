import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderModule } from '../login-header/login-header.module'
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent,manageAlertDialog, myChartDialog } from './dashboard.component';
// import { D3chartsv3Directive } from '../d3chartsv3.directive';
import{ D3ChartsModule} from '../d3Directive/d3Directive.module'

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
    LoginHeaderModule,
    // ScrollContainerModule,
    D3ChartsModule
  ],
  declarations: [
    DashboardComponent,
    // D3chartsv3Directive,
    manageAlertDialog,
    myChartDialog,
  ],
  entryComponents: [
    manageAlertDialog,
    myChartDialog,
  ],
})

export class DashboardModule { }

