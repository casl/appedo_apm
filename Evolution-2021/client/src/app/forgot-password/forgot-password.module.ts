import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderModule } from '../login-header/login-header.module'

@NgModule({
  imports: [
    CommonModule,
    ForgotPasswordRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
    LoginHeaderModule
  ],
  declarations: [
    ForgotPasswordComponent,
  ],
})

export class ForgotPasswordModule { }

