import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})

export class ForgotPasswordComponent implements OnInit {
  isLoading:boolean = false; mode="indeterminate"; color="primary"; value=50;
  passwordReqData:String='';
  showPasswordRequest:boolean=false; showChangePassword=false; hide=true;

  email = new FormControl('', [Validators.required, Validators.email]);
  newPassword = new FormControl('',[Validators.required]);
  confirm = new FormControl('',[Validators.required]);

	getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' : '';
	}

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
  ) { }

  ngOnInit() {
    if (this.activatedRoute.snapshot.paramMap.has('data')) {
      this.showChangePassword = true;
      this.passwordReqData = this.activatedRoute.snapshot.paramMap.get('data');
    } else {
      this.showPasswordRequest = true;
    }
  }

  onClickCancel() {
    this.router.navigate(['login']);
  }

  async changeForgottenPassword() {
    if (!(this.newPassword.value.trim().length > 0  || this.confirm.value.trim().length > 0 )) {
      this.reusable.openAlertMsg("Password column can't be empty.","error");
    } else if (this.newPassword.value != this.confirm.value) {
      this.reusable.openAlertMsg("Both passwords are not same. Enter same value.","error");
    } else {
      this.isLoading = true;
      let paramData={
        newPassword: this.newPassword.value,
        confirmPassword: this.confirm.value,
        data: this.passwordReqData
      };
      let data = await this.authService.changeForgottenPassword(paramData);
      this.isLoading = false;
      if (data.success) {
          this.reusable.openAlertMsg(data.message,"info");
          this.router.navigate(['/login']);
      } else {
        this.reusable.openAlertMsg(data.message,"error");
        this.authService.invalidSession(data,false);
      }
    }
  }

  async sendForgotPasswordReq() {
    this.isLoading = true;
    if (this.email.valid) {
      let paramData = {
        emailId: this.email.value
      };
      let data = await this.authService.forgotPassword(paramData);
        this.isLoading = false;
        if (data.success){
            this.reusable.openAlertMsg(data.message,"info");
            this.router.navigate(['/login']);
        } else {
          this.reusable.openAlertMsg(data.message,"error");
          this.authService.invalidSession(data,false);
        }
    } else {
      this.reusable.openAlertMsg("Enter valid E-Mail Id.","warn");
    }
  }
}
