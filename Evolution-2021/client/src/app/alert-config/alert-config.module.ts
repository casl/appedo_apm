import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AlertConfigRoutingModule } from './alert-config-routing.module';
import { AlertConfigComponent } from './alert-config.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderModule } from '../login-header/login-header.module'
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';

@NgModule({
  imports: [
    CommonModule,
    AlertConfigRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
    LoginHeaderModule,
    // ScrollContainerModule
  ],
  declarations: [
    AlertConfigComponent,
  ],
})

export class AlertConfigModule { }

