import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { FormBuilder, FormControl, FormGroup, Validators, FormControlName } from '@angular/forms';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatPaginator } from '@angular/material/paginator';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { noop as _noop } from 'lodash';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-alert-config',
  templateUrl: './alert-config.component.html',
  styleUrls: ['./alert-config.component.scss']
})
export class AlertConfigComponent implements OnInit {
  //[x: string]: any;
  form: FormGroup; alertForm: FormGroup; isLoading;
  pageTitle; mode; obsEnt;
  //variables for alert
  openAlert = false; dispAlert = "none"; openAlertTitle; dispAlertGrid; agentId; formTitle; avmMode; entName; avmAlertId;
  slaSettingId; alertEmailSmsId; oldEmailId; oldValidatedOn; 
  alertColl = new MatTableDataSource([]);
  returnUrl: string;
  //scroll variables
  offset = 0; 
  limit = 10;
  isFetched = false;
  lastScrolled = 0;
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;
  setIntervalId;
  isClickedVerifyOTP = false;
  otpNumber:string;
  showResend = true;

  screenParam:any;
  @ViewChild(MatSort) sort: MatSort;
  theme:any;

  handleScroll = (scrolled: boolean) => {
    if (scrolled) { this.offset += this.limit;}
    scrolled && this.isFetched? this.getAlertForSetting(this.offset, this.limit) : _noop();
  }

  hasMore(){
    return this.isFetched;
  }

  constructor (    
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ){
    this.createForm();
    this.createAlertForm();
  }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    if (this.obsEnt!=null)
      this.obsEnt.unsubscribe();
  }
  
  ngOnInit() {
    this.returnUrl = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/alertconfig");
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })

    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.owner_id == null || entItem.owner_id == usrDet.unq_id){
      this.obsEnt = this.authService.watchStorage().subscribe(data =>{
        if (data){
          let entItem = JSON.parse(sessionStorage.getItem('entItem'));
          this.entName = entItem.owner_id != null ? " for enterprise '"+ entItem.e_name +"'" : '';
          this.formTitle = "Alert Setting"+this.entName;
          this.openAlertTitle = "Add/Update Email/SMS" + this.entName;
          this.openAlert = false;
          let alertData = {
            entId : entItem.owner_id != null ? entItem.e_id : null,
            eName : entItem.owner_id != null ? entItem.e_name : null
          }
          this.getAlertSetting(alertData);
        }
      });
      let alertData = {
        entId : entItem.owner_id != null ? entItem.e_id : null,
        eName : entItem.owner_id != null ? entItem.e_name : null
      }
      this.getAlertSetting(alertData);
      this.entName = entItem.owner_id != null ? " for enterprise '"+ entItem.e_name +"'" : '';
      this.pageTitle= "Alert Setting - Set";
      this.formTitle = "Alert Setting"+this.entName;
      this.openAlertTitle = "Add/Update Email/SMS" + this.entName;
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can modify the alert setting",'error');
      this.closeAlertModal();
    }

    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
  }

  async resendOTP(element){
    setTimeout(() => {
      this.showResend = true;
    }, 60000);
    let param = {sla_setting_id : element.sla_setting_id, email_mobile : element.email_mobile};
    this.isLoading = true;
    let result = await this.authService.resendOTP(param);
    this.isLoading = false;
    if (result.success){
      this.reusable.openAlertMsg(result.message,'info');
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async checkOTP(slaSettingId){
    if (this.otpNumber == undefined || this.otpNumber.trim().length != 4){
      this.reusable.openAlertMsg("Invalid OTP, Enter 4 digit number you have received","error");
      return;
    }
    let param = {sla_setting_id: slaSettingId, OTP: this.otpNumber.trim()};
    let result = await this.authService.validateOTP(param);
    if (result.success){
      this.otpNumber = undefined;
      this.reusable.openAlertMsg(result.message, "info");
        let entItem = JSON.parse(sessionStorage.getItem('entItem'));
        this.getAlertForSetting();
    } else {
      this.reusable.openAlertMsg(result.message+". It is best practice to start with +91 and 10 digit mobile number.", "error");
    }
  }

  async getAlertSetting(alertData){
    this.isLoading = true;
    let result = await this.authService.getAlertSettings(alertData);
    this.isLoading = false;
    if (result.success){
      let res = result.result[0];
      this.slaSettingId = res.sla_setting_id;
      this.form.setValue({
        tryCountDurationInMin: res.try_count_duration_in_min,
        triggerAlertEveryInMin: res.trigger_alert_every_in_min,
        maxTryCount: res.max_try_count
      })
    } else {
      this.form.setValue({
        tryCountDurationInMin: 60,
        triggerAlertEveryInMin: 30,
        maxTryCount: 2,
      });
      this.slaSettingId = "New";
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
 
  save(action){
    if (this.form.valid){
      let entItem = JSON.parse(sessionStorage.getItem('entItem'));
      let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
      if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
        let setting = {
          e_id: entItem.owner_id == null ? null : entItem.e_id,
          e_name: entItem.owner_id == null ? null : entItem.e_name,
          try_count_duration_in_min: this.form.get('tryCountDurationInMin').value,
          trigger_alert_every_in_min: this.form.get('triggerAlertEveryInMin').value, 
          max_try_count: this.form.get('maxTryCount').value,
          sla_setting_id: this.slaSettingId == 'New' ? null : this.slaSettingId
        };
        this.addUpdateAlertSetting(setting,action);
      } else {
        this.reusable.openAlertMsg("Sorry, Only owner of enterprise '"+entItem.e_name+"' can set alert settings",'error');
      }
    } else {
      this.reusable.openAlertMsg("Form is not valid, check the errors and correct the same", 'error');
    }
  }

  async addUpdateAlertSetting(setting, action){
    this.isLoading = true;
    let result = await this.authService.addUpdateAlertSettings(setting);
    this.isLoading = false;
    if (result.success){
      this.reusable.openAlertMsg(result.message, true);
      if(action == 'saveNext'){
        this.openAlertModal();
      } else {
        this.closeAlertModal();
      }
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='tryCountDurationInMin') {msg += (control.errors.min || control.errors.max) ? 'Must be between 0 & 300': ''}
    if (controlName =='triggerAlertEveryInMin') {msg += (control.errors.min || control.errors.max) ? 'Must be between 0 & 300': ''}
    if (controlName =='maxTryCount') {msg += (control.errors.min || control.errors.max) ? 'Must be between 0 & 30': ''}
    return msg;
  }

  createForm(){
		this.form = this.formBuilder.group({
      tryCountDurationInMin:[60, Validators.compose([
				Validators.required,
        Validators.min(0),
        Validators.max(300),
        // this.validateNumber
      ])],
      triggerAlertEveryInMin:[30, Validators.compose([
				Validators.required,
        Validators.min(0),
        Validators.max(300),
        // this.validateNumber
      ])],
      maxTryCount:[2, Validators.compose([
        Validators.required,
        Validators.min(0),
        Validators.max(30),
        // this.validateNumber
      ])]
     });	
  }

  getErrMsgAlert(control, controlName) {
    let msg ='';
    if (controlName =='email') {msg += control.hasError('email') ? 'Not a valid email' :''}
    if (controlName =='sms') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 13 & 15 Char length including +': ''}
    if (controlName =='sms') {msg += !control.value.startsWith('+') ? 'Must start with +': ''}
    if (controlName =='sms') {msg += control.hasError('validateMobile') ? 'Only Numeric Fields Allowed' :''}
    return msg;
  }

  createAlertForm(){
		this.alertForm = this.formBuilder.group({
      alertType:['email'],
      email:[null,Validators.compose([
        Validators.email
      ])],
      sms:[null,Validators.compose([
        Validators.minLength(13),
        Validators.maxLength(15),
        this.validateMobile
      ])]
    }, 
      {validator: this.validateEmailOrSMS('email', 'sms','alertType')}
    );
  }

  selectEmailOrSMS(){
    let varEmail = this.alertForm.get('email').value;
    let varSMS = this.alertForm.get('sms').value;
    if (this.alertForm.get('alertType').value == 'email'){
      this.alertForm.setValue({'sms':null,'alertType':'email', 'email':varEmail});
    } else {
      this.alertForm.setValue({'email':null,'alertType':'sms','sms': varSMS});
    }
  }

  validateEmailOrSMS(email,sms,alertType){
    return (group: FormGroup) => {
      if (group.controls[alertType].value == 'email' && group.controls[email].value != null && group.controls[email].value != ''){
        return null;
      } else if (group.controls[alertType].value == 'sms' && group.controls[sms].value != null){
        return null;
      } else {
        return {'validateEmailOrSMS' : true};
      }
    }
  }

  validateNumber(controls){
		const reqExp = new RegExp(/^[0-9 -_\&]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
  }

  validateEmail(controls){
    if (controls.value == null) return null;
		const reqExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateEmail' : true};
		}
	}

  validateMobile(controls){
    if (controls.value != null){
      if (controls.value.substring(0,1) != '+' || controls.value.length < 12 || controls.value.length>20){
        return { 'validateMobile' : true};
      }
      const reqExp = new RegExp(/^[+][0-9]*$/);
      if (reqExp.test(controls.value)){
        return null;
      } else{
        return { 'validateMobile' : true};
      }
    } else {
      return null;
    }
	}

  openAlertModal(){
    this.openAlert = true;
    this.dispAlert = "block";
    this.dispAlertGrid = true;
    this.getAlertForSetting();
  }

  displayedColumns=['edit','delete','alert_type','email_mobile','validated_on','OTP Status'];
  async getAlertForSetting(offset?:number, limit?:number){
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 10;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let param = {
      e_id: entItem.owner_id == null ? null : entItem.e_id,
      e_name: entItem.owner_id == null ? null : entItem.e_name,
      offset: offset,
      limit: limit
    };
    this.isLoading = true;
    let result = await this.authService.getAlertsEmailSms(param);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      result.result.map(row => {
          row['isOTPVerified'] = false;
      })
      this.isFetched = true;
      if (this.alertColl.data.length == 0 || reset){
        this.alertColl = new MatTableDataSource(result.result);
      } else {
        let newData = this.alertColl.data.concat(result.result);
        this.alertColl = new MatTableDataSource(newData);
      }
      this.alertColl.sort = this.sort;
    } else if (result.rowCount == 0){
      if (reset){
        this.alertColl = new MatTableDataSource([]);
      }
      this.isFetched = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  addAlert(){
    this.alertEmailSmsId = null;
    this.dispAlertGrid = false;
    this.alertForm.setValue({
      alertType: 'email',
      email: null,
      sms:null,
    });
  }

  editAlert(element){
    this.dispAlertGrid = false;    
    this.alertEmailSmsId  = element.sla_setting_id;
    this.oldEmailId = element.email_mobile;
    this.oldValidatedOn = element.validated_on;
    this.alertForm.setValue({
      alertType: element.alert_type.toLowerCase(),
      email: element.alert_type.toLowerCase() == 'email' ? element.email_mobile : null,
      sms: element.alert_type.toLowerCase() == 'sms' ? element.email_mobile : null,
    });
  }

  async removeAlert(element){
    let cMsg = confirm("Are you sure, you want to remove?");
    if (!cMsg) return;
    this.isLoading = true;
    let rmData = {
      sla_setting_id : element.sla_setting_id
    }
    let result = await this.authService.removeAlertsEmailSms(rmData);
    if (result.success){
      this.reusable.openAlertMsg(result.message,'info');
      this.dispAlertGrid = true;
      this.getAlertForSetting();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  async addUpdAlert(param,nxtAct){
    this.isLoading = true;
    let result = await this.authService.updateAddAlertsEmailSms(param);
    this.isLoading = false;
    if (result.success){
      if(nxtAct == 'finish'){
        this.reusable.openAlertMsg(result.message,'info');
        this.dispAlertGrid = true;
        this.alertEmailSmsId = null;
        this.oldEmailId = null;
        this.oldValidatedOn = null;
        this.getAlertForSetting();
      } else {
        this.reusable.openAlertMsg(result.message+", Add another alert","info");
        this.alertForm.setValue({
          alertType: 'email',
          email: null,
          sms: null,
        });
      } 
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }

  saveAlert(nxtAct) {
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      if (this.alertForm.get('alertType').value == 'email' && (this.alertForm.get("email").value == null || this.alertForm.get("email").value=='')){
        this.reusable.openAlertMsg("Must enter valid email","error");
        return;
      }
      if (this.alertForm.get('alertType').value == 'sms' && (this.alertForm.get("sms").value == null || this.alertForm.get("sms").value=='' || this.alertForm.get("sms").value=='+')){
        this.reusable.openAlertMsg("Must enter valid Mobile","error");
        return;
      }
      let param = {
        userFirstName : usrDet.name,
        IS_NON_LOGIN_USER : this.alertForm.get('alertType').value == 'email' ? (this.alertForm.get("email").value != usrDet.email ? true : false ) : false, 
        e_id : entItem.owner_id == null ? null : entItem.e_id,
        e_name : entItem.owner_id == null ? null : entItem.e_name,
        sla_setting_id : this.alertEmailSmsId,
        alert_type : (this.alertForm.get('alertType').value).toUpperCase(),
        email_mobile : this.alertForm.get('alertType').value == 'email' ? this.alertForm.get('email').value : this.alertForm.get('sms').value,
        is_valid : false,
        validated_on: this.alertForm.get("email").value != this.oldEmailId ? this.alertForm.get("sms").value == this.oldEmailId ? this.oldValidatedOn : null : this.oldValidatedOn,
      };
      this.addUpdAlert(param, nxtAct);
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner cand add/update email or SMS", 'error');
    }
  }

  async cancelAlert(){
    await this.getAlertForSetting();
    if (this.alertColl.data.length > 0){
      this.dispAlertGrid = true;
    }
    else 
      this.closeAlertModal();
  }

  closeAlertModal(){
    if (this.returnUrl != undefined && this.returnUrl != '/home/alertconfig')
      this.router.navigate([this.returnUrl]);
    else
      this.router.navigate(['/home']);
  }
}
