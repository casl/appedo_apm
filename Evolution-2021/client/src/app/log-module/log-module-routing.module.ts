import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogModuleComponent, LogDetailsComponent } from './log-module.component';

const routes: Routes = [
  {
    path: '',
    component: LogModuleComponent
  },
  {
    path: 'logdetails',
    component: LogDetailsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class LogModuleRoutingModule { }

