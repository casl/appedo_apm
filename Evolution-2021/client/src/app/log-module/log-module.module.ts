import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { LogModuleRoutingModule } from './log-module-routing.module';
import { LogModuleComponent, AlertDialog, LogDetailsComponent } from './log-module.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ModuleDetailsModule } from '../module-details/module-details.module';

@NgModule({
  imports: [
    CommonModule,
    LogModuleRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    ModuleDetailsModule,
  ],
  exports:[
    AlertDialog,
    LogDetailsComponent
  ],
  declarations: [
    LogModuleComponent,
    AlertDialog,
    LogDetailsComponent
  ],
  entryComponents: [
    AlertDialog
  ],
})

export class LogModuleModule { }

