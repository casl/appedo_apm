import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, forkJoin, pipe } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { moduleEditDialog } from '../module-details/module-details.component'
import { FormControl, FormBuilder, FormGroup, Validators, FormControlName} from '@angular/forms';
import { filter, map } from 'rxjs/operators';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { noop as _noop} from 'lodash';


export interface ClassAlertData {
  log_grok: string,
  log_table_name: string,
  module_name: string,
  uid: number,
  module_code: string
}

@Component({
  selector: 'app-log-module',
  templateUrl: './log-module.component.html',
  styleUrls: ['../visualizer/visualizer.component.css']
})
export class LogModuleComponent implements OnInit {
  isLoading:boolean = false;
  modDetails: any;
  modCode: any;
  moduleTitle1: string;
  moduleTitle2: string;
  modIcon: { iconName: string; clrClass: string; };
  entItem: any;
  logModuleColl = [];
  modColl = new MatTableDataSource([]);
  modCollDispColumn = ['delete','edit', 'uid', 'log_grok', 'module_name', 'alert','chart','details','count','critical', 'warning'];
  userDet: any;
  alertData: ClassAlertData;
  theme:any;
  @ViewChild(MatSort, { static: true }) tblSort: MatSort;
  modEditParam: { uid: any; modName: any; modDesc: any; modCode: any; };
  screenParam: any;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    public dialog: MatDialog
  ) { }


  ngOnInit() {
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })

    sessionStorage.setItem("currentRoute","/home/logmodule");
    this.entItem = JSON.parse(sessionStorage.getItem("entItem"));
    this.userDet = JSON.parse(sessionStorage.getItem("apdUserDet"));
    let modDetails = this.modDetails = JSON.parse(sessionStorage.getItem("logmodule"));
    if (modDetails == undefined){
      this.goBack();
    } else {
      this.modCode = modDetails.moduleType;
      let selRow = modDetails.selRow;
      this.getModuleIcon(this.modCode);
      this.moduleTitle1 = this.modCode+" details of ";
      this.moduleTitle2 = selRow.system_name+" "+selRow.owner+"("+selRow.system_id+")";
      this.getModuleDetails();
    }
  }

  openLogDetails(ele){
    sessionStorage.setItem("logDetails", JSON.stringify(ele));
    this.router.navigate(['/home/logmodule/logdetails']);
  }

  openAlertModule(ele){
    this.alertData = {uid:ele.uid, log_grok:ele.log_grok, log_table_name: ele.log_table_name, module_code:'LOG', module_name: ele.module_name};
    this.openAlertDialog();
  }

  openAlertDialog(): void {
    const dialogRef = this.dialog.open(AlertDialog, {
      width: '60%',
      data: {uid:this.alertData.uid, log_grok:this.alertData.log_grok, log_table_name: this.alertData.log_table_name, module_code:'LOG', module_name: this.alertData.module_name}
    });
    dialogRef.afterClosed().subscribe(result => {
        this.getModuleDetails();
    });
  }

  goBack() {
    sessionStorage.removeItem("logmodule");
    this.router.navigate(['home/systemdetails']);
  }

  getModuleIcon(moduleName: string) {
    this.modIcon = this.reusable.getIconName(moduleName);
  }

  openModuleEdit(row){
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      this.modEditParam = {uid:row.uid, modName:row.module_name, modDesc: row.description, modCode: 'log'};
      this.openModuleEditDialog();
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can Edit Module Attributes","info");
    }
  }

  openModuleEditDialog(): void {
    const dialogRef = this.dialog.open(moduleEditDialog, {
      width: '40%',
      data: {uid:this.modEditParam.uid, modName:this.modEditParam.modName, modDesc: this.modEditParam.modDesc, modCode: this.modEditParam.modCode}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined)
        this.updLocalVariables(result);
    });
  }
  updLocalVariables(element){
    this.modColl.data.map(row => {
      row.module_name = element.modName;
      row.description = element.modDesc;
    });
  }
  async drawChart(element){
    let uidData = {module_code: this.modCode, module_name:element.module_name, uid: element.uid,chartCnt:null};
      sessionStorage.setItem("drawChartUid",JSON.stringify(uidData));
      sessionStorage.removeItem("dashSelVal");
      this.router.navigate(['/home/dashboard']);
  }
  public async deleteMMCard(ele) {
    let modCode = this.modCode;
    let module = ele.module_type == undefined? modCode : ele.module_type;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'));
    let usrDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (entItem.e_id == 0 || entItem.e_id == null || usrDet.unq_id == entItem.owner_id){
      let confirmMessage = confirm("Please confirm the deletion of Module - "+module+" :: "+ele.module_name+" ("+ele.uid+")");
      if(confirmMessage) {
        this.isLoading = true;
        let entDet = JSON.parse(sessionStorage.getItem('entItem'));
        let reqData = {uid : +ele.uid, moduleCode: modCode, entId : entDet.e_id == 0 ? null : entDet.e_id };
        let res = await this.authService.deleteModule(reqData);
        this.isLoading = false;
        if (res.success){
          this.reusable.openAlertMsg(res.message, "info");
          this.getModuleDetails();
        } else {
          this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
        }
      }
    } else {
      this.reusable.openAlertMsg("Sorry, Only owner can delete this card","error");
    }
  }

  async getModuleDetails() {
    this.modColl = new MatTableDataSource([]);
    let selRow = this.modDetails['selRow'];
    let param = {
      modCode: this.modCode,
      systemId: selRow.system_id
    };
    let logCardColl = [];
    let mmCardColl = await this.authService.getLogCard(param);
    if (mmCardColl.success){
      mmCardColl.result.map((row,ix) => {
        let param = {uid: row.uid, modCode: this.modCode, ownerId: this.entItem.owner_id};
        logCardColl[ix] = this.authService.getGrokPatternByUid(param);
      });
      forkJoin(logCardColl).subscribe(results => {
        mmCardColl.result.map((card,ix) => {
          if (results[ix]['success']){
            let grokDet = results[ix]['result'];
            grokDet.map((grok,iy) =>{
              this.logModuleColl[iy] = JSON.parse(JSON.stringify(card)); //to ensure reference is removed
              this.logModuleColl[iy]['log_grok'] = grok.log_grok;
              this.logModuleColl[iy]['log_table_name'] = grok.log_table_name;
            });
          } else {
            card['log_grok'] = null;
            card['log_table_name'] = null;
            this.logModuleColl[ix] = card;
          }
        });
        this.getAlertsForEachGrok();
        this.getPolicyCntByUid();
      })
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(mmCardColl),"error");
    }
  }

  async getPolicyCntByUid(){
    let policyCnt=[];
    this.logModuleColl.map((grok,ix)=>{
      let param = {uid: grok.uid, table_name:'so_sla_log', log_grok:grok.log_grok};
      policyCnt[ix] = this.authService.getPolicyCntByUid(param);
    })
    forkJoin(policyCnt).subscribe(results => {
      results.map((result,ix)=>{
        this.logModuleColl[ix]['policy_cnt'] = result['result[0]']['count'];
      })
    }, err=>{
      console.log("err from getPolicyCntByUid - log module component", err);
    })
  }

  async getAlertsForEachGrok(){
    //this.modColl = new MatTableDataSource([]);
    let ownerId = this.entItem.owner_id;
    let param = {uid:[], ownerId: ownerId, modCode:this.modCode};
    this.logModuleColl.map(row => {
      if (!param['uid'].includes(Number(row.uid))){
        param['uid'].push(Number(row.uid));
      }
    });
    let result = await this.authService.getLogAlertsByUid(param);
    if (result.success){
      result.result.map(row => {
        let findUid = this.logModuleColl.find(x => x.log_grok.toLowerCase() == row.log_grok_name);
        if (findUid != undefined) {
          findUid['critical'] = row.critical;
          findUid['warning'] = row.warning;
        }
      });
    } else {
      this.authService.invalidSession(result);
    }
    this.modColl = new MatTableDataSource(this.logModuleColl);
    this.modColl.sort = this.tblSort;
    this.modColl.data.map((row, ix) => {
      let userId = this.entItem.ownerId == null ? this.userDet.unq_id : this.entItem.ownerId;
      let userOrUid = this.modCode == 'network' ? userId : row.uid;
      let param = { tableName: row.log_table_name+userOrUid, modCode : this.modCode, uid : row.uid};
      this.getAlertsCntByTable(row, param);
    });
  }

  async getAlertsCntByTable(row, param){
    let result = await this.authService.getAlertsCntByTable(param).toPromise();
    if (result.success)
      row['count']= result.result[0].count;
    else 
      row['count'] = "-";
  }
}


/* Alert Dialog for Log & Network */
@Component({
  selector: 'alert-log',
  templateUrl: 'alert-log.html',
  styleUrls: ['../visualizer/visualizer.component.css']
})

export class AlertDialog implements OnInit {
  form: FormGroup;
  title: string;
  isLoading: boolean;
  dispAlertGrid: boolean = true;
  alertPolicyCollValue = new MatTableDataSource([]);
  alertPolicyCollPattern = new MatTableDataSource([]);
  policyName: string; policyDesc: string; warnThreshold: number; criticalThreshold: number; minBreachCnt: number; 
  isAboveThreshold: boolean = true;
  tableName: string;
  dispPolicyValColumns = ['delete','edit','policy_name','log_grok_name','grok_column','critical_threshold_value','warning_threshold_value','min_breach_count'];
  dispPolicyPatternCol = ['delete','edit','policy_name','log_grok_name','breach_pattern','breached_severity','min_breach_count']
  @ViewChild(MatSort) sort: MatSort;
  selectedIdx: number = 0;
  slaId: any;
  grokColumn: any;
  grokColColl = [];
  breachPatern: any;
  breachSeverity: any;
  theme:any;
  isContains: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<AlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ClassAlertData,
    private authService: AuthenticationService,
    private reusable: ReusableComponent,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(){
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    })
    this.title = "Configure Alert for "+this.data.module_code+" "+this.data.module_name+ "("+ this.data.uid +")";
    this.policyName = this.data.uid+'::'+this.data.module_name;
    this.policyDesc = this.policyName +"_"+ this.data.log_grok;
    this.isAboveThreshold = true;
    this.manageAlert();
  }

  async removeAlert(policy){
    let conMsg = confirm("Are you sure, you like to remove this policy?");
    if (!conMsg){
      return;
    }
    let qryData = {sla_id: policy.sla_id};
    let result = await this.authService.deleteLogAlert(qryData);
    if (result.success){
      this.reusable.openAlertMsg(result.message,"info");
      this.manageAlert();
    } else {
      this.authService.invalidSession(result);
    }
  }

  editAlert(alert, tabIdx){
    if (tabIdx == 0){
      this.policyName = alert.policy_name;
      this.policyDesc = alert.sla_description;
      this.slaId = alert.sla_id;
      this.breachPatern = alert.breach_pattern;
      this.breachSeverity = alert.breached_severity;
      this.minBreachCnt = alert.min_breach_count;
      this.isContains = alert.is_contains;
      this.openFormPattern();
    } else {
      this.policyName = alert.policy_name;
      this.policyDesc = alert.sla_description;
      this.slaId = alert.sla_id;
      this.warnThreshold = alert.warning_threshold_value;
      this.criticalThreshold = alert.critical_threshold_value;
      this.minBreachCnt = alert.min_breach_count;
      this.isAboveThreshold = alert.is_above_threshold;
      this.grokColumn = alert.grok_column;
      this.getNumericColumnForTable();
      this.openFormValue();
    }
  }

  createPolicy(){
    this.clearPolicyData();
    this.policyName = this.data.uid+'::'+this.data.module_name;
    this.policyDesc = this.policyName +"_"+ this.data.log_grok;
    if (this.selectedIdx == 0){
      this.openFormPattern();
      this.title = "Add Pattern Based "+this.data.module_code+" Policy for "+this.data.module_name+ "("+ this.data.uid +")";
    } else {
      this.title = "Add Value Based "+this.data.module_code+" Policy for "+this.data.module_name+ "("+ this.data.uid +")";
      this.getNumericColumnForTable();
      this.openFormValue();
    }
  }

  openFormPattern(){
    this.dispAlertGrid = false;
    this.form = this.formBuilder.group({
      PolicyName:[this.policyName, Validators.compose([
        Validators.required,
      ])],
      PolicyDesc:[this.policyDesc, Validators.compose([
        Validators.required,
      ])],
      BreachPattern:[this.breachPatern, Validators.compose([
        Validators.required,
      ])],
      BreachSeverity:[this.breachSeverity,Validators.compose([
        Validators.required,
      ])],
      MinBreachCount:[this.minBreachCnt,Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      IsContains:[this.isContains]
    });	
  }

  openFormValue(){
    this.dispAlertGrid = false;
    this.form = this.formBuilder.group({
      PolicyName:[this.policyName, Validators.compose([
        Validators.required,
      ])],
      PolicyDesc:[this.policyDesc, Validators.compose([
        Validators.required,
      ])],
      GrokColumn:[this.grokColumn, Validators.compose([
        Validators.required,
      ])],
      WarnThreshold:[this.warnThreshold,Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      CriticalThreshold:[this.criticalThreshold,Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      MinBreachCount:[this.minBreachCnt,Validators.compose([
        Validators.required,
        Validators.min(1),
      ])],
      IsAboveThreshold:[this.isAboveThreshold],
    });	
  }

  getErrorMessage(control) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    msg += control.hasError('min') ? 'Must be a positive integer' :'';
    return msg;
  }

  onClose(){
    if (!this.dispAlertGrid){
      this.clearPolicyData();
      this.dispAlertGrid = true;
    } else {
      this.dialogRef.close()
    }
  }

  onTabChange(tabIdx){
    this.selectedIdx = tabIdx;
  }

  async manageAlert(){
    this.alertPolicyCollValue.data = [];
    this.alertPolicyCollPattern.data = [];
    this.dispAlertGrid = true;
    this.tableName = "so_sla_log";
    this.isLoading = true;    
    let alertData = {
      uid: this.data.uid,
      source: this.tableName   
    };
    let result = await this.authService.getSlaPoliciesByMetric(alertData);
    this.isLoading = false;    
    if (result.success) {
      this.alertPolicyCollValue = result.result.filter(x=> x.breach_pattern == null);
      this.alertPolicyCollPattern = result.result.filter(x=> x.breach_pattern != null);
      this.alertPolicyCollValue.sort = this.sort;
      this.alertPolicyCollPattern.sort = this.sort;
    } else {
      this.alertPolicyCollPattern = new MatTableDataSource([]);
      this.alertPolicyCollValue = new MatTableDataSource([]);
      this.reusable.openAlertMsg(this.authService.invalidSession(result),'error');
    }
  }

  async alertPolicySubmit(){
    this.isLoading=true;
    let entItem = JSON.parse(sessionStorage.getItem('entItem'))
    let eId = entItem != undefined && entItem.e_id != undefined && entItem.e_id !=0 ? entItem.e_id : null
    let policyData = {
      sla_id: this.slaId == undefined ? null : this.slaId,
      sla_name: this.form.get('PolicyName').value,
      sla_description: this.form.get('PolicyDesc').value,
      server_details:"",
      server_details_type:"",
      e_id: eId,
      sla_type: "Alert",
      log_grok: this.data.log_grok,
      breached_severity: this.selectedIdx == 1 ? null : this.form.get("BreachSeverity").value,
      grok_column: this.selectedIdx == 0 ? null : this.form.get("GrokColumn").value,
      is_contains: this.selectedIdx == 1 ? true : this.form.get("IsContains").value,
      log_table_name:this.data.log_table_name,
      breach_pattern: this.selectedIdx == 1 ? null : this.form.get("BreachPattern").value,
      uid: this.data.uid,
      warn_threshold_value: this.selectedIdx == 0 ? null : this.form.get('WarnThreshold').value,
      critical_threshold_value: this.selectedIdx == 0 ? null : this.form.get('CriticalThreshold').value,
      is_above_threshold: this.selectedIdx == 0 ? null : this.form.get("IsAboveThreshold").value,
      min_breach_cnt: this.form.get('MinBreachCount').value,
      table_name: this.tableName
    };
    if (this.slaId == null){
      let res = await this.authService.insSLA(policyData);
      this.isLoading=false;
      if (res.success){
        this.reusable.openAlertMsg(res.message, "info");
        this.dispAlertGrid = true;
        this.manageAlert();
        this.form.reset();
        this.clearPolicyData();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
      }
    } else {
      let res = await this.authService.updSLA(policyData)
      this.isLoading=false;
      if (res.success){
        this.reusable.openAlertMsg(res.message, "info");
        this.dispAlertGrid = true;
        this.manageAlert();
        this.clearPolicyData();
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(res),"error");
      }
    }
  }

  clearPolicyData(){
    this.slaId = undefined;
    this.policyName = undefined;
    this.policyDesc = undefined;
    this.isAboveThreshold = undefined;
    this.criticalThreshold = undefined;
    this.warnThreshold = undefined;
    this.minBreachCnt = undefined;
    this.breachPatern = undefined;
    this.breachSeverity = undefined;
  }

  async getNumericColumnForTable(){
    let tableName;
    if (this.data.log_table_name.startsWith('log')){
      tableName = this.data.log_table_name+this.data.uid;
    } else if (this.data.log_table_name.startsWith('network')) {
      tableName = this.data.log_table_name+JSON.parse(sessionStorage.getItem('apdUserDet'))['unq_id'];
    } else {
      this.reusable.openAlertMsg(this.data.log_table_name +" not found", "error");
      return;
    }
    let param = {table: tableName};
    let result = await this.authService.getNumericColumnForTable(param);
    if (result.success){
      this.grokColColl = result.result;
      if (result.result.length == 1){
        this.form.get("GrokColumn").setValue(result.result[0].column_name);
      }
    } else {
      this.authService.invalidSession(result);
    }
  }
}

/* Log Details Page */
@Component({
  selector: 'log-details',
  templateUrl: 'log-details.html',
  styleUrls: ['../visualizer/visualizer.component.css']
})

export class LogDetailsComponent implements OnInit {
  returnUrl;
  parentRoute;
  titleIcon;
  title
  theme: string;
  screenParam: any;
  stEndDate:any;
  callFromSubscribe = false;
  dashSelVal: string;
  logDetails: any;
  qryResultSet = new MatTableDataSource([]);
  totalRows:number;
  @ViewChild(MatSort, { static: true }) tblSort: MatSort;
  tableFilter:string;
  //scroll variables
  offset = 0; 
  limit = 100;
  isFetched = false;
  lastScrolled = 0;
  @ViewChild(CdkScrollable, { static: true }) virtualScroll: CdkScrollable;
  setIntervalId;
  dispColumns = [];
  dataTypeColl = [];
  
  
  handleScroll = (scrolled: boolean) => {
    if (scrolled) { this.offset += this.limit;}
    scrolled && this.isFetched? this.getLogDetails(this.offset, this.limit) : _noop();
  }
  isLoading: boolean;

  hasMore(){
    return this.isFetched;
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
		private reusable: ReusableComponent,
  ) { }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
    this.authService.dispOnRequest.next(false);
    this.authService.dispMyChart.next(false);
    this.authService.dispEnt.next(true);
  }

  ngOnInit(){
    this.authService.dispOnRequest.next(true);
    this.authService.dispMyChart.next(false);
    this.authService.dispEnt.next(false);
    this.returnUrl = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute",'/home/logmodule/logdetails')
    this.logDetails = JSON.parse(sessionStorage.getItem('logDetails'));
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].includes('light') ? 'light' : 'dark';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    this.authService.dateEpoch.subscribe(dtColl => {
      this.stEndDate = dtColl;
      if (!this.callFromSubscribe){
        this.callFromSubscribe = true;
        this.getLogDetails();
      }
    });

    if (this.logDetails){
      this.titleIcon = "assignment";
      this.title = 'Log Details of ' +this.logDetails.module_name+ '-'+this.logDetails.log_grok+'('+this.logDetails.uid+')';
    } else {
      this.goBack()
    }
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      if(this.hasMore())
        this.handleScroll(true);
    });
    this.setIntervalId = setInterval(()=>{this.cd.detectChanges();},2000);
    setTimeout(()=>{
      this.authService.toggleMenu.next(false);
    },5000)
  }

  async getLogDetailsRowCount(param){
    let result = await this.authService.getLogDetailsRowCount(param);
    if(result.success){
      this.totalRows = result.result[0].count;
    } else {

    }
  }

  async getLogDetails(offset?:number, limit?:number){
    let entItem = JSON.parse(sessionStorage.getItem("entItem"));
    let apdUserDet = JSON.parse(sessionStorage.getItem("apdUserDet"));
    this.isLoading = true;
    let reset = false;
    if (offset == undefined){offset = this.offset = 0; reset = true; } 
    if (limit == undefined) limit = 100;
    let param = {
      startDate: this.stEndDate.stDate,
      endDate: this.stEndDate.endDate,
      log_table_name: this.logDetails.log_table_name,
      uid: this.logDetails.uid,
      offset: offset,
      limit: limit,
      user_id: entItem.owner_id == null ? apdUserDet.unq_id : entItem.owner_id,
      grok: this.logDetails.log_grok
    }
    let result = await this.authService.getLogDetails(param);
    this.isLoading = false;
    if (result.success && result.rowCount > 0) {
      this.isFetched = true;
      if (this.qryResultSet.data.length == 0 || reset){
        this.qryResultSet = new MatTableDataSource(result.result);
        this.getLogDetailsRowCount(param);
        this.setDispColumnType(this.logDetails.log_table_name);
      } else {
        let newData = this.qryResultSet.data.concat(result.result);
        this.qryResultSet = new MatTableDataSource(newData);
      }
      if (this.tableFilter != undefined && this.tableFilter.length>0){
        this.applyFilter(this.tableFilter);
      }
      this.qryResultSet.sort = this.tblSort;
    } else if (result.rowCount == 0){
      if (reset){
        this.qryResultSet = new MatTableDataSource([]);
      }
      this.isFetched = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    setTimeout(()=>{this.callFromSubscribe = false;},1000);
  }

  setDispColumnType(logTableName){
    if (logTableName == "log_syslog_"){
      this.dispColumns = ['id', 'appedo_received_on', 'os', 'source', 'program', 'priority', 'pid','facility', 'logmessage'];
      this.dataTypeColl =['int', 'timestamp', 'text','text','text','text','text','text','text'];
    } else if (logTableName == "log_windows_event_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'level', 'event_action', 'message'];
      this.dataTypeColl =['int', 'timestamp', 'text','text','text'];
    } else if (logTableName == "log_iis_access_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'url_path', 'port', 'query', 'source_address', 'response_status_code', 'duration', 'response_body_bytes'];
      this.dataTypeColl =['int', 'timestamp', 'text','int','text','text','int','numeric','int'];
    } else if (logTableName == "log_iis_error_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'remote_ip', 'remote_port', 'queue_name', 'reason_phrase'];
      this.dataTypeColl =['int', 'timestamp', 'text','int','text','text'];
    } else if (logTableName == "network_http_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'client_ip', 'event_category', 'event_duration_ms', 'url_path', 'url_query', 'url_port', 'http_res_status', 'http_res_status_code', 'http_res_bytes'];
      this.dataTypeColl =['int', 'timestamp', 'text','text','numeric','text','text','int','text','int', 'int'];
    } else if (logTableName == "network_icmp_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'client_ip', 'event_category', 'event_duration_ms', 'network_type', 'icmp_version', 'status', 'req_msg', 'res_msg', 'res_code', 'network_direction'];
      this.dataTypeColl =['int', 'timestamp', 'text','text','numeric','text','text','text','text','text', 'int','text'];
    } else if (logTableName == "log_custom_log_v7_"){
      this.dispColumns = ['uid', 'appedo_received_on', 'file_path', 'log_message' ];
      this.dataTypeColl =['int', 'timestamp', 'text','text'];
    } else if (logTableName == "log_avm_"){
      this.dispColumns = ['avm_test_id', 'appedo_received_on', 'country', 'city', 'success', 'status_text','status_code','resp_time_ms','bandwidth','content_length','response','agent_tested_on' ];
      this.dataTypeColl =['int', 'timestamp', 'text','text','bool','text','int','numeric','numeric','int','text','timestamp'];
    } else if (logTableName == "avm_test_run_details"){
      this.dispColumns = ['avm_test_id', 'testname', 'testurl', 'location', 'status_text','status_code','resp_time_ms','agent_tested_on'];
      this.dataTypeColl =['int','text','text', 'text','text','int','numeric','timestamp'];
    } else if (logTableName == "so_log_threshold_breach_"){
      this.dispColumns = ['appedo_received_on', 'breached_severity', 'breach_pattern','received_message', 'received_response_code', 'grok_column', 'is_above', 'critical', 'warning','received_value'];
      this.dataTypeColl =['timestamp','text','text', 'text','text','text','bool','numeric','numeric', 'numeric'];
    } else if (logTableName == "so_threshold_breach_"){
      this.dispColumns = ['received_on','metric_id','metric_name','process_name','is_above','critical','warning','received_value'];
      this.dataTypeColl =['timestamp','int','text','text', 'bool','numeric','numeric', 'numeric'];
    }
  }

  goBack(){
    sessionStorage.removeItem('logDetails');
    this.authService.dispOnRequest.next(false);
    this.router.navigate([this.returnUrl]);
  }
  applyFilter(filterValue: string) {
    this.tableFilter = filterValue;
    this.qryResultSet.filter = filterValue.trim().toLowerCase();
  }
}
