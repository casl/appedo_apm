import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormControlName } from '@angular/forms';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  isLoading = false;
  userId;  oldPassword; newPassword
  licenseType;  dataSize; dataRetentionDays; checkEmail = true;
  returnUrl;
  form: FormGroup;
  hide=true;
  modPassword;

  createForm() {
		this.form = this.formBuilder.group({
			oldPassword:[this.oldPassword,Validators.compose([
				Validators.required,
				Validators.minLength(1),
				Validators.maxLength(20)
			])],
			newPassword:[this.newPassword,Validators.compose([
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(20)
			])],
			confirm:['',Validators.required],
		},
			{validator: this.matchingPasswords('newPassword', 'confirm')}
		);	
	}

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private formBuilder: FormBuilder) {
      this.createForm();
  }

  ngOnInit() {
    this.returnUrl = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/cp");
    this.authService.dispOnRequest.next(false);
  }

  matchingPasswords(pass,conf){
    return (group: FormGroup) => {
      if(group.controls[pass].value === group.controls[conf].value){
        return null;
      } else {
        return {'matchingPasswords' : true};
      }
    }
  }

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='oldPassword') {msg += (control.errors.minlength || control.errors.maxlength)? 'Must be between 1 & 20 char length': '';}
    if (controlName =='newPassword') {msg += (control.errors.minlength || control.errors.maxlength)? 'Must be between 5 & 20 char length': '';}
    return msg;
	}

  onClickCancel(){
    if (this.returnUrl != undefined && this.returnUrl != '/home/cp')
      this.router.navigate([this.returnUrl]);
    else
      this.router.navigate(['/home']);
  }

  async chgPassword() {
    if (this.form.valid){
      this.isLoading = true;
      const chgPassword = {
        oldPassword: this.form.get('oldPassword').value,
        newPassword: this.form.get('newPassword').value,
      };
      const pass = {data:this.authService.encrypt(JSON.stringify(chgPassword))};
      let data = await this.authService.chgPassword(pass);
      this.isLoading = false;
      if (data.success){
          this.reusable.openAlertMsg(data.message,"info");
          this.authService.logout();
          this.router.navigate(['/login']);
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(data),"error");      } 
    } else {
      this.reusable.openAlertMsg("New Password and Confirm Password must be same","error");
    }
  }
}
