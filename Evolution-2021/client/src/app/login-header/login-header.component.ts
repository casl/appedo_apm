import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Title }  from '@angular/platform-browser';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { find as _find } from 'lodash';

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class LoginHeaderComponent {
  isLoading = false;
  showHeader = true;
  showProfile = false;
  userName:String ;
  cusName:string = environment.customer;
  cusLogo:string = environment.customer_logo;
  entSelVal:String;  entCollection=[]; myDashSelVal; myDashColl=[];
  appTitle:string = environment.browser_title.substr(0,1).toUpperCase()+environment.browser_title.substr(1);
  userDet; entObj;
  dispMenu = true;
  dispCalendar = false; dispOnRequest = false; dispEnt = true; dispMyChart = false;
  maxDateTime = new Date().toISOString(); stDateTime; 
  endDateTime = new Date();
  sliderCurValue = 1;
  sliderSetValue = 1;
  appliedSlider = false;
  arr = [1*60*60*1000, 3*60*60*1000-(60*1000), 6*60*60*1000-(60*1000), 12*60*60*1000-(60*1000), 1*24*60*60*1000-(60*1000),7*24*60*60*1000-(60*1000),15*24*60*60*1000-(60*1000)];
  dispArr = ['1h', '3h', '6h', '12h', '1d', '7d', '15d'];

  themeColl=[
    {themeClass:"darkbrowntheme",themeName:"Dark Brown", theme:"dark"},
    {themeClass:"lightbrowntheme",themeName:"Light Brown", theme:"light"},
    {themeClass:"darkindigotheme",themeName:"Dark Indigo", theme:"dark"},
    {themeClass:"lightindigotheme",themeName:"Light Indigo", theme:"light"}
  ];

  selTheme={};

  constructor(
		private titleService: Title, 
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
  ) { }

  ngOnInit(){
    this.userDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    this.entObj = JSON.parse(sessionStorage.getItem('entItem'));
    this.selTheme = JSON.parse(sessionStorage.getItem('theme'));
    this.stDateTime = new Date(this.endDateTime.getTime()-this.arr[0]);
    if (this.selTheme == undefined){
      this.selTheme = {themeClass:"lightindigotheme",themeName:"Light Indigo", theme:"light"};
      sessionStorage.setItem('theme', JSON.stringify(this.selTheme));
    } else {
      this.authService.themeChange.next(this.selTheme);
    }
    this.authService.dispOnRequest.subscribe(val =>{
      this.dispOnRequest = val; //used for showing slider && myDashboard on request from other modules. Dashboard module will send this req.
      if (val){ 
        this.getDashboard(); 
      }
    });
    this.authService.newMyChart.subscribe(val => {
      if(val == true){
        this.getDashboard();
      }
    });
    this.authService.entChange.subscribe(res => {
      if (res){
        this.header(res);
      }
    });
    this.authService.dispEnt.subscribe(res => {
      this.dispEnt = res;
    })
    this.authService.dispMyChart.subscribe(res => {
      this.dispMyChart = res;
    })
    this.header();
  }

  async getDashboard() {
    this.isLoading = true;
    let dash = await this.authService.getDashboard();
    this.isLoading = false;
    if (dash.success) {
      this.myDashColl = dash.result;
      this.myDashSelVal = sessionStorage.getItem('dashSelVal');
      let isAvl = _find(this.myDashColl,{mc_name:this.myDashSelVal});
      if (isAvl == undefined) {
        this.myDashSelVal = "--my dashboard--";
        sessionStorage.setItem("dashSelVal", this.myDashSelVal);
      } else {
        this.onMyDashChange(this.myDashSelVal);
      }
    } else {
      this.myDashSelVal = "--my dashboard--";
      // this.onMyDashChange(this.myDashSelVal);
      this.authService.invalidSession(dash, false);
    }
  }

  onMyDashChange(selVal){
    this.myDashSelVal = selVal;
    sessionStorage.setItem("dashSelVal", this.myDashSelVal);
    this.authService.myDashSelVal.next(this.myDashSelVal);
  }

  formatLabel(value: number | null) {
    let arr = ['1h', '3h', '6h', '12h', '1d', '7d', '15d'];
    return arr[value-1];
  }
  
  changeSlider(value){
    let endDate = new Date().getTime();
    this.endDateTime = new Date(endDate);
    this.stDateTime = new Date(endDate-this.arr[value-1]);
    this.sliderCurValue = value;
  }

  applyDtControls(dispCalendar){
    if (!dispCalendar){
      this.changeSlider(this.sliderCurValue);
    };
    if (this.changeDateTime()){
      this.sliderSetValue = this.sliderCurValue;
      this.authService.dateEpoch.next({stDate: new Date(this.stDateTime).getTime(), endDate: new Date(this.endDateTime).getTime()});
    }
  }

  changeDateTime(){
    let stDateTime;
    let endDateTime;
    if (this.endDateTime == undefined){
      this.reusable.openAlertMsg("Enddate cannot be empty","error");
      this.endDateTime = new Date();
      return false;
    } 
    else {
      endDateTime = new Date(this.endDateTime).getTime();
      if (endDateTime < (new Date().getTime() - 75*24*60*60*1000)) {
        this.endDateTime = new Date(new Date().getTime() - 75*24*60*60*1000); 
        endDateTime = new Date(this.endDateTime).getTime();
        this.reusable.openAlertMsg("End date cannot be less than 75 days", 'error'); 
      } else if (endDateTime > new Date().getTime()){
        this.endDateTime = new Date(); 
        endDateTime = new Date(this.endDateTime).getTime();
        this.reusable.openAlertMsg("End date cannot be in future.", 'error'); 
      }
      if (this.stDateTime == undefined) {
        this.stDateTime = new Date(endDateTime-1*60*60*1000); 
        return false;
      } else {
        stDateTime = new Date(this.stDateTime).getTime();
        if (stDateTime > endDateTime) {
          this.stDateTime = new Date(endDateTime-1*60*60*1000); 
          this.reusable.openAlertMsg("Start date must not be greater than end date time","error"); 
          return false;
        } else if (stDateTime < endDateTime - 15*24*60*60*1000) {
          this.reusable.openAlertMsg("Start date must be within 15 days from end date time","error");
          this.stDateTime = new Date(endDateTime-15*24*60*60*1000);
          return false;
        } else if (endDateTime-stDateTime < 1*60*60*1000) {
          this.stDateTime = new Date(endDateTime-1*60*60*1000);
          this.reusable.openAlertMsg("Start date must be 1 hour earlier than end date time","error");
          return false;
        } else {
          return true;
        }
      }
    }
  }

  toggleMenu(mode?:string){
    if (mode != "dashboard") this.dispMenu = !this.dispMenu;
    else {
      this.dispMenu = false;
      mode= undefined;
    } 
    this.authService.toggleMenu.next(this.dispMenu);
  }

  public setTitle(newTitle: any) {
		this.titleService.setTitle( newTitle );
	}

  changeValue(event){
    sessionStorage.setItem('theme', JSON.stringify(event));
    this.selTheme = event;
    this.authService.themeChange.next(event);
  }

  onEntChange(entItem){
    if (entItem) {
      this.entSelVal = entItem.e_name;
      this.entObj = entItem;
      this.authService.setItem('entItem', JSON.stringify(entItem));
    }
  }

  async header(mode?:boolean) {
    if (this.authService.loggedIn()){
      this.showProfile = true;
      this.userName = sessionStorage.getItem('apdUser');
      this.entCollection = JSON.parse(sessionStorage.getItem('entCollection'));
      if (this.entCollection && !mode) {
        this.entObj = JSON.parse(sessionStorage.getItem('entItem'));
        this.entSelVal = JSON.parse(sessionStorage.getItem('entItem')).e_name;
      } else {
        let entColl = await this.authService.getEnterprise();
        if (entColl.success){
          this.entCollection = entColl.result;
        } else {
          this.entCollection = [];
          this.authService.invalidSession(entColl, false);
        }
        this.entCollection.push({e_id:0, e_name: "Select Enterprise", owner_id:null, "is_owner":true});
        this.entSelVal = "Select Enterprise";
        this.authService.setItem('entItem', JSON.stringify({e_id:0, e_name: "Select Enterprise", owner_id:null, "is_owner":true}));
        this.entObj = {e_id:0, e_name: "Select Enterprise", owner_id:null};
        sessionStorage.setItem('entCollection', JSON.stringify(this.entCollection));
      }
    }
  }

  //downloadAgent
  downloadAgent(type) {
    let url;
    let selEntObj = JSON.parse(sessionStorage.getItem('entItem'));
    if (!selEntObj.is_owner) {
      return;
    }

    if (type == 'AVM')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&guid=&encryptedUserId='+this.userDet.encryptedUserId+'&location=country##city##';
    else if (type == 'LINUX_MONITOR_AGENT_INSTALLER')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);
    else if (type == 'WINDOWS_MONITOR_AGENT_INSTALLER')
      url = environment.domain+'/downloader/downloadAgent?type='+type+'&encryptedUserId='+this.userDet.encryptedUserId+'&entId='+(selEntObj.is_owner ? selEntObj.e_id : 0);
    if (url != undefined || url.length>0) 
      window.location.href=url;
  }

  async logout() {
    let userDet=JSON.parse(sessionStorage.getItem('apdUserDet'));
    if (userDet.id > 0) {
      let data={id: userDet.id};
      let result = await this.authService.logoutUser(data);
      if (!result.success){
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
    }
  
    this.authService.logout();
    this.authService.dateEpoch.next({stDate: new Date().getTime()-60*60*1000, endDate: new Date().getTime()});
    this.router.navigate(['/login']);
  }

  hideHeader() {
    this.showHeader = this.showHeader ? false : true ;
  }

}
