import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { LoginHeaderComponent } from './login-header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    MatDatetimepickerModule,
    MatNativeDatetimeModule,
  ],
  declarations: [LoginHeaderComponent],
  exports: [LoginHeaderComponent]
})
export class LoginHeaderModule { }

