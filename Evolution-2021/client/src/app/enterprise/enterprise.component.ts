import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormControlName } from '@angular/forms';
import { AuthenticationService } from '../_services/index';
import { ReusableComponent } from '../reusable/reusable.component';
import { MatPaginator } from '@angular/material/paginator';
import { Sort, MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { throttle as _throttle, noop as _noop } from 'lodash';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/scrolling';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.scss'],
})
export class EnterpriseComponent implements OnInit {
  isLoading = false;
  form: FormGroup;
  lastVisitedURL; displayGrid=true;  eId; dispUsrMapGrid = false;
  usrMapScreenTitle; selEntForUM; usrMapForm = false; isExistingUser=false; existingUserDet; ownerDet;
  enterpriseColl = new MatTableDataSource([]);
  userMapColl = new MatTableDataSource([]);
  screenParam:any;
  scrolled:boolean = false;

  //scroll variables
  offset = {eg:0, umg:0}; //eg = enterprise grid, umg - user map grid
  limit = {eg:10, umg:10};
  lastScrolled = {eg:0, umg:0};
  isFetched = {eg:false, umg:false};
  @ViewChild(CdkScrollable) virtualScroll: CdkScrollable;
  
  @ViewChild(MatSort) sort: MatSort;
  theme:any;

  setIntervalId;

  handleScroll = (scrolled: boolean, origin) => {
    if (scrolled) {
      if(origin=="eg") this.offset['eg'] += this.limit['eg']; 
      if(origin=="umg") this.offset['umg'] += this.limit['umg']; 
    }
    scrolled && origin=='eg' && this.isFetched['eg']? this.getEntByUser(this.offset['eg'], this.limit['eg']) : _noop();
    scrolled && origin=='umg' && this.isFetched['umg']? this.getUsrEntMap(this.selEntForUM, this.offset['umg'], this.limit['umg']) : _noop();
  }

  hasMore(origin){
    return this.isFetched[origin];
  }

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private reusable: ReusableComponent,
    private scrollDispatcher: ScrollDispatcher,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder) {
      this.createForm();
    }

  ngOnDestroy(){
    clearInterval(this.setIntervalId);
  }

  ngOnInit() {
    //theme change observable
    this.authService.themeChange.subscribe(theme => {
      this.theme = theme['themeClass'].startsWith('dark') ? 'dark' : 'light';
    });
    this.authService.screenChange.subscribe(res => {
      this.screenParam = res;
    })
    this.lastVisitedURL = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/enterprise");
    this.ownerDet = JSON.parse(sessionStorage.getItem('apdUserDet'));
    this.getEntByUser();
    this.authService.dispOnRequest.next(false);
    this.scrollDispatcher.scrolled(500).pipe(
      filter(event => this.virtualScroll.measureScrollOffset('bottom') === 0)
    ).subscribe(ele =>{
      let id = ele['elementRef']['nativeElement'].id;
      if(this.hasMore(id))
        this.handleScroll(true, id);
    });
    this.setIntervalId = setInterval(()=>{
      this.cd.detectChanges();
    },2000);
  }

  mapUsrToEnt(){
    this.usrMapForm = true;
    this.usrMapScreenTitle = "Map user for Enterprise "+ this.selEntForUM.e_name;
    this.createFormUsrMap();
  }

  async delMapUsr(mapUsrId){
    let qryData = {id: mapUsrId};
    let result = await this.authService.delMappedUserFromEnt(qryData)
    if (result.success){
      this.reusable.openAlertMsg(result.message,'info');
      this.getUsrEntMap(this.selEntForUM);
      this.getEntByUser();
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
  }
  
  openMappedUser(ele){
    this.selEntForUM = ele;
    this.displayGrid = false;
    this.dispUsrMapGrid = true;
    if (ele.mapped_user > 0)
      this.getUsrEntMap(ele);
    else 
     this.mapUsrToEnt();
    // this.cd.detectChanges();
  }

  displayedColumnsUsrEntMap=['delete','email_id','first_name','email_verified_on'];
  async getUsrEntMap(selEnt, offset?:number, limit?:number){
    this.isLoading = true;
    // this.cd.detectChanges();
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['umg'] = 0; reset = true;}
    if (limit == undefined){ limit = 10};
    let qryData = {e_id: selEnt.e_id, offset:offset, limit:limit};
    this.usrMapScreenTitle = selEnt.e_name +' ('+selEnt.e_id+')';
    let result = await this.authService.getUsrEntMap(qryData);
    this.isLoading = false;
    // this.cd.detectChanges();
    if (result.success){
      this.isFetched['umg'] = true;
      if (this.userMapColl.data.length == 0 || reset){
        this.userMapColl = new MatTableDataSource(result.result);
      } else {
        let newData = this.userMapColl.data.concat(result.result);
        this.userMapColl = new MatTableDataSource(newData);
      }
      this.userMapColl.sort = this.sort;        
    } else if (result.rowCount == 0) {
      if (offset==0){
        this.userMapColl.data=[]; 
        this.dispUsrMapGrid=false; 
        this.displayGrid=true; 
        this.reusable.openAlertMsg(result.message,'info');
      }
      this.isFetched['umg'] = false;
    } else {
      this.userMapColl.data =[];
      this.dispUsrMapGrid = false;
      this.displayGrid = true;
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  async getEnterprise() {
    this.isLoading = true;
    // this.cd.detectChanges();
    let entCollection = [];
    let entColl = await this.authService.getEnterprise();
    this.isLoading = false;
    if (entColl.success){
      entCollection = entColl.result;
    } else {
      entCollection = [];
      this.reusable.openAlertMsg(this.authService.invalidSession(entColl),"error");
    }
    entCollection.push({e_id:0, e_name: "Select Enterprise", owner_id:null, "is_owner":true});
    this.authService.setItem('entItem', JSON.stringify({e_id:0, e_name: "Select Enterprise", owner_id:null, "is_owner":true}));
    this.authService.setItem('entCollection', JSON.stringify(entCollection));
    // this.cd.detectChanges();
  }
  
  displayedColumns=['edit','delete','e_name','description','mapped_user'];
  async getEntByUser(offset?:number, limit?:number){
    this.isLoading = true;
    let reset = false;
    if (offset == undefined){offset = 0; this.offset['eg'] = 0; reset = true; } 
    if (limit == undefined) limit = 10;
    let param = {offset:offset, limit:limit};
    let result = await this.authService.getEntByUser(param);
    this.isLoading = false;
    if (result.success && result.rowCount>0){
      this.isFetched['eg'] = true;
      this.displayGrid = !this.dispUsrMapGrid;
      if (this.enterpriseColl.data.length == 0 || reset){
        this.enterpriseColl = new MatTableDataSource(result.result);
      } else {
        let newData = this.enterpriseColl.data.concat(result.result);
        this.enterpriseColl = new MatTableDataSource(newData);
      }
      this.enterpriseColl.sort = this.sort;
    } else if (result.rowCount == 0){
      if (reset){
        this.enterpriseColl = new MatTableDataSource([]);
      }
      this.isFetched['eg'] = false;
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  async unDelEnt(entId){
    this.isLoading = true;
    // this.cd.detectChanges();
    let qryData = {e_id: entId};
    let result = await this.authService.unDelEnterprise(qryData);
    this.isLoading = false;
    if (result.success){
      this.getEntByUser();        
      this.getEnterprise();
      this.authService.entChange.next(true);
      this.reusable.openAlertMsg(result.message,'info');
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  async delEnt(entId){
    let confirmMsg = confirm("Are you sure, you want to deactivate enterprise?")
    if (!confirmMsg) return;
    this.isLoading = true;
    // this.cd.detectChanges();
    let qryData = {e_id: entId};
    let result = await this.authService.delEnterprise(qryData);
    this.isLoading = false;
    if (result.success){
      this.getEntByUser(); 
      this.getEnterprise();       
      this.reusable.openAlertMsg(result.message,'info');
      this.authService.entChange.next(true);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  editEnt(ele){
    this.createForm();
    this.form.setValue({
      entName: ele.e_name,
      entDesc: ele.description,    
    });
    this.eId = ele.e_id;
    this.displayGrid = false;
    // this.cd.detectChanges();
  }

  async addUpdateEnt(){
    if (this.form.valid){
      this.isLoading = true;
      // this.cd.detectChanges();
      let qryData = {
        e_id: this.eId == undefined? null: this.eId, 
        e_name: this.form.get('entName').value, 
        description: this.form.get('entDesc').value
      };
      let result = await this.authService.addUpdEnterprise(qryData);
      this.isLoading = false;
      if(result.success){
        this.displayGrid = true;
        this.getEntByUser(); 
        this.getEnterprise();
        this.reusable.openAlertMsg(result.message,'info');
        this.authService.entChange.next(true);
      } else {
        this.displayGrid = false;
        this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
      }
      // this.cd.detectChanges();
    } 
  }

  cancel(){
    if (this.displayGrid)
      this.router.navigate([this.lastVisitedURL]);
    else if (this.usrMapForm) {
      this.eId = null;
      this.getUsrEntMap(this.selEntForUM);
      this.usrMapForm=false;
    } else {
      this.eId = null;
      this.getEntByUser();
      this.dispUsrMapGrid = false;
      this.displayGrid = true;
    }
    // this.cd.detectChanges();
  }

  getErrorMessage(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='entName') {msg += (control.errors != undefined && (control.errors.minlength || control.errors.maxlength)) ? 'Must be between 3 & 30 char length': ''}
    if (controlName =='entDesc') {msg += (control.errors.maxlength) ? 'Max 100 char length': ''}
    if (controlName =='entName') {msg += control.hasError('validateName') ? 'AlphaNumeric,Space,_,- are allowed' :''}
    return msg;
  }
  
  createEnt(){
    this.displayGrid = false;
    this.dispUsrMapGrid = false;
    this.eId = null;
    this.createForm();
    // this.cd.detectChanges();
  }

  createForm() {
		this.form = this.formBuilder.group({
      entName:['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
        Validators.maxLength(30),
        this.validateName
      ])],
      entDesc:['', Validators.compose([
				Validators.required,
        Validators.maxLength(100),
      ])],
     },
		);	
  }

  validateName(controls){
		const reqExp = new RegExp(/^[a-zA-Z0-9 _-]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateName' : true};
		}
  }

  async addUsrMap(cntrl){
    if (this.form.get('email').value.length > 0 && this.ownerDet.email == this.form.get('email').value){
      this.reusable.openAlertMsg("Not allowed to map owner as enterprise user.",'info');
      return;
    }
    if (this.existingUserDet.user_id == null){
      this.isLoading = true;
      this.cd.detectChanges();
      const user = {
				email:this.form.get('email').value,
				first_name: this.form.get('fname').value,
				last_name: this.form.get('lname').value,
				password: 'p@ssw0rd',
				license_level: 'level0',
        mobile: this.form.get('mobile_no').value,
        telephone_code: '+91',
        operation: 'EnterpriseInviteMail',
        isNewUser: true,
        enterprise_name: this.selEntForUM.e_name,
        entOwner_emailId: this.ownerDet.email
			}; 
			const usr = {data:this.authService.encrypt(JSON.stringify(user))};
			let data = await this.authService.registerUser(usr);
      this.isLoading = false;
      if (data.success){
        let qryData = {
          email_id : this.form.get("email").value,
          e_id: this.selEntForUM.e_id,
          status: null,
          map_user_id : data.result[0].user_id,
        }
        this.mapUsr(qryData,cntrl,'new');
        this.reusable.openAlertMsg(data.message+' User Successfully Mapped.','info');
      } else {
        this.reusable.openAlertMsg(this.authService.invalidSession(data),"error");
      }
    } else {
      let qryData = {
        email_id : this.form.get("email").value,
        e_id: this.selEntForUM.e_id,
        first_name: this.form.get('fname').value,
        last_name: this.form.get('lname').value,
        mobile: this.form.get('mobile_no').value,
        telephone_code: '+91',
        status: null,
        map_user_id : this.existingUserDet.user_id,
        isNewUser: false,
        operation: 'EnterpriseInviteMail',
        enterprise_name: this.selEntForUM.e_name,
        entOwner_emailId: this.ownerDet.email
      }
      this.mapUsr(qryData,cntrl);
      // this.cd.detectChanges();
    }
  }

  async mapUsr(qryData,cntrl,mode?){
    this.isLoading = true;
    // this.cd.detectChanges();
    let result = await this.authService.mapUsrToEnt(qryData);
    this.isLoading = false;
    if(result.success){
      if (mode != 'new'){
        this.reusable.openAlertMsg(result.message,'info');
      }
      this.mapUserCallback(cntrl);
    } else {
      this.reusable.openAlertMsg(this.authService.invalidSession(result),"error");
    }
    // this.cd.detectChanges();
  }

  mapUserCallback(cntrl){
    if (cntrl=='finish') {
      this.usrMapForm = false;
      this.getUsrEntMap(this.selEntForUM);
    }
    this.form.setValue({
      email: null,
      fname: null,
      lname: null,
      mobile_no: '+'
    });
    // this.cd.detectChanges();
  }

  checkEmailAvl(){
    if (this.form.get('email').value == null){
      return;
    }
    if (this.form.get('email').value != null && this.form.get('email').value.length > 0 && this.ownerDet.email == this.form.get('email').value){
      this.reusable.openAlertMsg("Not allowed to map owner as enterprise user.",'info');
      return;
    }
    this.isLoading = true;  
    // this.cd.detectChanges();
    let emailData = {email: this.form.get("email").value};
    this.authService.checkEmailAvlInclOwner(emailData).subscribe( result => {
      this.isLoading = false;
      if (result.success) {
        if (result.userExist) {
          this.isExistingUser = true;
          this.existingUserDet = result.result[0];
          this.form.setValue({
            email: this.existingUserDet.email_id,
            fname: this.existingUserDet.first_name,
            lname: this.existingUserDet.last_name,
            mobile_no: this.existingUserDet.mobile_no
          });
          this.form.get("fname").disable();
          this.form.get("lname").disable();
          this.form.get("mobile_no").disable();
          this.reusable.openAlertMsg("User Already Exist and cannot change first name, last name and mobile number",'info');
        } else {
          this.isExistingUser = false;
          this.existingUserDet =[];
          this.form.get("fname").enable();
          this.form.get("lname").enable();
          this.form.get("mobile_no").enable();
          this.form.setValue({
            email: this.form.get("email").value,
            fname: null,
            lname: null,
            mobile_no: '+'
          });
        }
      } else {
        this.reusable.openAlertMsg(result.message,'info');
        this.cancel();
      }
    },error => {
      this.isLoading = false;
      this.reusable.openAlertMsg(error.message,'error');
    });
    // this.cd.detectChanges();
  }

  createFormUsrMap() {
		this.form = this.formBuilder.group({
			email:[null,Validators.compose([
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(50),
				Validators.email
			])],
			fname:[null,Validators.compose([
				Validators.required,
				Validators.minLength(3),
        Validators.maxLength(50),
        this.validateflname
			])],
			lname:[null,Validators.compose([
				Validators.required,
				Validators.minLength(1),
				Validators.maxLength(50),
        this.validateflname
			])],
			mobile_no:['+',Validators.compose([
				Validators.minLength(13),
				Validators.maxLength(15),
				this.validateMobile
			  ])]
     },
    );	
    // this.cd.detectChanges();
  }

  getErrorMessageUsr(control, controlName) {
    let msg ='';
    msg += control.hasError('required') ? 'You must enter a value' :'';
    if (controlName =='email') {msg += control.hasError('email') ? 'Not a valid email' :''}
    if (controlName =='fname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 3 & 50 char length': ''}
    if (controlName =='lname') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 1 & 50 char length': ''}
    if (controlName =='mobile_no') {msg += (control.errors.minlength || control.errors.maxlength) ? 'Must be between 13 & 15 length includes country code': ''}
    if (controlName =='mobile_no') {msg += !control.value.startsWith('+') ? 'Must start with +': ''}
    if (controlName =='mobile_no') {msg += control.hasError('validateMobile') ? 'Only Numeric Fields Allowed' :''}
    // this.cd.detectChanges();
    return msg;
	}

  validateflname(controls){
		const reqExp = new RegExp(/^[a-zA-Z]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateflname' : true};
		}
	}

  validateMobile(controls){
		const reqExp = new RegExp(/^[+][0-9]*$/);
		if (reqExp.test(controls.value)){
			return null;
		} else{
			return { 'validateMobile' : true};
		}
  }
}
