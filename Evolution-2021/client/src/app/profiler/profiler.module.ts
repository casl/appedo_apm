import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ProfilerRoutingModule } from './profiler-routing.module';
import { ProfilerComponent } from './profiler.component';
import { MaterialModule } from '../material'
import { FlexLayoutModule } from '@angular/flex-layout';
// import { ScrollContainerModule } from '../scroll-container/scroll-container.module';
import { ModuleDetailsModule } from '../module-details/module-details.module';

@NgModule({
  imports: [
    CommonModule,
    ProfilerRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    // ScrollContainerModule,
    ModuleDetailsModule,
  ],
  declarations: [
    ProfilerComponent
  ],
  entryComponents: [
    
  ],
})

export class ProfilerModule { }

