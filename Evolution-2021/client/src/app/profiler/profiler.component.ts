import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/index';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { filter as _filter, groupBy as _groupBy }from 'lodash';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-profiler',
  templateUrl: './profiler.component.html',
  styleUrls: ['./profiler.component.scss']
})
export class ProfilerComponent implements OnInit {

  isLoading = false;
  pageTitle:String;
  profilerData;
  profilerPage={};
  returnURL:string;
  screenChange:any;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
		private sanitizer: DomSanitizer,
    public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.returnURL = sessionStorage.getItem("currentRoute");
    sessionStorage.setItem("currentRoute","/home/profile");
    this.authService.screenChange.subscribe(res => {
      this.screenChange = res;
    });
    this.isLoading = true;
    this.pageTitle = 'Java Profiler';
    let profilerLocData = sessionStorage.getItem('profilerLocData');
    console.log(profilerLocData);
    if (profilerLocData == undefined) {
      this.isLoading = false;
      this.router.navigate(['this.returnURL']);
    } else {
      //sessionStorage.removeItem("profilerLocData");
      this.profilerData = JSON.parse(profilerLocData);
      this.getResourceURL(this.profilerData);
    }
  }

  getResourceURL(profilerData) {
    this.authService.getProfilerResources(profilerData).subscribe(res => {
      this.isLoading = false;
      if (res.success && res.result.length > 0){
        this.profilerData.urls = [];
        for (let i=0; i<res.result.length; i++) {
          var obj = res.result[i];
          
          obj.url = this.sanitizer.bypassSecurityTrustResourceUrl(obj.url);
          //obj.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://wpt.appedo.com:4000/transaction/average?agent-id=55904e86-e730-4442-b38c-349f0794c1fd');
          this.profilerData.urls.push(obj);
        }
        console.log(this.profilerData);
      } else {
        //this.alertService.error("No URLs found.");
      }
    }, error => {
      this.isLoading=false;
      console.log(error);
      //this.alertService.error("Problem while retrieving Profiler data.");
  })
  }

  ngOnDestroy(){
    sessionStorage.removeItem("profilerLocData");
  }

  goBack(){
    sessionStorage.removeItem("profilerLocData");
    this.router.navigate([this.returnURL]);
  }

}
