import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilerComponent } from './profiler.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProfilerRoutingModule { }

