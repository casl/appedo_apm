
export const environment = {
  production: true,
  customer: 'appedo',
  customer_logo: '/assets/images/appedo.svg',
  browser_title: 'appedo',
  keyEncryptDecrypt: '9r55hnl26wwmfm2397@kl$hjkazgpk^w',
  domain: '',
  dbEngineName: {psql:"pgCustom", mssql:"mssqlCustom",mysql:"mysqlCustom", oracle:"oracleCustom"}
};
