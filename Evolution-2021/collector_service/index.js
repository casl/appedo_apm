var express = require('express');
const mountRoutes = require('./routes');
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');

var app = express();
var fs = require("fs");

var whitelist = ['http://localhost', 'http://example2.com']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}
// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
// app.use(bodyParser.json());
app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));

// app.get('/products/:id', cors(corsOptions), function (req, res, next) {
//   res.json({msg: 'This is CORS-enabled for a whitelisted domain.'})
// })

// app.get('/listUsers',cors(corsOptions), function (req, res) {
//    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
//       console.log( data );
//       res.end( data );
//    });
// })

mountRoutes(app);
var httpPort = 8081;
app.listen(httpPort, () => console.log('Appedo Collector listening at port '+httpPort+'!'));

// var server = app.listen(8081, function () {
//    var host = server.address().address
//    var port = server.address().port
//    console.log("Collector Services listening at port ",port);
// })