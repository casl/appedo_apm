const services = require('./collector_services');

module.exports = (app) => {
  console.log("loading CollectorServices");
  app.use('/services', services);
}
