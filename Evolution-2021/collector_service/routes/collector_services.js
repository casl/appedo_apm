const Router = require('express-promise-router');
global.logger = require('../log');
const router = new Router();
const pgDb = require('./psqlAPM')
const DbConfig = require('./config/apd_constants');
const iisAccessQueue = new Queue();
function Queue(){var a=[],b=0;this.getLength=function(){return a.length-b};this.isEmpty=function(){return 0==a.length};this.enqueue=function(b){a.push(b)};this.dequeue=function(){if(0!=a.length){var c=a[b];2*++b>=a.length&&(a=a.slice(b),b=0);return c}};this.peek=function(){return 0<a.length?a[b]:void 0}};
const fetch = require('node-fetch');
const tableCreationQueue = new Queue();
let moduleSerivceURL;
getModuleServiceUrl();

module.exports = router;
let collPartionTable = [];
let collGuidUsrUid = [];
let missingGUID = [];
let collTypeTable ={MetricSet:'collector_', SlowQuerySet:'mssql_slowquery_', WaitStat:'mssql_wait_stat_', LockSet:'mssql_lock_data_',SlowProcSet:'mssql_slowprocedure_', SLASet:'so_threshold_breach_', iisaccess:'log_iis_access_v7_', iiserror:'log_iis_error_v7_', winEventLog:"log_windows_event_v7_", customLog:"log_custom_log_v7_", icmpLog:"network_icmp_v7_", httpLog:"network_http_v7_", psqlSlowQuerySet: "postgres_slowquery_", oracleSlowQuerySet: "oracle_slowquery_",  mysqlSlowQuerySet:"mysql_slowquery_", mysqlLockQuerySet:"mysql_lockquery_", psqlLockQuerySet:"postgres_lockquery_",oracleLockQuerySet:"oracle_lockquery_", LINUX_SYSLOG:"log_syslog_", AVM:"log_avm_", NETSTACK:"net_stack_"};
let chartVisualColl = [];
let queueObj = [];
let isIISQueueRunning = false;
let isCreateTblQueueRunning = false;
let updLastReceivedOn = {};
let updAvmLastReceivedOn = {};
let netStackTimer;
let isNetStackTimerStarted = false;
let isPtTableTimerStarted = false;
let isProcessingNetStackData = false;
let tableCreationTimer;
let isDBserviceUp = false;

async function chkPSQLDBConnection(){
  let chkDBId= setInterval(() => {
    if(isDBserviceUp) clearInterval(chkDBId);
    else {
      let chkConn = pgDb.chkPSQLDB();
      if (chkConn == "success") isDBserviceUp = true;
      else isDBserviceUp = false;
    }
  }, 10000);
}

async function getModuleServiceUrl(){
  let queryText = "SELECT value FROM appedo_config_properties WHERE property = $1";
  let queryParam = ['MODULE_SERVICES'];
  let result = await pgDb.fnDbQuery("getModuleServiceUrl", queryText, queryParam);
  if (result.success){
    moduleSerivceURL = result.rows[0].value;
  } else {
    logger.error("getModuleServiceUrl failed with message "+result.message+" will try after 10 sec.");
    setTimeout(() => {
      getModuleServiceUrl();
    }, 10000);
  }
}

function netStackTimerStart(){
  netStackTimer = setInterval(() => {
    if (!isProcessingNetStackData){
      processNetStackData();
    }
  }, 10000);
}

function netStackTimerStop(){
  clearInterval(netStackTimer);
}

function postToModuleService(body){
  fetch(moduleSerivceURL+"/AvmAlertServiceTrigger",{
    method : 'post',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' },
  })
  .then(res => {
    logger.info(res);
  })
  .catch(err => {
    logger.error("postToModuleService "+moduleSerivceURL+" ErrMsg:"+err.message);
  })
}


//to delete earlier days table references from the collPartionTable. checks once in a day only. 86400000 ms - for 24 hours 
setInterval(() => {
  try {
    let dt = new Date();
    let numMonth = dt.getMonth()+1;
    let strMonth = numMonth.toString().length == 1 ? '0'+numMonth.toString() : numMonth.toString();
    let numDt = dt.getDate()-1;
    let strDt = numDt.toString().length == 1 ? '0'+ numDt.toString() : numDt.toString();
    let strDate = dt.getFullYear()+ strMonth+ strDt;
    delete collPartionTable['str'+strDate];
    logger.info("Deleted all references of "+strDate+" from collPartionTable");
  } catch (e) {
    logger.error("SetInterval delete old array partition tables interval" ,e.message, e.stack);
  }
}, 24*60*60*1000)

// //Deleted uids are checked every 30 min and removed all the references from the collection.
// setInterval(()=>{
//   getDeletedUids();
// }, 30*60*1000)

// async function getDeletedUids(){
//   let removedTableName = '';
//   let removedUid = '';
//   let queryText = "SELECT uid FROM deleted_uids;"
//   let result = await pgDb.fnDbQuery("getDeletedUids",queryText,[]);
//   if (result.success && result.rowCount > 0){
//     let deletedUids = result.rows;
//     let dt = new Date();
//     let numMonth = dt.getMonth()+1;
//     let strMonth = numMonth.toString().length == 1 ? '0'+numMonth.toString() : numMonth.toString();
//     let numDt = dt.getDate();
//     let strDt = numDt.toString().length == 1 ? '0'+ numDt.toString() : numDt.toString();
//     let strDate = dt.getFullYear()+ strMonth+ strDt;
//     deletedUids.map( record => {
//       if (removeUid != '') removeUid += ",";
//       removedUid += record.uid;
//       delete collGuidUsrUid[record.guid];
//       let partitionTables = [];
//       collPartionTable['str'+strDate].map((tableName,ix) =>{
//         if (tableName.includes(record.uid)){
//           partitionTables.push(ix);
//           if (removedTableName != "") removedTableName += ",";
//           removedTableName += tableName;
//         }
//       });
//       partitionTables.map(idxNumber =>{
//         collPartionTable.splice(idxNumber,1);
//       });
//     });
//     let splitUids = removedUid.split(',');
//     queryText = "DELETE FROM deleted_uids WHERE uid IN [;"
//     splitUids.map((uid,ix) => {
//       if (ix != 0) queryText += ",";
//       queryText += uid;
//     });
//     queryText += "]";
//     pgDb.fnDbQuery("getDeletedUids-cleanProcessedUid",queryText,[]);
//     logger.info("Removed deleted uids "+removedUid);
//     logger.info("Removed Tables from collection "+removedTableName);
//   }
// }

//Queues are cleared if data exist. Queue is checked for every 500ms.
setInterval(() => {
  try {
    if (!isIISQueueRunning && iisAccessQueue.getLength()>0){
      isIISQueueRunning = true;
      drainDBInsQueue();
    }
    if (queueObj.length>0){
      let obj = queueObj.shift();
      chkInsChartVisual(obj.type, obj.partTblRef, obj.arrUid);
    }
  } catch (e){
    isIISQueueRunning = false;
    logger.error("Exception SetInterval db drain every 500 ms interval" ,e.message, e.stack);
  }
}, 500);

function  ptTableCreationTimerStart(){
  tableCreationTimer = setTimeout(() => {
    try{
      if(!isCreateTblQueueRunning && tableCreationQueue.getLength()>0){
        isCreateTblQueueRunning = true;
        isPtTableTimerStarted = true;
        drainCreateTableQueue();
      }
    }catch(e){
      logger.error("Exception SetInterval parent table creation every 100 ms interval" ,e.message, e.stack);
    }
  });
}

function ptTableCreationTimerStop(){
  clearInterval(tableCreationTimer);
}

//Interval is set to update last received column once in 20 sec.
setInterval(()=>{
  updLastReceivedOnColumn();
  updAvmLastReceivedOnColumn();
}, 60*1000);

function updAvmLastReceivedOnColumn(){
  try {
    let keys = Object.keys(updAvmLastReceivedOn);
    if (keys != undefined && keys.length > 0) {
      let qryText = '';
      keys.map(key => {
        let splitKey = updAvmLastReceivedOn[key].split("|");
        if (splitKey[2] == "true") {
          logger.info("AVM last updated on", key, new Date().toISOString());
          qryText += "UPDATE avm_test_master SET last_appedo_received_on='" + splitKey[0] + "', last_status_text = '"+splitKey[1]+"' WHERE avm_test_id=" + key.replace('str', '') + ";";
          updAvmLastReceivedOn[key] = new Date().toISOString()+"|"+splitKey[1]+"|false";
        }
      });
      if (qryText.length > 0) {
        iisAccessQueue.enqueue(qryText);
      }
    }
  }
  catch (e) {
    logger.error("Exception SetInterval update last received on for AVM Table status", e.message, e.stack);
  }
}

function updLastReceivedOnColumn() {
  try {
    let keys = Object.keys(updLastReceivedOn);
    if (keys != undefined && keys.length > 0) {
      let qryText = '';
      keys.map(key => {
        let splitKey = updLastReceivedOn[key].split("|");
        if (splitKey[1] == "true") {
          logger.info("Metric set last updated on", key, splitKey[0]);
          qryText += "UPDATE module_master SET last_appedo_received_on='" + splitKey[0] + "' WHERE uid=" + key.replace('str', '') + ";";
          updLastReceivedOn[key] = new Date().toISOString()+"|false";
        }
      });
      if (qryText.length > 0) {
        iisAccessQueue.enqueue(qryText);
      }
    }
  }
  catch (e) {
    logger.error("Exception in SetInterval update for module master table: last received on status", e.message, e.stack);
  }
}

async function drainCreateTableQueue(){
  try{
    let tableParam;
    let tableName = [];
    while(tableCreationQueue.getLength()>0){
      tableParam = tableCreationQueue.dequeue();
      if(tableName.indexOf(tableParam.inheritTableName) == -1) {
        await createParentTable(tableParam.type, tableParam.inheritTableName, tableParam.partTblRef, tableParam.ptTableName,tableParam.ptCheckConstDt, tableParam.arrUid);
        tableName.push(tableParam.inheritTableName);
      }
    }
    isPtTableTimerStarted = false;
    isCreateTblQueueRunning = false;
  }catch(e){
    isPtTableTimerStarted = false;
    isCreateTblQueueRunning = false;
    logger.error("Exception in drainCreateTableQueue", e.message)
  }
}

async function drainDBInsQueue(){
  try{
    let queueCnt = iisAccessQueue.getLength();
    let cnt = 0;
    let strQry = '';
    while(cnt < 1000 && iisAccessQueue.getLength() > 0 ){
      cnt ++;
      strQry += iisAccessQueue.dequeue();
    }
    logger.info("Inserted psql Query Length "+strQry.length+" & Queue count "+queueCnt);
    let result = await pgDb.fnDbQuery("drainDBInsQueue()",strQry,[]);
    if (!result.success){
      logger.error('Failed to Insert drainDBInsQueue', strQry, result.message);
    }
    isIISQueueRunning = false;
  } catch (e) {
    isIISQueueRunning = false;
    logger.error("Exception in drainDBInsQueue", e.message)
  }
}

async function dbAVMTestIns(data){
  try {
    let arrUid = collGuidUsrUid[data.guid];
    if (arrUid == undefined){
        collGuidUsrUid[data.guid] = arrUid = {user_id: data.userId, uid: data.avmTestId};
    }
    let type = "AVM";
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['timestamp']), type);
    if (ptTableName.success){
        insAVMTestRecords(data, ptTableName.ptTableName, arrUid);
    }
  } catch (e) {
    logger.error("dbSyslogIns Exception "+ e.message + " trace "+ e.stack);
  }
}

async function insAVMTestRecords(data, tableName,arrUid){
  try {
    let location = data.location.split('#');
    queryText = '';
    queryParam = [];
    let qryRunDetails ='';
    if (!data.error){
      let acceptRanges = data.headers['accept-ranges'] == undefined ? null : "\'"+data.headers['accept-ranges'][0]+"\'";
      let contLength = data.headers['content-length'] == undefined ? null : data.headers['content-length'][0];
      let body = strReplace(data.body);
      let headers = JSON.stringify(data.headers);
      try{
        data.headers = JSON.parse(headers.replace(/'/gm,""));
      } catch {
        data.headers = JSON.parse(headers)
      }
      queryText = "INSERT INTO "+tableName+"(avm_test_id, appedo_received_on, country, state, city, region, zone, success, resp_time_ms, bandwidth, status_code, status_text, redirected, redirected_url, headers, content_length, accept_ranges, resp_body, agent_tested_on, created_on)VALUES ("+data.avmTestId+",'"+data['timestamp']+"','"+location[0]+"','"+location[1]+"','"+location[2]+"','"+location[3]+"','"+location[4]+"',"+data.success+","+data.respTime+","+data.bandwidth+","+data.status+",'"+data.statusText+"',"+data.redirected+",'"+data.redirectedUrl+"','"+JSON.stringify(data.headers)+"',"+contLength+","+acceptRanges+",'"+body+"','"+data['timestamp']+"',now());"
      qryRunDetails = "INSERT INTO avm_test_run_details(avm_test_id, agent_id,location,agent_tested_on,status_text,status_code,resp_time_ms) VALUES("+data.avmTestId+","+data.agentId+",'"+data.location+"','"+data.timestamp+"','"+data.statusText+"',"+data.status+","+data.respTime+");"
    } else {
      queryText = "INSERT INTO "+tableName+"(avm_test_id, appedo_received_on, country, state, city, region, zone, success,err_code, err_msg, errno, agent_tested_on, resp_body,status_text) VALUES ("+data.avmTestId+",'"+data['timestamp']+"','"+location[0]+"','"+location[1]+"','"+location[2]+"','"+location[3]+"','"+location[4]+"',"+data.success+",'"+data.err.code+"','"+data.err.message+"','"+data.err.errno+"','"+data['timestamp']+"','"+data.err.message+"','"+data.err.code+"');";
      qryRunDetails = "INSERT INTO avm_test_run_details(avm_test_id, agent_id,location,agent_tested_on,status_text) VALUES("+data.avmTestId+","+data.agentId+",'"+data.location+"','"+data.timestamp+"','"+data.err.code+"');"
    }
    iisAccessQueue.enqueue(queryText);
    iisAccessQueue.enqueue(qryRunDetails);
    updAvmLastReceivedOn['str'+arrUid.uid] = new Date().toISOString()+"|"+data.statusText+"|true";
    let msg = JSON.stringify(data);
    insLogBreachPattern(arrUid, msg, tableName, data.timestamp, data, 'AVM');
    insLogBreachValue(arrUid, msg, tableName, data.timestamp,'AVM', data, data.guid)
  } catch (e){
    logger.error("Exception insAVMTestRecords", e.message, e.stack, JSON.stringify(data));
  }
}

async function dbSyslogIns(data){
  try {
    let arrUid = collGuidUsrUid[data.fields.guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(data.fields.guid);
      if (resGuid.success){
        collGuidUsrUid[data.fields.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let type = "LINUX_SYSLOG";
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), type);
    if (ptTableName.success){
        insSyslogRecords(data,arrUid, ptTableName.ptTableName, data.fields.guid);
    }
  } catch (e) {
    logger.error("dbSyslogIns Exception "+ e.message + " trace "+ e.stack);
  }
}

async function insSyslogRecords(data, arrUid, tableName,guid) {
  try {
    let appedoReceivedOn = new Date(data['@timestamp']).toISOString();
    let msg = strReplace(data.message); 
    let tableColArr = {hostname:"host", message:"logmessage",process_name:"program", timestamp:"log_timestamp",pid:"pid"};
    let qryText="";
    let qryIns ="INSERT INTO "+tableName+" (uid, guid, appedo_received_on, os, source, type, input_type, message";
    let osName = (data.host.name == undefined ? data.host.os.name : data.host.name);
    let qryValues = "VALUES ("+arrUid.uid+",'"+guid+"','"+appedoReceivedOn+"','"+osName+"','"+data.log.file.path+"','LINUX_SYSLOG','"+data.input.type+"','"+msg+"'";
    syslogData = data.system.syslog;
    keys = Object.keys(syslogData);
    keys.map(key => {
      qryIns += ","+tableColArr[key];
      qryValues += ",'"+strReplace(syslogData[key])+"'"; 
    });
    qryIns += ") ";
    qryValues += ")";
    qryText = qryIns+qryValues+";";
    iisAccessQueue.enqueue(qryText);
    updLastReceivedOn['str'+arrUid.uid] = new Date().toISOString()+"|true";
    insLogBreachPattern(arrUid, msg, tableName, appedoReceivedOn, data, 'LINUX_SYSLOG');
  } catch (e) {
    logger.error("insSyslogRecords Exception "+ e.message + " trace "+ e.stack );
    logger.error("insSyslogRecords Exception "+ e.message + " Data "+ JSON.stringify(data));
  }
}
async function dbMetricBeatApacheIns(data){
  try {
    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[data.fields.guid];
    let type = 'MetricSet';
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(data.fields.guid);
      if (resGuid.success){
        collGuidUsrUid[data.fields.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), type)
    let jsonRecords = json2array(data);
    if (ptTableName.success){
      insMetricSetRecords(ptTableName.ptTableName, jsonRecords, arrUid.uid, new Date(data['@timestamp']));
    }
  }
  catch (e) {
    logger.error("dbMetricBeatApacheBeatIns Exception "+ e.message + " trace "+ e.stack +" data "+JSON.stringify(data));
  }
}

function json2array(json_data)
{
  let json_Arr_data = [];
  Object.keys(json_data.apache).map(function(key) {
    let couterData ={
        "counter_type" : key,
        "counter_value" : json_data.apache[key],
        "exception":"",
        "process_name":""
    }
    json_Arr_data.push(couterData);
  }); 
  return json_Arr_data
}

async function dbNetworkIns(data){
  try {
    let arrUid;
    let resGuid = await getUserIdUidForGuid(data.fields.guid);
    if (resGuid.success){
      arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
    } else {
      return;
    }

    let type;
    if (!logArray.includes(data.type)){
      logArray.push(data.type);
      logger.info(data.type, JSON.stringify(data));
    }
  
    if (data.type == 'icmp') type = 'icmpLog';
    else if (data.type == 'http' || data.type == 'tls'){
      type = 'httpLog';
    } else {
      logger.error("Network Log not implemented for data type ",data.type, JSON.stringify(data));
      return;
    }
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), type);
    if (ptTableName.success){
      if (data.type == 'icmp') {
        insNetworkIcmpRecords(data,arrUid, ptTableName.ptTableName);
      } else if (data.type == 'http' || data.type == 'tls'){
        insNetworkHttpRecords(data,arrUid, ptTableName.ptTableName);
      }
    }
  } catch (e) {
    logger.error("dbNetworkIns Exception "+ e.message + " trace "+ e.stack+" data ",JSON.stringify(data));
  }
}

async function insNetworkHttpRecords(data, arrUidUsrId, tableName){
  try {
    let appedoReceivedOn = new Date(data['@timestamp']).toISOString();
    let qryIns = "INSERT INTO "+tableName+" (uid, appedo_received_on, destination_ip,destination_bytes, client_ip,client_bytes,source_ip,network_protocol,network_bytes,network_type,host,status, network_direction,network_transport";
    let desBytes = data.destination.bytes == undefined ? null : data.destination.bytes;
    let cliBytes = data.client.bytes == undefined ? null : data.client.bytes;
    let netBytes = data.network.bytes == undefined ? null : data.network.bytes;
    let netDirection = data.network.direction == undefined ? null : "'"+data.network.direction+"'";
    cliBytes = cliBytes == undefined ? 0 : cliBytes;
    let qryValues = " VALUES ("+arrUidUsrId.uid+",'"+appedoReceivedOn+"','"+data.destination.ip+"',"+desBytes+",'"+data.client.ip+"',"+cliBytes+",'"+data.source.ip+"','"+data.network.protocol+"',"+netBytes+",'"+data.network.type+"','"+JSON.stringify(data.host)+"','"+data.status+"',"+netDirection+",'"+data.network.transport+"'";
    let arrEvent = Object.keys(data.event);
    let netD =  netDirection == null ? '' : netDirection;
    let ddb = data.destination.bytes == undefined ?'':data.destination.bytes;
    ddb = ddb == undefined ? 0 : ddb;
    let dcb = data.client.bytes==undefined?'':data.client.bytes;
    dcb = dcb == undefined ? 0 : dcb;
    let dnb = data.network.bytes==undefined?'':data.network.bytes;
    dnb = dnb == undefined ? 0 : dnb;
    let msg = arrUidUsrId.uid+" , "+ appedoReceivedOn+" , "+ data.destination.ip+" , "+data.client.ip+" , "+ dcb+" , "+ data.source.ip+" , "+ data.event.category+" , "+ data.network.type+" , "+netD+","+data.network.protocol+", "+dnb+","+data.status+","+data.network.transport+","+ddb ;
    arrEvent.map(event =>{
      if (event == "start" || event == "end" || event == "duration" || event == "category") {
        qryIns += ",event_"+event;
        qryValues += ",'"+data.event[event]+"'";
        msg += ","+event+":"+data.event[event];
      }
    });
    data['response_status_code'] = 0;
    if (data.http != undefined && data.http.version != undefined ){ qryIns += ", http_version", qryValues += ",'"+data.http.version+"'"};
    if (data.http != undefined && data.http.response != undefined && data.http.response.status_code != undefined){
      qryIns += ",http_res_status, http_res_status_code,http_res_bytes";
      qryValues += ",'"+data.http.response.status_phrase+"',"+data.http.response.status_code+","+data.http.response.bytes;
      msg +=","+data.http.response.status_phrase+","+data.http.response.status_code+","+data.http.response.bytes;
      data['response_status_code'] = data.http.response.status_code;
    }
    if (data.http != undefined && data.status.toLowerCase() == 'error' && (data.http.response == undefined || data.http.response.status_code == undefined )){
      qryIns += ",http_res_status, http_res_status_code";
      qryValues += ",'"+data.error.message+"',598";
      data['response_status_code'] = 598;
    }
    if (data.http != undefined && data.http.request != undefined){
      qryIns += ",http_req_method,http_req_bytes";
      qryValues += ",'"+data.http.request.method+"',"+data.http.request.bytes;
      msg += ","+data.http.request.method+","+data.http.request.bytes;
    }
    if (data.url != undefined){
      qryIns += ",url_path,url_domain,url_scheme";
      qryValues += ",'"+data.url.path+"','"+data.url.domain+"','"+data.url.scheme+"'";
      msg +=","+data.url.path+","+data.url.domain+","+data.url.scheme;
      if (data.url.query != undefined){
        qryIns += ",url_query"; 
        qryValues += ",'"+data.url.query+"'";
        msg += ","+data.url.query;
      }
      if (data.url.port != undefined){
        qryIns += ",url_port";
        qryValues += ","+data.url.port;
        msg += ","+data.url.port;
      }
    }
    if (data.type == 'tls'){
      let statusCode;
      if (data.status.toLowerCase() == 'ok'){statusCode = 299; data['response_status_code'] = 299;}
      else {
        statusCode = 599; 
        data['response_status_code'] = 599; 
      }
      qryIns += ",url_path,url_domain,url_scheme,http_res_status,http_res_status_code";
      qryValues += ",'HTTPS data Encrypted','"+data.server.domain+"','HTTPS','"+data.status+"',"+statusCode;
      msg += ",HTTPS data Encrypted,"+data.server.domain+",HTTPS,"+data.status+","+statusCode;
    }
    qryIns += ")"+qryValues+");";
    iisAccessQueue.enqueue(qryIns);
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    Object.keys(data.host).map(key => {
      if (key != "os") msg += " , "+key+":"+data.host[key];
      else {
        Object.keys(data.host[key]).map(oskey => {
          msg += " , "+oskey+":"+data.host['os'][oskey];
        });
      }
    });
    insLogBreachPattern(arrUidUsrId, msg, tableName, appedoReceivedOn, data, 'HTTP');
    insLogBreachValue(arrUidUsrId, msg, tableName, appedoReceivedOn, 'HTTP', data, data.fields.guid);
  } catch (e) {
    logger.error(e.stack);
    logger.error("insNetworkHttpRecords Failed inside catch ",JSON.stringify(data));
  }
}

async function insNetworkIcmpRecords(data, arrUidUsrId, tableName){
  try {
    data['response_status_code'] = 0;
    let appedoReceivedOn = new Date(data['@timestamp']).toISOString();
    let netDirection = data.network.direction == undefined ? null : data.network.direction;
    let qryIns = "INSERT INTO "+tableName+" (uid, appedo_received_on, destination_ip, client_ip,client_bytes,source_ip,network_type,host,status,icmp_version, network_direction";
    let qryValues = " VALUES ("+arrUidUsrId.uid+",'"+appedoReceivedOn+"','"+data.destination.ip+"','"+data.client.ip+"',"+data.client.bytes+",'"+data.source.ip+"','"+data.network.type+"','"+JSON.stringify(data.host)+"','"+data.status+"','"+data.icmp.version+"','"+netDirection+"'";
    let arrEvent = Object.keys(data.event);
    let icmpReq = data.icmp.request;
    let msg = '';
    let dispMsg = "Uid : "+arrUidUsrId.uid+" ,dateTime : "+ appedoReceivedOn+" , destination_ip : "+ data.destination.ip+ " ,client_ip : "+data.client.ip+ " ,status : "+data.status;
    if (icmpReq != undefined){
      qryIns += "  ,req_msg, req_type, req_code";
      qryValues += " ,'"+icmpReq.message+"',"+icmpReq.type+","+icmpReq.code;
      msg += " ,"+icmpReq.message+","+icmpReq.type+","+icmpReq.code;
      dispMsg += ' ,ReqMsg : '+icmpReq.message+' ,ReqType : '+icmpReq.type;
    }
    let icmpRes = data.icmp.response;
    if (icmpRes != undefined){
      if (icmpRes.message == undefined) icmpRes['message'] = "Null";
      qryIns += "  ,res_msg, res_type, res_code";
      qryValues += " ,'"+icmpRes.message+"',"+icmpRes.type+","+icmpRes.code;
      msg += " ,"+icmpRes.message+","+icmpRes.type+","+icmpRes.code;
      dispMsg += ' ,ResMsg : '+icmpRes.message+' ,ResType : '+icmpRes.type;
    }
    arrEvent.map(event =>{
      if (event == "start" || event == "end" || event == "duration" || event == "category") {
        qryIns += ",event_"+event;
        qryValues += ",'"+data.event[event]+"'";
        msg += ","+data.event[event];
      }
    })
    qryIns += ")"+qryValues+");";
    iisAccessQueue.enqueue(qryIns);
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    //insert records if found for SLA
    let netD =  netDirection==null?'':netDirection;
    msg += arrUidUsrId.uid+" , "+ appedoReceivedOn+" , "+ data.destination.ip+" , "+data.client.ip+" , "+ data.client.bytes+" , "+ data.source.ip+" , "+ data.event.category+" , "+ data.network.type+" , "+data.status+" , "+ data.icmp.version+" , "+netD;
    Object.keys(data.host).map(key => {
      if (key != "os") msg += " , "+key+":"+data.host[key];
      else {
        Object.keys(data.host[key]).map(oskey => {
          msg += " , "+oskey+":"+data.host['os'][oskey];
          dispMsg += " , "+oskey+":"+data.host['os'][oskey];
        });
      }
    });
    insLogBreachPattern(arrUidUsrId, dispMsg, tableName, appedoReceivedOn, data, 'ICMP');
    insLogBreachValue(arrUidUsrId, msg, tableName, appedoReceivedOn, 'ICMP', data, data.fields.guid);
  } catch (e) {
    logger.error(e.stack);
    logger.error("insNetworkIcmpRecords Failed", JSON.stringify(data));
  }
}

async function dbCustomLogIns(data){
  try {
    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[data.fields.guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(data.fields.guid);
      if (resGuid.success){
        collGuidUsrUid[data.fields.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let type='customLog';
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), type );
    if (ptTableName.success) {
      // if (data["customLog"] != undefined){
        insCustomLogRecords(data, arrUid, ptTableName.ptTableName);
      // } else {
        // logger.error("yet to develop for custom log "+JSON.stringify(data));
      // }
    }
  } catch (e) {
    logger.error("dbCustomLogIns thrown Exception "+e.message+" trace "+e.stack +" data "+JSON.stringify(data));
  }
}

async function insCustomLogRecords(data, arrUidUsrId, tableName){
  try {
    let appedoReceivedOn = new Date(data['@timestamp']).toISOString();
    let msg;
    if(data.fields.log4j_info != undefined && data.fields.log4j_info){
      msg = data.log4j_time +" - "+ strReplace(data.customLog);
    }else{
      msg = strReplace(data.customLog);
    }
    let qryIns = "INSERT INTO "+tableName+" (uid, user_id, host, file_path, appedo_received_on, log_message, metadata) VALUES ("+arrUidUsrId.uid+","+arrUidUsrId.user_id+",'"+JSON.stringify(data.host)+"','"+data.log.file.path+"','"+appedoReceivedOn+"','"+msg+"',to_tsvector('"+msg+"'));";
    iisAccessQueue.enqueue(qryIns);
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    //insert records if found for SLA
    await insLogBreachPattern(arrUidUsrId, msg, tableName, appedoReceivedOn, data, 'CUSTOM_LOG');
  } catch (e) {
    logger.error(e.stack);
    logger.error("insCustomLogRecords Failed",JSON.stringify(data));
  }
}

async function insLogBreachValue(arrUidUsrId, msg, tableName, appedoReceivedOn, grokName, data, guid) {
  try {
    let qrySLA = "SELECT sla_id, sla_log_id, grok_column, is_above_threshold, critical_threshold_value, warning_threshold_value FROM so_sla_log WHERE uid=$1 AND log_grok_name =$2 AND grok_column IS NOT NULL AND (critical_threshold_value IS NOT NULL OR warning_threshold_value IS NOT NULL)";
    let qrySLAParam = [arrUidUsrId.uid, grokName];
    let resSLAValue = await pgDb.fnDbQuery('insLogBreachValue-getSLAPattern', qrySLA, qrySLAParam);
    if (resSLAValue.success && resSLAValue.rowCount > 0) {
      let res = resSLAValue.rows;
      let slaColl = []; 
      let insSLAQry = "INSERT INTO so_log_threshold_breach_" + arrUidUsrId.user_id + " (sla_log_id, sla_id, uid, guid, log_table_name, log_grok_name, grok_column,breached_severity, received_value,received_response_code, is_above_threshold, critical_threshold_value, warning_threshold_value, appedo_received_on) VALUES ";
      res.map(row => {
        let dur
        if (data.avmTestId != undefined){
          if (row.grok_column == 'status_code') dur = Number(data['status']);
          if (row.grok_column == 'resp_time_ms') dur = Number(data['respTime']);
          if (row.grok_column == 'content-length') {
            dur = data.headers != undefined ? data.headers['content-length'] != undefined ? Number(data['headers']['content-length'][0]): null : null;
          }
          data['response_status_code'] = data['status'];
        } else {
          dur = Number(data[row.grok_column]);
        }
        if (row.is_above_threshold){
          if ( dur >= Number(row.critical_threshold_value)){
            slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, received_value: dur, received_message: strReplace(msg), breach_severity: 'critical', log_table_name: tableName, log_grok_column:row.grok_column, appedo_received_on: appedoReceivedOn,received_response_code:data['response_status_code'], critical_value:Number(row.critical_threshold_value), warning_value:Number(row.warning_threshold_value),is_above_threshold:row.is_above_threshold});
          } else if (dur >= Number(row.warning_threshold_value)) {
            slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, received_value: dur, received_message: strReplace(msg), breach_severity: 'warning', log_table_name: tableName, log_grok_column: row.grok_column, appedo_received_on: appedoReceivedOn,received_response_code:data['response_status_code'], critical_value:Number(row.critical_threshold_value), warning_value:Number(row.warning_threshold_value),is_above_threshold:row.is_above_threshold});
          }
        } else {
          if (dur <= Number(row.critical_threshold_value)){
            slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, received_value: dur, received_message: strReplace(msg), breach_severity: 'critical', log_table_name: tableName, log_grok_column: row.grok_column, appedo_received_on: appedoReceivedOn,received_response_code:data['response_status_code'], critical_value:Number(row.critical_threshold_value)*1000, warning_value:Number(row.warning_threshold_value)*1000,is_above_threshold:row.is_above_threshold});
          } else if (dur <= Number(row.warning_threshold_value)) {
            slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, received_value: dur, received_message: strReplace(msg), breach_severity: 'warning', log_table_name: tableName, log_grok_name: row.grok_column, appedo_received_on: appedoReceivedOn,received_response_code:data['response_status_code'], critical_value:Number(row.critical_threshold_value)*1000, warning_value:Number(row.warning_threshold_value)*1000,is_above_threshold:row.is_above_threshold});
          }
        }
      });
      if(slaColl.length > 0){
        slaColl.map((sla, ix) => {
          if (ix != 0)
            insSLAQry += ",";
          insSLAQry += "(" + sla.sla_log_id + "," + sla.sla_id + "," + arrUidUsrId.uid + ",'" + guid + "','" + tableName + "','"+grokName+"','"+sla.log_grok_column+"','"+sla.breach_severity+"',"+sla.received_value+",'"+sla.received_response_code+"',"+sla.is_above_threshold+","+sla.critical_value+","+sla.warning_value+",'"+sla.appedo_received_on+"')"
        });
        insSLAQry += ";";
        iisAccessQueue.enqueue(insSLAQry);
        qryText = "UPDATE user_pvt_counters SET log_breached = TRUE WHERE user_id = "+arrUidUsrId.user_id+" AND NOT log_breached;";
        iisAccessQueue.enqueue(qryText);
        if (!tableName.includes('log_avm_')){
          let slaBreachQry = "INSERT INTO sla_breaches(user_id, uid, system_id, appedo_received_on, module, ref_table, count) SELECT "+arrUidUsrId.user_id+","+arrUidUsrId.uid+",mm.system_id,'"+appedoReceivedOn+"','"+grokName+"','"+tableName+"',1 FROM module_master mm WHERE mm.uid = "+arrUidUsrId.uid+";";
          iisAccessQueue.enqueue(slaBreachQry);
        } else {
          let slaBreachQry = "INSERT INTO sla_breaches(user_id, uid, appedo_received_on, module, ref_table,count) VALUES ("+arrUidUsrId.user_id+","+arrUidUsrId.uid+",'"+appedoReceivedOn+"','"+grokName+"','"+tableName+"',1);";
          iisAccessQueue.enqueue(slaBreachQry);
        }
      }
    }
  } catch (e) {
    logger.error("insLogBreachValue thrown Exception "+e.message+" trace "+e.stack+" data "+JSON.stringify(data));
  }
}

async function insLogBreachPattern(arrUidUsrId, msg, tableName, appedoReceivedOn, data, grokName) {
  try {
    let isAvmUp = false;
    is_icmp_process = false;
    if(data.fields == undefined){
      data['fields'] ={'guid':data.guid};
    }
    let qrySLA = "SELECT sla_id, sla_log_id, breach_pattern, breached_severity,is_contains, has_avm_process, has_icmp_process, avm_service_status FROM so_sla_log WHERE uid=$1 AND log_grok_name =$2 AND breach_pattern IS NOT NULL";
    let qrySLAParam = [arrUidUsrId.uid, grokName];
    let resSLAPattern = await pgDb.fnDbQuery('insLogBreachPattern-getSLAPattern', qrySLA, qrySLAParam);
    if (resSLAPattern.success && resSLAPattern.rowCount > 0) {
      let res = resSLAPattern.rows;
      let slaColl = []; 
      let insSLAQry = "INSERT INTO so_log_threshold_breach_" + arrUidUsrId.user_id + " (sla_log_id, sla_id, uid, guid, log_table_name, log_grok_name, breach_pattern, grok_column,breached_severity, received_message, received_pattern, appedo_received_on, destination_ip, source_ip, status, is_contains, has_avm_process) VALUES ";
      //res.map(async row => {
      for(i=0; i< res.length; i++){
        let row = res[i];
        if (row.is_contains){
          if (msg.toLowerCase().includes(row.breach_pattern.toLowerCase())) {
            if(row.has_icmp_process){
              is_icmp_process = true;
            }
            if(row.has_avm_process && row.avm_service_status){
              if(!data.error){
                let is_insert = await isAVMUp(row, arrUidUsrId);
                if(is_insert){
                  isAvmUp = true;
                  slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, breach_pattern: row.breach_pattern, received_message: /*data.reqUrl*/data.testName+' test is Up', breach_severity: row.breached_severity, log_table_name: tableName, log_grok_name: grokName, appedo_received_on: appedoReceivedOn, destination_ip: data.destination == undefined ? '' : data.destination.ip, source_ip: data.source == undefined ? '' : data.source.ip, status:data.status == undefined ? '' : data.status, is_contains: row.is_contains, has_avm_process: row.has_avm_process, has_icmp_process: row.has_icmp_process});
                }
              }
            }else{
              slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, breach_pattern: row.breach_pattern, received_message: strReplace(msg), breach_severity: row.breached_severity, log_table_name: tableName, log_grok_name: grokName, appedo_received_on: appedoReceivedOn, destination_ip: data.destination == undefined ? '' : data.destination.ip, source_ip: data.source == undefined ? '' : data.source.ip, status:data.status == undefined ? '' : data.status, is_contains: row.is_contains, has_avm_process: row.has_avm_process, has_icmp_process: row.has_icmp_process});
            }
          } 
        } else {
          if (!msg.toLowerCase().includes(row.breach_pattern.toLowerCase())) {
            if(row.has_icmp_process){
              is_icmp_process = true;
            }
            if(row.has_avm_process){
              if(!data.error){
                let is_insert = await isAVMUp(row, arrUidUsrId, data.timestamp);
                if(is_insert){
                  isAvmUp = true;
                  slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, breach_pattern: row.breach_pattern, received_message: /*data.reqUrl*/data.testName+' test is Up', breach_severity: row.breached_severity, log_table_name: tableName, log_grok_name: grokName, appedo_received_on: appedoReceivedOn, destination_ip: data.destination == undefined ? '' : data.destination.ip, source_ip: data.source == undefined ? '' : data.source.ip, status:data.status == undefined ? '' : data.status, is_contains: row.is_contains, has_avm_process: row.has_avm_process, has_icmp_process: row.has_icmp_process});
                }
              }
            }else{
              slaColl.push({ sla_id: row.sla_id, sla_log_id: row.sla_log_id, breach_pattern: row.breach_pattern, received_message: strReplace(msg), breach_severity: row.breached_severity, log_table_name: tableName, log_grok_name: grokName, appedo_received_on: appedoReceivedOn, destination_ip: data.destination == undefined ? '' : data.destination.ip, source_ip: data.source == undefined ? '' : data.source.ip, status:data.status == undefined ? '' : data.status, is_contains: row.is_contains, has_avm_process: row.has_avm_process, has_icmp_process: row.has_icmp_process});
            }
          } 
        }
        if(row.has_avm_process && !row.avm_service_status){
          isAvmUp = true;
        }
      };
      
      if(slaColl.length > 0){
        slaColl.map((sla, ix) => {
          if (ix != 0)
            insSLAQry += ",";
          insSLAQry += "(" + sla.sla_log_id + "," + sla.sla_id + "," + arrUidUsrId.uid + ",'" + data.fields.guid + "','" + tableName + "','"+grokName+"','" + sla.breach_pattern + "','log_message','" + sla.breach_severity + "','" +sla.received_message + "','" + sla.received_message + "','" + sla.appedo_received_on +"','"+sla.destination_ip+"','"+sla.source_ip+"','"+sla.status + "',"+sla.is_contains+ " ,"+ sla.has_avm_process+ " ,"+ sla.has_icmp_process+")";
        });
        insSLAQry += ";";
        iisAccessQueue.enqueue(insSLAQry);
        qryText = "UPDATE user_pvt_counters SET log_breached = TRUE WHERE user_id = "+arrUidUsrId.user_id+" AND NOT log_breached;";
        iisAccessQueue.enqueue(qryText);
        if(is_icmp_process){
          qryText = "UPDATE user_pvt_counters SET icmp_breached = TRUE WHERE user_id = "+arrUidUsrId.user_id+" AND NOT icmp_breached;";
          iisAccessQueue.enqueue(qryText);
        }
        if (!tableName.includes('log_avm_')){
          let slaBreachQry = "INSERT INTO sla_breaches(user_id, uid, system_id, appedo_received_on, module, ref_table, count) SELECT "+arrUidUsrId.user_id+","+arrUidUsrId.uid+",mm.system_id,'"+appedoReceivedOn+"','"+grokName+"','"+tableName+"',1 FROM module_master mm WHERE mm.uid = "+arrUidUsrId.uid+";";
          iisAccessQueue.enqueue(slaBreachQry);
        } else {
          let slaBreachQry = "INSERT INTO sla_breaches(user_id, uid, appedo_received_on, module, ref_table,count) VALUES ("+arrUidUsrId.user_id+","+arrUidUsrId.uid+",'"+appedoReceivedOn+"','"+grokName+"','"+tableName+"',1);";
          iisAccessQueue.enqueue(slaBreachQry);
        }
      }

      if(isAvmUp){
        console.log('Trigger the Avm Mail.');
        let param = {userId : arrUidUsrId.user_id};
        postToModuleService(param);
      }
    }
  } catch (e) {
    logger.error("insLogBreachPattern thrown Exception "+e.message+" trace "+e.stack+  strReplace(msg));
  }
}

async function isAVMUp(row, arrUidUsrId, dateTime){
  try{
    let is_insert = false;
    let qryText = "SELECT * FROM check_avm_url_up_process($1, $2, $3, $4, $5)";
    let qryParam = [arrUidUsrId.user_id, arrUidUsrId.uid, row.sla_id, row.breach_pattern, dateTime];
    console.log(qryParam);
    let response = await pgDb.fnDbQuery('insLogBreachPattern-isAVMUp', qryText, qryParam);
    console.log(response);
    if (response.success && response.rowCount > 0) {
      is_insert = response.rows[0].check_avm_url_up_process;
    }
    return is_insert;
  }catch (e) {
    logger.error("isAVMUp thrown Exception "+e.message+" trace "+e.stack);
  }
}

async function checkPerviousMsg(row, arrUidUsrId){
  try{
    let is_insert = false;
    let qryText = "SELECT frequency FROM avm_test_master WHERE avm_test_id = $1";
    let qryParam = [arrUidUsrId.uid];
    let response = await pgDb.fnDbQuery('insLogBreachPattern-getFrequency', qryText, qryParam);
    if (response.success && response.rowCount > 0) {
      console.log(response.rows[0]);
      let qrySLA = "SELECT received_message FROM so_log_threshold_breach_"+ arrUidUsrId.user_id +" WHERE uid = "+arrUidUsrId.uid+" AND breach_pattern = '"+row.breach_pattern+"' AND sla_id <> "+row.sla_id+" AND appedo_received_on > now() - interval '"+response.rows[0].frequency+" min 30 sec' order by appedo_received_on desc";
      let qrySLAParam = [];
      let resSLAValue = await pgDb.fnDbQuery('insLogBreachPattern-getSLAPattern', qrySLA, qrySLAParam);
      if (resSLAValue.success && resSLAValue.rowCount > 0) {
        is_insert = true;
      }else{
        console.log('');
      }
    }
    return is_insert;
  }catch (e) {
    logger.error("checkperviousMsg thrown Exception "+e.message+" trace "+e.stack);
  }
}

async function dbWinEventLogIns(data){
  try {

    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[data.fields.guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(data.fields.guid);
      if (resGuid.success){
        collGuidUsrUid[data.fields.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let type='winlog';
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), 'winEventLog');
    if (ptTableName.success) {
      if (data["winlog"] != undefined){
        insWinEventLogRecords(data, arrUid, ptTableName.ptTableName);
      } else {
        logger.error("yet to develop for win log "+JSON.stringify(data));
      }
    }
  } catch (e) {
    logger.error("dbWinEventLogIns thrown Exception "+e.message+" trace "+e.stack+"data "+JSON.stringify(data));
  }
}

async function insWinEventLogRecords(data, arrUidUsrId, tableName){
  try {
    let appedoReceivedOn = new Date(data['@timestamp']).toISOString();
    let msg = data.message != undefined ? strReplace(data.message) : '';
    let level = data.log.level;
    let channel = data.winlog.channel;
    let computerName = data.host.hostname;
    let eventAction = data.event.action;
    let winLog = JSON.stringify(data.winlog).replace("'","");
    let qryIns = "INSERT INTO "+tableName+" (uid, appedo_received_on, message, metadata, host, agent, level, event_action, winlog , channel, computer_name) VALUES ("+arrUidUsrId.uid+",'"+appedoReceivedOn+"','"+msg+"',to_tsvector('"+msg+"'), '"+JSON.stringify(data.host)+"', '"+JSON.stringify(data.agent)+"', '"+level+"', '"+eventAction+"','"+winLog+"','"+channel+"','"+computerName+"');";
    iisAccessQueue.enqueue(qryIns);
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    await insLogBreachPattern(arrUidUsrId, msg, tableName, appedoReceivedOn, data, 'windows_event_v7');
  } catch (e) {
    logger.error("insWinEventLogRecords",e.stack);
    logger.error("insWinEventLogRecords Failed",JSON.stringify(data),e.stack);
  }
}

async function dbIISLogIns(data){
  try {
    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[data.fields.guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(data.fields.guid);
      if (resGuid.success){
        collGuidUsrUid[data.fields.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let type=data.event.module+data.fileset.name;
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(data['@timestamp']), data.event.module+data.fileset.name);
    if (ptTableName.success) {
      if (data.event.module=="iis" && data.fileset.name =="access"){
        insLogIISAccessRecords(data, arrUid, ptTableName.ptTableName);
      } else if (data.event.module=="iis" && data.fileset.name =="error"){
        insLogIISErrorRecords(data, arrUid, ptTableName.ptTableName);
      } else {
        logger.error("yet to develope for "+data.event.module+"-"+data.fileset.name);
      }
    }
  } catch (e) {
    logger.error("dbIISLogIns thrown Exception "+e.message+" trace "+e.stack+ " data "+ JSON.stringify(data));
  }
}

async function insLogIISErrorRecords(data, arrUidUsrId, tableName){
  let jsoniisError;
  try {
    let uid = arrUidUsrId.uid;
    let appedo_received_on = new Date(data['@timestamp']).toISOString();
    let msg = data.message;
    //sorting is done to ensure mapping is smooth as columns are retrieved dynamically.
    let keysIISError = Object.keys(data.iis.error).sort();
    jsoniisError = data.iis.error;
    let sortedJson ={};
    let dataStruc ={reason_phrase:"varchar",queue_name:"VARCHAR",server_ip:"VARCHAR",server_port:"int",geoip:"json",remote_ip:"varchar",time:"timestamp without time zone",remote_port:"numeric", created_on: "timestamp with time zone"};
    let xQryText = "";
    let selQryText = "";
    let insQryText ="INSERT INTO "+tableName+" (uid, appedo_received_on, message, metadata,";
    keysIISError.map((key,idx) => {
      if (idx != 0) {xQryText += ","; insQryText += ","; selQryText += ",";}
      //incase of int or numeric json holds as string and if log does not have data for these column they substitute with '-'. 
      //While inserting to DB we are replacing '-' with null. For numeric column replace command fails when '-' is used hence 
      //converting the same to -1 and then do the replacement in the select query.
      sortedJson[key] = jsoniisError[key].length == 1 && jsoniisError[key] != '-'  ? dataStruc[key] == 'int' ? jsoniisError[key] : -1 :jsoniisError[key] ;
      xQryText += key+" "+dataStruc[key];
      insQryText += key;
      selQryText += jsoniisError[key].length == 1 && dataStruc[key] != 'int' ? "REPLACE("+key+",'-', null)" : key;
    });
    insQryText += ") SELECT "+uid +",'"+appedo_received_on+"','"+msg+"',to_tsvector('"+msg+"'), "+selQryText+" FROM jsonb_to_record('"+JSON.stringify(sortedJson)+"') as x("+xQryText+");";
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(insQryText);
    await insLogBreachPattern(arrUidUsrId, msg, tableName, appedo_received_on, data,'iis_error_v7');
  } catch (e) {
    logger.error(e.stack);
    logger.error("insLogIISErrorRecords Failed",JSON.stringify(jsoniisError));
  }
}

async function insLogIISAccessRecords(data, arrUidUsrId, tableName){
  let jsoniisAccess;
  try {
    let uid = arrUidUsrId.uid;
    let appedo_received_on = new Date(data['@timestamp']).toISOString();
    let msg = data.message;
    //sorting is done to ensure mapping is smooth as columns are retrieved dynamically.
    let keysIISAccess = Object.keys(data.iis.access).sort();
    jsoniisAccess = data.iis.access;
    let sortedJson ={};
    let dataStruc ={duration:"int",port:"int",url_path:"varchar", query:"varchar", win32_status:"varchar", geoip:"json", user_agent:"varchar", source_address:"varchar", response_status_code: "int",user_name: "varchar",referrer: "varchar", address: "varchar", sub_status: "int", cookie: "varchar", time: "timestamp without time zone", request_method:"varchar", response_body_bytes: "int", request_body_bytes: "int", created_on: "timestamp with time zone"};
    let xQryText = "";
    let selQryText = "";
    let insQryText ="INSERT INTO "+tableName+" (uid, appedo_received_on, message, metadata,";
    keysIISAccess.map((key,idx) => {
      if (idx != 0) {xQryText += ","; insQryText += ","; selQryText += ",";}
      //incase of int or numeric json holds as string and if log does not have data for these column they substitute with '-'. 
      //While inserting to DB we are replacing '-' with null. For numeric column replace command fails when '-' is used hence 
      //converting the same to -1 and then do the replacement in the select query.
      sortedJson[key] = jsoniisAccess[key].length == 1 && jsoniisAccess[key] != '-'  ? dataStruc[key] == 'int' ? jsoniisAccess[key] : -1 :jsoniisAccess[key] ;
      xQryText += key+" "+dataStruc[key]
      insQryText += key;
      selQryText += jsoniisAccess[key].length == 1 && dataStruc[key] != 'int' ? "REPLACE("+key+",'-', null)" : key;
    });
    insQryText += ") SELECT "+uid +",'"+appedo_received_on+"','"+msg+"',to_tsvector('"+msg+"'), "+selQryText+" FROM jsonb_to_record('"+ JSON.stringify(sortedJson)+"') as x("+xQryText+");";
    iisAccessQueue.enqueue(insQryText);
    updLastReceivedOn['str'+arrUidUsrId.uid] = new Date().toISOString()+"|true";
    await insLogBreachPattern(arrUidUsrId, msg, tableName, appedo_received_on, data,'iis_access_v7');
    await insLogBreachValue(arrUidUsrId, msg, tableName, appedo_received_on, 'iis_access_v7', data.iis.access, data.fields.guid);
  } catch (e) {
    logger.error(e.stack);
    logger.error("insLogIISAccessRecords Failed",JSON.stringify(jsoniisAccess));
  }
}

function strReplace(string){
  //to remove all other than character, numbers, dot and space (linefeed, cariage return, tab, colon and single quotes)
 // return string.replace(/\u000a/g,'').replace(/\u000d/g,'').replace(/(\r\n|\n|\r)/gm,"").replace(/\t/gm, " ").replace(/'|"/gm,"");
  return string.replace(/[^a-zA-Z0-9 \.]+/g, " ");
}

async function dbMetricIns(data){
  try {
    let counterMessage = JSON.parse(data.counterMessage);
    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[counterMessage.guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(counterMessage.guid);
      if (resGuid.success){
        collGuidUsrUid[counterMessage.guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        param = {
          guid: counterMessage.guid,
          moduleType: counterMessage.mod_type,
          table: 'module_master'
        }
        // postToModuleService("metricSetGuidNotFound",param);
        return;
      }
    }
    let type = "";
    if (!logArray.includes(counterMessage.type+counterMessage.mod_type)){
      logArray.push(counterMessage.type+counterMessage.mod_type);
      logger.info(counterMessage.type, counterMessage.mod_type, JSON.stringify(data));
    }
    if (counterMessage.type == "MetricSet"){
      type = counterMessage.type;
    } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "MSSQL") {
      type = counterMessage.type;
    } else if (counterMessage.type == "WaitStat" && counterMessage.mod_type == "MSSQL") {
      type = counterMessage.type;
    } else if (counterMessage.type == "LockSet" && counterMessage.mod_type == "MSSQL") {
      type = counterMessage.type;
    } else if (counterMessage.type == "SlowProcSet" && counterMessage.mod_type == "MSSQL") {
      type = counterMessage.type;
    } else if (counterMessage.type == "SLASet") {
      type = counterMessage.type;
    } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "PostgreSQL") {
      type = "psqlSlowQuerySet";
    } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "Oracle") {
      type = "oracleSlowQuerySet";
    } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "MySQL") {
      type = "mysqlSlowQuerySet";
    }else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "MySQL") {
      type = "mysqlLockQuerySet";
    }else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "PostgreSQL") {
      type = "psqlLockQuerySet";
    }else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "Oracle") {
      type = "oracleLockQuerySet";
    }
    
    if(type == undefined){
      logger.error("dbMetricIns", counterMessage.type, counterMessage.mod_type,"does not exist");
      logger.error("dbMetricIns", counterMessage.type, counterMessage.mod_type,"does not exist",JSON.stringify(data));
      return;
    }
  
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(counterMessage.datetime), type);
    if (ptTableName.success){
      if (counterMessage.type == "MetricSet"){
        insMetricSetRecords(ptTableName.ptTableName, counterMessage.MetricSet,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "MSSQL") {
        insSlowQuerySetRecords(ptTableName.ptTableName, counterMessage.SlowQuerySet,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "WaitStat" && counterMessage.mod_type == "MSSQL") {
        insWaitStatRecords(ptTableName.ptTableName, counterMessage.WaitStat,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "LockSet" && counterMessage.mod_type == "MSSQL") {
        insLockSetRecords(ptTableName.ptTableName, counterMessage.LockSet,arrUid.uid, counterMessage.datetime,counterMessage.guid);
      } else if (counterMessage.type == "SlowProcSet" && counterMessage.mod_type == "MSSQL") {
        insSlowProcSetRecords(ptTableName.ptTableName, counterMessage.SlowProcSet,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "SLASet") {
        insSLASetRecords(ptTableName.ptTableName, counterMessage.SLASet,arrUid, counterMessage.datetime,counterMessage.guid);
      } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "PostgreSQL") {
        insPsqlSlowQuerySetRecords(ptTableName.ptTableName, counterMessage.SlowQuerySet,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "PostgreSQL") {
        insPsqlLockQuerySetRecords(ptTableName.ptTableName, counterMessage.LockQuerySet,arrUid.uid, counterMessage.datetime,counterMessage.guid);
      } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "Oracle") {
        insPsqlSlowQuerySetRecords(ptTableName.ptTableName, counterMessage.SlowQuerySet,arrUid.uid, counterMessage.datetime);
      } else if (counterMessage.type == "SlowQuerySet" && counterMessage.mod_type == "MySQL") {
        insMysqlSlowQuerySetRecords(ptTableName.ptTableName, counterMessage.SlowQuerySet,arrUid.uid, counterMessage.datetime);
      }else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "MySQL") {
        insMysqlLockQuerySetRecords(ptTableName.ptTableName, counterMessage.LockQuerySet,arrUid.uid, counterMessage.datetime,counterMessage.guid);
      }else if (counterMessage.type == "LockQuerySet" && counterMessage.mod_type == "Oracle") {
        insOracleLockQuerySetRecords(ptTableName.ptTableName, counterMessage.LockQuerySet,arrUid.uid, counterMessage.datetime,counterMessage.guid);
      }
    }
  } catch (e) {
    logger.error("dbMetricIns exception "+e.message+" trace "+e.stack + " data "+JSON.stringify(data));
  }
}
async function insPsqlLockQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on, guid){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];       
    let qryText = "INSERT INTO "+tableName+" (uid,guid, appedo_received_on, blocking_query, blocking_session_id, client_net_address, username ,blocked_mode, connect_time, db_name,blocking_mode,request_query,request_session_id,resource_type, blocked_state,blocking_state, wait_duration_ms) SELECT "+uid+",'"+guid+"','"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(blocking_query varchar, blocking_session_id int, client_net_address varchar, username varchar, blocked_mode varchar,connect_time timestamp with time zone, db_name varchar, blocking_mode varchar,request_query varchar, request_session_id int,resource_type varchar, blocked_state varchar, blocking_state varchar, wait_duration_ms int);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insPsqlLockQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insPsqlLockQuerySetRecords Failed",strJsonRecords);
  }
}
async function insOracleLockQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on, guid){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];       
    let qryText = "INSERT INTO "+tableName+" (uid,guid, appedo_received_on, blocking_query, blocking_session_id, username, blocked_mode, connect_time, db_name, blocking_mode, request_query, request_session_id,blocked_state,blocking_state, wait_duration_ms) SELECT "+uid+",'"+guid+"','"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(blocking_query varchar, blocking_session_id int, username varchar,blocked_mode varchar,connect_time timestamp with time zone, db_name varchar, blocking_mode varchar,request_query varchar, request_session_id int, blocked_state varchar, blocking_state varchar, wait_duration_ms int);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insOracleLockQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insOracleLockQuerySetRecords Failed",strJsonRecords);
  }
}
async function insMysqlLockQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on, guid){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];       
    let qryText = "INSERT INTO "+tableName+" (uid,guid, appedo_received_on, blocking_query, blocking_session_id, client_net_address, command, connect_time, db_name,request_mode,request_query,request_session_id,resource_type, status, wait_duration_ms) SELECT "+uid+",'"+guid+"','"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(blocking_query varchar, blocking_session_id int, client_net_address varchar, command varchar, connect_time timestamp with time zone, db_name varchar, request_mode varchar, request_query varchar, request_session_id int, resource_type varchar, status varchar, wait_duration_ms int);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insMysqlLockQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insMysqlLockQuerySetRecords Failed",strJsonRecords);
  }
}


async function insPsqlSlowQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on) {
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, appedo_received_on, calls, duration_ms, query) SELECT "+uid+",'"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(calls int, duration_ms int, query text);";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insPsqlSlowQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insPsqlSlowQuerySetRecords Failed",strJsonRecords);
  }
}
async function insMysqlSlowQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on) {
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, appedo_received_on, start_time, calls, duration_ms, query) SELECT "+uid+",'"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(start_time timestamp without time zone, calls int, duration_ms int, query text);";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insMysqlSlowQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insMysqlSlowQuerySetRecords Failed",strJsonRecords);
  }
}
async function getParentTableQry(tableType, tableName){
  let qryText;
  if (tableType == 'collector_'){
    qryText = "CREATE TABLE "+tableName+"(id serial,uid int, received_on timestamp with time zone, appedo_received_on timestamp with time zone, agent_version varchar, counter_type int, exception text, counter_value numeric, top_process json, is_first boolean, process_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mssql_slowquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, appedo_received_on timestamp with time zone, calls int, duration_ms int, query text, cached_time timestamp with time zone, last_execution_time timestamp with time zone, avg_cpu_time_ms varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mssql_wait_stat_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, appedo_received_on timestamp with time zone, status varchar, count int DEFAULT 0, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mssql_lock_data_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, guid varchar, appedo_received_on timestamp with time zone, request_session_id int,blocking_session_id int, wait_duration_ms int, client_net_address varchar, connect_time timestamp with time zone, resource_type varchar, request_query varchar, blocking_query varchar, request_mode varchar,command varchar,status varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mssql_slowprocedure_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, appedo_received_on timestamp with time zone, calls int, duration_ms int, procedure text, procedure_name varchar, cached_time timestamp with time zone, last_execution_time timestamp with time zone, avg_cpu_time_ms varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'so_threshold_breach_'){
    qryText = "CREATE TABLE "+tableName+"(so_tb_id bigserial, sla_id bigint, user_id bigint, enterprise_id bigint, guid varchar, uid bigint, counter_id bigint, counter_template_id bigint, is_above boolean DEFAULT false, in_percentage boolean DEFAULT false, percentage bigint, percentage_of_value bigint, critical_threshold_value bigint, received_value numeric, appedo_received_on timestamp with time zone, thread_created boolean DEFAULT false, warning_threshold_value bigint, breached_severity varchar, has_processed boolean DEFAULT false, breached_cnt integer, breached_cnt_processed boolean DEFAULT false, process_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_iis_access_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, message text, metadata tsvector, duration integer, port integer, url_path varchar, query varchar, win32_status varchar, geoip json, user_agent varchar, source_address varchar, remote_ip varchar, response_status_code integer, user_name varchar, referrer varchar, address varchar, sub_status integer, cookie varchar, time timestamp without time zone, request_method varchar, response_body_bytes integer, request_body_bytes integer, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_iis_error_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, message text, metadata tsvector, reason_phrase varchar, queue_name varchar, server_ip varchar, server_port integer, geoip json, remote_ip varchar, time timestamp without time zone, remote_port numeric, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_windows_event_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, message text, metadata tsvector, host json, agent json, level varchar, event_action varchar, winlog json, channel varchar, computer_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_custom_log_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, user_id integer, appedo_received_on timestamp with time zone, log_message text, file_path varchar, metadata tsvector, host json, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'network_icmp_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer,appedo_received_on timestamp with time zone, destination_ip varchar,client_ip varchar,client_bytes integer,source_ip varchar, event_category varchar,event_duration bigint,event_start timestamp with time zone,event_end timestamp with time zone,icmp_version varchar, network_type varchar,host json, status varchar, req_msg varchar, req_code smallint,req_type smallint,res_msg varchar, res_code smallint,res_type smallint,geo_ip_client json,geo_ip_dest json,network_direction varchar,created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'network_http_v7_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, destination_ip varchar, destination_bytes bigint, client_ip varchar, client_bytes bigint, source_ip varchar, event_category varchar, event_duration bigint, event_start timestamp with time zone,  event_end timestamp with time zone, http_version varchar,  network_protocol varchar,  network_bytes bigint, network_type varchar, network_direction varchar, network_transport varchar, host json, status varchar, url_path varchar, url_domain varchar, url_query varchar, url_port integer, url_scheme varchar, http_res_status varchar,  http_res_status_code smallint, http_res_bytes integer, http_req_method varchar, http_req_bytes integer, geo_ip_client json, geo_ip_dest json, flow_final boolean, flow_source_packets integer, flow_dest_packets integer, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'postgres_slowquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, calls integer, duration_ms integer, query text, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'oracle_slowquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, calls integer, duration_ms integer, query text, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mysql_slowquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, appedo_received_on timestamp with time zone, start_time timestamp without time zone, calls integer, duration_ms integer, query text, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'mysql_lockquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, guid varchar, appedo_received_on timestamp with time zone, request_session_id bigint,blocking_session_id bigint, wait_duration_ms int, client_net_address varchar, connect_time timestamp with time zone, resource_type varchar, request_query varchar, blocking_query varchar, request_mode varchar,command varchar,status varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'postgres_lockquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, guid varchar, appedo_received_on timestamp with time zone, request_session_id bigint,blocking_session_id bigint, wait_duration_ms int, client_net_address varchar, username varchar,connect_time timestamp with time zone, resource_type varchar, request_query varchar, blocking_query varchar, blocked_mode varchar,blocking_mode varchar,blocked_state varchar,blocking_state varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'oracle_lockquery_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial,uid int, guid varchar, appedo_received_on timestamp with time zone, blocking_query varchar, blocking_session_id int, username varchar,blocked_mode varchar,connect_time timestamp with time zone, db_name varchar, blocking_mode varchar,request_query varchar, request_session_id int, blocked_state varchar, blocking_state varchar, wait_duration_ms int, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_syslog_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer,guid varchar, host varchar,appedo_received_on timestamp with time zone,os varchar,source varchar,type varchar, input_type varchar,program varchar,logsource varchar,priority varchar,log_timestamp varchar,pid varchar,facility varchar,logmessage varchar,message text, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'log_avm_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, avm_test_id integer, appedo_received_on timestamp with time zone, country varchar, state varchar, region varchar, zone varchar, city varchar, agent_id bigint, success boolean, resp_time_ms integer, bandwidth numeric,status_code smallint, status_text varchar, redirected boolean, redirected_url varchar, requested_url varchar, headers json, content_length integer,accept_ranges varchar, resp_body text, err_code varchar, err_msg varchar, errno varchar, agent_tested_on timestamp with time zone, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on);"
  } else if (tableType == 'net_stack_'){
    qryText = "CREATE TABLE "+tableName+"(id bigserial, uid integer, guid varchar, thread_id integer, function_id integer, hierarchy integer, start_time timestamp with time zone, end_time timestamp with time zone, resp_time integer, continuity_id integer, created_on timestamp with time zone DEFAULT now(), appedo_received_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on); CREATE INDEX idx_"+tableName+"_start_time ON "+tableName+"(start_time); CREATE INDEX idx_"+tableName+"_end_time ON "+tableName+"(end_time);"
  }
  qryText += "CREATE INDEX IF NOT EXISTS idx_"+tableName+"_appedo_received_on ON "+tableName+"(appedo_received_on);" 
  logger.info("getParentTableQry "+tableType+"\t"+ tableName+"\t"+qryText);
  return qryText;
}

async function getSetPartitionTableUid(arrUid, dt, type){
  try {
    if (type == "SLASet") {
      let ptTableName = collTypeTable[type]+arrUid.user_id;
      return {success:true, ptTableName:ptTableName};
    }
    let numMonth = dt.getMonth()+1;
    let strMonth = numMonth.toString().length == 1 ? '0'+numMonth.toString() : numMonth.toString();
    let numDt = dt.getDate();
    let strDt = numDt.toString().length == 1 ? '0'+ numDt.toString() : numDt.toString();
    let strDate = dt.getFullYear()+ strMonth+ strDt;
    let partTblRef ;
    // for network partition table is user based with daily partition. aim to bring all data from all system of this user to one table for mainly topology analysis. each protocol will have different table 
    if (type == 'icmpLog' || type == 'httpLog') partTblRef = arrUid.user_id; 
    else partTblRef = arrUid.uid;
    let ptTableName = collTypeTable[type]+partTblRef+'_'+strDate;
    let ptCheckConstDt = dt.getFullYear()+'-'+strMonth+'-'+strDt;
    if (collPartionTable['str'+strDate] == undefined || collPartionTable['str'+strDate].indexOf(ptTableName) == -1 ){
      let qryText = "SELECT table_name FROM information_schema.tables WHERE table_name = $1";
      let qryParam = [ptTableName];
      let resPtTable = await pgDb.fnDbQuery('getSetPtCollectorTableUid-chkpartition-'+arrUid.uid+"-"+type,qryText,qryParam );
      if (resPtTable.success && resPtTable.rowCount>0){
        if (collPartionTable['str'+strDate] == undefined) {
          collPartionTable['str'+strDate]= [];
          collPartionTable['str'+strDate].push(resPtTable.rows[0].table_name);
        } else {
          collPartionTable['str'+strDate].push(resPtTable.rows[0].table_name);
        }
        if (chartVisualColl['str'+arrUid.uid] == undefined) {chartVisualColl['str'+arrUid.uid] = [];};
        if ((type == 'icmpLog' || type == 'httpLog') && chartVisualColl['str'+arrUid.uid].indexOf(type) == -1){
          queueObj.push({type:type, partTblRef:partTblRef, arrUid:arrUid});
        }
      } else {
        if (resPtTable.success){
          let inheritTableName = collTypeTable[type]+partTblRef;
          let resCreatePartition = await createPartitionTable(ptTableName, ptCheckConstDt, inheritTableName);
          if (resCreatePartition.success){
            if (collPartionTable['str'+strDate] == undefined){
              collPartionTable['str'+strDate]=[];
              collPartionTable['str'+strDate].push(ptTableName);
            }
            logger.info("Partition table "+ ptTableName +"created before inserting data");
          } else {
            if (resCreatePartition.message.includes("\""+inheritTableName+"\" does not exist")){
              let param = {
                type : type,
                inheritTableName : inheritTableName,
                partTblRef : partTblRef,
                ptTableName : ptTableName,
                ptCheckConstDt : ptCheckConstDt,
                arrUid : arrUid
              }
              tableCreationQueue.enqueue(param);
              if(!isPtTableTimerStarted){
                ptTableCreationTimerStart();
              }
            } else { 
              if (resCreatePartition.message.includes("duplicate key value violates unique constraint")) {
                return {success:true,ptTableName: ptTableName};
              } else if (resCreatePartition.message.includes("already exists")){
                if (collPartionTable['str'+strDate] == undefined || collPartionTable['str'+strDate].indexOf(ptTableName) == -1 ){
                  if (collPartionTable['str'+strDate] == undefined )
                    collPartionTable['str'+strDate]=[];
                  if (collPartionTable['str'+strDate].indexOf(ptTableName) == -1)
                    collPartionTable['str'+strDate].push(ptTableName);
                }
                return {success:true,ptTableName: ptTableName};
              }
              logger.error(resCreatePartition.message);
              logger.error("Partition table creation failed for user: " +arrUid.user_id+" UID: "+arrUid.uid+" Date: " +dt+" type: "+type+" ptTableName "+ptTableName+" Query "+resCreatePartition.query+" Query Param "+resCreatePartition.queryParam);
              return {success:false, ptTableName:''};
            }
          }
        } else {
          logger.error(insResult.message);
          logger.error("Insert Failed",strJsonRecords);
          return {success:false, ptTableName:''};
        }
      }
    }
    return ({success:true, ptTableName:ptTableName})
  } catch (e) {
    logger.error(e.stack);
    logger.error("getSetPtCollectorTableUid Failed");
    return {success:false, ptTableName:''};
  }
}

async function createParentTable(type, inheritTableName, partTblRef, ptTableName, ptCheckConstDt, arrUid){
  try{
    //create parent table from template table
    let qText = await getParentTableQry(collTypeTable[type], inheritTableName);
    let resCreateParentTable = await pgDb.fnDbQuery("getSetPtCollectorTableUid-createParentTable",qText,[]);
    if (resCreateParentTable.success ){
      if (resCreateParentTable.success) {
        // charts to be created in chart visual table 
        if (type == 'iisaccess'){
          let dispName ="iis_access_v7"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM add_access_log_query($1, $2, $3, $4, $5);";
          insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'iiserror') {
          let dispName ="iis_error_v7"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM add_iis_error_log_query($1, $2, $3, $4, $5);";
          insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'winEventLog') {
          let dispName ="windows_event_v7"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM  add_windows_event_chart_visual_query($1, $2, $3, $4, $5);";
          insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'customLog'){
          let dispName ="custom_log_v7"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM  add_iis_error_custom_log_query($1, $2, $3, $4, $5);";
          insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'httpLog'){
          let dispName ="HTTP"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
          //insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'icmpLog'){
          let dispName ="ICMP"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
          //insChartVisual(arrUid, type, dispName, qryT, tableName);
        } else if (type == 'LINUX_SYSLOG') {
          let dispName ="LINUX_SYSLOG"; let tableName = collTypeTable[type]+partTblRef;
          let qryT = "SELECT * FROM add_generic_query($1, $2, $3, $4, $5);";
          insChartVisual(arrUid, type, dispName, qryT, tableName);
        }
      }
      let resCreatePartition = await createPartitionTable(ptTableName, ptCheckConstDt, inheritTableName);
      if (resCreatePartition.success){
        if (collPartionTable['str'+strDate] == undefined){
          collPartionTable['str'+strDate]=[];
          collPartionTable['str'+strDate].push(ptTableName);
        }
        logger.info("Partition table "+ ptTableName +"created before inserting data");
      } else {
        logger.error(resCreateParentTable.message);
        logger.error("Parent table creation failed ");
        return {success:false, ptTableName:''};
      }
    } else if (resCreateParentTable.message.includes("\""+inheritTableName+"\" already exists")){
      logger.info("Parent table already exists. returning table name "+ptTableName);
      return {success:true,ptTableName: ptTableName};
    }
    else {
      logger.error(resCreateParentTable.message);
      logger.error("Parent table creation failed for "+"user: " +arrUid.user_id+" UID: "+arrUid.uid+" Date: " +dt+" type: "+type+" ptTableName "+ptTableName);
      return {success:false, ptTableName:''};
    }
  }catch (e) {
    logger.error(e.stack);
    logger.error("createParentTable Failed");
  }
}

async function chkInsChartVisual(type, partTblRef, arrUid) {
  try {
    let rtn = collTypeTable[type] + partTblRef; //rtn - ref_table_name used in chart_visual
    let qryT = "SELECT chart_id FROM chart_visual_" + arrUid.user_id + " WHERE ref_table_pkey_id=$1 AND ref_table_name = $2 LIMIT 1";
    let qryP = [arrUid.uid, rtn];
    let resUid = await pgDb.fnDbQuery('getSetPtCollectorTableUid-Network-chkUID-' + arrUid.uid + type, qryT, qryP);
    if (resUid.success && resUid.rowCount == 0) {
      if (type == 'httpLog') {
        let dispName = "HTTP";
        let tableName = collTypeTable[type] + partTblRef;
        chartVisualColl['str' + arrUid.uid].push(type);
        let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
        insChartVisual(arrUid, type, dispName, qryT, tableName);
      }
      else if (type == 'icmpLog') {
        let dispName = "ICMP";
        let tableName = collTypeTable[type] + partTblRef;
        chartVisualColl['str' + arrUid.uid].push(type);
        let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
        insChartVisual(arrUid, type, dispName, qryT, tableName);
      }
    }
    else {
      chartVisualColl['str' + arrUid.uid].push(type);
    }
  } catch (e) {
    logger.error("chkInsChartVisual thrown Exception "+e.message+" trace "+e.stack);
  }
}

async function insChartVisual(arrUid, type,dispName, qryT, tName) {
  try { 
    let qryText = "SELECT id FROM log_master_table WHERE log_grok=$1";
    let qryParam = [dispName];
    let insLogRefTable = await pgDb.fnDbQuery('insChartVisual-getIdFromLogMasterTable', qryText, qryParam);
    if (insLogRefTable.success) {
      qryP = [arrUid.user_id, arrUid.uid, insLogRefTable.rows[0].id, tName, dispName];
      let insChartVisual = await pgDb.fnDbQuery('insChartVisual-'+type+'-'+arrUid.uid, qryT, qryP);
      if (!insChartVisual.success) {
        logger.error(insChartVisual.message);
        logger.error("Chart visual creation failed for table " + collTypeTable[type] + arrUid.uid);
      }
    }
    else {
      logger.error(insLogRefTable.message);
      logger.error("Insert to log ref table failed for table " + collTypeTable[type] + arrUid.uid);
    }
  } catch (e) {
    logger.error("insChartVisual thrown Exception "+e.message+" trace "+e.stack);
  }
}

async function createPartitionTable(ptTableName, ptCheckConstDt, inheritTableName) {
  try {
    let dt = new Date(ptCheckConstDt);
    let toDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()+1);
    let toStrDate = toDate.getFullYear()+'-'+(toDate.getMonth()+1)+'-'+toDate.getDate();
    let createPartitionQryText = "CREATE TABLE IF NOT EXISTS "+ptTableName+" PARTITION OF "+inheritTableName+" FOR VALUES FROM ('"+ptCheckConstDt+"') TO ('"+toStrDate+"')";
    let resCreatePartition = await pgDb.fnDbQuery("getSetPtCollectorTableUid-CreatePartition", createPartitionQryText, []);
    return resCreatePartition;
  } catch (e) {
    logger.error("createPartitionTable thrown Exception "+e.message+" trace "+e.stack);
  }
}

async function insSLASetRecords(tableName, jsonRecords, arrUid, appedo_received_on, guid){
  let strJsonRecords;
  let jlength = jsonRecords.length;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, user_id, guid, received_on, breached_severity,counter_id,critical_threshold_value,is_above,in_percentage,process_name,received_value,sla_id,warning_threshold_value) SELECT "+arrUid.uid+","+arrUid.user_id+",'"+guid+"','"+dt+"',* FROM jsonb_to_recordset($1) as x(breached_severity varchar,counter_id bigint,critical_threshold_value bigint,is_above boolean,percentage_calculation boolean,process_name varchar ,received_value numeric,sla_id bigint,warning_threshold_value bigint);";
    let insResult = await pgDb.fnDbQuery('insSLASetRecords',qryText, qryParam);
    if (!insResult.success){
      logger.error(insResult.message);
      logger.error('Failed to Insert',strJsonRecords);
    } else {
      qryText = "UPDATE user_pvt_counters SET oad_breached = TRUE WHERE user_id = "+arrUid.user_id+" AND NOT oad_breached;";
      iisAccessQueue.enqueue(qryText);
      let slaBreachQry = "INSERT INTO sla_breaches(user_id, uid, system_id, appedo_received_on, module, ref_table, count) SELECT "+arrUid.user_id+","+arrUid.uid+",mm.system_id,'"+dt+"',mm.module_code,'"+tableName+"',"+jlength+" FROM module_master mm WHERE mm.uid = "+arrUid.uid+";";
      iisAccessQueue.enqueue(slaBreachQry);
    }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insSLASetRecords Failed",strJsonRecords);
  }
}

async function insSlowProcSetRecords(tableName, jsonRecords, uid, appedo_received_on){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, appedo_received_on, avg_cpu_time_ms, cached_time, calls, db_name, duration_ms, last_execution_time, procedure, procedure_name) SELECT "+uid+",'"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x( avg_cpu_time_ms varchar, cached_time timestamp with time zone, calls int, db_name varchar, duration_ms integer,last_execution_time timestamp with time zone, procedure text, procedure_name varchar);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insSlowProcSetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insSlowProcSetRecords Failed",strJsonRecords);
  }
}

async function insWaitStatRecords(tableName, jsonRecords, uid, appedo_received_on){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, appedo_received_on, count, db_name, status) SELECT "+uid+",* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(appedo_received_on timestamp with time zone, count int, db_name varchar, status varchar);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insWaitStatRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insWaitStatRecords Failed",strJsonRecords);
  }
}

async function insLockSetRecords(tableName, jsonRecords, uid, appedo_received_on, guid){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid,guid, appedo_received_on, blocking_query, blocking_session_id, client_net_address, command, connect_time, db_name,request_mode,request_query,request_session_id,resource_type, status, wait_duration_ms) SELECT "+uid+",'"+guid+"','"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(blocking_query varchar, blocking_session_id int, client_net_address varchar, command varchar, connect_time timestamp with time zone, db_name varchar, request_mode varchar, request_query varchar, request_session_id int, resource_type varchar, status varchar, wait_duration_ms int);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insLockSetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insLockSetRecords Failed",strJsonRecords);
  }
}

async function insSlowQuerySetRecords(tableName, jsonRecords, uid, appedo_received_on){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, appedo_received_on, avg_cpu_time_ms, cached_time, calls, db_name, duration_ms, last_execution_time, query) SELECT "+uid+",'"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(avg_cpu_time_ms varchar, cached_time timestamp with time zone, calls int, db_name varchar, duration_ms int, last_execution_time timestamp with time zone, query text);";
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insSlowQuerySetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
  } catch (e) {
    logger.error(e.stack);
    logger.error("insSlowQuerySetRecords Failed",strJsonRecords);
  }
}

async function insMetricSetRecords(tableName, jsonRecords, uid, appedo_received_on){
  let strJsonRecords;
  try {
    let dt = new Date(appedo_received_on).toISOString();
    strJsonRecords = JSON.stringify(jsonRecords);
    // let qryParam = [strJsonRecords];
    let qryText = "INSERT INTO "+tableName+" (uid, received_on, appedo_received_on, counter_type, counter_value, exception, process_name) SELECT "+uid+", now(),'"+dt+"',* FROM jsonb_to_recordset('"+strJsonRecords+"') as x(counter_type int, counter_value numeric, exception text, process_name varchar);";
    iisAccessQueue.enqueue(qryText);
    // let insResult = await pgDb.fnDbQuery('insMetricSetRecords',qryText, qryParam);
    // if (!insResult.success){
    //   logger.error(insResult.message);
    //   logger.error('Failed to Insert',strJsonRecords);
    // }
    updLastReceivedOn['str'+uid] = new Date().toISOString()+"|true";
  } catch (e) {
    logger.error(e.stack);
    logger.error("insMetricSetRecords Failed",strJsonRecords);
  }
}

async function getUserIdUidForGuid(guid){
  try {
    let missingGuid = missingGUID.indexOf(guid);
    if (missingGuid == -1){
      let qryText = "SELECT user_id, uid FROM module_master WHERE guid=$1";
      let qryParam = [guid];
      let result = await pgDb.fnDbQuery('getUserIdUidForGuid-'+guid, qryText, qryParam);
      if (result.success && result.rowCount > 0){
        return result;
      } else {
        logger.error("GUID Not FOUND ", guid);
        missingGUID.push(guid);
        return {sucess:false}
      }
    } else {
      logger.error("GUID Not FOUND ", guid);
      return {sucess:false}
    }
  } catch (e) {
    logger.error("getUserIdUidForGuid thrown Exception "+e.message+" trace "+e.stack);
    return {sucess:false}
  }
}

let logArray = [];
router.post('/putCustomLog', async(req,res)=>{
  try {
    dbCustomLogIns(req.body);
    if (!logArray.includes('putCustomLog')){
      logArray.push('putCustomLog');
      logger.info("putCustomLog", JSON.stringify(req.body));
    }
    res.send("Received Custom Log Set");
  } catch (e){
    logger.error("Received Custom Log Set with exception",e.message, e.stack);
    res.send("Received Custom Log Set with exception"+e.message);
  }
})

router.post('/putLogSet', async(req,res)=>{
  try {
    dbIISLogIns(req.body);
    if (!logArray.includes('putLogSet')){
      logArray.push('putLogSet');
      logger.info("putLogSet", JSON.stringify(req.body));
    }
    res.send("Received LogSet");
  } catch (e) {
    logger.error("Received Log Set with exception", e.message, e.stack);
    res.send("Received Log Set with exception"+e.message);
  }
})

router.post('/putMetricSet', async(req,res)=>{
  try {
    dbMetricIns(req.body);
    res.send("Received MetricSet");
  } catch (e) {
    logger.error("Received MetricSet with exception",e.message, e.stack);
    res.send("Received MetricSet with exception"+e.message);
  }
});

router.post('/putWinEventlog', async(req,res)=>{
  try {
    dbWinEventLogIns(req.body);
    if (!logArray.includes('putWinEventlog')){
      logArray.push('putWinEventlog');
      logger.info("putWinEventlog", JSON.stringify(req.body));
    }
    res.send("Received Windows log beat");
  } catch (e) {
    logger.error("Received Windows Log Set with exception",e.message, e.stack);
    res.send("Received Windows Log Set with exception"+e.message);
  }
});

router.post('/putMetricBeatApachelog', async(req,res)=>{
  try {
    dbMetricBeatApacheIns(req.body);
    if (!logArray.includes('putMetricBeatApachelog')){
      logArray.push('putMetricBeatApachelog');
      logger.info("putMetricBeatApachelog", JSON.stringify(req.body));
    }
    res.send("Received Metricbeat Apache log");
  } catch (e) {
    logger.error("Received Metricbeat Apache Log Set with exception",e.message, e.stack);
    res.send("Received Metricbeat Apache Log Set with exception"+e.message);
  }
});

router.post('/putNetworkLog', async(req,res)=>{
  try {
    dbNetworkIns(req.body);
    res.send("Received Network Log");
  } catch (e) {
    logger.error("Received Network Log Set with exception",e.message, e.stack);
    res.send("Received Network Log Set with exception"+e.message);
  }
});

router.post('/putSyslog', async(req,res)=>{
  try {
    dbSyslogIns(req.body);
    //console.log(req.body);
    //console.log(JSON.parse(req.body));
    if (!logArray.includes('putSyslog')){
      logArray.push('putSyslog');
      logger.info("putSyslog", JSON.stringify(req.body));
    }
    res.send("Received Syslog");
  } catch (e) {
    logger.error("Received Syslog with exception",e.message, e.stack)
    res.send("Received Syslog with exception"+e.message);
  }
});

router.get('/testCollectorService', async(req,res)=>{
  try {
    res.json({success:true, message:"Success", result : "received test request" });
  } catch (e) {
    logger.error(process.pid,e.stack, 'getCollData');
    res.json({success:false, error:true, message: e.message});
  }
})

router.post('/avmAgentData', async(req,res)=>{
  try {
    dbAVMTestIns(req.body.testResult);
    res.json({success:true, message:"Received "+req.body.testResult.avmTestId});
  } catch (e) {
    logger.error(process.pid,e.stack,'avmAgentData' );
    res.json({success:false, error:true, message: e.message});
  }
})

router.post('/putNetStackData', async(req,res)=>{
  try {
    qNetStackData(req.body);
    res.json({success:true, message:"Received "});
  } catch (e) {
    logger.error(process.pid,e.stack,'avmAgentData' );
    res.json({success:false, error:true, message: e.message});
  }
})
let guidWiseQueue = [];

async function qNetStackData(netStackData){
  try {
    stackData = JSON.parse(netStackData.counterMessage);
    let guid = stackData.guid;
    //getting uid from global array if not get from db and store to global array
    let arrUid = collGuidUsrUid[guid];
    if (arrUid == undefined){
      let resGuid = await getUserIdUidForGuid(guid);
      if (resGuid.success){
        collGuidUsrUid[guid] = arrUid = {user_id : resGuid.rows[0].user_id, uid: resGuid.rows[0].uid};
      } else {
        return;
      }
    }
    let type = "NETSTACK";
    //check partition table for current date if not create table and store to global variable for easy access
    let ptTableName = await getSetPartitionTableUid(arrUid, new Date(netStackData['@timestamp']), type);
    if (!ptTableName.success){
      logger.error("partition table failed to create for uid "+arrUid.uid+" type "+type)
      return;
    }
    let netStack = stackData.NETSTACK;
    if(Object.keys(guidWiseQueue).indexOf(guid) == -1){
      guidWiseQueue[guid] = new Queue();
    }
    netStack.map((data,i)=>{
      guidWiseQueue[guid].enqueue(data);
    });
    updLastReceivedOn['str'+arrUid.uid] = new Date().toISOString()+"|true";
    if (!isNetStackTimerStarted){
      logger.info("Net Stack timer started @"+new Date().toLocaleDateString());
      netStackTimerStart();
      isNetStackTimerStarted = true;
    }
  } catch (e) {
    logger.error("qNetStackData thrown Exception "+e.message+" trace "+e.stack);
  }
}

let time = new Date().getTime();
function processNetStackData(){
  if (isProcessingNetStackData){
    return;
  }
  isProcessingNetStackData = true;
  let cnt = 0; //to ensure all guids are processed before setting the isprocessing attribute to false
  let qCountZero = true; //to stop the netstack process timer.
  Object.keys(guidWiseQueue).map(guid =>{
    if (time+10000 <= new Date().getTime() || guidWiseQueue[guid].getLength() > 1000) {
      let processedRecCnt = 0;
      let stackDataArr = [];
      while (guidWiseQueue[guid].getLength() > 0 || processedRecCnt > 1000){
        processedRecCnt++;
        stackDataArr.push(guidWiseQueue[guid].dequeue());
      }
      if (guidWiseQueue[guid].getLength() > 0){
        qCountZero = false;
      }
      logger.info("Stack data processed for guid "+guid+" userID "+collGuidUsrUid[guid].user_id+" uid "+collGuidUsrUid[guid].uid); 
      //$1 - bigint uid, $2 - string guid, $3 - string received on, $4 - json stack data 
      let queryText = "SELECT * FROM ins_net_stack($1,$2,$3,$4)";
      let queryParam = [collGuidUsrUid[guid].uid, guid, new Date().toISOString(),JSON.stringify(stackDataArr)];
      pgDb.fnDbQuery("processNetStackData", queryText, queryParam);
      logger.info("Stack data processed count "+stackDataArr.length+" query: "+queryText);
      logger.info(JSON.stringify(stackDataArr));
    }
    cnt ++;
  });
  if (cnt == Object.keys(guidWiseQueue).length){
    isProcessingNetStackData = false;
    isNetStackTimerStarted = false;
    if (qCountZero){
      // if all the queue of the guid is processed and no more incoming data then timer is stopped.
      netStackTimerStop();
    }
  }
}

router.post('/deletedModule', async(req,res)=>{
  try {
    let param = req.body;
    delete collGuidUsrUid[param.guid];
    res.json({success:true, message:"Received"});
  } catch (e) {
    logger.error(process.pid,e.stack,'deletedModule', req.body);
    res.json({success:false, error:true, message: e.stack});
  }
})

process.on('uncaughtException', err => {
	console.log(`Uncaught Exception: ${err.message}`)
})
