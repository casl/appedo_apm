//const Router = require('express-promise-router')
const { Pool } = require('pg')
const { ClickHouse } = require('clickhouse')
const dateFormat = require('dateformat');
const PgConfig = require('./config/apd_constants');
// const pool = new Pool (PgConfig.pgDbConfig); // used for pg V9.6.
const pool = new Pool (PgConfig.pgV12DbConfig); // used for pg V12
//const ch = new ClickHouse(PgConfig.chDbConfig);
global.logger = require('../log');
module.exports.fnDbQuery = fnDbQuery;
module.exports.chkPSQLDB = chkConnection;

let chkDBPSQL = chkConnection();
if (chkDBPSQL =="success"){
  console.log("PGV12 Connection Successful");
  logger.info("PGV12 Connection Successful");
} else {
  console.log("PGV12 Connection Failed,", chkDBPSQL);
  logger.error("PGV12 Connection Failed, ", chkDBPSQL);
}

async function chkConnection(){
  try{
    const client = await pool.connect();
    await client.query('SELECT now()');
    client.release();
    return "success";
   } catch (e) {
    return e.message;
   }
}

async function fnDbQuery(methodName,queryText, queryParam) {
  let client ;
  let start;
  let qryResult;
  try {
    start = Date.now();
    client = await pool.connect();
    try {
      const result = await client.query(queryText, queryParam);
      const duration = Date.now() - start;
      result.success = true;
      result.error = false;
      console.log(dateFormat(start,"isoDateTime")+", "+methodName+" , "+duration+' ms'+' ,Pool Idle: '+pool.idleCount +' ,QueryWaiting: '+pool.waitingCount +' Pool Total Cnt: '+pool.totalCount);
      logger.info(process.pid,'psqlCollector',methodName,duration +' ms',pool.idleCount,pool.waitingCount,pool.totalCount);
      qryResult =  result;
    } catch (e) {
        logger.error(process.pid,methodName,e.message, queryText, queryParam);
        qryResult = {success:false, error: true, message: e.message, query: queryText, queryParam: queryParam};
    } finally {
      client.release();
    }
  } catch (e){
    qryResult = {success:false, error: true, message: e.message, query: queryText, queryParam: queryParam};
  } finally {
    return qryResult;
  }
}

pool.on('error', (err) => {
  console.error('An idle client has experienced an error', err.message)
});
