const crypto = require('crypto').randomBytes(32).toString('hex');
const keyEncryptDecrypt = '9r55hnl26wwmfm2397';
const Crypt = require('crypto');

module.exports = {
privateKey: crypto,
dbPwdPvtKey:'$2a$31$BQOhDZT/Ppu69lJNNiIRee',
keyEncryptDecrypt: '9r55hnl26wwmfm2397',
//resourcePath: 'c:/Appedo/',
resourcePath: '/mnt/appedo/',
downloads: 'resource/downloads/',
seleniumScriptClassFilePath: 'resource/sum/',
pgDbConfig:{
        user: 'postgres',
        database: 'appedo_apm',
        password: 'sa',
        host: 'localhost',
        port: 5432,
        max: 100, // max number of clients in the pool
        idleTimeoutMillis: 300000
},
chDbConfig:{
        database: 'appedo_apm',
        host: 'localhost',
        port: 8123,
        basicAuth: {
            username: 'default',
            password: 'sa',
        }
},
pgV12DbConfig:{
    user: 'postgres', 
    database: 'appedo_apm', 
    password: 'sa', 
    host: 'localhost', 
    port: 5432, 
    max: 100, // max number of clients in the pool
    idleTimeoutMillis: 300000
},
Currency: 'rs',
NumberFormat: 'crore', //supported values are Crore, Million, thousands if not given will take thousands as default
tokenExpiresIn: '30m', 
tokenExpInSec: 30*60, //converted above value in sec, this is important for calculating refresh token calculation
collRefreshToken : []
}

module.exports.encrypt = encrypt;
async function encrypt(val) {
    const cipher = Crypt.createCipher('aes192', keyEncryptDecrypt);
    let encrypted = cipher.update(val, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted;
}

module.exports.decrypt = decrypt;
async function decrypt(val) {
    const decipher = Crypt.createDecipher('aes192', keyEncryptDecrypt);
    let decrypt = decipher.update(val, 'hex', 'utf8');
    decrypt += decipher.final('utf8');
    return decrypt;
}


