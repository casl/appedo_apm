const dateFormat = require('dateformat');
const winston = require('winston');
require('winston-daily-rotate-file');

var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.DailyRotateFile)({
        name:'info',
        filename: './log/info/appedo-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'info'
      }),
      new (winston.transports.DailyRotateFile)({
        name:'error',
        filename: './log/error/appedo-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'error'
      }),
      new (winston.transports.DailyRotateFile)({
        name:'dbinserror',
        filename: './log/dbinserror/appedo-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'debug'
      })
    ]
  });
logger.info('logger Started for process '+process.pid +' and parent process '+process.ppid);

module.exports = logger;
