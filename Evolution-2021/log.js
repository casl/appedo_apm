const { createLogger, format, transports } = require('winston');
var winston = require('winston');
const { combine, timestamp, label, printf,splat, json } = format;
require('winston-daily-rotate-file');

const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

/*const logger = createLogger({
  format: combine(
    splat(),
    timestamp(),
    myFormat
  ),    
  transports: [
    new (transports.DailyRotateFile)({
        filename: './log/error/error-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'error',
    }),
    new (transports.DailyRotateFile)({
        filename: './log/info/info-%DATE%.log',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        level: 'info',            
    }),
  ]
});*/

var logger = {
  appedo: winston.createLogger({
    format: combine(
      splat(),
      timestamp(),
      myFormat
    ),    
    transports: [
      new (winston.transports.DailyRotateFile)({
          filename: './log/appedo/error/error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
      }),
      new (winston.transports.DailyRotateFile)({
          filename: './log/appedo/info/info-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',            
      })
    ]
  }),
  report: winston.createLogger({
    format: combine(
      splat(),
      timestamp(),
      myFormat
    ),    
    transports: [
      new (winston.transports.DailyRotateFile)({
          filename: './log/ReportScheduler/error/error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
      }),
      new (winston.transports.DailyRotateFile)({
          filename: './log/ReportScheduler/info/info-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',            
      })
    ]
  }),
  avmScheduler: winston.createLogger({
    format: combine(
      splat(),
      timestamp(),
      myFormat
    ),    
    transports: [
      new (winston.transports.DailyRotateFile)({
          filename: './log/AvmScheduler/error/error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
      }),
      new (winston.transports.DailyRotateFile)({
          filename: './log/AvmScheduler/info/info-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',            
      })
    ]
  }),
  moduleService: winston.createLogger({
    format: combine(
      splat(),
      timestamp(),
      myFormat
    ),    
    transports: [
      new (winston.transports.DailyRotateFile)({
          filename: './log/moduleService/error/error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
      }),
      new (winston.transports.DailyRotateFile)({
          filename: './log/moduleService/info/info-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',            
      })
    ]
  }),
  appedoMailer: winston.createLogger({
    format: combine(
      splat(),
      timestamp(),
      myFormat
    ),    
    transports: [
      new (winston.transports.DailyRotateFile)({
          filename: './log/appedoMailer/error/error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
      }),
      new (winston.transports.DailyRotateFile)({
          filename: './log/appedoMailer/info/info-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',            
      })
    ]
  })
};


// if (process.env.NODE_ENV !== 'production') {
//   logger.add(new transports.Console({
//     format: format.simple()
//   }));
// }

//logger.log('info','Info logger started at %s %s', new Date().toLocaleString(), 'local time');
//logger.log('error','Error logger Started at %s %s', new Date().toLocaleString(), 'local time');

logger.appedo.log('info','Info logger started at %s %s', new Date().toLocaleString(), 'local time');
logger.appedo.log('error','Error logger Started at %s %s', new Date().toLocaleString(), 'local time');
logger.report.info('ReportScheduler info logger started at %s %s', new Date().toLocaleString(), 'local time');
logger.report.error('ReportScheduler error loggerstarted at %s %s', new Date().toLocaleString(), 'local time');
logger.avmScheduler.info('AvmScheduler info logger started at %s %s', new Date().toLocaleString(), 'local time');
logger.avmScheduler.error('AvmScheduler error loggerstarted at %s %s', new Date().toLocaleString(), 'local time');
logger.moduleService.info('moduleService info logger started at %s %s', new Date().toLocaleString(), 'local time');
logger.moduleService.error('moduleService error loggerstarted at %s %s', new Date().toLocaleString(), 'local time');
logger.appedoMailer.info('appedoMailer info logger started at %s %s', new Date().toLocaleString(), 'local time');
logger.appedoMailer.error('appedoMailer error loggerstarted at %s %s', new Date().toLocaleString(), 'local time');

module.exports.appedoLogger = logger.appedo;
module.exports.reportLogger = logger.report;
module.exports.avmLogger = logger.avmScheduler;
module.exports.moduleServiceLogger = logger.moduleService;
module.exports.appedoMailerLogger = logger.appedoMailer;

//module.exports = logger;