module.exports = {
    //Below settings are used for dev environment
    dev:{
        getAVMTest:'http://localhost:3000/avmScheduler',
        collectorService:'http://localhost:8081/services', //point to collector service
        moduleService: "http://localhost:3000/moduleService", //point to module service to add register agent 
    },
    prod:{
        getAVMTest:'@AVM_SCHEDULER@',
        moduleService: "@AVM_UI_SERVICES_V1@", //point to module service to add register agent 
        collectorService:'@AVM_COLLECTOR@', //point to collector service
    },
    //for all environment
    useIPLocation: false, //for onPrem and no internet connectivity this must be false
    debugMode: false,
    isProxyRequired: false,
    // proxy url like as : 'https://username:password@your-proxy.com:port'
    ProxyURL: '',
    isSSLConfigure: false,
    abortRequestTimeOutInMs: 60000, // set time out value in MilliSec. Ex (1 Min : 1000 * 60). 
    //will be used only if useIpLocation is false or the ip address is not part of public ip. 
    location: 'India#Tamilnadu#Chennai##', // country#state#city#region#zone
}

//running the service in production mode for the first time below code to be used.
/*
set NODE_ENV=production //only for the first time.
node avmAgent.js
*/

