const Router = require('express-promise-router')
const fetch = require('node-fetch');
const AbortController = require('abort-controller');
const { URLSearchParams } = require('url');
const settings = require('../config/apd_constants');
const https = require('https');
const http = require('http');
const {machineId, machineIdSync} = require('node-machine-id');
global.logger = require('../log');
const os = require('os');
var pjson = require('../package.json');
var HttpsProxyAgent = require('https-proxy-agent');
function Queue(){var a=[],b=0;this.getLength=function(){return a.length-b};this.isEmpty=function(){return 0==a.length};this.enqueue=function(b){a.push(b)};this.dequeue=function(){if(0!=a.length){var c=a[b];2*++b>=a.length&&(a=a.slice(b),b=0);return c}};this.peek=function(){return 0<a.length?a[b]:void 0}};
collectorQueue = new Queue();

let systemId = machineIdSync({original: true});
logger.info("Agent running in system: " +systemId);
let ipLocation;
const router = new Router();
module.exports = router;

isFetching = false;
sendSuccess = true; //used to ensure collector service is up and running

processAVMTestCnt = 0;
totalExecutedTestCnt = 0;
let agtLocation ;
let AVMConfigProperties = {}
let agentId;
let lastRequestReceivedAt;
let lastResponseSentAt;
let lastErrorAt;
let loggerGetAvmTestErr = 0;
let getAVMTestSuccess = true;

logger.info("current Node evironment is "+process.env.NODE_ENV);
if (process.env.NODE_ENV == 'production'){
  logger.info("loading prod environment parameters"+JSON.stringify(settings.prod));
  AVMConfigProperties['getAVMTest'] = settings.prod.getAVMTest;
  AVMConfigProperties['collectorService'] = settings.prod.collectorService;
  AVMConfigProperties['moduleService'] = settings.prod.moduleService;
} else {
  logger.info("loading dev environment parameters"+JSON.stringify(settings.dev));
  AVMConfigProperties['getAVMTest'] = settings.dev.getAVMTest;
  AVMConfigProperties['collectorService'] = settings.dev.collectorService;
  AVMConfigProperties['moduleService'] = settings.dev.moduleService;
}


if (settings.useIPLocation){
  getIPLocation().then(res => {
    try {
      ipLocation = res;
      agtLocation = (res.country_name+'#'+res.state+'#'+res.city+'##').toLowerCase();
    } catch {
      agtLocation = settings.location;
    }
    setIPLocation();
  });
} else {
  agtLocation = settings.location;
  setIPLocation();
  logger.info("Set Agent Location is "+agtLocation);
}

errorOnSend = false; //untill collector queue is having data this will be set to true, will be made false even if collector service is not running.
logger.info("Collector queue service started, will send processed request every 500ms. If collector is down, processing of AVM will be temporarily stopped till collector is up and running.")
setInterval(() => {
  if (collectorQueue.getLength()>0 && !errorOnSend){
    sendAVMTestResult();
  }
}, 10000);

function setIPLocation(){
  let location = agtLocation.split("#");
  if (ipLocation == undefined){
    ipLocation = {"country_name":location[0],"city":location[2],"postal":"NA","latitude":null,"longitude":null,"IPv4":null,"state":location[1]};
  };
  logger.info("Current Agent Location is "+JSON.stringify(ipLocation));
  registerAgent();
}

function registerAgent(){
  logger.info("Registering Agent "+systemId);
  param = {
    user_id: 1,
    is_private: false,
    ip_address: ipLocation.IPv4,
    country: ipLocation.country_name.toLowerCase(),
    state: ipLocation.state == undefined?'': ipLocation.state.toLowerCase(),
    city: ipLocation.city == undefined?'': ipLocation.city.toLowerCase(),
    region: '',
    zone: '',
    latitude: ipLocation.latitude,
    longitude: ipLocation.longitude,
    os_type: os.type(),
    operating_system: os.platform(),
    os_version: os.release(),
    agent_version: pjson.version,
    status: 'ACTIVE',
    created_by: 1,
    created_on: new Date().toISOString(),
    guid: systemId
  };
  let url = AVMConfigProperties.moduleService+'/registerAVL';
  let promise = fetch(url, { method: 'POST', body: JSON.stringify(param),headers: { 'Content-Type': 'application/json' }});
  promise
    .then(res => res.json())
    .then(json => {
      agentId = json.agentId;
      totalExecutedTestCnt += Number(json.urlCnt); //if registration method takes some time and agent already processed some test url then this will add the cnt instead of assigning the received value.
      logger.info(json.message+" Total Executed test "+ totalExecutedTestCnt, json);
    })
    .catch(err =>{
      logger.error("registerAgent() exception "+err);
      logger.info("Module Service is down, Registering agent will be tried after 10 sec.");
      setTimeout(() => {
        registerAgent();
      }, 10000);
    })
}

//getting the avm test from server and it will fetch data once if the current run is complete or will check every 500 ms.
//Log will be written every 10 min.
let logTimerSendSuccess = 0;
setInterval(() => {
  if (processAVMTestCnt == 0){
    logger.info("AVM Agent Started, will check availability of test every 500 ms. from "+ new Date().toLocaleString() +" in domain "+AVMConfigProperties.getAVMTest);
    setTimeout(() => {
      updateAgentStatus();
    }, 20000);
  }
  if (processAVMTestCnt != 0 && processAVMTestCnt % 1200 == 0){
    // log will be written every 10 minutes
    logger.info("Agent running and checking pending test for location "+agtLocation+" in domain "+AVMConfigProperties.getAVMTest +" every 500ms. local time "+new Date().toLocaleString()+ " Total Executed test till now is "+totalExecutedTestCnt.toString());
  }
  processAVMTestCnt++;
  if (processAVMTestCnt == 1000000){
    processAVMTestCnt = 1;
  }
  if(!isFetching && sendSuccess){ 
    isFetching = true;
    processAVMTest();
  }
  if (!sendSuccess){
    getCollectorStatus();
    //log will be written every minute.
    if(logTimerSendSuccess%120 == 0){
      logger.error("Could not collect AVM Test as Collector Service is down");
      logTimerSendSuccess++;
    }
  }
}, 500);

//updating agent status once in 5 minutes.
setInterval(() => {
  updateAgentStatus();
}, 5*60*1000);

function updateAgentStatus(){
  if(agentId == undefined) {
    logger.error("Agent Id not found hence quiting the update process, will be tried again in next 5 minutes");
    return;
  }
  let url = AVMConfigProperties.moduleService+"/updateAVMAgentStatus";
  let param = {agentId: agentId, last_requested_on: lastRequestReceivedAt, last_responded_on: lastResponseSentAt,last_error_on:lastErrorAt, last_received_url_cnt:totalExecutedTestCnt, location: agtLocation, ipAddress: ipLocation.IPv4, latitude: ipLocation.latitude, longitude: ipLocation.longitude};
  let promise = fetch(url,{ 
    method: 'POST', 
    body: JSON.stringify(param),
    headers: { 'Content-Type': 'application/json' },
  });
  promise
    .then(res => res.json())
    .then(json => {
      if (json.success){
        logger.info(json.message);
      } else {
        logger.error(json.message);
      }
    })
    .catch (e => {
      logger.error("updateAgentStatus()"+e.message);
    })
}

//When this url fails, default location given in the apd_constants.js file will be used.
function getIPLocation(){
  let promise =  fetch('https://geolocation-db.com/json/');
  return promise.then(res => res.json())
      .then(json => { return json })
      .catch (e =>{
        logger.error("getIPLocation()"+e.message);
        return undefined;
      });
}

logCollector = 0;

function getCollectorStatus(){
  let url = AVMConfigProperties['collectorService']+"/testCollectorService";
  let promise =  fetch(url);
  return promise.then(res => res.json())
      .then(json => { 
        sendSuccess = true;
        errorOnSend = false;
        logger.info("Collector service is up and running, AVM Test will be processed.");
        return json 
      })
      .catch (e =>{
        sendSuccess = false;
        if (logCollector%120 == 0){
          logger.info("Collector service is down, AVM Test will be not be processed.");
        }
        logCollector++;
        if (logCollector >1000000) logCollector = 1;
        return undefined;
     });
}

function getAVMTest(){
  let param = {location: agtLocation};
  let getTestDomain = AVMConfigProperties.getAVMTest;
  // if (settings.debugMode){
  //   logger.info("Domain used to fetch pending test is "+getTestDomain);
  // }
  return fetch(getTestDomain+'/getAVMTest', { 
    method: 'POST', 
    body: JSON.stringify(param),
    headers: { 'Content-Type': 'application/json' },
  })
  .then(async res => {
    lastRequestReceivedAt = new Date().getTime();
    let bodyContent = await res.json();
      return {success:res.ok,error:false, res: res, resBody:bodyContent};
  })
  .then(fullResponse => {
    if (!getAVMTestSuccess){
      getAVMTestSuccess = true;
      logger.info("Scheduler up and running. Will start collecting the pending test from queue");
    }
    return fullResponse;
  })
  .catch(err =>{
    if (loggerGetAvmTestErr%600 == 0){
      logger.error("Scheduler down. getAVMTest() failed. "+err);
    }
    loggerGetAvmTestErr++;
    if (loggerGetAvmTestErr == 1000000) loggerGetAvmTestErr = 1;
    getAVMTestSuccess = false;
    return {success:false, error:true};
  })
}

async function processAVMTest(){
  try { 
    let testRes = await getAVMTest();
    if (settings.debugMode){
      logger.info(JSON.stringify(testRes));
    }
    let result;
    if (testRes.success && testRes.resBody.success){
      let test = testRes.resBody.result;
      //Replace variables with evaluated result incase variables are using in the avm test
      if (test.variables != null){
        let variables = Object.keys(test.variables);
        let strTest = JSON.stringify(test);
        let val;
        variables.map(name =>{
          try {
            let param = '@@'+name+'@@'
            val = eval(test.variables[name]);
            while (strTest.includes(param)){
              strTest = strTest.replace(param,val);
            }
            if (settings.debugMode){
              logger.info("Following variables are replaced Parameter Name: %s, Value: %s, test id %s",name, val, test.avm_test_id);
            }
          } catch (e) {
            logger.error("Evaluation of variable failed %s, %s, %s for test id %s",name, test.variables[name],e.message, test.avm_test_id);
            //if eval fails, replacement will not be done for the  variable
          }
        });
        test = JSON.parse(strTest);
      }
      let headers = {}
      test.request_headers.map(header => {
        headers[header.name] = header.value;
      });
      //console.log('AVM Test Id : '+test.avm_test_id+' <> sending request to test_url');
      if (test.request_parameters.length == 0){
        result = await getURL(test.testurl, test.request_method, headers, test.request_body);
      } else {
          let params = {};
          test.request_parameters.map(param => {
            params[param.name] = param.value;
          });
          let formData = Object.keys(params);
          let body = new URLSearchParams();
          formData.map(key=>{
              body.append(key, params[key]);
          });
          result = await getURL(test.testurl, test.request_method, headers, body);
      }
      //console.log('AVM Test Id : '+test.avm_test_id+' <> received response from test_url');
      //console.log('AVM Test Id : '+test.avm_test_id+' <> result : '+JSON.stringify(result));
      if (!result.error){
        let headers = result.res.headers.raw();
        if (headers['content-length'] != undefined){
          let contLength = headers['content-length'][0];
          result.res['bandwidth'] = (Math.round(Number(contLength)/Number(result.res.respTime)*100)/100).toString();
        } else {
          result.res['bandwidth'] = null;
        }
        result.res["timestamp"] = new Date().toISOString();
        result = {success:result.res.ok, body:result.resBody, statusText:result.res.statusText, status:result.res.status,headers:result.res.headers.raw(), type:result.res.type, redirected:result.res.redirected, redirectedUrl: result.res.url, reqUrl:test.testurl, testName:test.testname, respTime: result.res.respTime, avmTestId: test.avm_test_id, location:agtLocation, bandwidth:result.res['bandwidth'], timestamp: result.res.timestamp, error:false, userId: test.user_id, guid: test.guid, agentId: agentId};
      } else {
        result["timestamp"] = new Date().toISOString();
        result["avmTestId"] = test.avm_test_id;
        result["testName"] = test.testname;
        result["userId"] = test.user_id;
        result["guid"] = test.guid;
        result["location"] = agtLocation;
        result["error"] = true;
        result["agentId"] = agentId;
      }
      if (settings.debugMode){
        logger.info(result.avmTestId +" successfully processed at " + new Date().toLocaleString());
      }
      collectorQueue.enqueue(JSON.stringify(result));
      //console.log(JSON.stringify(result));
      // sendAVMTestResult(result);
    } else {
      if (testRes.error){
        lastErrorAt = new Date().getTime();
        logger.error("processAVMTest() failed, "+JSON.stringify(testRes));
      } 
    }
  } catch (e){
    logger.error("processAVMTest() exception, "+e.message + e.stack)
  }
  isFetching = false;
}

function getURL(url, method, headers, body){
  if (method == 'GET' || method == 'HEAD'){
    body = null;
  };
  if (method == 'WSDL'){
    method = 'POST';
  }
  let startTime = new Date().getTime();
  let duration = null;
  let controller = new AbortController();
  let timeout = setTimeout(() => {
    controller.abort();
  }, settings.abortRequestTimeOutInMs);

  let options = {
    method: method,
    headers: headers,
    body: body, 
    signal: controller.signal
  }

  if (settings.isProxyRequired) {
    options['agent'] = new HttpsProxyAgent(settings.ProxyURL);
  }

  if(settings.isSSLConfigure){
    let param = {
      rejectUnauthorized: false
    };
    options['agent'] = url.includes('https') ? new https.Agent(param) : new http.Agent(param);
  }
  
  return fetch(url, options)
  .then(async res => {
      let bodyContent = await res.text();
      duration = new Date().getTime() - startTime;
      res['respTime'] = duration;
      return {success:res.ok, error:false, res: res, resBody:bodyContent};
  })
  .then(fullResponse => {
      return fullResponse;
  })
  .catch(err =>{
    return {success:false, error:true, res: err, err:err};
  }).finally(() => {
		clearTimeout(timeout);
  });
}

logSendTimer = 0;
function sendAVMTestResult(){
  try{
    while (collectorQueue.getLength()>0 && !errorOnSend){
      let resSet = JSON.parse(collectorQueue.dequeue());
      if (settings.debugMode){
        logger.info("Current Collector Queue Length "+collectorQueue.getLength()+" AVM test id "+resSet.avmTestId);
      } else {
        if (logSendTimer%60 == 0){
          logger.info("Current Collector Queue Length "+collectorQueue.getLength()+" AVM test id "+resSet.avmTestId);
        }
      }
      let param = {testResult: resSet};
      let sendTestDomain = AVMConfigProperties.collectorService;
      fetch(sendTestDomain+'/avmAgentData', { 
          method: 'POST', 
          body: JSON.stringify(param),
          headers: { 'Content-Type': 'application/json' },
        })
        .then(res => {
            let bodyContent = res.json();
            return {success:res.ok,error:false, res: res, resBody:bodyContent};
        })
        .then(fullResponse => {
          if (!sendSuccess){
            sendSuccess = true;
            logger.info("Collector service is up and running, AVM Test will be processed.");
          }
          totalExecutedTestCnt++;
          lastResponseSentAt = new Date().getTime();
          if (settings.debugMode){
            if (fullResponse.success)
              logger.info(resSet.avmTestId +" successfully sent to server "+ JSON.stringify(fullResponse));
          }
          if (!fullResponse.success){
              logger.error(resSet.avmTestId +" Failed to process the request by server "+ JSON.stringify(fullResponse));
          }
        })
        .catch(err =>{
          sendSuccess = false;
          errorOnSend = true;
          lastErrorAt = new Date().getTime();
          //send log every minute (this req is tried every 10 sec)
          if (logSendTimer%6){
            logger.error("Collector is down, AVM Process will be stopped until collector is up and running, system will try every 10 sec."+err);
          }
          collectorQueue.enqueue(JSON.stringify(resSet));
        })
  
    }
    if(logSendTimer > 1000000) logSendTimer == 1;
    logSendTimer++;
    errorOnSend = false;
  } catch (e) {
    logger.error(e.message+" stack " + e.stack);
  }
}
