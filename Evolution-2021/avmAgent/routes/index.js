const services = require('./avmAgent');

module.exports = (app) => {
  app.use('/avmagent', services);
}
