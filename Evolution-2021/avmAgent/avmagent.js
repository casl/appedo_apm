var express = require('express');
const mountRoutes = require('./routes');
const path = require('path');
const bodyParser = require('body-parser');
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());
mountRoutes(app);

console.log("AVM Agent Started");