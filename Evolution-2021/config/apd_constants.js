const crypto = require('crypto').randomBytes(32).toString('hex');
// const crypto = require('crypto').randomBytes(32);
const keyEncryptDecrypt =  '9r55hnl26wwmfm2397@kl$hjkazgpk^w';
const Crypt = require('crypto');
// const IV_LENGTH = 16; // For AES, this is always 16
// let iv = Crypt.randomBytes(IV_LENGTH);
// console.log("iv",iv);
// console.log("crypto", crypto);

module.exports = {
    privateKey: crypto,
    dbPwdPvtKey:'$2a$31$BQOhDZT/Ppu69lJNNiIRee',
    keyEncryptDecrypt:'9r55hnl26wwmfm2397@kl$hjkazgpk^w',
    //resourcePath: 'c:/Appedo/',
    resourcePath: '/mnt/appedo/',
    attachementPath: 'resource/csv/',
    emailTemplatePath: 'resource/email_templates/',
    downloads: 'resource/downloads/',
    seleniumScriptClassFilePath: 'resource/sum/',
    jmeterscenariospath: 'resource/scenarios/jmeter/',
    devEnvironment: true,
    // devEnvironment: false,
    pgDbConfig:{
            user: 'postgres',
            database: 'apppedo_apm',
            password: 'sa',
            host: 'localhost',
            port: 5432,
            max: 100, // max number of clients in the pool
            idleTimeoutMillis: 300000
        },
    smtpConfig:{
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: '',
            pass: ''
        },
    },
    swuConfig:{
        SWU_API_KEY: '',
        SWU_SENDER_NAME: '',
        swu_sender_emailid: '',
        swu_reply_to: '',
        SWU_ESP_ACCOUNT: ''
    },
    // pgV12DbConfig:{
    //     user: 'postgres', 
    //     database: 'appedo_apm', 
    //     password: 'sa', 
    //     host: 'localhost', 
    //     port: 5433, 
    //     max: 100, // max number of clients in the pool
    //     idleTimeoutMillis: 300000
    // },
    Currency: 'rs',
    ReportSchedulerRunningMode: false,
    NumberFormat: 'crore', //supported values are Crore, Million, thousands if not given will take thousands as default
    tokenExpiresIn: '30m', 
    tokenExpInSec: 30*60, //converted above value in sec, this is important for calculating refresh token calculation
    collRefreshToken : [],
    avmSchedulerDebug: false,
    avmSchedulerRunningMode: false,
    noLocationDBUpdate:true,
    mailing_system: 'SENDWITHUS' // set the mail system like SMTP ot SENDWITHUS.
}
const IV_LENGTH = 16; // For AES, this is always 16

function encrypt(text) {
    let iv = Crypt.randomBytes(IV_LENGTH);
    let cipher = Crypt.createCipheriv('aes-256-cbc', Buffer.from(keyEncryptDecrypt), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
    let textParts = text.split(':');
    let iv = Buffer.from(textParts.shift(), 'hex');
    let encryptedText = Buffer.from(textParts.join(":"), 'hex');
    let decipher = Crypt.createDecipheriv('aes-256-cbc', Buffer.from(keyEncryptDecrypt), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

module.exports.encrypt = encrypt;
// async function encrypt(val) {
//     const cipher = Crypt.createCipher('aes192', keyEncryptDecrypt);
//     // const cipher = Crypt.createCipheriv('aes-256-cbc', keyEncryptDecrypt, iv);
//     let encrypted = cipher.update(val, 'utf8', 'hex');
//     encrypted += cipher.final('hex');
//     return encrypted;
// }

module.exports.decrypt = decrypt;
// async function decrypt(val) {
//     const decipher = Crypt.createDecipher('aes192', keyEncryptDecrypt);
//     // const decipher = Crypt.createDecipheriv('aes-256-cbc', keyEncryptDecrypt, iv);
//     let decrypt = decipher.update(val, 'hex', 'utf8');
//     decrypt += decipher.final('utf8');
//     return decrypt;
// }


