const Router = require('express-promise-router')
const PgConfig = require('../config/apd_constants');
const psqlAPM = require('./psqlAPM');
const fs = require('fs');
const { URLSearchParams } = require('url');
const parseString = require("xml2js").parseString;
const xml2js = require("xml2js");
const fetch = require('node-fetch');
const appedoMailer = require('./appedoMailer');
var path = require("path");

//global.logger = require('../log');
const log = require('../log');
var logger = log.moduleServiceLogger;

let appedoConfigProperties = {};
let JMETERSCENARIOSPATH = PgConfig.resourcePath+PgConfig.jmeterscenariospath;

// it allows you to use async functions as route handlers
const router = new Router();
// export our router to be mounted by the parent application
module.exports = router;
module.exports.sendVerifyMail = sendVerifyMail;
module.exports.sendResetPasswordMail = sendResetPasswordMail;
module.exports.sendAlertVerifyMail = sendAlertVerifyMail;
module.exports.loadConfigProp = loadConfigProp;
module.exports.runScenario = runScenario;
module.exports.uploadJmeterScript = uploadJmeterScript;
module.exports.readRegions = readRegions;
module.exports.getJMeterScriptSettings = getJMeterScriptSettings;
module.exports.updateJMeterScenarioSettings = updateJMeterScenarioSettings;
module.exports.deleteJMeterScenarios = deleteJMeterScenarios;

async function loadConfigProp(configData){
    try{
        appedoConfigProperties = configData;
    }catch{
        logger.error('loadConfigProp : '+e.stack);
    }
}

async function sendVerifyMail(param){
    let response = {};
    try{
        let mailDetails = {};
        let mailSubject;
        if(param.operation == 'signUpVerificationMail'){
            mailDetails.USERNAME = param.first_name;
            mailDetails.LINK = param.link;
            mailSubject = '{{ appln_heading }}: Email-Id Verification';
            appedoMailer.sendMail(appedoMailer.MODULE_ID.VERIFY_USER_EMAIL, mailDetails, param.email_id, mailSubject);
            let message = "Dear "+param.first_name+" "+param.last_name+", Thanks for registering with Appedo. Check your inbox for more details. ";
            await appedoMailer.sendSMS(param.mobile_no, message);
        }else if (param.operation == 'EnterpriseInviteMail'){
            if(param.isNewUser){
                mailDetails.USERNAME = param.first_name;
                mailDetails.fullEntName = param.enterprise_name;
                mailDetails.email_id = param.email_id;
                mailDetails.password = param.password;
                mailDetails.entOwnerEmailId = param.entOwner_emailId;
                mailDetails.LINK = param.link;
                mailSubject = '{{ appln_heading }} Enterprise Invitation';
                appedoMailer.sendMail(appedoMailer.MODULE_ID.ENTERPRISE_SIGNUP_INVITATION, mailDetails, param.email_id, mailSubject);
            }else{
                mailDetails.USERNAME = param.first_name;
                mailDetails.fullEntName = param.enterprise_name;
                mailDetails.entOwnerEmailId = param.entOwner_emailId;
                mailDetails.APPEDO_URL = param.link;
                mailDetails.LINK = param.link;
                mailSubject = '{{ appln_heading }} Enterprise Invitation';
                appedoMailer.sendMail(appedoMailer.MODULE_ID.ENTERPRISE_INVITATION, mailDetails, param.email_id, mailSubject);
            }
        }
        response.success = true;
        response.failure = false;
        response.message = 'Verification mail successfully sent.';
    }catch(e){
        logger.error('sendVerifyMail : '+e.stack);
        response.success = false;
        response.failure = true;
        response.message = e.stack;
    }finally{
        return response;
    }
}

async function sendResetPasswordMail(param){
    let response = {};
    try{
        let mailDetails = {};
        mailDetails.USERNAME = param.firstName;
        mailDetails.LINK = param.link;
        mailDetails.EMAIL = param.emailId;
        let mailSubject = param.subject;
        appedoMailer.sendMail(appedoMailer.MODULE_ID.PASSWORD_RESET, mailDetails, param.emailId, mailSubject);
        response.success = true;
        response.failure = false;
        response.message = 'ForgotPassword mail successfully sent.';
    }catch(e){
        logger.error('sendVerifyMail : '+e.stack);
        response.success = false;
        response.failure = true;
        response.message = e.stack;
    }finally{
        return response;
    }
}

async function sendAlertVerifyMail(param){
    let response = {};
    try{
        let mailDetails = {};
        let mailSubject;
        if(param.type.toLowerCase().includes('email')){
            mailDetails.USERNAME = param.userFirstName;
            mailDetails.TYPE = 'SLA';
            mailDetails.LINK = param.link;
            mailDetails.IS_NON_LOGIN_USER = param.IS_NON_LOGIN_USER;
            mailSubject = '{{ appln_heading }} : Accept SLA Alerts';
            appedoMailer.sendMail(appedoMailer.MODULE_ID.SLA_VERIFY_EMAIL, mailDetails, param.emailornumber, mailSubject);
            response.success = true;
            response.failure = false;
            response.message = 'Alert verfication mail successfully sent.';
        }
    }catch(e){
        logger.error('sendVerifyMail : '+e.stack);
        response.success = false;
        response.failure = true;
        response.message = e.stack;
    }finally{
        return response;
    }
}

async function getSystemId(req, res){
    try{
        let apd_uuid = req.body.systemGeneratorUUID;
        let queryText = 'SELECT system_id FROM server_information WHERE apd_uuid = $1';
        let queryParam = [apd_uuid];
        let result = await psqlAPM.fnDbQuery('getSystemId-LinuxUnification',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            res.json({success:true, failure:false, message: "successfully getting System Id", systemId:result.rows[0].system_id});
        }else{
            res.json({success:false, failure:true, message: "System Id could not be found in ServerInformation table, Contact System Admin."});
        }
    }catch(e){
        logger.error('getSystemId : '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}
async function getWinSystemId(req, res){
    try{
		let system_uuid = req.body.systemGeneratorUUID;
		let queryText = 'SELECT system_id FROM server_information WHERE system_uuid = $1';
		let queryParam = [system_uuid];
		let result = await psqlAPM.fnDbQuery('getWinSystemId-LinuxUnification',queryText, queryParam, null, null);
		if(result.rowCount > 0){
			res.json({success:true, failure:false, message: "successfully getting System Id", systemId:result.rows[0].system_id});
		}else{
			res.json({success:false, failure:true, message: "System Id could not be found in ServerInformation table, Contact System Admin."});
		}
	}catch(e){
		logger.error('getWinSystemId : '+e.stack);
		res.json({success:false, failure:true, message: e.stack});
	}
}
async function getModuleStatus(req, res){
    try{
        let GUID = req.body.GUID;
        let queryText = 'SELECT user_status FROM module_master WHERE guid = $1';
        let queryParam = [GUID];
        let result = await psqlAPM.fnDbQuery('getModuleStatus-LinuxUnification',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            let status = result.rows[0].user_status;
            if(status.toLowerCase() == 'restart'){
                queryText = 'UPDATE module_master SET user_status = $1 WHERE guid = $2';
                queryParam = ['Running', GUID];
                let result = await psqlAPM.fnDbQuery('updateModuleStatus-LinuxUnification',queryText, queryParam, null, null);
            }
            res.json({success:true, failure:false, message: status});
        }else{
            res.json({success:true, failure:false, message: "Kill"});
        }
    }catch(e){
        logger.error('getModuleStatus : '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}

async function createLinuxModule(req, res, data){
    let cmd;
    let queryText;
    let queryParam;
    let msg={};
    if(data.moduleType=="SERVER"){
        cmd="cmd";
        queryText = 'INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, created_by, created_on, e_id, client_unique_id, system_id, module_type, type_version, user_status) VALUES ($1, uuid_generate_v4(), $2, $3, $4, $5, $6, now(), $7, $8, $9, $10, $11, $12) RETURNING uid, guid';
        queryParam = [data.userId, data.moduleType, 0, data.moduleName, data.moduleName, data.userId, data.enterprise_Id, data.UUID, data.systemId, data.moduleTypeName, data.VERSION_ID, 'Running'];
    } else if(data.moduleType=="DATABASE"){
        cmd="query";
        queryText = 'INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, created_by, created_on, e_id, client_unique_id, system_id, module_type, type_version, user_status, application_context_name, jmx_port) VALUES ($1, uuid_generate_v4(), $2, $3, $4, $5, $6, now(), $7, $8, $9, $10, $11, $12, $13, $14) RETURNING uid, guid';
        queryParam = [data.userId, data.moduleType, 0, data.moduleName, data.moduleDescription, data.userId, data.enterprise_Id, data.UUID, data.systemId, data.moduleTypeName, data.VERSION_ID, 'Running', data.moduleName, null];
    }else{
        cmd="jvm";
        queryText = 'INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, created_by, created_on, e_id, client_unique_id, system_id, module_type, type_version, user_status, application_context_name, jmx_port) VALUES ($1, uuid_generate_v4(), $2, $3, $4, $5, $6, now(), $7, $8, $9, $10, $11, $12, $13, $14) RETURNING uid, guid';
        queryParam = [data.userId, data.moduleType, 0, data.moduleName+":"+data.jmx_port, data.moduleDescription, data.userId, data.enterprise_Id, data.UUID, data.systemId, data.moduleTypeName, data.VERSION_ID, 'Running', data.moduleName, data.jmx_port];
    }
    try{
        let result = await psqlAPM.fnDbQuery('createLinuxModule-LinuxUnification',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            msg = {
                message : 'successfully created linux module.',
                moduleGUID : result.rows[0].guid,
                moduleUID: result.rows[0].uid,
                moduleTypeName : data.moduleTypeName
            }
            let uid = result.rows[0].uid;
            let guid = result.rows[0].guid;
            logger.info('Uid : '+uid+' GUID : '+guid);
            cmd = data.moduleTypeName.toLowerCase().includes('apache') ? 'http' : cmd;
            queryText = 'SELECT create_counter_table_partitionsV1($1, $2::json, $3, $4, $5, $6) ';
            //Database module will not get any Counter set/ list of counters and this parameter will be empty
            queryParam = [uid, Object.keys(data.joCounterSet).length > 0 ? JSON.stringify(data.joCounterSet.counterData):JSON.stringify([]), data.userId, guid, cmd, data.moduleTypeName];
            let resultCM = await psqlAPM.fnDbQuery('insCounterMasterTable-LinuxUnification',queryText, queryParam, null, null);
            if (resultCM.error){
                msg= {
                    moduleGUID : result.rows[0].guid,
                    moduleUID: result.rows[0].uid,
                    moduleTypeName : data.moduleTypeName,
                    message: "created linux module - error in creating counter table partition: " + resultCM.message
		        }
            }
     
            queryText = 'SELECT * from insert_chart_visual_data($1, $2, $3, $4) ';
            queryParam = [uid, data.userId, data.moduleType, data.moduleName];
            let resultChart = await psqlAPM.fnDbQuery('insChartVisTable-LinuxUnification',queryText, queryParam, null, null);
            if (resultChart.error){
                msg = {
                    moduleGUID : result.rows[0].guid,
                    moduleUID: result.rows[0].uid,
                    moduleTypeName : data.moduleTypeName,
                    message: "created linux module - error in inserting data for chart visual: " + resultChart.message
                }
            }
        res.json({success:true, failure:false, message: msg});
        } else{
           res.json({success:false, failure:true, message: "Unable to insert Module Master table due to : "+result.message});
        }
    }   catch(e){
        logger.error('Error in createLinuxModule: '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}

async function getSysInfoFromAPDUUID(req, res, param){
    try{
        let apd_uuid = req.body.UUID;
        let queryText = 'select system_name, system_number, system_id, user_id from server_information WHERE apd_uuid = $1';
        let queryParam = [apd_uuid];
        let result = await psqlAPM.fnDbQuery('getSysInfoFromAPDUUID',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            param.moduleName = result.rows[0].system_name+'_'+param.moduleTypeName;
            param.moduleDescription = result.rows[0].system_name+'_'+param.moduleTypeName;
            param.userId = result.rows[0].user_id;
        }else{
         res.json({success:false, failure:true, message: "System information could not be found in ServerInformation table, Contact System Admin."});
        }
    }catch(e){
        logger.error('Error in getSysInfoFromAPDUUID: '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}

async function getServerInfo(uuid){
  try{
        let joServerDetails = {};
	let queryText = "SELECT user_id, system_id FROM server_information WHERE apd_uuid = '"+uuid+"'";
        let queryParam = [];
        let result = await psqlAPM.fnDbQuery('getServerInfo',queryText, queryParam, null, null);
        if(result.rowCount > 0){
           joServerDetails.isExistsUUID = true;
           joServerDetails.user_id = result.rows[0].user_id;
           joServerDetails.system_id = result.rows[0].system_id;
        }else{
	   joServerDetails.isExistsUUID = false;
        }
       return joServerDetails;
     }catch(e){
	    logger.error('Error in getServerInfo: '+e.stack);
        e.message='Error in getServerInfo:'+e.stack;
        throw e;
     }
}
async function getWinServerInfo(uuid){
	try{
		let joServerDetails = {};
		let queryText = "SELECT user_id, system_id FROM server_information WHERE system_uuid = '"+uuid+"'";
		let queryParam = [];
		let result = await psqlAPM.fnDbQuery('getWinServerInfo',queryText, queryParam, null, null);
		if(result.rowCount > 0){
			joServerDetails.isExistsUUID = true;
			joServerDetails.user_id = result.rows[0].user_id;
			joServerDetails.system_id = result.rows[0].system_id;
		}else{
		joServerDetails.isExistsUUID = false;
		}
		return joServerDetails;
		}catch(e){
		   logger.error('Error in getWinServerInfo: '+e.stack);
		   e.message='Error in getWinServerInfo:'+e.stack;
throw e; 
	}
}	   
async function createServerModule(req, res){
    try{
        let moduleTypeName;
        let serverInformation = JSON.parse(req.body.moduleInformation);
        
        if(serverInformation.PRETTY_NAME.toLowerCase().includes("amazon")){
            moduleTypeName = 'AMAZON_LINUX';
        }else if(serverInformation.PRETTY_NAME.toLowerCase().includes("centos")){
            moduleTypeName = 'CentOS';
        }else if(serverInformation.PRETTY_NAME.toLowerCase().includes("red hat")){
            moduleTypeName = 'RedHat';
        }else if(serverInformation.PRETTY_NAME.toLowerCase().includes("fedora")){
            moduleTypeName = 'Fedora';
        }else if(serverInformation.PRETTY_NAME.toLowerCase().includes("ubuntu")){
            moduleTypeName = 'Ubuntu';
        }else if(serverInformation.PRETTY_NAME.toLowerCase().includes("solaris")){
            moduleTypeName = 'Solaris';
        }

        let param = {
            joCounterSet : JSON.parse(req.body.jocounterSet),
            systemId : req.body.systemId,
            UUID : req.body.UUID,
            enterprise_Id : req.body.Enterprise_Id,
            moduleTypeName : moduleTypeName,
            moduleVersion : serverInformation.VERSION_ID,
            moduleType : 'SERVER'
        }
        let queryText = 'SELECT uid, guid from module_master WHERE system_id = $1 AND lower(module_type) = lower($2)';
        let queryParam = [param.systemId,param.moduleTypeName];
        let result = await psqlAPM.fnDbQuery('createServerModule-LinuxUnification',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            let msg = {
                message : 'module already exists',
                moduleGUID : result.rows[0].guid,
                moduleTypeName : moduleTypeName
            };
            res.json({success:true, failure:false, message: msg});
        }else{
           
            //need some clarfication from SK.
            await getSysInfoFromAPDUUID(req, res, param);
            await createLinuxModule(req, res, param);
        }
    }catch(e){
        logger.error('Error in createServerModule: '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}

async function linuxUnification(req, res){
    try{
        let command = req.body.command;
       switch(command){
            case 'systemGeneratorInfo':
				getSystemId(req, res);
				break;
			case 'winsystemGeneratorInfo':
				getWinSystemId(req, res);
				break;
			case 'moduleRunningStatus':
				getModuleStatus(req, res);
				break;
			case 'serverInformation':
				createServerModule(req, res);
				break;
			case 'appInformation':
				createApplicationModule(req,res);
				break;
			case 'winappInformation':
				createWinApplicationModule(req,res);
				break;
			case 'DBInformation':
                createDatabaseModule(req,res);
                break;
            case 'UnifiedAgentFirstRequest':
                getConfigGUID(req,res);
            case 'getUIDStatus':
                getAgentStatus(req,res);
            case 'NETSTACK':
                getProfilerConfigGUID(req,res);
            default:
                res.json({success:false, failure:false, message: "sorry, could not create linux unified module due to invalid input for 'command' type.."});
                break;
        }
    }catch(e){
        logger.error('Error in linuxUnification: '+e.stack);
        res.json({success:false, failure:true, message: e.stack});
    }
}

async function getConfigGUID(req,res){
    try{
        let param ={
            strUUID: req.body.uuid,
        }
        let struuid = param.strUUID;
        let joGUID, strResponseMsg, struid;
        let hmKeyValues = hmResponse = joSystemData = joAgentData = {};
        let jaCounters = [];
        joSystemData = await getSystemDetails(struuid);
        if(joSystemData.system_id == -1)
            hmKeyValues.message = "System information not available, Contact administrator";
        else{
            joGUID = {};
            joGUID.guid = "";
            let systemid = joSystemData.system_id;
            hmKeyValues.WINDOWS = joGUID;
            hmKeyValues.MSSQL = joGUID;
            hmKeyValues.MSIIS = joGUID;
            let hmKeyValues = await getModuleInfo(struuid, systemid, hmKeyValues);
            hmResponse = hmKeyValues;
            let hmKeys = Object.keys(hmKeyValues);
            for(i=0; i< hmkeys.length; i++){
                key = hmKeys[i]; 
                let joVal = hmKeyValues[key];
                if (joVal.hasOwnProperty("uid")){
                    struid = joVal.uid;                    
                    jaCounters = await getConfigCounters(struid);
                    joVal.counters = jaCounters;
                    let strguid = joVal.guid;
                    jaSlaCounters = await getSlaConfigCountersV2(strguid);
                    joVal.SLA = jaSlaCounters;
                    hmKeyValues.key=joVal;
                } else{
                    strResponseMsg = await agentProgress(struuid, key);
                    joVal.Message = strResponseMsg;
                    hmKeyValues.key = joVal;
                }
            }
        }
        res.json({success:true, error:false, hmKeyValues:hmKeyValues})
    }catch(e){
        logger.error('Error in getConfigGUID: '+e.stack);
        res.json({success:false, error:true, message: "There was an error in getConfigGUID method: " + e.stack});
    }    
}

async function agentProgress(struuid, key){
    try{
        let joAgentData = await getAgentProgressStatus(struuid,key);
        if (joAgentData!=null){
            if(joAgentData.InProgress == true){
                if (joAgentMessage.Message.length > 0){
                    strResponseMsg = joAgentMessage.Message;
                    await deleteAgentProgressJson(struuid, key);
                }else{
                    strResponseMsg = "Inprogress";
                }
            }
        }
        return strResponseMsg;
    }catch(e){
        e.message = "There was an error in getting agent progress status: " + e.stack;
        logger.debug("There was an error in agentProgress method: " + e.stack);
        throw e;
    }
}

async function deleteAgentProgressJson(struuid, strmodulename){
    try{
        qryText = "UPDATE server_information SET agentinprogress = agentinprogress::jsonb - '"+strmodulename+"' WHERE system_uuid = '" + struuid +"'";
        qryParam = [];
        let result = await psqlAPM.fnDbQuery('deleteAgentProgressJson',qryText, qryParam, null, null);
    } catch(e){
        e.message = "There was an error in deleting agent progress json: " + e.stack;
        logger.debug("There was an error in deleteAgentProgressJson method: " + e.stack);
        throw e;
    }
}

async function getAgentProgressStatus(struuid, strmodulename){
    try{
        let qryText = "select AgentInprogress -> '" + strmodulename+ "' ->> 'InProgress' AS InProgress, AgentInprogress ->'"+strmodulename+"'->>'Message' AS Message from server_information WHERE system_uuid = $1";
        let qryParam = [struuid];
        let result = await psqlAPM.fnDbQuery('agentProgressStatus',qryText, qryParam, null, null);
        if(result.next()){
            joAgentData = {};
            joAgentData.InProgress = result.InProgress;
            joAgentData.Message = result.Message;
        }
        return joAgentData;
    } catch(e){
        e.message = "There was an error in getting agent progress status: " + e.stack;
        logger.debug("There was an error in agentProgressStatus method: " + e.stack);
        throw e;
    }
}

async function getAgentStatus(req,res){
    try{
       let param = {
           strGuid: req.body.strGUID
       }
       let strguid = param.strGuid, struid;
       let hmKeyValues = {};
       let jaCounters = jaSlaCounters = [];
       let joModuleStatus = joModuleCounters = {};
       let qryText;
       let qryParam=[];
       qryText = "SELECT user_status, uid, module_type FROM module_master WHERE guid = $1";
       qryParam = [strguid];
       let result = await psqlAPM.fnDbQuery('getAgentStatus',qryText, qryParam, null, null);
       if (result.next()){
            joModuleStatus.user_status = result.user_status;
            joModuleStatus.uid = struid = result.uid;
            joModuleStatus.module_type=result.module_type;
       }else{
           joModuleStatus.user_status = "deleted";
       }
       joModuleCounters.message = joModuleStatus.user_status;
       if (joModuleStatus.user_status == "restart" && joModuleStatus.module_type != "NETSTACK"){
            jaSlaCounters = [];
            jaCounters = getConfigCounters(struid);
            joModuleCounters.counters = jaCounters;
            jaSlaCounters = getSlaConfigCountersV2(strguid);
            joModuleCounters.SLA = jaSlaCounters;
       }
       if(joModuleStatus.user_status == "deleted"){
           hmKeyValues.message = joModuleStatus.user_status;
       } else{
           hmKeyValues.module_type = joModuleCounters;
       }
       if (joModuleStatus.user_status == 'restart'){
          await updateModuleStatus(strguid);
       }
       res.json({success:true, failure:false, message: "response of " + strguid + "status: " + hmKeyValues});
    }catch(e){
        logger.debug("Error in getAgentStatus method: " + e.message);
        res.json({success:false, failure:true, message: "unable to get agent status: " + e.stack});
    }
}

async function updateModuleStatus(strguid){
    try{
        qryText = "UPDATE module_master SET user_status = 'Running' WHERE guid = $1";
        qryParam = [strguid];
        await psqlAPM.fnDbQuery('updateModuleStatus',qryText, qryParam, null, null);
    } catch(e){
        e.message = "There was an error in updating module status: " + e.stack;
        logger.debug("Error in updateModuleStatus method: " + e.message);
        throw e;
    }
}

async function getProfilerConfigGUID(req,res){
    try{
        let param ={
            strEID: req.body.eid,
            strUUID: req.body.uuid,
        }
        let struuid = param.strUUID;
        let strGuid, strResponse;
        let joSysData = await getSystemDetails(struuid);
        let sys_id = joSysData.system_id;
        if(sys_id == -1) {
            strResponse = "System information not available, Contact administrator";
        }else{
            strGuid = await getModuleGUID(sys_id, struuid);
            if(strGuid == ""){
                strResponse = await addModules(param, joSysData);
            }else{
                strResponse = strGuid;
            }
        }
        res.json({success:true, failure:false, message:strResponse });
    } catch(e){
        logger.debug("Error in getProfilerConfigGUID method: " + e.message);
        res.json({success:false, failure:true, message: "unable to get profiler info: " + e.stack});
    }
}

async function getModuleGUID(sys_id, struuid){
     try{
        let strGuid;
        let qryText = "SELECT guid from module_master WHERE system_id = $1 AND client_unique_id = $2 AND module_code = 'NETSTACK'";
        let qryParam = [sys_id,struuid];
        let result = await psqlAPM.fnDbQuery('getModuleGUID',qryText, qryParam, null, null);
        if (result.next()){
            strGuid = result.guid;
        }
        return strGuid;
    } catch(e){
        e.message = "There was in error while getting module GUID-getModuleGUID method: "+e.stack;
        logger.debug("Error in getModuleGUID method: " + e.message);
        throw e;
    }
}

async function addModules(param, joSysData){
    try{
        let struuid = param.strUUID;
        let josystemdata = joSysData;
        let systemid = josystemdata.system_id;
        let userid = josystemdata.user_id;
        let uid, guid,strGuid;
        let moduledesc = JSON.parse(josystemdata).system_name + "_NETSTACK_" + JSON.parse(josystemdata).system_number;
        let eid = (param.strEID != null && param.strEID != "") ? param.strEID:null;
        let qryText = "INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, created_by, created_on, e_id, client_unique_id, system_id, module_type, user_status ) VALUES ($1, uuid_generate_v4(), $2, $3, $4, $5, $6, now(), $7, $8, $9, $10, $11) RETURNING uid, guid";
        let qryParam=[userid, "NETSTACK", 0, moduledesc, moduledesc, userid, eid, struuid, systemid, "NETSTACK", "Running"];
        let result = await psqlAPM.fnDbQuery('addModules-insert',qryText, qryParam, null, null);
        if (result.next()){
            uid=result.uid;
            guid=result.guid;
        }
        qryText = "SELECT * FROM add_net_stack_collector_table($1,$2)";
        qryParam = [uid,userid];
        result = await psqlAPM.fnDbQuery('addModules-add_net_stack_collector_table',qryText, qryParam, null, null);
        strGuid = guid;
        return strGuid;        
    }catch(e){
        e.message = "unable to get add module: " + e.stack;
        logger.debug("Error in addModules method: " + e.message);
        throw e;
    }
}

async function getSlaConfigCountersV2(strGuid){
    try{
        let hmGuidMappedToSlas = {};
        let joSlaCounter;
        let qryText;
        let qryParam = [];
        let alSlas = [];
        qryText = "SELECT ssc.sla_id, ss.user_id, ssc.counter_id, ssc.counter_template_id, ssc.guid, ssc.is_above_threshold, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.percentage_calculation, ssc.denominator_counter_id, ssc.process_name FROM so_sla_counter ssc INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id AND ssc.guid=$1 AND ss.is_active = true";
        qryParam=[strGuid];
        let result = await psqlAPM.fnDbQuery('getSlaConfigCountersV2',qryText, qryParam, null, null);
        while(result.next()){
            joSlaCounter = {};
            strGuid = result.guid;
            joSlaCounter.sla_id = result.sla_id;
            joSlaCounter.user_id = result.user_id;
            joSlaCounter.counter_id  = result.counter_id;
            joSlaCounter.counter_template_id = result.counter_template_id;
            joSlaCounter.is_above = result.is_above_threshold;
            joSlaCounter.warning_threshold_value = result.warning_threshold_value;
            joSlaCounter.critical_threshold_value = result.critical_threshold_value;
            joSlaCounter.percentage_calculation = result.percentage_calculation;
            joSlaCounter.process_name = result.process_name != null? result.process_name:"";
            if(result.denominator_counter_id != null)
                joSlaCounter.denominator_counter_id = result.denominator_counter_id;
            if (hmGuidMappedToSlas.hasOwnProperty('strGuid'))
                alSlas = hmGuidMappedToSlas.strGuid;
            else{
                alSlas = [];
                hmGuidMappedToSlas.strGuid = alSlas;
            }
            alSlas.push(joSlaCounter);
        }
        return hmGuidMappedToSlas;
    } catch(e){
        e.message = "unable to get configured sla counters: " + e.stack;
        logger.debug("Error in getSlaConfigCountersV2 method: " + e.message);
        throw e;
    }
}

async function collectAgentMessage(req, res){
    try{
        let isGuidExists = false;
        let hmResponse = {};
        let joResponse = {};
        //let joCounterParams = req.
        let oldGuid;
        if(!joCounterParams.hasOwnProperty('oldGuid') && joCounterParams.oldGuid == ''){
            insertAndCreateModulesV2(joCounterParams);
        } else{
            oldGuid = joCounterParams.oldGuid;            
            isGuidExists = isGuidExist(oldGuid);
            if(isGuidExists){
                joResponse = migrationCounterModulesV2(joCounterParams);
            }else{
                joResponse = insertAndCreateModulesV2(joCounterParams);
            }
        }
        let strmoduletype = joCounterParams.command;
        hmResponse = {
            [strmoduletype]:joResponse
        }
        res.send({success:true, failure:false,hmResponse:hmResponse});
    } catch(e){
        e.message = "could not collect agent message: " + e.stack;
        logger.debug("Error in collectAgentMessage method: " + e.stack);
        res.send({success:false, error:true, message:"Error in collectAgentMessage method"});
    }
}

async function migrationCounterModulesV2(joModuleData){
    try{
        let param = {
            uuid: joModuleData.uuid,
            clientuniqueid: joModuleData.uuid,
            oldguid: joModuleData.oldGuid,
            guid: joModuleData.oldGuid,
            moduletypename: joModuleData.moduleTypeName,
            moduleversion: joModuleData.moduleVersion,
            jocounterset: joModuleData.JocounterSet
        }
        let strmodulename = param.strModuleName = joModuleData.command;
        let uuid =  client_unique_id  = param.uuid;
        let systemid = param.systemid = await getSysId(uuid);
        let jocounterset = param.joCounterSet;
        await migrationModuleCounters(param);
        let joRequest = fetchConfigCounters(param);
        return joRequest;
    }catch(e){
        e.message = "unable to add migration counter modules " + e.stack;
        logger.debug("Error in migrationCounterModulesV2 method: " + e.message);
        throw e;
    }
}

async function fetchConfigCounters(param){
    try{
        let jaSLACounters = [], jaCounters = [];
        let joCounterSet = {};
        jaCounters = await getConfigCounters(param.moduleid);
        jaSlaCounters = await getSlaConfigCounters(param.moduleid);
        joCounterSet.guid = param.guid;
        joCounterSet.counters = jaCounters;
        joCounterSet.SLA = jaSlaCounters;
        return joCounterSet;
    }catch(e){
        e.message = "unable to fetch config counters: " + e.stack;
        logger.debug("Error in fetchConfigCounters method: " + e.message);
        throw e;
    }
}

async function getSlaConfigCounters(uid){
    try{
        let joSLACounter = {};
        let jaSLACounter = [];
        let qryText = "SELECT ssc.sla_id, ss.user_id, ssc.counter_id, ssc.counter_template_id, ssc.guid, ssc.is_above_threshold, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.percentage_calculation, ssc.denominator_counter_id, ssc.process_name FROM so_sla_counter ssc INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id AND ssc.uid=$1 AND ss.is_active = true";
        let qryParam=[uid];
        let result = await psqlAPM.fnDbQuery('getSlaConfigCounters',qryText, qryParam, null, null);
        while(result.next()){
            joSLACounter = {};
            joSlaCounter.sla_id = result.sla_id;
            joSlaCounter.user_id = result.user_id;
            joSlaCounter.counter_id  = result.counter_id;
            joSlaCounter.counter_template_id = result.counter_template_id;
            joSlaCounter.is_above = result.is_above_threshold;
            joSlaCounter.warning_threshold_value = result.warning_threshold_value;
            joSlaCounter.critical_threshold_value = result.critical_threshold_value;
            joSlaCounter.percentage_calculation = result.percentage_calculation;
            joSlaCounter.process_name = result.process_name != null? result.process_name:"";
            if(result.denominator_counter_id != null)
                joSlaCounter.denominator_counter_id = result.denominator_counter_id;
            jaSLACounter.push(joSLACounter);
        }
        return jaSLACounter;
    } catch(e){
        e.message = "unable to get sla config counters: " + e.stack;
        logger.debug("Error in getSlaConfigCounters method: " + e.message);
        throw e; 
    }
}

async function migrationModuleCounters(param){
    try{
        await migrationASDModules(param);
        param = await getModuleData(param);
        await migrationModuleCounters(param);
        let strCounterIds = await getCounterIds(param);
        await updateChartData(param, strCounterIds);''
    }catch(e){
        e.message = "unable to add migration module counters: " + e.stack;
        logger.debug("Error in migrationModuleCounters method: " + e.message);
        throw e; 
    }
}

async function updateChartData(param, strcounterids){
    try{
        let userid = param.userid;
        let qryText = "SELECT * FROM update_counter_in_chart_visual($1,$2,$3,$4,$5)";
        let qryParam = [userid, param.moduleid, strcounterids, param.modulename, param.moduletype];
        await psqlAPM.fnDbQuery('updateChartData',qryText, qryParam, null, null);
    } catch(e){
        e.message = "unable to update chart data: " + e.stack;
        logger.debug("Error in updateChartData method: " + e.message);
        throw e;
    }
}

async function getCounterIds(param){
    try{
        let strCounterIds = "";
        let qryText = "SELECT string_agg(counter_id::text, ',') AS counters_ids FROM counter_master_"+param.moduleid+" WHERE is_selected = true";
        let qryParam = [];
        let result = await psqlAPM.fnDbQuery('getCounterIds',qryText, qryParam, null, null);
        if(result.next()){
            strCounterIds = result.counter_ids;
        }
        return strCounterIds
    }catch(e){
        e.message = "unable to get counter ids: " + e.stack;
        logger.debug("Error in getCounterIds method: " + e.message);
        throw e;
    }
}

async function migrationModuleCounters(param){
    try{
        let qryText = "select * from migration_counter_table_partitionsV2 (?, ?::json, ?, ?, ?)";
        let qryParam = [param.moduleid, JSON.parse(param.jocounterset).counterData, param.userid, param.oldguid, param.createdon];
        await psqlAPM.fnDbQuery('migrationModuleCounters',qryText, qryParam, null, null);
    }catch(e){
        e.message = "unable to get migration module counter data: " + e.stack;
        logger.debug("Error in migrationModuleCounters method: " + e.message);
        throw e;
    }
}

async function getModuleData(param){
    try{
        let strguid = param.oldguid;
        let qryText = "SELECT uid, user_id, created_on, module_name, module_code FROM module_master WHERE guid = '" + strguid + "'";
        let qryParam = [];
        let result = await psqlAPM.fnDbQuery('getModuleData',qryText, qryParam, null, null);
        while(result.next()){
            param.moduleid = result.uid;
            param.userid = result.user_id;
            param.createdon = result.created_on;
            param.modulename = result.module_name;
            param.moduletype = result.module_code;
        }
        return param;
    }catch(e){
        e.message = "unable to get module data: " + e.stack;
        logger.debug("Error in getModuleData method: " + e.message);
        throw e;
    }
}

async function migrationASDModules(param){
    try{
       let qryText = "UPDATE module_master SET client_unique_id = $1 , system_id = $2, module_type = $3, type_version = $4, user_status = 'Running' WHERE guid = $5";
        let qryParam = [param.clientuniqueid,param.systemid,param.moduletypename,param.moduleversion,param.oldguid];
        await psqlAPM.fnDbQuery('migrationASDModules',qryText, qryParam, null, null);
    }catch(e){
        e.message = "unable to update module master: " + e.stack;
        logger.debug("Error in migrationASDModules method: " + e.message);
        throw e;
    }
}

async function getSysId(struuid){
    try{
        let lSystemId = -1;
        let qryText = "SELECT system_id FROM server_information WHERE system_uuid = '"+struuid+"'";
        let qryParam = [];
        let result = await psqlAPM.fnDbQuery('getSysId',qryText, qryParam, null, null);
        while(result.next()){
            lSystemId = result.system_id;
        }
        return lSystemId;
    }catch(e){
        e.message = "unable to get system id: " + e.stack;
        logger.debug("Error in getSysId method: " + e.message);
        throw e;
    }
}

async function isGuidExist(oldGuid){
    try{
        let bGuidExists = false;
        let qryText = "SELECT EXISTS( SELECT guid from module_master WHERE guid = $1) AS guid_exists";
        let qryParam = [oldGuid];
        let result = await psqlAPM.fnDbQuery('isGuidExist',qryText, qryParam, null, null);
            if(result.next()){
                bGuidExists = result.guid_exists;
            }
        return bGuidExists
    } catch(e){
        e.message = "unable to check if guid exists: " + e.stack;
        logger.debug("Error in isGuidExist method: " + e.message);
        throw e;
    }
}

async function insertAndCreateModulesV2(joModuleData){
    let joSystemData = {};
    let strModuleName = jModuleData.command;
    let strUUID = joModuleData.uuid;
    let strModuleType;
    let joResponse = {};
    let lModuleId;
    try{
        joSystemData = getSystemData(strUUID, strModuleName, "WINDOWS");
        strModuleType = strModuleName=="WINDOWS" ? "SERVER" : (strModuleName=="MSSQL" ? "DATABASE" : "APPLICATION");
        let param = {
            moduleName: joSystemData.module_name,
            moduleTypeName: strModuleName,
            moduleDescription: joSystemData.module_name,
            moduleVersion: joModuleData.version,
            moduleType: strModuleType,
            clrVersion: joModuleData.clrVersion == null?'':joModuleData.clrVersion,
            joCounterSet: joModuleData.counterSet,
            strUUID: strUUID,
            systemId: joSystemData.system_id,
            userId: joSystemData.user_id,
            clientUniqueId: strUUID,
            agentVersionId: joModuleData.moduleVersion,
            e_id: joModuleData.eid,
            joSysData: joSystemData
        }
        lModuleId = getModuleId(param);
        if(lModuleId == -1){
            lModuleId = insertAPMModuleAndCreateUidTablePartitionsV2(param);
        }
        return lModuleId;
    } catch(e){
        e.message = "unable to insert and create apm module: " + e.stack;
        logger.debug("Error in insertAndCreateModulesV2 method: " + e.message);
        throw e;
    }
}

async function insertAPMModuleAndCreateUidTablePartitionsV2(param){
    try{
        let lModuleId = -1;
        let user_id = param.userId;
        let module_code = param.moduleType;
        let counter_type_version_id = param.clrVersion;
        let module_name = param.moduleName;
        let description = param.moduleDescription;
        let clr_version = param.clrVersion;
        let created_by = param.userId;
        let e_id = param.e_id;
        let client_unique_id = param.clientUniqueId;
        let system_id = param.systemId;
        let module_type = param.moduleTypeName;
        let type_version = param.moduleVersion;
        let qryText = "INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, clr_version, created_by, created_on, e_id, client_unique_id, system_id, module_type, type_version, user_status) VALUES ($1, uuid_generate_v4(), $2, $3, $4, $5, $6, $7, now(), $8, $9, $10, $11, $12, 'Running') returning uid";
        let qryParam = [user_id,module_code,counter_type_version_id,module_name,description,clr_version,created_by,e_id,client_unique_id,system_id,module_type,type_version];
        let result = await psqlAPM.fnDbQuery('insertAPMModuleAndCreateUidTablePartitionsV2',qryText, qryParam, null, null);
        if(result.next()){
            lModuleId = result.uid;
        }
        return lModuleId
    } catch(e){
        e.message = "unable to insert apm module and create uid table partitions: " + e.stack;
        logger.debug("Error in insertAPMModuleAndCreateUidTablePartitionsV2 method: " + e.message);
        throw e;
    }
}

async function getModuleId(param){
    let system_id = param.systemId;
    let client_unique_id = param.clientUniqueId;
    let module_type = param.moduleTypeName;
    let lModuleId = -1;
    try{
        let strQuery = "SELECT uid from module_master WHERE system_id = $1 AND client_unique_id = $2 AND module_type = $3"
        let strParam = [system_id, client_unique_id, module_type];
        let result = await psqlAPM.fnDbQuery('getModuleId',qryText, qryParam, null, null);
        if(result.next()){
            lModuleId = result.uid;
        }
        return lModuleId;
    } catch(e){
        e.message = "unable to get module id: " + e.stack;
        logger.debug("Error in getModuleId method: " + e.message);
        throw e;
    }
}

async function getSystemData(strUUID, strModuleName, osType){
    try {
        let sysData = {};
        if(osType == "WINDOWS"){
            let system_uuid = strUUID;
            sysData = getSystemDetails(system_uuid);
        } else{
            let apd_uuid = strUUID;
            sysData = getSystemInfo(apd_uuid);
        }
        sysData.module_name = sysData.system_name+"_"+strModuleName;
        return sysData;
    } catch(e){
        e.message = "unable to get system data: " + e.stack;
        logger.debug("Error in getSystemData method: " + e.message);
        throw e;
    }
}

async function getConfigCounters(uid){
    try{
        let joCounter;
        let jaCounters = [];
        let qryText = "SELECT counter_id, query_string, top_process_query_string, execution_type, is_delta, is_top_process, is_static_counter,max_value_counter_id FROM counter_master_"+uid+" WHERE is_selected = true ORDER BY counter_id";
        let qryParam=[];
        let result = await psqlAPM.fnDbQuery('getConfigCounters',qryText, qryParam, null, null);
        while(result.next()){
            joCounter = {};
            joCounter.counter_id = result.counter_id;
            joCounter.query = result.query_string;
            joCounter.top_process_query = result.top_process_query;
            joCounter.executiontype = result.execution_type;
            joCounter.isdelta = result.is_delta;
            joCounter.isTopProcess = result.is_top_process;
            joCounter.isStaticCounter = result.is_static_counter;
            joCounter.maxValueCounterId = result.max_value_counter_id;
            jaCounters.push(joCounter);               
        }
        return jaCounters; 
    }catch(e){
        e.message = "unable to get configured counters: " + e.stack;
        logger.debug("Error in getConfigCounters method: " + e.message);
        throw e;
    }
}

async function getModuleInfo(struuid, systemid, hmkeyvalues){
    try {
        let qryText;
        let hmKeyValues = hmkeyvalues;
        let qryParam=[];
        let joModuleInfo = {}, jaModuleInfo = [];
        qryText = "SELECT uid, guid, module_code, module_name from module_master WHERE system_id = $1 AND client_unique_id = $2 AND module_type IN ('MSIIS','MSSQL','WINDOWS')";
        qryParam=[systemid, struuid];
        let result = await psqlAPM.fnDbQuery('getModuleInfo',qryText, qryParam, null, null);
        while(result.next()){
            joModuleInfo = {};
            joModuleInfo.guid = result.guid;
            joModuleInfo.uid = result.uid;
            joModuleInfo.module_code = result.module_code;
            joModuleInfo.module_name = result.module_name;
            joModuleInfo.message = "";
            if(result.module_code.endsWith("SERVER"))
                hmKeyValues.WINDOWS = joModuleInfo;
            else if(result.module_code.endsWith("APPLICATION"))
                hmKeyValues.MSIIS = joModuleInfo;
            else if(result.module_code.endsWith("DATABASE"))
                hmKeyValues.MSSQL = joModuleInfo;
            jaModuleInfo.push(joModuleInfo);
        }
        return hmKeyValues;
    } catch(e){
        e.message = "unable to get module details: " + e.stack;
        logger.debug("Error in getModuleInfo method: " + e.message);
        throw e;
    }
}

async function getSystemDetails(struuid){
    try{
        let joSysData = {};
        let sys_id = -1;
        let qryText = "SELECT system_id, user_id, system_name, system_number FROM server_information WHERE system_uuid = $1";
        let qryParam = [struuid];
        let result = await psqlAPM.fnDbQuery('getSystemDetails',qryText, qryParam, null, null);
        if (result.next()){
            joSysData.system_id = result.system_id;
            joSysData.user_id = result.user_id;
            joSysData.system_number = result.system_number;
            joSysData.system_name = result.system_name;
        }else{
            joSysData.system_id = sys_id;
        }
        return joSysData;
    }catch(e){
        logger.debug("There was an error in getting system info: " + e.stack);
        e.message="There was an error in getSystemDetails method: " + e.stack;
        throw e;
    }
}

async function getModuleUID(sysid, modulename, jmxport, iscontextname){
    try{
        let joModuleData={};
        qryText = "SELECT uid, guid from module_master where system_id = $1 "
        qryParam = [sysid];
        if(!iscontextname){
        qryText+= " AND module_type = $2"
        } else {
        qryText+=" AND application_context_name = $2"
        }
        qryParam[qryParam.length]=modulename;
        if(jmxport!=null){
            qryText+=" AND jmx_port = $3"
            qryParam[qryParam.length]=jmxport;
        }
        let result = await psqlAPM.fnDbQuery('getModuleUID',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            joModuleData.lModuleId=result.rows[0].uid;
            joModuleData.moduleGUID=result.rows[0].guid;
            joModuleData.isExistsGUID=true;
        }else{
            joModuleData.isExistsGUID=false;
        }
        return joModuleData;
    }catch(e){
        logger.error('Error in getting module id: '+e.stack);
        e.message = "Error in getModuleUID method: "+e.stack;
        throw e;
    }
}

async function getModuleConfigCounters(req,res){
    let guid = req.body.guid;
    let hmKeyValues = {};
    let jaCounters =[];
    let jaSlaCounters = [];
    try{
        let lUid = await getModuleUidFromGuid(guid);
        if (lUid == -1)
            hmKeyValues.KILL = true;
        else {
            jaCounters = await getConfigCounters(lUid);
            hmKeyValues.MonitorCounterSet = jaCounters;
            jaSlaCounters = await getSlaConfigCountersForFirstRequest(guid);
            if (jaSlaCounters!=null)
                hmKeyValues.SlaCounterSet = jaSlaCounters;
        }
        let response = {success:true, failure:false, SlaCounterSet: jaSlaCounters, MonitorCounterSet: jaCounters};
        res.json(response);
    }catch(e){
        logger.error('Error in getting module config counters: '+e.stack);
        res.send({success:false, failure:true, message: "Error getting module config counters: "+e.stack});
    }
}

async function getSlaConfigCountersForFirstRequest(guid){
    try{
        let hmGuidMappedToSlas = {};
        let i = 0;
        let joSlaCounter;
        let qryText;
        let qryParam = [];
        let alSlas = [];
        qryText = "SELECT ssc.sla_id, ss.user_id, ssc.counter_id, ssc.counter_template_id, ssc.guid, ssc.is_above_threshold, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.percentage_calculation, ssc.denominator_counter_id, ssc.process_name FROM so_sla_counter ssc INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id AND ssc.guid=$1 AND ss.is_active = true";
        qryParam=[guid];
        let result = await psqlAPM.fnDbQuery('getSlaConfigCountersForFirstRequest',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            while ( i < result.rowCount){
                joSlaCounter = {};
                strGuid = result.rows[i].guid;
                joSlaCounter.slaid = result.rows[i].sla_id;
                joSlaCounter.userid = result.rows[i].user_id;
                joSlaCounter.counterid  = result.rows[i].counter_id;
                joSlaCounter.countertempid = result.rows[i].counter_template_id;
                joSlaCounter.isabovethreshold = result.rows[i].is_above_threshold;
                joSlaCounter.warning_threshold_value = result.rows[i].warning_threshold_value;
                joSlaCounter.critical_threshold_value = result.rows[i].critical_threshold_value;
                joSlaCounter.percentage_calculation = result.rows[i].percentage_calculation;
                if(result.rows[i].denominator_counter_id != null)
                    joSlaCounter.denominator_counter_id = result.rows[i].denominator_counter_id;
                if (hmGuidMappedToSlas.strGuid)
                    alSlas = hmGuidMappedToSlas.strGuid;
                else{
                    alSlas = [];
                    hmGuidMappedToSlas.strGuid = alSlas;
                }
            alSlas.push(joSlaCounter);
            i++;
            }
        }
        return alSlas;
    }catch(e){
        e.message = "unable to get configured sla counters: " + e.stack;
        logger.debug("Error in getSlaConfigCountersForFirstRequest method: " + e.message);
        throw e;
    }
}

async function getConfigCounters(uid){
    let joCounter = {};
    let jaCounters = [];
    let i=0;
    try{
        let qryText = "SELECT counter_id, query_string, top_process_query_string, execution_type, is_delta, is_top_process, is_static_counter, max_value_counter_id FROM counter_master_"+uid+" WHERE is_selected = true ORDER BY counter_id";
        let qryParam = [];
        let result = await psqlAPM.fnDbQuery('getConfigCounters: ',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            while(i < result.rowCount){
                joCounter.counter_id = result.rows[i].counter_id;
                let query = result.rows[i].query_string;
                if (query.startsWith("{") && query.endsWith("}"))
		   query = JSON.parse(query);
                joCounter.query = query;
                joCounter.top_process_query = result.rows[i].top_process_query_string;
                joCounter.executiontype = result.rows[i].execution_type;
                joCounter.isdelta = Boolean(result.rows[i].is_delta);
                joCounter.isTopProcess = result.rows[i].is_top_process;
                joCounter.isStaticCounter = result.rows[i].is_static_counter;
                joCounter.maxValueCounterId = result.rows[i].max_value_counter_id;
                jaCounters.push(joCounter);
                joCounter = {};
                i++;
            } 
        }
        return jaCounters;
    }catch(e){
        logger.error('Error in gettingconfigured counters: '+e.stack);
        e.message = "Error in getConfigCounters method: "+e.stack;
        throw e;
    }
}

async function getModuleUidFromGuid(guid){
    let struid = -1;
    try{
        let qryText = "SELECT uid FROM module_master WHERE guid = '"+guid+"'";
        let qryParam = [];
        let result = await psqlAPM.fnDbQuery('getModuleUidFromGuid: ',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            struid = result.rows[0].uid;
        }
    return struid;
   }catch(e){
    logger.error('Error in getting module id: '+e.stack);
    e.message = "Error in getModuleUidFromGuid method: "+e.stack;
    throw e;
   }
}

async function createApplicationModule(req, res){
    try{
        let param = {
            appInformation : req.body.moduleInformation,
            joCounterSet : JSON.parse(req.body.jocounterSet),     
            systemId : req.body.systemId,
            UUID : req.body.UUID,
            enterprise_Id : req.body.Enterprise_Id,
            moduleType:"APPLICATION",
            moduleTypeName: JSON.parse(req.body.moduleInformation).moduleTypeName,
            moduleName: JSON.parse(req.body.moduleInformation).moduleName,
            VERSION_ID : JSON.parse(req.body.moduleInformation).VERSION_ID
        };
        let joServerInfo;
        let iscontextname;
        let modulename = param.moduleName;
        let moduletypename = param.moduleTypeName;
        if (modulename == undefined || moduletypename == undefined)
          throw new Error('ModuleName/ ModuleTypeName parameters were not received; please check application configuration or contact System Administrator');
        if(moduletypename.toLowerCase() == 'jboss'){
            iscontextname = false;
            modulename = moduletypename;
        } else{
            iscontextname = true;
        }
        let jmxPort=param.jmx_port=JSON.parse(param.appInformation).hasOwnProperty('jmx_port')?JSON.parse(param.appInformation).jmx_port:null;
        let joModuleData = await getModuleUID(param.systemId, modulename, jmxPort, iscontextname);
        if(!joModuleData.isExistsGUID){
            joServerInfo = await getServerInfo(param.UUID)
            param.moduleDescription=(jmxPort==null?param.moduleName:param.moduleName+"::"+jmxPort);
            param.userId = joServerInfo.user_id;
            await createLinuxModule(req,res,param);
        }else{
            let msg= {
                moduleGUID : joModuleData.moduleGUID,
                message :"Application module already exists",
                moduleUID: joModuleData.lModuleId
            }
            res.json({success: true, failure: false, message: msg});
        }
    } catch(e){
        logger.debug("Error creating application module: "+e.stack);
        res.json({success:false, failure:true, message: "Error creating application module: "+e.stack});
    }
}
async function createWinApplicationModule(req, res){
	try{
		let param = {
			appInformation : req.body.moduleInformation,
			joCounterSet : JSON.parse(req.body.jocounterSet),
			systemId : req.body.systemId,
			UUID : req.body.UUID,
			enterprise_Id : req.body.Enterprise_Id,
			moduleType:"APPLICATION",
			moduleTypeName: JSON.parse(req.body.moduleInformation).moduleTypeName,
			moduleName: JSON.parse(req.body.moduleInformation).moduleName,
			VERSION_ID : JSON.parse(req.body.moduleInformation).VERSION_ID
		};
		let joServerInfo;
		let iscontextname;
		let modulename = param.moduleName;
		let moduletypename = param.moduleTypeName;
		if (modulename == undefined || moduletypename == undefined)
			throw new Error('ModuleName/ ModuleTypeName parameters were not received; please check application configuration or contact System Administrator');
		if(moduletypename.toLowerCase() == 'jboss'){
			iscontextname = false;
			modulename = moduletypename;
		} else{
			iscontextname = true;
		}
		let jmxPort=param.jmx_port=JSON.parse(param.appInformation).hasOwnProperty('jmx_port')?JSON.parse(param.appInformation).jmx_port:null;
		let joModuleData = await getModuleUID(param.systemId, modulename, jmxPort, iscontextname);
		if(!joModuleData.isExistsGUID){
			joServerInfo = await getWinServerInfo(param.UUID)
			param.moduleDescription=(jmxPort==null?param.moduleName:param.moduleName+"::"+jmxPort);
			param.userId = joServerInfo.user_id;
			await createLinuxModule(req,res,param);
		}else{
			let msg= {
				moduleGUID : joModuleData.moduleGUID,
				message :"Application module already exists",
				moduleUID: joModuleData.lModuleId
			}
			res.json({success: true, failure: false, message: msg});
		}
	} catch(e){
		logger.debug("Error creating application module: "+e.stack);
		res.json({success:false, failure:true, message: "Error creating application module: "+e.stack});
	}
}
async function createDatabaseModule(req, res){
    try{
        let param = {
            appInformation: req.body.moduleInformation,
            joCounterSet: JSON.parse(req.body.jocounterSet),
            systemId: req.body.systemId+"",
            UUID: req.body.UUID,
            enterprise_Id: req.body.Enterprise_Id,
            moduleType: "DATABASE",
            moduleTypeName: JSON.parse(req.body.moduleInformation).moduleTypeName,
            moduleName: JSON.parse(req.body.moduleInformation).moduleName,
            VERSION_ID : JSON.parse(req.body.moduleInformation).VERSION_ID
        };
        let uuid = param.UUID;
        let sysid = param.systemId;
        let joSystemData = {}
        let modulename = param.moduleName;
        let joModuleData = await getModuleUID(sysid, modulename, null, true);
        if(!joModuleData.isExistsGUID){
            joSystemData = await getSystemInfo(uuid);
            param.userId = joSystemData.user_id;
            param.moduleDescription=param.moduleName;
            await createLinuxModule(req,res,param);
        }else{
            let msg= {
                moduleGUID : joModuleData.moduleGUID,
                message :"Database module already exists",
                moduleUID: joModuleData.lModuleId
            }
            res.json({success: true, failure:false, message:msg});
        }
    }catch(e){
        logger.debug("Error creating database module: "+e.stack);
        res.json({success:false, failure:true, message: "Error creating database module: "+e.stack});
    }
}

async function getSystemInfo(apd_uuid){
    let joSysData = {};
    let sys_id = -1;
    let qryText = "SELECT system_id, system_name, system_number,user_id FROM server_information WHERE apd_uuid = '"+apd_uuid+"'";
    let qryParam = [];
    try{
        let result = await psqlAPM.fnDbQuery('getSystemInfo: ',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            joSysData.system_id = result.rows[0].system_id;
            joSysData.user_id = result.rows[0].user_id;
            joSysData.system_number = result.rows[0].system_number;
            joSysData.system_name = result.rows[0].system_name;
            joSysData.isExistsUUID = true;
        } else{
            joSysData.system_id=sys_id;
            joSysData.isExistsUUID = false;
        }
        return joSysData;
    }catch(e){
        e.message="Error in getting system info: "+e.stack;
        logger.debug("Error in getSystemInfo method: "+e.stack);
        throw e;
    }
}

async function addUpdateServerInfo(req, res){
    try{
        let systemId = null;
        let encryptedUserId = req.body.encryptedUserId;
        let serverInfo = JSON.parse(req.body.serverInfo);
        let userId = await getUserId(encryptedUserId);
        if(userId != null){
            if(serverInfo.apd_uuid){
                let queryText = 'SELECT system_id FROM server_information WHERE apd_uuid = $1';
                let queryParam = [serverInfo.apd_uuid];
                let result1 = await psqlAPM.fnDbQuery('getSystemId-addUpdateServerInfo',queryText, queryParam, null, null);
                if(result1.rowCount > 0){
                    queryText = 'SELECT system_id FROM server_information WHERE apd_uuid = $1 and user_id = $2';
                    queryParam = [serverInfo.apd_uuid, userId];
                    let result = await psqlAPM.fnDbQuery('getSystemIdfromApdUUID-addUpdateServerInfo',queryText, queryParam, null, null);
                    if(result.rowCount > 0){
                        res.send(serverInfo.apd_uuid);
                    }else{
                        let userInfo;
                        queryText = 'select first_name, last_name from usermaster where user_id = (select user_id from server_information where apd_uuid = $1)';
                        queryParam = [serverInfo.apd_uuid];
                        let resultUser = await psqlAPM.fnDbQuery('getUserInfo-addUpdateServerInfo',queryText, queryParam, null, null);
                        if(resultUser.rowCount > 0){
                            userInfo = resultUser.rows[0].first_name+' '+resultUser.rows[0].last_name;
                        }
                        res.json({message:'UUID mapped for another USER, Please contact System Admin or '+userInfo});
                    }
                }
            }else{
                if(serverInfo.system_uuid){
                    let queryText = 'SELECT system_id FROM server_information WHERE system_uuid = $1';
                    let queryParam = [serverInfo.system_uuid];
                    let result1 = await psqlAPM.fnDbQuery('getSystemId-addUpdateServerInfo',queryText, queryParam, null, null);
                    if(result1.rowCount > 0){
                        systemId = result1.rows[0].system_id;
                    }
                }
                if(systemId != null){
                    queryText = 'SELECT system_id FROM server_information WHERE system_uuid = $1 and user_id = $2';
                    queryParam = [serverInfo.system_uuid, userId];
                    let result = await psqlAPM.fnDbQuery('getSystemIdfromSysUUID-addUpdateServerInfo',queryText, queryParam, null, null);
                    if(result.rowCount > 0){
                        queryText = 'UPDATE server_information set apd_uuid = uuid_generate_v4(), modified_on = now() WHERE system_uuid = $1 and system_id = $2 RETURNING apd_uuid';
                        queryParam = [serverInfo.system_uuid, systemId];
                        let resultApdUUID = await psqlAPM.fnDbQuery('UpdateServerInfo-addUpdateServerInfo',queryText, queryParam, null, null);
                        if(resultApdUUID.rowCount > 0){
                            let apd_uuid = resultApdUUID.rows[0].apd_uuid;
                            res.send(apd_uuid);
                        }
                    }else{
                        let userInfo;
                        queryText = 'select first_name, last_name from usermaster where user_id = (select user_id from server_information where system_uuid = $1)';
                        queryParam = [serverInfo.system_uuid];
                        let resultUser = await psqlAPM.fnDbQuery('getUserInfo-addUpdateServerInfo',queryText, queryParam, null, null);
                        if(resultUser.rowCount > 0){
                            userInfo = resultUser.rows[0].first_name+' '+resultUser.rows[0].last_name;
                        }
                        res.json({message:'UUID mapped for another USER, Please contact System Admin or '+userInfo});
                    }
                } else {
                    let queryText = 'INSERT INTO server_information (user_id, manufacturer, system_name, system_uuid, system_number, apd_uuid, created_on)  VALUES ($1, $2, $3, $4, $5, uuid_generate_v4(), now()) RETURNING apd_uuid';
                    let queryParam = [userId, serverInfo.manufacturer, serverInfo.prod_name, serverInfo.system_uuid, serverInfo.ser_num];
                    let result = await psqlAPM.fnDbQuery('InsertServerInfo-addUpdateServerInfo',queryText, queryParam, null, null);
                    if(result.rowCount > 0){
                        let apd_uuid = result.rows[0].apd_uuid;
                        res.send(apd_uuid);
                    }
                }
            }
        }else{
            res.send('User Id not exist.');
        }
    }catch(e){
        logger.error('Error in createServerModule: '+e.stack);
        res.send('Exception as '+e.stack);
    }
}

async function getUserId(EncryptedUserId){
    let userId = null;
    try{
        let queryText = 'SELECT user_id FROM usermaster WHERE encrypted_user_id = $1';
        let queryParam = [EncryptedUserId];
        let result = await psqlAPM.fnDbQuery('getUserId-addUpdateServerInfo',queryText, queryParam, null, null);
        if(result.rowCount > 0){
            userId = result.rows[0].user_id;
        }
    }catch(e){
        logger.error('Error in createServerModule: '+e.stack);
        throw e;
    }finally{
        return userId;
    }
}

async function addLogMonitor(req, res){
    try{
        let param={
            moduleName: req.body.moduleName,
            apdGenUUID: req.body.apd_uuid,
            clientUniqueId: req.body.client_unique_id,
            encryptedUserId: req.body.encryptedUserId,
            description: req.body.description,
            enterpriseId: req.body.enterpriseId == null? "0":req.body.enterpriseId
        }
        let apd_uuid = param.apdGenUUID;
        let bExist, strGUID, strUserName, tmpguid, strModuleCode = param.moduleCode = "LOG";
        let loginUserDtl={}, serviceMapDtl={}, joSystemData = {}, moduleDetails ={};
        joSystemData = await getSystemInfo(apd_uuid);
        let lSystemId = joSystemData.system_id;
        if(lSystemId > 0){
            bExist = await isModuleExists(strModuleCode, lSystemId);
            if(bExist){
                tmpguid = await getGUID(lSystemId, strModuleCode);
                if(tmpguid == ''){
                    strUserName = await getUserNameFromUUID(apd_uuid);
                    strGUID = "UUID mapped with another USER, Please contact System Admin / "+strUserName;
                } else{
                    strGUID = tmpguid;
                }
            }else{
            let curdatetime=new Date().toISOString().slice(0,19);
            curdatetime=curdatetime.replace('T','_').replace(':','').replace('-','');
            strModuleName = "LOG_MONITOR_"+curdatetime;
            if(param.moduleName.length==0){
                param.moduleName=strModuleName;
            }
                param.agentVersionId=-1;
                param.moduleType = "LOG";
                moduleDetails = await insertLogOrNetWorkMonitor(param, lSystemId);
                param.moduleId = moduleDetails.moduleId;
                strGUID = moduleDetails.GUID;
                loginUserDtl.userId = await getUserIdFromSystemInfo(lSystemId);
                await mapModuleToSystemServiceMap(param,loginUserDtl,serviceMapDtl);
                await insertServiceMapDetails(serviceMapDtl);
            }
        }else{
            strGUID = "System Id not found in our system";
        }
        logger.debug("created new log monitor: "+strGUID);
        res.send(strGUID);
    }catch(e){
        logger.debug("There was an error while trying to add log monitor: "+e.stack);
        res.json({message:"There was an error while trying to addLogMonitor: "+ e.stack});
    }
}

async function addNetworkMonitor(req,res){
    try{
        let param={
            moduleName: req.body.moduleName,
            apdGenUUID: req.body.apd_uuid,
            clientUniqueId: req.body.client_unique_id,
            encryptedUserId: req.body.encryptedUserId,
            description: req.body.description,
            enterpriseId: req.body.enterpriseId == null? "0":req.body.enterpriseId,
        }
        let apd_uuid = param.apdGenUUID;
        let bExist, strGUID, strUserName, tmpguid, strModuleCode = param.moduleCode = "NETWORK";
        let loginUserDtl={}, serviceMapDtl={}, joSystemData = {}, moduleDetails ={};
        joSystemData = await getSystemInfo(apd_uuid);
        let lSystemId = joSystemData.system_id;
        if(lSystemId > 0){
            bExist = await isModuleExists(strModuleCode, lSystemId);
            if(bExist){
                tmpguid = await getGUID(lSystemId, strModuleCode);
                if(tmpguid == ''){
                    strUserName = await getUserNameFromUUID(apd_uuid);
                    strGUID = "UUID mapped with another USER, Please contact System Admin / "+strUserName;
                }else{
                    //strGUID = "Module already exists with GUID: " + tmpguid;
                    strGUID = tmpguid;
                }
            }else{
                let curdatetime=new Date().toISOString().slice(0,19);
                curdatetime=curdatetime.replace('T','_').replace(':','').replace('-','');
                strModuleName = "NETWORK_MONITOR_"+curdatetime;
                if(param.moduleName.length==0){
                    param.moduleName=strModuleName;
                }
                param.agentVersionId=-1;
                param.moduleType = "NETWORK";
                moduleDetails = await insertLogOrNetWorkMonitor(param, lSystemId);
                param.moduleId = moduleDetails.moduleId;
                strGUID = moduleDetails.GUID;
                loginUserDtl.userId = await getUserIdFromSystemInfo(lSystemId);

                let dispName ="HTTP"; let tableName = 'network_http_v7_'+loginUserDtl.userId;
                let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
                let arrUid = {user_id : loginUserDtl.userId, uid: param.moduleId};
                await insChartVisual(arrUid, 'httpLog', dispName, qryT, tableName);
                dispName ="ICMP"; 
                tableName = 'network_icmp_v7_'+loginUserDtl.userId;
                await insChartVisual(arrUid, 'icmpLog', dispName, qryT, tableName);

                //await mapModuleToSystemServiceMap(param,loginUserDtl,serviceMapDtl);
                //await insertServiceMapDetails(serviceMapDtl);
                }
        }else{
            strGUID = "System Id not found in our system";
        }
        res.send(strGUID);
    }catch(e){
        logger.debug("There was an error while trying to add network monitor: "+e.stack);
        res.json({message:"There was an error while trying to addNetworkMonitor: "+ e.stack});
    }
}

async function insNetworkSLA(Uid, userId,param){
    try{
        let queryText;
        let queryParam;
        let sla_name = uid+'::'+param.moduleName;
        let sla_desc = uid+'::'+param.moduleName+'_ICMP';

        queryText = "INSERT INTO so_sla (sla_name,sla_description,server_details,server_details_type,user_id,e_id,sla_type,created_by,created_on,modified_by,modified_on) VALUES ($1,$2,$3,$4,$5,$6,$7,$9,$8,$9,$8) RETURNING sla_id";
        queryParam = [pdata.sla_name, pdata.sla_description, pdata.server_details, pdata.server_details_type, req.decoded.userId,pdata.e_id, pdata.sla_type,created_on,user];

    }catch(e){
        logger.debug("There was an error while trying to add network monitor: "+e.stack);
        throw e;
    }
}
async function insChartVisual(arrUid, type,dispName, qryT, tName) {
    try { 
      let qryText = "SELECT id FROM log_master_table WHERE log_grok=$1";
      let qryParam = [dispName];
      let insLogRefTable = await psqlAPM.fnDbQuery('insChartVisual-getIdFromLogMasterTable', qryText, qryParam);
      if (insLogRefTable.success) {
        qryP = [arrUid.user_id, arrUid.uid, insLogRefTable.rows[0].id, tName, dispName];
        let insChartVisual = await psqlAPM.fnDbQuery('insChartVisual-'+type+'-'+arrUid.uid, qryT, qryP);
        if (!insChartVisual.success) {
          logger.error(insChartVisual.message);
          logger.error("Chart visual creation failed for table " + collTypeTable[type] + arrUid.uid);
        }
      }
      else {
        logger.error(insLogRefTable.message);
        logger.error("Insert to log ref table failed for table " + collTypeTable[type] + arrUid.uid);
      }
    } catch (e) {
      logger.error("insChartVisual thrown Exception "+e.message+" trace "+e.stack);
    }
}

async function insertServiceMapDetails(servicemapdtl){
    try{
        qryText="INSERT INTO service_map_details (service_map_id, mapped_service) VALUES ($1, $2::json)";
        qryParam=[servicemapdtl.serviceMapId,servicemapdtl.serviceMap];
        let result = await psqlAPM.fnDbQuery('insertServiceMapDetails',qryText, qryParam, null, null);
        if(result.rowCount > 0)
            servicemapdtl.message="Successfully inserted record into service map details table";
        else
            servicemapdtl.message="Unable to insert record into service map details table"; 
    }catch(e){
        e.message="There was an error while inserting service map details: "+e.stack;
        logger.debug("There was an error in insertServiceMapDetails method: "+e.stack);
        throw e;
    }
}

async function mapModuleToSystemServiceMap(param,loginusrdtl,servicemapdtl){
    let joModule ={module_master:{module_code: param.moduleCode, uid:param.moduleId}};
    qryText="SELECT service_map_id FROM service_map WHERE user_id = $1 AND is_system = TRUE ";
    qryParam=[loginusrdtl.userId];
    try{
        let result = await psqlAPM.fnDbQuery('mapModuleToSystemServiceMap',qryText, qryParam, null, null);
        if(result.rowCount > 1){
            servicemapdtl.message="More than 1 row were returned";
        } else if(result.rowCount == 1){
            servicemapdtl.message="1 row was returned";
        }
        servicemapdtl.serviceMapId=result.rows[0].service_map_id;
        servicemapdtl.serviceMap=joModule;
    }catch(e){
        e.message="There was an error while trying to map module to service map: "+e.stack;
        logger.debug("There was an error in mapModuleToSystemServiceMap method: "+e.stack);
        throw e;
    }
}

async function insertLogOrNetWorkMonitor(param, sysid){
    let moduleId, GUID;
    qryText="INSERT INTO module_master (user_id, guid, module_code, module_type, counter_type_version_id, module_name, description, e_id, client_unique_id, system_id, created_by, created_on) SELECT si.user_id, uuid_generate_v4(), $1, $2, $3, $4, si.manufacturer||' | '||si.system_name||' | '||si.system_number, $5, si.system_uuid, si.system_id, si.user_id, now() from server_information si where si.system_id = $6 returning uid, guid";
    qryParam=[param.moduleCode, param.moduleType, param.agentVersionId, param.moduleName, param.enterpriseId>0?param.enterpriseId:null,sysid];
    try{
        let result = await psqlAPM.fnDbQuery('insertLogOrNetWorkMonitor',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            moduleId = result.rows[0].uid;
            GUID = result.rows[0].guid;
        }
        return {moduleId: moduleId, GUID: GUID};
    } catch(e){
        e.message="There was an error in inserting log/network monitor: "+e.stack;
        logger.debug("There was an error in insertLogOrNetWorkMonitor method: "+e.stack);
        throw e;
    }
}

async function getUserIdFromSystemInfo(sysid){
    let userid;
    qryText="SELECT user_id FROM server_information WHERE system_id = $1";
    qryParam=[sysid];
    try{
        let result = await psqlAPM.fnDbQuery('getUserIdFromSystemInfo',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            userid=result.rows[0].user_id;
        }
        return userid;
    }catch(e){
        e.message="There was an error in getting user id: "+e.stack;
        logger.debug("There was an error in getUserIdFromSystemInfo method: "+e.stack);
        throw e;
    }
}

async function getUserNameFromUUID(apduuid){
    let qryText="select first_name, last_name from usermaster where user_id = (select user_id from server_information where apd_uuid = $1)";
    let qryParam=[apduuid];
    let strUserName='';
    try{
        let result = await psqlAPM.fnDbQuery('getUserNameFromUUID',qryText, qryParam, null, null);
        if(result.rowCount > 0){
           strUserName=result.rows[0].first_name+" "+result.rows[0].last_name;
        }
        return strUserName;
    }catch(e){
        e.message="Error in getting username: "+e.stack;
        logger.debug("Error in getUserNameFromUUID method: "+e.stack);
        throw e;
    }
}

async function isModuleExists(strmodulecode, sysid){
    let bUsrMappingExists=false;
    let qryText="SELECT EXISTS (SELECT 1 FROM module_master WHERE system_id = $1 AND module_code = $2) AS is_system_id_exists";
    let qryParam=[sysid,strmodulecode];
    try{
        let result = await psqlAPM.fnDbQuery('getUsrMappingIfExists',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            bUsrMappingExists=result.rows[0].is_system_id_exists;
        }
        return bUsrMappingExists;
    }catch(e){
        e.message = "Error in checking if module exists: "+ e.stack;
        logger.debug("Error in isModuleExists method: "+e.stack);
        throw e;
    }
}

async function getGUID(sysid, strmodulecode){
    let strguid;
    let qryText="SELECT mm.GUID AS guid FROM module_master mm join server_information si on si.system_id=mm.system_id and si.user_id=mm.user_id WHERE si.system_id = $1 AND module_code = $2";
    let qryParam=[sysid,strmodulecode];
    try{
        let result = await psqlAPM.fnDbQuery('getGUID',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            strguid=result.rows[0].guid;
        }
        return strguid;
    }catch(e){
        e.message="Error in getting GUID: "+ e.stack;
        logger.debug("Error in getGUID method: "+e.stack);
        throw e;
    }
}

async function addProfiler(req,res){
    try {
        let lModuleId = -1;
        let param = {
            moduleName: req.body.moduleName,
            description: req.body.description,
            clientUniqueId: req.body.client_unique_id,
            systemId: req.body.systemId,
            enterpriseId: req.body.ent_id == null ? -1:req.body.ent_id,
            agentVersionId: -1,
            moduleType: "PROFILER"
        }
        lModuleId = await addProfilerModule(param);
        if (lModuleId > 0) {
            res.send("success#@#"+lModuleId);
        }else{
            res.send("failure#@#Module creation failed, kindly check with System Administrator.")
        }
    }catch(e){
        e.message="failure#@#Module creation failed, kindly check with System Administrator."+e.stack;
        logger.debug("failure#@#Module creation failed, kindly check with System Administrator."+e.stack);
        throw e;
    }
}

async function addProfilerModule(param){
    try{
        let moduleid;
        let qryText="INSERT INTO module_master (user_id, guid, module_code, counter_type_version_id, module_name, description, module_type, created_by, created_on, e_id, client_unique_id, system_id) SELECT si.user_id, uuid_generate_v4(), $1, $2, $3, si.system_name||'::'||si.manufacturer||'::'||si.system_number, $4, si.user_id, now(), $5, si.system_uuid, si.system_id from server_information si where system_id = $6 RETURNING uid";
        let qryParam=[param.moduleType,param.agentVersionId,param.moduleName,param.moduleType,param.enterpriseId > 0 ? param.enterpriseId:null,param.systemId];
        let result = await psqlAPM.fnDbQuery('addProfilerModule',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            moduleid = result.rows[0].uid;
        }
        return moduleid;
    }catch(e){
        e.message="Error in addProfilerModule: "+ e.stack;
        logger.debug("Error in addProfilerModule method: "+e.stack);
        throw e;
    }
}

async function getSystemIdFromApdUUID(req, res){
    let sysId = -1;
    let strRtnData = "";
    let apd_uuid = req.body.apd_uuid;
    try{
        let qryText = 'SELECT system_id FROM server_information WHERE apd_uuid = $1';
        let qryParam = [apd_uuid];
        let result = await psqlAPM.fnDbQuery('getSystemIdFromApdUUID',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            sysId = result.rows[0].system_id;
        }
        if(sysId > 0)
            strRtnData = "success#@#"+sysId;
        else
            strRtnData = "failed#@#System ID is not found, kindly check with System Administrator."
    }catch(e){
        strRtnData = "failed#@#Unable to get system details.";
        e.message="Error in getSystemIdFromApdUUID: "+ e.stack;
        logger.debug("Error in getSystemIdFromApdUUID method: "+e.stack);
        throw e;
    }finally{
       res.send(strRtnData);
    }
}

async function checkProfilerAlreadyExists(req, res){
    let lSystemId = -1;
    let joModDetails ={};
    let lUserId, clientUniqueId;
    let strRtnData="";
    try{
        lSystemId = req.body.systemId;
        clientUniqueId = req.body.client_unique_id;
        lUserId = await getUserIdFromSystemId(lSystemId);
        if(lUserId > 0){
            joModDetails = await isInstanceHasProfiler(lUserId, lSystemId);
            if(joModDetails.hasOwnProperty("agentCount") && joModDetails.agentCount > 0)
                strRtnData = "warning#@#"+joModDetails.agentCount+"#@#"+joModDetails.agentName;
            else
                strRtnData = "success#@#No Profiler Exists. Cleared to go"
        }else{
            strRtnData = "failure#@#User Id not found in our database, kindly check with System Administrator."
        }
    }catch(e){
        strRtnData = "failure#@#Unable to get Profiler Details."
        e.message="Error in checkProfilerAlreadyExists: "+ e.stack;
        logger.debug("Error in checkProfilerAlreadyExists method: "+e.stack);
        throw e;
    }finally{
       res.send(strRtnData);
    }
}

async function isInstanceHasProfiler(userid,systemid){
    let joRtn = {};
    let strRtn = "";
    try{
        let qryText = "SELECT string_agg(module_name, ',') as module_name, count(module_name) as cnt FROM module_master WHERE user_id = $1 AND system_id = $2 AND module_code = $3 ";
        let qryParam = [userid,systemid,"PROFILER"];
        let result = await psqlAPM.fnDbQuery('isInstanceHasProfiler',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            joRtn.agentName = result.rows[0].module_name == null?"":result.rows[0].module_name;
            joRtn.agentCount = result.rows[0].cnt;
        }
        return joRtn;
    }catch(e){
        e.message="Error in isInstanceHasProfiler: "+ e.stack;
        logger.debug("Error in isInstanceHasProfiler method: "+e.stack);
        throw e;
    }
}

async function getUserIdFromSystemId(systemid){
    let userid = -1;
    try{
        let qryText = "SELECT user_id FROM server_information WHERE system_id = $1";
        let qryParam = [systemid];
        let result = await psqlAPM.fnDbQuery('getUserIdFromSystemId',qryText, qryParam, null, null);
        if(result.rowCount > 0){
            userid = result.rows[0].user_id;
        }
        return userid;
    }catch(e){
        e.message="Error in getUserIdFromSystemId: "+ e.stack;
        logger.debug("Error in getUserIdFromSystemId method: "+e.stack);
        throw e;
    }
}

async function sendAvmAlert(sla_data, userId){
    try{
        let mobileNumber = [];
        let emailIds = [];
        let qryText = "select lower(alert_type) AS alert_type, email_mobile from so_alert WHERE is_valid = TRUE AND user_Id = $1"
        let qryParam = [userId];
        let result = await psqlAPM.fnDbQuery('getUserAlertToAddresses',qryText, qryParam, null, null);
        if(result.success && result.rowCount > 0){
            result.rows.forEach(function(alert){
                if(alert.alert_type == 'sms'){
                    mobileNumber.push(alert.email_mobile);
                }else{
                    emailIds.push(alert.email_mobile);
                }
            });
        }
        if(mobileNumber.length > 0 || emailIds.length>0){
            let qryText = "SELECT atm.testname AS testname, atm.testurl AS url, atam.country||'-'||atam.state||'('||atam.city||')' AS location, CASE WHEN ssl.has_avm_process = TRUE AND ssl.avm_service_status THEN 'Website is Up.' ELSE 'Website is Unreachable.' END AS breached_severity_message,  CASE WHEN ssl.has_avm_process = TRUE AND ssl.avm_service_status THEN 'Website is Up.' ELSE 'Website is Unreachable.' END AS breached_severity_subject, CASE WHEN ssl.has_avm_process = TRUE AND ssl.avm_service_status THEN 'ORANGE' ELSE 'RED' END AS severity_color, CASE WHEN ssl.has_avm_process = TRUE AND ssl.avm_service_status THEN atm.testname||' test is Up' ELSE atm.testname||' test is Down' END AS sms FROM so_sla_log ssl INNER JOIN avm_test_master atm ON atm.avm_test_id = ssl.uid INNER JOIN avm_test_agent_mapping atam ON atam.avm_test_id = atm.avm_test_id WHERE ssl.sla_id = $1";
            let qryParam = [sla_data.sla_id];
            let result = await psqlAPM.fnDbQuery('getAvmAlertDetails',qryText, qryParam, null, null);
            if(result.success && result.rowCount > 0){
                let updQryText = "UPDATE sla_alert_log_"+userId+" SET ";
                let updQryParam = [];
                let mailDetails = result.rows[0];
                if(mobileNumber.length > 0){
                    let message = "Alert from Appedo! "+mailDetails.sms;
                    await appedoMailer.sendSMS(mobileNumber.toString(), message);
                    updQryText += "sms = $1, alert_content_sms = $2";
                    updQryParam.push(mobileNumber.toString());
                    updQryParam.push(message);
                    updQryText = emailIds.length > 0 ? updQryText+", " : updQryText;
                }
                if(emailIds.length > 0){
                    let mailSubject = "FYA: {{ appln_heading }} AVM Alert - {{testname}} : {{breached_severity_subject}}";
                    emailIds.forEach(async function(emailId){
                        await appedoMailer.sendMail(appedoMailer.MODULE_ID.AVM_ALERT_EMAIL, mailDetails, emailId, mailSubject);
                    });
                    updQryText += "email_id= $3, alert_content_email = $4 ";
                    updQryParam.push(emailIds.toString());
                    updQryParam.push(mailDetails.breached_severity_message);
                }

                updQryText += "WHERE alert_log_id = "+sla_data.alert_log_id;

                qryText = "UPDATE sla_alert_log_"+userId+" SET is_sent = TRUE, is_success = TRUE, sent_alert_log_id = $1 WHERE alert_log_id <= $2 AND sla_id = $3 AND is_sent = FALSE";
                qryParam = [sla_data.alert_log_id, sla_data.alert_log_id, sla_data.sla_id];
                await psqlAPM.fnDbQuery('Update-SlaAlertLog-'+userId+' : '+sla_data.sla_id,qryText, qryParam, null, null);

                await psqlAPM.fnDbQuery('Update-SlaAlertLog Mail Details-'+userId+' : '+sla_data.sla_id, updQryText, updQryParam, null, null);
            }
        }
    }catch(e){
        logger.debug("Error in AvmAlertProcess: "+e.stack);
    }
}

async function avmAlertProcess(param){
    let userid = param.userId;
    try{
        let qryText = "SELECT * FROM avm_alert_breache_process($1)";
        let qryParam = [userid];
        let result = await psqlAPM.fnDbQuery('avm_alert_breache_process',qryText, qryParam, null, null);
        if(result.success && result.rowCount > 0){
            result.rows.map((sla, idx) => {
                sendAvmAlert(sla, userid);
            });
        }
    }catch(e){
        logger.debug("Error in AvmAlertProcess: "+e.stack);
    }
}

router.post('/AvmAlertServiceTrigger', async(req,res)=>{
    try{
        setTimeout(() => {
            avmAlertProcess(req.body);
          }, 10000);
        res.send("Received AVM Alert Data.");
    } catch (e) {
      logger.error("AvmAlertServiceTrigger : ",e.message+e.stack);
      res.send("AvmAlertServiceTrigger : "+e.message);
    }
});

router.post('/getSystemIdFromApdUUID', async (req,res) => {
    getSystemIdFromApdUUID(req,res);
});

router.post('/checkProfilerAlreadyExists', async (req,res) => {
    checkProfilerAlreadyExists(req,res);
});

router.post('/javaUnification', async (req, res) => {
    linuxUnification(req, res); 
});

router.post('/addProfiler', async(req, res) => {
    addProfiler(req, res);
});

router.post('/addUpdateServerInfo', async (req, res) => {
    addUpdateServerInfo(req, res);
});

router.post('/addLogMonitor',async(req,res)=>{
    addLogMonitor(req,res);
});

router.post('/getConfigurationsV2', async(req, res) => {
    //collectAgentMessage(req, res);
});

router.post('/addNetworkMonitor',async(req,res)=>{
    addNetworkMonitor(req,res);
});

router.post('/getConfigurations', async(req,res) =>{
    getModuleConfigCounters(req,res);
});

router.post('/registerAVL',async(req,res) => {
    let data = req.body;
    let queryText = "SELECT agent_id,last_received_url_cnt FROM avm_agent_master WHERE guid=$1"
    let queryParam = [data.guid];
    let result = await psqlAPM.fnDbQuery('registerAVL-Select',queryText, queryParam, null, null);
    if (result.rowCount == 0){
        queryText = "INSERT INTO avm_agent_master(user_id,is_private,ip_address,country,state,city,region, zone, latitude,longitude, os_type,operating_system, os_version,agent_version, status, created_by, created_on, guid) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18) RETURNING agent_id";
        queryParam = [data.user_id,data.is_private,data.ip_address,data.country,data.state,data.city,data.region, data.zone, data.latitude,data.longitude, data.os_type,data.operating_system, data.os_version,data.agent_version, data.status, data.created_by, data.created_on, data.guid];
        let result1 = await psqlAPM.fnDbQuery('registerAVL-Insert',queryText, queryParam, null, null);
        if (result1.success){
            res.json({success:true,message:"AVM Agent registered "+result1.rows[0].agent_id, agentId: result1.rows[0].agent_id, urlCnt:0});
        } else {
            res.json({success:false,message:"AVM Agent could not be registered"});
        }
    } else {
        res.json({success:true,message:"AVM Agent already registered with id "+result.rows[0].agent_id+" total url processed till now "+result.rows[0].last_received_url_cnt, agentId:result.rows[0].agent_id, urlCnt:result.rows[0].last_received_url_cnt});
    }
});

async function avmHeartBeat(data){
    let queryText = "INSERT INTO avm_heartbeat_log(agent_id,location,appedo_received_on, ip_address, latitude, longitude) VALUES($1,$2,now(),$3, $4, $5)";
    let queryParam = [data.agentId, data.location, data.ipAddress, data.latitude, data.longitude];
    let result = await psqlAPM.fnDbQuery("avmHeartBeat",queryText, queryParam, null, null);
    if (result.success){
        logger.info("Successfully inserted the AVM heartbeat status "+data.agentId)        
    } else {
        logger.error("Failed to insert the AVM heartbeat status "+JSON.stringify(data));
    }
}

router.post('/updateAVMAgentStatus',async(req,res) => {
    avmHeartBeat(req.body);
    let data = req.body;
    let queryText = "UPDATE avm_agent_master SET last_requested_on=$1, last_received_url_cnt=$2, last_responded_on=$3,last_error_on=$4, modified_on=now(), modified_by=1 WHERE agent_id=$5";
    let queryParam = [data.last_requested_on, data.last_received_url_cnt,data.last_responded_on,data.last_error_on,data.agentId];
    let result = await psqlAPM.fnDbQuery('updateAVMAgentStatus',queryText, queryParam, null, null);
    if (result.success){
        res.json({success:true,message:"Successfully updated the agent status"});
    } else {
        res.json({success:false ,message:"Error updating the agent status"});
    }
});

router.post('/addSystemDetails',async(req,res) =>{
    let body = req.body;
    let queryParam = [body.system_uuid, body.encrypt_userid];
    let queryText = "SELECT system_id, user_id FROM server_information WHERE system_uuid = $1 AND user_id IN (SELECT user_id FROM usermaster WHERE encrypted_user_id = $2)";
    let result = await psqlAPM.fnDbQuery("addSystemDetails-GetSystemId",queryText, queryParam);
    if (result.success){
        if (result.rowCount > 0){
            //getting registered cards for this system 
            queryText = "SELECT uid, guid, module_code, module_type, system_id FROM module_master WHERE system_id=$1";
            queryParam = [result.rows[0].system_id];
            let result2 = await psqlAPM.fnDbQuery("addSystemDetails-GetModules",queryText, queryParam, null, null);
            if (result2.rowCount>0){
                res.json(result2.rows);
            } else {
                res.send(result.rows[0].system_id);
            }
        } else {
            //Inserting new server information.
            queryParam = [body.manufacturer, body.os_name, body.system_name, body.system_model, body.system_type, body.os_serial_no, body.system_number, body.system_uuid, body.ram_capacity, body.installed_ram, body.ram_part_number, body.ram_serial_number, body.processor_manufacturer, body.processor_id, body.processor_max_speed, body.processor_external_clock, body.processor_version, body.processor_number, body.logical_processor_number, body.number_of_core, body.core_per_socket, body.processor_part_number, body.processor_thread_cnt, body.dd_name, body.dd_manufacturer, body.dd_size_bytes, body.dd_media_type, body.dd_serial_number, body.dd_model, body.dd_interface_type, body.os_architecture, body.processor_family, body.processor_type, body.ram_type,body.encrypt_userid];
            queryText = "INSERT INTO server_information (manufacturer, os_name, system_name, system_model, system_type, os_serial_no, system_number, system_uuid, ram_capacity, installed_ram, ram_part_number, ram_serial_number, processor_manufacturer, processor_id, processor_max_speed, processor_external_clock, processor_version, processor_number, logical_processor_number, number_of_core, core_per_socket, processor_part_number, processor_thread_cnt, dd_name, dd_manufacturer, dd_size_bytes, dd_media_type, dd_serial_number, dd_model, dd_interface_type, os_architecture, processor_family, processor_type, ram_type, user_id, created_on) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,(SELECT user_id FROM usermaster WHERE encrypted_user_id = $35 LIMIT 1),now()) RETURNING system_id";
            let result1 = await psqlAPM.fnDbQuery("addSystemDetails-AddSystem", queryText,queryParam);
            if (result1.success){
                res.send(result1.rows[0].system_id);
            } else {
                if (result1.message.includes("user_id")) {
                    //This is for null constraint violation possibility. user_id cannot be null and happens if the previously registered user id does not exist in system
                    res.send("User is not registered in APM Server, please register and download the agent again to install.");
                } else {
                    res.send("addSystemDetails failed to insert system information. ErrMsg "+result1.message);
                }
            }
        }
    } else {
        res.send("addSystemDetails failed to get system information. ErrMsg "+result.message);
    }
});

router.post('/getCurrentVersions',async(req,res) =>{
    let body = req.body;
    let queryParam = [body.projectName];
    let queryText = "SELECT package, version, release_version, downloader_name FROM current_release_versions WHERE project_name = $1";
    let result = await psqlAPM.fnDbQuery("getCurrentVersions",queryText, queryParam, null, null);
    if (result.success){
        if (result.rowCount > 0){
            res.json(result.rows);
        } else {
            //for c# this code is managed to deal with text hence no json return.
            res.send("No Versions found for project "+body.projectName);
        }
    } else {
        //for c# this code is managed to deal with text hence no json return.
        //to manage json, might require all possible error syntax hence avoided. 
        res.send(result.message);
    }
})

router.post('/getSetupUrlPath',async(req,res) =>{
    let body = req.body;
    let queryParam = [body.moduleName];
    let queryText = "SELECT counter_type_name AS setup_name, monitor_agent_full_path AS on_prem_path, cloud_path, display_name, monitor_guid_files AS files_to_update FROM counter_type WHERE module_name = $1 AND enable_add";
    let result = await psqlAPM.fnDbQuery("getSetupUrlPath",queryText, queryParam, null, null);
    if (result.success){
        if (result.rowCount > 0){
            res.json(result.rows);
        } else {
            //for c# this code is managed to deal with text hence no json return.
            res.send("No Setup files found for Module "+body.moduleName);
        }
    } else {
        //for c# this code is managed to deal with text hence no json return.
        //to manage json, might require all possible error syntax hence avoided. 
        res.send(result.message);
    }
})

router.post('/registerModule',async(req,res) =>{
    let body = req.body;
    let result = await registerModule(body);
    if (result.success){
        res.send(result.rows[0].guid+"|"+result.rows[0].uid);
    } else {
        if (result.message.includes("user_id")) {
            //This is for null constraint violation possibility. user_id cannot be null and happens if the previously registered user id does not exist in system
            res.send("registerModule - User is not registered in APM Server, please register and download the agent again to install.");
        } else {
            res.send("registerModule failed to create "+body.module_code+" Card. ErrMsg "+result.message);
        }
    }
})

router.post('/getConfigParameters',async(req,res) => {
    try {
        body = req.body;
        res.send(appedoConfigProperties[body.property]);
    } catch (e) {
        res.send("ErrMsg:"+e.message);
    }
})

router.post('/getSetWindowsCard',async(req,res) => {
    try {
        let strCounterData = JSON.stringify(req.body.counterSet.counterData);
        strCounterData = strCounterData.replace("'","''");
        body = req.body;
        let param = {encrypted_user_id:body.encrypted_user_id,module_code: body.module_code,module_name: body.module_name,description: body.description, client_unique_id:body.client_unique_id, e_id: body.e_id,system_id: body.system_id, module_type: body.module_type};
        let result = await registerModule(param);
        if (result.success){
            let uid = result.rows[0].uid;
            let guid = result.rows[0].guid;
            let userId = result.rows[0].user_id;
            if (param.module_type == "MSSQL"){
                mssqlTables(uid);
            }
            //adding counter_master
            let tableName = "counter_master_"+uid;
            let queryText = "CREATE TABLE IF NOT EXISTS "+tableName+" (like counter_master_template_v1 including all)";
            let result1 = await psqlAPM.fnDbQuery("getSetWindowsCard-CreateTableCounterMaster", queryText, null, null, null);
            if (result1.success){
                queryText = "ALTER TABLE "+tableName+" ADD COLUMN counter_id SERIAL";
                result1 = await psqlAPM.fnDbQuery("getSetWindowsCard-ALTERTableCounterMaster", queryText, null, null, null);
                if (result1.success){
                    queryText = "ALTER SEQUENCE "+tableName+"_counter_id_seq RESTART WITH 200000";
                    result1 = await psqlAPM.fnDbQuery("getSetWindowsCard-ModifySeqCounterMaster", queryText, null, null, null);
                    if (result1.success){
                        let queryParam = [userId, guid, strCounterData];
                        queryText = "INSERT INTO counter_master_"+uid+"(user_id, guid, execution_type, is_enabled, created_on, created_by, category, counter_name, has_instance, instance_name, unit, is_selected, is_static_counter, query_string, counter_description) SELECT $1, $2, 'PERFMON' AS execution_type, true as is_enabled, now() AS created_on, $1 AS created_by, category, counter_name, has_instance, instance_name, unit, is_selected, is_static_counter, query_string, counter_description FROM json_populate_recordset(null::counter_master_"+uid+", $3)";
                        result1 = await psqlAPM.fnDbQuery("getSetWindowsCard-InsertedCounterMaster", queryText, queryParam, null, null);
                        if (result1.success){
                            //creating chart visual 
                            //creating chart visual, mssql - wait stat, lock data, slow query and procedure tables 
                            let qText = "SELECT * from insert_chart_visual_data($1,$2,$3,$4)";
                            let qParam = [uid, userId,body.module_code, body.module_name];
                            psqlAPM.fnDbQuery("getSetWindowsCard-CreateChartVisual", qText, qParam, null, null);
                            res.send(guid+"|"+uid);
                        } else {
                            logger.error("getSetWindowsCard-InsertedCounterMaster "+result1.message);
                            logger.error("getSetWindowsCard-InsertedCounterMaster-Data "+userId+', '+guid+' : '+strCounterData);
                            res.send("Failed to Insert Data to counter master table for "+body.module_code+" "+body.module_type+" ErrMsg: "+result1.message);
                        }
                    } else {
                        logger.error("getSetWindowsCard-ModifySeqCounterMaster : "+result1.message,)
                        res.send("Failed to modify seq counter master ErrMsg "+result1.message);
                    }
                } else {
                    logger.error("getSetWindowsCard-ALTERTableCounterMaster : ",result1.message,)
                    res.send("Failed to alter counter master ErrMsg "+result1.message);
                }
            } else {
                logger.error("getSetWindowsCard-CreateTableCounterMaster : ",result1.message,)
                res.send("Failed to Create counter master table ErrMsg "+result1.message);
            }
        } else {
            res.send("Failed to create the card ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("ErrMsg: "+e.message);
    }
})

async function registerModule(param) {
    let body = param;
    let eId = body.e_id == '' ? null : body.e_id;
    let moduleType = body.module_type == undefined ? body.module_code : body.module_type;
    let queryParam = [body.encrypted_user_id, body.module_code, body.module_name, body.description, body.client_unique_id, eId, body.system_id, moduleType];
    queryText = "INSERT INTO module_master (user_id, module_code, module_name, description, created_by, client_unique_id, e_id, system_id, module_type) VALUES ((SELECT user_id FROM usermaster WHERE encrypted_user_id = $1), $2, $3, $4, (SELECT user_id FROM usermaster WHERE encrypted_user_id = $1), $5, $6, $7, $8) RETURNING guid,uid, user_id";
    let result = await psqlAPM.fnDbQuery("registerModule", queryText, queryParam, null, null);
    if (result.success){
        if (moduleType == "NETWORK") {
            let dispName ="HTTP"; 
            let tableName = 'network_http_v7_'+result.rows[0].user_id;
            let qryT = "SELECT * FROM add_network_query($1, $2, $3, $4, $5);";
            let arrUid = {user_id : result.rows[0].user_id, uid: result.rows[0].uid};
            await insChartVisual(arrUid, 'httpLog', dispName, qryT, tableName);
            dispName ="ICMP"; 
            tableName = 'network_icmp_v7_'+result.rows[0].user_id;
            await insChartVisual(arrUid, 'icmpLog', dispName, qryT, tableName);
        } else if (moduleType == "NETSTACK"){
            let queryT = "SELECT * FROM add_netstack_chart_visual($1, $2)"
            let qryP = [result.rows[0].user_id, result.rows[0].uid];
            psqlAPM.fnDbQuery("registerModule-NetstackChartVisual",queryT,qryP, null,null);
            let tableName = "net_stack_"+result.rows[0].uid;
            let tableName1 = "net_stack_func_"+result.rows[0].uid;
            let qry = "CREATE TABLE IF NOT EXISTS "+tableName+"(id bigserial, uid integer, guid varchar, thread_id integer, function_id integer, hierarchy integer, start_time timestamp with time zone, end_time timestamp with time zone, resp_time integer, continuity_id integer, created_on timestamp with time zone DEFAULT now(), appedo_received_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on); CREATE INDEX idx_"+tableName+"_start_time ON "+tableName+"(start_time); CREATE INDEX idx_"+tableName+"_end_time ON "+tableName+"(end_time); CREATE TABLE IF NOT EXISTS "+tableName1+"(function_id serial, function_name varchar);"
            psqlAPM.fnDbQuery("registerModule-netstackTables",qry,[], null, null);
        }
    }
    return result;
}

async function mssqlTables(uid){
    let tableN1 = "mssql_wait_stat_"+uid;
    let qText1 = "CREATE TABLE IF NOT EXISTS "+tableN1+"(id bigserial,uid int, appedo_received_on timestamp with time zone, status varchar, count int DEFAULT 0, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on)";
    psqlAPM.fnDbQuery("getSetWindowsCard-MSSQLWaitStat",qText1,[],null,null);
    let tableN2 = "mssql_lock_data_"+uid;
    let qText2 = "CREATE TABLE IF NOT EXISTS "+tableN2+"(id bigserial,uid int, guid varchar, appedo_received_on timestamp with time zone, request_session_id int,blocking_session_id int, wait_duration_ms int, client_net_address varchar, connect_time timestamp with time zone, resource_type varchar, request_query varchar, blocking_query varchar, request_mode varchar,command varchar,status varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on)";
    psqlAPM.fnDbQuery("getSetWindowsCard-MSSQLlockdata",qText2,[],null,null);
    let tableN3 = "mssql_slowprocedure_"+uid;
    let qText3 = "CREATE TABLE IF NOT EXISTS "+tableN3+"(id bigserial,uid int, appedo_received_on timestamp with time zone, calls int, duration_ms int, procedure text, procedure_name varchar, cached_time timestamp with time zone, last_execution_time timestamp with time zone, avg_cpu_time_ms varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on)";
    psqlAPM.fnDbQuery("getSetWindowsCard-MSSQLslowprocedure",qText3,[],null,null);
    let tableN4 = "mssql_slowquery_"+uid;
    let qText4 = "CREATE TABLE IF NOT EXISTS "+tableN4+"(id bigserial,uid int, appedo_received_on timestamp with time zone, calls int, duration_ms int, query text, cached_time timestamp with time zone, last_execution_time timestamp with time zone, avg_cpu_time_ms varchar, db_name varchar, created_on timestamp with time zone DEFAULT now()) PARTITION BY RANGE (appedo_received_on)";
    psqlAPM.fnDbQuery("getSetWindowsCard-MSSQLslowprocedure",qText4,[],null,null);
}

router.post('/getModuleInfoBySys',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [body.uuid];
        let queryText = "SELECT uid, guid, system_id, module_code, module_type FROM module_master where client_unique_id = $1 AND module_type IN ('MSSQL', 'MSIIS', 'WINDOWS')";
        let result = await psqlAPM.fnDbQuery("getModuleInfoBySys", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.json(result.rows);
        } else if (result.rowCount == 0 ){
            res.send ("getModuleInfoBySys - No rows found for this system.");
        } else {
            res.send("getModuleInfoBySys - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("ErrMsg:"+e.message);
    }
})

router.post('/getMetricSetByUid',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [];
        let tableName = "counter_master_"+body.uid;
        let queryText = "SELECT counter_id, query_string, execution_type, has_instance, COALESCE(instance_name,'') AS instance_name, is_static_counter FROM  "+tableName+" WHERE is_selected AND is_enabled";
        let result = await psqlAPM.fnDbQuery("getMetricSetByUid", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.json(result.rows);
        } else if (result.rowCount == 0 ){
            res.send ("norows");
        } else {
            res.send("getMetricSetByUid - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("ErrMsg:"+e.message);
    }
})

router.post('/getMetricSlaSetByUid',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [body.uid];
        let queryText = "SELECT counter_id, sla_id, is_above_threshold, critical_threshold_value, warning_threshold_value, COALESCE(process_name,'') AS instance_name FROM so_sla_counter WHERE uid = $1";
        let result = await psqlAPM.fnDbQuery("getMetricSlaSetByUid", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.json(result.rows);
        } else if (result.rowCount == 0 ){
            res.send("norows");
        } else {
            res.send("getMetricSlaSetByUid - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("ErrMsg:"+e.message);
    }
})

router.post('/getUIDStatus',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [body.uuid];
        let queryText = "SELECT uid, COALESCE(user_status,'') as user_status from module_master WHERE client_unique_id = $1 AND module_type IN ('MSSQL', 'MSIIS', 'WINDOWS')";
        let result = await psqlAPM.fnDbQuery("getUIDStatus", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            queryText = "UPDATE module_master SET user_status = 'Running' WHERE uid IN (";
            result.rows.map((row,ix) =>{
                if (ix != 0) queryText += ","
                queryText += row.uid;
                if (result.rowCount == ix+1){
                    queryText += ");"
                }
            });
            psqlAPM.fnDbQuery("getUIDStatus-resetUserstatus", queryText, []);
            res.json(result.rows);
        } else if (result.rowCount == 0 ){
            res.send("norows");
        } else {
            res.send("getUIDStatus - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("ErrMsg:"+e.message);
    }
})

router.post('/getAgentVersions',async(req,res) => {
    try{
        let queryText = "SELECT release_version, project_name, project_type, downloader_name, package, version, is_active, created_on FROM current_release_versions WHERE is_active = true AND project_name ilike 'linux_unified_agent'";
        let queryParam = [];
        let result = await psqlAPM.fnDbQuery("getAgentVersions - AgentNotification", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.json({success:true, failure:false, message: result.rows});
        } else if (result.rowCount == 0 ){
            res.json({success:false, failure:true, message: "No rows return"});
        } else {
            res.json({success:false, failure:true, message: result.message});
        }
    }catch(e){
        res.json({success:false, failure:true, message: e});
    }
});

router.post('/getGUIDByModType',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [body.uuid, body.moduleType];
        let queryText = "SELECT guid from module_master WHERE client_unique_id = $1 AND module_type = $2";
        let result = await psqlAPM.fnDbQuery("getGUIDByModType", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.send("GUID|"+result.rows[0].guid);
        } else if (result.rowCount == 0 ){
            res.send("norows");
        } else {
            res.send("getGUIDByModType - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("getGUIDByModType - ErrMsg: "+e.message);
    }
})

router.post('/getAgentCurVer',async(req,res) => {
    try {
        let body = req.body;
        let queryParam = [body.package];
        let queryText = "SELECT version FROM current_release_versions WHERE package = $1 AND project_name = 'windows_unified_agent' LIMIT 1";
        let result = await psqlAPM.fnDbQuery("getAgentCurVer", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            res.send("VERSION|"+result.rows[0].version);
        } else if (result.rowCount == 0 ){
            res.send("norows");
        } else {
            res.send("getAgentCurVer - ErrMsg: "+result.message);
        }
    } catch (e) {
        res.send("getAgentCurVer - ErrMsg: "+e.message);
    }
})

router.post('/getAgentSetupUrl', async(req,res) => {
    try{
        let body = req.body;
        let queryText = " SELECT cv.downloader_name, ct.monitor_agent_full_path, ct.cloud_path, cv.version FROM current_release_versions cv JOIN counter_type ct ON LOWER(counter_type_name) = cv.downloader_name WHERE downloader_name = $1";
        let queryParam = [body.downloader_name];
        let result = await psqlAPM.fnDbQuery("getAgentSetupUrl", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            let row = result.rows[0];
            res.send("SETUP|"+row.downloader_name+","+row.monitor_agent_full_path+","+row.cloud_path+","+row.version);
        } else if (result.rowCount == 0 ){
            res.send("norows");
        } else {
            res.send("getAgentSetupUrl - ErrMsg: "+result.message);
        }
    } catch (e){
        res.send("getAgentSetupUrl - ErrMsg: "+e.message);
    }
})

router.get('/downloadFiles/:filename', async(req,res) => {
    try{
        path = req.params.filename ;
        res.download(path);
    } catch (e){
        res.send("downloadFiles - ErrMsg: "+e.message);
    }
})

async function deleteJMeterScenarios(param){
    let response = {};
    try{
        let fileName = param.scenarioName + '.jmx';
        let UploadedFilePath = JMETERSCENARIOSPATH + param.userId + '/' + fileName;
        if(fs.existsSync(UploadedFilePath)){           
            fs.unlinkSync(UploadedFilePath);
        }
        response.success = true;
        response.error = false;
        response.message = "Scenario's are deleted.";
    }catch(e){
        logger.debug("Error in deleteJMeterScenarios: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function getJMeterScriptSettings(param){
    let response = {};
    let scenarioData = {};
    let stringProp = {'ThreadGroup.num_threads' : 'runThreads', 'ThreadGroup.ramp_time' : 'rampUpTime', 'ThreadGroup.duration' : 'duration', 'ThreadGroup.delay' : 'delay'};
    let boolProp = {'ThreadGroup.scheduler': 'scheduler'};
    let elementProp = {'LoopController.loops' : 'loop', 'LoopController.continue_forever' : 'continueForever'};
    try{
        let fileName = param.scenarioName + '.jmx';
        let UploadedFilePath = JMETERSCENARIOSPATH + param.userId + '/' + fileName;
        let xmlData = fs.readFileSync(UploadedFilePath, 'utf8');
        let jsonData = await xml2js.parseStringPromise(xmlData);
        let ThreadGroup = jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup;

        for(let idx=0; idx < ThreadGroup.length; idx++){
            let threadData = {};
            threadData['scenario'] = ThreadGroup[idx].$.testname;
            ThreadGroup[idx].stringProp.forEach(function(data){
                if(stringProp.hasOwnProperty(data.$.name)){
                    threadData[stringProp[data.$.name]] = data._ == undefined ? '' : typeConvertion(data._);
                }
            });

            ThreadGroup[idx].boolProp.forEach(function(data){
                if(boolProp.hasOwnProperty(data.$.name)){
                    threadData[boolProp[data.$.name]] = data._ == undefined ? '' : typeConvertion(data._);
                }
            });

            ThreadGroup[idx].elementProp[0].boolProp.forEach(function(data){
                if(elementProp.hasOwnProperty(data.$.name)){
                    threadData[elementProp[data.$.name]] = data._ == undefined ? '' : typeConvertion(data._);
                }
            });

            ThreadGroup[idx].elementProp[0].stringProp.forEach(function(data){
                if(elementProp.hasOwnProperty(data.$.name)){
                    threadData[elementProp[data.$.name]] = data._ == undefined ? '' : typeConvertion(data._);
                }
            });

            scenarioData[ThreadGroup[idx].$.testname] = threadData;
        }
        response.success = true;
        response.error = false;
        response.message = scenarioData;
    }catch(e){
        logger.debug("Error in getJMeterScriptSettings: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

function typeConvertion(value){
    try{
        if(!isNaN(value)){
            return Number(value);
        }else if(value == 'true'){
            return true;
        }else if(value == 'false'){
            return false;
        }else{
            return value;
        }
    }catch(e){
        console.log(e);
    }
}

async function readRegions(param){
    let response = {};
    try{
        let regions = await getRegions();
        regions.forEach(function(region){
            if(region['is_default'] == true){
                region['showRegion'] = true;
                region['distribution'] = 100;
            }else{
                region['showRegion'] = false;
                region['distribution'] = 0;
            }
        });

        //get Jemter user total count.
        let userCountData = [];
        let fileName = param.scenarioName + '.jmx';
        let UploadedFilePath = JMETERSCENARIOSPATH + param.userId + '/' + fileName;
        let xmlData = fs.readFileSync(UploadedFilePath, 'utf8');
        let jsonData = await xml2js.parseStringPromise(xmlData);
        let ThreadGroup = jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup;
        for(let idx=0; idx < ThreadGroup.length; idx++){
            ThreadGroup[idx].stringProp.forEach(function(data){
                if(data.$.name.includes('num_threads')){
                    userCountData.push({userCount : typeConvertion(data._)});
                }
            });
        }
        response.success = true;
        response.error = false;
        response.message = {regionData : regions, scenarioData : userCountData};
    }catch(e){
        logger.debug("Error in readRegions: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function getRegions(){
    let regions = [];
    try{
        let queryText = "SELECT * FROM lt_region_details WHERE is_deleted = FALSE AND reservation <> total_availability AND os_type = 'FEDORA' ORDER BY region;";
        let queryParam = [];
        let result = await psqlAPM.fnDbQuery("getRegions", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            regions = result.rows;
        }
    }catch(e){
        logger.debug("Error in getRegions: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        throw e;
    }finally{
        return regions;
    }
}

async function uploadJmeterScript(param){
    let response = {};
    let arrJmeterFiles = [];
    let isJmxFileUpload = false;
    try{
        let responseUploadFile = await uploadJmeterFiles(param);
        if(responseUploadFile.success){
            arrJmeterFiles = responseUploadFile.message;
        }else{
            throw new Error(responseUploadFile.message);
        }
        let reponseScriptName;
        for(var idx = 0; idx<arrJmeterFiles.length; idx++){
            let fileName = arrJmeterFiles[idx];
            let fileExtension = fileName.substring(fileName.lastIndexOf('.')+1);
            let UploadedFilePath = JMETERSCENARIOSPATH + param.userId + '/' + fileName;
            if(fileExtension.toLowerCase() == 'jmx'){
                isJmxFileUpload = true;
                let responseValidateJMX = await validateUploadScripts(UploadedFilePath, param);
                if(!responseValidateJMX.success){
                    throw new Error(responseValidateJMX.message);
                }
                let JemterScenarioName = fileName.substring(0,fileName.lastIndexOf('.'));
                //insert Scenario Deatails.
                let enterpriseId = param.ent_id == 0 ? null : param.ent_id;
                let JmeterInsResponse = await insertScenarioName(JemterScenarioName, param.userId, enterpriseId);
                if(JmeterInsResponse.success){
                    let scenarioId = JmeterInsResponse.message;
                    reponseScriptName = await getScriptName(scenarioId, UploadedFilePath, param);
                }else{
                    throw new Error(JmeterInsResponse.message);
                }
            }
        }

        if(isJmxFileUpload){
            if(reponseScriptName.success){
                response = reponseScriptName;
            }else{
                throw new Error(reponseScriptName.message);
            }
        }else{
            response.success = true;
            response.error = false;
            response.message = "Csv are uploaded.";    
        }
        

    }catch(e){
        logger.debug("Error in uploadJmeterScript: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function uploadJmeterFiles(param){
    let response = {};
    try{
        let arrJmeterFiles = [];
        for(var i=0; i<param.upload_file_count; i++){
            let fileName = param['file_name_'+i];
            let fileExtension = fileName.substring(fileName.lastIndexOf('.')+1);
            if(!(fileExtension.toLowerCase() == 'jmx' || fileExtension.toLowerCase() == 'csv')){
                throw new Error('Script(s) were not uploaded.\n Files must be jmx/csv type.');
            }
            let JmeterScenarioPath = JMETERSCENARIOSPATH + param.userId;
            //Directory created.
            if (!fs.existsSync(JmeterScenarioPath)) {
                fs.mkdirSync(JmeterScenarioPath);
            }
            let JmeterScenarioXmlPath = JmeterScenarioPath+'/'+fileName;
            if(fs.existsSync(JmeterScenarioXmlPath)){
                //JMX file is delete process.
                fs.unlinkSync(JmeterScenarioXmlPath);
            }
            fs.writeFileSync(JmeterScenarioXmlPath, param['file_content_'+i]);
            arrJmeterFiles.push(fileName);
        }
        response.success = true;
        response.error = false;
        response.message = arrJmeterFiles;
    }catch(e){
        logger.debug("Error in uploadJmeterFiles: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function validateUploadScripts(UploadedFilePath, param){
    let response = {};
    let list = [];
    try{
        let fileData = fs.readFileSync(UploadedFilePath, 'utf8');
        let json = await xml2js.parseStringPromise(fileData);
        let hashTree = json.jmeterTestPlan.hashTree[0].hashTree[0].hashTree;
        for(let idx=0; idx < hashTree.length; idx++){
            if(hashTree[idx].CSVDataSet != undefined){
                hashTree[idx].CSVDataSet.forEach(function(CSVDataSet){
                    CSVDataSet.stringProp.forEach(function(stringProp){
                        if(stringProp.$.name == 'filename'){
                            let fileName = stringProp._;
                            list.push(fileName);
                        }
                    });
                });               
            }
        }
        if(list.length > 0){
            CsvParamFilePath = JMETERSCENARIOSPATH + param.userId + '/';
            for(let i=0; i<list.length; i++){
                if (!fs.existsSync(CsvParamFilePath+list[i])) {
                    throw new Error('Script(s) were not uploaded. Execption is file not found : '+list[i]);
                }
            }
        }

        response.success = true;
        response.error= false;
        response.message= 'Scripts validate sucessfull.';
    }catch(e){
        logger.debug("Error in validateUploadScripts : "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}

async function getScriptName(scenarioId, UploadedFilePath, param){
    let response = {};
    let arrScriptId = [];
    try{
        let xmlData = fs.readFileSync(UploadedFilePath, 'utf8');
        let jsonData = await xml2js.parseStringPromise(xmlData);
        let ThreadGroup = jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup;
        for(let idx=0; idx < ThreadGroup.length; idx++){
            let scriptName = ThreadGroup[idx].$.testname.toLowerCase();
            let ScriptInsResponse = await insertScriptName(scriptName, param.userId, param.ent_id);
            if(ScriptInsResponse.success){
                let scriptId = ScriptInsResponse.message;
                arrScriptId.push(scriptId);
            }else{
                throw new Error(ScriptInsResponse.message);
            }
        }

        let mappedScriptsResponse = await mappingScripts(scenarioId, arrScriptId, param.userId)
        if(mappedScriptsResponse.success){
            response.success = true;
            response.error = false;
            response.message = "Script are uploaded";
        }else{
            throw new Error(mappedScriptsResponse.message);
        }

    }catch(e){
        logger.debug("Error in getScriptName: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}
        
async function mappingScripts(scenario_id, arrScriptId, userid){
    let response = {};
    try{
        let qryText = "DELETE FROM lt_scenario_script_mapping WHERE scenario_id = $1 AND created_by = $2";
        let qryParam = [scenario_id, userid];
        let deleteResult = await psqlAPM.fnDbQuery("deleteMappedScripts", qryText, qryParam, null, null);

        let queryText = "INSERT INTO lt_scenario_script_mapping (scenario_id, script_id, scenario_settings, created_by, created_on) VALUES ";
        arrScriptId.map((scriptId,idx) => {
            queryText += idx != 0 ? "," : "";
            queryText += "("+scenario_id+","+scriptId+", '{\"browsercache\":\"false\", \"currentloadgenid\":\"1\", \"durationtime\":\"0;1;0\", \"incrementtime\":\"0;0;1\", \"incrementuser\":\"1\", \"iterations\":\"1\", \"maxuser\":\"5\", \"startuser\":\"1\", \"startuserid\":\"0\", \"totalloadgen\":\"1\", \"type\":\"1\"}', "+userid+", now())";
        });
        let queryParam = [];
        let result = await psqlAPM.fnDbQuery("insertMappingScripts", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            queryText = "UPDATE lt_scenario_master SET v_users = (SELECT SUM((scenario_settings->>'maxuser')::bigint) AS vuser FROM lt_scenario_script_mapping WHERE created_by = $1 AND scenario_id = $2) WHERE user_id = $3 AND scenario_id = $4 AND scenario_type = 'JMETER'";
            queryParam = [userid, scenario_id, userid, scenario_id];
            let updResult = await psqlAPM.fnDbQuery("updateVuser", queryText, queryParam, null, null);
            if (result.success && result.rowCount>0){
                response.success = true;
                response.error= false;
                response.message= 'Sucessfully insert Mapping Scripts.';
            }
        }
    }catch(e){
        logger.debug("Error in mappingScripts: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}

async function insertScriptName(scriptName, userId, enterpriseId){
    let response = {};
    try{
        let queryText = "SELECT script_id FROM lt_script_master WHERE user_id = $1 AND script_type = $2 AND LOWER(script_name) = LOWER($3)";
        let queryParam = [userId, 'JMETER', scriptName];
        let validateResult = await psqlAPM.fnDbQuery("IsExistsScriptName", queryText, queryParam, null, null);
        if(validateResult.success && validateResult.rowCount > 0){
            response.success = true;
            response.error= false;
            response.message= validateResult.rows[0].script_id;
        }else if(validateResult.success){
            let queryText = "INSERT INTO lt_script_master (user_id, script_name, script_type, created_by, created_on, enterprise_id) VALUES ($1, $2, $3, $4, now(), $5) RETURNING script_id";
            let queryParam = [userId, scriptName, 'JMETER', userId, enterpriseId];
            let result = await psqlAPM.fnDbQuery("insertScriptName", queryText, queryParam, null, null);
            if (result.success && result.rowCount>0){
                response.success = true;
                response.error= false;
                response.message= result.rows[0].script_id;
            }
        }
    }catch(e){
        logger.debug("Error in insertScriptName: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}

async function updateJMeterScenarioSettings(param){
    let response = {};
    let isXmlUpdate = false;
    try{
        let stringProp = {'ThreadGroup.num_threads' : 'runThreads', 'ThreadGroup.ramp_time' : 'rampUpTime', 'ThreadGroup.duration' : 'duration', 'ThreadGroup.delay' : 'delay'};
        let boolProp = {'ThreadGroup.scheduler': 'scheduler'};
        let elementProp = {'LoopController.loops' : 'loop', 'LoopController.continue_forever' : 'continueForever'};

        let fileName = param.scenarioName + '.jmx';
        let UploadedFilePath = JMETERSCENARIOSPATH + param.userId + '/' + fileName;
        let xmlData = fs.readFileSync(UploadedFilePath, 'utf8');
        let jsonData = await xml2js.parseStringPromise(xmlData);
        let ThreadGroup = jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup;
        let scriptData = JSON.parse(param.scriptData);
        
        if(ThreadGroup.length > 0 && ThreadGroup.length == Object.keys(scriptData).length){
            for(let idx=0; idx < ThreadGroup.length; idx++){
                let scenarioName = ThreadGroup[idx].$.testname;
                if(scriptData.hasOwnProperty(scenarioName)){
                    isXmlUpdate = true;
                    jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup[idx].stringProp.forEach(function(data){
                        if(stringProp.hasOwnProperty(data.$.name)){
                            data._ = scriptData[scenarioName][stringProp[data.$.name]];
                        }
                    });

                    jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup[idx].boolProp.forEach(function(data){
                        if(boolProp.hasOwnProperty(data.$.name)){
                            data._ = scriptData[scenarioName][boolProp[data.$.name]];
                        }
                    });

                    jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup[idx].elementProp[0].boolProp.forEach(function(data){
                        if(elementProp.hasOwnProperty(data.$.name)){
                            data._ = scriptData[scenarioName][elementProp[data.$.name]];
                        }
                    });

                    jsonData.jmeterTestPlan.hashTree[0].hashTree[0].ThreadGroup[idx].elementProp[0].stringProp.forEach(function(data){
                        if(elementProp.hasOwnProperty(data.$.name)){
                            data._ = scriptData[scenarioName][elementProp[data.$.name]];
                        }
                    });
                }
            }
            if(isXmlUpdate){

                let backUpPath = fs.existsSync(UploadedFilePath+'_original') ? UploadedFilePath+'_'+datePartition() : UploadedFilePath+'_original';
                fs.copyFileSync(UploadedFilePath, backUpPath);
    
                var builder = new xml2js.Builder();
                var xml = builder.buildObject(jsonData);
                fs.writeFileSync(UploadedFilePath, xml, 'utf8');
            }
        }else{
            throw new Error('Problem while processing, kindly re-open the configuration to continue.');
        }
        response.success = true;
        response.error= false;
        response.message= "Scenarios updated successfully";
    }catch(e){
        logger.debug("Error in updateJMeterScenarioSettings: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}

function datePartition(){
    try{
        let date = new Date();
        return date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2( date.getDate()) + pad2( date.getHours() ) + pad2( date.getMinutes() ) + pad2( date.getSeconds() );
    }catch(e){
        console.log(e);
    }
}

function pad2(n) { 
    return n < 10 ? '0' + n : n 
}

async function insertScenarioName(scenarioName, userId, enterpriseId){
    let response = {};
    try{
        let queryText = "SELECT scenario_id FROM lt_scenario_master WHERE user_id = $1 AND scenario_type = $2 AND LOWER(scenario_name) = LOWER($3)";
        let queryParam = [userId, 'JMETER', scenarioName];
        let validateResult = await psqlAPM.fnDbQuery("IsExistsScenarioName", queryText, queryParam, null, null);
        if(validateResult.success && validateResult.rowCount > 0){
            response.success = true;
            response.error= false;
            response.message= validateResult.rows[0].scenario_id;
        }else if(validateResult.success){
            let queryText = "INSERT INTO lt_scenario_master (user_id, enterprise_id, scenario_type, scenario_name, created_on) VALUES ($1, $2, $3, $4, now()) RETURNING scenario_id";
            let queryParam = [userId, enterpriseId, 'JMETER', scenarioName];
            let result = await psqlAPM.fnDbQuery("insertScenarioName", queryText, queryParam, null, null);
            if (result.success && result.rowCount>0){
                response.success = true;
                response.error= false;
                response.message= result.rows[0].scenario_id;
            }
        }
    }catch(e){
        logger.debug("Error in getUserIdFromSystemId method: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}

async function runScenario(param){
    let response = {};
    try{
        let jmeterData = {};
        let scriptName = '', scriptId = '';
        let scriptDetails = await getScriptDetails(param.scenarioId);
        scriptDetails.map((key, idx) => {
            if (idx != 0){
                scriptName += ","
                scriptId += ","
            }
            scriptName += key.scriptname;
            scriptId += key.scriptid;
        });

        jmeterData.TestType = param.testType;
        jmeterData.LoadgenIpAddress = '';
        jmeterData.ScenarioId = param.scenarioId;
        jmeterData.ScenarioName = param.scenarioName;
        jmeterData.ReportName = param.reportName;
        jmeterData.strLicense = 'level3';
        jmeterData.userId = param.userId;
        jmeterData.strLoadGenRegions = param.regions;
        jmeterData.strDistributions = param.distributions;
        jmeterData.maxUserCount = param.maxUserCount;
        jmeterData.runType = param.runType;
        jmeterData.startMonitor = param.startMonitor;
        jmeterData.endMonitor = param.endMonitor;
        jmeterData.scriptId = scriptId;
        jmeterData.scriptName = scriptName;
        jmeterData.userAgent = param.userAgent;

        let reportDetails = await insertIntoReportMaster(jmeterData);
        if(reportDetails.success){
            jmeterData.lRunId = reportDetails.message;
        }else{
            throw new Error(reportDetails.message);
        }
        let runJmeter = await putTestInQueue(jmeterData);
        if(runJmeter.success){
            response.success = true;
            response.error = false;
            response.message = "Your test is in queue, as soon as the Run starts, status will change to GREEN";
        }else{
            throw new Error(runJmeter.message);
        }
    }catch(e){
        logger.debug("Error in runScenario: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function getScriptDetails(scenarioId){
    let scriptDetails;
    try{
        let queryText = "SELECT sc.script_id AS scriptId, sc.script_name AS scriptName FROM lt_scenario_script_mapping sm INNER JOIN lt_script_master sc ON sc.script_id = sm.script_id AND sm.created_by = sc.user_id WHERE scenario_id = $1";
        let queryParam = [scenarioId];
        let result = await psqlAPM.fnDbQuery("getScriptDetails", queryText, queryParam, null, null);
        if (result.success && result.rowCount>0){
            scriptDetails = result.rows;
        }
    }catch(e){
        logger.debug("Error in getUserIdFromSystemId method: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        throw e;
    }finally{
        return scriptDetails;
    }
}

async function insertIntoReportMaster(jMeterData){
    let runId;
    let response = {};
    try{
        response.success = true;
        response.error = false;
        let queryText = "SELECT true from tblreportmaster where lower(reportname)=LOWER($1) AND userid=$2 AND is_deleted=false";
        let queryParam = [jMeterData.ReportName, jMeterData.userId];
        let result = await psqlAPM.fnDbQuery("isReportNameExist", queryText, queryParam, null, null);
        if (result.success){
            if(result.rowCount>0){
                throw new Error('Report Name already exist.');
            }else{
                queryText = "INSERT INTO tblreportmaster (userid, reportname, reporttype, scenarioname, created_by,created_on, is_active, runstarttime, scenario_id, runtype, start_monitor_in_mins, end_monitor_in_mins) VALUES ($1, $2, $3, $4, $5, now(), true, now(), $6, $7, $8, $9) RETURNING runid";
                queryParam = [jMeterData.userId, jMeterData.ReportName , jMeterData.TestType , jMeterData.ScenarioName, jMeterData.userId, jMeterData.ScenarioId, jMeterData.runType, jMeterData.startMonitor, jMeterData.endMonitor];
                let result1 = await psqlAPM.fnDbQuery("insertIntoReportMaster", queryText, queryParam, null, null);
                if (result1.success && result1.rowCount>0){
                    response.message = result1.rows[0].runid;
                }
            }
        }
    }catch(e){
        logger.debug("Error in insertIntoReportMaster: "+e.stack);
        logger.error(process.pid+' : '+e.stack);
        response.success = false;
        response.error = true;
        response.message = e.message;
    }finally{
        return response;
    }
}

async function putTestInQueue(jmeterData){
    let response = {};
    try{
        var url = appedoConfigProperties.LT_QUEUE_SERVICES + '/ltQueue/putTestInQueue';
        const params = new URLSearchParams();
        params.append('testBean', JSON.stringify(jmeterData));
        let option ={
            method : 'POST',
            body : params
        };
        let ltQueueResponse = await fetch(url, option);
        
        if(ltQueueResponse.status == 200){
            response.success = true;
            response.error= false;
            response.message= 'Sucessfully sent Jmeter data.';
        }else{
            response.success = false;
            response.error= true;
            response.message= 'failed to sent Jmeter data.';
        }
    }catch(e){
        logger.debug("Error in getUserIdFromSystemId method: "+e.stack);
        response.success = false;
        response.error= true;
        response.message= e.message;
    }finally{
        return response;
    }
}