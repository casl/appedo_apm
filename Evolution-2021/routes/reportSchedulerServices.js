const Router = require('express-promise-router')
const psqlAPM = require('./psqlAPM');
const PgConfig = require('../config/apd_constants');
const appedoMailer = require('./appedoMailer');
const pgCustom = require('./psqlCustom');
const { Parser } = require('json2csv');
const json2csvParser = new Parser();
const fs = require('fs');
const util = require('util');
const { data } = require('../log');
const { Buffer } = require('buffer');
const writeFile = util.promisify(fs.writeFile);
//global.logger = require('../log');
const log = require('../log');
var logger = log.reportLogger; 

// it allows you to use async functions as route handlers
const router = new Router()

// export our router to be mounted by the parent application
module.exports = router;
module.exports.startReportService = startReportService;


SCHEDULER_SERVICE_INTERVAL_MS = 1000 * 60 * 10; // 10 Minutes
//PgConfig.ReportSchedulerRunningMode ? setInterval(startReportService, SCHEDULER_SERVICE_INTERVAL_MS) : '';
//PgConfig.ReportSchedulerRunningMode ? appedoMailer.loadMailTemplate() : '';
let EMAIL_ATTACHMENT_PATH = PgConfig.resourcePath+PgConfig.attachementPath;
logger.info('Report Scheduler running mode is : %s', PgConfig.ReportSchedulerRunningMode);
//log.info('Report Scheduler running mode is :'+ PgConfig.ReportSchedulerRunningMode);
//logger.log('info', 'Report Scheduler running mode is :', PgConfig.ReportSchedulerRunningMode);

//SCHEDULER_SERVICE_START_TIME_MS = (60*60*1000) - (new Date().getTime()%(60*60*1000));
SCHEDULER_SERVICE_START_TIME_MS = (10*60*1000) - (new Date().getTime()%(10*60*1000));

//var start_interval = setInterval(startReportService, SCHEDULER_SERVICE_START_TIME_MS);
//startReportService();

function startReportService() {
    if(PgConfig.ReportSchedulerRunningMode){
        setInterval(reportSchedulerTimerStart, SCHEDULER_SERVICE_INTERVAL_MS);
        reportSchedulerTimerStart();
    }
    //clearInterval(start_interval);
};

async function reportSchedulerTimerStart(){
    try{
        //let query_text = "select id, connector_id, query_text, created_by, is_scheduled, frequency, email_arr, enable_scheduler, report_title, report_description, send_as_attachment, last_email_sent_on, is_success, time_offset from custom_chart_queries where is_scheduled AND enable_scheduler AND (CASE WHEN last_email_sent_on IS NULL THEN true WHEN lower(frequency) = 'hourly' THEN last_email_sent_on + interval '1 hour' <= now() WHEN lower(frequency) = 'daily' THEN last_email_sent_on + interval '1 day' <= now() WHEN lower(frequency) = 'weekly' THEN last_email_sent_on + interval '7 day' <= now() WHEN lower(frequency) = 'fortnightly' THEN last_email_sent_on + interval '14 day' <= now() WHEN lower(frequency) = 'monthly' THEN last_email_sent_on + interval '1 month' <= now() WHEN lower(frequency) = 'quarterly' THEN last_email_sent_on + interval '3 month' <= now() WHEN lower(frequency) = 'halfyearly' THEN last_email_sent_on + interval '6 month' <= now() WHEN lower(frequency) = 'yearly' THEN last_email_sent_on + interval '12 month' <= now() END)";
        let query_text = "select id, connector_id, query_text, created_by, is_scheduled, frequency, email_arr, enable_scheduler, report_title, report_description, send_as_attachment, last_email_sent_on, is_success, time_offset, EXTRACT(EPOCH FROM last_email_sent_on) AS epoch_time from custom_chart_queries where id = 1056";
        let queryParam = [];
        let result = await psqlAPM.fnDbQuery('reportScheduler-TimerStart',query_text, queryParam, null, null);
        if(result.rowCount > 0){
            console.log(result.rows)
            await executeReport(result.rows);
        }else{
            logger.info(process.pid+" There is now no new reports available.");
        }
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
    }
};

async function executeReport(reports){
    try{
        let mailBody; 
        let reportData = {};
        for(let j=0;j<reports.length;j++) {
            let report = reports[j];
            let DbConnectionString = await getDBConnectorForDbId(report.connector_id);
            report.connectionString = DbConnectionString.connection_details;
            let queryRst = await runQuery(report);
            console.log(queryRst);
            if(queryRst.length != 0){
                if(report.send_as_attachment){
                    if(PgConfig.mailing_system.toLowerCase().includes('sendwithus')){
                        let csvData = json2csvParser.parse(queryRst);
                        let buff = new Buffer(csvData);
                        //let buff = new Buffer.alloc()
                        let base64data = buff.toString('base64');
                        report.attachment_detail = {id : report.report_description+'.csv', data : '{'+base64data+'}'};
                    }else if(PgConfig.mailing_system.toLowerCase().includes('smtp')){
                    	await CreateCsv(report.id, queryRst);
                    	report.attachment_detail = {filename: report.report_description+'.csv', path: EMAIL_ATTACHMENT_PATH+'report_'+report.id+'.csv'};
                    }
                }else{
                    mailBody = await mailBodyContent(report, queryRst);
                    report.mailBody = mailBody;
                }
                reportData[report.id] = report;
                reports[j].qry_rst = true;
            }else{
                logger.info(process.pid+" There is no query result in this report-id : "+ report.id);
                console.log(process.pid+" There is no query result in this report-id : "+ report.id);
                reports[j].qry_rst = false;
            }
        }
        let MailReport = await createMailWiseRportId(reports);

        console.log(MailReport);
        console.log(reportData);
        if(Object.keys(MailReport).length != 0 && Object.keys(reportData).length != 0){
            await sendMailWiseRports(MailReport, reportData);
        }else{
            Object.keys(MailReport).length == 0 ? logger.info(process.pid+" There is no Email-ID in current reports") : '';
        }

    }catch(e){
        logger.error(process.pid+' : '+e.stack);
    }
}

async function getDBConnectorForDbId(dbId){
    let rst;
    try{
        const query_text = "SELECT db.db_id, db.connector_name, db.engine_name, db.connection_details, db.user_name, db.password, EXTRACT(EPOCH FROM db.created_on)*1000 as created_on, db.modified_on FROM db_connector_details db WHERE db.db_id = $1"
        const queryParam = [dbId];
        let result = await psqlAPM.fnDbQuery('getDBConnectorForDbId',query_text, queryParam, null, null)
          if (result.rowCount > 0){
            rst = result.rows[0]
          } else {
            rst = 'No rows return';
          }
    } catch (e) {
        logger.error(process.pid+' : '+e.stack);
    } finally {
        return rst;
    }
}

async function runQuery_new(queryParam){
    try{
        if(queryParam.last_email_sent_on != null){

        }else{

        }
        if(queryParam.queryText.includes('@startDate') && queryParam.queryText.includes('@endDate@')){
          let endDate = new Date().getTime()/1000;
          let startDate = (new Date().getTime()-60*60*1000)/1000;
          queryParam.queryText = queryParam.queryText.replace('@startDate@',startDate).replace('@endDate@',endDate);
        } else if(queryParam.queryText.includes('@startDay@')){
          let today = new Date();
          let startDay = new Date(new Date(today.getFullYear(), today.getMonth(), today.getDate()).getTime() + queryParam.timeOffset).toISOString();
          queryParam.queryText = queryParam.queryText.replace('@startDay@',startDay).replace('@endDay@',today.toISOString());
        } else if(queryParam.queryText.includes('@startWeek@')){
          let stWeekDate = startOfWeek(new Date());
          let startWeek = new Date(stWeekDate.getTime() + queryParam.timeOffset).toISOString();
          queryParam.queryText = queryParam.queryText.replace('@startWeek@',startWeek).replace('@endWeek@',new Date().toISOString());
        } else if(queryParam.queryText.includes('@startMonth@')){
          let dt = new Date();
          let stMonth = new Date(dt.getFullYear(), dt.getMonth());
          let startMonth = new Date(stMonth.getTime() + queryParam.timeOffset).toISOString();
          queryParam.queryText = queryParam.queryText.replace('@startMonth@',startMonth).replace('@endMonth@',new Date().toISOString());
        }
    } catch (e) {
        console.log(e);
        logger.error(process.pid+' : '+e.stack);
    }finally {
        return rst;
    }
}

async function runQuery(queryParam){
    let rst;
    try{
        console.log('last_email_sent_on : '+ queryParam.last_email_sent_on);
        console.log('frequency : '+ queryParam.frequency);

        let endDate = new Date().getTime()/1000;
        let startDate = (new Date().getTime()-24*60*60*1000)/1000;
        console.log(new Date());
        console.log(startDate);
        console.log(endDate);
        console.log('toISOString() : ', new Date().toISOString());
        console.log(new Date(1602644399999));
        if(queryParam.last_email_sent_on != null){
            console.log('is not null value');
        }else{
            console.log('is null value');
        }

        let updated_endData;
        let epochTime;
        queryParam.frequency = 'daily'; 
        if(queryParam.frequency.toLowerCase() == 'hourly'){
            epochTime = new Date().getTime() - (new Date().getTime()%(60*60*1000));
        }else{
            epochTime = new Date().getTime() - (new Date().getTime()%(24*60*60*1000));
        }

        updated_endData = new Date(epochTime-1);
        queryParam.updated_email_sent_on = updated_endData;

        if(queryParam.query_text.includes('@startDate') && queryParam.query_text.includes('@endDate@')){
          //let endDate = new Date().toISOString();
          let endDate = updated_endData.toISOString(); 
          let startDate = new Date(new Date().getTime()-60*60*1000).toISOString();
          queryParam.query_text = queryParam.query_text.replace('@startDate@',startDate).replace('@endDate@',endDate);
        } else if(queryParam.query_text.includes('@startDay@')){
          let today = new Date();
          let startDay = new Date(new Date(today.getFullYear(), today.getMonth(), today.getDate()).getTime() + queryParam.time_offset).toISOString();
          queryParam.query_text = queryParam.query_text.replace('@startDay@',startDay).replace('@endDay@',today.toISOString());
        } else if(queryParam.query_text.includes('@startWeek@')){
          let stWeekDate = startOfWeek(new Date());
          let startWeek = new Date(stWeekDate.getTime() + queryParam.time_offset).toISOString();
          queryParam.query_text = queryParam.query_text.replace('@startWeek@',startWeek).replace('@endWeek@',new Date().toISOString());
        } else if(queryParam.query_text.includes('@startMonth@')){
          let dt = new Date();
          let stMonth = new Date(dt.getFullYear(), dt.getMonth());
          let startMonth = new Date(stMonth.getTime() + queryParam.time_offset).toISOString();
          queryParam.query_text = queryParam.query_text.replace('@startMonth@',startMonth).replace('@endMonth@',new Date().toISOString());
        }
    
        console.log(queryParam.query_text);
        let result = await pgCustom.fnDbQuery('runQuery-reportScheduler',queryParam.connectionString, queryParam.query_text, [], null, null);

        if (result.success){
            rst = result.rows;
        }else{
            rst = [];
        }
    } catch (e) {
        logger.error(process.pid+' : '+e.stack);
    }finally {
        return rst;
    }
};

async function sendMailWiseRports(mailIds, reports){
    try{
        let EmailIds = Object.keys(mailIds);
        for(let i =0; i<EmailIds.length; i++ ){
            let mailID = EmailIds[i];
            let mailBody = '';
            let attachement_body = '';
            let is_attachemet = false;
            let attachement_field = [];
            let subject = '';
            let mailData = {};

            attachement_body += '<table width=\"100%\"><tbody><tr><td class=\"left\" style=\"padding-top:20px; padding-bottom:5px; text-align:left;\">';
            attachement_body += 'Herewith enclosing the status report. Below the report enclosed.</td></tr>';
            mailIds[mailID].map(reportId => {
                subject = reports[reportId].report_title;
                if(reports[reportId].send_as_attachment){
                    attachement_field.push(reports[reportId].attachment_detail)
                    attachement_body += '<tr><td style=\"padding-top:5px; padding-left:45px; text-align:left;\">'
                    attachement_body += '<b>'+reports[reportId].report_description+' ('+reports[reportId].frequency+')</b>';
                    attachement_body += '</td></tr>'
                    is_attachemet = true;
                }else{
                    mailBody += reports[reportId].mailBody;
                }
            });
            attachement_body += '</tbody></table>';

            if(is_attachemet){
                mailBody += attachement_body;
            }
            if(mailIds[mailID].length > 1){
                subject = 'Scheduled Reports';
            }

            mailData.subject = subject;
            mailData.email_id = mailID;
            mailData.mail_body = mailBody;
            mailData.attachments = attachement_field;
            appedoMailer.sendReportSchedulerMail(appedoMailer.MODULE_ID.VIS_REPORT_EMAIL, mailData);
        }
        await updateLastSentTime(reports);
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
    }
}

async function updateLastSentTime(reports){
    try{
        let reportIds = Object.keys(reports);
        reportIds.map(async reportId => {
            let query_text = "update custom_chart_queries set last_email_sent_on = $1 WHERE id = $2";
            let queryParam = [reports[reportId].updated_email_sent_on, reportId];
            let result = await psqlAPM.fnDbQuery('updateLastSentTime - reportID '+reportId,query_text, queryParam, null, null);
            if(result.rowCount > 0){
                logger.info(process.pid+' Update successful for last_email_sent_on in this report id : '+ reportId);
            }else{
                logger.error(process.pid+' Unable to update for last_email_sent_on in this report id : '+ reportId);
            }
        });
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
    }
}

async function createMailWiseRportId(reports){
    let mailIds = {};
    try{
        reports.map( report => {
            report.email_arr == null ? report.email_arr = [] : report.email_arr;
            if(report.email_arr.length == 0){
                logger.info(process.pid+" There is no Email-Id in this report : "+ report.id);
            }
            if(report.qry_rst){
                report.email_arr.map(mailId => {
                    if(mailIds.hasOwnProperty(mailId)){
                        mailIds[mailId].push(report.id);
                    }else{
                        mailIds[mailId] = [report.id];
                    }
                });
            }
        })
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
        throw e;
    }
    return mailIds;
}

async function CreateCsv(reportId, queryRst){
    try{
        let csvData = json2csvParser.parse(queryRst);
        let filename = 'report_'+reportId+'.csv';
        await writeFile(EMAIL_ATTACHMENT_PATH + filename, csvData, 'utf8');
    }catch(e){
        logger.error(process.pid+' CreateCsv() : '+e.stack);
    }
}

async function mailBodyContent(report, queryRst){
    let mailBody = '';
    try{
        //Report Details
        mailBody += '<br><table id=\"t01\"><tr id=\"t01\"><th id=\"t01\">Report Title:</th>';
        mailBody += '<td id=\"t01\">'+report.report_title+'</td></tr>';
        mailBody += '<tr id=\"t01\" style = \"background-color: #eee\"><th id=\"t01\">Frequency:</th>';
        mailBody += '<td id=\"t01\">'+report.frequency+'</td></tr>';
        mailBody += '<tr id=\"t01\"><th id=\"t01\">Description:</th>';
        mailBody += '<td id=\"t01\">'+report.report_description+'</td></tr></table><br>';

        //Result Details
        mailBody += '<table id=\"t01\"><tr id=\"t01\">';
        let qryColumn = Object.keys(queryRst[0]);
        qryColumn.map(key => {
            mailBody += '<th id=\"t01\">'+key+'</th>';
        });
        mailBody += '</tr>';
        for(let i=0; i<queryRst.length;i++){

            mailBody += (i%2 == 0) ? '<tr id=\"t01\" style = \"background-color : #f2f2f2\">' : '<tr>';
            Object.keys(queryRst[i]).map(key => {
                if(queryRst[i][key] == 'Up' || queryRst[i][key] == 'Ok'){
                    mailBody += '<td id=\"t01\" style=\"background :#00b050;\">'+queryRst[i][key]+'</td>';
                }else if(queryRst[i][key] == 'Down'){
                    mailBody += '<td id=\"t01\" style=\"background :red;\">'+queryRst[i][key]+'</td>';
                }else{
                    mailBody += '<td id=\"t01\">'+queryRst[i][key]+'</td>';
                }
            });
            mailBody += '</tr>';
        }
        mailBody += '</table><br>';
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
    }finally{
        return mailBody;
    }
}

function startOfWeek(date)
{
  let diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
  let stWeekDate = new Date(date.setDate(diff));
  return new Date(stWeekDate.getFullYear(), stWeekDate.getMonth(), stWeekDate.getDate());
}