const nodemailer = require('nodemailer');
//global.logger = require('../log');
const log = require('../log');
var logger = log.appedoMailerLogger;
const fs = require('fs');
const PgConfig = require('../config/apd_constants');
const psqlAPM = require('./psqlAPM');
const request = require('request');
const SWU_API =  require('sendwithus')(PgConfig.swuConfig.SWU_API_KEY);

module.exports.sendReportSchedulerMail = sendReportSchedulerMail;
module.exports.sendMail = sendMail;
module.exports.sendSMS = sendSMS;

let EMAIL_TEMPLATES_PATH = PgConfig.resourcePath+PgConfig.emailTemplatePath;
let MODULE_ID = {
    VERIFY_USER_EMAIL: {TEMPLATE_ID : '', HTML_FILE: 'verify_user_email.html'},
    ENTERPRISE_SIGNUP_INVITATION: {TEMPLATE_ID : '', HTML_FILE: 'enterprise_signup_invitation.html'},
    ENTERPRISE_INVITATION: {TEMPLATE_ID : '', HTML_FILE: 'enterprise_invitation.html'},
    PASSWORD_RESET : {TEMPLATE_ID : '', HTML_FILE: 'password_reset.html'},
    SLA_VERIFY_EMAIL : {TEMPLATE_ID : '', HTML_FILE : 'sla_verify_email.html'},
    AVM_ALERT_EMAIL : {TEMPLATE_ID : '', HTML_FILE : 'avm_service_alert_email.html'},
    VIS_REPORT_EMAIL : {TEMPLATE_ID : '', HTML_FILE : 'vis_report_status_email.html'}
};
let APPEDO_WHITE_LABELS = {}

module.exports.MODULE_ID = MODULE_ID;
const transport = nodemailer.createTransport(PgConfig.smtpConfig);

const message = {
    from: '', // Sender address
    //to: '',        // List of recipients
    //subject: 'Design Your Model S | Tesla', // Subject line
    //text: 'Have the most fun you can in a car. Get your Tesla today!' // Plain text body
};

loadAppedoWhiteLabels();
async function loadAppedoWhiteLabels(){
    try{
        if (Object.keys(APPEDO_WHITE_LABELS).length == 0) {
            const queryText = 'SELECT key, value FROM appedo_whitelabel';
            const queryParam = [];
            psqlAPM.fnDbQuery('getAppedoWhiteLabels',queryText, queryParam, null, null).then( async result => {
                if ( result.rowCount > 0 ){
                    result.rows.map(item => APPEDO_WHITE_LABELS[item.key] = item.value);
                } else {
                    if (result.error){
                        logger.error("Failed. loadAppedoWhiteLabels() will be retried after 10 sec");
                        setTimeout(() => {
                            loadAppedoWhiteLabels();
                        }, 10000);
                    }
                }
            });
        }
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
        logger.error("Failed. loadAppedoWhiteLabels() will be retried after 10 sec")
        setTimeout(() => {
            loadAppedoWhiteLabels();
        }, 10000);
    }
}

async function sendReportSchedulerMail(module_template, mailDetails){
    try{
        if(PgConfig.mailing_system.toLowerCase().includes('sendwithus')){
            console.log('sendwithus');
            mailDetails = await addAppedoWhitelabelsEmailReplacements(mailDetails);
            let mailParam = {
                template: module_template.TEMPLATE_ID,
                recipient: { address: mailDetails.email_id},
                template_data: mailDetails,
                esp_account: PgConfig.swuConfig.SWU_ESP_ACCOUNT,
                sender : {
                    address: PgConfig.swuConfig.swu_sender_emailid, // required
                    name: PgConfig.swuConfig.SWU_SENDER_NAME,
                    reply_to: PgConfig.swuConfig.swu_reply_to
                },
                files : mailDetails.attachments
            }
            sendMailThroughSWU(mailParam);
        }else if(PgConfig.mailing_system.toLowerCase().includes('smtp')){
            mailDetails = await addAppedoWhitelabelsEmailReplacements(mailDetails);
            let mailData = await replaceMailVariables(module_template.HTML_FILE, mailDetails, mailDetails.subject);
            let result = await sendMailThroughSMTP(mailDetails.email_id, mailData.subject, mailData.mailContent, mailDetails.attachments);
        }
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
        logger.error("Failed. sendMail() : "+e.stack);
        console.log(e);
    }
}

async function sendMail(module_template, mailDetails, email_id, mailSubject){
    try{
        if(PgConfig.mailing_system.toLowerCase().includes('sendwithus')){
            mailDetails = await addAppedoWhitelabelsEmailReplacements(mailDetails);
            let mailParam = {
                template: module_template.TEMPLATE_ID,
                recipient: { address: email_id},
                template_data: mailDetails,
                esp_account: PgConfig.swuConfig.SWU_ESP_ACCOUNT,
                sender : {
                    address: PgConfig.swuConfig.swu_sender_emailid, // required
                    name: PgConfig.swuConfig.SWU_SENDER_NAME,
                    reply_to: PgConfig.swuConfig.swu_reply_to
                }
            }
            sendMailThroughSWU(mailParam);
        }else if(PgConfig.mailing_system.toLowerCase().includes('smtp')){
            mailDetails = await addAppedoWhitelabelsEmailReplacements(mailDetails);
            let mailData = await replaceMailVariables(module_template.HTML_FILE, mailDetails, mailSubject);
            let attachement_field = [];
            let result = await sendMailThroughSMTP(email_id, mailData.subject, mailData.mailContent, attachement_field);
        }
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
        logger.error("Failed. sendMail() : "+e.stack);
    }
}

async function replaceMailVariables(fileName, mailDetails, subject){
    try{
        let respose = {};
        let mailContent = fs.readFileSync(EMAIL_TEMPLATES_PATH+fileName, "utf-8");        
        mailContent = mailContent.replace(/(\r\n|\n|\r)/gm,"");
        
        for(var key in mailDetails){
            let replace = new RegExp('{{'+key+'}}', 'gi');
            let replace2 = new RegExp('{{ '+key+' }}', 'gi');

            mailContent = mailContent.replace(replace, mailDetails[key]).replace(replace2, mailDetails[key]);
            subject = subject.replace(replace, mailDetails[key]).replace(replace2, mailDetails[key]);
        }
        respose.mailContent = mailContent;
        respose.subject = subject;
        return respose;
    }catch(e){
        console.log(e);
        logger.error(process.pid+' : '+e.stack);
        logger.error("Failed replaceMailVariable() : "+e.stack);
    }
}

async function sendMailThroughSMTP(to_address, subject, mailBody, attachment_data){
    let rtnResult;
    try{
        message.to = to_address;
        message.subject = subject;
        message.html = mailBody;
        message.attachments = attachment_data;
        const result = await transport.sendMail(message);
        result.success = true;
        result.error = false; 
        rtnResult = result;
        console.log(result);
    }catch(e){
        console.log(e);
        logger.error(process.pid+' : '+e.stack);
        rtnResult = {success:false, error: true, message: e.stack};
        throw e;
    }finally {
        return rtnResult;
    }
};

async function sendMailThroughSWU(mailParam){
    try{
        var callback = function(err, response) {
            console.log('***SWU Mail Response***');
            console.log('Mail sent to <> ', mailParam.recipient)
            logger.info('Mail sent to <> %s', mailParam.recipient)
            if (err) {
                console.log(err.statusCode, response);
                logger.error(err.statusCode+' : '+ response);
            } else {
                console.log(JSON.stringify(response));
                logger.info(JSON.stringify(response));
            }
        };
        SWU_API.send(mailParam, callback);
    }catch(e){
        console.log(e);
        logger.error(process.pid+' : '+e.stack);
    }
}

async function addAppedoWhitelabelsEmailReplacements(mailDetails){
    let mailData = mailDetails;
    try{
        for(var key in APPEDO_WHITE_LABELS){
            mailData[key] = APPEDO_WHITE_LABELS[key];
        }
    }catch(e){
        logger.error(process.pid+' : '+e.stack);
        logger.error("Failed. addAppedoWhitelabelsEmailReplacements : "+e.stack);
    }finally{
        return mailData;
    }
    
}

async function sendSMS(mobileNumber, message){
    try{
        let response = {};
        var formData = {authkey : '', sender : 'Appedo', mobiles : mobileNumber, message : message};
        var url = "https://sms.provider/send_api";
        request.post(url,{form: formData}, function (httpError, httpResp, body) {
            if (httpError || (httpResp && httpResp.statusCode != 200)){
                response.success = false;
                response.error = true;
                response.message = httpError;
            }else{
                response.success = true;
                response.error = false;
                response.message = 'Successfully sent SMS.';
            }
        });
        console.log(response);
        return response;
    }catch(e){
        console.log('SMS Error : '+e);
        response.success = false;
        response.error = true;
        response.message = e;
        return response;
    }
}