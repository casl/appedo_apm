// Node agent counter bean

var nodecounterbean = {
		counter_value : 0.0,
		counter_type : 0,
		process_name : '',
		exception : '',
	
    get getCounter_type() {
        return this.counter_type;
     },
  
	set setCounter_type(value) {
			this.counter_type = value;
	},
	
    get getCounter_value() {
        return this.counter_value;
     },

	set setCounter_value(value) {
			this.counter_value = value;
	},

    get getProcess_name() {
        return this.process_name;
     },
 	
	set setProcess_name (value) {
			this.process_name = value;
	},
		
    get getException() {
        return this.exception;
     },
 
	set setException(value) {
			this.exception = value;
	}
	
};

module.exports.tocounterString =  function(){	

		var sb = "";
		sb+="{";
		sb+="\"counter_type\":\"";
		sb+=nodecounterbean.counter_type;
		sb+="\",";
		sb+="\"counter_value\":\"";
		sb+=nodecounterbean.counter_value;
		sb+="\",";
		sb+="\"exception\":\"";
		sb+=nodecounterbean.exception;
		sb+="\",";
		sb+="\"process_name\":\"";
		sb+=nodecounterbean.process_name;
		sb+="\"";
		sb+="}";
		//console.log(sb.toString());		
		return sb.toString();

 	};

module.exports.nodecounterbean = nodecounterbean;
module.exports.tocounterString();
