﻿
let alCounterEntries = [];
let alCounter = [];
module.exports.alCounterEntries = alCounterEntries;
module.exports.alCounter = alCounter;

var nodebean = {
		Type : '',
		Mod_type : '',
		Guid : '',
		DateTime : Date,
	
    get getMod_type () {
			return this.Mod_type;
	},

	set setMod_type(value) {
			this.Mod_type = value;
	},

	get	getType() {
			return this.Type;
	},
 
	set	setType(value) {
			this.Type = value;
	},

	get getGuid() {
			return this.Guid;
	},
 
	set setGuid(value) {
			this.Guid = value;
	},
	
	get getDateTime() {
			return this.DateTime;
	},
  
	set setDateTime(value) {
			this.DateTime = value;
	}

};
let hmcounterentries = new Map();

module.exports.addNewCounterEntry=async function(counter_id){
	
	hmcounterentries.set(counter_id,new Array());
};

module.exports.addCounterEntry=async function(counter_id,nodearray){

	alCounter.push(nodearray);
};

module.exports.tonodecounterString =  function(){
	var i;
	var sbJSON = "";

	sbJSON += "{";
	sbJSON+="\"mod_type\":\"";
	sbJSON+=nodebean.Mod_type;
	sbJSON+="\",";
	sbJSON+="\"type\":\"";
	sbJSON+=nodebean.Type;
	sbJSON+="\",";
	sbJSON+="\"guid\":\"";
	sbJSON+=nodebean.Guid;
	sbJSON+="\",";
	sbJSON+="\"datetime\":\"";
	sbJSON+=nodebean.DateTime;
	sbJSON+="\",";
	sbJSON+="\"MetricSet\":";
	sbJSON+="[";
	for(i=0;i<alCounter.length;i++){
	  sbJSON+=alCounter[i];
	if(i<alCounter.length-1){
	     sbJSON+=",";
   		}
	}
	sbJSON+="]";
	sbJSON+="}";
	
	alCounter.length = 0;

	return sbJSON.toString();
}

module.exports.nodebean=nodebean;