var log4js = require('log4js');

log4js.configure({
		appenders: {
		  multi: { type: 'multiFile', base: './node_modules/appedonodeagent/Appedo_logs/', property: 'categoryName', extension: '.log',maxLogSize: 10485760, backups: 7, pattern: '.yyyy-MM-dd-hh',compress: true,layout: { type: 'messagePassThrough' } }
		},
		categories: {
		  default: { appenders: [ 'multi' ], level: 'debug' }
		}
	  });


module.exports = log4js;	  