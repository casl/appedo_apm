﻿exports.monitor = () => {
	const constants = require('./constants.js')
	const nodecounterbean = require('./nodecounterbean');
	const nodebean = require('./nodebean.js');
	const config = require('./config');
	const fetch = require('node-fetch');
	var fs = require('fs');
	const http = require('http'); 	
	const net = require('net');
	var Promise = require('promise');
	var path = process.cwd();
	var os = require('os'); 
	var si = require('systeminformation');
	var eventLoopStats  = require('event-loop-stats');
	var v8 = require('v8');
	var pid = process.pid;
	const { exec } = require('child_process');	
	global.log4js = require('./log');
	let totalCpus = os.cpus().length;
	let prevCpuUsage = process.cpuUsage();
	let prevHrTime = process.hrtime.bigint();
	
	const logger = log4js.getLogger();
	const DataLogger = log4js.getLogger('nodeapplicationlog');
	const errorlogger = log4js.getLogger('nodeerrorlog');

	console.log("Starting Node Agent....");
	logger.debug("NODE JS ENVIRONMENT DETAILS");  
	  if(os.platform().includes('win')){ 
		logger.debug("Filepath: "+process.argv[1]);
		logger.debug("Working directory: "+path);
	    logger.debug("systemname: "+process.env.USERNAME); 
		logger.debug("command line : "+process.title.split("- ")[1].replace(/\s\s+/g, ' '));
	  }else{
		logger.debug("Filepath: "+process.argv[1]);
		logger.debug("Working directory: "+path);
		logger.debug("systemname: "+process.env.SUDO_USER); 
		logger.debug("command line : "+process.title);
	  }	 
	  logger.debug("OS Architecture : "+os.arch());
	  logger.debug("No of processors : "+os.cpus().length);
	
	var objectValue;
    var array = [];
	
 
   //entry point of the node agent
    nodethread();

	    
async function nodethread(){	
	try{
		if(os.platform().includes('win')){
			let sysuuid = await si.system().then((data) => {return Promise.resolve(data)}).catch(error => console.error(error));	     
			let jsonRes = await sysuuid.uuid;
			logger.debug("systemuuid : "+jsonRes);
			constants.systemuuid = jsonRes;
		}else{
			const data = fs.readFileSync('/root/.sysAppedo', 'utf8')
			let system_uuid = data.trim();	  
			logger.debug("systemuuid : "+system_uuid);
			logger.debug("systemuuid : "+system_uuid.length);

			constants.systemuuid = system_uuid;
		}

		await getsysid(constants.systemuuid);
		logger.debug("systemId : "+constants.systemId);
		
		//node monitor main function
		nodemonitor(constants.systemuuid,constants.systemId);
	}catch(e){
		errorlogger.error('nodethread',e.stack);

	}	
}
    


async function getsysid(sysuuid){
	try{
		if(os.platform().includes('win')){
			logger.debug("systemuuid : win");
			param={
				systemGeneratorUUID:sysuuid,
				command:'winsystemGeneratorInfo'
			};
	    }else if(os.platform().includes('linux')){
			 logger.debug("systemuuid : linux");
			 param={
				systemGeneratorUUID:sysuuid,
				command:'systemGeneratorInfo'
			};
		}
		
		let url = config.Webservice_url+"/javaUnification";
		logger.debug(url);

		let res = await fetch(url, { method: 'POST', body: JSON.stringify(param),headers: { 'Content-Type': 'application/json' }});
		let jsonRes = await res.json();
		constants.systemId=jsonRes.systemId; 
	} catch(e){
		errorlogger.error('getsysid',e.stack);
	}
}


async function nodemonitor(uuid,id){
	try{
		let jsonappinfo ={};
		jsonappinfo = await getserverdetails();
		logger.debug("JsonAppinfo for Node process :" + JSON.stringify(jsonappinfo));
		let jsoncounterset = {};
		var ent_Id = '0';
		if(os.platform().includes('win')){
			await sendmoduleinfo(jsonappinfo,constants.systemId,constants.systemuuid,jsoncounterset,ent_Id,'winappInformation');
		}else{
			await sendmoduleinfo(jsonappinfo,constants.systemId,constants.systemuuid,jsoncounterset,ent_Id,'appInformation');

		}
		logger.debug("GUID : "+constants.moduleGUID);
		if(hasValue(constants.moduleGUID)){
		if(await getconfigurations(constants.moduleGUID))
			{
				logger.debug("node monitor timer started");
				constants.Timer_ID = setInterval(() => {
				nodeAgentTimer(constants.moduleGUID);
			}, 20000);		  
		}
		}
	}catch(e){
		errorlogger.error('nodemonitor',e.stack);
	}
}


async function getserverdetails(){
	try {
			let jsonnodedetails = {}; 
			var version=process.version;
			//var jmxport = config.port;
		    let jmxport;
			try {
				jmxport = await getnodePort();
				logger.debug("Adding port from Netstat");
			} catch(e) {
				errorlogger.error("Netstat command not working... kindly check OR add port no manually in config file");
				logger.debug("Adding port from config file");
				jmxport = config.port;
			}
			var moduleTypeName = 'Node';
			let moduleName = await getmoduleName();
			jsonnodedetails.moduleName=moduleName;
			jsonnodedetails.moduleTypeName=moduleTypeName;
			jsonnodedetails.VERSION_ID=version;
			jsonnodedetails.jmx_port=jmxport;

			return jsonnodedetails;
	} catch(e) {
		errorlogger.error('getserverdetails',e.stack);

	}
}

async function getnodePort(){
	try{	
		if(os.platform().includes('win')){
			var nodeport = "netstat -ano | find \"" + pid + "\"" ;
            var port = '';
			return new Promise(function(resolve,reject) {
					exec(nodeport, (error, stdout) => {
						if (error) {
							errorlogger.error(`error: ${error.message}`);
							reject(error);
							return;
						}				
						var strsplit = stdout.split("  ")[3].split(":")[1];
						port  = strsplit;
						constants.port = port;
					resolve(port);
					});
			});
	    }else if(os.platform().includes('linux')){
			var nodeport = "netstat -tp -ano | grep " + pid + " | awk '{print $4}' | cut -d':' -f4 ";
			return new Promise(function(resolve,reject) {
				exec(nodeport, (error, stdout) => {
					if (error) {
						errorlogger.error(`error: ${error.message}`);
						reject(error);
						return;
					}
					port = stdout;
					constants.port  = stdout;
				resolve(port);
				});
			});
		}
	}catch(e){
		errorlogger.error('getnodePort',e.stack);

	}
}

async function getmoduleName(){
	try{
		if(os.platform().includes('win')){
			var res = process.cwd().split("\\");
		}else{
			var res = process.cwd().split("/")
		}
		var module = res[res.length-1]
		return module;
	}catch(e){
		errorlogger.error('getmoduleName',e.stack);

	}
}


async function sendmoduleinfo(jsonappinfo,systemId,systemuuid,jsoncounterset,ent,command){
	try{
		    param={
				moduleInformation:JSON.stringify(jsonappinfo),
				systemId:systemId,
				UUID:systemuuid,
				Enterprise_Id:ent,
				jocounterSet:JSON.stringify(jsoncounterset),
				command:command
    		};
		  	
		  	let url = config.Webservice_url+"/javaUnification";
		  	let response = await fetch(url, { method: 'POST', body: JSON.stringify(param),headers: { 'Content-Type': 'application/json' }});
		  	let jsonRes = await response.json();
		  	constants.moduleGUID = jsonRes.message.moduleGUID;
	}catch(e){
			errorlogger.error('sendmoduleinfo',e.stack);
    }
}

async function hasValue(data) {
    return (data !== undefined) && (data !== null) && (data !== "");
}


async function getconfigurations(Guid){
	try{
		
			var breturn=false;
			param={
			guid:Guid,
			command:"AgentFirstRequest"	
			};
			
			let url = config.Webservice_url+"/getConfigurations";
			let response = await fetch(url,{method:'POST',body:JSON.stringify(param),headers:{'Content-Type':'application/json'}});
			let jsonRes = await response.json();
			if(jsonRes.success==true)
			{
				console.log("Monitor Counter Set : " + JSON.stringify(jsonRes.MonitorCounterSet));
				//updatecounters()
				constants.counterset.set(Guid,jsonRes.MonitorCounterSet);
				breturn = true;
			}
			
			return breturn;
	}catch(e){
		errorlogger.error('getconfigurations',e.stack);
	}
}


async function nodeAgentTimer(guid){
	try{	
		let moduleStatus = await getModuleRunningStatus(guid);
		
		let date = new Date().toISOString();

		if(moduleStatus.includes("Running")){
			nodeMonitorCounter(guid,date);	
		}else if(moduleStatus.includes("restart")){
			await getconfigurations(constants.moduleGUID);
			nodeMonitorCounter(guid,date);	
		}else if(moduleStatus.includes("Kill")){
			clearTimeout(constants.Timer_ID);
		}
    }catch(e){
		errorlogger.error('nodeAgentTimer',e.stack);

   }
}


async function getModuleRunningStatus(guid){
   try{	
		param={
			GUID:guid,
			command:"moduleRunningStatus"		
		};
		
		let url = config.Webservice_url+"/javaUnification";
		let response = await fetch(url, { method: 'POST', body: JSON.stringify(param),headers: { 'Content-Type': 'application/json' }});
		let jsonRes = await response.json();
		logger.debug(jsonRes);
		let modstatus = jsonRes.message;
		logger.debug(modstatus);
		
		return modstatus;
	}catch(e){
		errorlogger.error('getModuleRunningStatus',e.stack);

	}
}


async function nodeMonitorCounter(Guid,Date){
	let joselectedcounterset = [];
	let joselectcounter = {};
	var dCounterValue = 0.0;
	var nCounterId = null;
	var strExecutionType = null,query = null;
	var bIsDelta = false;
	var i;

	try{
	
       joselectedcounterset = constants.counterset.get(Guid);	
	   
	   nodebean.nodebean.setMod_type = 'Node';
	   nodebean.nodebean.setType='MetricSet'
	   nodebean.nodebean.setGuid= Guid;
	   nodebean.nodebean.setDateTime=Date;

	   for(i=0;i<joselectedcounterset.length;i++){
		 	try {
				joselectcounter = joselectedcounterset[i];
			  	nCounterId = joselectcounter.counter_id;
			  	query = joselectcounter.query;
			  	bIsDelta = joselectcounter.isdelta;
			  	strExecutionType = joselectcounter.executiontype;
			  

			  	if(strExecutionType){
				  	if(query.includes('process.cpu.total')){
						let hrTime = process.hrtime.bigint();
						let currCpuUsage = process.cpuUsage();
						let cpuUsage = process.cpuUsage(prevCpuUsage);
						let totCpu = (cpuUsage.user+cpuUsage.system)/1000; 
						let timeSpent = Number(hrTime-prevHrTime)/1000000; 
						prevCpuUsage = Object.assign({},currCpuUsage);
						prevHrTime = hrTime;
						let cpupercent = ((totCpu/timeSpent)*100)/totalCpus;
						dCounterValue= cpupercent;
					}else{
						var counterValue = eval(query);	
						dCounterValue=Number(counterValue).toFixed(1);  
					}
				}
			  	if(bIsDelta){
			        dCounterValue =  await addDeltaCounterValue_v1(nCounterId, dCounterValue);
			    }
			  
			  	nodecounterbean.nodecounterbean.setCounter_type= nCounterId;
			  	nodecounterbean.nodecounterbean.setException= '';
			  	nodecounterbean.nodecounterbean.setProcess_name = "";
			  	nodecounterbean.nodecounterbean.setCounter_value = dCounterValue;
			  
			  	let nodearr = [];
			  	nodearr = nodecounterbean.tocounterString();	

			  	nodebean.addCounterEntry(joselectcounter.counter_id,nodearr);
			  
			  	nodearr.length = 0;

			}catch(e){
				errorlogger.error('nodeMonitorCounter'+e.stack);
			}
		}
		//console.log("metrics###"+nodebean.tonodecounterString().toString());
		DataLogger.info("metrics###"+nodebean.tonodecounterString().toString());

	}catch(e){	
		errorlogger.error('nodeMonitorCounter'+e.stack);
	}

}


async function addDeltaCounterValue_v1(nCounterId, dCounterValue){

 dCurrentCounterValue = await doDelta(nCounterId, dCounterValue);

}

async function doDelta(nCounterId, dCurrentCounterValue){
  var dPrevCounterValue = 0.0,dDeltaCounterValue =0.0;
  if(constants.hmlinuxDelta.has(nCounterId)){  
    
	dPrevCounterValue = constants.hmlinuxDelta.get(nCounterId);
    
	if(dPrevCounterValue > dCurrentCounterValue){
       dDeltaCounterValue = 0.0;
    }else{
        dDeltaCounterValue = dCurrentCounterValue - dPrevCounterValue;
    }
	 
	constants.hmlinuxDelta.set(nCounterId,dCurrentCounterValue);
  
  }else {
	 constants.hmlinuxDelta.set(nCounterId,dCurrentCounterValue);
      dDeltaCounterValue = 0.0;
  }
  
  return dDeltaCounterValue;

}

};


