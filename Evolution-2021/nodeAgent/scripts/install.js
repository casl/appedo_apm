var fs = require('fs');
var os = require('os');
var path = process.cwd();
var array = [];
var flag = false;

console.log("Path : ",path);
var filename,data,path1 ;

if(os.platform().includes('win')){
	path1=path.split('\\node_modules')[0];
}else{
	path1=path.split('/node_modules')[0];
}

var PackageFile = fs.readFileSync(path1 + '/package.json', 'utf8');
array = PackageFile.split(',');

for(var i = 0;i<array.length;i++ ){ 
	if (array[i].includes("appedonodeagent")){
		console.log("already exists");
		flag = false;
	}else if (array[i].includes("main")){
		filename = array[i].trim().split(':')[1].replace(/['"]+/g, '').trimStart();
		console.log("Mainfilename : ",filename);
		data = "require('appedonodeagent').monitor(); \n";
		flag = true;
	}	   
} 

if(flag){
	const mainfile = fs.readFileSync(path1 +'/' + filename, 'utf8').toString().split("\n");
	mainfile.splice(0,0,data);
	var newtext = mainfile.join("\n");
	var updated_file = path1 +'/' + filename;
	fs.writeFile(updated_file, newtext, function (err) {
		if (err) return err;
   });
}
