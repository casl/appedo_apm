package com.appedo.resource_shipper.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;

import com.appedo.manager.LogManager;
import com.appedo.manager.ReportManager;
import com.appedo.resource_shipper.common.Constants;
import com.appedo.resource_shipper.utils.UtilsFactory;

public class ResourceShipperController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResourceShipperController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doAction(request, response);
	}
	
	
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		String strFilePath = null;
		byte[] bytes = null;
		boolean bOverwrite = false;
		
		File fileWrite = null;
		FileOutputStream fos = null;
		
		String strRequestAction = null;
		strRequestAction = request.getRequestURI();
		strRequestAction = strRequestAction.substring( strRequestAction.indexOf("/", 1), strRequestAction.length());
		
		
		// TODO receive the zip/tar file. Extract it and save.
		if( strRequestAction.endsWith("/writeFile") ){
			Date dateLog = LogManager.logMethodStart();
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("file_path");
				bytes = UtilsFactory.replaceNull(request.getParameter("file_content"), request.getParameter("file_in_bytes")).getBytes("UTF-8");
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(strFilePath);
				
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					// write the file
					
					fileWrite.getParentFile().mkdirs();
					
					fos = new FileOutputStream(fileWrite);
					fos.write(bytes);
				}
				
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				fos.close();
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/readFile") ){
			Date dateLog = LogManager.logMethodStart();
			FileInputStream fis = null;
			File f = null;
			StringBuilder output = new StringBuilder();
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("file_path");
				f = new File(strFilePath);
				fis = new FileInputStream(f);
				int content;
				while ((content = fis.read()) != -1) {
					output.append((char) content);
				}
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn(output.toString()).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Read File Service: "+th.getMessage()).toString() );
			} finally {
				UtilsFactory.close(fis);
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/checkFileExists") ){
			Date dateLog = LogManager.logMethodStart();
			File f = null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("file_path");
				f = new File(strFilePath);
				if( f.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File Found").toString());
				}else{
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Not Found").toString());
				}
				
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Check File Exists Service : "+th.getMessage()).toString() );
			}finally{
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/removeFile") ){
			Date dateLog = LogManager.logMethodStart();
			File f = null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("file_path");
				f = new File(strFilePath);
				if( f.exists() ) {
					f.delete();
					response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File Deleted").toString());
				}else{
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Not Deleted").toString());
				}
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Remove File Service: "+th.getMessage()).toString() );
			}finally{
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/removeFolder") ){
			Date dateLog = LogManager.logMethodStart();
			File f = null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("file_path");
				f = new File(strFilePath);
				if(f.isDirectory()){
					if(f.list().length==0){
						f.delete();
						response.getWriter().write( UtilsFactory.getJSONSuccessReturn("Folder Deleted").toString());
					}else{
						String files[] = f.list();
						for (String temp : files) {
							//construct the file structure
							File fileDelete = new File(f, temp);
							fileDelete.delete();
						}
						if(f.list().length==0){
							f.delete();
							response.getWriter().write( UtilsFactory.getJSONSuccessReturn("Folder Deleted").toString());
						}
					}
				}else{
					response.getWriter().write( UtilsFactory.getJSONFailureReturn(strFilePath + "is Not Directory").toString());
				}
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Remove Folder Service: "+th.getMessage()).toString() );
			}finally{
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeContainerXlsFile") ){
			Date dateLog = LogManager.logMethodStart();
			HSSFWorkbook workbook;
	        HSSFSheet sheet;
	        HSSFRow row;
	        HSSFCell cell;
	        int rownum = 0;
	        JSONArray jaChartData = null;
			JSONObject joChart = null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				
				jaChartData = JSONArray.fromObject(request.getParameter("dataContent"));
				
				workbook = new HSSFWorkbook();
                sheet = workbook.createSheet("Container Wise Report");
                rownum = 0;
                
				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Name");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("reportName"));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Start Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runStartTime"));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report End Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runEndTime"));


				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Duration Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runTime"));
				
				row = sheet.createRow(rownum++);
				
				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("S.No");
				cell = row.createCell(1);
				cell.setCellValue("Container Name");
				cell = row.createCell(2);
				cell.setCellValue("Throughput in MB");
				cell = row.createCell(3);
				cell.setCellValue("Min in ms");
				cell = row.createCell(4);
				cell.setCellValue("Avg in ms");
				cell = row.createCell(5);
				cell.setCellValue("Max in ms");
				cell = row.createCell(6);
				cell.setCellValue("Error 400");
				cell = row.createCell(7);
				cell.setCellValue("Error 500");
				cell = row.createCell(8);
				cell.setCellValue("Error Others");
				
				for(int i=0; i<jaChartData.size(); i++){
					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
                    cell.setCellValue(i+1);
					joChart = jaChartData.getJSONObject(i);
					
					cell = row.createCell(1);
                    cell.setCellValue(joChart.getString("containerName"));
                    cell = row.createCell(2);
    				cell.setCellValue(joChart.getString("tPutInMb"));
    				cell = row.createCell(3);
    				cell.setCellValue(joChart.getString("minInMs"));
    				cell = row.createCell(4);
    				cell.setCellValue(joChart.getString("avgInMs"));
    				cell = row.createCell(5);
    				cell.setCellValue(joChart.getString("maxInMs"));
    				cell = row.createCell(6);
    				cell.setCellValue(joChart.getString("error400"));
    				cell = row.createCell(7);
    				cell.setCellValue(joChart.getString("error500"));
    				cell = row.createCell(8);
    				cell.setCellValue(joChart.getString("error700"));
				}
				
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(strFilePath);
				
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					// write the file
					
					fileWrite.getParentFile().mkdirs();
					
					fos = new FileOutputStream(fileWrite);
					workbook.write(fos);
//					fos.close();
				}
				
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				fos.close();
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeTransactionXlsFile") ){
			Date dateLog = LogManager.logMethodStart();
			HSSFWorkbook workbook;
	        HSSFSheet sheet;
	        HSSFRow row;
	        HSSFCell cell;
	        int rownum = 0;
	        JSONArray jaChartData = null;
			JSONObject joChart = null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				
				jaChartData = JSONArray.fromObject(request.getParameter("dataContent"));
				
				workbook = new HSSFWorkbook();
                sheet = workbook.createSheet("Transaction Wise Report");
                rownum = 0;
                
				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Name");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("reportName"));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Start Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runStartTime"));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report End Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runEndTime"));


				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Duration Time");
				cell = row.createCell(1);
				cell.setCellValue(request.getParameter("runTime"));
				
				row = sheet.createRow(rownum++);
				
				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("S.No");
				cell = row.createCell(1);
				cell.setCellValue("Transaction Name");
				cell = row.createCell(2);
				cell.setCellValue("Min in ms");
				cell = row.createCell(3);
				cell.setCellValue("Avg in ms");
				cell = row.createCell(4);
				cell.setCellValue("Max in ms");
				for(int i=0; i<jaChartData.size(); i++){
					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
                    cell.setCellValue(i+1);
					joChart = jaChartData.getJSONObject(i);
					
					cell = row.createCell(1);
                    cell.setCellValue(joChart.getString("transactionName"));
                    cell = row.createCell(2);
    				cell.setCellValue(joChart.getString("minTTInMs"));
    				cell = row.createCell(3);
    				cell.setCellValue(joChart.getString("avgTTInMs"));
    				cell = row.createCell(4);
    				cell.setCellValue(joChart.getString("maxTTInMs"));
					
				}
				
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(strFilePath);
				
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					// write the file
					
					fileWrite.getParentFile().mkdirs();
					
					fos = new FileOutputStream(fileWrite);
					workbook.write(fos);
//					fos.close();
				}
				
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				fos.close();
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeScenarioXlsFile") ){
			Date dateLog = LogManager.logMethodStart();
			HSSFWorkbook workbook;
	        HSSFSheet sheet;
	        HSSFRow row;
	        HSSFCell cell;
	        int rownum = 0;
			JSONObject joSummaryContent = null,joScriptContent=null,joContainerAndTransactionContentByScriptId=null,joContainer=null,joTransaction=null;
	        JSONArray jaScriptContent = null,jaContainer=null,jaTransaction=null,jaContainerAndTransactionContent=null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				
				joSummaryContent = JSONObject.fromObject(request.getParameter("summaryContent"));
				jaScriptContent = JSONArray.fromObject(request.getParameter("scriptContent"));
				jaContainerAndTransactionContent = JSONArray.fromObject(request.getParameter("containerAndTransactionContent"));
				
				workbook = new HSSFWorkbook();
                sheet = workbook.createSheet("Scenario Summary");
                rownum = 0;

                HSSFFont hSSFHeaderFont = workbook.createFont();
                hSSFHeaderFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

                CellStyle cellHeaderStyle = workbook.createCellStyle();
                cellHeaderStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellHeaderStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellHeaderStyle.setBorderBottom((short) 1);
                cellHeaderStyle.setBorderTop((short) 1);
                cellHeaderStyle.setBorderLeft((short) 1);
                cellHeaderStyle.setBorderRight((short) 1);
                cellHeaderStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
                cellHeaderStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                cellHeaderStyle.setFont(hSSFHeaderFont);

                CellStyle cellContentStyle = workbook.createCellStyle();
                cellContentStyle.setAlignment(CellStyle.ALIGN_CENTER);
                cellContentStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellContentStyle.setBorderBottom((short) 1);
                cellContentStyle.setBorderTop((short) 1);
                cellContentStyle.setBorderLeft((short) 1);
                cellContentStyle.setBorderRight((short) 1);

                CellStyle cellContentLeftStyle = workbook.createCellStyle();
                cellContentLeftStyle.setAlignment(CellStyle.ALIGN_LEFT);
                cellContentLeftStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellContentLeftStyle.setBorderBottom((short) 1);
                cellContentLeftStyle.setBorderTop((short) 1);
                cellContentLeftStyle.setBorderLeft((short) 1);
                cellContentLeftStyle.setBorderRight((short) 1);

                CellStyle cellContentLeftBoldStyle = workbook.createCellStyle();
                cellContentLeftBoldStyle.setAlignment(CellStyle.ALIGN_LEFT);
                cellContentLeftBoldStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellContentLeftBoldStyle.setBorderBottom((short) 1);
                cellContentLeftBoldStyle.setBorderTop((short) 1);
                cellContentLeftBoldStyle.setBorderLeft((short) 1);
                cellContentLeftBoldStyle.setBorderRight((short) 1);
                cellContentLeftBoldStyle.setFont(hSSFHeaderFont);
                
                CellStyle cellContentRightStyle = workbook.createCellStyle();
                cellContentRightStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                cellContentRightStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellContentRightStyle.setBorderBottom((short) 1);
                cellContentRightStyle.setBorderTop((short) 1);
                cellContentRightStyle.setBorderLeft((short) 1);
                cellContentRightStyle.setBorderRight((short) 1);

                CellStyle cellContentRightBoldStyle = workbook.createCellStyle();
                cellContentRightBoldStyle.setAlignment(CellStyle.ALIGN_RIGHT);
                cellContentRightBoldStyle.setVerticalAlignment(CellStyle.ALIGN_FILL);
                cellContentRightBoldStyle.setBorderBottom((short) 1);
                cellContentRightBoldStyle.setBorderTop((short) 1);
                cellContentRightBoldStyle.setBorderLeft((short) 1);
                cellContentRightBoldStyle.setBorderRight((short) 1);
                cellContentRightBoldStyle.setFont(hSSFHeaderFont);

                row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Name");
				cell.setCellStyle(cellContentLeftBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("reportName"));
				cell.setCellStyle(cellContentRightBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":E"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Start Time");
				cell.setCellStyle(cellContentLeftBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runStartTime"));
				cell.setCellStyle(cellContentRightBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":E"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report End Time");
				cell.setCellStyle(cellContentLeftBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runEndTime"));
				cell.setCellStyle(cellContentRightBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":E"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Report Duration Time");
				cell.setCellStyle(cellContentLeftBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runTime"));
				cell.setCellStyle(cellContentRightBoldStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":E"+rownum));
				
				row = sheet.createRow(rownum++);
				
				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Summary Report");
				cell.setCellStyle(cellHeaderStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Start Time");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));

				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runStartTime"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));
				
				cell = row.createCell(4);
				cell.setCellValue("Average Request Response(Ms)");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("averageReqResponse"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("End Time");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runEndTime"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));
				
				cell = row.createCell(4);
				cell.setCellValue("Average Page Response(Ms)");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));

				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("averagePageResponse"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Duration");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));

				cell = row.createCell(2);
				cell.setCellValue(request.getParameter("runTime"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Total Throughput(Mb)");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("totalThroughput"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("User Count");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(joSummaryContent.getString("userCount"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Average Throughput(Mbps)");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("avgThroughput"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Total Hits");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(joSummaryContent.getString("totalHits"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Response Code 200 Count");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("responseCode200Count"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Average Hits/Sec");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(joSummaryContent.getString("averageHitsSec"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Response Code 300 Count");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("responseCode300Count"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Total Errors");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(joSummaryContent.getString("totalError"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Response Code 400 Count");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("responseCode400Count"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				cell = row.createCell(0);
				cell.setCellValue("Total Pages");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":B"+rownum));
				
				cell = row.createCell(2);
				cell.setCellValue(joSummaryContent.getString("totalPage"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("C"+rownum+":D"+rownum));

				cell = row.createCell(4);
				cell.setCellValue("Response Code 500 Count");
				cell.setCellStyle(cellContentLeftStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("E"+rownum+":F"+rownum));
				
				cell = row.createCell(6);
				cell.setCellValue(joSummaryContent.getString("responseCode500Count"));
				cell.setCellStyle(cellContentRightStyle);
				sheet.addMergedRegion(CellRangeAddress.valueOf("G"+rownum+":H"+rownum));

				row = sheet.createRow(rownum++);
				
				for(int i=0; i<jaScriptContent.size(); i++){
					joScriptContent = jaScriptContent.getJSONObject(i);
					joContainerAndTransactionContentByScriptId=null;
					jaContainer=null;
					jaTransaction = null;
					
					joContainerAndTransactionContentByScriptId = jaContainerAndTransactionContent.getJSONObject(i);
					JSONObject jObj = (JSONObject) joContainerAndTransactionContentByScriptId.get("dataContent");
					jaContainer = jObj.getJSONArray(Constants.CONTAINER_RESPONSE);
					jaTransaction = jObj.getJSONArray(Constants.TRANSACTION_RESPONSE);
//					LogManager.infoLog(joContainerAndTransactionContentByScriptId.toString());
//					LogManager.infoLog(jObj.toString());
//					LogManager.infoLog("joContainerAndTransactionContentByScriptId.getJSONObject(Constants.CONTAINER_RESPONSE) : "+jObj.getJSONArray(Constants.CONTAINER_RESPONSE).toString());
//					LogManager.infoLog("joContainerAndTransactionContentByScriptId.getJSONObject(Constants.TRANSACTION_RESPONSE) : "+jObj.getJSONArray(Constants.TRANSACTION_RESPONSE).toString());

					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
					cell.setCellValue("Conatiner Wise - "+joScriptContent.getString("scriptName"));
					cell.setCellStyle(cellHeaderStyle);
					sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":I"+rownum));
					
					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
					cell.setCellValue("S.No");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(1);
					cell.setCellValue("Container Name");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(2);
					cell.setCellValue("Throughput in MB");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(3);
					cell.setCellValue("Min in ms");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(4);
					cell.setCellValue("Avg in ms");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(5);
					cell.setCellValue("Max in ms");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(6);
					cell.setCellValue("Error 400");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(7);
					cell.setCellValue("Error 500");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(8);
					cell.setCellValue("Error Others");
					cell.setCellStyle(cellHeaderStyle);

					if(jaContainer.size()>0){
						for(int j=0; j<jaContainer.size(); j++){
//							if(j==0){
//								row = sheet.createRow(rownum++);
//								cell = row.createCell(0);
//								cell.setCellValue("S.No");
//								cell = row.createCell(1);
//								cell.setCellValue("Container Name");
//								cell = row.createCell(2);
//								cell.setCellValue("Throughput in MB");
//								cell = row.createCell(3);
//								cell.setCellValue("Min in ms");
//								cell = row.createCell(4);
//								cell.setCellValue("Avg in ms");
//								cell = row.createCell(5);
//								cell.setCellValue("Max in ms");
//								cell = row.createCell(6);
//								cell.setCellValue("Error 400");
//								cell = row.createCell(7);
//								cell.setCellValue("Error 500");
//								cell = row.createCell(8);
//								cell.setCellValue("Error Others");
//							}
							
							row = sheet.createRow(rownum++);
							cell = row.createCell(0);
		                    cell.setCellValue(j+1);
		    				cell.setCellStyle(cellContentStyle);
							joContainer = jaContainer.getJSONObject(j);
							
							cell = row.createCell(1);
		                    cell.setCellValue(joContainer.getString("containerName"));
		    				cell.setCellStyle(cellContentStyle);
		                    cell = row.createCell(2);
		    				cell.setCellValue(joContainer.getString("tPutInMb"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(3);
		    				cell.setCellValue(joContainer.getString("minInMs"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(4);
		    				cell.setCellValue(joContainer.getString("avgInMs"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(5);
		    				cell.setCellValue(joContainer.getString("maxInMs"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(6);
		    				cell.setCellValue(joContainer.getString("error400"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(7);
		    				cell.setCellValue(joContainer.getString("error500"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(8);
		    				cell.setCellValue(joContainer.getString("error700"));
		    				cell.setCellStyle(cellContentStyle);
						}
					}else{
						row = sheet.createRow(rownum++);
						cell = row.createCell(0);
	                    cell.setCellValue("No Record(s) Found");
	    				cell.setCellStyle(cellContentStyle);
	                    sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":I"+rownum));
					}
					row = sheet.createRow(rownum++);
					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
					cell.setCellValue("Transaction Wise - "+joScriptContent.getString("scriptName"));
					cell.setCellStyle(cellHeaderStyle);
                    sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":E"+rownum));

					row = sheet.createRow(rownum++);
					cell = row.createCell(0);
					cell.setCellValue("S.No");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(1);
					cell.setCellValue("Transaction Name");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(2);
					cell.setCellValue("Min in ms");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(3);
					cell.setCellValue("Avg in ms");
					cell.setCellStyle(cellHeaderStyle);
					cell = row.createCell(4);
					cell.setCellValue("Max in ms");
					cell.setCellStyle(cellHeaderStyle);
					if(jaTransaction.size()>0){
						for(int j=0; j<jaTransaction.size(); j++){
//							if(j==0){
//								row = sheet.createRow(rownum++);
//								cell = row.createCell(0);
//								cell.setCellValue("S.No");
//								cell = row.createCell(1);
//								cell.setCellValue("Transaction Name");
//								cell = row.createCell(2);
//								cell.setCellValue("Min in ms");
//								cell = row.createCell(3);
//								cell.setCellValue("Avg in ms");
//								cell = row.createCell(4);
//								cell.setCellValue("Max in ms");
//							}

							row = sheet.createRow(rownum++);
							cell = row.createCell(0);
		                    cell.setCellValue(j+1);
		    				cell.setCellStyle(cellContentStyle);
		                    joTransaction = jaTransaction.getJSONObject(j);
							
							cell = row.createCell(1);
		                    cell.setCellValue(joTransaction.getString("transactionName"));
		    				cell.setCellStyle(cellContentStyle);
		                    cell = row.createCell(2);
		    				cell.setCellValue(joTransaction.getString("minTTInMs"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(3);
		    				cell.setCellValue(joTransaction.getString("avgTTInMs"));
		    				cell.setCellStyle(cellContentStyle);
		    				cell = row.createCell(4);
		    				cell.setCellValue(joTransaction.getString("maxTTInMs"));
		    				cell.setCellStyle(cellContentStyle);
							
						}
					}else{
						row = sheet.createRow(rownum++);
						cell = row.createCell(0);
	                    cell.setCellValue("No Record(s) Found");
	    				cell.setCellStyle(cellContentStyle);
	                    sheet.addMergedRegion(CellRangeAddress.valueOf("A"+rownum+":E"+rownum));
					}
				}
				
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(strFilePath);
				
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					// write the file
					
					fileWrite.getParentFile().mkdirs();
					
					fos = new FileOutputStream(fileWrite);
					workbook.write(fos);
//					fos.close();
				}
				
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				fos.close();
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeScenarioPdfFile") ){
			Date dateLog = LogManager.logMethodStart();
			JSONObject joFinalContent = null;
	        String jasperFilePath = null,jasperResouceFilePath=null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				jasperFilePath = request.getParameter("jasperFilePath");
				jasperResouceFilePath = request.getParameter("jasperResouceFilePath");
				joFinalContent = JSONObject.fromObject(request.getParameter("joFinalContent"));
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(jasperFilePath+ File.separator+"empty.txt");
				fileWrite.getParentFile().mkdirs();
				
				fileWrite = new File(strFilePath);
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					fileWrite.getParentFile().mkdirs();
					
					if(ReportManager.generateScenorioReport(joFinalContent, Constants.APPEDO_WHITE_LABELS, strFilePath, jasperResouceFilePath, jasperFilePath)){
						LogManager.infoLog("Pdf successfully generated");
					}else{
						LogManager.errorLog("Pdf generation failed");
					}
				}
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeASDChartPdfFile") ){
			Date dateLog = LogManager.logMethodStart();
			JSONObject joFinalContent = null;
	        String jasperFilePath = null,jasperResouceFilePath=null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				jasperFilePath = request.getParameter("jasperFilePath");
				jasperResouceFilePath = request.getParameter("jasperResouceFilePath");
				joFinalContent = JSONObject.fromObject(request.getParameter("joFinalContent"));
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(jasperFilePath+ File.separator+"empty.txt");
				fileWrite.getParentFile().mkdirs();
				
				fileWrite = new File(strFilePath);
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					fileWrite.getParentFile().mkdirs();
					
					if(ReportManager.generateASDChartReports(joFinalContent, Constants.APPEDO_WHITE_LABELS, strFilePath, jasperResouceFilePath, jasperFilePath)){
						LogManager.infoLog("Pdf successfully generated");
					}else{
						LogManager.errorLog("Pdf generation failed");
					}
				}
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				LogManager.logMethodEnd(dateLog);
			}
		} else if( strRequestAction.endsWith("/writeMyChartPdfFile") ){
			Date dateLog = LogManager.logMethodStart();
			JSONObject joFinalContent = null;
	        String jasperFilePath = null,jasperResouceFilePath=null;
			try{
				strFilePath = Constants.RESOURCE_PATH + File.separator + request.getParameter("filePath");
				jasperFilePath = request.getParameter("jasperFilePath");
				jasperResouceFilePath = request.getParameter("jasperResouceFilePath");
				joFinalContent = JSONObject.fromObject(request.getParameter("joFinalContent"));
				bOverwrite = Boolean.parseBoolean( UtilsFactory.replaceNullBlank(request.getParameter("overwrite"), "false") );
				
				fileWrite = new File(jasperFilePath+ File.separator+"empty.txt");
				fileWrite.getParentFile().mkdirs();
				
				fileWrite = new File(strFilePath);
				if( !bOverwrite && fileWrite.exists() ) {
					response.getWriter().write( UtilsFactory.getJSONFailureReturn("File Already Exists. Send request along with overwrite permission.").toString() );
				} else {
					fileWrite.getParentFile().mkdirs();
					
					if(ReportManager.generateMyChartReports(joFinalContent, Constants.APPEDO_WHITE_LABELS, strFilePath, jasperResouceFilePath, jasperFilePath)){
						LogManager.infoLog("Pdf successfully generated");
					}else{
						LogManager.errorLog("Pdf generation failed");
					}
				}
				response.getWriter().write( UtilsFactory.getJSONSuccessReturn("File is written in "+strFilePath).toString() );
			} catch ( Throwable th ) {
				LogManager.errorLog(th);
				response.getWriter().write( UtilsFactory.getJSONFailureReturn("Problem in Write File Service: "+th.getMessage()).toString() );
			} finally {
				LogManager.logMethodEnd(dateLog);
			}
			
		}
		// Reload Configs
		else if(strRequestAction.equals("/reloadConfigProperties")) {
			try {
				// reload Constant properties
				Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
				
				// reload WhiteLabels
				Constants.loadAppedoConstants(Constants.APPEDO_CONFIG_FILE_PATH);
				
				// reload Appedo-config properties from the system path
				Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
				
				response.getWriter().write("<html><body>Loaded <B>Appedo-Resource-Shipping-Services</B>, config, appedo_config properties & White-Labels.</body></html>");
			} catch (Exception e) {
				LogManager.errorLog(e);
				response.getWriter().write("<B style=\"color: red; \">Exception occurred Appedo-VelocityUI: "+e.getMessage()+"</B>");
			}
		}
	}
}
