package com.appedo.resource_shipper.common;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import net.sf.json.JSONObject;

import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;
import com.appedo.resource_shipper.utils.UtilsFactory;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author navin
 *
 */
public class Constants {
	
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	
	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";
	
	public static String REQUEST_RESPONSE = "requestResponse";
	public static String CONTAINER_RESPONSE = "containerResponse";
	public static String TRANSACTION_RESPONSE = "transactionResponse";
	public static String ERROR_COUNT = "errorCount";
	public static String ERROR_DESCRIPTION = "errorDescription";
	
	// to replace words related to `Appedo` with White-Labels configured in DB 
	public static JSONObject APPEDO_WHITE_LABELS = null;
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(srtConstantsPath);
			prop.load(is);
			
	 		// Appedo application's resource directory path
	 		RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
	 		
	 		APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
	 		
	 		LOG4J_PROPERTIES_FILE = RESOURCE_PATH+prop.getProperty("LOG4J_CONFIG_FILE_PATH");
		} catch(Throwable th) {
			System.out.println("Exception in loadConstantsProperties: "+th.getMessage());
			LogManager.errorLog(th);
			
			throw th;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Loads appedo config properties, from the system specifed path, 
	 * (Note.. loads other than db related)
	 *  
	 * @param strAppedoConfigPath
	 */
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		JSONObject joAppedoCollector = null;
		
		try {
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
	 		// RUM related variables
	 		//RUM_JS_SCRIPT_FILES_URL = prop.getProperty("RUM_JS_SCRIPT_FILES_URL");
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.close(is);
			is = null;
			
			UtilsFactory.clearCollectionHieracy(joAppedoCollector);
			joAppedoCollector = null;
		}	
	}
	
	/**
	 * loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoConstants(String strAppedoConfigPath) throws Exception {
		
		try {
			// 
			AppedoConstants.getAppedoConstants().loadAppedoConstants(strAppedoConfigPath);
			
			APPEDO_WHITE_LABELS = JSONObject.fromObject(AppedoConstants.getAppedoWhiteLabels());
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Collector in JSON format return as URL
	 * 
	 * @param joAppedoCollector
	 * @return
	 */
	public static String getAsURL(JSONObject joAppedoCollector) {
		return joAppedoCollector.getString("protocol") +"://"+ joAppedoCollector.getString("server") + 
				( joAppedoCollector.getString("port").length() > 0 ? ":"+joAppedoCollector.getString("port") : "" ) + 
				"/"+ joAppedoCollector.getString("application_name");
	}
}
