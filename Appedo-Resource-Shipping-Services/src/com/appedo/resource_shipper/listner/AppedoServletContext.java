package com.appedo.resource_shipper.listner;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.appedo.manager.LogManager;

public class AppedoServletContext implements ServletContextListener {
	
	private ServletContext context = null;
	
	/**
	 * This method is invoked when the Web Application
	 * is ready to service requests
	 */
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		
		//Output a simple message to the server's console
		System.out.println("The Appedo-Resource-Shipping-Services Web App is Ready in "+context.getServerInfo());
	}
	
	/**
	 * Do the uninstall or closing operations, when this WebAppln is stopped or Tomcat is stopped, like:
	 * - Stopping all the timers
	 * - Stop the LogManager's config file refresher.
	 */
	public void contextDestroyed(ServletContextEvent event) {
		
		LogManager.stopLogRefresh();
		
		System.out.println("The Appedo-Resource-Shipping-Services Web App is stopped.");
		this.context = null;
	}

}
