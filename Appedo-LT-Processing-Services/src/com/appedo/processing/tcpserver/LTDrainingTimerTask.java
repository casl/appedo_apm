package com.appedo.processing.tcpserver;

import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class LTDrainingTimerTask extends TimerTask{
	private long tNo;
	
	public LTDrainingTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in LTDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		String strQueueDetails = null;
		
		TaskExecutor executor = null;
		
		try {
			long ltActualQueue = 0L;
			strQueueDetails = DrainManager.getLTQueueDetails("getSize").getResponse();
			ltActualQueue = Long.parseLong( strQueueDetails );
			
			// long connectionSize = Long.valueOf(Constants.PROCESSING_THREAD);
			long connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
			
			// System.out.println("connectionSize:: "+connectionSize);
			long ltQueueSize = ltActualQueue < connectionSize ? ltActualQueue : connectionSize;
			
			if( ltQueueSize <= 0 ) {
				LogManager.errorLog("Unable to create New Threads. As required count is "+ltQueueSize);
			} else {
				for(int i=0; i<ltQueueSize; i++ ){
					executor = TaskExecutor.getExecutor(Constants.DEFAULT_LT_THREADPOOL_NAME);
					executor.submit(new LTProcessinngTimerTask());
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Closed Connection count in LTDrainingTimerTask: " + tNo);
		super.finalize();
	}
}
