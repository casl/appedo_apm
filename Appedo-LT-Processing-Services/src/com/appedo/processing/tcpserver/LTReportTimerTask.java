package com.appedo.processing.tcpserver;

import java.util.TimerTask;

import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.TaskExecutor;

public class LTReportTimerTask extends TimerTask{
	RunManager rm = null;
	private long tNo;
	
	public LTReportTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		this.rm = new RunManager();
		LogManager.infoLog("Opened Connection count in LTReportTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		String strQueueDetails = null;
		
		TaskExecutor executor = null;
		
		try {
			executor = TaskExecutor.getExecutor(Constants.DEFAULT_LT_REPORTING_NAME);
			
			long reportingThreadSize = Long.valueOf(Constants.REPORTING_THREAD);
			strQueueDetails = DrainManager.getLTQueueDetails("getRunIDQueueSize").getResponse();
			long runIdQueueSize = Long.parseLong(strQueueDetails);
			LogManager.infoLog("Report Generating Timer for RunId: " + runIdQueueSize);
			
			long ltQueueSize = reportingThreadSize < runIdQueueSize ? reportingThreadSize : runIdQueueSize;
			
			// Active count >
			for(int i=0; i<ltQueueSize; i++ ){
				long activeThreadSize = executor.activeThreadCount();
				if(activeThreadSize < ltQueueSize){
					executor.submit(new LTReportingThread());
				}
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		rm = null;
		super.finalize();
		LogManager.infoLog("Closed Connection count in LTReportTimerTask: "+tNo);
	}
}
