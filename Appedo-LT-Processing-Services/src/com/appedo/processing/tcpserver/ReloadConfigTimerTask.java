package com.appedo.processing.tcpserver;

import java.util.TimerTask;

import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;

public class ReloadConfigTimerTask extends TimerTask{
	
	public ReloadConfigTimerTask()throws Throwable{
		
	}
	
	@Override
	public void run() {
		
		try {
			Constants.reloadConstantsProperties();
		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
