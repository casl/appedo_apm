package com.appedo.processing.tcpserver;

import java.util.HashSet;
import java.util.Set;
import java.util.TimerTask;

import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class LTPaidQueueTimerTask extends TimerTask{
	
	long tNo;
	public static Set<Long> previousRunUserId = new HashSet<Long>();
	public LTPaidQueueTimerTask(){
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in LTPaidQueueTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		String strResponse = null;
		try {
			TaskExecutor executor = null;
			executor = TaskExecutor.getExecutor(Constants.DEFAULT_LT_PAID_SERVICE);
			long ltActualQueue = 0L;
			strResponse = DrainManager.getLTQueueDetails("getPaidQueueSize").getResponse();
			LogManager.infoLog("getPaidQueueSize :"+strResponse);
			ltActualQueue = Long.parseLong(strResponse);
			
			long activeThreadSize = executor.activeThreadCount();
			LogManager.infoLog("activeThreadSize :"+activeThreadSize);
			long paidQueueThreadSize = Long.valueOf(Constants.PAID_QUEUE_THREAD) - activeThreadSize;
			LogManager.infoLog("activeThreadSize :"+activeThreadSize);
			long actualQueueSize = ltActualQueue > paidQueueThreadSize ? paidQueueThreadSize : ltActualQueue;
			LogManager.infoLog("Converted to actualQueueSize :"+actualQueueSize);
			// actualQueueSize = actualQueueSize > activeThreadSize ? activeThreadSize : actualQueueSize;
			for(int i=0; i<actualQueueSize; i++ ){
				executor.submit(new LTPaidQueueThread(ltActualQueue, "LT"));
			}
						
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			LogManager.infoLog("Closed Connection count in LTPaidQueueTimerTask: "+tNo);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
