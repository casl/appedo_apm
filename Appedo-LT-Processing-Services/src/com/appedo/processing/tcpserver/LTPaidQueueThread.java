package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.UtilsFactory;

public class LTPaidQueueThread extends Thread{
	Connection con = null;
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	RunManager runManager = null;
	HashMap<JSONObject, Integer> hmInstance = null;
	String region, osType = null;
	JSONObject instance_config, region_details;
	String[] regions, distributions;
	int totalUser = 0;
	JSONArray inactiveInstance;
	int totalAvailability = 0, userCount;
	private long tNo;
	long ltPaidQueueSize = 0;
	String ltType;

	public LTPaidQueueThread(long ltActualQueue, String ltType) throws Throwable{
		tNo = Constants.getLongCount();
		this.ltType = ltType;
		this.con = DataBaseManager.giveConnection();
		this.ltPaidQueueSize = ltActualQueue;
		LogManager.infoLog("Opened Connection count in LTPaidQueueThread: "+tNo);
		runManager = new RunManager();
	}
	
	@Override
	public void run() {
		StringBuilder stringBuilder_IP = new StringBuilder();
		StringBuilder stringBuilder_Distribution = new StringBuilder();
		String strSearchExcludeIP = null;
		boolean isFirstDistribution = true, isFirstIP = true;
		
		try {
			this.con = DataBaseManager.reEstablishConnection(con);
			
			LogManager.infoLog("Before Paid loadGeneratorCreation()");
			loadGeneratorCreation(this.ltType);
			LogManager.infoLog("After Paid loadGeneratorCreation()");
			
			if(regions!=null){
				// check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				for (int i = 0; i < regions.length; i++) {
					double percentageValue = Double.valueOf(distributions[i]) / 100;
					int distributionCount = (int) (totalUser * percentageValue);

					int neededInstance = 0;
					if (distributionCount % userCount == 0) {
						neededInstance = distributionCount / userCount;
					} else {
						neededInstance = (distributionCount / userCount) + 1;
					}
					if (stringBuilder_Distribution.length() > 0) {
						isFirstDistribution = false;
					}
					for (int dist = 0; dist < neededInstance; dist++) {
						if (isFirstDistribution) {
							stringBuilder_Distribution.append(Integer.valueOf(distributions[i]) / neededInstance);
							isFirstDistribution = false;
						} else {
							stringBuilder_Distribution.append(",").append(Integer.valueOf(distributions[i]) / neededInstance);
						}
					}
					
					// If Controller is free then, it could be also assigned as LoadGen for a LT-Test
					if( Constants.CONTROLLER_AS_LOADGEN_FOR_ANY_USER ) {
						strSearchExcludeIP = null;
					} else {
						strSearchExcludeIP = Constants.FG_CONTROLLER_IP;
					}
					
					if (loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER")) {
						//strSearchExcludeIP = null;
						osType = "FEDORA";
					} else {
						osType = "WINDOWS";
					}
					
					inactiveInstance = runManager.getInactiveLoadAgentDetails(con, regions[i], osType, strSearchExcludeIP);
					LogManager.infoLog("runid: "+loadTestSchedulerBean.getRunId()+" inactiveinstance: "+inactiveInstance+ "inactiveInstance.size(): "+inactiveInstance.size());
					
					if (inactiveInstance.size() > 0 && neededInstance <= inactiveInstance.size()) {
						for (int j = 0; j <= inactiveInstance.size(); j++) {
							runManager.updateLoadAgentDetails(con, ((JSONObject) inactiveInstance.get(j)).getString("public_ip"), ((JSONObject) inactiveInstance.get(j)).getString("instance_id"));
							runManager.insertRunGenDetails(con, loadTestSchedulerBean, ((JSONObject) inactiveInstance.get(j)).getString("instance_id"));
							LogManager.infoLog("runid: "+loadTestSchedulerBean.getRunId()+"IP ::"+stringBuilder_IP);
							if (stringBuilder_IP.length() > 0) {
								isFirstIP = false;
							}
							if (isFirstIP) {
								stringBuilder_IP.append(((JSONObject) inactiveInstance.get(j)).getString("public_ip"));
								isFirstIP = false;
							} else {
								stringBuilder_IP.append(",").append(((JSONObject) inactiveInstance.get(j)).getString("public_ip"));
							}
							LogManager.infoLog("runid: "+loadTestSchedulerBean.getRunId()+" IP1 ::"+stringBuilder_IP);
							if (j == neededInstance - 1) {
								break;
							}
						}
					}
				}
				
				if (stringBuilder_IP.length() > 0) {
					if (stringBuilder_Distribution.toString().split(",").length == stringBuilder_IP.toString().split(",").length) {
						LogManager.infoLog("runid: "+loadTestSchedulerBean.getRunId()+" IP2 ::"+stringBuilder_IP);
//						response = DrainManager.getLTQueueDetails("getPaidQueue");
//						loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(response));
						loadTestSchedulerBean.setDistributions(stringBuilder_Distribution.toString());
						loadTestSchedulerBean.setLoadgenIpAddress(stringBuilder_IP.toString());
						if (loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER")) {
							//update loadGen details in tblreport master table.
							runManager.updateReportMasterLoadGenIP(con, loadTestSchedulerBean.getRunId(), stringBuilder_IP.toString());
							
							//Update Scriptwise details
							updateScritptWiseStatus(con, loadTestSchedulerBean);
							
							DrainManager.putTestInJMeterProcessQueue(loadTestSchedulerBean);
						} else {
							DrainManager.putTestInControllerQueue(loadTestSchedulerBean);
						}
						
						LogManager.infoLog("runid: "+loadTestSchedulerBean.getRunId()+" IP3 ::"+stringBuilder_IP);
						synchronized (LTPaidQueueTimerTask.previousRunUserId) {
							LTPaidQueueTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
							LogManager.infoLog("synchronized block of remove");
						}
					} else {
						runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId());
					}
				}
			}

		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			runManager = null;
			loadTestSchedulerBean = null;
			regions = null;
			distributions = null;
			region = null;
			osType = null;
			
			UtilsFactory.clearCollectionHieracy( instance_config );
			UtilsFactory.clearCollectionHieracy( region_details );
			UtilsFactory.clearCollectionHieracy( inactiveInstance );
			UtilsFactory.clearCollectionHieracy( stringBuilder_IP );
			UtilsFactory.clearCollectionHieracy( hmInstance );
			
			DataBaseManager.close(con);
			con = null;
			
			LogManager.infoLog("Closed Connection count in LTPaidQueueThread: "+tNo);
		}
	}
	
	private void updateScritptWiseStatus (Connection con, LoadTestSchedulerBean loadTestSchedulerBean) {
		RunManager runManager = null;
		try {
			// To insert Scriptwise Status 
			String scriptIds[] = loadTestSchedulerBean.getScriptId().split(",");
			String scriptNames[] = loadTestSchedulerBean.getScriptName().split(",");
			for ( int k=0; k<scriptIds.length; k++ ){
				HashMap<String, Object> hmStatuswiseDetails = new HashMap<String, Object>();
				hmStatuswiseDetails.put("runid", String.valueOf(loadTestSchedulerBean.getRunId()));
				hmStatuswiseDetails.put("script_id", String.valueOf(scriptIds[k]));
				hmStatuswiseDetails.put("created_users", "0");
				hmStatuswiseDetails.put("completed_users", "0");
				hmStatuswiseDetails.put("http_200_count", "0");
				hmStatuswiseDetails.put("http_300_count", "0");
				hmStatuswiseDetails.put("http_400_count", "0");
				hmStatuswiseDetails.put("http_500_count", "0");
				hmStatuswiseDetails.put("is_completed", "0");
				hmStatuswiseDetails.put("error_count", "0");
				hmStatuswiseDetails.put("script_name", scriptNames[k]);
				if(runManager.isRecordAvailable(con, String.valueOf(loadTestSchedulerBean.getRunId()), String.valueOf(scriptIds[k]))){
					runManager.updateScriptwiseStatus(con,hmStatuswiseDetails);
				}else{
					runManager.insertScriptwiseStatus(con,hmStatuswiseDetails);
				}
			}
			
			runManager = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		
	}
	
	private boolean loadGeneratorCreation(String ltType) throws Throwable{
		String strSearchExcludeIP = null;
		int availability = 0;
		JSONObject joResponse = null;
		runManager = new RunManager();
		hmInstance = new HashMap<JSONObject, Integer>();
		if (ltType.equals("JMETER")) {
			joResponse = DrainManager.getLTQueueDetails("getJmeterQueue").getJSONObjectResponse();
		} else {
			joResponse = DrainManager.getLTQueueDetails("getPaidQueue").getJSONObjectResponse();
		}
		if( joResponse.isEmpty() ){
			return false;
		}
		loadTestSchedulerBean = new LoadTestSchedulerBean();
		loadTestSchedulerBean.fromJSONObject(joResponse);
		synchronized (LTPaidQueueTimerTask.previousRunUserId) {
			if( !LTPaidQueueTimerTask.previousRunUserId.isEmpty() ){
				if( LTPaidQueueTimerTask.previousRunUserId.contains(loadTestSchedulerBean.getUserId())){
					return false;
				} else {
					LTPaidQueueTimerTask.previousRunUserId.add(loadTestSchedulerBean.getUserId());
				}
			} else if( LTPaidQueueTimerTask.previousRunUserId.isEmpty() ){
				LTPaidQueueTimerTask.previousRunUserId.add(loadTestSchedulerBean.getUserId());
			}
			LogManager.infoLog("synchronized block of add");
		}
		regions = loadTestSchedulerBean.getLoadGenRegions().split(",");
		distributions = loadTestSchedulerBean.getDistributions().split(",");
		JSONObject instance_config = runManager.getInstanceDetails(con, "LT");
		
		// To get maxUser for Particular Instance
		if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
			osType = "FEDORA";
		}else{
			osType = "WINDOWS";
		}
		userCount = runManager.getMaxUserCount(con, loadTestSchedulerBean, osType);
		totalUser = loadTestSchedulerBean.getMaxUserCount();
		
		for( int i=0; i<regions.length; i++ ) {
			double percentageValue = Double.valueOf(distributions[i])/100;	//100/100 = 1
			int distributionCount = (int) (totalUser * percentageValue);	// 666 * 1 = 666
			
			int neededInstance = 0;
			if( distributionCount % userCount == 0 ){						// 666%1000 = 666
				neededInstance = distributionCount/userCount;				//
			} else {
				neededInstance = (distributionCount/userCount) + 1;			// 0 + 1 = 1 instance
			}
			region_details = runManager.getRegionDetails(con, regions[i], osType);	
			
			if( Constants.CONTROLLER_AS_LOADGEN_FOR_ANY_USER ) {
				strSearchExcludeIP = null;
				availability = 0;
			} else {
				strSearchExcludeIP = Constants.FG_CONTROLLER_IP;
				availability = runManager.updateInstanceAvailability(con, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), region_details.getString("os_type") );
			}
			inactiveInstance = runManager.getInactiveLoadAgentDetails(con, regions[i], osType, strSearchExcludeIP);
			
			if( region_details.getInt("reservation") - availability >= (neededInstance - inactiveInstance.size()) ) {
				if( neededInstance < inactiveInstance.size() ){
					hmInstance.put( region_details, 0 );
				}else{
					hmInstance.put( region_details, (neededInstance - inactiveInstance.size()) );
				}
				region_details = null;
				inactiveInstance = null;
			} else {
//				response = DrainManager.getLTQueueDetails("getPaidQueue");
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(response));
				// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
				runManager.updateNonAvailabilityRegion(con, "No Region Available", loadTestSchedulerBean);
				hmInstance.clear();
				synchronized (LTPaidQueueTimerTask.previousRunUserId) {
					LTPaidQueueTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
					LogManager.infoLog("synchronized block of remove");
				}
				hmInstance = null;
//				loadTestSchedulerBean = null;
				break;
			}
		}
		
		if( hmInstance != null ) {
			String instanceType = null;
			if( loadTestSchedulerBean != null ) {
				instanceType = runManager.ltLicenseInstanceDetails(con, loadTestSchedulerBean.getLicense());	
			}
			
			// List of Threads for stored thread variable
			List<Thread> threads = new ArrayList<Thread>();
			for( Map.Entry<JSONObject, Integer> e: hmInstance.entrySet() ) {
				
				if( e.getValue() > 0 ) {
					Thread th = new InstanceCreationThread(instance_config, e.getKey(), loadTestSchedulerBean, con, instanceType, e.getValue());
					th.start();
					threads.add(th);
				}
			}
			
			// To check all Threads status before going to finally block
			while(true){
				 int count = 0;
				 for ( Thread th : threads){
					 if(!th.isAlive()){
						 count ++;
					 }
				 }
				 if(count == threads.size()){
					 break;
				 }
				 else{
					Thread.sleep(1000);
				 }
			 }
		}
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
