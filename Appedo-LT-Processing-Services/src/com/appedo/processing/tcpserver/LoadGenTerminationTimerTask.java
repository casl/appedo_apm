package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.Date;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.UtilsFactory;

public class LoadGenTerminationTimerTask extends TimerTask{
	
	RunManager runManager = null;
	Connection con = null;
	JSONArray jaDynamicAgentDetails = null;
	long tNo;

	public LoadGenTerminationTimerTask(){
		this.con = DataBaseManager.giveConnection();
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in LoadGenTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		try {
			// check for DB connection
			this.con = DataBaseManager.reEstablishConnection(this.con);

			
			runManager = new RunManager();
			jaDynamicAgentDetails = new JSONArray();
			jaDynamicAgentDetails = runManager.getDynamicLoadAgentDetails(con);
			for( int i=0; i<jaDynamicAgentDetails.size(); i++ ){
				JSONObject joLTDetail = jaDynamicAgentDetails.getJSONObject(i);
				long nextCheckTime = (long) joLTDetail.get("next_check_time");
				Date dt = new Date();
				if( nextCheckTime <= dt.getTime() ){
					if( joLTDetail.get("instance_status").toString().equalsIgnoreCase("inactive") ){
						runManager.terminateInstance(con, joLTDetail.getString("endpoint").toString(), joLTDetail.get("instance_id").toString());
					}else{
						runManager.updateNextCheckTime(con, joLTDetail.get("instance_id").toString(), nextCheckTime);
					}
				}
				joLTDetail = null;
				dt = null;
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			UtilsFactory.clearCollectionHieracy( jaDynamicAgentDetails );
			runManager = null;
			LogManager.infoLog("Closed Connection count in LoadGenTimerTask: "+tNo);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		super.finalize();
	}
}
