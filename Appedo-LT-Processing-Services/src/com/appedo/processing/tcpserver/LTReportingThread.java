package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.Calendar;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.RunManager;

public class LTReportingThread extends Thread{
	Connection con = null;
	RunManager rm = null;
	long lRunId = 0;
	private long tNo;

	public LTReportingThread() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Connection count in LTReportingThread: "+tNo);
		rm = new RunManager();
	}
	
	@Override
	public void run() {
		String strQueueDetails = null;
		
		try {
			strQueueDetails = DrainManager.getLTQueueDetails("getRunID").getResponse();
			lRunId = Long.parseLong(strQueueDetails);
			LogManager.infoLog("Report Generating for RunId: " + lRunId);
			
			if (lRunId > 0) {
				
				// check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				// rm.ltSummaryReport(con, lRunId);
				rm.updateReportMasterStatus(con, lRunId, Constants.AUTO_REPORT_PREPARATION_STARTED );
				
				Calendar cal = Calendar.getInstance();
				Constants.AUTO_REPORT_START_TIME = cal.getTimeInMillis();
				if( rm.autoReportPreparation(con, lRunId) ){
					rm.autoReportGeneration(con, lRunId);
				}
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
			LogManager.infoLog("Closed Connection count in LTReportingThread: "+tNo);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		rm = null;
		super.finalize();
	}
}
