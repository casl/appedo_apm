package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TimerTask;

import net.sf.json.JSONArray;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LTFreeQueueCountTimerTask extends TimerTask{
	RunManager runManager = null;
	Connection con = null;
	JSONArray instance_status;
	long tNo;
	
	public static Set<Long> previousRunUserId = new HashSet<Long>();
	
	public LTFreeQueueCountTimerTask() throws Throwable{
		try {
			this.con = DataBaseManager.giveConnection();
			tNo = Constants.getLongCount();
			LogManager.infoLog("Opened Connection count in LTFreeQueueCountTimerTask: "+tNo);
			runManager = new RunManager();
			instance_status = runManager.getInactiveLoadAgentDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS", null);
			int count = runManager.getLoadAgentDetailsCount(con, Constants.FG_CONTROLLER_IP);
			if( instance_status.size() == 0 && count == 0 ){
				HashMap<String, String> hm = new HashMap<String, String>();
				hm.put("instance_id", "123_abc");
				hm.put("region", Constants.DEFAULT_LOADGEN);
				hm.put("endpoint", "");
				hm.put("location", "");
				hm.put("instance_status", "inactive");
				hm.put("public_ip", Constants.FG_CONTROLLER_IP);
				hm.put("start_time", String.valueOf(new Date().getTime()));
				hm.put("os_type", "WINDOWS");
				runManager.insertLoadAgentDetails( con, hm );
			}
		} catch (Throwable th) {
			throw th;
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
	
	@Override
	public void run() {
		try {

			TaskExecutor executor = null;
			
			executor = TaskExecutor.getExecutor(Constants.DEFAULT_LT_FREE_SERVICE);
			
			long freeQueueThreadSize = Long.valueOf(Constants.FREE_QUEUE_THREAD);
			for(int i=0; i<freeQueueThreadSize; i++ ){
				long activeThreadSize = executor.activeThreadCount();
				if(activeThreadSize < freeQueueThreadSize){
					executor.submit(new LTFreeQueueThread());
				}
			}
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			runManager = null;
			UtilsFactory.clearCollectionHieracy( instance_status );
			LogManager.infoLog("Closed Connection count in LTFreeQueueCountTimerTask: "+tNo);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
