package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.json.JSONObject;

import com.amazonaws.services.ec2.model.Instance;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.AmazonConfiguration;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.UtilsFactory;

public class InstanceCreationThread extends Thread{
	
	JSONObject instance_config, region_details;
	String instanceType; 
	LoadTestSchedulerBean loadTestSchedulerBean;
	Connection con;
	RunManager runManager = null;
	AmazonConfiguration amazonConfiguration = null;
	int instanceCount = 0;
	List instanceId;

	public InstanceCreationThread(JSONObject instance_config1, JSONObject region_details1, LoadTestSchedulerBean loadTestSchedulerBean1, Connection con1, String instanceType1, int maxInstanceCount){
		this.instanceType = instanceType1;
		this.region_details = region_details1;
		this.instance_config = instance_config1;
		this.loadTestSchedulerBean = loadTestSchedulerBean1;
		this.con = con1;
		this.instanceCount = maxInstanceCount;
	}
	
	@Override
	public void run() {
		try {
			runManager = new RunManager();
			amazonConfiguration = new AmazonConfiguration();
			String strControllerStatus = "inactive";
			long startTime = System.currentTimeMillis();
			long endTime = startTime + (600*1000);
			List<Instance> ipAddress = null;
			JSONObject joResponse = null;
			
			if( instance_config != null ){
				LogManager.infoLog("Before createInstance :"+ region_details.getString("image_id")+" SECGROUP: "+instance_config.getString("sec_group_name"));
				instanceId = amazonConfiguration.createInstance(region_details.getString("image_id"), region_details.getString("endpoint"), instanceType, instance_config.getString("keypair"), instance_config.getString("sec_group_name"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), instanceCount);
				LogManager.infoLog("After createInstance :"+ region_details.getString("image_id"));
				// To update in Region Details Table
				runManager.updateInstanceAvailability(con, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), region_details.getString("os_type"));
				Object obj = instanceId.get(0);
				if (obj instanceof String){
					String errorCheck = (String)instanceId.get(0);
					if( errorCheck.contains("Error:") ){
						if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
							// Need to change
							joResponse = DrainManager.getLTQueueDetails("getJmeterQueue").getJSONObjectResponse();
							loadTestSchedulerBean.fromJSONObject(joResponse);
						}else {
							if(loadTestSchedulerBean.getLicense().equalsIgnoreCase("level0")){
								joResponse = DrainManager.getLTQueueDetails("getFreeQueue").getJSONObjectResponse();
								loadTestSchedulerBean.fromJSONObject(joResponse);
								synchronized (LTFreeQueueCountTimerTask.previousRunUserId) {
									LTFreeQueueCountTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
									LogManager.infoLog("synchronized block of remove");
								}
							} else {
//								response = DrainManager.getLTQueueDetails("getPaidQueue");
//								loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(response));
								synchronized (LTPaidQueueTimerTask.previousRunUserId) {
									LTPaidQueueTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
									LogManager.infoLog("synchronized block of remove");
								}
								// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
							}
						}
						updateTableValues(errorCheck.replace("Error:", ""), loadTestSchedulerBean);
					} 	
				} else {
					Thread.sleep(1000);
					LogManager.infoLog("Created instance id: "+instanceId);
					while(ipAddress == null){
						ipAddress = amazonConfiguration.getPublicIP(instanceId, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"));
						for ( Instance ins: ipAddress ){
							if ( ins.getPublicIpAddress() == null ){
								ipAddress = null;	
							}
						}
					}
					
					if( !loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
						for( Instance ins: ipAddress ){
							insertDynamicLoadAgentDetails(strControllerStatus, endTime, ins.getPublicIpAddress(), ins.getInstanceId());
						}
					} else {
						//Thread.sleep(60000);
						for( Instance ins: ipAddress ){
							insertDynamicLoadAgentDetailsJMETER(strControllerStatus, endTime, ins.getPublicIpAddress(), ins.getInstanceId());
						}
					}
					
					ipAddress = null;
					instanceId = null;
					strControllerStatus = null;
					instance_config = null;
					instanceType = null;
					loadTestSchedulerBean = null;
					joResponse = null;
				}
				
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			runManager = null;
			amazonConfiguration = null;
		}
	}
	
	private void updateTableValues(String status, LoadTestSchedulerBean loadTestSchedulerBean) throws Throwable{
		try {
			runManager.updateNonAvailabilityRegion(con, status, loadTestSchedulerBean);
			runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId());
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	private void insertDynamicLoadAgentDetails(String strControllerStatus, long endTime, String ipAddress, String instanceId) throws Throwable{
		JSONObject joResponse = null;
		
		LogManager.infoLog("IpAddress: "+ipAddress);
		while( System.currentTimeMillis() < endTime ){
			strControllerStatus = runManager.getControllerStatus(ipAddress, 8889);
			if( strControllerStatus.equalsIgnoreCase("active") ){
				LogManager.infoLog("Inside the if before breaking");
				break;
			}
		}
		LogManager.infoLog("While loop completed");
		
		if ( strControllerStatus.equalsIgnoreCase("inactive") ){
			if(loadTestSchedulerBean.getLicense().equalsIgnoreCase("level0")){
				joResponse = DrainManager.getLTQueueDetails("getFreeQueue").getJSONObjectResponse();
				loadTestSchedulerBean.fromJSONObject(joResponse);
				synchronized (LTFreeQueueCountTimerTask.previousRunUserId) {
					LTFreeQueueCountTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
					LogManager.infoLog("synchronized block of remove");
				}
			} else {
//				response = DrainManager.getLTQueueDetails("getPaidQueue");
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(response));
				synchronized (LTPaidQueueTimerTask.previousRunUserId) {
					LTPaidQueueTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
					LogManager.infoLog("synchronized block of remove");
				}
			}
			
			// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
			updateTableValues("LoadGen is not Active", loadTestSchedulerBean);
		}
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("instance_id", instanceId);
		hm.put("region", region_details.getString("region"));
		hm.put("endpoint", region_details.getString("endpoint"));
		hm.put("location", "");
		hm.put("instance_status", "inactive");
		hm.put("public_ip", ipAddress);
		hm.put("start_time", String.valueOf(new Date().getTime()));
		if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
			hm.put("os_type", "FEDORA" );
		}else{
			hm.put("os_type", "WINDOWS" );
		}
		runManager.insertLoadAgentDetails( con, hm );
		UtilsFactory.clearCollectionHieracy( hm );
	}
	
	private void insertDynamicLoadAgentDetailsJMETER(String strControllerStatus, long endTime, String ipAddress, String instanceId) throws Throwable{
		JSONObject joResponse = null;
		
		LogManager.infoLog("IpAddress: "+ipAddress);
		//commeted below part, as it required for LT, need to confirm the same for JMETER also.
		/*while( System.currentTimeMillis() < endTime ){
			strControllerStatus = runManager.getControllerStatus(ipAddress, 8889);
			if( strControllerStatus.equalsIgnoreCase("active") ){
				LogManager.infoLog("Inside the if before breaking");
				break;
			}
		}
		LogManager.infoLog("While loop completed");*/
		
		//commeted below part, as it required for LT, need to confirm the same for JMETER also.
		/*if ( strControllerStatus.equalsIgnoreCase("inactive") ){
			if(loadTestSchedulerBean.getLicense().equalsIgnoreCase("level0")){
				joResponse = DrainManager.getLTQueueDetails("getFreeQueue").getJSONObjectResponse();
				loadTestSchedulerBean.fromJSONObject(joResponse);
				synchronized (LTFreeQueueCountTimerTask.previousRunUserId) {
					LTFreeQueueCountTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
					LogManager.infoLog("synchronized block of remove");
				}
			} else {
//				response = DrainManager.getLTQueueDetails("getPaidQueue");
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(response));
				synchronized (LTPaidQueueTimerTask.previousRunUserId) {
					LTPaidQueueTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
					LogManager.infoLog("synchronized block of remove");
				}
			}
			
			// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
			updateTableValues("LoadGen is not Active", loadTestSchedulerBean);
		}*/
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("instance_id", instanceId);
		hm.put("region", region_details.getString("region"));
		hm.put("endpoint", region_details.getString("endpoint"));
		hm.put("location", "");
		hm.put("instance_status", "inactive");
		hm.put("public_ip", ipAddress);
		hm.put("start_time", String.valueOf(new Date().getTime()));
		if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
			hm.put("os_type", "FEDORA" );
		}else{
			hm.put("os_type", "WINDOWS" );
		}
		runManager.insertLoadAgentDetails( con, hm );
		UtilsFactory.clearCollectionHieracy( hm );
	}
	
}
