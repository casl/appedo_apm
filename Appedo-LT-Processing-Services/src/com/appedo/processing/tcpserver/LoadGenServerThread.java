
package com.appedo.processing.tcpserver;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.w3c.dom.Document;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.UtilsFactory;

public class LoadGenServerThread extends Thread {
	Connection con = null;
	RunManager rm = null;
//	LTScheduler ltScheduler = null;
	
	long lUserid = 1l;
	Socket socketCon = null;
	int varFirst = 0;
	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	LoadGenServerThread(Socket sCon) throws Throwable {
		try {
			this.socketCon = sCon;
			rm = new RunManager();
//			ltScheduler = new LTScheduler();
			start();
		}catch(Throwable th) {
			System.out.println(th);
		}
	}
	
	public void run() {
		try {
			acceptData();
		} catch (Throwable e) {
			System.out.println(e);
		}
	}
	
	public synchronized void acceptData() throws Throwable {
		
		String operation="";
		int streamLength=0;
		OutputStream varos = null;
		InputStream instream  = null;
		String strReceivedStreamContent = "";		
//		UserDBI userDBI = null;
		long lRunId = 0L;
		
		try {
			// if(socketCon.getInetAddress().getHostName()=="127.0.0.1")
			// InetAddress ip = InetAddress.getLocalHost();
			/** print the hostname and port number of the connection */
			/**Instantiate an InputStream with the optional character encoding. */
			InputStream bis=socketCon.getInputStream();
			/**Read the socket's InputStream and append to a StringBuffer */		      
			// To get the header information 
			OutputStream os = socketCon.getOutputStream();
			// Get Header from received conection
			String header= rm.readHeader(bis);	
//			System.out.println("header  :" + header);
			String[] tokens=header.trim().split(": ");
			operation=tokens[0].toString();
			streamLength=Integer.parseInt(tokens[1].toString().split("\r\n")[0]);					
			// System.out.println("OPERATION ="+operation);

			if(streamLength>0)	{
				int  readCount = 0;
				byte[] bytes = new byte[streamLength];
				
				while (readCount < streamLength) {
					readCount+= bis.read(bytes, readCount, streamLength-readCount);
				}
				strReceivedStreamContent = new String(bytes);
			}
			
			switch(operation) {
			
				case "TEST" : {
					String strresponse = null;
					byte[] b = null;
					try {
						
						strresponse="OK: 0\r\nurl= "+Constants.LT_EXECUTION_SERVICES_FAILED.trim()+"\r\n\r\n";
						b=strresponse.getBytes();
						os.write(b, 0, b.length);
						
					}catch(Exception e) {
						System.out.println(e);
					}finally {
//						strresponse = null;
//						b = null;
						UtilsFactory.close(os);
					}
					break;
				}
				
				case "getrundetail": {
					byte[] b = null;
					LoadTestSchedulerBean ltb = null;
					String process = null;
					String scenarioName = null;
					String reportName = null;
					String loadgen = null;
					String distributions = null;
					String txtbody = null;
					Document varDoc = null;
					Document scenarioDoc = null;
					Document mergeDoc = null;
					HashMap<Long, String> hmap = null;
					try {
						con = DataBaseManager.giveConnection();
//						ltb = LTScheduler.pollProcessingQueue();
//						rm.updatePrivateCounters(con, "lt_controller_queue", LTScheduler.ltQueues.size());
						
						lRunId = ltb.getRunId();
						varDoc = rm.getVariables(ltb.getUserId());
						scenarioDoc = rm.getScenarios(ltb.getUserId(), ltb.getScenarioId());
						mergeDoc =rm.mergeDoc(varDoc, scenarioDoc);
						txtbody = rm.convertDocumentToString(mergeDoc);
						loadgen = "loadgens= "+ltb.getLoadgenIpAddress();
						reportName = "runid= "+lRunId;
						scenarioName = "scenarioname= "+ltb.getScenarioName();
						distributions = "distribution= "+ltb.getDistributions();
						
						rm.updateReportMasterLoadGen(con, lRunId, loadgen);
						
						// Store Script Id and Name to DB
						hmap = rm.getScriptId(ltb.getUserId(), ltb.getScenarioId());
						rm.insertRunScriptDetails(con, lRunId, hmap);
						
						process ="RUN: "+txtbody.length()+"\r\n"+reportName+"\r\n"+scenarioName+"\r\n"+loadgen+"\r\n"+distributions+"\r\n\r\n"+txtbody;
						b = process.getBytes();
						os.write(b, 0, b.length);
						
					} catch(Exception e) {
						System.out.println(e);
					} finally {
						b = null;
						process = null;
						scenarioName = null;
						reportName = null;
						loadgen = null;
						txtbody = null;
						varDoc = null;
						scenarioDoc = null;
						mergeDoc = null;
						ltb = null;
						DataBaseManager.close(con);
						con = null;
						UtilsFactory.close(os);
					}
					break;
				}
				
				case "scriptwisestatus" : {

					String strCreatedUser = "";
					String strCompletedUser = "";
					String str200Count = "", str300Count = "", str400Count ="", str500Count = "", strIsCompleted = "";
					String strScriptId = "";
					String strScriptName = "";
					String strErrorCount = "";
					String strRespHeader = "";
					String strRunId = "";
					String[] splitLines = null;
					String[] splitLine = null;
					byte[] b = null;
					//long lRunId = 0L;
					HashMap<String, Object> hmStatuswiseDetails = null;
					String[] strTokens = null;
					int is_completed = 0;
					
					try {
						con = DataBaseManager.giveConnection();
						strTokens = tokens[1].split("\r\n");
						strRunId = strTokens[1].toString().split("= ")[1].trim();
						is_completed = Integer.valueOf(strTokens[2].toString().split("= ")[1]);
						
						lRunId = Long.parseLong(strRunId);
						splitLines = strReceivedStreamContent.split("\r\n");
						for(String str : splitLines) {
							if(str.contains(",")) {
								hmStatuswiseDetails = new HashMap<String, Object>();
								hmStatuswiseDetails.put("runid", strRunId);
								splitLine = str.split(",");
								strScriptId = splitLine[0];
								hmStatuswiseDetails.put("script_id", strScriptId);
								strCreatedUser = splitLine[1] ;
								hmStatuswiseDetails.put("created_users", strCreatedUser);
								strCompletedUser = splitLine[2];
								hmStatuswiseDetails.put("completed_users", strCompletedUser);
								str200Count = splitLine[3];
								hmStatuswiseDetails.put("http_200_count", str200Count);
								str300Count = splitLine[4];
								hmStatuswiseDetails.put("http_300_count", str300Count);
								str400Count = splitLine[5];
								hmStatuswiseDetails.put("http_400_count", str400Count);
								str500Count = splitLine[6];
								hmStatuswiseDetails.put("http_500_count", str500Count);
								strIsCompleted =  splitLine[7];
								hmStatuswiseDetails.put("is_completed", strIsCompleted);
								strErrorCount = splitLine[8];
								hmStatuswiseDetails.put("error_count", strErrorCount);
								strScriptName = splitLine[9];
								hmStatuswiseDetails.put("script_name", strScriptName);
								if(rm.isRecordAvailable(con, strRunId, strScriptId)){
									rm.updateScriptwiseStatus(con,hmStatuswiseDetails);
								}else{
									rm.insertScriptwiseStatus(con,hmStatuswiseDetails);
								}
							}
						}
						
						strRespHeader = "ok: 0\r\n\r\n";
						b = strRespHeader.getBytes();
						os.write(b, 0, b.length);
						
						if( is_completed == 1 ){
							
							rm.updateInActiveLoadAgent(con, lRunId);
							int iWaitLoop=0;
							while(iWaitLoop<11){
								long queuelength = rm.getRunQueueLength(con, lRunId);
								if(queuelength == 0){
									iWaitLoop=0;
									rm.updateReportMasterStatus(con, lRunId, Constants.AUTO_REPORT_PREPARATION_STARTED );
									if(rm.autoReportPreparation(con, lRunId)){
										rm.autoReportGeneration(con, lRunId);
									}
									rm.createSummaryTable(con, lRunId);
									// Queue
									DrainManager.putRunId(lRunId);
									break;
								}else{
									iWaitLoop++;
								}
								Thread.sleep(3000); // 11 Sec delay as we are polling every 10 secs.
							}
							
							if(iWaitLoop == Constants.QUEUE_WAITING_LOOP_COUNT){ //10
								rm.updateReportMasterStatus(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
							}
							
						}
						
//						System.out.println("Received Script Wise Status: \n"+strReceivedStreamContent+"\n Run_Id : "+lRunId);						
					}catch(Exception e) {
						System.out.println(e);
					}finally {
						DataBaseManager.close(con);
						con = null;
						strRespHeader = null;
						strRunId = null;
						b = null;
						splitLines = null;
						splitLine = null;
						strCreatedUser = null;
						strCompletedUser = null;
						str200Count = null;
						str300Count = null;
						str400Count = null;
						str500Count = null;
						strIsCompleted = null;
						strScriptId = null;
						strScriptName = null;
						strErrorCount = null;
						UtilsFactory.clearCollectionHieracy(hmStatuswiseDetails);
						UtilsFactory.close(os);
					}
					break;
				}
				
			}
		}catch(Exception e) {
			System.out.println(e);
			throw e;
		}finally {
			socketCon.close();
			operation = null;
			UtilsFactory.close(varos);
			varos = null;
			UtilsFactory.close(instream);
			instream  = null;
			strReceivedStreamContent = null;
		}
	}
}
