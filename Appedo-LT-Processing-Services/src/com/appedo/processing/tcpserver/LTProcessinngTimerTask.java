package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.sql.PreparedStatement;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.utils.UtilsFactory;

public class LTProcessinngTimerTask extends Thread{
	Connection con = null;
	JSONObject json = null;
	JSONArray jaArray = null;
	long lRunId = 0;
	String loadgens = null;
	String ltData = null;
	private long tNo;
	String type = null;

	public LTProcessinngTimerTask() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Thread in LTP TimerTask count: "+tNo);
	}
	
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		HttpClient client = new HttpClient();
		PostMethod method = null;
		PreparedStatement pstmt = null;
		
		try {
			// check for DB connection
			this.con = DataBaseManager.reEstablishConnection(this.con);
			
			int i = 0;
			while (i < 10) {
				method = new PostMethod(Constants.WEBSERVICE_HOST+ "/ltQueue/getData");
				int statusCode = client.executeMethod(method);
				if (statusCode != HttpStatus.SC_OK) {
					LogManager.errorLog("Method failed: "+ method.getStatusLine());
				}

				String strReportData = method.getResponseBodyAsString();
				// LogManager.infoLog("RESPONSE:: "+strReportData);

				if (!strReportData.isEmpty()) {
					json = JSONObject.fromObject(strReportData);
					lRunId = Long.valueOf(json.getString("runid"));
					
					sbQuery.setLength(0);
					sbQuery.append("UPDATE tblreportmaster SET queue_length = queue_length-1 WHERE runid = "+ lRunId);
					pstmt = con.prepareStatement(sbQuery.toString());
					pstmt.executeUpdate();

					insertLTData(json);
				} else {
					Thread.sleep(500); // To be rechecked
				}
				i++;
			}
			// method.setRequestHeader("Connection", "close");

		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;

			DataBaseManager.close(con);
			con = null;

			client.getHttpConnectionManager().closeIdleConnections(0);
			method.releaseConnection();
			method = null;
			LogManager.infoLog("Closed Thread in LTP TimerTask Count: " + tNo);
			sbQuery.setLength(0);
		}
	}
	
	private void insertLTData(JSONObject jsonData) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			type = jsonData.getString("type");
			
			switch (type) {
			case "log": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery	.append("insert into lt_log_").append(lRunId).append(" (iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id) ")
						.append("select iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id from json_populate_recordset(NULL::lt_log_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append("); ");
				break;
			}
			case "error": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery.append("insert into lt_error_").append(lRunId).append(" (iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id, scriptid, container_id, container_name) ")
				.append("select iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id, scriptid, container_id, container_name from json_populate_recordset(NULL::lt_error_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append("); ");
				break;
			}
			case "reporddata": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery	.append("INSERT INTO lt_reportdata_").append(lRunId).append(" (source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize, end_hrs, end_mins, end_secs, first_byte_received_on, ttfb_ms) ")
						.append("SELECT source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize, end_hrs, end_mins, end_secs, first_byte_received_on, ttfb_ms ")
						.append("FROM json_populate_recordset(NULL::lt_reportdata_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append(")");
				break;
			}
			case "transactions": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery.append("insert into lt_transactions_").append(lRunId).append(" (script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff) ")
				.append("select script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff from json_populate_recordset(NULL::lt_transactions_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append("); ");
				break;
			}
			case "userdetail": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery	.append("insert into lt_user_runtime_").append(lRunId).append(" (runtime, runtype, userid, script_id, loadgen_name) ")
						.append("select runtime, runtype, userid, script_id, loadgen_name from json_populate_recordset(NULL::lt_user_runtime_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append("); ");
				break;
			}
			/*case "loadgenstatus": {
				jaArray = (JSONArray) jsonData.get("data");
				sbQuery.append("insert into lt_loadgen_status_").append(lRunId).append(" (loadgen_name, received_on, counter_id, counter_value, end_hrs, end_mins, end_secs) ").append("select loadgen_name, received_on, counter_id, counter_value, end_hrs, end_mins, end_secs from json_populate_recordset(NULL::lt_loadgen_status_").append(lRunId).append(",").append(UtilsFactory.makeValidVarchar(jaArray.toString())).append("); ");
				break;
			}*/
			}

			pstmt = con.prepareStatement(sbQuery.toString());
			try {
				long startTime = System.currentTimeMillis();
				pstmt.executeUpdate();
				LogManager.infoLog("RunId:" + lRunId + ":: DB Ins Time : " + (System.currentTimeMillis() - startTime) + "," + "JSON :: Type: " + type + " Size:" + jaArray.size());
			} catch (Exception e) {
				LogManager.errorLog(e);
				LogManager.errorLog("Error in DB Insert in insertLTJsonDetails: " + sbQuery.toString());
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
