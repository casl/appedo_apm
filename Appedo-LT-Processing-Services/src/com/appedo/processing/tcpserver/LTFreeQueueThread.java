package com.appedo.processing.tcpserver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.model.AmazonConfiguration;
import com.appedo.processing.model.RunManager;
import com.appedo.processing.utils.UtilsFactory;

public class LTFreeQueueThread extends Thread{
	AmazonConfiguration amazonConfiguration = null;
	RunManager runManager = null;
	Connection con = null;
	JSONObject region_details, instance_config;
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	private long tNo;

	public LTFreeQueueThread() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Connection count in LTFreeQueueThread: "+tNo);
		runManager = new RunManager();
	}
	
	@Override
	public void run() {
		String strResponse = null;
		JSONObject joResponse = null;
		
		try {
			long ltFreeQueueSize = 0;
			
			this.con = DataBaseManager.reEstablishConnection(con);
			
			strResponse = DrainManager.getLTQueueDetails("getFreeQueueSize").getResponse();
			
			if (!strResponse.equals("0")) {
				ltFreeQueueSize = Long.parseLong(strResponse);
				
				if (ltFreeQueueSize > 4) {
					loadGeneratorCreator(ltFreeQueueSize);
				}
				
				joResponse = DrainManager.getLTQueueDetails("peekFreeQueue").getJSONObjectResponse();
				if( joResponse.isEmpty() ){
					return;
				}
				
				// check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject( joResponse );
				
				JSONArray inactiveInstance = runManager.getInactiveLoadAgentDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS", null);
				if (inactiveInstance.size() == 0) {
					// Pending to be discussed
				} else {
					// Need to clarify on ltFreeQueueSize/inactiveInstance
					for (int j = 0; j < inactiveInstance.size(); j++) {
						JSONObject joObject = (JSONObject) inactiveInstance.get(j);
						joResponse = DrainManager.getLTQueueDetails("getFreeQueue").getJSONObjectResponse();
						loadTestSchedulerBean.fromJSONObject(joResponse);
						// runManager.updatePrivateCounters(con, "lt_free_q", ltFreeQueueSize);
						loadTestSchedulerBean.setLoadgenIpAddress(joObject.getString("public_ip"));
						runManager.updateLoadAgentDetails(con, joObject.getString("public_ip"), ((JSONObject) inactiveInstance.get(j)).getString("instance_id"));
						runManager.insertRunGenDetails(con, loadTestSchedulerBean, joObject.getString("instance_id"));
						DrainManager.putTestInControllerQueue(loadTestSchedulerBean);
						synchronized (LTFreeQueueCountTimerTask.previousRunUserId) {
							LTFreeQueueCountTimerTask.previousRunUserId.remove(loadTestSchedulerBean.getUserId());
							LogManager.infoLog("synchronized block of remove");
						}
						joObject = null;
						if( ltFreeQueueSize <= j+1 ){
							break;
						}
					}
				}
			}

		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			amazonConfiguration = null;
			runManager = null;
			loadTestSchedulerBean = null;
			UtilsFactory.clearCollectionHieracy( instance_config );
			UtilsFactory.clearCollectionHieracy( region_details );
			DataBaseManager.close(con);
			con = null;
			LogManager.infoLog("Closed Connection count in LTFreeQueueThread: "+tNo);
		}
	}
	
	private boolean loadGeneratorCreator(long ltFreeQueueSize) throws Throwable{
		JSONObject joResponse = null;
		amazonConfiguration = new AmazonConfiguration();
		runManager = new RunManager();
		int maxInstanceCreated = runManager.getCreatedInstanceCount(con, Constants.DEFAULT_LOADGEN, "WINDOWS");
		if (maxInstanceCreated <= Integer.valueOf(Constants.MAX_LOADGEN_CREATION)) {
			region_details = runManager.getRegionDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS");
			instance_config = runManager.getInstanceDetails(con, "LT");
			int availability = amazonConfiguration.getAmazonCount(region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"));
			if (region_details.getInt("reservation") - availability > 0) {
				joResponse = DrainManager.getLTQueueDetails("peekFreeQueue").getJSONObjectResponse();
				if( joResponse.isEmpty() ){
					return false;
				}
				
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject(joResponse);
				synchronized (LTFreeQueueCountTimerTask.previousRunUserId) {
					if( !LTFreeQueueCountTimerTask.previousRunUserId.isEmpty() ){
						if( LTFreeQueueCountTimerTask.previousRunUserId.contains(loadTestSchedulerBean.getUserId())){
							return false;
						} else {
							LTFreeQueueCountTimerTask.previousRunUserId.add(loadTestSchedulerBean.getUserId());
						}
					} else if( LTFreeQueueCountTimerTask.previousRunUserId.isEmpty() ){
						LTFreeQueueCountTimerTask.previousRunUserId.add(loadTestSchedulerBean.getUserId());
					}
					LogManager.infoLog("synchronized block of add");
				}
				String instanceType = runManager.ltLicenseInstanceDetails(con, loadTestSchedulerBean.getLicense());

				// List of Threads for stored thread variable
				List<Thread> threads = new ArrayList<Thread>();
				if (instance_config != null) {
					// for (int i = 0; i < (ltFreeQueueSize) / 4; i++) {
						Thread th = new InstanceCreationThread(instance_config, region_details, loadTestSchedulerBean, con, instanceType, (int) ((ltFreeQueueSize) / 4));
						th.start();
						threads.add(th);
					// }
				}

				// To check all Threads status before going to finally block
				while (true) {
					int count = 0;
					for (Thread th : threads) {
						if (!th.isAlive()) {
							count++;
						}
					}
					if (count == threads.size()) {
						break;
					} else {
						Thread.sleep(1000);
					}
				}
			}
		}
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}
}
