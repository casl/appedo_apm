package com.appedo.processing.tcpserver;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.Connection;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.dbi.RunDBI;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.UtilsFactory;

import net.sf.json.JSONObject;

public class JMeterProcessingThread  extends Thread {

	Connection con = null;
	private long tNo;
	JSONObject joResponse = null;
	
	public JMeterProcessingThread() throws Throwable {
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		//this.joResponse = joResponse;
		//LogManager.infoLog("Opened Thread in RUM TimerTask count: "+tNo);
	}
	
	@Override
	public void run() {
	
		String loadGenIps[] = null;
		String loadGenIp = null;
		JSONObject joLoadGenData = null;
		Socket clientSocket = null;
		ObjectOutputStream objOutputStream = null;
		ObjectInputStream objInputStream = null;
		RunDBI runDBI = null;
		LoadTestSchedulerBean loadTestSchedulerBean = null;
		try {
			
			runDBI = new RunDBI();
			loadTestSchedulerBean = new LoadTestSchedulerBean();
			joResponse = DrainManager.getJMeterQueueDetails("getJMeterProcessQueue").getJSONObjectResponse();
			
			if (joResponse == null && !joResponse.getBoolean("success")) {
				throw new Exception("Draining JMeterProcessQueue returns null or failure response.");
			}
			
			joLoadGenData = joResponse.getJSONObject("message");
			
			//joLoadGenData = DrainManager.getJMeterQueueDetails("peekJMeterProcessQueue").getJSONObjectResponse();
			
			loadTestSchedulerBean.fromJSONObject(joLoadGenData);
			
			loadGenIps = joLoadGenData.getString("LoadgenIpAddress").split(",");
			
			loadGenIp = loadGenIps[0];
			
			clientSocket = new Socket(loadGenIp, Integer.parseInt(Constants.JM_CONTROLLER_PORT));
			objOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
            System.out.println("Sending request to Socket Server running in JMeterLoadGen: "+loadGenIp);
            
            objOutputStream.writeObject(joLoadGenData.toString());
            
            objInputStream = new ObjectInputStream(clientSocket.getInputStream());
            String message = (String) objInputStream.readObject();
            
            if (message !=  null && message.trim().startsWith("{") && message.trim().endsWith("}")) {
            	JSONObject joResp = JSONObject.fromObject(message);
            	if (joResp.getBoolean("success")) {
            		LogManager.infoLog("Success reponse received from jmeter loadGen jar");
            		//Updating loadGen IP in tblreportmaster table.
                    runDBI.updateJMeterTestRunnningInReportMaster(this.con, joLoadGenData.getLong("lRunId"), loadGenIp);
            	} else {
            		LogManager.errorLog("Failure response from jmeter loadgen jar: "+joResp.getString("errorMessage"));
            		//confirm whether does required to queue again.
            	}
            } else {
            	LogManager.errorLog("Invalid server Socket response from jmeter loadgen jar.");
            	System.out.println("Invalid server Socket response from jmeter loadgen jar.");
            }
            
            objInputStream.close();
            objOutputStream.close();
            clientSocket.close();
            runDBI = null;
		} catch (Throwable e) {
			LogManager.errorLog("JMeterProcessingThread: "+e.getMessage());
			//Queuing JMeter Test again as processing fails, need to confirm
			try {
				DrainManager.putTestInJMeterProcessQueue(loadTestSchedulerBean);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			UtilsFactory.clearCollectionHieracy(objInputStream);
			objInputStream = null;
			
			UtilsFactory.clearCollectionHieracy(objOutputStream);
            objOutputStream = null;
			
            UtilsFactory.clearCollectionHieracy(clientSocket);
            clientSocket = null;
            
		}
		
	}
	
}
