package com.appedo.processing.tcpserver;

import java.net.ServerSocket;
import java.net.Socket;

import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;

public class LoadGenServer extends Thread {
	
	private ServerSocket servSoc = null;
	Socket socketCon = null;
	
	public LoadGenServer() throws Throwable {
		
	}
	
	public void run() 
	{
		try {
			listen();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	
	public void listen() throws Throwable {
		
		//1. creating a server socket - 1st parameter is port number and 2nd is the Queuelimit
		servSoc = new ServerSocket(Integer.parseInt(Constants.LT_PROCESSING_PORT) , 0);
		//2. Wait for an incoming connection
		LogManager.infoLog("LoadGen Server up.Waiting for connection...");
		
		while(true) {
			try {
				/** get the connection socket */
				socketCon = servSoc.accept(); // conn contains handle to socket type
				new LoadGenServerThread(socketCon);
			}catch(Throwable t) {
				LogManager.errorLog(t);
			}
		}
	}
}