package com.appedo.processing.tcpserver;

import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class JMeterDrainingTimerTask  extends TimerTask{

private long tNo;
	
	public JMeterDrainingTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in JMeterDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		String strQueueDetails = null;
		
		TaskExecutor executor = null;
		
		try {
			long ltActualQueue = 0L;
			strQueueDetails = DrainManager.getJMeterQueueDetails("getJMeterProcessQueueSize").getResponse();
			ltActualQueue = Long.parseLong( strQueueDetails );
			//System.out.println("JMETER_SYSOUT: Queue Actual Returns: "+ strQueueDetails);
			// long connectionSize = Long.valueOf(Constants.PROCESSING_THREAD);
			long connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
			
			/*System.out.println("JMETER_SYSOUT: DataBaseManager.getMaxTotal(): "+ DataBaseManager.getMaxTotal());
			System.out.println("JMETER_SYSOUT: Constants.MAX_THREADS_RETAINED: "+ Constants.MAX_THREADS_RETAINED);
			System.out.println("JMETER_SYSOUT: DataBaseManager.getCurrentActiveCount(): "+ DataBaseManager.getCurrentActiveCount());
			System.out.println("JMETER_SYSOUT: connectionSize: "+ connectionSize);*/
			// System.out.println("connectionSize:: "+connectionSize);
			long ltQueueSize = ltActualQueue < connectionSize ? ltActualQueue : connectionSize;
			//System.out.println("JMETER_SYSOUT: ltQueueSize: "+ ltQueueSize);
			if( ltQueueSize <= 0 ) {
				LogManager.errorLog("JMETER: ltActualQueue: "+ltActualQueue+", DataBaseManager.getMaxTotal(): "+DataBaseManager.getMaxTotal()+", DataBaseManager.getCurrentActiveCount(): "+DataBaseManager.getCurrentActiveCount()+", connectionSize: "+connectionSize);
				LogManager.errorLog("Unable to create New Threads. As required count is "+ltQueueSize);
			} else {
				for(int i=0; i<ltQueueSize; i++ ){
					executor = TaskExecutor.getExecutor(Constants.DEFAULT_JMETER_THREADPOOL_NAME);
					executor.submit(new JMeterProcessingThread());
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Closed Connection count in JMeterDrainingTimerTask: " + tNo);
		super.finalize();
	}
	
}
