package com.appedo.processing.manager;

import java.util.HashMap;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import net.sf.json.JSONObject;

import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LoadTestSchedulerBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.utils.UtilsFactory;
import com.appedo.processing.utils.WebServiceManager;

/**
 * Manager to do common activities for any agent.
 * Activities such as send first request, send counter values are defined here.
 * 
 * @author Ramkumar
 *
 */
public class DrainManager {
	
	/**
	 * Constructor which initializes the HashMap which holds the counter-set and the ArrayList which holds the counter-sets
	 */
	public DrainManager() {
		
	}
	
	public static JSONObject getRUMQueueDetails(String param) throws Throwable {
		JSONObject joResponse = null;
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_RUM_HOST+"/rumQueue/"+param);
			
			joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse == null ) {
				throw new Exception( wsm.getResponse() );
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getRUMQueueDetails: "+e.getMessage());
			throw e;
		}
		
		return joResponse;
	}
	
	public static JSONObject getCIQueueDetails(String param) throws Throwable {
		JSONObject joResponse = null;
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_RUM_HOST+"/ciQueue/"+param);
			
			joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse == null ) {
				throw new Exception( wsm.getResponse() );
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getCIQueueDetails: "+e.getMessage());
			throw e;
		}
		
		return joResponse;
	}
	
	public static WebServiceManager getLTQueueDetails(String param) throws Throwable {
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_HOST+"/ltQueue/"+param);
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getLTQueueDetails: "+e.getMessage());
			throw e;
		}
		
		return wsm;
	}
	
	public static WebServiceManager getJMeterQueueDetails(String param) throws Throwable {
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_HOST+"/ltQueue/"+param);
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getJMeterQueueDetails: "+e.getMessage());
			throw e;
		}
		
		return wsm;
	}
	
	/**
	 * To get LOG Queue details
	 * 
	 * @param param
	 * @return
	 * @throws Throwable
	 */
	public static JSONObject getLOGQueueDetails(String param) throws Throwable {
		JSONObject joResponse = null;
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_LOG_HOST+"/logQueue/"+param);
			
			joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse == null ) {
				throw new Exception( wsm.getResponse() );
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getLOGQueueDetails: "+e.getMessage());
			throw e;
		}
		
		return joResponse;
	}
	
	public static JSONObject getSLALog(String param) throws Throwable {
		JSONObject joResponse = null;
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest(Constants.WEBSERVICE_LOG_HOST+"/logQueue/"+param);
			//System.out.println("URL : "+Constants.WEBSERVICE_LOG_HOST+"/logQueue/"+param);
			joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse == null ) {
				throw new Exception( wsm.getResponse() );
			}
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in getSLALog: "+e.getMessage());
			throw e;
		}
		
		return joResponse;
	}
	
	public static String putRunId(long runId) throws Throwable {
		String strResponse = null;
		
		try{
			WebServiceManager wsm = new WebServiceManager();
			wsm.addParameter("runid", String.valueOf(runId));
			wsm.sendRequest(Constants.WEBSERVICE_HOST+"/ltQueue/putRunID");
			
			strResponse = wsm.getResponse();
			
		} catch(Throwable e) {
			LogManager.errorLog("Exception in putRunId: "+e.getMessage());
			throw e;
		}
		
		return strResponse;
	}
	
	public static String writeFile(String filePath, String dataFile, String overwrite) throws Throwable {
		String strResponse = null;
		
		try{
			WebServiceManager wsm = new WebServiceManager();
			wsm.addParameter("file_path", filePath);
			wsm.addParameter("file_content", dataFile);
			wsm.addParameter("overwrite", overwrite);
			wsm.sendRequest(Constants.APPEDO_URL_FILE_TRANSFER+"writeFile");
			
			strResponse = wsm.getResponse();
			
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog("Exception in writeFile: "+e.getMessage());
			throw e;
		}
		
		return strResponse;
	}
	
	public static String getLTQueueDetailsAfterChange(HttpClient client, PostMethod method) throws Throwable {
		
		String responseStream = null;
		HashMap<String, String> hmResponse = new HashMap<String, String>();
		
		try{
			int statusCode = client.executeMethod(method);
			// System.err.println("statusCode: "+statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				LogManager.errorLog("Method failed: " + method.getStatusLine());
			}
			
			responseStream = method.getResponseBodyAsString();
			// System.out.println("responseJSONStream: "+responseStream);
		} catch(Throwable e) {
			LogManager.errorLog("Exception in getLTQueueDetailsAfterChange: "+e.getMessage());
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy( hmResponse );
		}
		
		return responseStream;
	}
	
	public static String putTestInControllerQueue(LoadTestSchedulerBean testBean) throws Exception {
		HttpClient client = null;
		PostMethod method = null;
		String responseStream = null;
		HashMap<String, String> hmResponse = new HashMap<String, String>();
		
		try{
			client = new HttpClient();
			// URLEncoder.encode(requestUrl,"UTF-8");
			method = new PostMethod(Constants.WEBSERVICE_HOST+"/ltQueue/putInControllerQueue");
			method.addParameter("testBean", testBean.toJSON().toString());
			method.setRequestHeader("Connection", "close");
			int statusCode = client.executeMethod(method);
			// System.err.println("statusCode: "+statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				LogManager.errorLog("Method failed: " + method.getStatusLine());
			}
			
			responseStream = method.getResponseBodyAsString();
			// System.out.println("responseJSONStream: "+responseStream);
		} catch(Throwable e) {
			LogManager.errorLog("Exception in putTestInControllerQueue: "+e.getMessage());
			throw e;
		} finally {
			method.releaseConnection();
			method = null;
			UtilsFactory.clearCollectionHieracy( hmResponse );
		}
		
		return responseStream;
	}
	
	public static String putTestInJMeterProcessQueue(LoadTestSchedulerBean testBean) throws Exception {
		HttpClient client = null;
		PostMethod method = null;
		String responseStream = null;
		HashMap<String, String> hmResponse = new HashMap<String, String>();
		
		try{
			client = new HttpClient();
			// URLEncoder.encode(requestUrl,"UTF-8");
			method = new PostMethod(Constants.WEBSERVICE_HOST+"/ltQueue/putInJMeterProcessQueue");
			method.addParameter("testBean", testBean.toJSON().toString());
			method.setRequestHeader("Connection", "close");
			int statusCode = client.executeMethod(method);
			// System.err.println("statusCode: "+statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				LogManager.errorLog("Method failed: " + method.getStatusLine());
			}
			
			responseStream = method.getResponseBodyAsString();
			// System.out.println("responseJSONStream: "+responseStream);
		} catch(Throwable e) {
			LogManager.errorLog("Exception in putTestInControllerQueue: "+e.getMessage());
			throw e;
		} finally {
			method.releaseConnection();
			method = null;
			UtilsFactory.clearCollectionHieracy( hmResponse );
		}
		
		return responseStream;
	}
}
