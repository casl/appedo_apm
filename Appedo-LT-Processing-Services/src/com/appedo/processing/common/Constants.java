package com.appedo.processing.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;
import com.appedo.processing.utils.UtilsFactory;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author navin
 *
 */
public class Constants {
	
	public static boolean DEV_ENVIRONMENT = false;
	
	public static String THIS_JAR_PATH = null;
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	public static String SMTP_MAIL_CONFIG_FILE_PATH = "";
	public static String DELAY_SAMPLING = "";
	
	public static String PATH = ""; 
	public static String VUSCRIPTSPATH = "";
	public static String JMETERVUSCRIPTSFOLDERPATH = "";
	public static String JMETERVUSCRIPTSPATH = "";
	public static String FLOODGATESVUSCRIPTSPATH = "";
	public static String FLOODGATESSCENARIOXMLPATH = "";
	public static String FLOODGATESSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLPATH = "";
	public static String JMETERSCENARIOFOLDERPATH = "";
	public static String JMETERSCENARIOSPATH = "";
	public static String CSVFILE = "";
	public static String UPLOADPATH = "";
	public static String VARIABLEPATH = "";
	public static String SUMMARYREPORTPATH = "";
	public static String VARIABLEXMLPATH = "";
	public static String VARIABLEXMLFOLDERPATH = "";
	public static String JMETERTESTPATH = "";
	public static String JMETERTESTJTLPATH = "";
	public static String JMETERCSVPATH = "";
	public static String JMETERSUMMARYREPORTPATH="";
	public static String DOWNLOADS = "";
	public static int TIME_INTERVAL_MS = 100;
	public static int LOG_GETSIZE_INTERVAL_MS = 10000;
	
	public static String SELENIUM_SCRIPT_CLASS_FILE_PATH = "";
	public static String FROMEMAILADDRESS = "";

	public static String FG_CONTROLLER_IP = "";
	public static String FG_CONTROLLER_PORT = "";
	public static String LT_PROCESSING_PORT = "";
	public static String FG_APPLICATION_IP = "";
	
	public static String JM_CONTROLLER_IP= "";
	public static String JM_CONTROLLER_PORT = "";

	public static String APPEDO_URL = "";
	public static String APPEDO_URL_FILE_TRANSFER = "";
	public static String LT_EXECUTION_SERVICES_FAILED = "";
	public static String WEBSERVICE_HOST = "";
	public static String WEBSERVICE_RUM_HOST = "";
	public static String WEBSERVICE_LOG_HOST = "";
	public static String FLOODGATESVUSCRIPTSFOLDERPATH = "";
	public static String FLOODGATESVUSCRIPTSTEMPFOLDERPATH = "";
	public static String APPEDO_SCHEDULER = "";
	public static String LICENSEPATH = "";
	public static String LICENSEXMLPATH = "";
	public static String APPEDO_HARFILES = "";
	public static String HAR_REPOSITORY_URL = "";
	public static String APPEDO_COLLECTOR = "";
	public static String MAX_COUNTERS = "";
	public static String MAX_LOADGEN_CREATION = "";
	public static String DEFAULT_LOADGEN = "";
	public static boolean CONTROLLER_AS_LOADGEN_FOR_ANY_USER = false;
	public static HashMap<String, String> AGENT_LATEST_BUILD_VERSION = new HashMap<String, String>();
	public static String FG_VUSCRIPTS_TEMP_PATH = "";
	public static String SUMMARY_PATH;
	//public static JSONObject LOAD_TEST_LICENSE_DETAILS = new JSONObject();
	
	
	// Limitation for Counter Charts time window
	public static int COUNTER_CHART_START_DELAY = 60;//62;
	public static int COUNTER_CHART_END_DELAY = 0;//2;
	public static int COUNTER_MINICHART_START_DELAY = 10;//12;
	public static int COUNTER_MINICHART_END_DELAY = 0;//2;
	
	// tried to append 0 for diff between the prev result time and current result time is > 3 min
	public static final int COUNTER_CHART_TIME_INTERVAL = 3;
	public static final int COUNTER_CHART_ADD_0_FOR_EVERY = 3;
	
	public static String CLASS_EXPORT_URL;
	
	public static String SELENIUM_SCRIPT_PACKAGES, DEFAULT_LT_THREADPOOL_NAME, DEFAULT_JMETER_THREADPOOL_NAME, DEFAULT_RUM_THREADPOOL_NAME, DEFAULT_LOG_THREADPOOL_NAME, DEFAULT_LOG_SLA_THREADPOOL_NAME, DEFAULT_CI_THREADPOOL_NAME, MAX_THREADS_RETAINED, PROCESSING_THREAD, DEFAULT_LT_REPORTING_NAME, REPORTING_THREAD, FREE_QUEUE_THREAD, PAID_QUEUE_THREAD;
	
	public static String DEFAULT_LT_FREE_SERVICE, DEFAULT_LT_PAID_SERVICE, DEFAULT_JMETER_SERVICE;
	
	public static int QUEUE_WAITING_LOOP_COUNT = 10;//2;
	
	public static String RUM_LOCATION;
	public static String APPEDO_SLA_COLLECTOR_URL;
	
	public static String GRAFANA_DASHBOARD_DEFAULT_SLUG = null;
	public static String GRAFANA_COLLECTOR_PROTOCAL = null;
	public static String GRAFANA_COLLECTOR_IP = null;
	public static Integer GRAFANA_COLLECTOR_PORT = null;
	public static String GRAFANA_LOADGEN_PROTOCAL = null;
	public static Integer GRAFANA_LOADGEN_PORT = null;
	
	public static String AUTO_REPORT_PREPARATION_STARTED = "AUTO REPORT PREPARATION STARTED";
	public static String AUTO_REPORT_GENERATION_STARTED = "AUTO REPORT GENERATION STARTED";
	public static String AUTO_REPORT_GENERATION_COMPLETED = "AUTO REPORT GENERATION COMPLETED";
	public static String AUTO_REPORT_GENERATION_FAILED = "AUTO REPORT GENERATION FAILED";
	public static long AUTO_REPORT_START_TIME = 0;

	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";
	
	public static boolean IS_CAPTCHA_VALIDATION_ENABLE = true;
	
	public static HashMap<String, String> MINICHART_COUNTERS = new HashMap<String, String>();
	
	public static AtomicLong count = new AtomicLong(1);
	
	public static long getLongCount(){
		return count.incrementAndGet();
	}
	
	public enum CHART_START_INTERVAL {
		_1("3 hours"), _2("24 hours"), _3("7 days"), _4("15 days"), _5("30 days");
		
		private String strInterval;
		
		private CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	/**
	 * for SUM chart results interval
	 * @author navin
	 *
	 */
	public enum SUM_CHART_START_INTERVAL {
		_1("24 hours"), _2("7 days"), _3("15 days"), _4("30 days"), _5("60 days"), _6("120 days"), _7("180 days");
		
		private String strInterval;
		
		private SUM_CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	public enum SLA_BREACH_SEVERITY {
		WARNING("WARNING"), CRITICAL("CRITICAL");
		
		private String strSLABreachSeverity;
		
		private SLA_BREACH_SEVERITY(String sSLABreachSeverity) {
			strSLABreachSeverity = sSLABreachSeverity;
		}
		
		public String toString() {
			return strSLABreachSeverity;
		}
	}
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties() throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(THIS_JAR_PATH+File.separator+"config.properties");
			prop.load(is);
			
			// Appedo application's resource directory path
			RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
			
			APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
			
			PATH = RESOURCE_PATH+prop.getProperty("Path");
			
			FLOODGATESVUSCRIPTSFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptsfolderpath");
			FLOODGATESVUSCRIPTSTEMPFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptstempfolderpath");
			VUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("VUScriptspath");
			FLOODGATESVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptspath");
			JMETERVUSCRIPTSFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptsfolderpath");
			JMETERVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptspath");
			FLOODGATESSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("floodgatesscenarioxmlpath");
			FLOODGATESSCENARIOXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesscenarioxmlfolderpath");
			JMETERSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlpath");
			JMETERSCENARIOXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlfolderpath");
			JMETERSCENARIOFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariofolderpath");
			CSVFILE = RESOURCE_PATH+prop.getProperty("Csvfile");
			UPLOADPATH = RESOURCE_PATH+prop.getProperty("Uploadpath");
			VARIABLEPATH = RESOURCE_PATH+prop.getProperty("Variablepath");
			SUMMARYREPORTPATH = prop.getProperty("summaryreportpath");
			VARIABLEXMLPATH = RESOURCE_PATH+prop.getProperty("VariableXMLpath");
			VARIABLEXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("VariableXMLFolderpath");
			JMETERSCENARIOSPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariospath");
			JMETERTESTPATH = RESOURCE_PATH+prop.getProperty("jmetertestpath");
			JMETERTESTJTLPATH = RESOURCE_PATH+prop.getProperty("jmetertestjtlpath");
			DOWNLOADS = RESOURCE_PATH+prop.getProperty("downloads");
			JMETERCSVPATH = RESOURCE_PATH+prop.getProperty("jmetercsvpath");
			JMETERSUMMARYREPORTPATH = RESOURCE_PATH+prop.getProperty("jmetersummaryreportpath");
			LICENSEPATH = RESOURCE_PATH+prop.getProperty("licensepath");
			LICENSEXMLPATH = RESOURCE_PATH+prop.getProperty("licensexmlpath");
			MAX_COUNTERS = prop.getProperty("max_counters");
			DELAY_SAMPLING = prop.getProperty("DELAY_SAMPLING");
			MAX_LOADGEN_CREATION = prop.getProperty("MAX_LOADGEN_CREATION");
			DEFAULT_LOADGEN = prop.getProperty("DEFAULT_LOADGEN");
			if( prop.containsKey("CONTROLLER_AS_LOADGEN_FOR_ANY_USER") ) {
				CONTROLLER_AS_LOADGEN_FOR_ANY_USER = Boolean.parseBoolean( prop.getProperty("CONTROLLER_AS_LOADGEN_FOR_ANY_USER") );
			}
			FG_VUSCRIPTS_TEMP_PATH = RESOURCE_PATH+prop.getProperty("FG_VUSCRIPTS_TEMP_PATH");
			// Mail configuration 
			SMTP_MAIL_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("SMTP_MAIL_CONFIG_FILE_PATH");
			FROMEMAILADDRESS = prop.getProperty("FromEmailAddress");
			
			Constants.SELENIUM_SCRIPT_CLASS_FILE_PATH = Constants.RESOURCE_PATH+prop.getProperty("sumtransactionfilepath");
			SELENIUM_SCRIPT_PACKAGES = prop.getProperty("seleniumscriptpackages"); 
			DEFAULT_LT_THREADPOOL_NAME = prop.getProperty("DEFAULT_LT_THREADPOOL_NAME");
			DEFAULT_JMETER_THREADPOOL_NAME = prop.getProperty("DEFAULT_JMETER_THREADPOOL_NAME");
			DEFAULT_RUM_THREADPOOL_NAME = prop.getProperty("DEFAULT_RUM_THREADPOOL_NAME");
			DEFAULT_LOG_THREADPOOL_NAME = prop.getProperty("DEFAULT_LOG_THREADPOOL_NAME");
     		DEFAULT_LOG_SLA_THREADPOOL_NAME = prop.getProperty("DEFAULT_LOG_SLA_THREADPOOL_NAME");
			DEFAULT_CI_THREADPOOL_NAME = prop.getProperty("DEFAULT_CI_THREADPOOL_NAME");
			DEFAULT_LT_REPORTING_NAME = prop.getProperty("DEFAULT_LT_REPORTING_NAME");
			DEFAULT_LT_FREE_SERVICE = prop.getProperty("DEFAULT_LT_FREE_SERVICE");
			DEFAULT_LT_PAID_SERVICE = prop.getProperty("DEFAULT_LT_PAID_SERVICE");
			DEFAULT_JMETER_SERVICE = prop.getProperty("DEFAULT_JMETER_SERVICE");
			MAX_THREADS_RETAINED = prop.getProperty("MAX_THREADS_RETAINED");
			PROCESSING_THREAD = prop.getProperty("PROCESSING_THREAD");
			REPORTING_THREAD = prop.getProperty("REPORTING_THREAD");
			FREE_QUEUE_THREAD = prop.getProperty("FREE_QUEUE_THREAD");
			PAID_QUEUE_THREAD = prop.getProperty("PAID_QUEUE_THREAD");
			SUMMARY_PATH = RESOURCE_PATH+prop.getProperty("SUMMARY_PATH");
			
		} catch(Exception e) {
			System.out.println(e);
			throw e;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	public static void reloadConstantsProperties() throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(THIS_JAR_PATH+File.separator+"config.properties");
			prop.load(is);
			
	 		// Appedo application's resource directory path
	 		MAX_THREADS_RETAINED = prop.getProperty("MAX_THREADS_RETAINED");
	 		PROCESSING_THREAD = prop.getProperty("PROCESSING_THREAD");
	 		REPORTING_THREAD = prop.getProperty("REPORTING_THREAD");
	 		
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Loads appedo config properties, from the system specifed path, 
	 * (Note.. loads other than db related)
	 *  
	 * @param strAppedoConfigPath
	 */
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			if( prop.getProperty("ENVIRONMENT") != null && prop.getProperty("ENVIRONMENT").equals("DEVELOPMENT") 
					&& AppedoConstants.getAppedoConfigProperty("ENVIRONMENT") != null && AppedoConstants.getAppedoConfigProperty("ENVIRONMENT").equals("DEVELOPMENT") ) 
			{
				DEV_ENVIRONMENT = true;
			}
			
	 		// Load Test configuration & LoadGen Agent details
			FG_CONTROLLER_IP = getProperty("FG_CONTROLLER_IP", prop);
			FG_CONTROLLER_PORT = getProperty("FG_CONTROLLER_PORT", prop);
			LT_PROCESSING_PORT = getProperty("LT_PROCESSING_PORT", prop);
			FG_APPLICATION_IP = getProperty("FG_APPLICATION_IP", prop);
			
	 		JM_CONTROLLER_IP = getProperty("JM_CONTROLLER_IP", prop);
	 		JM_CONTROLLER_PORT = getProperty("JM_CONTROLLER_PORT", prop);
	 		
	 		// User's email address verification mail's URL link starter
	 		APPEDO_URL = getProperty("APPEDO_URL", prop);
	 		LT_EXECUTION_SERVICES_FAILED = getProperty("LT_EXECUTION_SERVICES_FAILED", prop);
	 		APPEDO_URL_FILE_TRANSFER = getProperty("APPEDO_URL_FILE_TRANSFER", prop);
	 		
	 		// Url to delete har files from har repository
	 		HAR_REPOSITORY_URL = getProperty("HAR_REPOSITORY_URL", prop);	
	 		
	 		// Webservice collector
	 		WEBSERVICE_HOST = getProperty("LT_QUEUE_SERVICES", prop);
	 		WEBSERVICE_RUM_HOST = getProperty("RUM_QUEUE_SERVICES", prop);
	 		WEBSERVICE_LOG_HOST = getProperty("LOG_QUEUE_SERVICES", prop);
	 		APPEDO_SCHEDULER = getProperty("APPEDO_SCHEDULER", prop);
	 		APPEDO_HARFILES = getProperty("APPEDO_HARFILES", prop);
	 		CLASS_EXPORT_URL = getProperty("URL_TO_EXPORT_CLASS_FILE", prop);
	 		APPEDO_COLLECTOR = getProperty("APPEDO_COLLECTOR", prop);
	 		RUM_LOCATION = getProperty("RUM_LOCATION", prop);
	 		APPEDO_SLA_COLLECTOR_URL = getProperty("APPEDO_SLA_COLLECTOR", prop);
	 		
	 		GRAFANA_DASHBOARD_DEFAULT_SLUG = getProperty("GRAFANA_SQLITE_DB_DEFAULT_SLUG",prop);
			GRAFANA_COLLECTOR_PROTOCAL = getProperty("GRAFANA_COLLECTOR_PROTOCAL",prop);
			GRAFANA_COLLECTOR_IP = getProperty("GRAFANA_COLLECTOR_IP",prop);
			GRAFANA_COLLECTOR_PORT = Integer.parseInt( getProperty("GRAFANA_COLLECTOR_PORT",prop) );
			GRAFANA_LOADGEN_PROTOCAL = getProperty("GRAFANA_LOADGEN_PROTOCAL",prop);
			GRAFANA_LOADGEN_PORT = Integer.parseInt( getProperty("GRAFANA_LOADGEN_PORT",prop) );
			
	 		IS_CAPTCHA_VALIDATION_ENABLE = Boolean.parseBoolean( UtilsFactory.replaceNull(prop.getProperty("IS_CAPTCHA_VALIDATION_ENABLE"), "true") );
			
		} catch(Throwable th) {
			throw th;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}	
	}
	
	/**
	 * loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoConstants(Connection con) throws Throwable {
		
		try {
			AppedoConstants.getAppedoConstants().loadAppedoConstants(con);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Get the property's value from appedo_config.properties, if it is DEV environment;
	 * Otherwise get the property's value from DB.
	 * 
	 * @param strPropertyName
	 * @param prop
	 * @return
	 */
	private static String getProperty(String strPropertyName, Properties prop) throws Throwable {
		if( DEV_ENVIRONMENT && prop.getProperty(strPropertyName) != null )
			return prop.getProperty(strPropertyName);
		else
			return AppedoConstants.getAppedoConfigProperty(strPropertyName);

	}
}
