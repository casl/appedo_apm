package com.appedo.processing.init;

import java.io.File;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.tcpserver.LTReportTimerTask;
import com.appedo.processing.tcpserver.ReloadConfigTimerTask;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LTReportingInit {

	public static TimerTask ltReportTimerTask= null, reloadTimerTask = null;;
	public static Timer ltReportTimer = new Timer(), ltReloadTimer = new Timer();

	public static void main(String[] args) throws Throwable {
		Connection con = null;
		
		try {
			Constants.THIS_JAR_PATH = UtilsFactory.getThisJarPath();
	 		Constants.LOG4J_PROPERTIES_FILE = Constants.THIS_JAR_PATH+File.separator+"log4j.properties";
	 		
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator( Constants.LOG4J_PROPERTIES_FILE );
			
			// Loads Constant properties
			Constants.loadConstantsProperties();
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Reporting-Service", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			reloadTimerTask = new ReloadConfigTimerTask();
			ltReloadTimer.schedule(reloadTimerTask, 1000*60*10, 1000*60*10);
			
			TaskExecutor.newExecutor(Constants.DEFAULT_LT_REPORTING_NAME, 2, 2, 1); // PoolName, minthread, maxthread, queue
			
			ltReportTimerTask = new LTReportTimerTask();
			ltReportTimer.scheduleAtFixedRate(ltReportTimerTask, 150, 1000*30);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}
