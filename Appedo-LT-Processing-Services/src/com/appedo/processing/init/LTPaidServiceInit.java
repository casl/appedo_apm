package com.appedo.processing.init;

import java.io.File;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.tcpserver.JMeterQueueTimerTask;
import com.appedo.processing.tcpserver.LTPaidQueueTimerTask;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LTPaidServiceInit {

	public static TimerTask timerTaskPaid = null, jmeterTimerTask = null;
	public static Timer timerPaid = new Timer(), jmeterTimer = new Timer();

	public static void main(String[] args) throws Throwable {
		Connection con = null;
		
		try {
			Constants.THIS_JAR_PATH = UtilsFactory.getThisJarPath();
	 		Constants.LOG4J_PROPERTIES_FILE = Constants.THIS_JAR_PATH+File.separator+"log4j.properties";
	 		
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator( Constants.LOG4J_PROPERTIES_FILE );
			
			// Loads Constant properties
			Constants.loadConstantsProperties();
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Paid-Service", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			TaskExecutor.newExecutor(Constants.DEFAULT_LT_PAID_SERVICE, 1, 20, 1); // PoolName, minthread, maxthread, queue
			TaskExecutor.newExecutor(Constants.DEFAULT_JMETER_SERVICE, 1, 20, 1); // PoolName, minthread, maxthread, queue
			
			timerTaskPaid = new LTPaidQueueTimerTask();
			timerPaid.schedule(timerTaskPaid, 500, 1000*10);
			
			jmeterTimerTask = new JMeterQueueTimerTask();
			jmeterTimer.schedule(jmeterTimerTask, 500, 1000*10);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}
