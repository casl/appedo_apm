package com.appedo.processing.init;

import java.io.File;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.tcpserver.ReloadConfigTimerTask;
import com.appedo.processing.timer.LogDrainingTimerTask;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LogProcessInit { 
	
	public static TimerTask logDrainTimerTask = null, reloadTimerTask = null;
	public static Timer logDrainTimer = new Timer(), logReloadTimer = new Timer();
	
	public static void main(String[] args) throws Throwable {
		Connection con = null;

		try {
			Constants.THIS_JAR_PATH = UtilsFactory.getThisJarPath();
	 		Constants.LOG4J_PROPERTIES_FILE = Constants.THIS_JAR_PATH+File.separator+"log4j.properties";
	 		
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator( Constants.LOG4J_PROPERTIES_FILE );
			
			// Loads Constant properties
			Constants.loadConstantsProperties();
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-Log-Processing-Service", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			reloadTimerTask = new ReloadConfigTimerTask();
			logReloadTimer.schedule(reloadTimerTask, 1000 * 60 * 10, 1000 * 60 * 10);
			TaskExecutor.newExecutor(Constants.DEFAULT_LOG_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue

			//Thread Pool for processing LOG SLA
			TaskExecutor.newExecutor(Constants.DEFAULT_LOG_SLA_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue
			
			logDrainTimerTask = new LogDrainingTimerTask();
			logDrainTimer.scheduleAtFixedRate(logDrainTimerTask, 3000, Constants.TIME_INTERVAL_MS);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}