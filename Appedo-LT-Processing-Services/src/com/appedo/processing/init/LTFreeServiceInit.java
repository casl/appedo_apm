package com.appedo.processing.init;

import java.io.File;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.tcpserver.LTFreeQueueCountTimerTask;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LTFreeServiceInit {

	public static TimerTask timerTaskFree = null;
	public static Timer timerCount = new Timer();

	public static void main(String[] args) throws Throwable {
		Connection con = null;
		
		try {
			Constants.THIS_JAR_PATH = UtilsFactory.getThisJarPath();
	 		Constants.LOG4J_PROPERTIES_FILE = Constants.THIS_JAR_PATH+File.separator+"log4j.properties";
	 		
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator( Constants.LOG4J_PROPERTIES_FILE );
			
			// Loads Constant properties
			Constants.loadConstantsProperties();
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Free-Service", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			TaskExecutor.newExecutor(Constants.DEFAULT_LT_FREE_SERVICE, 1, 5, 1); // PoolName, minthread, maxthread, queue
			
			timerTaskFree = new LTFreeQueueCountTimerTask();
			timerCount.schedule(timerTaskFree, 500, 1000*10);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}
