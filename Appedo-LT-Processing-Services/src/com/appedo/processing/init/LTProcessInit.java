package com.appedo.processing.init;

import java.io.File;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.tcpserver.JMeterDrainingTimerTask;
import com.appedo.processing.tcpserver.LTDrainingTimerTask;
import com.appedo.processing.tcpserver.LoadGenServer;
import com.appedo.processing.tcpserver.LoadGenTerminationTimerTask;
import com.appedo.processing.tcpserver.ReloadConfigTimerTask;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

public class LTProcessInit {

	public static TimerTask ltDrainTimerTask= null, reloadTimerTask = null, timerTerminationTaskLoadTest = null, jmeterDrainTimerTask = null;
	public static Timer ltDrainTimer = new Timer(), ltReloadTimer = new Timer(), timerTerminationLT = new Timer(), jmeterDrainTimer = new Timer();

	public static void main(String[] args) throws Throwable {
		Connection con = null;
		
		try {
			Constants.THIS_JAR_PATH = UtilsFactory.getThisJarPath();
	 		Constants.LOG4J_PROPERTIES_FILE = Constants.THIS_JAR_PATH+File.separator+"log4j.properties";
	 		
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator( Constants.LOG4J_PROPERTIES_FILE );
			
			// Loads Constant properties
			Constants.loadConstantsProperties();
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Processing-Service", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			reloadTimerTask = new ReloadConfigTimerTask();
			ltReloadTimer.schedule(reloadTimerTask, 1000*60*10, 1000*60*10);
			
			Thread t1 = new LoadGenServer();
			t1.start();
			
			TaskExecutor.newExecutor(Constants.DEFAULT_LT_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue
			
			TaskExecutor.newExecutor(Constants.DEFAULT_JMETER_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue
			
			//LT draining process started here
			ltDrainTimerTask = new LTDrainingTimerTask();
			ltDrainTimer.scheduleAtFixedRate(ltDrainTimerTask, 150, 1000);
			
			//JMeter draining process started here
			jmeterDrainTimerTask = new JMeterDrainingTimerTask();
			jmeterDrainTimer.scheduleAtFixedRate(jmeterDrainTimerTask, 150, 1000);
			
			timerTerminationTaskLoadTest = new LoadGenTerminationTimerTask();
			timerTerminationLT.schedule(timerTerminationTaskLoadTest, 500, 1000*60);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
}
