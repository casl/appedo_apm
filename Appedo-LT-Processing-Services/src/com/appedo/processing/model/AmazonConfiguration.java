package com.appedo.processing.model;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;

public class AmazonConfiguration {


	public AmazonConfiguration(){
		
	}
	
	public List createInstance(String imageId, String region, String InstanceType, String keyPairName, String secGroupName, String accesskey, String secretkey, int instanceCount) throws InterruptedException {
		List createdInstanceList = new ArrayList();
		
		try {
			AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
			ec2.setEndpoint(region);
			
			RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
			runInstancesRequest.withImageId(imageId)
				.withInstanceType(InstanceType)
				.withKeyName(keyPairName)
				.withSecurityGroups(secGroupName)
				.withMinCount(1)
				.withMaxCount(instanceCount);
			
			RunInstancesResult runInstancesResult = ec2.runInstances(runInstancesRequest);
//			List<Instance> instances = runInstancesResult.getReservation().getInstances();
			createdInstanceList = runInstancesResult.getReservation().getInstances(); 
		} catch (Exception e) {
			System.out.println(e);
			createdInstanceList.add("Error:"+e.getMessage());
		}
		
		return createdInstanceList;
	}

	public List<Instance> getPublicIP(List instanceId, String region, String accesskey, String secretkey){
		List<Instance> publicIp = null;
		try {
			AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
			ec2.setEndpoint(region);
			
			DescribeInstancesRequest ec2Request = new DescribeInstancesRequest();
			
			List<String> instanceIds = new ArrayList<String>();
			for ( Instance ins : (List<Instance>) instanceId ){
				instanceIds.add(ins.getInstanceId());
			}
			
			ec2Request.setInstanceIds(instanceIds);
			DescribeInstancesResult res = ec2.describeInstances(ec2Request);
			List<Reservation> instances = res.getReservations();
			publicIp = instances.get(0).getInstances();
		} catch (Exception e) {
			System.out.println(e);
		}
		return publicIp;
	}
	
	private AmazonEC2Client getAmazonClient(String accesskey, String secretkey) {
		return new AmazonEC2Client(new BasicAWSCredentials(accesskey, secretkey));
	}

	public int getAmazonCount(String region, String accesskey, String secretkey) {
		AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
		ec2.setEndpoint(region);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		DescribeInstancesResult res = ec2.describeInstances(request);
		System.out.println(res.getReservations().size());
		return res.getReservations().size();
	}
	
	public void terminateInstance(String region, List<String> instanceIds, String accesskey, String secretkey){
		AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
		ec2.setEndpoint(region);
		TerminateInstancesRequest tir = new TerminateInstancesRequest(instanceIds);
        ec2.terminateInstances(tir);
	}
}
