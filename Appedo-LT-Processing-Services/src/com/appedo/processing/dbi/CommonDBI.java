package com.appedo.processing.dbi;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.appedo.manager.LogManager;

public class CommonDBI {

	
	public static String createRUMDailyPartition(PreparedStatement pstmt, long lUID, String strPartitionKey) {
		ResultSet rst = null;
		
		try {
			pstmt.setLong(1, lUID);
			pstmt.setString(2, strPartitionKey);
			
			rst = pstmt.executeQuery();
			while( rst.next() ) {
				strPartitionKey = rst.getString(1);
			}
		} catch(Exception ex) {
			LogManager.errorLog(ex);
		}
		
		return strPartitionKey;
	}
	
	public static String createCIDailyPartition(PreparedStatement pstmt, long lUID, long lEventId, String strPartitionKey) {
		ResultSet rst = null;
		
		try {
			pstmt.setLong(1, lUID);
			pstmt.setLong(2, lEventId);
			pstmt.setString(3, strPartitionKey);
			
			rst = pstmt.executeQuery();
			while( rst.next() ) {
				strPartitionKey = rst.getString(1);
			}
		} catch(Exception ex) {
			LogManager.errorLog(ex);
		}
		
		return strPartitionKey;
	}
}
