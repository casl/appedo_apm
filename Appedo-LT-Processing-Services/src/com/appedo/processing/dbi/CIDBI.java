package com.appedo.processing.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.CIDataBean;
import com.appedo.processing.utils.UtilsFactory;

/**
 * Class to do the DML operations of the CI module.
 * 
 * @author veeru
 *
 */
public class CIDBI {
	
	/**
	 * Inserts the given CI-CollectorBean into the transaction table of the given UID-EventId partition.
	 * 
	 * @param con
	 * @param lUID
	 * @param eventId
	 * @param hmap
	 * @param strYear
	 * @param ciLogBean
	 * @throws Throwable
	 */
	public void insertCITable(Connection con, long lEventId, HashMap<String,String> hmap, CIDataBean ciLogBean) throws Throwable {
		PreparedStatement pstmtTrans = null, pstmtPartitionTables = null;
		StringBuilder sbQuery = new StringBuilder();
		StringBuilder sbValues = new StringBuilder();
		StringBuilder sbColumns = new StringBuilder();
		String strPartitionKey = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			strPartitionKey = UtilsFactory.formatYYYYMMDD( ciLogBean.getQueuedOn() );
			
			pstmtPartitionTables = con.prepareStatement("SELECT create_ci_daily_partition_tables(?, ?, ?)");
			
			// Send the date as parameter and get actual partition's suffix. Eg.: UID_yyyymmdd
			strPartitionKey = CommonDBI.createCIDailyPartition(pstmtPartitionTables, ciLogBean.getUID(), lEventId, strPartitionKey);
			
			sbQuery	.append("INSERT INTO ci_trans_").append(strPartitionKey).append(" (ci_event_id, guid, uid, event_name, ")
					//.append("uri, base_url, ")
					.append("ip_address, browser, browser_version, os, os_version, device_name, device_type, ")
					.append("merchant_name, ")
					//.append("merchant_name, referrer_url, evt_start_time");
					  .append("evt_start_time, evt_end_time, evt_duration, email_id, first_name, last_name, age, mobile, received_on, appedo_received_on, agent_type, env ");
			sbValues.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?");
			
			if ( ciLogBean.getCiColumns()  != null ){
				for (Map.Entry<String, String> e: ciLogBean.getCiColumns().entrySet()){
					sbColumns.append(", "+hmap.get(e.getKey()));
					sbValues.append(", '"+e.getValue()+"'");
				}
			}
			
			sbQuery.append(sbColumns).append(")").append(sbValues).append(")");
			//System.out.println("sbQuery: "+sbQuery.toString());
			
			pstmtTrans = con.prepareStatement( sbQuery.toString() );
			pstmtTrans.setLong(1, lEventId);
			pstmtTrans.setString(2, ciLogBean.getGuid());
			pstmtTrans.setLong(3, ciLogBean.getUID());
			pstmtTrans.setString(4, ciLogBean.getEventName());
//			pstmtTrans.setString(5, ciLogBean.getUri());
//			pstmtTrans.setString(6, ciLogBean.getBase_url());
			pstmtTrans.setString(5, ciLogBean.getIpAdress());
			pstmtTrans.setString(6, ciLogBean.getBrowserName());
			pstmtTrans.setString(7, ciLogBean.getBrowserVersion());
			pstmtTrans.setString(8, ciLogBean.getOs());
			pstmtTrans.setString(9, ciLogBean.getOsVersion());
			pstmtTrans.setString(10, ciLogBean.getDevicename());
			pstmtTrans.setString(11, ciLogBean.getDeviceType());
			pstmtTrans.setString(12, ciLogBean.getMerchantName());
			pstmtTrans.setTimestamp(13, new Timestamp( ciLogBean.getEventStartTime() ) );
			pstmtTrans.setTimestamp(14, new Timestamp( ciLogBean.getEventEndTime() ) );
			pstmtTrans.setLong(15, ciLogBean.getEventDuration());
			pstmtTrans.setString(16, ciLogBean.getEmailId());
			pstmtTrans.setString(17, ciLogBean.getFirstName());
			pstmtTrans.setString(18, ciLogBean.getLastName());
			pstmtTrans.setString(19, ciLogBean.getAge());
			pstmtTrans.setString(20, ciLogBean.getMobileNo());
			pstmtTrans.setTimestamp(21, new Timestamp( ciLogBean.getQueuedOn() ) );
			pstmtTrans.setTimestamp(22, new Timestamp( ciLogBean.getQueuedOn() ) );
			pstmtTrans.setString(23, ciLogBean.getAgentType());
			pstmtTrans.setString(24, ciLogBean.getEnv());
			pstmtTrans.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(pstmtTrans);
			pstmtTrans = null;
			
			DataBaseManager.close(pstmtPartitionTables);
			pstmtPartitionTables = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			UtilsFactory.clearCollectionHieracy( sbColumns );
			UtilsFactory.clearCollectionHieracy( sbValues );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * Insert the event, in the given UID partition.
	 * 
	 * @param con
	 * @param lUID
	 * @param strEventName
	 * @param strEventMethod
	 * @param strAgentType
	 * @return
	 * @throws Exception
	 */
	public long insertCIEvents(Connection con, long lUID, String strEventName, String strEventMethod, String strAgentType, String strEnvironment) throws Exception {
		long eventId = 0;
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery	.append("INSERT INTO ci_events_"+lUID+" (event_name, event_method, agent_type, env, created_on) ")
					.append("VALUES (?, ?, ?, ?, now())");
			//System.out.println("sbQuery: "+sbQuery);
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			
			pstmt.setString(1, strEventName);
			pstmt.setString(2, strEventMethod);
			pstmt.setString(3, strAgentType);
			pstmt.setString(4, strEnvironment);
			pstmt.executeUpdate();
			eventId = DataBaseManager.returnKey( pstmt );
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return eventId;
	}
	
	/**
	 * Insert the event's properties, in the given UID partition.
	 * 
	 * @param con
	 * @param lUID
	 * @param eventId
	 * @param ciLogBean
	 * @throws Exception
	 */
	public void insertEventProperty(Connection con, long lUID, long eventId, String ciColumnKey) throws Exception {
		PreparedStatement pstmtProp = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			
			sbQuery.append("insert into ci_evt_prop_"+lUID+" (ci_event_id, property_name, created_on) ")
					.append("values (?, ?, now())");
			
			pstmtProp = con.prepareStatement(sbQuery.toString());
			pstmtProp.setLong(1, eventId);
			pstmtProp.setString(2, ciColumnKey);
			pstmtProp.executeUpdate();
					
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(pstmtProp);
			pstmtProp = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * Get all the event properties for the given UID-EventId
	 * 
	 * @param con
	 * @param lUID
	 * @param lEventId
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> getEventProperty(Connection con, long lUID, long lEventId) throws Exception {
		HashMap<String, String> hmap = new HashMap<String, String>();
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			strQuery = "SELECT * FROM ci_evt_prop_"+lUID+" WHERE ci_event_id = '"+lEventId+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			while( rst.next() ){
				hmap.put(rst.getString("property_name"), rst.getString("column_name"));
			}
		} catch (Exception e) {
			LogManager.errorLog( e );
		} finally {
			DataBaseManager.close( rst );
			rst = null;
			
			DataBaseManager.close( stmt );
			stmt = null;
			
			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return hmap;
	}
	
	/**
	 * Returns the UID of the Rum Module, for the given encrypted UID.
	 * 
	 * @param con
	 * @param strEncryptedUID
	 * @return
	 * @throws Exception
	 */
	public long getModuleUID(Connection con, String strGUID) throws Exception {
		long lUID = -1l;
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();
		
		try{
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				lUID = rst.getLong("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		
		return lUID;
	}

	public boolean isValueExist(Connection con, long lUID, long eventId, String propertyName) throws Exception {
		boolean isExist = false;
		String strQuery = null;
		
		Statement stmt = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			strQuery = "SELECT * FROM ci_evt_prop_"+lUID+" WHERE ci_event_id = '"+eventId+"' AND property_name = '"+propertyName+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			if( rst.next() ){
				isExist = true;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return isExist;
	}
	
	public synchronized void updateTemplate(Connection con, long lUID, long eventId, String propertyName) throws Exception {
		String strQuery = null;
		int totalCount = 0;
		
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			strQuery = "SELECT count(*) as total_count FROM ci_evt_prop_"+lUID+" WHERE ci_event_id = '"+eventId+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			if( rst.next() ){
				totalCount = rst.getInt("total_count");
			}
			
			strQuery = null;
			strQuery = "UPDATE ci_evt_prop_"+lUID+" SET column_name = ? WHERE property_name = ? AND ci_event_id = ?";
			pstmt = con.prepareStatement(strQuery);
			pstmt.setString(1, "col_"+totalCount);
			pstmt.setString(2, propertyName);
			pstmt.setLong(3, eventId);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		}  finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void createCITransTablePartitions(Connection con, long uid, long eventId) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery.append("SELECT create_ci_trans_table_partitions(?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, uid);
			pstmt.setLong(2, eventId);
			pstmt.execute();
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public long isExistEvents(Connection con, long lUID, String strEventMethod, String strEventName, String strAgentType, String strEnvironment) throws Exception {
		long eventId = 0;
		StringBuilder sbQuery = new StringBuilder();
		
		Statement stmt = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery	.append("SELECT ci_event_id FROM ci_events_").append(lUID).append(" ")
					.append("WHERE event_name = '").append(strEventName).append("' AND event_method = '").append(strEventMethod).append("' ")
					.append("AND agent_type = '").append(strAgentType).append("' AND env = '").append(strEnvironment).append("' ");
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(sbQuery.toString());
			
			if( rst.next() ){
				eventId = rst.getLong("ci_event_id");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		
		return eventId;
	}
}
