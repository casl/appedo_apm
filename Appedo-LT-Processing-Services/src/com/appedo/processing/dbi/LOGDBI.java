package com.appedo.processing.dbi;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LOGDataBean;
import com.appedo.processing.utils.UtilsFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * To insert the log details
 * 
 */
public class LOGDBI { 
	
	/**
	 * To insert the log messages into their respective tables according to their log type
	 * 
	 * @param con
	 * @param logBean
	 * @throws Throwable
	 */
	public void addLOGDataToBatch(CallableStatement callableStmt, LOGDataBean logBean) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		
		//System.out.println(logBean.getLogData().toString());
		
		try {
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        
	        String strDate = sdf.format(UtilsFactory.toDate(logBean.getLogData().getString("@timestamp"), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
	        //System.out.println(strDate);
			callableStmt.setString(1, logBean.getLogData().getString("guid"));
			callableStmt.setString(2, logBean.getLogData().getString("type"));
			callableStmt.setString(3, strDate);
			callableStmt.setString(4, logBean.getLogData().toString());
			//Add to Batch
			callableStmt.addBatch();
		} catch (Throwable t) {
			LogManager.errorLog("Exception while processing: "+logBean.toString());
			LogManager.errorLog(t, sbQuery);
			throw t;
		} /*finally {
			//DataBaseManager.close(pstmt);
			//pstmt = null;
			
			//UtilsFactory.clearCollectionHieracy( sbQuery );
		}*/
	}
	
	public CallableStatement createLogBreachCallablestmt(Connection con) throws Throwable {
		CallableStatement callableStmt = null;
		StringBuilder sbQuery = new StringBuilder();
		try {
			sbQuery.append("{ call insert_log_breach_data(?, ?::json)}");
			callableStmt = con.prepareCall(sbQuery.toString());
		} catch (Throwable t) {
			LogManager.errorLog("Exception while preparing connection statement for Log Breach data Batch : ");
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return callableStmt;
	}
	
	public void addLogBreachToBatch(CallableStatement callableStmt, JSONObject joLogBreachData) throws Throwable {
		
		try {
	        
				callableStmt.setLong(1, joLogBreachData.getLong("user_id"));
				callableStmt.setString(2, joLogBreachData.toString());
				//Add to Batch
				callableStmt.addBatch();
			
		} catch (Throwable t) {
			LogManager.errorLog("Exception while processing: "+joLogBreachData.toString());
			LogManager.errorLog(t);
			throw t;
		} /*finally {
			//DataBaseManager.close(pstmt);
			//pstmt = null;
			
			//UtilsFactory.clearCollectionHieracy( sbQuery );
		}*/
	}
	
	public int insertLogBreachDataBatch(CallableStatement callableStmt) throws Throwable {
		int ins[], nInserted = 0;
		
		try {
			ins = callableStmt.executeBatch();
			//pstmt.
			nInserted = ins.length;
		} catch (Throwable t) {
			int i =0;
			SQLException sqlExcpNext = null;
			if( t instanceof SQLException ) {
				while( (sqlExcpNext = ((SQLException)t).getNextException()) != null) {
					LogManager.errorLog(sqlExcpNext.getMessage());
					sqlExcpNext.printStackTrace();
					//To break the exception print after 10 error
					i++;
					if( i > 10 ) {
						break;
					}
				}
			}
			
			throw t;
		}
		return nInserted;
	}
	
	public CallableStatement createLogTablePrepredstmt(Connection con) throws Throwable {
		CallableStatement callableStmt = null;
		StringBuilder sbQuery = new StringBuilder();
		try {
			sbQuery.append("{ call insert_log_entry(?, ?, ?, ?::json) }");
			callableStmt = con.prepareCall(sbQuery.toString());
		} catch (Throwable t) {
			LogManager.errorLog("Exception while preparing connection statement for LOG Batch : ");
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return callableStmt;
	}
	
	public int insertBatch(CallableStatement callableStmt) throws Throwable {
		int ins[], nInserted = 0;
		
		try {
			ins = callableStmt.executeBatch();
			nInserted = ins.length;
		} catch (Throwable t) {
			int i =0;
			SQLException sqlExcpNext = null;
			if( t instanceof SQLException ) {
				while( (sqlExcpNext = ((SQLException)t).getNextException()) != null) {
					LogManager.errorLog(sqlExcpNext.getMessage());
					sqlExcpNext.printStackTrace();

					//To break the exception print after 10 error
					i++;
					if( i > 10 ) {
						break;
					}
				}
			}
			
			throw t;
		}
		return nInserted;
	}
	
	public void updateBreachedUsers(Connection con, String userIds) throws Exception {
		Statement stmt = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append(" UPDATE user_pvt_counters ")
					.append(" SET log_breached = TRUE ")
					.append(" WHERE user_id IN ").append(userIds)
					.append(" AND NOT log_breached");
			
			stmt = con.createStatement();
			stmt.executeUpdate(sbQuery.toString());
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(stmt);
			stmt = null;
		}
	}
	
	/**
	 * Returns the UID of the Rum Module, for the given encrypted UID.
	 * 
	 * @param con
	 * @param strEncryptedUID
	 * @return
	 * @throws Exception
	 */
	public long getModuleUID(Connection con, String strGUID, String module_code) throws Exception {
		long lUID = -1l;
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"' and module_code like '"+module_code+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				lUID = rst.getLong("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
		}
		
		return lUID;
	}
	
}
