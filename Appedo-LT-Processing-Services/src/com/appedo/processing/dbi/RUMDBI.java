package com.appedo.processing.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.RUMDataBean;
import com.appedo.processing.bean.RUMSLABean;
import com.appedo.processing.utils.UtilsFactory;

/**
 * To insert the 
 * 
 * @author veeru
 *
 */
public class RUMDBI {
	
	public void insertRumTable(Connection con, RUMDataBean rumBean) throws Throwable {
		PreparedStatement pstmt = null, pstmtPartitionTables = null;
		StringBuilder sbQuery = new StringBuilder();
		String strPartitionKey = null;
		long lRUMId = 0;
		
		try {
			strPartitionKey = UtilsFactory.formatYYYYMMDD( rumBean.getDateQueuedOn() );
			
			pstmtPartitionTables = con.prepareStatement("SELECT create_rum_daily_partition_tables(?, ?)");
			
			// Send the date as parameter and get actual partition's suffix. Eg: UID_yyyymmdd
			strPartitionKey = CommonDBI.createRUMDailyPartition(pstmtPartitionTables, rumBean.getUID(), strPartitionKey);
			
			sbQuery	.append("INSERT INTO rum_collector_").append(strPartitionKey).append(" ")		// rum_collector_131_2015
					.append(" (os_version, browser_name, ip_address, nt_red_cnt, nt_nav_type, nt_nav_st, nt_red_st, ")
					.append("   nt_red_end, nt_fet_st, nt_dns_st, nt_dns_end, nt_con_st, nt_con_end, ")
					.append("   nt_req_st, nt_res_st, nt_res_end, nt_domloading, nt_domint, ")
					.append("   nt_domcontloaded_st, nt_domcontloaded_end, nt_domcomp, ")
					.append("   nt_load_st, nt_load_end, nt_unload_st, nt_unload_end, ")
					.append("   url, boomerang_ver, beacon_url, user_id, device_type, merchant_name, ")
					.append("   device_name, os, received_on, appedo_received_on, uid, url_without_query_string, query_string) ")
					.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, (SELECT user_id FROM module_master WHERE uid = ?), ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, rumBean.getOsVersion());
			pstmt.setString(2, rumBean.getBrowserName());
			pstmt.setString(3, rumBean.getIpAddress());
			pstmt.setLong(4, rumBean.getNt_red_cnt());
			pstmt.setLong(5, rumBean.getNt_nav_type());
			pstmt.setLong(6, rumBean.getNt_nav_st());
			pstmt.setLong(7, rumBean.getNt_red_st());
			pstmt.setLong(8, rumBean.getNt_red_end());
			pstmt.setLong(9, rumBean.getNt_fet_st());
			pstmt.setLong(10, rumBean.getNt_dns_st());
			pstmt.setLong(11, rumBean.getNt_dns_end());
			pstmt.setLong(12, rumBean.getNt_con_st());
			pstmt.setLong(13, rumBean.getNt_con_end());
			pstmt.setLong(14, rumBean.getNt_req_st());
			pstmt.setLong(15, rumBean.getNt_res_st());
			pstmt.setLong(16, rumBean.getNt_res_end());
			pstmt.setLong(17, rumBean.getNt_domloading());
			pstmt.setLong(18, rumBean.getNt_domint());
			pstmt.setLong(19, rumBean.getNt_domcontloaded_st());
			pstmt.setLong(20, rumBean.getNt_domcontloaded_end());
			
			pstmt.setLong(21, rumBean.getNt_domcomp());
			pstmt.setLong(22, rumBean.getNt_load_st());
			pstmt.setLong(23, rumBean.getNt_load_end());
			pstmt.setLong(24, rumBean.getNt_unload_st());
			pstmt.setLong(25, rumBean.getNt_unload_end());
			pstmt.setString(26,rumBean.getUrl());
			pstmt.setString(27, rumBean.getBoomerang_ver());
			pstmt.setString(28, rumBean.getBeacon_url()); // --user_id,device_type,merchant_name,device_name
			pstmt.setLong(29, rumBean.getUID());
			pstmt.setString(30, rumBean.getDeviceType());
			pstmt.setString(31, rumBean.getMerchantName());
			pstmt.setString(32, rumBean.getDevicename());
			pstmt.setString(33, rumBean.getOs());
			pstmt.setTimestamp(34, new Timestamp( rumBean.getDateQueuedOn() ) );
			pstmt.setTimestamp(35, new Timestamp( rumBean.getDateQueuedOn() ) );
			pstmt.setLong(36, rumBean.getUID());
			pstmt.setString(37, rumBean.getURLWithoutQueryString());
			pstmt.setString(38, rumBean.getQueryString());
			pstmt.executeUpdate();
			
			lRUMId = DataBaseManager.returnKey(pstmt);
			
			rumBean.setRUMId(lRUMId);
		} catch (Throwable t) {
			LogManager.errorLog("Exception while processing: "+rumBean.toString());
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(pstmtPartitionTables);
			pstmtPartitionTables = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	/**
	 * Returns the UID of the Rum Module, for the given encrypted UID.
	 * 
	 * @param con
	 * @param strEncryptedUID
	 * @return
	 * @throws Exception
	 */
	public long getModuleUID(Connection con, String strGUID) throws Exception {
		long lUID = -1l;
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"'";
			
			//System.out.println("ff"+strQuery);
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				lUID = rst.getLong("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
		}
		
		return lUID;
	}
	
	public boolean checkIpExist(Connection con, String ipAddress) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		boolean ipExist = false;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery.append("SELECT true FROM ip_location_master WHERE ipaddress = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, ipAddress);
			rst = pstmt.executeQuery();
			if(rst.next()){
				ipExist = true;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return ipExist;
	}
	
	public void insertIPLocationMaster(Connection con, JSONObject joLoc, String ipAddress) throws Exception {
		PreparedStatement stmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery = new StringBuilder();
			sbQuery	.append("insert into ip_location_master (ipAddress, country_code, country_name, region_name, city_name, latitude, longitude, zip_code, isp, domain, created_on) ")
					.append("values (?,?,?,?,?,?,?,?,?,?,now())");
			
			stmt = con.prepareStatement(sbQuery.toString());

			stmt.setString(1, ipAddress);
			stmt.setString(2, joLoc.getString("country_code"));
			stmt.setString(3, joLoc.getString("country_name"));
			stmt.setString(4, joLoc.getString("region_name"));
			stmt.setString(5, joLoc.getString("city_name"));
			stmt.setString(6, joLoc.getString("latitude"));
			stmt.setString(7, joLoc.getString("longitude"));
			stmt.setString(8, joLoc.getString("zip_code"));
			stmt.setString(9, joLoc.getString("isp"));
			stmt.setString(10, joLoc.getString("domain"));
			stmt.executeUpdate();
		
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally{
			DataBaseManager.close(stmt);
			stmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * gets RUM uid's SLA details, `null` if no SLA has configured or SLA `is_active` = false
	 * 
	 * @param con
	 * @param lUID
	 * @return
	 * @throws Exception
	 */
	public RUMSLABean getRUMSLADetails(Connection con, long lUID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		RUMSLABean rumslaBean = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append("SELECT ssr.* ")
					.append("FROM so_sla_rum ssr ")
					.append("INNER JOIN so_sla ss ON ss.sla_id = ssr.sla_id ")
					.append("  AND ss.is_active = true ")
					.append("  AND ssr.uid = ? ");
					
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUID);
			rst = pstmt.executeQuery();
			if ( rst.next() ) {
				rumslaBean = new RUMSLABean();
				rumslaBean.setUserId(rst.getLong("user_id"));
				rumslaBean.setSlaId(rst.getLong("sla_id"));
				rumslaBean.setUId(lUID);
				rumslaBean.setModuleName(rst.getString("module_name"));
				rumslaBean.setGUID(rst.getString("guid"));
				rumslaBean.setAboveThreshold(rst.getBoolean("is_above_threshold"));
				rumslaBean.setBreachTypeId(rst.getInt("breach_type_id"));
				rumslaBean.setWarningThresholdValue(rst.getInt("warning_threshold_value"));
				rumslaBean.setCriticalThresholdValue(rst.getInt("critical_threshold_value"));
				rumslaBean.setMinBreachCount(rst.getInt("min_breach_count"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return rumslaBean;
	}
}
