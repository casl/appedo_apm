package com.appedo.processing.timer;

import java.util.TimerTask;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class CIDrainingTimerTask extends TimerTask{
	private long tNo;
	
	public CIDrainingTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in CIDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		JSONObject joResponse = null;
		
		try {
			TaskExecutor executor = null;
			long lCIActualQueue = 0L;
			
			joResponse = DrainManager.getCIQueueDetails("getSize");
			
			if( joResponse.getBoolean("success") ) {
				lCIActualQueue = joResponse.getLong("message");
			}
//			LogManager.infoLog("Queue Size:: "+lCIActualQueue);
			
			// long connectionSize = Long.valueOf(Constants.PROCESSING_THREAD);
			long connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
//			LogManager.infoLog("DB Processing ConnectionSize:: "+DataBaseManager.getCurrentActiveCount());
//			LogManager.infoLog("DB required ConnectionSize:: "+connectionSize);
			
			long ltQueueSize = lCIActualQueue < connectionSize ? lCIActualQueue : connectionSize;
			
			if( ltQueueSize <= 0 ) {
				LogManager.errorLog("Unable to create New Threads. As required count is "+ltQueueSize);
			} else {
				for(int i=0; i<ltQueueSize; i++ ){
					executor = TaskExecutor.getExecutor(Constants.DEFAULT_CI_THREADPOOL_NAME);
					executor.submit(new CIProcessingThread());
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Closed Connection count in CIDrainingTimerTask: " + tNo);
		super.finalize();
	}
}
