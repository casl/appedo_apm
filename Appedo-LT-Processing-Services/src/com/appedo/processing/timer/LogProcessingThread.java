package com.appedo.processing.timer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.LOGDataBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.dbi.LOGDBI;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;
import com.appedo.processing.utils.UtilsFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class LogProcessingThread extends Thread{
	Connection con = null;
	private long tNo;
	JSONObject joResponse = null;
	
	public LogProcessingThread() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		//this.joResponse = joResponse;
		//LogManager.infoLog("Opened Thread in RUM TimerTask count: "+tNo);
	}
	
	@Override
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		LOGDBI logDBI = null;
		LOGDataBean logBean = null;
		JSONObject joLogData = null;
		Timestamp tslogstash = null;
		JSONArray jaLogDatas = null, jaProcLogData = null;
		CallableStatement callableStmt = null;
		int insertedCount = 0;
		Map<String, HashSet<String>> hmGuids = new HashMap<String, HashSet<String>>();
		try {
			
			jaLogDatas = new JSONArray();
			int remainingData = 0;
				
				//startTime = System.currentTimeMillis();
				//System.out.println("Start time "+ startTime);
				joResponse = DrainManager.getLOGQueueDetails("getData");
				//totalTimeTaken = System.currentTimeMillis() - startTime;
				//LogManager.infoLog("Totaltime :"+totalTimeTaken+" T_no :"+tNo );
				//System.out.println("get Queue Time :"+totalTimeTaken+" T_no :"+tNo );
				
				if( joResponse == null || ! joResponse.getBoolean("success") ) {
					throw new Exception("Problem with services ");
				}
						
				jaLogDatas = joResponse.getJSONArray("message");
				
				//joResponse.getJSONArray(key)
				// Insert LOG data
				logDBI = new LOGDBI();
				logBean = new LOGDataBean();
				
				//check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				callableStmt = logDBI.createLogTablePrepredstmt(con); 
				remainingData = jaLogDatas.size(); 
				jaProcLogData= new JSONArray();
				for (int count = 0; count < jaLogDatas.size(); count++) {
					
					// get uid for given guid
					logBean.setLogData(jaLogDatas.getJSONObject(count).getJSONObject("logData"));
					
					long lUID = logDBI.getModuleUID(con, logBean.getLogData().getString("guid"), "LOG");
					
					//totalTimeTaken = System.currentTimeMillis() - startTime;
					//System.out.println("get UID :"+totalTimeTaken+" T_no :"+tNo );
					if( lUID == -1l ) {
						LogManager.errorLog("Given GUID is not matching: " +logBean.getLogData().getString("guid"));
						continue;
					}
					
					joLogData  = logBean.getLogData();
					
					tslogstash = new Timestamp(UtilsFactory.toDate(joLogData.getString("@timestamp"), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").getTime());
					joLogData.put("logstash_timestamp", tslogstash.toString());
					joLogData.put("appedo_received_on", tslogstash.toString());
					
					Iterator keys = joLogData.keys();
					Set<String> hs = new HashSet<String>();
					
					while(keys.hasNext()){
						String key = (String) keys.next();
						if ( joLogData.get(key) instanceof JSONArray || joLogData.get(key) instanceof JSONObject) {
							//joLogData.remove(key);
							hs.add(key);
					    }
					}
					//totalTimeTaken = System.currentTimeMillis() - startTime;
					//	System.out.println("Set Keys :"+totalTimeTaken+" T_no :"+tNo );
					// remove keys which has object and array.
					Iterator<String> setIterator = hs.iterator();
					
					while(setIterator.hasNext()){
						String item = setIterator.next();
						joLogData.remove(item);
					}
					//totalTimeTaken = System.currentTimeMillis() - startTime;
					//System.out.println("Set Iterator:"+totalTimeTaken+" T_no :"+tNo );
					jaProcLogData.add(joLogData);
					//hmGuids.put(joLogData.getString("guid"), value)
					if (hmGuids.containsKey(joLogData.getString("guid"))) {
						
						HashSet<String> hsGrok = hmGuids.get(joLogData.getString("guid"));
						hsGrok.add(joLogData.getString("type"));
						hmGuids.put(joLogData.getString("guid"), hsGrok);
						
					} else {
						hmGuids.put(joLogData.getString("guid"), new HashSet<String>(Arrays.asList(joLogData.getString("type"))));
					}
					
					try {
						// Insert LOG data
						logDBI.addLOGDataToBatch(callableStmt, logBean);
						//totalTimeTaken = System.currentTimeMillis() - startTime;
						//System.out.println("DB Ins time :"+totalTimeTaken+" T_no :"+tNo );
					} catch(Exception e){
						//System.out.println(e.getMessage());
						if(e.getMessage().startsWith("ERROR: duplicate key value violates unique constraint")){
							Thread.sleep(1000 * 5);
							// aging insert LOG data if the above fails
							logDBI.addLOGDataToBatch(callableStmt, logBean);	
						}
					}
					//totalTimeTaken = System.currentTimeMillis() - startTime;
					//LogManager.infoLog("Totaltime :"+totalTimeTaken+" T_no :"+tNo );
					//System.out.println("Totaltime :"+totalTimeTaken+" T_no :"+tNo );
					
					if (count % 1000 == 0 && count >= 1000) {
						long dbStartTime = System.currentTimeMillis();
						insertedCount = logDBI.insertBatch(callableStmt);
						long dbTotalTime = System.currentTimeMillis() - dbStartTime;
						//System.out.println(" DB Totaltime :"+dbTotalTime+" T_no :"+tNo );
						LogManager.infoLog("logDBInsTime:"+dbTotalTime+"|size:1000");
						remainingData = remainingData - 1000;
					} 
				}
				
				//pstmt.executeBatch();
				if (remainingData > 0 ){
					long dbStartTime = System.currentTimeMillis();
					insertedCount = logDBI.insertBatch(callableStmt);
					long dbTotalTime = System.currentTimeMillis() - dbStartTime;
					LogManager.infoLog("logDBInsTime:"+dbTotalTime+"|size:"+remainingData);
				}
				
				initiateSLALogProcessing(hmGuids, jaProcLogData);
				
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(callableStmt);
			callableStmt = null;
			
			DataBaseManager.close(con);
			con = null;
			
			logDBI = null;
			LogManager.infoLog("Closed Thread in LOG TimerTask Count: "+tNo);
		}
	}
	
	public void initiateSLALogProcessing (Map<String, HashSet<String>> hmGuid, JSONArray jaProcessingData) throws Throwable {
		JSONObject joRespSLAData = null;
		TaskExecutor executor = null;
		
		if (jaProcessingData.size() > 0) {
			joRespSLAData = DrainManager.getSLALog("getLogSLA");
			
			executor = TaskExecutor.getExecutor(Constants.DEFAULT_LOG_SLA_THREADPOOL_NAME);
			executor.submit(new LogSlaProcessingThread(hmGuid, jaProcessingData, joRespSLAData));
		}
	}
			
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}