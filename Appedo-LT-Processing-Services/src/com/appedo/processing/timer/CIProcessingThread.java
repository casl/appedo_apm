package com.appedo.processing.timer;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.CIDataBean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.dbi.CIDBI;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.UtilsFactory;

public class CIProcessingThread extends Thread{
	Connection con = null;
	private long tNo;
	
	public CIProcessingThread() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Thread in CI TimerTask count: "+tNo);
	}
	
	@Override
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		CIDBI ciDBI = null;
		CIDataBean ciBean = null;
		JSONObject joResponse = null;
		int i = 0;
		
		try {
			while( i <= 10) {
				
				joResponse = DrainManager.getCIQueueDetails("getData");
				
				if( joResponse == null || ! joResponse.getBoolean("success") ) {
					Thread.sleep(500); // TODO re-confirm
					i++;
					
					continue;
				}
				
				i = 0;
				ciBean = (CIDataBean) JSONObject.toBean(joResponse.getJSONObject("message"), CIDataBean.class);
				
				
				// check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				
				// Insert CI data
				ciDBI = new CIDBI();
				
				// get uid for given guid
				long lUID = ciDBI.getModuleUID(con, ciBean.getGuid());
				
				if( lUID == -1l ) {				
					throw new Exception("Given GUID is not matching: "+ciBean.getGuid());				
				}
				ciBean.setUID(lUID);
				
				
				// Insert CI data
				// Check ci_events_.. for entry
				long eventId = ciDBI.isExistEvents(con, lUID, ciBean.getEventMethod(), ciBean.getEventName(), ciBean.getAgentType(), ciBean.getEnv());
				
				// Insert into CIEvents
				if( eventId == 0 ){
					eventId = ciDBI.insertCIEvents(con, lUID, ciBean.getEventName(), ciBean.getEventMethod(), ciBean.getAgentType(), ciBean.getEnv());
					
					// Create partition table for Trans
					ciDBI.createCITransTablePartitions(con, lUID, eventId);
				}
				
				// Insert ci_evt_prop_template
				//ciDBI.insertEventProperty(con, lUID, eventId, ciBean);
				//Iterate property name
				for (Map.Entry<String, String> ciColumns: ciBean.getCiColumns().entrySet()){
					// Check for Existence
					if( !ciDBI.isValueExist(con, lUID, eventId, ciColumns.getKey()) ){
						// Insert ci_evt_prop_template
						ciDBI.insertEventProperty(con, lUID, eventId, ciColumns.getKey());
						// Update column_name as col_....
						ciDBI.updateTemplate(con, lUID, eventId, ciColumns.getKey());
					}
				}
				
				// Get inserted Values in Hashmap
				HashMap<String, String> hmap = ciDBI.getEventProperty(con, lUID, eventId);
				
				// Insert for Trans table
				ciDBI.insertCITable(con, eventId, hmap, ciBean);
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			ciDBI = null;
			
			LogManager.infoLog("Closed Thread in CI TimerTask Count: "+tNo);
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
		
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}