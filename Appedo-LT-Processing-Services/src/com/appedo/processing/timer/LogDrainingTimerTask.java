package com.appedo.processing.timer;

import java.util.TimerTask;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class LogDrainingTimerTask extends TimerTask{
	private static int threadCount;
	
	public LogDrainingTimerTask()throws Throwable{
		//tNo = Constants.getLongCount();
		//LogManager.infoLog("Opened Connection count in LOGDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		JSONObject joResponse = null, joRespData = null, joRespSLAData = null;
		
		threadCount++;
		
		try {
			TaskExecutor executor = null;
			//long totalTimeTaken;
			//long startTime;
			long connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
			
			joResponse = DrainManager.getLOGQueueDetails("getSize");
			
			//System.out.println("getSizeTime :"+totalTimeTaken+" queue size :"+joResponse.getLong("message"));
			
			//Below conditioned checked to print the queue size every 10s
			if (threadCount * Constants.TIME_INTERVAL_MS  >= Constants.LOG_GETSIZE_INTERVAL_MS ) {
				if (joResponse != null && joResponse.getBoolean("success")){
					LogManager.infoLog("logQueueSize:"+joResponse.getLong("message"));
					threadCount = 0;
				}
			}
			
			if (joResponse == null || ! joResponse.getBoolean("success")){
				LogManager.errorLog("getlogQueueSize: problem with services: "+joResponse);
				
			} else if( (joResponse != null && joResponse.getBoolean("success") && joResponse.getLong("message") > 0 ) && connectionSize > 0 ) {
				executor = TaskExecutor.getExecutor(Constants.DEFAULT_LOG_THREADPOOL_NAME);
				executor.submit(new LogProcessingThread());
				
				/*joRespSLAData = DrainManager.getSLALog("getLogSLA");
				
				executor = TaskExecutor.getExecutor(Constants.DEFAULT_LOG_SLA_THREADPOOL_NAME);
				executor.submit(new LogSlaProcessingThread(joRespData, joRespSLAData));*/
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Closed Connection count in LOGDrainingTimerTask ");
		super.finalize();
	}
}
