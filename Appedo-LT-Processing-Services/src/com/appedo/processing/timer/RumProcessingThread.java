package com.appedo.processing.timer;

import java.sql.Connection;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.bean.RUMDataBean;
import com.appedo.processing.bean.RUMSLABean;
import com.appedo.processing.common.Constants;
import com.appedo.processing.common.Constants.SLA_BREACH_SEVERITY;
import com.appedo.processing.dbi.RUMDBI;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.UtilsFactory;
import com.appedo.processing.utils.WebServiceManager;

public class RumProcessingThread extends Thread {
	Connection con = null;
	private long tNo;
	
	public RumProcessingThread() throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Thread in RUM TimerTask count: "+tNo);
	}
	
	@Override
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		RUMDBI rumDBI = null;
		RUMDataBean rumBean = null;
		JSONObject joResponse = null;
		String strIPAddress = null;
		int i = 0;
		
		try {
			while( i <= 10) {
				
				joResponse = DrainManager.getRUMQueueDetails("getData");
				
				if( joResponse == null || ! joResponse.getBoolean("success") ) {
					Thread.sleep(500); // TODO re-confirm
					i++;
					
					continue;
				}
				// else
				
				i = 0;
				rumBean = (RUMDataBean) JSONObject.toBean(joResponse.getJSONObject("message"), RUMDataBean.class);
				
				
				// check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				
				// Insert RUM data
				rumDBI = new RUMDBI();
				
				// get uid for given guid
				long lUID = rumDBI.getModuleUID(con, rumBean.getGuid());
				
				if( lUID == -1l ) {
					throw new Exception("Given GUID is not matching: "+rumBean.getGuid());				
				}
				rumBean.setUID(lUID);
				
				// Insert RUM data
				rumDBI.insertRumTable(con, rumBean);
				
				
				// Process the IP-location details
				JSONObject joLoc = null;
				strIPAddress = rumBean.getIpAddress();
				
				// check for IP-Address existence
				boolean isIpExist = rumDBI.checkIpExist(con, strIPAddress);
				if ( ! isIpExist ) {
					joLoc = getLocationDetails(strIPAddress);
				}
				
				if( joLoc != null ){
					rumDBI.insertIPLocationMaster(con, joLoc, strIPAddress);
				}
				
				
				// if SLA has configured, checks breach limits, if breaches send to `Appedo-SLA-Collector`
				verifySLABreach(rumBean);
				
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			rumDBI = null;
			
			LogManager.infoLog("Closed Thread in RUM TimerTask Count: "+tNo);
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	public static JSONObject getLocationDetails(String ipAddress) throws Throwable {
		JSONObject joResponse = null;
		
		WebServiceManager wsm = null;
		
		try{
			wsm = new WebServiceManager();
			wsm.sendRequest( Constants.RUM_LOCATION.replace("<IPADDRESS>", ipAddress) );
			joResponse = wsm.getJSONObjectResponse();
			
			if( joResponse == null ) {
				throw new Exception(wsm.getResponse());
			}
		} catch(Throwable e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return joResponse;
	}
	
	/**
	 * checks RUM breach for the uid and send to `Appedo-SLA-Collector`,
	 *   if SLA has configured for the guid, if response pageloadtime breached, send breached details to `Appedo-SLA-Collector`
	 * 
	 * @param rumBean
	 * @return
	 * @throws Exception
	 */
	public boolean verifySLABreach(RUMDataBean rumBean) throws Exception {
		RUMDBI rumDBI = null;
		RUMSLABean rumslaBean = null;
		
		JSONObject joRtnRUMSLABreach = null;
		
		boolean bBreached = false;
		
		try {
			rumDBI = new RUMDBI();
			
			// gets RUM uid's configured SLA details, `null` if no SLA has configured
			rumslaBean = rumDBI.getRUMSLADetails(con, rumBean.getUID());
			if ( rumslaBean != null ) {	// has SLA configured
				// checks with threshold limits & gets breached data, `null` if not breached
				joRtnRUMSLABreach = getBreachedData(rumBean, rumslaBean);
				if ( joRtnRUMSLABreach != null ) {	// has SLA breached
					bBreached = true;
					
					// send breached details to `Appedo-SLA-Collector`, 
					sendBreachedDataToSLACollector(joRtnRUMSLABreach, rumslaBean.getGUID());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			rumslaBean = null;
			
			UtilsFactory.clearCollectionHieracy(joRtnRUMSLABreach);
			joRtnRUMSLABreach = null;
		}
		
		return bBreached;
	}
	
	/**
	 * checks RUM collected data with configured threshold limits, 
	 *   if breached gets breached details, `null` if not breached
	 * 
	 * @param RUMDataBean
	 * @param rumslaBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getBreachedData(RUMDataBean RUMDataBean, RUMSLABean rumslaBean) throws Exception {
		JSONObject joRUMSLABreach = null;
		
		SLA_BREACH_SEVERITY sla_breach_severity = null;
		
		long lPageLoadTime = -1L;
		
		boolean bHasBreached = false;
		
		try {
			// TODO: RUM pageLoadTime formula, should be in single place
			lPageLoadTime = RUMDataBean.getNt_domcomp() - RUMDataBean.getNt_nav_st();
			
			// check with threshold limit
			if( rumslaBean.isAboveThreshold() ) {
				if ( lPageLoadTime > rumslaBean.getCriticalThresholdValue() ) {	// Critical
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.CRITICAL;
				} else if(lPageLoadTime > rumslaBean.getWarningThresholdValue() ) {	// Warning
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.WARNING; 
				}
			} else if( ! rumslaBean.isAboveThreshold() ) {
				if( lPageLoadTime < rumslaBean.getCriticalThresholdValue() ) {	// Critical
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.CRITICAL;
				} else if( lPageLoadTime < rumslaBean.getWarningThresholdValue() ) {	// Warning
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.WARNING;
				}
			}
			
			if ( bHasBreached ) {
				joRUMSLABreach = new JSONObject();
				joRUMSLABreach.put("slaId", rumslaBean.getSlaId());
				joRUMSLABreach.put("userId", rumslaBean.getUserId());
				
				joRUMSLABreach.put("uid", rumslaBean.getUId());
				joRUMSLABreach.put("guid", rumslaBean.getGUID());
				joRUMSLABreach.put("rum_id", RUMDataBean.getRUMId());
				
				joRUMSLABreach.put("is_above_threshold", rumslaBean.isAboveThreshold());
				joRUMSLABreach.put("warning_threshold_value", rumslaBean.getWarningThresholdValue());
				joRUMSLABreach.put("critical_threshold_value", rumslaBean.getCriticalThresholdValue());
				joRUMSLABreach.put("received_value", lPageLoadTime);
				joRUMSLABreach.put("appedo_received_on", RUMDataBean.getDateQueuedOn());
				joRUMSLABreach.put("breached_severity", sla_breach_severity.toString());
				joRUMSLABreach.put("min_breach_count", rumslaBean.getMinBreachCount());
				
				// RUM collector
				joRUMSLABreach.put("url", RUMDataBean.getUrl());
			}
		} catch (Exception e) {
			throw e;
		}
		
		return joRUMSLABreach;
	}
	
	/**
	 * Send the breached details to SLA collector
	 * 
	 * @param joRUMSLABreach
	 * @param strGUID
	 * @throws Exception
	 */
	public void sendBreachedDataToSLACollector(JSONObject joRUMSLABreach, String strGUID) throws Exception {
		WebServiceManager wsm = null;
		
		JSONObject joResp = null;
		
		try {
			wsm = new WebServiceManager();
			wsm.addParameter("command", "collectRUMUIDBreach");
			wsm.addParameter("breach_data", joRUMSLABreach.toString());
			wsm.addParameter("guid", strGUID);
			wsm.sendRequest(Constants.APPEDO_SLA_COLLECTOR_URL);
			
			if( wsm.getJSONObjectResponse() != null ) {
				joResp = wsm.getJSONObjectResponse();
			} else {
				throw new Exception("Problem with SLA Collector: "+wsm.getResponse());
			}
			
			// web service response
			if( joResp.getBoolean("success") ) {
				// success
				LogManager.infoLog("SLA policy added.");
			} else {
				// exception in SLA
				throw new Exception("Unable to queue RUM SLA breached details.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if ( wsm != null ) {
				wsm.destory();
			}
			wsm = null;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}