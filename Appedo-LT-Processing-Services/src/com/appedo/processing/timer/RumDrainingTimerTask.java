package com.appedo.processing.timer;

import java.util.TimerTask;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.manager.DrainManager;
import com.appedo.processing.utils.TaskExecutor;

public class RumDrainingTimerTask extends TimerTask{
	private long tNo;
	
	public RumDrainingTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in RUMDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		JSONObject joResponse = null;
		
		try {
			TaskExecutor executor = null;
			long lRUMActualQueue = 0L;
			
			joResponse = DrainManager.getRUMQueueDetails("getSize");
			
			if( joResponse.getBoolean("success") ) {
				lRUMActualQueue = joResponse.getLong("message");
			}
//			LogManager.infoLog("Queue Size:: "+lRUMActualQueue);
			
			// long connectionSize = Long.valueOf(Constants.PROCESSING_THREAD);
			long connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
//			LogManager.infoLog("DB Processing ConnectionSize:: "+DataBaseManager.getCurrentActiveCount());
//			LogManager.infoLog("DB required ConnectionSize:: "+connectionSize);
			
			long ltQueueSize = lRUMActualQueue < connectionSize ? lRUMActualQueue : connectionSize;
			
			if( ltQueueSize <= 0 ) {
				LogManager.errorLog("Unable to create New Threads. As required count is "+ltQueueSize);
			} else {
				for(int i=0; i<ltQueueSize; i++ ){
					executor = TaskExecutor.getExecutor(Constants.DEFAULT_RUM_THREADPOOL_NAME);
					executor.submit(new RumProcessingThread());
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Closed Connection count in RUMDrainingTimerTask: " + tNo);
		super.finalize();
	}
}
