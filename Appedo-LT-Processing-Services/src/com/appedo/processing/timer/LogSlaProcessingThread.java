package com.appedo.processing.timer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.processing.common.Constants;
import com.appedo.processing.dbi.LOGDBI;
import com.appedo.processing.utils.UtilsFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class LogSlaProcessingThread extends Thread{

	Connection con = null;
	private long tNo;
	JSONObject joSLALogData = null;
	JSONArray jaLogData = null;
	Map<String, HashSet<String>> hmGuids = new HashMap<String, HashSet<String>>();
	static int addedBatchCount = 0;
	HashSet<Long> hsUserIds = new HashSet<Long>();
	
	public LogSlaProcessingThread(Map<String, HashSet<String>> hmGuid, JSONArray jaLogData, JSONObject joSLALogData) throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		this.jaLogData = jaLogData;
		this.joSLALogData = joSLALogData;
		this.hmGuids = hmGuid;
		
		//LogManager.infoLog("Opened Thread in RUM TimerTask count: "+tNo);
	}
	
	@Override
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		LOGDBI logDBI = null;
		JSONArray jaSlaData = new JSONArray();
		Map<String, ArrayList<JSONObject>> hmSLADetails = new HashMap<String, ArrayList<JSONObject>>();
		//LOGDataBean logBean = null;
		JSONObject joLogDatum = null, joSlaLogDatum = null;
		// jaLogData = null, 
		
		CallableStatement callableStmt = null;
		
		boolean isSlaExist=false;
		try {
			
			//jaLogData = new JSONArray();
			int remainingData = 0;
			
			if (joSLALogData == null || ! joSLALogData.getBoolean("success")) {
				throw new Exception("Problem with services while getting SLA values");
			} else if (joSLALogData.getJSONObject("message").size() > 0) {
				isSlaExist=true;
				hmSLADetails = (Map<String, ArrayList<JSONObject>>) joSLALogData.get("message");
			} 
			
			hmSLADetails.keySet().retainAll(hmGuids.keySet());
			
			if (isSlaExist && hmSLADetails.size() > 0) {
				//jaLogData = jaLogData.getJSONArray("message");
				//System.out.println("Following Breached GUID & details available..");
				//System.out.println(hmSLADetails.toString());
				logDBI = new LOGDBI();
				//logBean = new LOGDataBean();
				//check for DB connection
				this.con = DataBaseManager.reEstablishConnection(this.con);
				callableStmt = logDBI.createLogBreachCallablestmt(con); 
				
				for (int count = 0; count < jaLogData.size(); count++) {
					joLogDatum = jaLogData.getJSONObject(count);
					
					String guid = joLogDatum.getString("guid");
					//sSystem.out.println("processing guid : "+ guid);
					if (hmSLADetails.get(guid) != null) {
						jaSlaData = JSONArray.fromObject(hmSLADetails.get(guid));
						//System.out.println("jaSlaData -> "+jaSlaData);
						for (int slaCount = 0; slaCount < jaSlaData.size(); slaCount++) {
							joSlaLogDatum = jaSlaData.getJSONObject(slaCount);
							String grokName = joLogDatum.getString("type");
							//System.out.println("joSlaLogDatum -> "+joSlaLogDatum.toString());
							if (grokName.equalsIgnoreCase(joSlaLogDatum.getString("log_grok_name"))) {
								if (joSlaLogDatum.containsKey("breach_pattern") && joSlaLogDatum.containsKey("grok_column")) {
									String grokCol = joSlaLogDatum.getString("grok_column");
									String breachPattern = joSlaLogDatum.getString("breach_pattern");
									
									if (joLogDatum.containsKey(grokCol) && joLogDatum.getString(grokCol).contains(breachPattern)) {
										//add breach
										joSlaLogDatum.put("received_message", joLogDatum.getString(grokCol));
										joSlaLogDatum.put("received_pattern", breachPattern);
										joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
										logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
										System.out.println("Added: "+joSlaLogDatum.toString());
										hsUserIds.add(joSlaLogDatum.getLong("user_id"));
										addedBatchCount++;
									} else {
										LogManager.errorLog("Breached column not found, hence skipping.");
										continue;
									}
									
								} else if (joSlaLogDatum.containsKey("breach_pattern")) {
									String breachPattern = joSlaLogDatum.getString("breach_pattern");
									
									if (joLogDatum.getString("message").contains(breachPattern)) {
										//add breach
										joSlaLogDatum.put("received_message", joLogDatum.getString("message").replace("\n", " "));
										joSlaLogDatum.put("received_pattern", breachPattern);
										joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
										logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
										System.out.println("Added: "+joSlaLogDatum.toString());
										hsUserIds.add(joSlaLogDatum.getLong("user_id"));
										addedBatchCount++;
										
									}
									
								} else {
									if (joSlaLogDatum.containsKey("grok_column")) {
										String grokCol = joSlaLogDatum.getString("grok_column");
										if (!joLogDatum.containsKey(grokCol)) {
											LogManager.errorLog("Breached column not found, hence skipping.");
											continue;
										} else if (!NumberUtils.isNumber(joLogDatum.getString(grokCol))) {
											LogManager.errorLog("Breached column is non-numeric value, hence skipping.");
											continue;
										}
										long grokColValue = joLogDatum.getLong(grokCol); 
										joSlaLogDatum.put("received_value", grokColValue);
										if (joSlaLogDatum.getBoolean("is_above_threshold")) {
											
											if (grokColValue >= joSlaLogDatum.getLong("critical_threshold_value")) {
												//add critical breach
												joSlaLogDatum.put("breached_severity", "critical");
												joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
												logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
												hsUserIds.add(joSlaLogDatum.getLong("user_id"));
												addedBatchCount++;
											} else if (grokColValue >= joSlaLogDatum.getLong("warning_threshold_value")) {
												// add warning breach
												joSlaLogDatum.put("breached_severity", "warning");
												joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
												logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
												System.out.println("Added: "+joSlaLogDatum.toString());
												hsUserIds.add(joSlaLogDatum.getLong("user_id"));
												addedBatchCount++;
											}
											
										} else {
											if (grokColValue <= joSlaLogDatum.getLong("critical_threshold_value")) {
												//add critical breach
												joSlaLogDatum.put("breached_severity", "critical");
												joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
												logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
												System.out.println("Added: "+joSlaLogDatum.toString());
												hsUserIds.add(joSlaLogDatum.getLong("user_id"));
												addedBatchCount++;
											} else if (grokColValue <= joSlaLogDatum.getLong("warning_threshold_value")) {
												// add warning breach
												joSlaLogDatum.put("breached_severity", "warning");
												joSlaLogDatum.put("appedo_received_on", joLogDatum.getString("appedo_received_on"));
												logDBI.addLogBreachToBatch(callableStmt, joSlaLogDatum);
												System.out.println("Added: "+joSlaLogDatum.toString());
												hsUserIds.add(joSlaLogDatum.getLong("user_id"));
												addedBatchCount++;
											}
										}
									}
								}
							}
						}
					}
					
				}
				if (addedBatchCount > 0) {
					logDBI.insertLogBreachDataBatch(callableStmt);
					
					if (hsUserIds.size() > 0) {
						long dbStartTime = System.currentTimeMillis();  
						logDBI.updateBreachedUsers(con, hsUserIds.toString().replace("[","(").replace("]", ")"));
						long dbTotalTime = System.currentTimeMillis() - dbStartTime;
						if (dbTotalTime > 1000) {
							Thread.sleep(500);
						}
					}
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(callableStmt);
			callableStmt = null;
			
			DataBaseManager.close(con);
			con = null;
			
			UtilsFactory.clearCollectionHieracy(jaSlaData);
			jaSlaData = null;
			
			UtilsFactory.clearCollectionHieracy(hmSLADetails);
			hmSLADetails = null;
			
			UtilsFactory.clearCollectionHieracy(joLogDatum);
			joLogDatum = null; 
			
			UtilsFactory.clearCollectionHieracy(joSlaLogDatum);
			joSlaLogDatum = null;
			
			logDBI = null;
			LogManager.infoLog("Closed LogSlaProcessingThread, TimerTask Count: "+tNo);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
	
	/*public static void main(String args[]) {
	
		JSONObject joObj = new JSONObject();
		joObj.put("message", "asdfasdfasfasdfas\n asdfasdfasfasdfqwerasdfasdfasdfasdf\nasqwerasdfqwefasdfasdfasdf\nwerfasgqwerfasdfasdf");
		System.out.println(joObj.getString("message"));
		System.out.println(joObj.getString("message").replace("\n", ""));
	}*/
	
}


