 package com.appedo.processing.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.appedo.manager.LogManager;

public class WebServiceManager {

	HashMap<String, String> hmMultipart = new HashMap<String, String>(); 
	HashMap<String, String> hmStringpart = new HashMap<String, String>(); 
	
	private Integer nStatusCode = null;
	private String strResponse = null;
	
	private JSONObject joResponse = null;
	private JSONArray jaResponse = null;
	
	/**
	 * Send a HTTP request
	 * 
	 * @param URL
	 */
	public void sendRequest(String URL) {
		Iterator<String> iterStringName = null;
		String strStringContent = null;
		
		HttpClient client = new HttpClient();
		PostMethod method = null;
		
		try{
			// Get the response from WebService
			method = new PostMethod(URL);
			method.setRequestHeader("Connection", "close");
			
			iterStringName = hmStringpart.keySet().iterator();
			while(iterStringName.hasNext()){
				strStringContent = iterStringName.next();
				method.setParameter(strStringContent, hmStringpart.get(strStringContent));
			}
			
			nStatusCode = client.executeMethod(method);
			if (nStatusCode != HttpStatus.SC_OK) {
				System.err.println("Excetion in URL call: "+URL);
				System.err.println("statusCode: "+nStatusCode);
				System.err.println("Method failed: " + method.getStatusLine());
			} else {
				
				strResponse = method.getResponseBodyAsString().trim();
				//System.out.println("strResponseJSONStream: "+strResponse);
				
				if( strResponse.length() > 0 ) {
					if( strResponse.startsWith("{") && strResponse.endsWith("}") ) {
						joResponse = JSONObject.fromObject( strResponse );
					} else if( strResponse.startsWith("[") && strResponse.endsWith("]") ) {
						jaResponse = JSONArray.fromObject( strResponse );
					}
				}
			}
		} catch (HttpException he) {
			LogManager.errorLog(he);
		} catch (IOException ie) {
			LogManager.errorLog(ie);
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			method.releaseConnection();
			method = null;
			
			client.getHttpConnectionManager().closeIdleConnections(0);
			client = null;
		}
	}
	
	/**
	 * Add a parameter, which can be sent through sendRequest()
	 * 
	 * @param key
	 * @param value
	 */
	public void addParameter(String key,String value) {
		try{
			hmStringpart.put(key, value);
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	/**
	 * Convert the InputStream into Bytes.
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	protected static byte[] getBytes(InputStream is) throws IOException {

		int len;
		int size = 1024;
		byte[] buf;

		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}
	
	public Integer getStatusCode() {
		return nStatusCode;
	}
	
	public String getResponse() {
		return strResponse;
	}
	
	public JSONObject getJSONObjectResponse() {
		return joResponse;
	}
	
	public JSONArray getJSONArrayResponse() {
		return jaResponse;
	}
	
	public void destory() {
		strResponse = null;
		nStatusCode = null;
	}
	
	@Override
	protected void finalize() throws Throwable {
		destory();
		super.finalize();
	}
}
