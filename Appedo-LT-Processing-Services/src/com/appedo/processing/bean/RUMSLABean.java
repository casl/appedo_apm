package com.appedo.processing.bean;

public class RUMSLABean {
	
	private long lUserId;
	private long lSlaId;
	private long lUId;
	private String strModuleName;
	private String strGUID;
	private boolean bAboveThreshold = true;
	private boolean bEnableResponseAlert;
	private int nBreachTypeId;
	private int nCriticalThresholdValue;
	private int nWarningThresholdValue;
	private int nMinBreachCount;
	
	
	public RUMSLABean() {
		// TODO Auto-generated constructor stub
	}
	
	public long getUserId() {
		return lUserId;
	}
	public void setUserId(long lUserId) {
		this.lUserId = lUserId;
	}
	
	public long getSlaId() {
		return lSlaId;
	}
	public void setSlaId(long lSlaId) {
		this.lSlaId = lSlaId;
	}
	
	public long getUId() {
		return lUId;
	}
	public void setUId(long lUId) {
		this.lUId = lUId;
	}
	
	public String getModuleName() {
		return strModuleName;
	}
	public void setModuleName(String strModuleName) {
		this.strModuleName = strModuleName;
	}
	
	public String getGUID() {
		return strGUID;
	}
	public void setGUID(String strGUID) {
		this.strGUID = strGUID;
	}
	
	public boolean isAboveThreshold() {
		return bAboveThreshold;
	}
	public void setAboveThreshold(boolean bAboveThreshold) {
		this.bAboveThreshold = bAboveThreshold;
	}
	
	public boolean isEnableResponseAlert() {
		return bEnableResponseAlert;
	}
	public void setEnableResponseAlert(boolean bEnableResponseAlert) {
		this.bEnableResponseAlert = bEnableResponseAlert;
	}
	
	public int getCriticalThresholdValue() {
		return nCriticalThresholdValue;
	}
	public void setCriticalThresholdValue(int nCriticalThresholdValue) {
		this.nCriticalThresholdValue = nCriticalThresholdValue;
	}
	
	public int getWarningThresholdValue() {
		return nWarningThresholdValue;
	}
	public void setWarningThresholdValue(int nWarningThresholdValue) {
		this.nWarningThresholdValue = nWarningThresholdValue;
	}
	
	public int getMinBreachCount() {
		return nMinBreachCount;
	}
	public void setMinBreachCount(int nMinBreachCount) {
		this.nMinBreachCount = nMinBreachCount;
	}
	
	public int getBreachTypeId() {
		return nBreachTypeId;
	}
	public void setBreachTypeId(int nBreachTypeId) {
		this.nBreachTypeId = nBreachTypeId;
	}
	/*
	public JSONObject toJSON() {
		JSONObject joRUMSLA = new JSONObject();
		joRUMSLA.put("userId", lUserId);
		joRUMSLA.put("slaId", lSlaId);
		joRUMSLA.put("UId", lUId);
		joRUMSLA.put("moduleName", strModuleName);
		joRUMSLA.put("guid", strGUID);
		joRUMSLA.put("aboveThreshold", bAboveThreshold);
		joRUMSLA.put("enableResponseAlert", bEnableResponseAlert);
		joRUMSLA.put("breachTypeId", nBreachTypeId);
		joRUMSLA.put("criticalThresholdValue", nCriticalThresholdValue);
		joRUMSLA.put("warningThresholdValue", nWarningThresholdValue);
		joRUMSLA.put("minBreachCount", nMinBreachCount);
		
		return joRUMSLA;
	}*/
}
