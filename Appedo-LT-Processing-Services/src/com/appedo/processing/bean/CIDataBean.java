package com.appedo.processing.bean;

import java.util.HashMap;

public class CIDataBean implements Comparable<CIDataBean>, Cloneable {
	
	private String emailId;
	private String firstName;
	private String lastName;
	private String age;
	private String browserName;
	private String browserVersion;
	private String devicename;
	private String deviceType;
	private String guid;
	private Long lUID;
	private String mobileNo;
	
	private String os;
	private String osVersion;
	private String ipAdress;
	private String uri;
	private String base_url;
	private String merchantName;
	private String eventName;
	private long evtPropId;
	private String eventMethod;
	private String referrer_url;
	private String propertyName;
	private String columnName;
	
	private HashMap<String, String> ciColumns;
	private Long eventStartTime;
	private Long eventEndTime;
	private long eventDuration;
	private String env;
	private String agentType;
	
	private Long dateQueuedOn = null;
	
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	public String getBase_url() {
		return base_url;
	}
	public void setBase_url(String base_url) {
		this.base_url = base_url;
	}
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public String getIpAdress() {
		return ipAdress;
	}
	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	
	public String getBrowserName() {
		return browserName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	
	public String getDevicename() {
		return devicename;
	}
	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}
	
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	public Long getUID() {
		return lUID;
	}
	public void setUID(Long lUID) {
		this.lUID = lUID;
	}
	
	public Long getQueuedOn() {
		return dateQueuedOn;
	}
	public void setQueuedOn(Long dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public long getEvtPropId() {
		return evtPropId;
	}
	public void setEvtPropId(long evtPropId) {
		this.evtPropId = evtPropId;
	}
	
	public String getReferrer_url() {
		return referrer_url;
	}
	public void setReferrer_url(String referrer_url) {
		this.referrer_url = referrer_url;
	}
	
	public HashMap<String, String> getCiColumns() {
		return ciColumns;
	}
	public void setCiColumns(HashMap<String, String> ciColumns) {
		this.ciColumns = ciColumns;
	}
	
	public String getEventMethod() {
		return eventMethod;
	}
	public void setEventMethod(String eventMethod) {
		this.eventMethod = eventMethod;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getBrowserVersion() {
		return browserVersion;
	}
	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}
	
	public Long getEventStartTime() {
		return eventStartTime;
	}
	public void setEventStartTime(Long eventStartTime) {
		this.eventStartTime = eventStartTime;
	}
	
	public Long getEventEndTime() {
		return eventEndTime;
	}
	public void setEventEndTime(Long eventEndTime) {
		this.eventEndTime = eventEndTime;
	}
	
	public long getEventDuration() {
		return eventDuration;
	}
	public void setEventDuration(long eventDuration) {
		this.eventDuration = eventDuration;
	}
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	
	public String getAgentType() {
		return agentType;
	}
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
	
	@Override
	public int compareTo(CIDataBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (dateQueuedOn - another.getQueuedOn()));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
