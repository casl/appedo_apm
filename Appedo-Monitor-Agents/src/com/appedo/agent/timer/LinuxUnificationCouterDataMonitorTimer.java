package com.appedo.agent.timer;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.manager.LinuxMonitorManager;
import com.appedo.agent.utils.Constants;

public class LinuxUnificationCouterDataMonitorTimer extends TimerTask {
	AgentManager am = null;
	
	public LinuxUnificationCouterDataMonitorTimer() {
		am = new AgentManager();
	}
	
	public void run() {
		boolean bReturn = false;
		FileWriter fileWriter;
		try {

			if(!Constants.FILE_WRITING_PROCESS) {
				System.out.println("Getting couterData from Queue.");
				ArrayList<String> alCouterData = LinuxMonitorManager.getLinuxMonitorManager().drianCounterBreachData();
				if(!Constants.FILE_WRITE_MODE) {
					for(String CouterData : alCouterData) {
						bReturn = am.sendCounterValueToCollector(CouterData);
						if(!bReturn) {
							System.out.println("Unable to send the Counter Data to Collector,So Data will write into the couter_data.log file.");
							Constants.FILE_WRITE_MODE = true;
							
							Timer timerLinuxUnificationCounterData = new Timer();
							TimerTask timerTaskLinuxQueue = new LinuxUnificationSendCouterDataTimer(timerLinuxUnificationCounterData);
							timerLinuxUnificationCounterData.schedule(timerTaskLinuxQueue, 300000, 300000);
						
							break;
						}
					}
				}
				
				if(Constants.FILE_WRITE_MODE) {
					System.out.println("File Write mode is ON...");
					
					File file = new File("/root/linux_unified_agent/logs/counter_data.log");
					if(file.createNewFile()) {
						fileWriter = new FileWriter(file);
					}else {
						fileWriter = new FileWriter(file, true);
					}
					
					for(String CouterData : alCouterData) {
						fileWriter.write(CouterData+System.getProperty("line.separator"));
					}
					fileWriter.close();
				}
			}else {
				System.out.println("Waiting Mode is ON.......");
			}
			
		}catch (Throwable e) {
			System.out.println("Exception in LinuxUnificationCouterDataMonitorTimer : "+e);
		}
	}
	
}
