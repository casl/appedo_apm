package com.appedo.agent.timer;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.manager.ApacheMonitorManager;
import com.appedo.agent.manager.LogManagerExtended;
import com.appedo.agent.utils.Constants;
import com.appedo.agent.utils.UtilsFactory;
import com.appedo.agent.utils.Constants.AGENT_TYPE;
import net.sf.json.JSONObject;

public class LinuxUnificationApacheMonitorTimer extends TimerTask{

	private Timer timerLinuxUnificationApacheObj = null;
	private Date collectionData = null;
	AgentManager am = null;
	String moduleStatus = "", moduleGUID = "";
	
	public LinuxUnificationApacheMonitorTimer(Timer timerObject, String agentGUID) {
		this.timerLinuxUnificationApacheObj = timerObject;
		this.moduleGUID = agentGUID;
		am = new AgentManager();
	}

	public void run() {
		try {
			 
			this.collectionData = new Date();
			
			if( Constants.APACHE_CONFIG!=null ) {
				JSONObject joRecord = JSONObject.fromObject(Constants.APACHE_CONFIG);
				String strHostName = joRecord.getString("host");
				String strAppPort = joRecord.getString("app_port");
				String strMonitorURL = joRecord.getString("monitor_url");
			
				this.moduleStatus = am.getModuleRunningStatus(moduleGUID, "appInformation");
				
				UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Status of Agent("+this.moduleGUID+") "+this.moduleStatus);
				
				if(moduleStatus.equalsIgnoreCase("running")) {
					
					LogManagerExtended.applicationInfoLog("Module Status is running status... Data is collecting");
					ApacheMonitorManager.getApacheMonitorManager(strHostName,strAppPort,strMonitorURL).monitorApacheCounters(moduleGUID,collectionData);
					
				}else if(moduleStatus.equalsIgnoreCase("restart")) {
		
					LogManagerExtended.applicationInfoLog("Module Status is restart mode... so counter data is reset");
					
					if(am.getConfigurationsFromCollector(moduleGUID, AGENT_TYPE.APACHE.toString())) {
						ApacheMonitorManager.getApacheMonitorManager(strHostName,strAppPort,strMonitorURL).monitorApacheCounters(moduleGUID,collectionData);     
					}
					
				}else if(moduleStatus.equalsIgnoreCase("stop")) {
					
					LogManagerExtended.applicationInfoLog("Module collect counter status is stop mode...");
					
				}else if(moduleStatus.equalsIgnoreCase("Kill")) {
					
					LogManagerExtended.applicationInfoLog("Module card is deleted....");
					this.timerLinuxUnificationApacheObj.cancel();
					this.timerLinuxUnificationApacheObj.purge();
					
				}
				LogManagerExtended.applicationInfoLog("================================================================");
			}else{
				LogManagerExtended.applicationInfoLog("Configuration problem.... Contact System Administration");
			}
		}catch (Throwable e) {
			LogManagerExtended.applicationInfoLog(e.getMessage());
		}
		
	}
	
	public static void main(String[] args) {

	}

}
