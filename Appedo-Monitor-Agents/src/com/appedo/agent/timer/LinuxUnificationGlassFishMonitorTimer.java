package com.appedo.agent.timer;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.manager.GlassFishMonitorManager;
import com.appedo.agent.manager.LogManagerExtended;
import com.appedo.agent.utils.Constants;
import com.appedo.agent.utils.UtilsFactory;
import com.appedo.agent.utils.Constants.AGENT_TYPE;

public class LinuxUnificationGlassFishMonitorTimer extends TimerTask{

	private Timer timerLinuxUnificationGlassFishObj = null;
	private Date collectionData = null;
	AgentManager am = null;
	String moduleStatus = "", moduleGUID = "",JMX_PORT;
	
	public LinuxUnificationGlassFishMonitorTimer(Timer timerObject, String agentGUID) {
		this.timerLinuxUnificationGlassFishObj = timerObject;
		this.moduleGUID = agentGUID;
		am = new AgentManager();
	}

	public void run() {
		try {
			 
			this.collectionData = new Date();
			
			this.moduleStatus = am.getModuleRunningStatus(moduleGUID, "appInformation");
			
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Status of Agent("+this.moduleGUID+")"+this.moduleStatus);
			
			if(moduleStatus.equalsIgnoreCase("running")) {
				
			   LogManagerExtended.applicationInfoLog("Module Status is running status... Data is collecting");
			   GlassFishMonitorManager.getGlassFishMonitorManager().monitorGlassFishCounters(moduleGUID, collectionData);
				
			}else if(moduleStatus.equalsIgnoreCase("restart")) {
          			  	
			   LogManagerExtended.applicationInfoLog("Module Status is restart mode... so counter data is reset");
			   if(am.getConfigurationsFromCollector(moduleGUID, AGENT_TYPE.GLASSFISH.toString())) {
				   GlassFishMonitorManager.getGlassFishMonitorManager().monitorGlassFishCounters(moduleGUID, collectionData);
				}
				
			}else if(moduleStatus.equalsIgnoreCase("stop")) {
				
				LogManagerExtended.applicationInfoLog("Module collect counter status is stop mode...");
				
			}else if(moduleStatus.equalsIgnoreCase("Kill")) {
				
				LogManagerExtended.applicationInfoLog("Module card is deleted....");
				this.timerLinuxUnificationGlassFishObj.cancel();
				this.timerLinuxUnificationGlassFishObj.purge();
				
			}
			LogManagerExtended.applicationInfoLog("================================================================");
		}
		 catch (Throwable e) {
		
			 LogManagerExtended.applicationInfoLog("Exception in GlassFish Timer Thread: "+e);
			 e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {

	}

}
