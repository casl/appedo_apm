package com.appedo.agent.timer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.utils.Constants;

public class LinuxUnificationSendCouterDataTimer extends TimerTask {
	AgentManager am = null;
	private Timer solarisTimerObj = null;
	
	public LinuxUnificationSendCouterDataTimer(Timer timerObject) {
		this.solarisTimerObj = timerObject;
		am = new AgentManager();
	}
	
	public void run() {
		try {
			boolean bReturn = false;
			Constants.FILE_WRITING_PROCESS = true;
			FileInputStream fstream = new FileInputStream("/root/linux_unified_agent/logs/counter_data.log");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			/* read log line by line */
			while ((strLine = br.readLine()) != null){
				bReturn = am.sendCounterValueToCollector(strLine);
				if(!bReturn) {
					Constants.FILE_WRITE_MODE = true;
					break;
				}else {
					Constants.FILE_WRITE_MODE = false;
				}
			}
			fstream.close();
			
			if(bReturn) {
				Files.delete(Paths.get("/root/linux_unified_agent/logs/counter_data.log"));
				this.solarisTimerObj.cancel();
				this.solarisTimerObj.purge();
			}
			
		}catch (Throwable e) {
			System.out.println("Exception in LinuxUnificationSendCouterDataTimer : "+e);
			Constants.FILE_WRITING_PROCESS = false;
		}finally {
			Constants.FILE_WRITING_PROCESS = false;
		}
	}
	
}
