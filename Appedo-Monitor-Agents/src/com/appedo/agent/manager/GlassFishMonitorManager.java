package com.appedo.agent.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.agent.bean.AgentCounterBean;
import com.appedo.agent.bean.LinuxUnificationBean;
import com.appedo.agent.bean.LinuxUnificationCounterBean;
import com.appedo.agent.bean.SlaCounterBean;
import com.appedo.agent.utils.Constants;
import com.appedo.agent.utils.UtilsFactory;
import com.appedo.agent.utils.Constants.AGENT_TYPE;


/**
 * GlassFish server monitoring class. This has the functionalities to get the counter values of GlassFish server.
 * 
 */
public class GlassFishMonitorManager extends AgentManager {

	private static GlassFishMonitorManager glassFishMonitorManager = null;
	private static HashMap<String, GlassFishMonitorManager> hmApplicationInstance = new HashMap<String, GlassFishMonitorManager>();

	private JMXConnector jmxc = null;
	private String hostName = null, JmxPort = null, userName = null, password = null;

	private JMXServiceURL url = null;
	private MBeanServerConnection connMBeanServer = null;

	Long lHitsCount = null, lActiveSessions = null, lRejectedSessions = null, lExpiredSessions = null;
	Long lRequestCount = null, lErrorCount = null, lBytesSent = null;
	Long lCurrentThreadsBusy = null, lCurrentThreadCount = null, lComittedHeapMemory = null;
	Long lMaxHeapMemory = null, lUsedHeapMemory = null, lFreePhysicalMemorySize = null, lTotalPhysicalMemorySize = null;

	private ObjectName glassFishDataObject = null;
	Double dCounterValue = null;

	/**
	 * Avoid the object creation for this Class from outside.
	 */
	public GlassFishMonitorManager() {
		// default constructor
	}

	/**
	 * Avoid the object creation for this Class from outside.
	 * Param constructor which creates JMX-connection using the credentials details of server from config file
	 * 
	 * @param strSvrAlias
	 * @throws Exception
	 */
	private GlassFishMonitorManager(String strSvrAlias) throws Exception {
		JSONObject joConfig = JSONObject.fromObject(Constants.ASD_DETAILS);
		JSONObject joSvrDetails = joConfig.getJSONObject(strSvrAlias);

		this.hostName = joSvrDetails.getString("host");
		this.JmxPort = joSvrDetails.getString("jmx_port");
		this.userName = joSvrDetails.getString("admin_user_name");
		this.password = joSvrDetails.getString("admin_user_password");

		boolean isRegistered = true;
		this.url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + hostName + ":" + JmxPort + "/jmxrmi");

		Map<String, String[]> env = new Hashtable<String, String[]>();
		String[] credentials = new String[] { userName, password };
		env.put(JMXConnector.CREDENTIALS, credentials);
		this.jmxc = JMXConnectorFactory.connect(url, env);
		this.connMBeanServer = jmxc.getMBeanServerConnection();

		// check whether AMX MBean is registered already if not invoke the same 
		isRegistered = this.connMBeanServer.isRegistered(new ObjectName("amx:pp=/mon/server-mon[server],type=memory-mon,name=jvm/memory"));
		if(!isRegistered){
			this.connMBeanServer.invoke(new ObjectName("amx-support:type=boot-amx"), "bootAMX",  new Object[0],  new String[0]);
		}
	}

	/**
	 * Returns the only object(singleton) of this Class, with respective of AppName
	 * 
	 * @param strAppName
	 * @param strSvrAlias
	 * @return
	 * @throws Exception
	 */
	public static GlassFishMonitorManager getGlassFishMonitorManager(String strAppName, String strSvrAlias) throws Exception {
		if (hmApplicationInstance.containsKey(strSvrAlias + " - " + strAppName)) {
			glassFishMonitorManager = hmApplicationInstance.get(strSvrAlias+" - "+strAppName);
		} else {
			glassFishMonitorManager = new GlassFishMonitorManager(strSvrAlias);
			hmApplicationInstance.put(strSvrAlias+" - "+strAppName, glassFishMonitorManager);
		}

		return glassFishMonitorManager;
	}
	public static GlassFishMonitorManager getGlassFishMonitorManager() throws Exception{
		return new GlassFishMonitorManager();
	}
	/**
	 * re-connects to the GlassFish's JMX Port 
	 * and initialize the objects.
	 * 
	 * @throws Exception
	 */
	private void reconnectGlassFishJMXObjects() throws Exception {
		try {
			Map<String, String[]> env = new Hashtable<String, String[]>();
			String[] credentials = new String[] { userName, password };
			env.put(JMXConnector.CREDENTIALS, credentials);
			this.jmxc = JMXConnectorFactory.connect(url, env);
			this.connMBeanServer = jmxc.getMBeanServerConnection();
		} catch (Throwable th) {
			System.out.println("Exception in reconnectObjects : " + th.getMessage());
			throw new Exception("Unable to reconnect GlassFish through JMX Port : " + JmxPort);
		}
	}

	/**
	 * Monitor the server and collect the counters
	 * 
	 * @param strGUID
	 * @param strAppName
	 */
	public void monitorGlassFishServer(String strGUID, String strAppName) {
		getCounters(strGUID, strAppName);
	}

	/**
	 * Send it to the Collector WebService
	 *
	 * @param strGUID
	 */
	public void sendGlassFishCounters(String strGUID) {
		sendCounters(strGUID);
	}

	/**
	 * Collect the counter with agent's types own logic or native methods
	 * 
	 * @param strGUID
	 * @param strAppName
	 */
	public void getCounters(String strGUID, String strAppName) {

		String strCounterId = null, strExecutionType = null;
		Integer nCounterId = null;
		String [] saQuery = null;
		boolean bIsDelta = false;

		ArrayList<String> alCommandOutput = null;

		try {
			// reset the counter collector variable in AgentManager.
			resetCounterMap(strGUID);

			// try to connect the MBean Objects, on failure reconnect to MBeanServer.
			try {
				connMBeanServer.getMBeanCount();
			} catch (IOException re) {
				reconnectGlassFishJMXObjects();
			}

			JSONArray joSelectedCounters = AgentCounterBean.getCountersBean(strGUID);

			for (int i = 0; i < joSelectedCounters.size(); i++) {
				try {
					JSONObject joSelectedCounter = joSelectedCounters.getJSONObject(i);				
					strCounterId = joSelectedCounter.getString("counter_id");
					nCounterId = Integer.parseInt(strCounterId);
					String query = joSelectedCounter.getString("query");
					bIsDelta = joSelectedCounter.getBoolean("isdelta");
					strExecutionType = joSelectedCounter.getString("executiontype");

					if ( strExecutionType.equals("cmd") ) {
						// counter value from command
						alCommandOutput = CommandLineExecutor.execute(query);
						if ( alCommandOutput.size() == 0 ) {
							throw new Exception("Metric doesn't return value");
						}
						dCounterValue = convertToDouble(alCommandOutput.get(0));
					} else {
						if(query.contains("#APP_NAME#")) {
							query = query.replace("#APP_NAME#", strAppName);
						}
						saQuery = query.split("#@#");

						glassFishDataObject = new ObjectName(saQuery[0].toString());	            	
						Object objVMInfo = connMBeanServer.getAttribute(glassFishDataObject, saQuery[1]);
						CompositeData compData = (CompositeData) objVMInfo;
						dCounterValue = convertToDouble(compData.get(saQuery[2]));
					}
					if (bIsDelta) {
						dCounterValue = addDeltaCounterValue(Integer.parseInt(strCounterId), dCounterValue);					
					} else {
						addCounterValue( Integer.parseInt(strCounterId), dCounterValue );
					}
					// TODO: Static Counter correction required

	            	// Verify SLA Breach
					// JSONObject joSLACounter = null;
					ArrayList<JSONObject> joSLACounter = null; // Need to change variable name as alSLACounters
					joSLACounter = verifySLABreach(strGUID, SlaCounterBean.getSLACountersBean(strGUID), Integer.parseInt(strCounterId), dCounterValue);

					// if breached then add it to Collector's collection
					if (joSLACounter != null) {
						addSlaCounterValue(joSLACounter);
					}
				} catch (Throwable th) {
					if( th.getMessage() != null && th.getMessage().startsWith("Connection refused to host :") ) {
						System.out.println("Exception in monitorGlassFishServer.counter-loop : "+th.getMessage());
						throw th;
					} else {
						System.out.println("Exception in monitorGlassFishServer.counter-loop : "+th.getMessage());
						th.printStackTrace();
						reportCounterError(nCounterId, th.getMessage());
					}
				}
			}
		} catch (Throwable th) {
			System.out.println("Exception in monitorGlassFishServer : "+th.getMessage());
			th.printStackTrace();
			reportGlobalError(th.getMessage());
		} finally {
			// queue the counter
			try {
				queueCounterValues();
			} catch (Exception e) {
				System.out.println("Exception in queueCounterValues() : "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	public void createGlassFishJMXConnection(String GlassFish_JMXPORT) {
		String conURL=null;
		
		try {
			conURL = Constants.GLASSFISH_JMX_CONNECTOR_URL.replace("#@#JMX_PORT#@#", GlassFish_JMXPORT).replace("#@#HOST#@#", Constants.GLASSFISH_HOST);
			
			LogManagerExtended.applicationInfoLog(conURL);
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Connection string for JMX : " + conURL);
			this.url = new JMXServiceURL(conURL);
			Map<String, String[]> env= new Hashtable<String, String[]>();  		                  
 			//LogManagerExtended.applicationInfoLog("ENV:"+ env);
			String username= Constants.GLASSFISH_USERNAME;
			String password= Constants.GLASSFISH_PASSWORD;
			String[] credentials = new String[] {username,password};
			//LogManagerExtended.applicationInfoLog("Username:"+username +" "+"Password:"+password);
			env.put(JMXConnector.CREDENTIALS, credentials);	
			//LogManagerExtended.applicationInfoLog("env");
			this.jmxc = JMXConnectorFactory.connect(url, env);	
			//System.out.println(jmxc);
			//LogManagerExtended.applicationInfoLog("jmxc." );
			this.connMBeanServer = jmxc.getMBeanServerConnection();
			
			LogManagerExtended.applicationInfoLog("GlassFish JMX Port is successfully connected");
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "JMX Port is successfully connected.");
		}catch (Exception e) {
			LogManagerExtended.applicationInfoLog("Exception in connectGlassFishObject : "+e);
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "JMX connection failed: "+e.getMessage());
			if(Constants.IS_DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void recreateGlassFishJMXConnection(String GlassFish_JMXPORT) {
		String conURL = null;
		try {
			
			conURL = Constants.GLASSFISH_JMX_CONNECTOR_URL.replace("#@#JMX_PORT#@#", GlassFish_JMXPORT);
			;
			this.url = new JMXServiceURL(conURL);
			
			Map<String, String[]> env= new Hashtable<String, String[]>();  		                  
 			;
			String username= Constants.GLASSFISH_USERNAME;
			String password= Constants.GLASSFISH_PASSWORD;
			String[] credentials = new String[] {username,password};
		
			env.put(JMXConnector.CREDENTIALS, credentials);	
			
			this.jmxc = JMXConnectorFactory.connect(url, env);	
			
			this.connMBeanServer = jmxc.getMBeanServerConnection();
			
		}catch (Exception e) {
			LogManagerExtended.applicationInfoLog("Exception in reconnectGlassFishObject : "+e);
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "JMX connection failed: "+e.getMessage());
			if(Constants.IS_DEBUG) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public JSONArray getGlassFishDynamicCounters() throws Exception {
		ObjectName http;
		String obj, category = "", name = "";
		JSONObject joNewCounterSet = null;
		JSONArray jaNewCounterSet = new JSONArray();
		String category1 = "",counter2 = null,counter4 = "";
		String[] type,counter3;
		
		try {	
			 createGlassFishJMXConnection(Constants.GLASSFISH_JMXPORT);
			 UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Getting all dynamic counter set.");
			 
			 Set<ObjectName> allObjectNames = this.connMBeanServer.queryNames(null, null);
			
			 for(Object mbean : allObjectNames){
				 http = (ObjectName)mbean;
				 obj = http.toString();
				
				 MBeanInfo info = this.connMBeanServer.getMBeanInfo(http);
				
				 MBeanAttributeInfo[] attrInfo = info.getAttributes();
				 
				 if(obj.contains("amx:pp=/mon/server-mon")&& !obj.contains("type=threadinfo-mon")&& !obj.contains("type=operating-system-mon")) {//&& (obj.contains("name=network/http-listener-1/connection-queue")||obj.contains("name=network/http-listener-2/thread-pool")||obj.contains("name=network//")||obj.contains("name=transaction-service")||obj.contains("name=http-service"))) {
	    				String[] value = obj.split(",");

	    				for (String res : value) {
	    					if(res.contains("name=jvm")){
	    					   type = res.split("/");
	    					 //type = res.replace("-", "");
	    				       category = type[1];
	    				       if(category.contains("-")) {
	    				    	   category1 = category.replace("-"," ");
	    				       }
	    				       else
	    				    	   category1 = type[1];
	    					   //if(type[1].contains("-")) { &&obj.contains("type=")()
	    						 // type[1].replaceAll("-", " ");
	    					      //category=type[1];
	    					   //}
	    					}  
				         else if(res.contains("name=network//")) {
				        	 type = res.split("//");
				        	 category1 = type[0]+" "+type[1];
				         }else if(res.contains("name=network/")) {//||(res.contains("name=jvm/garbage-collectors"))) {
				        	 type = res.split("/");
	    					 category1 = type[0]+" "+type[1]+" "+type[2];
				         }
				         else if(res.contains("name=transaction")) {
				        	 type = res.split("-");
				        	 category1 = type[0]+" "+type[1];
				         }else if(res.contains("name=http-service")) {
				        	 type=res.split("/");
				        	 String category2=type[1];
				        	 if(category2.contains("__"))
				        	 {
				        		 category1=type[0]+" "+category2.replace("__", "");//+" "+type[2];
				        	 }else
				        		 category1=type[0]+" "+type[1];//+" "+type[2];	 
				        	 
				         }
				      }
	    			}
			   
				 if(obj.contains("amx:pp=/mon/server-mon") && obj.contains("name=")  && obj.contains("type=garbage-collector-mon")) {
				  // LogManagerExtended.applicationInfoLog("Entering Mbeans for garbage collectors." );
				   String[] value = obj.split(",");
				   //LogManagerExtended.applicationInfoLog("Entering Mbeans for collector-garbage-mon." );
 				 for (String res : value) {
 					 if(res.contains("name=")){
 					    type = res.split("/");
 					    //type = res.replace("-", "");
 				        counter2 = type[2];
 				        //counter3 = type[2].split(" ");
 				        //counter4 = counter3[1];
 				       
 			        }  
			      } 
			   }
			  
			   if(obj.contains("amx:pp=/mon/server-mon")&&obj.contains("type=garbage-collector-mon")&&!obj.contains("type=threadinfo-mon")&&!obj.contains("type=operating-system-mon")){
		    	  for (MBeanAttributeInfo attr : attrInfo){
		    	     if(attr.getType().contains("javax.management.openmbean.CompositeData")&&(attr.getDescription().contains("nanoseconds")||attr.getDescription().contains("milliseconds")||attr.getName().contains("count"))) {
		    	    	 String attrInfo1 = attr.getName();
			    		 String[] attrInfo3 = attrInfo1.split("-");
		    	    	 
		    	    	 joNewCounterSet = new JSONObject();					    		
						 joNewCounterSet.put("category", category1);
						 joNewCounterSet.put("counter_name", attrInfo3[0].concat(counter2));
						 joNewCounterSet.put("has_instance", "f");
			    		 joNewCounterSet.put("instance_name", "");
			    		 joNewCounterSet.put("unit", setMetricsUnits(attr.getDescription() == null ? "" : attr.getDescription()));
			    		 joNewCounterSet.put("is_selected", setIsSelected(category1, attrInfo3[0].concat(counter2)));
			    		 joNewCounterSet.put("is_static_counter", "f");
			    		 joNewCounterSet.put("query_string", obj+"#@#"+attr.getName()+"#@#count");
		    		     joNewCounterSet.put("counter_description", attr.getDescription());
		    		     joNewCounterSet.put("is_delta", "f");
			    		           
		    		     jaNewCounterSet.add(joNewCounterSet); 
		    	    	
			    	
		            }
		         }
		      }
			  else if(obj.contains("amx:pp=/mon/server-mon")&&!obj.contains("type=threadinfo-mon")&&!obj.contains("type=operating-system-mon")&&!obj.contains("type=garbage-collector-mon")) {
				 for(MBeanAttributeInfo attr : attrInfo){
				     if(attr.getType().contains("javax.management.openmbean.CompositeData")&&(attr.getDescription().contains("nanoseconds")||attr.getDescription().contains("milliseconds")||attr.getName().contains("count"))) { 
				    	String attrInfo1 = attr.getName();
			    		String[] attrInfo3 = attrInfo1.split("-");
				    	
				    	joNewCounterSet = new JSONObject();					    		
					    joNewCounterSet.put("category", category1);
					    joNewCounterSet.put("counter_name", attrInfo3[0]);
					    joNewCounterSet.put("has_instance", "f");
		    		    joNewCounterSet.put("instance_name", "");
		    		    joNewCounterSet.put("unit", setMetricsUnits(attr.getDescription() == null ? "" : attr.getDescription()));
		    		    joNewCounterSet.put("is_selected", setIsSelected(category1, attrInfo3[0]));
		    		    joNewCounterSet.put("is_static_counter", "f");
		    		    joNewCounterSet.put("query_string", obj+"#@#"+attr.getName()+"#@#count");
	    		        joNewCounterSet.put("counter_description", attr.getDescription());
	    		        joNewCounterSet.put("is_delta", "f");
		    		           
	    		        jaNewCounterSet.add(joNewCounterSet); 
	    		        
				     }
				 }
			  }
		  }
			
						
		}catch(Exception e) {
			LogManagerExtended.applicationInfoLog("Exception in getGlassFishDynamicCounters "+e);
		}
		return jaNewCounterSet;
	}
   
	
	
	public boolean setIsSelected(String category, String counterName) {
		boolean bIsSeleted = false;
		
		if(category.equalsIgnoreCase("memory") && (counterName.equalsIgnoreCase("usedheapsize") || counterName.equalsIgnoreCase("committedheapsize"))) {
			bIsSeleted = true;
		}else if(category.equalsIgnoreCase("garbage collectors")) {
			if(counterName.equalsIgnoreCase("collectioncountMarkSweepCompact") || counterName.equalsIgnoreCase("collectiontimeCopy")) {
				bIsSeleted = true;
			}
		}else if(category.equalsIgnoreCase("class loading system")) {
			if(counterName.equalsIgnoreCase("totalloadedclass")) {
				bIsSeleted = true;
			}
		}else if(category.equalsIgnoreCase("compilation system")) {
			if(counterName.equalsIgnoreCase("totalcompilationtime")) {
				bIsSeleted = true;
			}
		}else if(category.equalsIgnoreCase("thread system")) {
			if(counterName.equalsIgnoreCase("currentthreadcputime") || counterName.equalsIgnoreCase("currentthreadusertime")) {
				bIsSeleted = true;
			}
		}else if(category.equalsIgnoreCase("runtime")) {
			if(counterName.equalsIgnoreCase("uptime")) {
				bIsSeleted = true;
			}
		}
		
		return bIsSeleted;
	
	}
	
   public void monitorGlassFishCounters(String strGUID, Date collectionDate){
		
		JSONArray joSelectedCounters = null, jaSlaCounters = null;
		JSONObject joSelectedCounter = null;
		
		LinuxUnificationBean beanLinuxUnification =null;
		LinuxUnificationBean beanSLA = null;
		
		LinuxUnificationCounterBean beanLinuxCounters = null;
		
		Integer nCounterId = null;
		String strExecutionType = null, query = null, strCommandOutput = null;
		String [] saQuery = null;
		boolean bIsDelta = false;
		
		CommandLineExecutor cmdExecutor = null;
		
		try {
			
			joSelectedCounters = AgentCounterBean.getCountersBean(strGUID);
			
			beanLinuxUnification = new LinuxUnificationBean();
			beanLinuxUnification.setMod_type("GlassFish");
			beanLinuxUnification.setType("MetricSet");
			beanLinuxUnification.setGuid(strGUID);
			beanLinuxUnification.setdDateTime(collectionDate);
			
			jaSlaCounters = SlaCounterBean.getSLACountersBean(strGUID);
			
			if(jaSlaCounters != null && jaSlaCounters.size() > 0) {
				beanSLA = new LinuxUnificationBean();
			}
			
			createGlassFishJMXConnection(Constants.GLASSFISH_JMXPORT);
			
			for (int i = 0; i < joSelectedCounters.size(); i++) {
				
				try {
					joSelectedCounter = joSelectedCounters.getJSONObject(i);
					nCounterId = Integer.parseInt(joSelectedCounter.getString("counter_id"));
					query = joSelectedCounter.getString("query");
					bIsDelta = joSelectedCounter.getBoolean("isdelta");
					strExecutionType = joSelectedCounter.getString("executiontype");
					beanLinuxUnification.addNewCounter(joSelectedCounter.getString("counter_id"));
					
					if ( strExecutionType.equals("cmd") ) {
						// Get counter-value after executing the command
						cmdExecutor = new CommandLineExecutor();
						cmdExecutor.executeCommand(query);
						strCommandOutput = cmdExecutor.getOutput().toString();
						
						if ( strCommandOutput.length() == 0 ) {
							throw new Exception("Metric doesn't return value: "+cmdExecutor.getErrors());
						}
						dCounterValue = convertToDouble(strCommandOutput);
					}else {
						
						saQuery = query.split("#@#");
						
						ObjectName glassFishDataObject = new ObjectName(saQuery[0].toString());
						Object objVMInfo = connMBeanServer.getAttribute(glassFishDataObject, saQuery[1]);
						CompositeData compData = (CompositeData) objVMInfo;
						Object dCounter = compData.get(saQuery[2]);
						dCounterValue = convertToDouble(dCounter);

						if ( bIsDelta ) {
							//dCounterValue = addDeltaCounterValue(Integer.parseInt(strCounterId), dCounterValue);
							dCounterValue = addDeltaCounterValue_v1(nCounterId, dCounterValue);
						}						
						// Create Bean for the LinuxUnification OS Module Counter entry(line)
						beanLinuxCounters = new LinuxUnificationCounterBean();
						beanLinuxCounters.setCounter_type(nCounterId);
						beanLinuxCounters.setException("");
						beanLinuxCounters.setProcess_name("");
						beanLinuxCounters.setCounter_value(dCounterValue);
						beanLinuxUnification.addCounterEntry(String.valueOf(nCounterId), beanLinuxCounters);
						if(SlaCounterBean.getSLACountersBean(strGUID) != null && SlaCounterBean.getSLACountersBean(strGUID).size() > 0) {
							//Verifying SLA Breach
							verifySLABreach_v1(jaSlaCounters, nCounterId, dCounterValue, beanSLA);
						}
					}
						
				}catch (Exception e) {
					LogManagerExtended.applicationInfoLog(e.getMessage());
				}
			}
			
			if(beanLinuxUnification.isCountersValueAvailable()) {
				if(Constants.IS_SOLARIS_OS) {
					LinuxMonitorManager.getLinuxMonitorManager().getQueueObject().add(beanLinuxUnification.toString("MetricSet"));
				}else {
					LogManagerExtended.logJStackOutput("metrics###"+beanLinuxUnification.toString("MetricSet"));
					LogManagerExtended.applicationInfoLog("metrics###"+beanLinuxUnification.toString("MetricSet"));
				}
			}
			
			if(beanSLA != null) {
				if(beanSLA.isSLACountersValueAvailable()) {
					beanSLA.setMod_type("GlassFish");
					beanSLA.setType("SLASet");
					beanSLA.setGuid(Constants.LINUX_AGENT_GUID);
					beanSLA.setdDateTime(collectionDate);
					
					if(Constants.IS_SOLARIS_OS) {
						LinuxMonitorManager.getLinuxMonitorManager().getQueueObject().add(beanSLA.toString("SLASet"));
					}else {
						LogManagerExtended.logJStackOutput("metrics###"+beanSLA.toString("SLASet"));
						LogManagerExtended.applicationInfoLog("metrics###"+beanSLA.toString("SLASet"));
					}
				}
			}
			
		}catch (Exception e) {
			LogManagerExtended.applicationInfoLog("Exception in monitorModuleCounters(): " + e);
			e.printStackTrace();
		}
	}
	
   
   public JSONObject getGlassFishServerDetails() {
		JSONObject joGlassFishInfo = new JSONObject();
		ObjectName http;
		String obj;
		String name = "GlassFish",VERSION_ID=null;
		try {
			 createGlassFishJMXConnection(Constants.GLASSFISH_JMXPORT);
			 Set<ObjectName> allObjectNames = this.connMBeanServer.queryNames(null, null);
			 for (Object mbean : allObjectNames){
				http = (ObjectName)mbean;
				obj = http.toString();
			 if(obj.contains("amx:pp=")&& obj.contains("type=domain-root")){
			   	Object value = this.connMBeanServer.getAttribute(http, "ApplicationServerFullVersion");
			   	String[] xvalue1 = ((String) value).split(" ");
			   	//LogManagerExtended.applicationInfoLog("value:"+value);//System.out.println("Value: \n"+value);
			   	for(String x : xvalue1) {
			   	   if(x.contains("."))
			   		VERSION_ID=x;
			   	//LogManagerExtended.applicationInfoLog("Value: \n"+x);
			    } 	
		     }
			
	        joGlassFishInfo.put("moduleName",name + "-" + Constants.SYSTEM_ID);
	        joGlassFishInfo.put("moduleTypeName",name);
	        joGlassFishInfo.put("VERSION_ID",VERSION_ID); 
	      }     
	  }catch (Exception e) {
			LogManagerExtended.applicationInfoLog("Exception in GlassFishServerDetails "+e);
	   }
		
	 return joGlassFishInfo;
	}	
   
   public static String setMetricsUnits(String MetricDesc) {
		String Units= "";
		
		if(MetricDesc.toLowerCase().contains("nanoseconds")) {
			Units = "ns";
		}else if(MetricDesc.toLowerCase().contains("milliseconds")) {
			Units = "ms";
		}else if(MetricDesc.toLowerCase().contains("seconds")) {
			Units = "sec";
		}else if(MetricDesc.toLowerCase().contains("bytes")) {
			Units = "bytes";
		}else if(MetricDesc.toLowerCase().contains("kb")) {
			Units = "kb";
		}else if(MetricDesc.toLowerCase().contains("time")) {
			Units = "ms";
		}else if(MetricDesc.toLowerCase().contains("count")) {
			Units = "count";
		}
		else {
			Units = "count";
		}
		
		return Units;
	}
	
	
	/**
	 * Send the collected counter-sets to Collector WebService, by calling parent's sendCounter method
	 * @param strGUID
	 */
	public void sendCounters(String strGUID) {
		// send the collected counters to Collector WebService through parent sender function
		sendCounterToCollector(strGUID, AGENT_TYPE.GLASSFISH);
		sendSlaCounterToCollector(strGUID, AGENT_TYPE.GLASSFISH);
	}

	public Long convertToLong(Object obj) {
		Long lReturn = 0l;
		if (obj instanceof Long) {
			lReturn = ((Long) obj).longValue();
		} else if (obj instanceof Integer) {
			lReturn = (new Integer((Integer) obj)).longValue();
		}

		return lReturn;
	}

	public Double convertToDouble(Object obj) {
		Double dReturn = 0D;

		if (obj instanceof Long) {
			dReturn = ((Long) obj).doubleValue();
		} else if (obj instanceof Integer) {
			dReturn = ((Integer) obj).doubleValue();
		} else if (obj instanceof Double) {
			dReturn = ((Double) obj).doubleValue();
		} else if (obj instanceof String) {
			dReturn = Double.parseDouble(obj.toString());
		}

		return dReturn;
	}

	@Override
	protected void finalize() throws Throwable {
		clearCounterMap();

		if (jmxc != null) {
			jmxc.close();
			jmxc = null;
		}

		super.finalize();
	}
}
