package com.appedo.agent.init;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.manager.LogManagerExtended;
import com.appedo.agent.manager.ApacheMonitorManager;
import com.appedo.agent.timer.LinuxUnificationApacheMonitorTimer;
import com.appedo.agent.utils.Constants;
import com.appedo.agent.utils.UtilsFactory;
import com.appedo.agent.utils.Constants.AGENT_TYPE;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AgentIgnitorLinuxUnificationApacheThread extends Thread{

	AgentManager am = null;
	ApacheMonitorManager apmManager = null;


	public AgentIgnitorLinuxUnificationApacheThread() {
		am = new AgentManager();
		
		start();
	}
	
	public void run() {
		
		JSONObject counterSet = null;
		String moduleGUID = "";
		JSONObject joModuleInfo = null;
		
		try { 
			LogManagerExtended.databaseInfoLog("Linux Unification Apache monitor services started...");
			
			if( Constants.APACHE_CONFIG!=null ) {
				JSONObject joRecord = JSONObject.fromObject(Constants.APACHE_CONFIG);
				String strHostName = joRecord.getString("host");
				String strAppPort = joRecord.getString("app_port");
				String strMonitorURL = joRecord.getString("monitor_url");
				   
				apmManager = new ApacheMonitorManager(strHostName,strAppPort,strMonitorURL);
				joModuleInfo = apmManager.getApacheServerDetails();
				counterSet = new JSONObject();
				moduleGUID = am.sendModuleInfoToCollector(joModuleInfo, Constants.SYSTEM_ID, Constants.SYS_UUID, counterSet, "appInformation");
				LogManagerExtended.applicationInfoLog("Application Information : "+joModuleInfo.toString());
				LogManagerExtended.applicationInfoLog("ModuleGUID : "+ moduleGUID);
			    
				if(!moduleGUID.isEmpty()) {
					Constants.APACHE_AGENT_GUID = moduleGUID;
					if(am.getConfigurationsFromCollector(Constants.APACHE_AGENT_GUID, AGENT_TYPE.APACHE.toString())) {
						Timer timerLinuxUnificationApacheObj = new Timer();
						TimerTask ttLinuxUnificationApache = new LinuxUnificationApacheMonitorTimer(timerLinuxUnificationApacheObj,moduleGUID);
						timerLinuxUnificationApacheObj.schedule(ttLinuxUnificationApache, 100l, Constants.MONITOR_FREQUENCY_MILLESECONDS);
					}
			    }
			}else {
				LogManagerExtended.applicationInfoLog("Configuration problem.... Contact System Administration");
			}
			
		}catch (Throwable e) {
			LogManagerExtended.applicationInfoLog("Exception in AgentIgnitorLinuxUnificationApcheThread : "+e);
		}
	}
}
