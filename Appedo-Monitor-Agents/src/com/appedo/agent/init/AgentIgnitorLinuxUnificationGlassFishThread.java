package com.appedo.agent.init;

import java.util.Timer;
import java.util.TimerTask;
import com.appedo.agent.manager.AgentManager;
import com.appedo.agent.manager.LogManagerExtended;
import com.appedo.agent.manager.GlassFishMonitorManager;
import com.appedo.agent.timer.LinuxUnificationGlassFishMonitorTimer;
import com.appedo.agent.utils.Constants;
import com.appedo.agent.utils.UtilsFactory;
import com.appedo.agent.utils.Constants.AGENT_TYPE;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AgentIgnitorLinuxUnificationGlassFishThread extends Thread{

	AgentManager am = null;
	GlassFishMonitorManager glManager = null;
	String JMX_PORT;
	
	boolean isAppServerRunning;
	
	public AgentIgnitorLinuxUnificationGlassFishThread(String JmxPort) {
		am = new AgentManager();
		glManager = new GlassFishMonitorManager();
		JMX_PORT = JmxPort;
		start();
	}

	public void run() {
		
		String moduleGUID = "";
		JSONArray jaCounterSet = null;
		JSONObject joAppInformation = null;
		JSONObject joAppNewCounterSet = new JSONObject();
		
		try {
			  
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Thread started for JMX port : "+JMX_PORT);
			LogManagerExtended.applicationInfoLog("Linux Unification GlassFish thread started. for JMX_PORT: "+JMX_PORT);
	
			glManager.createGlassFishJMXConnection(JMX_PORT);
			joAppInformation = glManager.getGlassFishServerDetails();
			jaCounterSet = glManager.getGlassFishDynamicCounters();
					
			joAppNewCounterSet.put("counterData", jaCounterSet);
			joAppInformation.put("jmx_port", JMX_PORT);
					
			UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Application Information : "+joAppInformation.toString());
			LogManagerExtended.applicationInfoLog("Application Information : "+joAppInformation.toString());
			moduleGUID = am.sendModuleInfoToCollector(joAppInformation, Constants.SYSTEM_ID, Constants.SYS_UUID, joAppNewCounterSet, "appInformation");
			//LogManagerExtended.applicationInfoLog("Application Information : "+joAppInformation.toString());
			LogManagerExtended.applicationInfoLog("ModuleGUID : "+ moduleGUID);
					
			if(!moduleGUID.isEmpty()) {
				Constants.GLASSFISH_AGENT_GUID = moduleGUID;
				if(am.getConfigurationsFromCollector(Constants.GLASSFISH_AGENT_GUID, AGENT_TYPE.GLASSFISH.toString())) {
					UtilsFactory.printDebugLog(Constants.IS_DEBUG, "Data collection started for GUID : "+Constants.GLASSFISH_AGENT_GUID);
					Timer timerLinuxUnificationGlassFishObj = new Timer();
					TimerTask ttLinuxUnificationGlassFish = new LinuxUnificationGlassFishMonitorTimer(timerLinuxUnificationGlassFishObj,moduleGUID);
					timerLinuxUnificationGlassFishObj.schedule(ttLinuxUnificationGlassFish, 100l, Constants.MONITOR_FREQUENCY_MILLESECONDS);
				}
			}
				
			LogManagerExtended.applicationInfoLog("Application module is created...");

					
			}catch (Throwable e) {
				LogManagerExtended.applicationInfoLog(e.getMessage());
				e.printStackTrace();
			}
				
		}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
