package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.manager.AppedoMailer;
import com.appedo.commons.manager.AppedoMailer.MODULE_ID;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SUMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.utils.UtilsFactory;

public class SUMManager {

	/**
	 * 
	 * @param strGUID
	 * @param strBreachCounterSet
	 * @return
	 * @throws Exception
	 */
	public boolean collectSUMBreach(String strBreachCounterSet) throws Exception {
		SUMCollectorBean sumBreachBean = null;
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		boolean bQueued = false;
		
		try {
			if( strBreachCounterSet != null ) {
				JSONObject joBreachCounters = JSONObject.fromObject(strBreachCounterSet);
				
				sumBreachBean = new SUMCollectorBean();
				sumBreachBean.setJoCountersBean(joBreachCounters);
				sumBreachBean.setDateQueuedOn(new Date());
				
				// add to Queue
				bQueued = collectorManager.collectSUMBean(sumBreachBean);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			sumBreachBean = null;
			collectorManager = null;
		}
		
		return bQueued;
	}
	
	
	/**
	 * 
	 * @param con
	 * @param lSlaid
	 * @return
	 * @throws Exception
	 */
	public void sendAlerts(Connection con, JSONObject joSUMBreachDetails, long lTimeAppedoQueuedOn) throws Throwable {
		Date dateLog = LogManager.logMethodStart();
		JSONObject joSUMDetails = null;
		
		String strThresholdOperator = null;
		String strToEmail = "", strToSMS = "", strISDCode = "", strEmailContent = "", strSMSContent = "", strSMSBody = "", strMessage = "";
		String strBreachedTestName = "", strBreachedLocation = "", strSubject = "";
		StringBuilder sbHTMLMessage = new StringBuilder();
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		HashMap<String, Object> hmMailDetails =  null;
		
		boolean bMailSent = false, bSMSSent = false, bEmailAlert = false, bMsgSent = false;
		
		long lUserId = -1L, lSLAId = -1L, lAlertLogId = 0;
		
		SlaManager slaManager = null;
		
		AppedoMailer appedoMailer = null;
		AppedoMobileManager appedoMobileMan = null;
		
		try{
			slaManager = new SlaManager();
			appedoMailer = new AppedoMailer( Constants.EMAIL_TEMPLATES_PATH );
			appedoMobileMan = new AppedoMobileManager();
			
			lSLAId = joSUMBreachDetails.getLong("sla_id");
			lUserId = joSUMBreachDetails.getLong("userid");
			strBreachedLocation = joSUMBreachDetails.getString("location");
			
			joSUMDetails = getSUMTestDetails(con, joSUMBreachDetails.getLong("sum_test_id"));
			strBreachedTestName = joSUMDetails.getString("sum_test_name");
			
			
			/* if((joDetails.getDouble("received_value")==0 && joDetails.getBoolean("is_Down"))){
				sb = getDownTimeCounter(joDetails, joSUMDetails);
			}else{
				sb = getCounterNames(joDetails, joSUMDetails);	
			} */
			
			
			hmMailDetails = new HashMap<String, Object>();
			//strSubject = "SUM Alert! "+(joDetails.getDouble("received_value")>joDetails.getDouble("err_set_value")?"- Error":"- Warning");
			//hmMailDetails.put("BODY", ((joDetails.getDouble("received_value")==0 && joDetails.getBoolean("is_Down"))?" The below configured Site is down ":rs.getString("ebody"))+sb.toString());
			hmMailDetails.put("test_type", joSUMDetails.getString("test_type"));
			hmMailDetails.put("url", joSUMDetails.getString("url"));
			hmMailDetails.put("testname", strBreachedTestName);
			hmMailDetails.put("location", strBreachedLocation);
			
			if ( joSUMDetails.getString("test_type").equals("URL") ) {
				hmMailDetails.put("test_details_1", joSUMDetails.getString("url"));
				hmMailDetails.put("test_details_2", strBreachedTestName);
			} else {
				hmMailDetails.put("test_details_1", strBreachedTestName);
				hmMailDetails.put("test_details_2", joSUMDetails.getString("test_type"));
			}
			
			if(joSUMBreachDetails.containsKey("received_value")){
				hmMailDetails.put("received_value", joSUMBreachDetails.getDouble("received_value"));
			}
			if( joSUMBreachDetails.getBoolean("is_above") ) {
				strThresholdOperator = ">=";
			} else {
				strThresholdOperator = "<=";
			}
			hmMailDetails.put("threshold_operator", strThresholdOperator);
			// TODO RAM P1: breached_on should be the time, when data received by Collector. 
			hmMailDetails.put("breached_on", lTimeAppedoQueuedOn);
			//hmMailDetails.put("breached_on", UtilsFactory.formatTimeStampToyyyyMMddHHmmssS(lTimeAppedoQueuedOn));
			
			// TODO RAM P1: Move this portion to WPT_Scheduler; Add DB columns for this. 
			// Web-site is Down
			if (joSUMBreachDetails.getBoolean("is_Down")) {
				hmMailDetails.put("breached_severity", joSUMBreachDetails.getString("breached_severity"));
				hmMailDetails.put("breached_severity_subject", "Website Not Reachable");
				hmMailDetails.put("breached_severity_message", "Your website is down!.");
				hmMailDetails.put("breached_severity_message_html", "<span style=\"padding: 3px 12px; width: 85px; height: 22px; vertical-align: middle; text-align:center; background-color:RED; color:#eee; font-weight:bold;\">Website is not reachable</span>");
				hmMailDetails.put("severity_color", "RED");
			}
			// Critical
			else if( joSUMBreachDetails.getDouble("received_value") >= joSUMBreachDetails.getDouble("err_set_value") ){
				hmMailDetails.put("breached_severity", joSUMBreachDetails.getString("breached_severity"));
				hmMailDetails.put("breached_severity_subject", "Critical");
				hmMailDetails.put("breached_severity_message", "Your website have crossed its threshold limit.");
				hmMailDetails.put("severity_color", "RED");
				hmMailDetails.put("threshold_value", joSUMBreachDetails.getDouble("err_set_value"));
				
				sbHTMLMessage
				.append("<span style=\"font-size: 22px; font-weight: bold;\">").append(joSUMBreachDetails.getDouble("received_value")).append("</span> sec ").append((String)hmMailDetails.get("threshold_operator")).append(" ").append((Double)hmMailDetails.get("threshold_value"))
				.append("<span style=\"padding: 10px;\"></span>")
				.append("<span style=\"padding: 3px 12px; width: 85px; height: 22px; vertical-align: middle; text-align:center; background-color:").append((String)hmMailDetails.get("severity_color")).append("; color:#eee; font-weight:bold;\">").append(joSUMBreachDetails.getString("breached_severity")).append("</span>");
				hmMailDetails.put("breached_severity_message_html", sbHTMLMessage.toString());
			}
			// Warning
			else if( joSUMBreachDetails.getDouble("received_value") >= joSUMBreachDetails.getDouble("threshold_set_value") ){
				hmMailDetails.put("breached_severity", joSUMBreachDetails.getString("breached_severity"));
				hmMailDetails.put("breached_severity_subject", "Warning");
				hmMailDetails.put("breached_severity_message", "Your website have crossed its threshold limit.");
				hmMailDetails.put("severity_color", "ORANGE");
				hmMailDetails.put("threshold_value", joSUMBreachDetails.getDouble("threshold_set_value"));
				
				sbHTMLMessage
				.append("<span style=\"font-size: 22px; font-weight: bold;\">").append(joSUMBreachDetails.getDouble("received_value")).append("</span> sec ").append((String)hmMailDetails.get("threshold_operator")).append(" ").append((Double)hmMailDetails.get("threshold_value"))
				.append("<span style=\"padding: 10px;\"></span>")
				.append("<span style=\"padding: 3px 12px; width: 85px; height: 22px; vertical-align: middle; text-align:center; background-color:").append((String)hmMailDetails.get("severity_color")).append("; color:#eee; font-weight:bold;\">").append(joSUMBreachDetails.getString("breached_severity")).append("</span>");
				hmMailDetails.put("breached_severity_message_html", sbHTMLMessage.toString());
			}
			
			// get user's alert to addresses of email ids & mobile numbers, with SLA details
			alAlertAddresses = slaManager.getUserAlertToAddresses(con, lUserId, "");
			for (int i = 0; i < alAlertAddresses.size(); i = i + 1) {
				hmAlertAddress = alAlertAddresses.get(i);
				
				bEmailAlert = hmAlertAddress.get("alertType").equals("Email");
				strMessage = "SUM \""+strBreachedTestName+"\" is breached in \""+strBreachedLocation+"\".";
				
				if( bEmailAlert ) {	// for email
					//strEmailContent = hmAlertAddress.get("emailBody");
					strEmailContent = strMessage;
					strToEmail = hmAlertAddress.get("emailMobile");
					strSMSContent = "";
					strToSMS = "";
					strISDCode = "";
				} else {	// for SMS
					strEmailContent = "";
					strToEmail = "";
					//strSMSContent = hmAlertAddress.get("smsText");
					strSMSContent = strMessage;
					strISDCode = hmAlertAddress.get("telephoneCode");
					strToSMS = strISDCode+hmAlertAddress.get("emailMobile");
				}
				// inserts into `sla_alert_log_<user_id>`
				lAlertLogId = slaManager.insertSLAAlertLog(con, lUserId, lSLAId, lTimeAppedoQueuedOn, strEmailContent, strToEmail, true, bMsgSent, strSMSContent, strToSMS);
				
				// sends msg to user, email OR SMS
				if ( bEmailAlert && joSUMDetails.getBoolean("email_alert") ) {
					//if( joSUMDetails.getBoolean("email_alert") ) {
						strSubject = "FYA: {{ appln_heading }} SUM Alert - {{testname}} : {{breached_severity_subject}}";
						bMailSent = appedoMailer.sendMail(MODULE_ID.SUM_ALERT_EMAIL, hmMailDetails, strToEmail.split(","), strSubject);
					//}
				} else if ( strToSMS != null && strToSMS.length() > 0 && joSUMDetails.getBoolean("sms_alert") ) {
					if ( joSUMBreachDetails.getBoolean("is_Down") ) {
						strSMSBody = "SUM Alert! "+joSUMBreachDetails.getString("breached_severity")+"\nTest Name: "+strBreachedTestName+"\nLocation: "+strBreachedLocation;
					} else {
						strSMSBody = "SUM Alert! "+joSUMBreachDetails.getString("breached_severity")+"\nTest Name: "+strBreachedTestName+"\nPage Load Time: "+joSUMBreachDetails.getDouble("received_value")+"s \nLocation: "+strBreachedLocation;
					}
					
					bSMSSent = appedoMobileMan.sendSMS(strISDCode, strToSMS, strSMSBody.trim(), lAlertLogId);
				}
			}
			
		} catch(Throwable th) {
			throw th;
		} finally {
			appedoMailer = null;
			appedoMobileMan = null;
			slaManager = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void sendDownTimeAlerts(Connection con, JSONObject joSUMBreachDetails) throws Throwable {
		Date dateLog = LogManager.logMethodStart();
		
		JSONObject joSUMDetails = null;
		
		HashMap<String, Object> hmMailDetails = null;
		
		AppedoMailer appedoMailer = null;
		AppedoMobileManager appedoMobileMan = null;
		String strSubject = "";
		
		try {
			appedoMailer = new AppedoMailer( Constants.RESOURCE_PATH );
			appedoMobileMan = new AppedoMobileManager();
			
			joSUMDetails = getSUMTestDetails(con, joSUMBreachDetails.getLong("sum_test_id"));
			
			hmMailDetails = new HashMap<String, Object>();
			
			hmMailDetails.put("test_type", joSUMDetails.getString("test_type"));
			hmMailDetails.put("url", joSUMDetails.getString("url"));
			hmMailDetails.put("testname", joSUMDetails.getString("sum_test_name"));
			hmMailDetails.put("location", joSUMBreachDetails.getString("location"));
			hmMailDetails.put("received_value", joSUMBreachDetails.getDouble("received_value"));
			hmMailDetails.put("breached_on", (new Date()).getTime());
			
			// TODO RAM P1: Move this portion to WPT_Scheduler; Add DB columns for this.
			if (joSUMBreachDetails.getDouble("received_value") == 0 && joSUMBreachDetails.getBoolean("is_Down")) {
				hmMailDetails.put("breached_severity", "WEBSITE_NOT_REACHABLE");
				hmMailDetails.put("breached_severity_subject", "Website Not Reachable");
				hmMailDetails.put("breached_severity_message", "Your website is down!.");
				hmMailDetails.put("breached_message_html", "<span style=\"padding: 3px 12px; width: 85px; height: 22px; vertical-align: middle; text-align:center; background-color:RED; color:#eee; font-weight:bold;\">Website is not reachable</span>");
				hmMailDetails.put("severity_color", "RED");
			}
			
			if ( joSUMBreachDetails.getString("test_type").equals("URL") ) {
				hmMailDetails.put("test_details_1", joSUMBreachDetails.getString("url"));
				hmMailDetails.put("test_details_2", joSUMBreachDetails.getString("sum_test_name"));
			} else {
				hmMailDetails.put("test_details_1", joSUMBreachDetails.getString("sum_test_name"));
				hmMailDetails.put("test_details_2", joSUMBreachDetails.getString("test_type"));
			}
			
			strSubject = "FYA: {{ appln_heading }} SUM Alert - {{testname}} : {{breached_severity_subject}}";
			appedoMailer.sendMail(MODULE_ID.SUM_ALERT_EMAIL, hmMailDetails, joSUMDetails.getString("email_id").split(","), strSubject);
			
			if (joSUMDetails.getString("mobile_no").length() > 0) {
				appedoMobileMan.sendSMS(joSUMDetails.getString("isd_code"), joSUMDetails.getString("isd_code")+joSUMDetails.getString("mobile_no"), "SUM Alert! " + joSUMBreachDetails.getString("breached_severity") + "\nTest Name: " + joSUMDetails.getString("sum_test_name") + "\nPage Load Time: " + joSUMBreachDetails.getDouble("received_value") + "s \nLocation: " + joSUMBreachDetails.getString("location").trim(), joSUMBreachDetails.getLong("sum_test_id"));
			}
		} catch (Throwable th) {
			throw th;
		} finally {
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void sendAlertsDevOps(Connection con, JSONObject joInActiveLocations) throws Throwable {
		Date dateLog = LogManager.logMethodStart();
		ResultSet rst = null;
		PreparedStatement pstmt = null;
		
		HashMap<String, Object> hmMailDetails =  null;	
		boolean bMailSent = false;
		AppedoMailer appedoMailer = null;
		StringBuilder sbMailContent = new StringBuilder();
		String strSubject = "";
		
		try{
			appedoMailer = new AppedoMailer( Constants.RESOURCE_PATH );
			hmMailDetails = new HashMap<String, Object>();
			
			if(joInActiveLocations.containsKey("inactive_nodes")){
				String inactive_locations = (joInActiveLocations.getString("inactive_nodes")).replaceAll("\",\"", "<br/>").replace("[\"", "").replace("\"]", "");
				hmMailDetails.put("inactive_locations", inactive_locations);
				hmMailDetails.put("inactive_location_message_color", "#222");
			}else{
				hmMailDetails.put("inactive_locations", "");
				hmMailDetails.put("inactive_location_message_color", "#AAA");
			}
			
			if(joInActiveLocations.containsKey("new_desktop_agents")){
				String new_desktop_location = (joInActiveLocations.getString("new_desktop_agents")).replaceAll(",", "<br/>").replace("[", "").replace("]", "").replace(" - ", "<br/><br/>");
				hmMailDetails.put("new_desktop_location", new_desktop_location);
				hmMailDetails.put("new_desktop_location_message_color", "#222");
			}else{
				hmMailDetails.put("new_desktop_location", "");
				hmMailDetails.put("new_desktop_location_message_color", "#AAA");
			}
			
			if(joInActiveLocations.containsKey("new_mobile_agents")){
				String new_mobile_location = (joInActiveLocations.getString("new_mobile_agents")).replaceAll(",", "<br/>").replace("[", "").replace("]", "").replace(" - ", "<br/><br/>");
				hmMailDetails.put("new_mobile_location", new_mobile_location);
				hmMailDetails.put("new_mobile_location_message_color", "#222");
			}else{
				hmMailDetails.put("new_mobile_location", "");
				hmMailDetails.put("new_mobile_location_message_color", "#AAA");
			}
			
			String emailsToAlert=Constants.EMAIL_TO_ALERT_FOR_SUM_AGENT;
			strSubject = "FYA: {{ appln_heading }} SUM Agent Alert";
			bMailSent = appedoMailer.sendMail(MODULE_ID.SUM_AGENT_ALERT_EMAIL, hmMailDetails, emailsToAlert.split(","), strSubject);
			
			if(bMailSent){
				LogManager.infoLog("Is mail sent to devops/admin "+ bMailSent);
			} else {
				LogManager.infoLog("error sending mail to devops/admin "+ bMailSent);
			}
		} catch(Throwable th) {
			throw th;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbMailContent );
			sbMailContent = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public JSONObject getSUMTestDetails(Connection con, long lSUMTestId) throws Exception {
		SlaCollectorDBI slaCollectorDBI = null;
		
		JSONObject joSUMTestDetails = null;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			
			joSUMTestDetails = slaCollectorDBI.getSUMTestDetails(con, lSUMTestId);
			if ( joSUMTestDetails == null ) {
				// would have been deleted
				throw new Exception("TestId: "+lSUMTestId+", not found; SUM test has been deleted.");
			}
			
			slaCollectorDBI = null;
		} catch(Exception e) {
			throw e;
		}
		
		return joSUMTestDetails;
	}
}
