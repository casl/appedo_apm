package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SlaCollectorBean;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.dbi.SlaDBI;
import com.appedo.slacollector.utils.UtilsFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author veeru
 *
 */
public class ModulePerformanceSlaManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected SlaCollectorDBI collectorDBI = null;
	
	protected CollectorManager collectorManager = null;
	
	
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public ModulePerformanceSlaManager() throws Exception {
		collectorDBI = new SlaCollectorDBI();
		
		establishDBonnection();
		
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @throws Exception
	 */
	public void fetchBean(SlaCollectorBean cbSlaEntry) throws Exception {
		JSONArray jaBreachedCounters = null, jaSLACounters = null;
		JSONObject joBreachedCounter = null;
		
		// TODO: Imp. ASK, ordered HashMap
		HashMap<Long, Long> hmSLAs = null;
		HashSet<Long> hsUserIds = null;
		
		long lUserId = -1L, lSLAId = -1L, lTimeAppedoQueuedOn = -1L;
		
		SlaManager slaManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			conPC = DataBaseManager.reEstablishConnection(conPC);
			
			jaBreachedCounters = cbSlaEntry.getCountersBean();
			lTimeAppedoQueuedOn = cbSlaEntry.getDateQueuedOn().getTime();
			
			hmSLAs = new HashMap<Long, Long>();
			hsUserIds = new HashSet<Long>();
			
			slaManager = new SlaManager();
			
			// inserts each breached counter(s) for the SLA into `so_threshold_breach_<user_id>`
			for(int i = 0; i < jaBreachedCounters.size(); i = i + 1) {
				joBreachedCounter = jaBreachedCounters.getJSONObject(i);
				lUserId = joBreachedCounter.getLong("userid");
				lSLAId = joBreachedCounter.getLong("slaid");
				
				// Alter older Agent's SLA result, as per current requirement
				if( ! joBreachedCounter.containsKey("warning_threshold_value") ) {
					joBreachedCounter.put("critical_threshold_value", joBreachedCounter.get("thresholdvalue"));
					joBreachedCounter.put("warning_threshold_value", -1);
					joBreachedCounter.put("breached_severity", "CRITICAL");
					
					joBreachedCounter.remove("thresholdvalue");
					
					LogManager.infoLog("@devops, Client has to upgrade his Agent - User-Id: "+joBreachedCounter.getLong("userid")+" <> GUID: "+joBreachedCounter.getString("guid"));
				}
				
				// Get SLA settings
				jaSLACounters = (new SlaDBI()).getConfigCounters(conPC, lUserId, lSLAId);
				
				// Confirm whether SLA is active
				if( jaSLACounters.size() > 0 ) {
					// inserts `so_threshold_breach_<user_id>`
					collectorDBI.insertSlaBreachTable(conPC, lUserId, joBreachedCounter, lTimeAppedoQueuedOn);
					
					/*
					 * to check the counter(s) has breached, based on configured SLA settings, say Max Try Duration, Alert Trigger Frequency, Min. breach count; 
					 * the breached counter(s)'s SLA ids added, since to check a `sla_id`'s mapped counter(s) all are satisfy breached condition
					 */
					//hmSLAs.put(lSLAId, lUserId);
					
					/*
					 * Collect distinct User-Ids, to update Private-Counters
					 */
					hsUserIds.add(lUserId);
				}
			}
			
			Iterator<Long> iterUserIds = hsUserIds.iterator();
			while ( iterUserIds.hasNext() ) {
				lUserId = iterUserIds.next();
				
				// To update user_pvt_counters as oad_breached for the user.
				collectorDBI.updateOADBreached(conPC, lUserId);
			}
			
			// check whether all the SLA's mapped counter(s) are satisfying breach condition
			//Commented below code because separate service implemented to insert breached data to sla_alert_log table. 
			/*for (Map.Entry<Long, Long> entry : hmSLAs.entrySet()) {
				lSLAId = entry.getKey();
				lUserId = entry.getValue();
				
				slaManager.checkSLABreach(lUserId, lSLAId, lTimeAppedoQueuedOn);
			}*/
		} catch(Throwable th) {
			LogManager.errorLog(th);
			if( th.getMessage().contains("JSONObject[\"slaid\"]") ) {
				LogManager.errorLog(jaBreachedCounters.toString());
			}
		} finally {
			DataBaseManager.close(conPC);
			conPC = null;
			
			UtilsFactory.clearCollectionHieracy(jaBreachedCounters);
			jaBreachedCounters = null;
			UtilsFactory.clearCollectionHieracy(joBreachedCounter);
			joBreachedCounter = null;
			UtilsFactory.clearCollectionHieracy(jaSLACounters);
			jaSLACounters = null;
		}
	}
	

	@Override
	protected void finalize() throws Throwable {
		//System.out.println("SlaCollectorThreadController :  finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}
