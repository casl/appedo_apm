package com.appedo.slacollector.manager;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.appedo.slacollector.common.Constants;

public class LogManager {
	static Logger errorLog = null;
	static Logger infoLog = null;
	
	
	public static void initializePropertyConfigurator() {

		PropertyConfigurator.configure(Constants.LOG4J_PROPERTIES_FILE);
		errorLog = Logger.getLogger("errorLogger");
		infoLog = Logger.getLogger("infoLogger");
	}
	
	/**
	 * To write given exception string into error.log
	 * 
	 * @param strException
	 */
	public static void errorLog(String strException) {
		
		errorLog.error("Exception: "+strException);
		
//		System.out.println("Exception: "+strException);
	}
	
	/**
	 * to log error's into error.log
	 * @param e
	 */
	public static void errorLog(Throwable th) {
		
		StringWriter stack = new StringWriter();
		PrintWriter pw = new PrintWriter(stack);
		
		try {
			th.printStackTrace(pw);
			errorLog.error(stack.toString());
			//System.out.println("SLACollector: "+stack.toString());
		}finally {
			pw = null;
			stack = null;
		}
	}
	
	public static void infoLog(String strInfo) {
		infoLog.info(strInfo);
		//System.out.println("SLACollector: "+strInfo);
	}
	
	
}
