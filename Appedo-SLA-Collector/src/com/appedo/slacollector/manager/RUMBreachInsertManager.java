package com.appedo.slacollector.manager;

import java.sql.Connection;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.RUMCollectorBean;
import com.appedo.slacollector.dbi.SlaCollectorDBI;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author Navin
 *
 */
public class RUMBreachInsertManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected SlaCollectorDBI collectorDBI = null;
	
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public RUMBreachInsertManager() throws Exception {
		collectorDBI = new SlaCollectorDBI();
		
		establishDBonnection();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @throws Exception
	 */
	public void fetchBean(RUMCollectorBean cbRUMBreachEntry) throws Exception {
		JSONObject joBreachedData = null;
		
		long lUserId = -1L, lSLAId = -1L, lTimeAppedoQueuedOn = -1L;
		
		RUMManager rumManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			conPC = DataBaseManager.reEstablishConnection(conPC);
			
			joBreachedData = cbRUMBreachEntry.getBreachDataBean();
			// ignored SLA received on time
			//lTimeAppedoQueuedOn = cbRUMBreachEntry.getDateQueuedOn().getTime();
			
			// collector received on
			lTimeAppedoQueuedOn = joBreachedData.getLong("appedo_received_on");
			lUserId = joBreachedData.getLong("userId");
			lSLAId = joBreachedData.getLong("slaId");
			
			// inserts uid's RUM breach to `so_rum_threshold_breach_<user_id>`
			collectorDBI.insertRUMBreach(conPC, lUserId, joBreachedData, lTimeAppedoQueuedOn);
			
			rumManager = new RUMManager();
			
			// check the SLA's has satisfy breached condition, for Alert
			rumManager.checkSLABreach(lUserId, lSLAId, joBreachedData, lTimeAppedoQueuedOn);
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			//System.out.println("SlaCollectorThreadController :  CLOSE " );
			DataBaseManager.close(conPC);
			conPC = null;
			
			rumManager = null;
		}
	}
	

	@Override
	protected void finalize() throws Throwable {
		//System.out.println("SlaCollectorThreadController :  finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}
