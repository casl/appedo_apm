package com.appedo.slacollector.manager;

import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.AVMCollectorBean;
import com.appedo.slacollector.bean.RUMCollectorBean;
import com.appedo.slacollector.bean.SUMCollectorBean;
import com.appedo.slacollector.bean.SlaCollectorBean;
import com.appedo.slacollector.utils.UtilsFactory;

/**
 * Manager which holds the queues, of all the performance counters
 * Singleton class. So only one instance can be created.
 * 
 * @author veeru
 *
 */
public class CollectorManager{
	
	// Singleton object, used globally with static getCollectorManager().
	private static CollectorManager collectorManager = new CollectorManager();
	
	// Queue to store performance counter data of agents
	private PriorityBlockingQueue<SlaCollectorBean> pqModulePerformanceSla = null;
	private PriorityBlockingQueue<SUMCollectorBean> pqModulePerformanceSUM = null;
	private PriorityBlockingQueue<AVMCollectorBean> pqAVMSLABreach = null;
	private PriorityBlockingQueue<RUMCollectorBean> pqModulePerformanceRUM = null;
	
	// Queue to store slave update details
	private PriorityBlockingQueue<String> pqSlaveDetails = new PriorityBlockingQueue<String>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 */
	private CollectorManager() {
		
		try {
			// initialize all required queue in this private constructor.
			pqModulePerformanceSla = new PriorityBlockingQueue<SlaCollectorBean>();
			pqModulePerformanceSUM = new PriorityBlockingQueue<SUMCollectorBean>();
			pqAVMSLABreach = new PriorityBlockingQueue<AVMCollectorBean>();
			pqModulePerformanceRUM = new PriorityBlockingQueue<RUMCollectorBean>();
			
			// initialize all required queue in this private constructor.
			pqSlaveDetails = new PriorityBlockingQueue<String>();
			
		} catch(Exception ex) {
			LogManager.errorLog(ex);
		}
	}
	
	
	/**
	 * Access the only[singleton] CollectorManager object.
	 * 
	 * @return CollectorManager
	 */
	public static CollectorManager getCollectorManager(){
		return collectorManager;
	}

	
	/**
	 * Queue the given string  into the asked Family queue.
	 * 
	 * @param strSlaveDetails
	 * @return
	 * @throws Exception
	 */
	public boolean collectSlaveDetails(String strSlaveDetails) throws Exception {
		
		try {
			return pqSlaveDetails.add(strSlaveDetails);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public String pollSlaveDetails(){
		String strEntry = null;
		
		try {
			strEntry = pqSlaveDetails.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
		
		return strEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getSlaveDetailsQueueLength(){
		return pqSlaveDetails.size();
	}
	
	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectSlaBean(SlaCollectorBean collBean) throws Exception {
		try {
			return pqModulePerformanceSla.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public SlaCollectorBean pollSlaBean(){
		SlaCollectorBean cbRumEntry = null;
		
		try {
			cbRumEntry = pqModulePerformanceSla.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
		
		return cbRumEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getSlaQueueLength(){
		return pqModulePerformanceSla.size();
	}
	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectSUMBean(SUMCollectorBean collBean) throws Exception {
		try {
			return pqModulePerformanceSUM.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public SUMCollectorBean pollSUMBean(){
		SUMCollectorBean cbRumEntry = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			cbRumEntry = pqModulePerformanceSUM.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally{
			LogManager.logMethodEnd(dateLog);
		}
		
		return cbRumEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getSUMQueueLength(){
		return pqModulePerformanceSUM.size();
	}
	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectAVMBreaches(AVMCollectorBean collBean) throws Exception {
		try {
			return pqAVMSLABreach.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public AVMCollectorBean pollAVMBean(){
		AVMCollectorBean cbRumEntry = null;
		Date dateLog = LogManager.logMethodStart();
		
		try {
			cbRumEntry = pqAVMSLABreach.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally{
			LogManager.logMethodEnd(dateLog);
		}
		
		return cbRumEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getAVMQueueLength(){
		return pqAVMSLABreach.size();
	}
	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectRUMBreachBean(RUMCollectorBean collBean) throws Exception {
		try {
			return pqModulePerformanceRUM.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	/**
	 * Poll the top RUM breach CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public RUMCollectorBean pollRUMBreachBean(){
		RUMCollectorBean cbRUMEntry = null;
		
		try {
			cbRUMEntry = pqModulePerformanceRUM.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
		
		return cbRUMEntry;
	}
	
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getRUMBreachesQueueLength(){
		return pqModulePerformanceRUM.size();
	}
	
	
	@Override
	/**
	 * Before destroying clear the objects. To prevent from MemoryLeak.
	 */
	protected void finalize() throws Throwable {
		UtilsFactory.clearCollectionHieracy(pqModulePerformanceSla);
		
		super.finalize();
	}
}