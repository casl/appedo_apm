package com.appedo.slacollector.manager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.dbi.SlaDBI;


public class SlaSlaveManager {
	public static ArrayList< PriorityBlockingQueue<String> > alSlaveDetailsQueues = null;
	
	public static void createAgentDetailsQueues() {
		PriorityBlockingQueue<String> pqSlaveDetails = null;
		alSlaveDetailsQueues = new ArrayList< PriorityBlockingQueue<String> >();
		
		for(int nLogTimerIndex=0; nLogTimerIndex<10; nLogTimerIndex++){
			pqSlaveDetails = new PriorityBlockingQueue<String>();
			alSlaveDetailsQueues.add(pqSlaveDetails);
		}
	}
	
	public static void agentLogQueue(String strAgentDetails){
		
		PriorityBlockingQueue<String> pqAgentDetails = alSlaveDetailsQueues.get( (int)Thread.currentThread().getId()%10 );
		synchronized ( pqAgentDetails ) {
			if( strAgentDetails != null ){
				pqAgentDetails.add(strAgentDetails);
			}
		}
	}
	
	/**
	 * this is to update the status of  node details table  
	 * where the status not received from the agent 
	 * @throws Throwable
	 */
	
	public void updateInActiveNodes(Connection con) throws Throwable {
		ResultSet rst = null;
		
		SlaDBI slaDBI = null;
		PreparedStatement pstmt = null;
		
		try {
	     	slaDBI = new SlaDBI();	     	
	     	slaDBI.updateNodeStatus(con);			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			// throw t;

			// re-establish the connection if it is disconnected.
			// this will keep on waiting for the Connection to get established.
			con = DataBaseManager.reEstablishConnection(con);
		}finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
		
		
	}



}
