package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.manager.AppedoMailer;
import com.appedo.commons.manager.AppedoMailer.MODULE_ID;
import com.appedo.manager.AppedoConstants;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.RUMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.controller.BreachRUMController;
import com.appedo.slacollector.utils.UtilsFactory;


public class RUMManager {
	
	
	public boolean collectRUMBreachData(String strRUMBreachData) throws Exception {
		RUMCollectorBean rumCollectorBean = null;
		
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		
		boolean bQueued = false;
		
		try {
			//if( strRUMBreachData != null ) {
			JSONObject  joRUMBreachData = JSONObject.fromObject(strRUMBreachData);
			
			rumCollectorBean = new RUMCollectorBean();
			rumCollectorBean.setBreachDataBean(joRUMBreachData);
			rumCollectorBean.setDateQueuedOn(new Date());			
			
			// add to Queue 
			bQueued = collectorManager.collectRUMBreachBean(rumCollectorBean);
			//}
		} catch(Exception e) {
			throw e;
		} finally {
			rumCollectorBean = null;
			collectorManager = null;
		}
		
		return bQueued;
	}
	
	/**
	 * checks SLA has breached with configured settings, 
	 *   if breaches inserts into `sla_alert_log_<user_id>`, alerts to user if user's alert cond. satisfy,
	 *   
	 * @param lUserId
	 * @param lSLAId
	 * @param joBreachedData
	 * @param lTimeAppedoQueuedOn
	 */
	public synchronized void checkSLABreach(long lUserId, long lSLAId, JSONObject joBreachedData, long lTimeAppedoQueuedOn) {
		Date dateLog = LogManager.logMethodStart();
		
		try {
			(new BreachRUMController(lUserId, lSLAId, joBreachedData, lTimeAppedoQueuedOn)).start();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally{
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * sends alerts to user, with configured alert email ids or mobile numbers
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSLAId
	 * @param joBreachedData
	 * @param lTimeAppedoQueuedOn
	 * @throws Exception
	 */
	public void sendAlerts(Connection con, long lUserID, long lSLAId, JSONObject joBreachedData, long lTimeAppedoQueuedOn) throws Throwable {
		String strSubject = null, strToEmail = "", strToSMS = "", strISDCode = "", strEmailContent = "", strSMSContent = "", strSMSBody = "", strMessage = "";
		
		boolean bMsgSent = false, bEmailAlert = false;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null, hmMessageDetails = null;
		// mail data
		HashMap<String, Object> hmMailDetails = null;
		
		AppedoMailer appedoMailer = null;
		AppedoMobileManager appedoMobileMan = null;
		
		SlaManager slaManager = null;
		long lAlertLogId = 0;
		
		try {
			appedoMailer = new AppedoMailer( Constants.EMAIL_TEMPLATES_PATH );
			
			slaManager = new SlaManager();
			hmMailDetails = new HashMap<String, Object>();
			
			// gets uid's alert message dynamic content details,
			hmMessageDetails = slaManager.getRUMAlertMessageDetails(con, joBreachedData.getLong("uid"));
			if ( hmMessageDetails == null ) {
				// would have been deleted
				throw new Exception("Uid not found, module has been deleted.");
			}
			
			hmMailDetails.put("moduleName", hmMessageDetails.get("moduleName"));
			hmMailDetails.put("url", joBreachedData.getString("url"));
			hmMailDetails.put("threshold_operator", joBreachedData.getBoolean("is_above_threshold") ? ">=" : "<=");
			// TODO RAM P1: breached_on should be the time, when data received by Collector. 
			//hmMailDetails.put("breached_on", (new Date()).getTime());
			hmMailDetails.put("breached_on", joBreachedData.getLong("appedo_received_on"));
			//hmMailDetails.put("breached_on", UtilsFactory.formatTimeStampToyyyyMMddHHmmssS(lTimeAppedoQueuedOn));
			// converts to sec
			hmMailDetails.put("received_value", joBreachedData.getLong("received_value") / 1000);
			hmMailDetails.put("breached_severity", joBreachedData.getString("breached_severity"));
			hmMailDetails.put("breached_severity_message", "Your website have crossed its threshold limit.");
			if ( joBreachedData.getString("breached_severity").equals("WARNING") ) {
				hmMailDetails.put("threshold_value", joBreachedData.getInt("warning_threshold_value") / 1000);	// converts to sec
				hmMailDetails.put("breached_severity_subject", "Warning");
				hmMailDetails.put("severity_color", "ORANGE");
			} else {
				hmMailDetails.put("threshold_value", joBreachedData.getInt("critical_threshold_value") / 1000);	// converts to sec
				hmMailDetails.put("breached_severity_subject", "Critical");
				hmMailDetails.put("severity_color", "RED");
			}
			
			// get user's alert to addresses of email ids & mobile numbers, with SLA details
			alAlertAddresses = slaManager.getUserAlertToAddresses(con, lUserID, "");
			for (int i = 0; i < alAlertAddresses.size(); i = i + 1) {
				hmAlertAddress = alAlertAddresses.get(i);
				
				bEmailAlert = hmAlertAddress.get("alertType").equals("Email");
				strMessage = "RUM \""+hmMessageDetails.get("moduleName")+"\" is breached.";
				
				if( bEmailAlert ) {
					//strEmailContent = hmAlertAddress.get("emailBody");
					strEmailContent = strMessage;
					strToEmail = hmAlertAddress.get("emailMobile");
					strSMSContent = "";
					strToSMS = "";
					strISDCode = "";
				} else {
					strEmailContent = "";
					strToEmail = "";
					//strSMSContent = hmAlertAddress.get("smsText");
					strSMSContent = strMessage;
					strISDCode = hmAlertAddress.get("telephoneCode");
					strToSMS = strISDCode+hmAlertAddress.get("emailMobile");
				}
				// inserts into `sla_alert_log_<user_id>`
				lAlertLogId = slaManager.insertSLAAlertLog(con, lUserID, lSLAId, lTimeAppedoQueuedOn, strEmailContent, strToEmail, true, bMsgSent, strSMSContent, strToSMS);
				
				
				// sends msg to user, email OR SMS
				if ( bEmailAlert ) {
					strSubject = "FYA: {{ appln_heading }} RUM Alert - {{moduleName}} : {{breached_severity_subject}}";
					bMsgSent = appedoMailer.sendMail(MODULE_ID.RUM_ALERT_EMAIL, hmMailDetails, strToEmail.split(","), strSubject);
				} else if ( strToSMS != null && strToSMS.length() > 0 ) {
					appedoMobileMan = new AppedoMobileManager();
					
					// MSG: Alert from Appedo! RUM "<RUM_NAME>" has breached its limit of 2 sec., by 10 sec.
					strSMSBody = "Alert from "+AppedoConstants.getAppedoWhiteLabel("download_appln_name_camelcase")+"! RUM \""+hmMessageDetails.get("moduleName")+"\" has breached its limit of "+hmMailDetails.get("threshold_value")+" sec., by "+hmMailDetails.get("received_value")+" sec.";
					bMsgSent = appedoMobileMan.sendSMS(strISDCode, strToSMS, strSMSBody.trim(), lAlertLogId);
				}
			}
			
		} catch(Throwable th) {
			throw th;
		} finally {
			strSubject = null;
			strToEmail = null;
			strToSMS = null;
			strEmailContent = null;
			strSMSContent = null;
			strSMSBody = null;
			strMessage = null;
			
			UtilsFactory.clearCollectionHieracy(alAlertAddresses);
			alAlertAddresses = null;
			
			UtilsFactory.clearCollectionHieracy(hmMessageDetails);
			hmMessageDetails = null;
			
			UtilsFactory.clearCollectionHieracy(hmMailDetails);
			hmMailDetails = null;
			
			appedoMailer = null;
			appedoMobileMan = null;
			slaManager = null;
		}
	}
}
