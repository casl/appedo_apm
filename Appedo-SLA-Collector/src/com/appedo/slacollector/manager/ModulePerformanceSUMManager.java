package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.util.Date;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SUMCollectorBean;
import com.appedo.slacollector.controller.BreachSUMController;
import com.appedo.slacollector.dbi.SlaCollectorDBI;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author veeru
 *
 */
public class ModulePerformanceSUMManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected SlaCollectorDBI collectorDBI = null;
	
	protected CollectorManager collectorManager = null;
	
	
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public ModulePerformanceSUMManager() throws Exception {
		collectorDBI = new SlaCollectorDBI();
		
		establishDBonnection();
		
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @throws Exception
	 */
	public void fetchBean(SUMCollectorBean cbSUMEntry) throws Exception {
		Date dateLog = LogManager.logMethodStart();
		JSONObject joBreachCounters = null;
		long lTimeAppedoQueuedOn = 0;
		
		try{
			// if connection is not established to db server then wait for 10 seconds
			conPC = DataBaseManager.reEstablishConnection(conPC);
			
			joBreachCounters = cbSUMEntry.getJoCountersBean();
			lTimeAppedoQueuedOn = joBreachCounters.getLong("appedoReceivedOn");
			
			collectorDBI.insertSUMBreachTable(conPC, joBreachCounters, lTimeAppedoQueuedOn);
			
			// Check breach count
			chkSUMBreach(joBreachCounters, lTimeAppedoQueuedOn);
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(conPC);
			conPC = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public synchronized void chkSUMBreach(JSONObject joBreachCounters, long lTimeAppedoQueuedOn) {
		Date dateLog = LogManager.logMethodStart();
		BreachSUMController breachSUMController = null;
		
		try{
			breachSUMController = new BreachSUMController(joBreachCounters, lTimeAppedoQueuedOn);
			breachSUMController.start();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally{
			LogManager.logMethodEnd(dateLog);
		}
	}
	

	@Override
	protected void finalize() throws Throwable {
		//System.out.println("SlaCollectorThreadController :  finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}
