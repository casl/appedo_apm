package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.sql.Statement;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.dbi.SlaCollectorDBI;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author veeru
 *
 */
public class SlaveDetailsManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected SlaCollectorDBI collectorDBI = null;
	
	protected CollectorManager collectorManager = null;
	
	
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public SlaveDetailsManager() throws Exception {
		collectorDBI = new SlaCollectorDBI();
		
		establishDBonnection();
		
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			System.out.println("SlaSlaveDetailsThreadController  : OPEN " );
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @throws Exception
	 */
	public void fetchBean(String  strSlaveEntry) throws Exception {
		Statement stmt = null;
		try{
			// if connection is not established to db server then wait for 10 seconds
			conPC = DataBaseManager.reEstablishConnection(conPC);
			
			//collectorDBI.insertSlaBreachTable(conPC, strSlaveEntry);
			if( strSlaveEntry != null ){
				stmt = conPC.createStatement();
				stmt.executeUpdate(strSlaveEntry);
			}
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			System.out.println("SlaSlaveDetailsThreadController  : CLOSE " );
			DataBaseManager.close(conPC);
			conPC = null;
		}
	}
	

	@Override
	protected void finalize() throws Throwable {
		System.out.println("SlaSlaveDetailsThreadController  : finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}
