package com.appedo.slacollector.manager;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.manager.AppedoMailer;
import com.appedo.commons.manager.AppedoMailer.MODULE_ID;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.AVMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.utils.UtilsFactory;

public class AVMBreachManager {

	
	public boolean collectAVMBreaches(String strBreachCounterSet) throws Exception {
		AVMCollectorBean avmBreachBean = null;
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		
		boolean bQueued = false;
		
		try {
			if( strBreachCounterSet != null ) {
				JSONObject joBreachCounters = JSONObject.fromObject(strBreachCounterSet);
				
				avmBreachBean = new AVMCollectorBean();
				avmBreachBean.setJoCountersBean(joBreachCounters);
				avmBreachBean.setDateQueuedOn(new Date());
				
				// add to Queue
				bQueued = collectorManager.collectAVMBreaches(avmBreachBean);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			avmBreachBean = null;
			collectorManager = null;
		}
		
		return bQueued;
	}
	
	/**
	 * 
	 * @param con
	 * @param lSlaid
	 * @return
	 * @throws Exception
	 */
	public void sendAlerts(Connection con, JSONObject joAVMBreachDetails, long lTimeAppedoQueuedOn) throws Throwable {
		Date dateLog = LogManager.logMethodStart();
		
		JSONObject joAVMDetails = null;
		
		String strMobile = null, strTestName = null, strLocationText = "", strAlertType = null;
		String strToEmail = "", strToSMS = "", strISDCode ="", strEmailContent = "", strSMSContent = "", strSubject = "", strSMSBody = "", strMessage = "";
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		HashMap<String, Object> hmMailDetails =  null;
		
		boolean bEmailAlert = false, bMsgSent = false;
		
		long lSLAId = -1L, lUserId = -1L, lAlertLogId = 0;
		
		SlaManager slaManager = null;
		SlaCollectorDBI slaCollectorDBI = null;
		
		AppedoMailer appedoMailer = null;
		AppedoMobileManager appedoMobileMan = null;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			slaManager = new SlaManager();
			
			appedoMailer = new AppedoMailer( Constants.EMAIL_TEMPLATES_PATH );
			appedoMobileMan = new AppedoMobileManager();
			
			lSLAId = joAVMBreachDetails.getLong("sla_id");
			lUserId = joAVMBreachDetails.getLong("userid");
			
			// gets AVM's test details 
			joAVMDetails = slaCollectorDBI.getAVMTestDetails(con, joAVMBreachDetails.getLong("avm_test_id"));
			
			strTestName = joAVMDetails.getString("avm_test_name");
			
			/* if((joDetails.getDouble("received_value")==0 && joDetails.getBoolean("is_Down"))){
				sb = getDownTimeCounter(joDetails, joAVMDetails);
			}else{
				sb = getCounterNames(joDetails, joAVMDetails);	
			} */
			
			// 
			/* { "severity_color": "RED", "testname": "ERP Website", "breached_severity": "AGENT_DOWN", "url": "http://www.appedo.com/", "location": "India - Chennai" }*/
			hmMailDetails = new HashMap<String, Object>();
			hmMailDetails.put("url", joAVMDetails.getString("url"));
			hmMailDetails.put("testname", strTestName);
			hmMailDetails.put("severity_color", "RED");
			hmMailDetails.put("breached_severity", joAVMBreachDetails.getString("breached_severity"));
			
			if ( joAVMBreachDetails.getString("breached_severity").equals("WEBSITE_UNREACHABLE") ) {
				hmMailDetails.put("breached_severity_message", "Website is Unreachable.");
			} else if ( joAVMBreachDetails.getString("breached_severity").equals("AGENT_UNREACHABLE") ) {
				hmMailDetails.put("breached_severity_message", "Agent is Unreachable.");
			} else if ( joAVMBreachDetails.getString("breached_severity").equals("AGENT_DOWN") ) {
				hmMailDetails.put("breached_severity_message", "Agent is Unreachable.");
			}

		    
			strLocationText = getLocationText(joAVMBreachDetails);
			hmMailDetails.put("location", strLocationText);
			
			// gets AVM agent's mapped to alert address 
			alAlertAddresses = slaManager.getAVMAgentAlertToAddresses(con, joAVMBreachDetails.getString("guid"));
			if ( alAlertAddresses.size() == 0 ) {
				// gets SLA's to alert address, for AVM agent has no alert address mapped 
				alAlertAddresses = slaManager.getUserAlertToAddresses(con, lUserId, "");
			}
			
			for (int i = 0; i < alAlertAddresses.size(); i = i + 1) {
				hmAlertAddress = alAlertAddresses.get(i);
				
				// `equalsIgnoreCase` added since `avm_agent_alert_mapping` has `alertType=EMAIL`, `so_alert` has `alertType=Email`; DON'T change `equalsIgnoreCase`
				bEmailAlert = hmAlertAddress.get("alertType").equalsIgnoreCase("EMAIL");
				strMessage = "AVM Alert! "+joAVMBreachDetails.getString("breached_severity")+"\nTest Name: "+strTestName+"\nLocation: "+strLocationText;
				
				if( bEmailAlert ) {	// for email
					//strEmailContent = hmAlertAddress.get("emailBody");
					strEmailContent = strMessage;
					strToEmail = hmAlertAddress.get("emailMobile");
					strSMSContent = "";
					strToSMS = "";
					strISDCode ="";
				} else {	// for SMS
					strEmailContent = "";
					strToEmail = "";
					strSMSContent = strMessage;
					strISDCode = hmAlertAddress.get("telephoneCode");
					strToSMS = strISDCode+hmAlertAddress.get("emailMobile");
				}
				// inserts into `sla_alert_log_<user_id>`
				lAlertLogId = slaManager.insertSLAAlertLog(con, lUserId, lSLAId, lTimeAppedoQueuedOn, strEmailContent, strToEmail, true, bMsgSent, strSMSContent, strToSMS);
				
				
				// sends msg to user, email OR SMS
				if ( bEmailAlert ) {
					strSubject = "FYA: {{ appln_heading }} AVM Alert - {{testname}} : {{breached_severity}}";
					bMsgSent = appedoMailer.sendMail(MODULE_ID.AVM_ALERT_EMAIL, hmMailDetails, strToEmail.split(","), strSubject);
				} else if ( strToSMS != null && strToSMS.length() > 0 ) {
					//appedoMobileMan = new AppedoMobileManager();
					
					//strSMSBody = "Alert from "+AppedoConstants.getAppedoWhiteLabel("download_appln_name_camelcase")+"! "+strCountersMsgMobileAlert+".";
					bMsgSent = appedoMobileMan.sendSMS(strISDCode, strToSMS, strMessage.trim(), lAlertLogId);
				}
			}
			
		} catch(Throwable th) {
			throw th;
		} finally {
			UtilsFactory.clearCollectionHieracy(joAVMDetails);
			joAVMDetails = null;
			
			UtilsFactory.clearCollectionHieracy(alAlertAddresses);
			alAlertAddresses = null;
			
			UtilsFactory.clearCollectionHieracy(hmMailDetails);
			hmMailDetails = null;
			
			
			slaCollectorDBI = null;
			slaManager = null;
			appedoMailer = null;
			appedoMobileMan = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public static String getLocationText(JSONObject joAVMBreachDetails) {
		StringBuilder sbLocation = new StringBuilder();
		
		sbLocation.append( joAVMBreachDetails.getString("country") );
		if( joAVMBreachDetails.getString("state").length() > 0 ) {
			sbLocation.append(" - ").append( joAVMBreachDetails.getString("state") );
		}
		sbLocation.append(" - ").append( joAVMBreachDetails.getString("city") );
		if( joAVMBreachDetails.getString("region").length() > 0 || joAVMBreachDetails.getString("zone").length() > 0 ) {
			sbLocation.append(" ( ");
			
			if( joAVMBreachDetails.getString("region").length() > 0 ) {
				sbLocation.append( joAVMBreachDetails.getString("region") );
			}
			if( joAVMBreachDetails.getString("region").length() > 0 && joAVMBreachDetails.getString("zone").length() > 0 ) {
				sbLocation.append(" - ");
			}
			if( joAVMBreachDetails.getString("zone").length() > 0 ) {
				sbLocation.append( joAVMBreachDetails.getString("zone") );
			}
			
			sbLocation.append(" )");
		}
		
		return sbLocation.toString();
	}
}
