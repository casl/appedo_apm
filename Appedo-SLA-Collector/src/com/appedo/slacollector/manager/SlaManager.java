package com.appedo.slacollector.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.manager.AppedoMailer;
import com.appedo.commons.manager.AppedoMailer.MODULE_ID;
import com.appedo.manager.AppedoConstants;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SlaCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.controller.BreachController;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.dbi.SlaDBI;
import com.appedo.slacollector.utils.UtilsFactory;

public class SlaManager {
	
	private static final int BUFFER_SIZE = 4096;
	public static ArrayList< PriorityBlockingQueue<SlaCollectorBean> > alSlaLogQueues = null;
	
	public static void createSlaLogQueues() {
		PriorityBlockingQueue<SlaCollectorBean> pqRumLogBeans = null;
		alSlaLogQueues = new ArrayList< PriorityBlockingQueue<SlaCollectorBean> >();
		
		for(int nLogTimerIndex=0; nLogTimerIndex<1; nLogTimerIndex++){
			pqRumLogBeans = new PriorityBlockingQueue<SlaCollectorBean>();
			alSlaLogQueues.add(pqRumLogBeans);
		}
	}
	
	public static void slaLogQueue(SlaCollectorBean rumLogBean){
		
		PriorityBlockingQueue<SlaCollectorBean> pqRumLogBeans = alSlaLogQueues.get( (int)Thread.currentThread().getId()%1 );
		synchronized ( pqRumLogBeans ) {
			pqRumLogBeans.add(rumLogBean);
		}
	}
	
	/**
	 * Send SMS if the SLA is not sent in SMS for last Alert-Every-Period.
	 * 
	 * Validate whether the breached SLA details are sent to user, through Email. If not, send a Consolidated-SLA-Alert-Email to the user.
	 * 
	 * @param con
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public void sendAlerts(Connection con, long lUserID, Long lSlaId, long lAlertLogId, int nAlertInMin) throws Throwable {
		String strToSMS = null, strISDCode = null, strSMSBody = null, strCountersMsgMobileAlert = null;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		
		boolean bSLAPolicyAlerted = false;
		
		AppedoMobileManager appedoMobileMan = null;
		
		try{
			// Whether alerted in last "Alert-In-Min" duration
			bSLAPolicyAlerted = isSLAAlerted(con, lUserID, lSlaId, nAlertInMin, "SMS");
			
			// send SMS, for individual SLA-Policy
			if( ! bSLAPolicyAlerted ) {
				
				alAlertAddresses = getUserAlertToAddresses(con, lUserID, "SMS");
				
				if( alAlertAddresses.size() > 0 ) {
					appedoMobileMan = new AppedoMobileManager();
					
					strCountersMsgMobileAlert = getCounterNamesForMobileAlert(con, lUserID, lSlaId);
					strSMSBody = "Alert from "+AppedoConstants.getAppedoWhiteLabel("download_appln_name_camelcase")+"! "+strCountersMsgMobileAlert+".";
				}
				
				for (int i = 0; i < alAlertAddresses.size(); i = i + 1) {
					hmAlertAddress = alAlertAddresses.get(i);
					
					strISDCode = hmAlertAddress.get("telephoneCode");
					strToSMS = hmAlertAddress.get("telephoneCode")+hmAlertAddress.get("emailMobile");
					
					// Send SMS
					appedoMobileMan.sendSMS(strISDCode, strToSMS, strSMSBody, lAlertLogId);
					
					// Update that SMS is sent.
					updateSMSAlertedSLALog(con, lUserID, strToSMS, strSMSBody, lAlertLogId);
				}
			}
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	/**
	 * Send SMS if the SLA is not sent in SMS for last Alert-Every-Period.
	 * 
	 * Validate whether the breached SLA details are sent to user, through Email. If not, send a Consolidated-SLA-Alert-Email to the user.
	 * 
	 * @param con
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public void sendLogAlerts(Connection con, long lUserID, Long lSlaId, long lAlertLogId, int nAlertInMin) throws Throwable {
		String strToSMS = null, strISDCode = null, strSMSBody = null, strCountersMsgMobileAlert = null;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		
		boolean bSLAPolicyAlerted = false;
		
		AppedoMobileManager appedoMobileMan = null;
		
		try{
			// Whether alerted in last "Alert-In-Min" duration
			bSLAPolicyAlerted = isSLAAlerted(con, lUserID, lSlaId, nAlertInMin, "SMS");
			
			// send SMS, for individual SLA-Policy
			if( ! bSLAPolicyAlerted ) {
				
				alAlertAddresses = getUserAlertToAddresses(con, lUserID, "SMS");
				
				if( alAlertAddresses.size() > 0 ) {
					appedoMobileMan = new AppedoMobileManager();
					 
					strCountersMsgMobileAlert = getLogNamesForMobileAlert(con, lUserID, lSlaId);
					strSMSBody = "Alert from "+AppedoConstants.getAppedoWhiteLabel("download_appln_name_camelcase")+"! "+strCountersMsgMobileAlert+".";
				}
				
				for (int i = 0; i < alAlertAddresses.size(); i = i + 1) {
					hmAlertAddress = alAlertAddresses.get(i);
					
					strISDCode = hmAlertAddress.get("telephoneCode");
					strToSMS = hmAlertAddress.get("telephoneCode")+hmAlertAddress.get("emailMobile");
					
					// Send SMS
					appedoMobileMan.sendSMS(strISDCode, strToSMS, strSMSBody, lAlertLogId);
					
					// Update that SMS is sent.
					updateSMSAlertedSLALog(con, lUserID, strToSMS, strSMSBody, lAlertLogId);
				}
			}
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	/***
	 * To get configured email & mobile numbers
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 */
	/*public String getToEmailMobile(long lUserID, long lSlaID) {
		String strReturn = null;
		SlaDBI slaDBI = new SlaDBI();
		 Date dateLog = LogManager.logMethodStart();
		try {
			strReturn = slaDBI.getToEmailMobile(lUserID, lSlaID);
			
		}catch(Exception e) {
			LogManager.errorLog(e);
		}finally {
			slaDBI = null;
			 LogManager.logMethodEnd(dateLog);
		}
		
		return strReturn;
	}*/
	
	/**
	 * To send email alert to configured mail id's when action performed
	 * @param con
	 * @param lSlaid
	 * @return
	 * @throws Exception
	 */
	public boolean sendActionEmailAlert(Connection con, long lUserID, long lSlaid,String strLogText) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		String strSubject = null;
		String strAlertType = null;
		StringBuilder sbQuery = new StringBuilder();
		
		HashMap<String, Object> hmMailDetails =  null;
		
		boolean bSend = false;
		
		AppedoMailer appedoMailer = null;
		
		try{
			appedoMailer = new AppedoMailer( Constants.EMAIL_TEMPLATES_PATH );
			
			sbQuery .append("select a.user_id, sla_name as subject, b.alert_type, b.email_mobile from so_sla a inner join so_alert b on a.user_id =b.user_id and is_valid=true and a.sla_id=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaid);
			rst = pstmt.executeQuery();
			while(rst.next()){
				
				hmMailDetails = new HashMap<String, Object>();
				hmMailDetails.put("USERNAME", " ");
				
				// email subject
				strSubject = rst.getString("subject");
				
				// Email body
				hmMailDetails.put("BODY", strSubject+" "+ strLogText);
				strAlertType = rst.getString("alert_type");
				if(strAlertType.equalsIgnoreCase("email")) {
					strSubject = "FYA: {{ appln_heading }}: SLA Alert";
					
					appedoMailer.sendMail(MODULE_ID.SLA_ALERT_EMAIL, hmMailDetails, rst.getString("email_mobile").split(","), strSubject);
				}
			}
		} catch(Throwable th) {
			throw th;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			sbQuery = null;
			hmMailDetails =  null;
		}
		return bSend;
	}
	
	/**
	 * to get the breach counternames
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 */
	public StringBuilder getBreachedCounterNames(Connection con, long lUserID, long lSlaID) throws Exception {
		SlaCollectorDBI slaCollectorDBI = null;
		
		StringBuilder sb = new StringBuilder();
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			sb = slaCollectorDBI.getBreachedCounterNames(con, lUserID, lSlaID);
		}catch(Exception e) {
			throw e;
		}finally {
			slaCollectorDBI = null;
		}
		
		return sb;
	}
	
	/**
	 * To update SLA alert table
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSlaid
	 * @param strMsgBody
	 * @param strEmailIds
	 */
	public long insertSLAAlertLog(Connection con, long lUserID, long lSlaid, long lTimeAppedoQueuedOn, String strMsgBody, String strToEmail, boolean bIsSent, boolean bIsSuccess, String strSmsContent, String strSms) throws Throwable {
		SlaCollectorDBI slaCollectorDBI = null;
		long lAlertLogId = 0;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			lAlertLogId = slaCollectorDBI.insertSLAAlertLog(con, lUserID, lSlaid, lTimeAppedoQueuedOn, strMsgBody, strSmsContent, strToEmail, strSms, bIsSent, bIsSuccess);
		} catch(Throwable th) {
			throw th;
		}
		
		return lAlertLogId;
	}
	
	/**
	 * Add entry in <b>sla_alert_log_&lt;USER-ID&gt;</b> table, when the whole SLA-Policy is breached.
	 * This update will happen with <b>blank</b> email-id & mobile number; with "is_sent" as false.
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSlaid
	 * @param lTimeAppedoQueuedOn
	 * @return
	 * @throws Throwable
	 */
	public long insertSLAAlertLog(Connection con, long lUserID, long lSlaid, long lTimeAppedoQueuedOn) throws Throwable {
		SlaCollectorDBI slaCollectorDBI = null;
		long lAlertLogId = 0;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			
			lAlertLogId = slaCollectorDBI.insertSLAAlertLog(con, lUserID, lSlaid, lTimeAppedoQueuedOn);
			
			slaCollectorDBI = null;
		} catch(Throwable th) {
			throw th;
		}
		
		return lAlertLogId;
	}
	
	
	/**
	 * gets SLAs active counter(s)
	 * 
	 * @param strGUIDs
	 * @return
	 * @throws Exception
	 */
	public String getActiveSLAsCounters(Connection con, String strGUIDs) throws Exception {
		SlaCollectorDBI slaCollectorDBI = null;
		
		HashMap<String, Object> hmGUIDsMappedToSLAs = null;
		
		String[] saGUIDs = null;
		String strRtnGUIDsSLACounters = null, strFormattedGUIDs = "";
		
		ArrayList<JSONObject> alBlank = new ArrayList<JSONObject>();
		
		try{
			slaCollectorDBI = new SlaCollectorDBI();
			
			// format adds quote
			strFormattedGUIDs = "'"+strGUIDs.replaceAll(",", "','")+"'";
			
			// gets GUID(s) active SLA(s)
			hmGUIDsMappedToSLAs = slaCollectorDBI.getActiveSLAsCounters(con, strFormattedGUIDs);
			
			// adds empty [] for guid(s) doesn't has SLA(s)
			saGUIDs = strGUIDs.split(",");
			for (String strGUID : saGUIDs) {
				
				if ( ! hmGUIDsMappedToSLAs.containsKey(strGUID) ) {
					hmGUIDsMappedToSLAs.put(strGUID, alBlank);
				}
			}
			
			// HashMap to JSON format
			strRtnGUIDsSLACounters = UtilsFactory.getJSONSuccessReturn(hmGUIDsMappedToSLAs).toString();
		} catch(Exception e){
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy(hmGUIDsMappedToSLAs);
			hmGUIDsMappedToSLAs = null;
			
			strFormattedGUIDs = null;
			slaCollectorDBI = null;
		}
		
		return strRtnGUIDsSLACounters;
	}
	
	/**
	 * gets SLAs active counter(s)
	 * 
	 * @param strGUIDs
	 * @return
	 * @throws Exception
	 */
	public String getActiveSLAsCountersV2(Connection con, String strGUIDs) throws Exception {
		SlaCollectorDBI slaCollectorDBI = null;
		
		HashMap<String, Object> hmGUIDsMappedToSLAs = null;
		
		String[] saGUIDs = null;
		String strRtnGUIDsSLACounters = null, strFormattedGUIDs = "";
		
		ArrayList<JSONObject> alBlank = new ArrayList<JSONObject>();
		
		try{
			slaCollectorDBI = new SlaCollectorDBI();
			
			// format adds quote
			strFormattedGUIDs = "'"+strGUIDs.replaceAll(",", "','")+"'";
			
			// gets GUID(s) active SLA(s)
			hmGUIDsMappedToSLAs = slaCollectorDBI.getActiveSLAsCountersV2(con, strFormattedGUIDs);
			
			// adds empty [] for guid(s) doesn't has SLA(s)
			saGUIDs = strGUIDs.split(",");
			for (String strGUID : saGUIDs) {
				
				if ( ! hmGUIDsMappedToSLAs.containsKey(strGUID) ) {
					hmGUIDsMappedToSLAs.put(strGUID, alBlank);
				}
			}
			
			// HashMap to JSON format
			strRtnGUIDsSLACounters = UtilsFactory.getJSONSuccessReturn(hmGUIDsMappedToSLAs).toString();
		} catch(Exception e){
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy(hmGUIDsMappedToSLAs);
			hmGUIDsMappedToSLAs = null;
			
			strFormattedGUIDs = null;
			slaCollectorDBI = null;
		}
		
		return strRtnGUIDsSLACounters;
	}
	
	
	/**
	 * 
	 * @param strGUID
	 * @param strBreachCounterSet
	 * @return
	 * @throws Exception
	 */
	public boolean collectBreachCounters(String strGUID, String strBreachCounterSet) throws Exception {
		SlaCollectorBean slaBean = null;
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		boolean bQueued = false;
		
		try {
			if( strBreachCounterSet != null ) {
				JSONArray jaBreachCounters = JSONArray.fromObject(strBreachCounterSet);
				
				slaBean = new SlaCollectorBean();
				slaBean.setCountersBean(jaBreachCounters);
				slaBean.setDateQueuedOn(new Date());
				
				// add to Queue
				bQueued = collectorManager.collectSlaBean(slaBean);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			slaBean = null;
			collectorManager = null;
		}
		
		return bQueued;
	}
	
	/**
	 * 
	 * @param lUserID
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public JSONArray getLatestBreachSummary(Connection con, long lUserID, long lSlaId) throws Exception {
		JSONArray jaCounters = null;
		SlaCollectorDBI slaCollectorDBI = null;
		
		try{
			slaCollectorDBI = new SlaCollectorDBI();
			
			jaCounters = slaCollectorDBI.getLatestBreachSummary(con, lUserID, lSlaId);
			
			slaCollectorDBI = null;
		} catch(Exception e) {
			throw e;
		}
		
		return jaCounters;
	}
	
	/**
	 * 
	 * @param lUserID
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public String getCounterNamesForMobileAlert(Connection con, long lUserID, long lSlaId) throws Exception {
		JSONArray jaCounters = null;
		SlaCollectorDBI slaCollectorDBI = null;
		StringBuilder sb = new StringBuilder();
		String strCounters = null;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			
			jaCounters = slaCollectorDBI.getLatestBreachSummary(con, lUserID, lSlaId);
			
			for (int i = 0; i < jaCounters.size(); i++) {
				JSONObject joCounter = jaCounters.getJSONObject(i);
				if (i > 0) {
					sb.append(",\n");
				}
				sb.append(joCounter.getString("module_code"))
					.append(" :: ").append(joCounter.getString("module_detail_name"))
					.append(" : ").append(joCounter.getString("category"))
					.append(" : ").append(joCounter.getString("counter_name"))
					.append(" : ").append(joCounter.getLong("received_value"))
					.append(" ").append(joCounter.getString("breached_unit"))
					.append(" ").append(joCounter.getString("threshold_operator"))
					.append(" ").append(joCounter.getLong("threshold_value"))
					.append(" ").append(joCounter.getString("threshold_unit"));
			}
			UtilsFactory.replaceAll(sb, "&gt;", ">");
			UtilsFactory.replaceAll(sb, "&lt;", "<");
			
			while( sb.indexOf("  ") > 0 ) {
				UtilsFactory.replaceAll(sb, "  ", " ");
			}
			
			strCounters = sb.toString();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			slaCollectorDBI = null;
			UtilsFactory.clearCollectionHieracy(sb);
			sb = null;
		}
		return strCounters;
	}
	
	/**
	 * 
	 * @param lUserID
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public String getLogNamesForMobileAlert(Connection con, long lUserID, long lSlaId) throws Exception {
		JSONArray jaCounters = null;
		SlaCollectorDBI slaCollectorDBI = null;
		StringBuilder sb = new StringBuilder();
		String strCounters = null;
		
		try {
			slaCollectorDBI = new SlaCollectorDBI();
			
			jaCounters = slaCollectorDBI.getLatestBreachSummaryForLog(con, lUserID, lSlaId);
			
			for (int i = 0; i < jaCounters.size(); i++) {
				JSONObject joCounter = jaCounters.getJSONObject(i);
				if (i > 0) {
					sb.append(",\n");
				}
				sb.append(joCounter.getString("module_code"))
					.append(" :: ").append(joCounter.getString("module_detail_name"))
					.append(" : ").append(joCounter.getString("grok_column"));

				if(joCounter.getBoolean("is_value_based")) {
					sb  .append(" : ").append(joCounter.getString("breached_severity"))
						.append(" : ").append(joCounter.getLong("received_value"))
						.append(" ").append(joCounter.getString("threshold_operator"))
						.append(" ").append(joCounter.getLong("threshold_value"));
				}else {
					sb  .append(" : ").append(joCounter.getString("breach_pattern"))
						.append(" : ").append(joCounter.getString("breached_severity"));
				}
			}
			UtilsFactory.replaceAll(sb, "&gt;", ">");
			UtilsFactory.replaceAll(sb, "&lt;", "<");
			
			while( sb.indexOf("  ") > 0 ) {
				UtilsFactory.replaceAll(sb, "  ", " ");
			}
			
			strCounters = sb.toString();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			slaCollectorDBI = null;
			UtilsFactory.clearCollectionHieracy(sb);
			sb = null;
		}
		return strCounters;
	}
	
	/**
	 * to read the header of stream data
	 * @param stream
	 * @return
	 * @throws Throwable
	 */
	
	public String readHeader(InputStream stream) throws Throwable {
		
		StringBuffer instr = null;
		int c;
		try	{
			instr = new StringBuffer();
			while (true ) {
				c=stream.read();
				instr.append( (char) c);
				if(instr.toString().endsWith("\r\n\r\n") )
					break;
			}
		}catch(Throwable t)	{
			throw t;
		}
		return instr.toString();
	}
	/**
	 * To get the operation 
	 * @param strHeader
	 * @return
	 */
	public String getOperation(String strHeader) {
		String strOperation = null;
		try {
			Pattern p = Pattern.compile("(.*): ([0-9]*)");
	        Matcher m = p.matcher(strHeader);
	        
	        if(m.find()) {
	        	strOperation = m.group(1);
	        }
			
		}catch(Exception e) {
			System.out.println("Exception in getOperation() :" + e.getMessage());
		}
		return strOperation;
	}
	/**
	 * To get the body length of operation 
	 * @param strHeader
	 * @return
	 */
	public int getBodyLength(String strHeader) {
		int nBodyLength = 0;
		try {
			Pattern p = Pattern.compile("(.*): ([0-9]*)");
	        Matcher m = p.matcher(strHeader);
	        
	        if(m.find()) {
	        	nBodyLength = Integer.parseInt(m.group(2));
	        }
			
		}catch(Exception e) {
			System.out.println("Exception in getBodyLength() :" + e.getMessage());
		}
		return nBodyLength;	
	}
	
	/**
	 * to get the header tokens
	 * @param strHeader
	 * @return
	 */
	public HashMap<String, String> getHeadTokens(String strHeader) {
		HashMap<String, String> hm = new HashMap<String, String>();
		try {
			
			Pattern p = Pattern.compile("(.*)= (.*)\r\n");
	        Matcher m = p.matcher(strHeader);
	        while(m.find()) {
	            hm.put(m.group(1), m.group(2));
	        }
			
		}catch(Exception e) {
			System.out.println("Exception in getHeadTokens :" + e.getMessage());
		}
		
		return hm;
	}
	/**
	 * 
	 * @param errorStream
	 * @return
	 */
	public String getErrorString(InputStream errorStream) {
		InputStreamReader isrError = null;
		BufferedReader rError = null;
		String line = null;
		StringBuilder sbError = new StringBuilder();
		
		try{
			isrError = new InputStreamReader(errorStream);
			rError = new BufferedReader(isrError);
			sbError.setLength(0);
			while ((line = rError.readLine()) != null) {
				sbError.append(line).append(" ");
			}
			if( sbError.length() > 0 ){
				sbError.deleteCharAt(sbError.length()-1);
				
				System.out.println("sbError in CPU: "+sbError);
			}
		} catch ( Exception e ) {
			System.out.println("Exception in getErrorString: "+e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				isrError.close();
			} catch(Exception e) {
				System.out.println("Exception in isrError.close(): "+e.getMessage());
				e.printStackTrace();
			}
			isrError = null;
			try{
				rError.close();
			} catch(Exception e) {
				System.out.println("Exception in rError.destroy(): "+e.getMessage());
				e.printStackTrace();
			}
			rError = null;
		}
		
		return sbError.toString();
	}
	/**
	 * get default settings of sla
	 * @param lUserID
	 * @return
	 */
	/*public JSONObject getSlaSettings(long lUserID) {
		Date dateLog = LogManager.logMethodStart();
		SlaDBI slaDBI = new SlaDBI();
		JSONObject joSlaSettings = new JSONObject();
		
		try {
			joSlaSettings = slaDBI.getSlaSettings(lUserID);
		} catch(Exception e) {
			LogManager.errorLog(e);
			
		} finally {
			slaDBI = null;
			LogManager.logMethodEnd(dateLog);
		}
		
		return joSlaSettings;
		
	}*/
	
	/**
	 * gets user's SLA alert settings 
	 * 
	 * @param con
	 * @param lUserID
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Integer> getSlaSettings(Connection con, long lUserID) throws Exception {
		SlaDBI slaDBI = null;

		HashMap<String, Integer> hmSLASettings = null;
		
		try {
			slaDBI = new SlaDBI();
			
			hmSLASettings = slaDBI.getSlaSettings(con, lUserID);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return hmSLASettings;
	}
	
	/**
	 * Checks SLA has alerted, within alert duration in SLA alert settings
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSlaID
	 * @param nTriggerAlertFreq
	 * @return
	 * @throws Exception
	 */
	public boolean isSLAAlerted(Connection con, long lUserID, long lSlaID, int nTriggerAlertFreq, String strAlertType) throws Exception {
		SlaDBI slaDBI = null;

		boolean bAlerted = false;
		
		try {
			slaDBI = new SlaDBI();
			
			bAlerted = slaDBI.isSLAAlerted(con, lUserID, lSlaID, nTriggerAlertFreq, strAlertType);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return bAlerted;
	}
	
	public void updateSMSAlertedSLALog(Connection con, long lUserId, String strMobileNumber, String strSentContent, long lAlertLogId) throws Exception {
		SlaCollectorDBI slaDBI = null;
		
		try {
			slaDBI = new SlaCollectorDBI();
			
			slaDBI.updateSMSSentDetail(con, lUserId, strMobileNumber, strSentContent, lAlertLogId);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * 
	 * @param lUserID
	 * @param lSlaID
	 * @param nTriggerAlertFreq
	 * @return
	 */
	/*public boolean isTriggerHealAlertFrq(long lUserID, long lSlaID, int nTriggerAlertFreq) {
		SlaDBI slaDBI = new SlaDBI();
		boolean bReturn = false;
		try {
			bReturn = slaDBI.isTriggerHealAlertFrq(lUserID, lSlaID, nTriggerAlertFreq);
		}catch(Exception e) {
			LogManager.errorLog(e);
		}finally {
			slaDBI = null;
		}
		return bReturn;
		
	}*/
	
	/**
	  * To export a file to agent location 
	  * @param filePath
	  * @throws Throwable
	  */
	 public void uploadLogFile(String strActionLogFile) throws Throwable {
		 
		 OutputStream outputStream = null;
		 FileInputStream inputStream = null;
		 BufferedReader reader = null;
		 
		 try {
			// takes file path from input parameter
			File uploadFile = new File(Constants.ACTION_LOG_FILE_FOLDER+File.separator+strActionLogFile);
			 
			LogManager.infoLog("File to upload: " + Constants.ACTION_LOG_FILE_FOLDER+File.separator+strActionLogFile);
			
			LogManager.infoLog("File to upload: " + Constants.UPLOAD_URL);
			// creates a HTTP connection
	        URL url = new URL(Constants.UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST");
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("fileName", uploadFile.getName());
	        httpConn.setRequestProperty("command", "UPLOAD");
	        
	        // opens output stream of the HTTP connection for writing data
	        outputStream = httpConn.getOutputStream();
	        
	        // Opens input stream of the file for reading data
	        inputStream = new FileInputStream(uploadFile);
	        
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        
	        LogManager.infoLog("Start writing data...");
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	        	outputStream.write(buffer, 0, bytesRead);
		    }
	        
	        LogManager.infoLog("Data was written.");
	        
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	        	// reads server's response
	        	reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
	        	String response = reader.readLine();
	        	
	        	LogManager.infoLog("Got response from the hitted URL. Response: "+response);
		    } else {
		    	LogManager.errorLog("Server returned non-OK code: " + responseCode);
		    }
	        
		 }catch(Throwable t) {
			 LogManager.errorLog(t);
			 throw t;
		 }finally {
			 if(reader!=null)
				 reader.close();
			 if(inputStream!=null) 
				 inputStream.close();
			 if(outputStream!=null)
				 outputStream.close();
		 }
	}
	 /**
	  * 
	  * @param strActionLogFile
	  * @param logContent
	  * @return
	  */
	 public boolean createSlaLog(String strActionLogFile, String logContent) {
			boolean bReturn = true;
			File fScript = null;
			FileWriter fw = null;
			BufferedWriter bw = null;
			try {
				fScript = new File(Constants.ACTION_LOG_FILE_FOLDER+File.separator+strActionLogFile);
				if (!fScript.exists()) {
					fScript.createNewFile();
					//set application user permissions to 777
					fScript.setExecutable(true);
					fScript.setReadable(true);
					fScript.setWritable(true);
					//System.out.println("File created");
				}
				
				fw = new FileWriter(fScript.getAbsoluteFile());
				bw = new BufferedWriter(fw);
				bw.write(logContent);
				bw.close();
				fw.close();
				//System.out.println("File content written");
				LogManager.infoLog("File content written");
				
			}catch(Throwable e) {
				bReturn = false;
				LogManager.errorLog(e);
				//System.out.println("Exception in createSlaLog() :" + e.getMessage());
			}
			return bReturn;	
		 
	 }
	 
	 /**
	  * Check whether all counters, configured in given SLA-Policy, are breached.
	  * 
	  * @param joSlaSettings
	  * @param lUserID
	  * @param lSlaID
	  * @return
	  */
	 public boolean hasSlaBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID) throws Exception {
		 boolean bReturn;
		 SlaDBI slaDBI = null;
		 try {
			 slaDBI = new SlaDBI();
			 bReturn = slaDBI.hasSlaBreached(con, hmSLASettings, lUserID, lSlaID);
		 }catch(Exception e) {
			 bReturn = false;
			 throw e;
		 }finally {
			 slaDBI = null; 
		 }
		 return bReturn;
		 
	 }
	 
	 /**
	  * To check the given sla of configured counters are breached
	  * 
	  * @param joSlaSettings
	  * @param lUserID
	  * @param lSlaID
	  * @return
	  */
	public boolean isSUMBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID, long lminBrchCount, String strLocation) throws Exception {
		Date dateLog = LogManager.logMethodStart();
		boolean bReturn;
		SlaDBI slaDBI = null;
		
		try {
			slaDBI = new SlaDBI();
			bReturn = slaDBI.isSUMBreached(con, hmSLASettings, lUserID, lSlaID, lminBrchCount, strLocation);
		} catch(Exception e) {
			bReturn = false;
		} finally {
			slaDBI = null; 
			LogManager.logMethodEnd(dateLog);
		}
		 
		return bReturn; 
	}
	 
	 /**
	  * To check the given sla of configured counters are breached
	  * 
	  * @param hmSlaSettings
	  * @param lUserID
	  * @param lSlaID
	  * @return
	  */
	public boolean isAVMBreached(Connection con, HashMap<String, Integer> hmSlaSettings, long lUserID, long lSlaID, long lminBrchCount, String strCountry, String strState, String strCity, String strRegion, String strZone) {
		Date dateLog = LogManager.logMethodStart();
		boolean bReturn;
		SlaDBI slaDBI = null;
		
		try {
			slaDBI = new SlaDBI();
			bReturn = slaDBI.isAVMBreached(con, hmSlaSettings, lUserID, lSlaID, lminBrchCount, strCountry, strState, strCity, strRegion, strZone);
		} catch(Exception e) {
			bReturn = false;
			throw e;
		} finally {
			slaDBI = null; 
			LogManager.logMethodEnd(dateLog);
		}
		 
		return bReturn; 
	}
	
	/**
	 * checks uid's RUM SLA has breached, based on SLA settings 
	 * 
	 * @param con
	 * @param hmSLASettings
	 * @param lUserID
	 * @param lSlaID
	 * @param nMinBreachCount
	 * @return
	 * @throws Exception
	 */
	public boolean isRUMBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID, int nMinBreachCount) throws Exception {
		SlaDBI slaDBI = null;
		
		boolean bSLABreached = false;
		
		try {
			slaDBI = new SlaDBI();
			
			bSLABreached = slaDBI.isRUMBreached(con, hmSLASettings, lUserID, lSlaID, nMinBreachCount);
			
			slaDBI = null;
		}catch(Exception e) {
			throw e;
		}
		 
		return bSLABreached; 
	}
	
	/**
	 * check for breach, the SLA's mapped counter(s) all breached, based on user SLA settings
	 * checks breach and alert(s) the user,
	 * 
	 * @param lUserId
	 * @param lSLAId
	 */
	public synchronized void checkSLABreach(long lUserId, long lSLAId, long lTimeAppedoQueuedOn) {
		Date dateLog = LogManager.logMethodStart();
		
		try {
			(new BreachController(lUserId, lSLAId, lTimeAppedoQueuedOn)).start();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally{
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * Gets user's alert configured email_ids & mobile numbers.
	 * If strAlertType = Email then return only EmailIds,
	 * If strAlertType = SMS then return only Mobile-Numbers,
	 * If strAlertType = "" then return all EmailIds and Mobile-Numbers.
	 * 
	 * @param con
	 * @param lSLAId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<HashMap<String, String>> getUserAlertToAddresses(Connection con, long lUserId, String strAlertType) throws Exception {
		SlaDBI slaDBI = null;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		
		try {
			slaDBI = new SlaDBI();
			
			alAlertAddresses = slaDBI.getUserAlertToAddresses(con, lUserId, strAlertType);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return alAlertAddresses;
	}
	
	/**
	 * gets user's alert configured email_ids & mobile numbers
	 * 
	 * @param con
	 * @param lSLAId
	 * @return
	 * @throws Exception
	 *
	public ArrayList<HashMap<String, String>> getUserAlertToAddresses(Connection con, long lSLAId) throws Exception {
		SlaDBI slaDBI = null;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		
		try {
			slaDBI = new SlaDBI();
			
			alAlertAddresses = slaDBI.getUserAlertToAddresses(con, lSLAId);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return alAlertAddresses;
	}*/
	
	/**
	 * gets AVM agent's to alert addresses
	 * 
	 * @param con
	 * @param strGUID
	 * @return
	 * @throws Exception
	 */
	public ArrayList<HashMap<String, String>> getAVMAgentAlertToAddresses(Connection con, String strGUID) throws Exception {
		SlaDBI slaDBI = null;
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		
		try {
			slaDBI = new SlaDBI();
			
			alAlertAddresses = slaDBI.getAVMAgentAlertToAddresses(con, strGUID);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return alAlertAddresses;
	}
	
	/**
	 * gets Alert message dynamic content for the uid
	 * 
	 * @param con
	 * @param lUId
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> getRUMAlertMessageDetails(Connection con, long lUId) throws Exception {
		SlaDBI slaDBI = null;
		
		HashMap<String, String> hmMessageDetails = null;
		
		try {
			slaDBI = new SlaDBI();
			
			hmMessageDetails = slaDBI.getRUMAlertMessageDetails(con, lUId);
			
			slaDBI = null;
		} catch (Exception e) {
			throw e;
		}
		
		return hmMessageDetails;
	}
}
