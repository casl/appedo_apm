package com.appedo.slacollector.common;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import net.sf.json.JSONObject;

import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.utils.UtilsFactory;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author veeru
 *
 */
public class Constants {
	
	public static boolean DEV_ENVIRONMENT = false;
	
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	
	public static String APPEDO_SLA_COLLECTOR = "";
	
	public static String PATH = ""; 
	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";
	
	public static String SMTP_MAIL_CONFIG_FILE_PATH = "";
	public static String MOBILE_CONFIG_FILE_PATH = "";
	
	public static String EMAIL_TO_ALERT_FOR_SUM_AGENT = "";
	
	public static String UPLOAD_URL = "";
	
	public static String ACTION_LOG_FILE_FOLDER = "";
	
	public static long TIMER_PERIOD;
	
	public static int INTERVAL;
	
	public static long COUNTER_CONTROLLER_REST_MILLESECONDS = 1000;
	
	public static String EMAIL_TEMPLATES_PATH = "";
	
	public static Long ALERT_LOG_SERVICE_RUNTIME_INTERVAL_SEC = null;
	
	public static String DEFAULT_SLA_THREADPOOL_NAME = "";
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(srtConstantsPath);
			prop.load(is);
			
			// Appedo application's resource directory path
			RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
			
			APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
			
			APPEDO_SLA_COLLECTOR = prop.getProperty("APPEDO_SLA_COLLECTOR");
			
			PATH = RESOURCE_PATH+prop.getProperty("Path");		
			
			LOG4J_PROPERTIES_FILE = RESOURCE_PATH+prop.getProperty("LOG4J_CONFIG_FILE_PATH");
			
			SMTP_MAIL_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("SMTP_MAIL_CONFIG_FILE_PATH");
			MOBILE_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("MOBILE_CONFIG_FILE_PATH");
			ACTION_LOG_FILE_FOLDER = RESOURCE_PATH+prop.getProperty("ACTION_LOG_FILE_FOLDER");
			EMAIL_TEMPLATES_PATH = RESOURCE_PATH+prop.getProperty("EMAIL_TEMPLATES_PATH");
			
			INTERVAL = Integer.parseInt(prop.getProperty("queryinterval"));
			TIMER_PERIOD = Long.parseLong(prop.getProperty("node_status__check_period_in_sec"));
			
			DEFAULT_SLA_THREADPOOL_NAME = prop.getProperty("DEFAULT_SLA_THREADPOOL_NAME");

			COUNTER_CONTROLLER_REST_MILLESECONDS = Integer.parseInt(prop.getProperty("COUNTER_CONTROLLER_REST_MILLESECONDS"));
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			UtilsFactory.close(is);
			is = null;
		}	
	}
	
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		JSONObject joAppedoCollector = null;
		
		try {
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			if( prop.getProperty("ENVIRONMENT") != null && prop.getProperty("ENVIRONMENT").equals("DEVELOPMENT") 
					&& AppedoConstants.getAppedoConfigProperty("ENVIRONMENT") != null && AppedoConstants.getAppedoConfigProperty("ENVIRONMENT").equals("DEVELOPMENT") ) 
			{
				DEV_ENVIRONMENT = true;
			}
			
			UPLOAD_URL = getProperty("UPLOAD_URL", prop);
			EMAIL_TO_ALERT_FOR_SUM_AGENT = getProperty("EMAIL_TO_ALERT_FOR_SUM_AGENT", prop);
			ALERT_LOG_SERVICE_RUNTIME_INTERVAL_SEC = Long.parseLong( getProperty("ALERT_LOG_SERVICE_RUNTIME_INTERVAL_SEC", prop) );
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
			throw th;
		} finally {
			UtilsFactory.close(is);
			is = null;
			
			UtilsFactory.clearCollectionHieracy(joAppedoCollector);
			joAppedoCollector = null;
		}	
	}
	
	/**
	 * loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoConstants(Connection con) throws Throwable {
		
		try {
			AppedoConstants.getAppedoConstants().loadAppedoConstants(con);
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Get the property's value from appedo_config.properties, if it is DEV environment;
	 * Otherwise get the property's value from DB.
	 * 
	 * @param strPropertyName
	 * @param prop
	 * @return
	 */
	private static String getProperty(String strPropertyName, Properties prop) throws Throwable {
		if( DEV_ENVIRONMENT && prop.getProperty(strPropertyName) != null )
			return prop.getProperty(strPropertyName);
		else
			return AppedoConstants.getAppedoConfigProperty(strPropertyName);

	}
}