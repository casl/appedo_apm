package com.appedo.slacollector.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.AVMBreachManager;
import com.appedo.slacollector.manager.RUMManager;
import com.appedo.slacollector.manager.SUMManager;
import com.appedo.slacollector.manager.SlaManager;
import com.appedo.slacollector.utils.UtilsFactory;

/**
 * Servlet implementation class RumCollector
 */
public class SlaCollector extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SlaCollector() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request,response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request,response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) {
		
		String strGUIDs = request.getParameter("guid");
		String strCommand = request.getParameter("command");
		
		SlaManager slaManager = new SlaManager();
		SUMManager sumManager = new SUMManager();
		AVMBreachManager avmManager = new AVMBreachManager();
		RUMManager rumManager = null;
		
		switch(strCommand) {
			case "AgentFirstRequest" : {
				String strRtn = "";
				
				Connection con = null;
				
				try {
					con = DataBaseManager.giveConnection();
					
					strRtn = slaManager.getActiveSLAsCounters(con, strGUIDs);
					
				} catch (Exception e) {
					LogManager.errorLog(e);
					strRtn = UtilsFactory.getJSONFailureReturn(e.getMessage()).toString();
				} finally {
					DataBaseManager.close(con);
					con = null;
					
					try {
						response.getWriter().write(strRtn);
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				} 
				break;
			}
			case "UnifiedAgentFirstRequest" : {
				String strRtn = "";
				
				Connection con = null;
				
				try {
					con = DataBaseManager.giveConnection();
					
					strRtn = slaManager.getActiveSLAsCountersV2(con, strGUIDs);
					
				} catch (Exception e) {
					LogManager.errorLog(e);
					strRtn = UtilsFactory.getJSONFailureReturn(e.getMessage()).toString();
				} finally {
					DataBaseManager.close(con);
					con = null;
					
					try {
						response.getWriter().write(strRtn);
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				} 
				break;
			}
			case "sumBreachCounterSet" : {
				String strRtn = null;
				
				try {
					String strBreachCounterSet = request.getParameter("sumBreachCounterset");
					boolean bQueued = sumManager.collectSUMBreach(strBreachCounterSet);
					
					if(bQueued){
						strRtn = UtilsFactory.getJSONSuccessReturn("Successfully queued").toString();
					}else{
						strRtn = UtilsFactory.getJSONSuccessReturn("Unable to queue sumBreachCounterset").toString();
					}
				} catch (Exception e) {
					LogManager.errorLog(e);
					strRtn = UtilsFactory.getJSONFailureReturn(e.getMessage()).toString();
				} finally {
					sumManager = null;
					try {
						response.getWriter().write(strRtn);
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				}
				break;
			}
			case "collectAVMBreaches" : {
				String strRtn = null;
				try {
					String strBreachCounterSet = request.getParameter("avmBreachSet");
					boolean bQueued = avmManager.collectAVMBreaches(strBreachCounterSet);
					
					if(bQueued){
						strRtn = UtilsFactory.getJSONSuccessReturn("Successfully queued").toString();
					} else {
						strRtn = UtilsFactory.getJSONSuccessReturn("Unable to queue collectAVMBreaches").toString();
					}
				} catch (Exception e) {
					LogManager.errorLog(e);
					strRtn = UtilsFactory.getJSONFailureReturn(e.getMessage()).toString();
				} finally {
					try {
						response.getWriter().write(strRtn);
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				}
				break;
			}
			case "sumDownTimeAlert" : {
				Connection con = null;
				
				try {
					con = DataBaseManager.giveConnection();
					
					String strBreachCounterSet = request.getParameter("sumBreachCounterset");
					//System.out.println("sum request: "+strBreachCounterSet);
					
					sumManager.sendDownTimeAlerts(con, JSONObject.fromObject(strBreachCounterSet));
				} catch (Throwable th) {
					LogManager.errorLog(th);
				} finally {
					DataBaseManager.close(con);
					con = null;
				}
				break;
			}
			case "inActiveLocations" : {
				Connection con = null;
				
				try {
					con = DataBaseManager.giveConnection();
					
					String strInActiveLocations = request.getParameter("inActiveLocations");
					LogManager.infoLog("Sum Agent Alert request : "+strInActiveLocations);
					if(strInActiveLocations!=null) {
						JSONObject  joInActiveLocations = JSONObject.fromObject(strInActiveLocations);
						
						sumManager.sendAlertsDevOps(con, joInActiveLocations);
					}
				} catch (Throwable th) {
					LogManager.errorLog(th);
				} finally {
					DataBaseManager.close(con);
					con = null;
					
					sumManager = null;
				}
				break;
			}
			case "collectRUMUIDBreach": {
				String strRUMBreachData = "", strGUID = "";

				JSONObject joRtn = null;

				try {
					rumManager = new RUMManager();

					strRUMBreachData = request.getParameter("breach_data");
					strGUID = request.getParameter("guid");

					// collects RUM breach data
					rumManager.collectRUMBreachData(strRUMBreachData);
					joRtn = UtilsFactory.getJSONSuccessReturn("Queued in SLA Collector.");

				} catch (Exception e) {
					LogManager.errorLog(e);
					joRtn = UtilsFactory.getJSONFailureReturn("Unable to collect breach data.");
				} finally {
					strRUMBreachData = null;
					strGUID = null;
					
					rumManager = null;
					
					try {
						response.getWriter().write(joRtn.toString());
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				}
				break;
			}
			case "sendOTPCode" : {
				String strISDCode = "", strToSMS = "", strSMSBody = "", strRtn = null;;
				long lAlertLogId = -1L;
				AppedoMobileManager appedoMobileMan = null;
				try {
					appedoMobileMan = new AppedoMobileManager();
					strToSMS = request.getParameter("ToSMS");
					strSMSBody = request.getParameter("SMSBody");
					appedoMobileMan.sendSMS(strISDCode, strToSMS, strSMSBody, lAlertLogId);
					strRtn = "Sucessfully Message send..";
				} catch (Exception e) {
					LogManager.errorLog(e);
					strRtn = UtilsFactory.getJSONFailureReturn(e.getMessage()).toString();
				} finally {
					try {
						response.getWriter().write(strRtn);
					} catch (IOException e) {
						LogManager.errorLog(e);
					}
				} 
				break;
			}
			default : {
				try{
					String strBreachCounterSet = request.getParameter("slaBreachCounterset");
					String strGUID = request.getParameter("guid");
					slaManager.collectBreachCounters(strGUID, strBreachCounterSet);
				}catch(Exception e){
					LogManager.errorLog(e);
				}
			}
		}
	}
}
