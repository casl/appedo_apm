package com.appedo.slacollector.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.manager.AppedoMailer;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.controller.AVMCollectorThreadController;
import com.appedo.slacollector.controller.RUMCollectorThreadController;
import com.appedo.slacollector.controller.SUMCollectorThreadController;
import com.appedo.slacollector.controller.SlaCollectorThreadController;
import com.appedo.slacollector.controller.SlaSlaveDetailsThreadController;
import com.appedo.slacollector.timer.AlertLogTimerTask;
import com.appedo.slacollector.timer.SlaveTimerTask;
import com.appedo.slacollector.utils.TaskExecutor;

/**
 * Servlet to handle one operation for the whole application
 * @author navin
 *
 */
public class InitServlet extends HttpServlet {
	// set log access
	
	private static final long serialVersionUID = 1L;
	public static String realPath = null;	
	public static Timer timerRumLog = new Timer();
	public static TimerTask timerTaskSlaveInactive = null;
	public static Timer timerSlaveInactive = new Timer();
	
	public static TimerTask ttAlertLogService = null;
	public static Timer	timerAlertLogService = new Timer();
	
	public static ArrayList<Timer> alAllTimerInApplication = new ArrayList<Timer>();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		Connection con = null;
		
		//super();
		
		// declare servlet context
		ServletContext context = getServletContext();
		
		realPath = context.getRealPath("//");
		
		try {
			String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
			String strLog4jFilePath = context.getInitParameter("LOG4J_PROPERTIES_FILE_PATH");
			
			
			Constants.CONSTANTS_FILE_PATH = InitServlet.realPath+strConstantsFilePath;
			Constants.LOG4J_PROPERTIES_FILE = InitServlet.realPath+strLog4jFilePath;
			
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator(Constants.LOG4J_PROPERTIES_FILE);
			
			// Loads Constant properties
			Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-SLA-Collector", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			// Loads mail config
			AppedoMailer.loadPropertyFileConstants(Constants.SMTP_MAIL_CONFIG_FILE_PATH);
			
			// load mobile properties from file
			AppedoMobileManager.loadPropertyFileConstants(Constants.MOBILE_CONFIG_FILE_PATH);
			
			AppedoMobileManager.populateCountryISDCodes(con);
			
			
			// To insert the breach counters 
			for( int i=0; i<10; i++ ) {
				(new SlaCollectorThreadController()).start();
			}
			
			// To insert the breach counters 
			for( int i=0; i<10; i++ ) {
				(new SUMCollectorThreadController()).start();
			}
			
			// To insert the Availability Breach details
			for( int i = 0; i < 10; i++ ) {
				(new AVMCollectorThreadController()).start();
			}
			
			// To insert the RUM breaches
			for(int i = 0; i < 10; i++) {
				(new RUMCollectorThreadController()).start();
			}
			
			// To update slave status
			for( int i=0; i<10; i++ ) {
				(new SlaSlaveDetailsThreadController()).start();
			}
			
			// Check & update inactive slaves for  Every 5 minutes
			timerTaskSlaveInactive = new SlaveTimerTask();
			timerSlaveInactive.schedule(timerTaskSlaveInactive, 500, Constants.TIMER_PERIOD * 1000);			
			alAllTimerInApplication.add(timerSlaveInactive);
			
			TaskExecutor.newExecutor(Constants.DEFAULT_SLA_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue
			
			ttAlertLogService = new AlertLogTimerTask();
			timerAlertLogService.schedule(ttAlertLogService, 1000, Constants.ALERT_LOG_SERVICE_RUNTIME_INTERVAL_SEC * 1000);			
			alAllTimerInApplication.add(timerAlertLogService);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}
	
	public void aaa(){
		
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
