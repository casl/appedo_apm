package com.appedo.slacollector.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.manager.AppedoMailer;
import com.appedo.manager.AppedoMobileManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.common.Constants;


public class Controller extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Handles POST request
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * Handles GET request comes
	 */	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * Accessed in both GET and POSTrequests for the operations below, 
	 * 1. Loads agents latest build version
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		Connection con = null;
		
		response.setContentType("text/html");
		String strActionCommand = request.getRequestURI();
		
		if(strActionCommand.endsWith("/common/reloadConfigAndMailProperties")) {
			// to reload config and appedo_config properties 
			
			try {
				// Loads Constant properties
				Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
				
				con = DataBaseManager.giveConnection();
				
				// load appedo constants; say loads appedoWhiteLabels, 
				Constants.loadAppedoConstants(con);
				
				// Loads Appedo config properties from the system path
				Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
				
				
				// Loads mail config
				AppedoMailer.loadPropertyFileConstants(Constants.SMTP_MAIL_CONFIG_FILE_PATH);
				
				// load mobile properties from file
				AppedoMobileManager.loadPropertyFileConstants(Constants.MOBILE_CONFIG_FILE_PATH);
				
				response.getWriter().write("Loaded <B>Appedo-SLA-Collector</B>, config, appedo_config, mail, mobile properties & appedo whitelabels.");
			} catch (Throwable th) {
				LogManager.errorLog(th);
				response.getWriter().write("<B style=\"color: red; \">Exception occurred Appedo-SLA-Collector: "+th.getMessage()+"</B>");
			} finally {
				DataBaseManager.close(con);
				con = null;
			}
		}
	}
}
