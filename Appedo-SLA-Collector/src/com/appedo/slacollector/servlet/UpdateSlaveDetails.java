package com.appedo.slacollector.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.utils.UtilsFactory;


/**
 * Servlet implementation class UpdateSlaveDetails
 */
public class UpdateSlaveDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con = null;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateSlaveDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONObject joPostResponse = new JSONObject() ;
		
		try {
			synchronized ( joPostResponse ) {
				joPostResponse = doAction(request,response);	
			}
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			joPostResponse.put("success", false);
			joPostResponse.put("error", e.getMessage());
			
		}
		
		response.getWriter().print(joPostResponse.toString());
	}
	
	/**
	 * to handle the incoming messages which is coming from the slave
	 * @param request
	 * @param response
	 * @throws Throwable
	 */
	private JSONObject doAction(HttpServletRequest request, HttpServletResponse response) throws Throwable {
		
		JSONObject joNodeSummary = new JSONObject() ;
		JSONObject joActionResponse = new JSONObject() ;
		
		String strCommand = null;
		String strMsg = null;
		
		try {
			
			joActionResponse.put("success", true);
			strCommand = request.getHeader("command").trim().toUpperCase();
			strMsg = request.getHeader("msg").trim();
			if(strMsg.trim()!=null && strMsg.trim().length()>0 && strMsg.trim().startsWith("{") && strMsg.trim().endsWith("}")) {
				strMsg = JSONObject.fromObject(strMsg).toString().trim();
				joNodeSummary = JSONObject.fromObject(strMsg);
			}else {
//				System.out.println("veeru found :" + strCommand );
			}
			
//			System.out.println(strCommand);
//			System.out.println(strMsg);
			switch(strCommand) {
			
				case "ISVALIDUSER" : {
					
					int nSlaveUserId = 0;
					
					try {
						con = DataBaseManager.giveConnection();
						// decrypt user id
						nSlaveUserId = getUserId(con, joNodeSummary.getString("slaveuserid"));
						if(nSlaveUserId>0)
							joActionResponse.put("isvaliduser", true);
						else
							joActionResponse.put("isvaliduser", false);
					}catch(Exception e) {
						LogManager.errorLog(e);
					}
					break;
				}
				
				case "SLAVEDETAILS" : {
					
					if(joNodeSummary.getBoolean("success")) {
						con = DataBaseManager.giveConnection();
						int nUserId = getUserId(con, joNodeSummary.getString("slaveuserid"));
						// if Node already exists then update its new details
						// otherwise add the Node
						Boolean bIsSlavexist = isSlaveExist(con, nUserId, joNodeSummary.getString("mac") );
						if(bIsSlavexist) {// update node details
							updateSlaveDetails(nUserId, joNodeSummary);
						}else {// insert node details
							addSlaveDetails(nUserId, joNodeSummary);
						}
					}
					break;
				}
				
				case "SLAVESTATUS" : {
					updateSlaveStatus(joNodeSummary);
					break;
				}
			}
		} catch(Throwable t) {
			LogManager.errorLog(t);
			joActionResponse.put("success", false);
			joActionResponse.put("error", t.getMessage());
			throw t;
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			strCommand = null;
			strMsg = null;
		}
		return joActionResponse;
	}
	
	/***
	 * to get node user id 
	 * @param strEnyUserId
	 * @return
	 * @throws Exception
	 */
	public int getUserId(Connection con, String strEncryUserId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		int nUserId = 0;
		String strQry = "select  user_id  from usermaster where encrypted_user_id='"+strEncryUserId+"'";
		
		try {
			pstmt = con.prepareStatement(strQry);
			rst = pstmt.executeQuery();
			while (rst.next()) {
				if( rst.getString(1) !=null )
					nUserId = Integer.parseInt(rst.getString(1));
				else
					nUserId = 0;
			}
		} catch (Exception e) {
			LogManager.infoLog("sbQuery : " + strQry);
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			strQry = null;
		}
		
		return nUserId;
	}
	public Boolean isSlaveExist(Connection con, int strSumUserId, String strMac) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		Boolean bool = false;
		String strQry = "select slave_id from sla_slave_status where mac_address='"+strMac+"'";
		try {
			
			pstmt = con.prepareStatement(strQry);
			rst = pstmt.executeQuery();
			while (rst.next()) {
				
				if(rst.getString(1)!=null)
					bool = true;
				else
					bool = false;
			}
		}catch (Exception e) {
			LogManager.infoLog("sbQuery : " + strQry);
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			strQry = null;
		}
		
		return bool;
	}
	/**
	 * to add details of node to node_details table
	 * @param strQry
	 * @throws Throwable
	 */
	public void addSlaveDetails(int nUserId, JSONObject joNodeSummary) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		CollectorManager collectorManager = null;
		try {
			sbQuery	.append("insert into sla_slave_status(slave_user_id,sla_slave_status ,sla_slave_version,mac_address,os_type,operating_system,os_version,created_by,remarks,ipaddress,created_on) values (")
					.append(nUserId).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("status"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("slave_version"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("mac"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("os_type"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("operating_system"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("os_version"))).append(",")
					.append(nUserId).append(",")
					//.append(joNodeSummary.getString("Is_active")).append(",'")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("remarks"))).append(",")
					.append(UtilsFactory.makeValidVarchar(joNodeSummary.getString("ipaddress")))
					.append(", now()  )");
			
			//SlaSlaveManager.agentLogQueue(sbQuery.toString());
			collectorManager = CollectorManager.getCollectorManager();
			collectorManager.collectSlaveDetails(sbQuery.toString());
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
			
			throw t;
		} finally {
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	/***
	 * to update node details
	 * @param joUpdateSummary
	 * @param sbQuery
	 * @throws Exception
	 */
	
	public void updateSlaveDetails(int nUserId, JSONObject joUpdateSummary) throws Exception {
		
		StringBuilder sbQuery = new StringBuilder();
		CollectorManager collectorManager = null;
		try {
			//sb.append("update sum_node_details set AgentType = ? , IpAddress=? , City=?, State=?, Country=?, Lat=?, Lon=?, Selenium_version=?,")
			//.append("Jre_version=? , Firebug_version=? , Netexport_version=? , Os_type=? , operating_system=? , os_version=?, chrome_version=? , is_active=? ,remarks=? ,modified_by=? , modified_on=now()" );
			sbQuery	.append("UPDATE sla_slave_status SET agent_type = ")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("agent_type"))).append(", os_type = ")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("os_type"))).append(", operating_system = ")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("operating_system"))).append(", os_version = ")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("os_version"))).append(", remarks = ")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("remarks"))).append(", modified_by = ")
					.append(nUserId).append(", modified_on = now() WHERE mac_address =")
					.append(UtilsFactory.makeValidVarchar(joUpdateSummary.getString("mac"))).append(" AND slave_user_id = ")
					.append(nUserId);
			
			collectorManager = CollectorManager.getCollectorManager();
			collectorManager.collectSlaveDetails(sbQuery.toString());
			//SlaSlaveManager.agentLogQueue(sbQuery.toString());
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	/**
	 * to update agent status
	 * @param joNodeStatus
	 * @throws Exception
	 */
	public void updateSlaveStatus(JSONObject joNodeStatus) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		CollectorManager collectorManager = null;
		try {
			// System.out.println(" test :"+ joNodeStatus.toString());
			if(joNodeStatus.getString("status").equalsIgnoreCase("error")) {
				sbQuery	.append("update sla_slave_status SET sla_slave_status = ")
				        .append(UtilsFactory.makeValidVarchar(joNodeStatus.getString("status")))
				        .append(", last_error_on=now(), remarks = ")
				        .append(UtilsFactory.makeValidVarchar(joNodeStatus.getString("remarks")))
				        .append(" , modified_on=now() ")
				        .append("where mac_address = ").append(UtilsFactory.makeValidVarchar(joNodeStatus.getString("mac")));
			}else {
				sbQuery	.append("update sla_slave_status SET sla_slave_status = ")
						.append(UtilsFactory.makeValidVarchar(joNodeStatus.getString("status")))
						.append(", modified_on = now() ")
						.append("where mac_address = ").append(UtilsFactory.makeValidVarchar(joNodeStatus.getString("mac")));
			}
			collectorManager = CollectorManager.getCollectorManager();
			collectorManager.collectSlaveDetails(sbQuery.toString());
			//SlaSlaveManager.agentLogQueue(sbQuery.toString());
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}

}
