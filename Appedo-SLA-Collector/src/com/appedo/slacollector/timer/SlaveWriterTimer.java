package com.appedo.slacollector.timer;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SlaveDetailsManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Veeru
 *
 */
public class SlaveWriterTimer extends Thread {
	
	String strSlaveEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public SlaveWriterTimer(String strSlaveEntry) {
		this.strSlaveEntry = strSlaveEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting Module Perf.Counter timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new SlaveDetailsManager()).fetchBean(strSlaveEntry);
			
//			Thread.currentThread().interrupt();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Module Perf.Counter thread destroyed "+lThisThreadId);
		
		super.finalize();
	}
}
