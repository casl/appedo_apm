package com.appedo.slacollector.timer;

import java.sql.Connection;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SlaSlaveManager;




public class SlaveTimerTask extends TimerTask {
	private Connection conPC = null;
	
	public SlaveTimerTask(){
		//System.out.println("SlaveTimerTask : OPEN");
		this.conPC = DataBaseManager.giveConnection();
	}
	
	@Override
	public void run() {
		//System.out.println("SlaveTimerTask : run()");
		try{
			// if connection is not established to db server then wait for 10 seconds
			conPC = DataBaseManager.reEstablishConnection(conPC);
			
			new SlaSlaveManager().updateInActiveNodes(conPC);
			
			//System.out.println("SlaveTimerTask : VEEru");
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			//System.out.println("SlaveTimerTask : CLOSE");
			DataBaseManager.close(conPC);
			conPC = null;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("SlaveTimerTask : finalize");
		//System.out.println("Node inactiavting Thread is stopping");
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}

}