package com.appedo.slacollector.timer;

import java.util.Date;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.AVMCollectorBean;
import com.appedo.slacollector.manager.AVMInsertManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Veeru
 *
 */
public class AVMWriterTimer extends Thread {
	
	AVMCollectorBean cbAVMEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public AVMWriterTimer(AVMCollectorBean cbAVMEntry) {
		this.cbAVMEntry = cbAVMEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting AVM Breach db insert Timer as "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new AVMInsertManager()).fetchBean(cbAVMEntry);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
		} finally {
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("AVM Breach db insert thread destroyed "+lThisThreadId);
		
		super.finalize();
	}
}
