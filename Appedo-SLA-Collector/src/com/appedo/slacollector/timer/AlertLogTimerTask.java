package com.appedo.slacollector.timer;

import java.sql.Connection;
import java.util.TimerTask;

import net.sf.json.JSONArray;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.controller.AlertLogThreadController;
import com.appedo.slacollector.dbi.AlertLogServiceDBI;
import com.appedo.slacollector.utils.TaskExecutor;

public class AlertLogTimerTask extends TimerTask {
	
	@Override
	public void run() {
		JSONArray jaUserIds = null;
		AlertLogServiceDBI alertLogServiceDBI = null;
		Connection conPC = null;
		
		try {
			alertLogServiceDBI = new AlertLogServiceDBI();
			conPC = DataBaseManager.giveConnection();
			TaskExecutor executor = null;
			
			jaUserIds = alertLogServiceDBI.getBreachedUserIds(conPC);
			
			for (int count=0; count < jaUserIds.size(); count++) {
				//AlertLogThreadController alertLogThreadController = new AlertLogThreadController(jaUserIds.getJSONObject(count), conPC);
				//alertLogThreadController.start();
				
				executor = TaskExecutor.getExecutor(Constants.DEFAULT_SLA_THREADPOOL_NAME);
				executor.submit(new AlertLogThreadController(jaUserIds.getJSONObject(count)));
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
		} finally {
			DataBaseManager.close(conPC);
			conPC = null;
			
			alertLogServiceDBI = null;
		}
		
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("AlertLogTimerTask : finalize");
		
		super.finalize();
	}
	
	/*public static void main(String args[]) {
		JSONObject joTest = new JSONObject();
		Iterator<String> iterSLAs = null;
		joTest.put("test", "test");
		iterSLAs = joTest.keySet().iterator();
		System.out.println("started");
		for(int idx = 1; iterSLAs.hasNext(); idx++ ) {
			String text = iterSLAs.next();
			System.out.println(idx+" -->> "+text);
		}
	}*/
}
