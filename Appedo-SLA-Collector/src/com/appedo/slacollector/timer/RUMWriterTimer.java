package com.appedo.slacollector.timer;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.RUMCollectorBean;
import com.appedo.slacollector.manager.RUMBreachInsertManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Navin
 *
 */
public class RUMWriterTimer extends Thread {
	
	RUMCollectorBean cbRUMBreachEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public RUMWriterTimer(RUMCollectorBean cbRUMEntry) {
		this.cbRUMBreachEntry = cbRUMEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting Module Perf.Counter timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new RUMBreachInsertManager()).fetchBean(cbRUMBreachEntry);
			
//			Thread.currentThread().interrupt();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Module Perf.Counter thread destroyed "+lThisThreadId);
		
		super.finalize();
	}
}
