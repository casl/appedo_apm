package com.appedo.slacollector.timer;

import java.util.Date;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SUMCollectorBean;
import com.appedo.slacollector.manager.ModulePerformanceSUMManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Veeru
 *
 */
public class SUMWriterTimer extends Thread {
	
	SUMCollectorBean cbSUMEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public SUMWriterTimer(SUMCollectorBean cbSUMEntry) {
		this.cbSUMEntry = cbSUMEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting Module Perf.Counter timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new ModulePerformanceSUMManager()).fetchBean(cbSUMEntry);
			
//			Thread.currentThread().interrupt();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}finally{
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Module Perf.Counter thread destroyed "+lThisThreadId);
		
		super.finalize();
	}
}
