package com.appedo.slacollector.timer;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SlaCollectorBean;
import com.appedo.slacollector.manager.ModulePerformanceSlaManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Veeru
 *
 */
public class ModuleWriterTimer extends Thread {
	
	SlaCollectorBean cbSlaEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public ModuleWriterTimer(SlaCollectorBean cbSlaEntry) {
		this.cbSlaEntry = cbSlaEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting SLA Module Perf.Counter timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new ModulePerformanceSlaManager()).fetchBean(cbSlaEntry);
			
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("SLA Module Perf.Counter thread destroyed "+lThisThreadId);
		
		super.finalize();
	}
}
