package com.appedo.slacollector.bean;

import java.util.Date;

import net.sf.json.JSONObject;

public class AVMCollectorBean implements Comparable<AVMCollectorBean>, Cloneable {
	
	private Date dateQueuedOn = null;
	private JSONObject joSUMBreachCountersBean = new JSONObject();
	
	public Date getDateQueuedOn() {
		return dateQueuedOn;
	}
	public void setDateQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	
	public JSONObject getJoCountersBean() {		
		return joSUMBreachCountersBean;
	}
	public void setJoCountersBean(JSONObject joBreachCounters) {
		joSUMBreachCountersBean = joBreachCounters;
	}
	
	@Override
	public int compareTo(AVMCollectorBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (dateQueuedOn.getTime() - another.getDateQueuedOn().getTime()));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}