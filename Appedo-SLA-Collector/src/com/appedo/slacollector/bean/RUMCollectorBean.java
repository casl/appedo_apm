package com.appedo.slacollector.bean;

import java.util.Date;

import net.sf.json.JSONObject;

public class RUMCollectorBean implements Comparable<RUMCollectorBean>, Cloneable {
	
	private Date dateQueuedOn = null;
	// thinks, avoid `= new JSONObject();`
	private JSONObject  joRUMBreachData = new JSONObject();
	
	public Date getDateQueuedOn() {
		return dateQueuedOn;
	}
	public void setDateQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	
	public JSONObject getBreachDataBean() {		
		return joRUMBreachData;
	}
	public void setBreachDataBean(JSONObject joBreachData) {
		joRUMBreachData = joBreachData;
	}
	
	@Override
	public int compareTo(RUMCollectorBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (dateQueuedOn.getTime() - another.getDateQueuedOn().getTime()));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}