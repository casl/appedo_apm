package com.appedo.slacollector.bean;

import java.util.Date;

import com.appedo.slacollector.utils.UtilsFactory;

import net.sf.json.JSONArray;

public class SlaCollectorBean implements Comparable<SlaCollectorBean>, Cloneable {
	private JSONArray joSlaBreachCountersBean = null;
	private Date dateQueuedOn = null;
	
	public Date getDateQueuedOn() {
		return dateQueuedOn;
	}
	public void setDateQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	
	public JSONArray getCountersBean() {		
		return joSlaBreachCountersBean;
	}
	public void setCountersBean(JSONArray JoBreachCounters) {
		joSlaBreachCountersBean = JoBreachCounters;
	}
	
	@Override
	public int compareTo(SlaCollectorBean another) {
		// compareTo should return < 0 if this is supposed to be
		// less than other, > 0 if this is supposed to be greater than 
		// other and 0 if they are supposed to be equal
		
		return ((int) (dateQueuedOn.getTime() - another.getDateQueuedOn().getTime()));
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb	.append("{\"beans\": ").append(joSlaBreachCountersBean).append(", ")
			.append("\"dateQueuedOn\": \""+UtilsFactory.formatTimeStampToyyyyMMddHHmmssSSS( dateQueuedOn.getTime() )).append("\" }");
		return sb.toString();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
