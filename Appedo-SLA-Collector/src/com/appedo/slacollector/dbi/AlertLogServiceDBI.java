package com.appedo.slacollector.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class AlertLogServiceDBI {

	public JSONArray getBreachedUserIds(Connection con) throws Exception {
		JSONArray jaUserDetails = null;
		Statement stmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		try {
			jaUserDetails = new JSONArray();
			//strQuery = "SELECT * FROM get_oad_breached_user()";
			
			sbQuery.append("select coalesce(oad.user_id, log.user_id) user_id, ")
					.append(" coalesce(oad.breached_max_id, 0) oad_max_id, ")
					.append(" coalesce(log.breached_max_id, 0) log_max_id ")
					.append(" from get_oad_breached_user() oad ")
					.append(" full outer join get_log_breached_user() log ")
					.append(" on oad.user_id = log.user_id; ");
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(sbQuery.toString());
			
			while( rst.next() ){
				JSONObject joUserBreachDet = new JSONObject();
				joUserBreachDet.put("userId", rst.getLong("user_id"));
				joUserBreachDet.put("oadBreachedMaxId", rst.getLong("oad_max_id"));
				joUserBreachDet.put("logBreachedMaxId", rst.getLong("log_max_id"));
				
				jaUserDetails.add(joUserBreachDet);
			}
			
		} catch(Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return jaUserDetails;
	}
	
	/**
	 * Add real breached counters, into <b>sla_alert_log_&lt;USER-ID&gt;</b> table.<br/>
	 * Real breach is on:<br/>
	 *   - Min-Breach count should be exceeded<br/>
	 *   - All Counters in the SLA-Policy has to be breached in 20 seconds window.<br/>
	 * 
	 * TODO: But now, instead-of 20 seconds window, 1 minute window (this Timer's interval) will be used.<br/>
	 * If this Thread's process take more than a minute then, 1 minute window will vary.
	 * 
	 * Once the sla_alert_log entries are added then, return all those entries to loop and send SMS.
	 * 
	 * @param con
	 * @param lUserId
	 * @param lMaxBreachedId
	 * @return
	 * @throws Throwable
	 */
	public JSONArray addBreachedToAlert(Connection con, long lUserId, long lMaxBreachedId) throws Throwable {
		JSONArray jaSLAAlertLog = new JSONArray();
		JSONObject joSLAAlertLog = new JSONObject();
		
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			// Update "breached_count" in "so_threashold_breach_<USER-ID> table.
			// i.e. SLA-Policy breach verification can be done for Min-Breach-Count comparison.
			sbQuery.append("SELECT * FROM update_breached_cnt(?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.execute();
			DataBaseManager.close(pstmt);
			
			
			// Add entries into "sla_alert_log_<USER-ID>" table
			sbQuery.setLength(0);
			sbQuery.append("SELECT * FROM add_breached_to_alert_log(?, ?)");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.setLong(2, lMaxBreachedId);
			
			rst = pstmt.executeQuery();
			while( rst.next() ) {
				joSLAAlertLog = new JSONObject();
				joSLAAlertLog.put("alert_log_id", rst.getLong("alert_log_id"));
				joSLAAlertLog.put("sla_id", rst.getLong("sla_id"));
				//joSLAAlertLog.put("received_on", rst.getDate("received_on"));
				joSLAAlertLog.put("alert_freq_min", rst.getInt("alert_freq_min"));
				
				jaSLAAlertLog.add(joSLAAlertLog);
			}
		} catch(Throwable th) {
			LogManager.errorLog(th, sbQuery);
			throw th;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			sbQuery = null;
		}
		
		return jaSLAAlertLog;
	}
	
	public JSONArray addBreachedLogToAlert(Connection con, long lUserId, long lMaxBreachedId) throws Throwable {
		JSONArray jaSLAAlertLog = new JSONArray();
		JSONObject joSLAAlertLog = new JSONObject();
		
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			// Update "breached_count" in "so_threashold_breach_<USER-ID> table.
			// i.e. SLA-Policy breach verification can be done for Min-Breach-Count comparison.
			sbQuery.append("SELECT * FROM update_log_breached_cnt(?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.execute();
			DataBaseManager.close(pstmt);
			
			
			// Add entries into "sla_alert_log_<USER-ID>" table
			sbQuery.setLength(0);
			sbQuery.append("SELECT * FROM add_breached_log_to_alert_log(?, ?)");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.setLong(2, lMaxBreachedId);
			
			rst = pstmt.executeQuery();
			while( rst.next() ) {
				joSLAAlertLog = new JSONObject();
				joSLAAlertLog.put("alert_log_id", rst.getLong("alert_log_id"));
				joSLAAlertLog.put("sla_id", rst.getLong("sla_id"));
				//joSLAAlertLog.put("received_on", rst.getDate("received_on"));
				joSLAAlertLog.put("alert_freq_min", rst.getInt("alert_freq_min"));
				
				jaSLAAlertLog.add(joSLAAlertLog);
			}
		} catch(Throwable th) {
			LogManager.errorLog(th, sbQuery);
			throw th;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			sbQuery = null;
		}
		
		return jaSLAAlertLog;
	}
}
