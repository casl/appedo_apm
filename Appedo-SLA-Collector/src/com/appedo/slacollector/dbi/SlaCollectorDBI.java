package com.appedo.slacollector.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.utils.UtilsFactory;
import com.appedo.utils.HumanReadableFormatter;

/**
 * To insert the ASD, SUM, AVM & RUM breaches
 * 
 * @author Veeru
 *
 */
public class SlaCollectorDBI {
	
	/**
	 * Insert the breach entries, detected in Agent, into so_threshold_breach_<USER-ID>
	 * 
	 * @param con
	 * @param lUserId
	 * @param joBreachedCounter
	 * @param lTimeAppedoQueuedOn
	 * @throws Throwable
	 */
	public void insertSlaBreachTable(Connection con, long lUserId, JSONObject joBreachedCounter, long lTimeAppedoQueuedOn) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			// Eg: slaid":201,"userid":3,"counterid":8,"countertempid":738,"isabovethreshold":false,"warning_threshold_value":700000,"critical_threshold_value":8000000,"received_value":98875,breach_severity:"CRITICAL"}]
			sbQuery	.append("INSERT INTO so_threshold_breach_").append(lUserId).append(" ")
					.append("(sla_id, user_id, guid, counter_id, counter_template_id, is_above, ")
					.append(" warning_threshold_value, critical_threshold_value, received_value, received_on, breached_severity, ")
					.append(" thread_created, enterprise_id, uid) ")
					.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(UtilsFactory.makeValidVarchar(UtilsFactory.formatTimeStampToyyyyMMddHHmmssSSS(lTimeAppedoQueuedOn))).append("::timestamp, ")
					.append("?, ?, ?, (SELECT uid FROM module_master WHERE guid = ?))");

			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, joBreachedCounter.getLong("slaid"));
			pstmt.setLong(2, joBreachedCounter.getLong("userid"));
			pstmt.setString(3, joBreachedCounter.getString("guid"));
			pstmt.setLong(4, joBreachedCounter.getLong("counterid"));
			pstmt.setLong(5, joBreachedCounter.getLong("countertempid"));
			pstmt.setBoolean(6, joBreachedCounter.getBoolean("isabovethreshold"));
			pstmt.setLong(7, joBreachedCounter.getLong("warning_threshold_value"));
			pstmt.setLong(8, joBreachedCounter.getLong("critical_threshold_value"));
			pstmt.setLong(9, joBreachedCounter.getLong("received_value"));
			pstmt.setString(10, joBreachedCounter.getString("breached_severity"));
			pstmt.setBoolean(11, false);
			pstmt.setLong(12, -1);
			pstmt.setString(13, joBreachedCounter.getString("guid"));

			pstmt.executeUpdate();
			
		} catch (Throwable t) {
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;

			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
	}
	
	/**
	 * Insert the breach entries, detected in WPT-Scheduler (data sent by WPT-Agent), into so_sum_threshold_breach_<USER-ID>
	 * 
	 * @param con
	 * @param joBreachCounters
	 * @param lTimeAppedoQueuedOn
	 * @throws Throwable
	 */
	public void insertSUMBreachTable(Connection con, JSONObject joBreachCounters, long lTimeAppedoQueuedOn) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			//slaid":201,"userid":3,"counterid":8,"countertempid":738,"isabovethreshold":false,"thresholdvalue":700000,"received_value":98875}]
			sbQuery .append("INSERT INTO so_sum_threshold_breach_")
					.append(joBreachCounters.getLong("userid"))
					.append("(sla_id, sla_sum_id, har_id, location, breached_severity, threshold_value, err_set_value, received_value, received_on )")
					.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(UtilsFactory.makeValidVarchar(UtilsFactory.formatTimeStampToyyyyMMddHHmmssSSS(lTimeAppedoQueuedOn))).append("::timestamp ) ");
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);

			pstmt.setLong(1, joBreachCounters.getLong("sla_id"));
			pstmt.setLong(2, joBreachCounters.getLong("sla_sum_id"));

			if (joBreachCounters.getString("breached_severity").equals("WEBSITE_NOT_REACHABLE")) {
				pstmt.setNull(3, Types.INTEGER);
				pstmt.setString(4, joBreachCounters.getString("location"));
				pstmt.setString(5, joBreachCounters.getString("breached_severity"));
				pstmt.setNull(6, Types.INTEGER);
				pstmt.setNull(7, Types.INTEGER);
				pstmt.setNull(8, Types.INTEGER);
			} else {
				pstmt.setLong(3, joBreachCounters.getLong("har_id"));
				pstmt.setString(4, joBreachCounters.getString("location"));
				pstmt.setString(5, joBreachCounters.getString("breached_severity"));
				pstmt.setLong(6, joBreachCounters.getLong("threshold_set_value"));
				pstmt.setLong(7, joBreachCounters.getLong("err_set_value"));
				pstmt.setDouble(8, joBreachCounters.getDouble("received_value"));
			}

			pstmt.executeUpdate();
			
		} catch (Throwable t) {
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * Insert the breach entries, detected in AVM-Controller (data sent by AVM-Agent), into so_avm_breach_<USER-ID>
	 * 
	 * @param con
	 * @param joBreachCounters
	 * @param lTimeAppedoQueuedOn
	 * @throws Throwable
	 */
	public void insertAVMBreachTable(Connection con, JSONObject joBreachCounters, long lTimeAppedoQueuedOn) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery .append("INSERT INTO so_avm_breach_").append(joBreachCounters.getLong("userid"))
					.append("(sla_id, avm_test_id, agent_id, guid, country, state, city, region, zone, breached_severity, received_status, received_response_code, received_on)")
					.append(" VALUES(?, ?, (SELECT agent_id FROM avm_agent_master WHERE guid = ?), ?, ?, ?, ?, ?, ?, ?, ?, ?, to_timestamp( ? /1000) )");
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			
			pstmt.setLong(1, joBreachCounters.getLong("sla_id"));
			pstmt.setLong(2, joBreachCounters.getLong("avm_test_id"));
			pstmt.setString(3, joBreachCounters.getString("guid"));
			pstmt.setString(4, joBreachCounters.getString("guid"));
			pstmt.setString(5, joBreachCounters.getString("country"));
			pstmt.setString(6, joBreachCounters.getString("state"));
			pstmt.setString(7, joBreachCounters.getString("city"));
			pstmt.setString(8, joBreachCounters.getString("region"));
			pstmt.setString(9, joBreachCounters.getString("zone"));
			pstmt.setString(10, joBreachCounters.getString("breached_severity"));
			pstmt.setString(11, joBreachCounters.getString("received_status"));
			pstmt.setInt(12, joBreachCounters.getInt("received_response_code"));
			pstmt.setLong(13, lTimeAppedoQueuedOn);
			
			pstmt.executeUpdate();
			
		} catch (Throwable t) {
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	/**
	 * Insert the breach entries, detected in RUM-Controller (data sent by browsers), into so_rum_threshold_breach_<USER-ID>
	 * 
	 * @param con
	 * @param lUserId
	 * @param joRUMBreachedData
	 * @param lTimeAppedoQueuedOn
	 * @throws Throwable
	 */
	public void insertRUMBreach(Connection con, long lUserId, JSONObject joRUMBreachedData, long lTimeAppedoQueuedOn) throws Throwable {
		PreparedStatement pstmt = null;

		StringBuilder sbQuery = new StringBuilder();

		try {
			sbQuery	.append("INSERT INTO so_rum_threshold_breach_").append(lUserId).append(" ")
					.append("  (sla_id, user_id, guid, uid, rum_id, is_above, warning_threshold_value, critical_threshold_value, received_value, received_on, breached_severity) ")
					.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
					//.append(UtilsFactory.makeValidVarchar(UtilsFactory.formatTimeStampToyyyyMMddHHmmssS(lTimeAppedoQueuedOn))).append("::timestamp, ?) ");

			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, joRUMBreachedData.getLong("slaId"));
			pstmt.setLong(2, lUserId);
			pstmt.setString(3, joRUMBreachedData.getString("guid"));
			pstmt.setLong(4, joRUMBreachedData.getLong("uid"));
			pstmt.setLong(5, joRUMBreachedData.getLong("rum_id"));
			pstmt.setBoolean(6, joRUMBreachedData.getBoolean("is_above_threshold"));
			pstmt.setInt(7, joRUMBreachedData.getInt("warning_threshold_value"));
			pstmt.setInt(8, joRUMBreachedData.getInt("critical_threshold_value"));
			pstmt.setLong(9, joRUMBreachedData.getLong("received_value"));
			pstmt.setTimestamp(10, new Timestamp(lTimeAppedoQueuedOn));
			pstmt.setString(11, joRUMBreachedData.getString("breached_severity"));

			pstmt.executeUpdate();
		} catch (Throwable t) {
			LogManager.errorLog(t, sbQuery);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;

			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
	}
	
	/**
	 * Add entry in <b>sla_alert_log_&lt;USER-ID&gt;</b> table, when the whole SLA-Policy is breached.
	 * This update will happen with <b>blank</b> email-id & mobile number; with "is_sent" as false.
	 * 
	 * @param con
	 * @param lUserId
	 * @param lSlaid
	 * @param lTimeAppedoQueuedOn
	 * @return
	 * @throws Exception
	 */
	public long insertSLAAlertLog(Connection con, long lUserId, long lSlaid, long lTimeAppedoQueuedOn) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		long lAlertLogId = 0;
		
		try {
			sbQuery .append("INSERT INTO sla_alert_log_").append(lUserId).append(" ")
					.append("(user_id, sla_id, alert_content_email, alert_content_sms, email_id, sms, is_sent, is_success, received_on) ")
					.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(UtilsFactory.makeValidVarchar(UtilsFactory.formatTimeStampToyyyyMMddHHmmssSSS(lTimeAppedoQueuedOn))).append("::timestamp)");
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, lUserId);
			pstmt.setLong(2, lSlaid);
			pstmt.setString(3, "");
			pstmt.setString(4, "");
			pstmt.setString(5, "");
			pstmt.setString(6, "");
			pstmt.setBoolean(7, false);
			pstmt.setBoolean(8, false);
			
			pstmt.execute();
			
			lAlertLogId = DataBaseManager.returnKey(pstmt);
		} catch(Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return lAlertLogId;
	}
	
	/**
	 * Add entry in <b>sla_alert_log_&lt;USER-ID&gt;</b> table, when the whole SLA-Policy is breached.
	 * This update happens <b>along</b> with blank email-id & mobile number; with "is_sent" as false.
	 * 
	 * @param con
	 * @param lUserId
	 * @param lSlaid
	 * @param strEmailContent
	 * @param strSmsContent
	 * @param strEmailId
	 * @param strSms
	 * @param bIsSent
	 * @param bIsSuccess
	 * @throws Exception
	 */
	public long insertSLAAlertLog(Connection con, long lUserId, long lSlaid, long lTimeAppedoQueuedOn, String strEmailContent, String strSmsContent, String strEmailId, String strSms, boolean bIsSent, boolean bIsSuccess) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		long lAlertLogId = 0;
		
		try {
			sbQuery .append("INSERT INTO sla_alert_log_").append(lUserId).append(" ")
					.append("(user_id, sla_id, alert_content_email, alert_content_sms, email_id, sms, is_sent, is_success, received_on) ")
					.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ")
					.append(UtilsFactory.makeValidVarchar(UtilsFactory.formatTimeStampToyyyyMMddHHmmssSSS(lTimeAppedoQueuedOn))).append("::timestamp)");
			
			pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setLong(1, lUserId);
			pstmt.setLong(2, lSlaid);
			pstmt.setString(3, strEmailContent);
			pstmt.setString(4, strSmsContent);
			pstmt.setString(5, strEmailId);
			pstmt.setString(6, strSms);
			pstmt.setBoolean(7, bIsSent);
			pstmt.setBoolean(8, bIsSent);
			
			pstmt.execute();
			
			lAlertLogId = DataBaseManager.returnKey(pstmt);
		} catch(Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return lAlertLogId;
	}
	
	/**
	 * Update the <b>sla_alert_log_&lt;USER-ID&gt;</b> table entry's email content and sent-to-emailIds for the given Alert-Log-Id.
	 * 
	 * @param con
	 * @param lUserId
	 * @param strMobileNumber
	 * @param strSentContent
	 * @param lAlertLogId
	 * 
	 * @throws Exception
	 */
	public void updateSMSSentDetail(Connection con, long lUserId, String strMobileNumber, String strSentContent, long lAlertLogId) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery .append("UPDATE sla_alert_log_").append(lUserId).append(" SET sms = ?, alert_content_sms = ? ")
					.append("WHERE alert_log_id = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strMobileNumber);
			pstmt.setString(2, strSentContent);
			pstmt.setLong(3, lAlertLogId);
			
			pstmt.execute();
		} catch(Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
	}
	
	/**
	 * Returns the UID of the Rum Module, for the given encrypted UID.
	 * 
	 * @param con
	 * @param strEncryptedUID
	 * @return
	 * @throws Exception
	 */
	public long getModuleUID(Connection con, String strGUID) throws Exception {
		long lUID = -1l;
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				lUID = rst.getLong("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
		}
		
		return lUID;
	}
	
	/**
	 * gets active SLAs counter(s)
	 * 
	 * @param strGUIDs
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> getActiveSLAsCounters(Connection con, String strGUIDs) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		String strGUID = "";
		
		HashMap<String, Object> hmGUIDsMappedToSLAs = new HashMap<String, Object>();
		ArrayList<JSONObject> alSLAs = null;
		
		JSONObject joSLACounter = null;
		
		try {
			sbQuery .append("SELECT ssc.sla_id, ss.user_id, ssc.counter_id, ssc.counter_template_id, ssc.guid, ")
					.append("  ssc.is_above_threshold, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.percentage_calculation, ssc.denominator_counter_id ")
					.append("FROM so_sla_counter ssc ")
					.append("INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id ")
					.append("  AND ssc.guid IN (").append(strGUIDs).append(") ")
					.append("  AND ss.is_active = TRUE ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			while (rst.next()) {
				joSLACounter = new JSONObject();
				strGUID = rst.getString("guid");

				joSLACounter.put("slaid", rst.getLong("sla_id"));
				joSLACounter.put("userid", rst.getLong("user_id"));
				joSLACounter.put("counterid", rst.getLong("counter_id"));
				joSLACounter.put("countertempid", rst.getLong("counter_template_id"));
				joSLACounter.put("isabovethreshold", rst.getBoolean("is_above_threshold"));
				joSLACounter.put("warning_threshold_value", rst.getLong("warning_threshold_value"));
				joSLACounter.put("critical_threshold_value", rst.getLong("critical_threshold_value"));
				joSLACounter.put("percentage_calculation", rst.getBoolean("percentage_calculation"));
				if (rst.getString("denominator_counter_id") != null) {
					joSLACounter.put("denominator_counter_id", rst.getInt("denominator_counter_id"));
				}

				if (hmGUIDsMappedToSLAs.containsKey(strGUID)) {
					alSLAs = (ArrayList<JSONObject>) hmGUIDsMappedToSLAs.get(strGUID);
				} else {
					alSLAs = new ArrayList<JSONObject>();

					// obj by ref
					hmGUIDsMappedToSLAs.put(strGUID, alSLAs);
				}
				alSLAs.add(joSLACounter);
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;

			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
			strGUID = null;
		}

		return hmGUIDsMappedToSLAs;
	}
	
	/**
	 * gets active SLAs counter(s)
	 * 
	 * @param strGUIDs
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> getActiveSLAsCountersV2(Connection con, String strGUIDs) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		String strGUID = "";
		
		HashMap<String, Object> hmGUIDsMappedToSLAs = new HashMap<String, Object>();
		ArrayList<JSONObject> alSLAs = null;
		
		JSONObject joSLACounter = null;
		
		try {
			sbQuery .append("SELECT ssc.sla_id, ss.user_id, ssc.counter_id, ssc.counter_template_id, ssc.guid, ")
					.append("ssc.is_above_threshold, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.percentage_calculation, ssc.denominator_counter_id, ")
					.append("ssc.process_name FROM so_sla_counter ssc ")
					.append("INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id ")
					.append("  AND ssc.guid IN (").append(strGUIDs).append(") ")
					.append("  AND ss.is_active = TRUE ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			while (rst.next()) {
				joSLACounter = new JSONObject();
				strGUID = rst.getString("guid");
				
				joSLACounter.put("sla_id", rst.getLong("sla_id"));
				joSLACounter.put("user_id", rst.getLong("user_id"));
				joSLACounter.put("counter_id", rst.getLong("counter_id"));
				joSLACounter.put("counter_template_id", rst.getLong("counter_template_id"));
				joSLACounter.put("is_above", rst.getBoolean("is_above_threshold"));
				joSLACounter.put("warning_threshold_value", rst.getLong("warning_threshold_value"));
				joSLACounter.put("critical_threshold_value", rst.getLong("critical_threshold_value"));
				joSLACounter.put("percentage_calculation", rst.getBoolean("percentage_calculation"));
				joSLACounter.put("process_name", UtilsFactory.replaceNull(rst.getString("process_name"), ""));
				if (rst.getString("denominator_counter_id") != null) {
					joSLACounter.put("denominator_counter_id", rst.getInt("denominator_counter_id"));
				}

				if (hmGUIDsMappedToSLAs.containsKey(strGUID)) {
					alSLAs = (ArrayList<JSONObject>) hmGUIDsMappedToSLAs.get(strGUID);
				} else {
					alSLAs = new ArrayList<JSONObject>();

					// obj by ref
					hmGUIDsMappedToSLAs.put(strGUID, alSLAs);
				}
				alSLAs.add(joSLACounter);
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;

			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
			strGUID = null;
		}

		return hmGUIDsMappedToSLAs;
	}
	
	public JSONObject getUnsentBreachSummary(Connection con, long lUserId, long lAlertLogId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = null;
		String strSLAName = null;
		
		JSONArray jaCounters = null;
		JSONObject joSLA = new JSONObject(), joCounter = null;
		
		Object[] aryValueUnit = null;
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT ss.sla_name, stb.uid, mm.module_code, mm.module_name, stb.counter_id, get_counter_category(stb.uid, stb.counter_id) AS category, get_counter_display_name(stb.uid, stb.counter_id) AS display_name, get_counter_unit(stb.uid, stb.counter_id) AS unit, ")
					.append("stb.is_above, stb.critical_threshold_value, stb.warning_threshold_value, ")
					.append("min(stb.received_value) AS min_breached, avg(stb.received_value) AS avg_breached, max(stb.received_value) AS max_breached, count(stb.so_tb_id) AS cnt_breached, ")
					.append("count(CASE WHEN breached_severity = 'WARNING' THEN 1 ELSE NULL END) AS warning_cnt, ")
					.append("count(CASE WHEN breached_severity = 'CRITICAL' THEN 1 ELSE NULL END) AS critical_cnt, ")
					.append("max(alert_log_id) AS max_alert_log_id ")
					.append("FROM so_threshold_breach_").append(lUserId).append(" stb ")
					.append("INNER JOIN sla_alert_log_").append(lUserId).append(" sal ON stb.sla_id = sal.sla_id ") 
					.append("  AND stb.received_on = sal.received_on AND sal.received_on > now()-interval '120 days' ")
					.append("  AND is_sent = false ")
					.append("  AND alert_log_id <= ? ")
					.append("INNER JOIN so_sla ss ON stb.sla_id = ss.sla_id ")
					.append("INNER JOIN module_master mm ON stb.uid = mm.uid ")
					.append("GROUP BY ss.sla_name, stb.uid, mm.module_code, mm.module_name, stb.counter_id, ")
					.append("stb.is_above, stb.critical_threshold_value, stb.warning_threshold_value ")
					.append("ORDER BY module_code, module_name, display_name");
					
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lAlertLogId);
			rst = pstmt.executeQuery();
			
			while( rst.next() ) {
				strSLAName = rst.getString("sla_name");
				
				if( ! joSLA.containsKey(strSLAName) ) {
					joSLA.put(strSLAName, new JSONArray());
				}
				jaCounters = joSLA.getJSONArray(strSLAName);
				
				joCounter = new JSONObject();
				joCounter.put("max_alert_log_id", rst.getLong("max_alert_log_id"));
				
				joCounter.put("module_code", rst.getString("module_code"));
				joCounter.put("module_name", rst.getString("module_name"));
				joCounter.put("category", rst.getString("category"));
				joCounter.put("display_name", rst.getString("display_name"));
				joCounter.put("unit", rst.getString("unit"));
				
				joCounter.put("warning_cnt", rst.getString("warning_cnt"));
				joCounter.put("critical_cnt", rst.getString("critical_cnt"));
				joCounter.put("cnt_breached", rst.getString("cnt_breached"));
				
				aryValueUnit = HumanReadableFormatter.getHumanReadableFormat(rst.getDouble("warning_threshold_value"), rst.getString("unit"), false, false);
				joCounter.put("warning_threshold_value", aryValueUnit[0]);
				joCounter.put("warning_breached_unit", aryValueUnit[1]);
				
				aryValueUnit = HumanReadableFormatter.getHumanReadableFormat(rst.getDouble("critical_threshold_value"), rst.getString("unit"), false, false);
				joCounter.put("critical_threshold_value", aryValueUnit[0]);
				joCounter.put("critical_breached_unit", aryValueUnit[1]);
				
				aryValueUnit = HumanReadableFormatter.getHumanReadableFormat(rst.getDouble("min_breached"), rst.getString("unit"), false, false);
				joCounter.put("min_breached_value", aryValueUnit[0]);
				joCounter.put("min_breached_unit", aryValueUnit[1]);

				aryValueUnit = HumanReadableFormatter.getHumanReadableFormat(rst.getDouble("avg_breached"), rst.getString("unit"), false, false);
				joCounter.put("avg_breached_value", aryValueUnit[0]);
				joCounter.put("avg_breached_unit", aryValueUnit[1]);
				
				aryValueUnit = HumanReadableFormatter.getHumanReadableFormat(rst.getDouble("max_breached"), rst.getString("unit"), false, false);
				joCounter.put("max_breached_value", aryValueUnit[0]);
				joCounter.put("max_breached_unit", aryValueUnit[1]);
				
				jaCounters.add(joCounter);
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		
		return joSLA;
	}
	
	public JSONArray getLatestBreachSummary(Connection con, long lUserId, long lSlaId) throws Exception {
		PreparedStatement pstmt = null, pstmt1 = null;
		ResultSet rst = null, rst1 = null ;
		
		StringBuilder sbQuery = null;
		
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		
		Object[] objBreachDetails = null;
		
		String strThresholdOperator = null, strCounterUnit = null;
		
		try{
			// TODO: Child counters/mertics, SLAs configured need to get respective `counter_name`
			
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT ssc.uid, ssc.counter_id, ssc.percentage_calculation, ")
					.append("ssc.module_detail_name, ssc.warning_threshold_value, ssc.critical_threshold_value, ssc.is_above_threshold ")
					.append("FROM so_sla_counter ssc ")
					.append("WHERE sla_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			rst = pstmt.executeQuery();
			
			while( rst.next() ) {
				
				sbQuery = new StringBuilder();
				sbQuery .append("SELECT cm.category, cm.display_name AS counter_name, cm.unit, mm.module_code, mm.module_name ")
						.append("FROM counter_master_").append(rst.getInt("uid"))
						.append(" cm INNER JOIN module_master mm ON mm.guid = cm.guid ")
						.append("WHERE cm.counter_id = ").append(rst.getLong("counter_id"));
				
				pstmt1 = con.prepareStatement(sbQuery.toString());
				rst1 = pstmt1.executeQuery();
				
				if(rst1.next()) {
					strCounterUnit = rst.getBoolean("percentage_calculation") ? "%" : rst1.getString("unit");
					joCounter = new JSONObject();
					joCounter.put("module_code", rst1.getString("module_code"));
					joCounter.put("module_detail_name", rst1.getString("module_name"));
					joCounter.put("category", rst1.getString("category"));
					joCounter.put("counter_name", rst1.getString("counter_name"));
					if( rst.getBoolean("is_above_threshold") ) {
						strThresholdOperator = "&gt;=";
					} else {
						strThresholdOperator = "&lt;=";
					}
					joCounter.put("threshold_operator", strThresholdOperator);
					
					// Get the latest breach details.
					// threshold_value will be critical_threshold_value (or) warning_threshold_value, based on the breached_severity
					//objBreachDetails = getLatestBreachedDetails(con, lUserId, rst.getLong("counter_template_id"));
					objBreachDetails = getLatestBreachedDetails(con, lUserId, rst.getInt("uid"), rst.getLong("counter_id"));
					joCounter.put("breached_severity", objBreachDetails[0]);
					joCounter.put("severity_color", objBreachDetails[1]);
					
					Object[] aryThreshold_value = HumanReadableFormatter.getHumanReadableFormat((double)objBreachDetails[2], strCounterUnit, false, false);
					joCounter.put("threshold_value", aryThreshold_value[0]);
					joCounter.put("threshold_unit", aryThreshold_value[1]);
					
					Object[] aryValueUnit = HumanReadableFormatter.getHumanReadableFormat((double)objBreachDetails[3], strCounterUnit, false, false);
					joCounter.put("received_value", aryValueUnit[0]);
					joCounter.put("breached_unit", aryValueUnit[1]);
					jaCounters.add(joCounter);
					
					joCounter = null;
				}
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(rst1);
			rst1 = null;
			
			DataBaseManager.close(pstmt1);
			pstmt1 = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return jaCounters;
	}
	
	public JSONArray getLatestBreachSummaryForLog(Connection con, long lUserId, long lSlaId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = null;
		
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		
		Object[] objBreachDetails = null;
		
		String strThresholdOperator = null;
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT ssc.uid, ssc.log_grok_name, ssc.breach_pattern, ssc.breached_severity, ssc.grok_column, ssc.warning_threshold_value, ")
					.append("CASE WHEN ssc.breach_pattern IS NULL THEN TRUE ELSE FALSE END AS is_value_based, ")
					.append("ssc.critical_threshold_value, ssc.is_above_threshold, COALESCE(mm.module_code, 'AVM') AS module_code, ")
					.append("COALESCE(mm.module_name, avm.testname) AS module_name FROM so_sla_log ssc ")
					.append("LEFT JOIN module_master mm ON ssc.uid = mm.uid LEFT JOIN avm_test_master avm ON ssc.uid = avm.avm_test_id ")
					.append("WHERE ssc.sla_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			rst = pstmt.executeQuery();
			
			while( rst.next() ) {
				
				joCounter = new JSONObject();
				joCounter.put("module_code", rst.getString("module_code"));
				joCounter.put("module_detail_name", rst.getString("module_name"));
				joCounter.put("log_grok_name", rst.getString("log_grok_name"));
				joCounter.put("breach_pattern", rst.getString("breach_pattern"));
				if( rst.getBoolean("is_above_threshold") ) {
					strThresholdOperator = "&gt;=";
				} else {
					strThresholdOperator = "&lt;=";
				}
				joCounter.put("threshold_operator", strThresholdOperator);
				joCounter.put("is_value_based", rst.getBoolean("is_value_based"));
				objBreachDetails = getLatestBreachedDetailsForLog(con, lUserId, rst.getInt("uid"), lSlaId);
				
				if(rst.getBoolean("is_value_based")) {
					joCounter.put("breached_severity", objBreachDetails[0]);
					joCounter.put("severity_color", objBreachDetails[1]);
					joCounter.put("threshold_value", objBreachDetails[2]);
					joCounter.put("received_value", objBreachDetails[3]);
					joCounter.put("grok_column", objBreachDetails[4]);
				}else {
					joCounter.put("breached_severity", rst.getString("breached_severity"));
					joCounter.put("grok_column", objBreachDetails[4]);
				}
				jaCounters.add(joCounter);
				joCounter = null;
				
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return jaCounters;
	}
	
	public StringBuilder getBreachedCounterNames(Connection con, long lUserId, long lSlaId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = null, sbRtn = null;
		
		try{
			sbQuery = new StringBuilder();
			sbRtn = new StringBuilder();
			sbQuery .append("SELECT counter_type_name ")
					.append("FROM so_sla_counter ")
					.append("WHERE sla_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			rst = pstmt.executeQuery();
			while( rst.next() ){
				sbRtn.append(rst.getString("counter_type_name")).append(",");
			}
			sbRtn.setCharAt(sbRtn.length()-1, '.');
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return sbRtn;
	}
	
	/**
	 * Return the last breached details for the given CounterId of the User.
	 * 
	 * @param lCounterId
	 * @param uid
	 * @param lUserId
	 * @return	Array: 0 - "Breach Severity"; 1 - "Breach Threshold Value"; 2 - "Received Value"
	 * 			"Breach Threshold Value" will be critical_threshold_value (or) warning_threshold_value, based on the breached_severity
	 * @throws Exception
	 */
	public Object[] getLatestBreachedDetails(Connection con, long lUserId, int uid, long lCounterId) throws Exception {
		StringBuilder sbQuery = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		double dReceivedValue = -1, dThreasholdValue = 0;
		
		String strBreachedSeverity = null, strSeverityColor = null;
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT breached_severity, warning_threshold_value, critical_threshold_value, received_value ")
					.append("FROM so_threshold_breach_").append(lUserId)
					.append(" WHERE counter_id = ? AND uid = ? ")
					.append("ORDER BY received_on DESC limit 1");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lCounterId);
			pstmt.setInt(2, uid);
			rst = pstmt.executeQuery();
			
			if( rst.next() ) {
				dReceivedValue = rst.getLong("received_value");
				strBreachedSeverity = rst.getString("breached_severity");
				
				if( strBreachedSeverity.equals("CRITICAL") ) {
					dThreasholdValue = rst.getDouble("critical_threshold_value");
					strSeverityColor = "RED";
				} else if( strBreachedSeverity.equals("WARNING") ) {
					dThreasholdValue = rst.getDouble("warning_threshold_value");
					strSeverityColor = "ORANGE";
				}
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		
		return new Object[]{strBreachedSeverity, strSeverityColor, dThreasholdValue, dReceivedValue};
	}
	
	/**
	 * Return the last breached details for the given CounterId of the User.
	 * 
	 * @param lCounterId
	 * @param uid
	 * @param slaId
	 * @param lUserId
	 * @return	Array: 0 - "Breach Severity"; 1 - "Breach Threshold Value"; 2 - "Received Value"
	 * 			"Breach Threshold Value" will be critical_threshold_value (or) warning_threshold_value, based on the breached_severity
	 * @throws Exception
	 */
	public Object[] getLatestBreachedDetailsForLog(Connection con, long lUserId, int uid, long slaId) throws Exception {
		StringBuilder sbQuery = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		double dReceivedValue = -1, dThreasholdValue = 0;
		
		String strBreachedSeverity = null, strSeverityColor = null, strGrokColumn = null;
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT breached_severity, warning_threshold_value, critical_threshold_value, received_value, grok_column ")
					.append("FROM so_log_threshold_breach_").append(lUserId)
					.append(" WHERE sla_id = ? AND uid = ? ")
					.append("ORDER BY appedo_received_on DESC LIMIT 1");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, slaId);
			pstmt.setInt(2, uid);
			rst = pstmt.executeQuery();
			
			if( rst.next() ) {
				dReceivedValue = rst.getLong("received_value");
				strBreachedSeverity = rst.getString("breached_severity");
				strGrokColumn = rst.getString("breached_severity");
				
				if( strBreachedSeverity.equalsIgnoreCase("critical") ) {
					dThreasholdValue = rst.getDouble("critical_threshold_value");
					strSeverityColor = "RED";
				} else if( strBreachedSeverity.equalsIgnoreCase("warning") ) {
					dThreasholdValue = rst.getDouble("warning_threshold_value");
					strSeverityColor = "ORANGE";
				}
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		
		return new Object[]{strBreachedSeverity, strSeverityColor, dThreasholdValue, dReceivedValue, strGrokColumn};
	}
	
	public JSONObject getSUMTestDetails(Connection con, long lSUMTestId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = null;
		
		JSONObject joSumDetails = null;
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT email_id, telephone_code, mobile_no, testname, testtype, CASE WHEN testtype = 'URL' THEN testurl ELSE testtransaction END AS testurl, sms_alert, email_alert FROM sum_test_master st, usermaster u WHERE st.user_id = u.user_id AND test_id = ")
					.append(lSUMTestId);
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			if( rst.next() ){
				joSumDetails = new JSONObject();
				joSumDetails.put("sum_test_name", rst.getString("testname"));
				joSumDetails.put("test_type", rst.getString("testtype"));
				joSumDetails.put("url", rst.getString("testurl"));
				joSumDetails.put("email_alert", rst.getBoolean("email_alert"));
				joSumDetails.put("sms_alert", rst.getBoolean("sms_alert"));
				joSumDetails.put("email_id", rst.getString("email_id"));
				joSumDetails.put("mobile_no", rst.getString("mobile_no"));
				joSumDetails.put("isd_code", rst.getString("telephone_code"));
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		return joSumDetails;
	}
	
	public JSONObject getAVMTestDetails(Connection con, long lAVMTestId) throws Exception {
		StringBuilder sbQuery = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		JSONObject joSumDetails = new JSONObject();
		
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT testname, testurl ")
					.append("FROM avm_test_master st ")
					.append("INNER JOIN usermaster u ON st.user_id = u.user_id AND avm_test_id = ").append(lAVMTestId);
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rs = pstmt.executeQuery();
			
			if( rs.next() ){
				joSumDetails.put("avm_test_name", rs.getString("testname"));
				joSumDetails.put("url", rs.getString("testurl"));
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex, sbQuery);
			throw ex;
		} finally {
			DataBaseManager.close(rs);
			rs = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return joSumDetails;
	}
	
	public long getAVMTestUserId(Connection con, long lTestId) throws Exception {
		long lUserId = -1l;
		
		StringBuilder sbQuery = new StringBuilder();
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			sbQuery.append("SELECT user_id FROM avm_test_master WHERE avm_test_id = '").append(lTestId).append("'");
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(sbQuery.toString());
			
			while( rst.next() ){
				lUserId = rst.getLong("user_id");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
		}
		
		return lUserId;
	}
	
	public void updateOADBreached(Connection con, long lUserId) throws Exception {
		
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt  = null;
		
		try{
			sbQuery.append("UPDATE user_pvt_counters SET oad_breached = true WHERE user_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);			
			
			pstmt.executeUpdate();

		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
	}
}
