package com.appedo.slacollector.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.utils.UtilsFactory;

public class SlaDBI {
	
	/**
	 * get configured counters for given sla
	 * @param con
	 * @param lSlaid
	 * @return
	 * @throws Exception
	 */
	public JSONArray getConfigCounters(Connection con, long lUserID, long lSlaid) throws Exception {		
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		
		try {
			// TODO: condition add for `uid` or `guid`, get uid or guid
			sbQuery .append("SELECT ssc.counter_id, ssc.min_breach_count AS setcount FROM so_sla_counter ssc ")
					.append("INNER JOIN so_sla ss ON ss.sla_id = ssc.sla_id AND ss.is_active = true ")
					.append("WHERE ssc.sla_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaid);
			rst = pstmt.executeQuery();
			while(rst.next()){
				joCounter = new JSONObject();
				joCounter.put("counter_id", rst.getInt("counter_id"));
				joCounter.put("setcount", rst.getInt("setcount"));
				jaCounters.add(joCounter);
			}
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return jaCounters;
	}
	
	/**
	 * Checks whether SLA is already logged by another GUID, in last 20 seconds.
	 * If alert is already logged in given period then, this current call is not a breach.
	 * 
	 * Or else, if SLA alert is not logged in last 20 seconds then, verify each Counter's breach:
	 * Check SLA's counter has breached maximum number of times in Alert-Duration
	 * If SLA breach record is already available then, it is not counted as breached this time.
	 * 
	 * @param joSlaSettings
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 */
	public boolean hasSlaBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		JSONArray jaConfigCounters = null;
		JSONObject joConfigCounter = null;
		
		boolean bSLABreached = false, bSLAAlreadyBreached = false;
		
		try {
			// Check whether SLA is already logged by another GUID, in last 20 seconds.
			bSLAAlreadyBreached = hasSLABreachedBefore20Seconds(con, lUserID, lSlaID);
			
			// if SLA Alert is not logged in last 20 seconds then, verify each Counter's breach
			if( bSLAAlreadyBreached ) {
				bSLABreached = false;	// Already breached; So not breached this time.
			} else {
				// #counters config
				jaConfigCounters = getConfigCounters(con, lUserID, lSlaID);
				
				// check SLA's counter has breached maximum number of times in Alert-Duration
				for(int i = 0; i < jaConfigCounters.size(); i = i + 1) {
					joConfigCounter = jaConfigCounters.getJSONObject(i);
					
					try {
						sbQuery.setLength(0);
						// TODO: condition add for `uid` or `guid`
						sbQuery	.append("SELECT count(*) as breachcount, max(received_on) AS last_breached_on FROM so_threshold_breach_").append(lUserID).append(" ")
								.append("WHERE counter_id = ").append(joConfigCounter.getInt("counter_id")).append(" ")
								.append("  AND sla_id = ").append(lSlaID)
								.append("  AND (received_on > now() - interval '").append(hmSLASettings.get("duration")).append(" min'").append(") ")
								.append("GROUP BY sla_id, counter_id ")
								.append("HAVING count(*) > ").append(joConfigCounter.getInt("setcount"))
								.append("  AND max(received_on) > now() - interval '20 seconds'");
						
						pstmt  = con.prepareStatement(sbQuery.toString());					
						rst = pstmt.executeQuery();
						
						// if SLA breach record is already available then, it is not counted as breached this time.
						if( ! rst.next() ) {
							bSLABreached = false;
							break;
						} else {
							bSLABreached = true;
						}
					} finally {
						DataBaseManager.close(rst);
						rst = null;
						
						DataBaseManager.close(pstmt);
						pstmt = null;
					}
				}
			}
		} catch(Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
			
			UtilsFactory.clearCollectionHieracy(jaConfigCounters);
			jaConfigCounters = null;
		}
		
		return bSLABreached;
	}
	
	/**
	 * 
	 * @param joSlaSettings
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 */
	public boolean isSUMBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID, long lminBrchCount, String strLocation) throws Exception {
		Date dateLog = LogManager.logMethodStart();

		// Check SLA has breach maximum number of times 
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQry = new StringBuilder();
		
		boolean bSLABreached = false;
		
		try {
			sbQry.append("SELECT count(*) AS breachcount FROM so_sum_threshold_breach_").append(lUserID)
				.append(" WHERE sla_id = ").append(lSlaID)
				.append(" AND (received_on > now() - interval ").append("' ").append(hmSLASettings.get("duration")).append(" min') ")
				.append("GROUP BY sla_id ")
				.append("HAVING count(*) > ").append(lminBrchCount);
			
			pstmt  = con.prepareStatement(sbQry.toString());
			rst = pstmt.executeQuery();
			
			// if above query returns result-set, then i.e. breach has happened more than min. breach times.
			bSLABreached = rst.next();
			
		} catch(Exception e) {
			LogManager.errorLog(e, sbQry);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			LogManager.logMethodEnd(dateLog);
		}

		
		return bSLABreached;
	}
	
	/**
	 * 
	 * @param hmSlaSettings
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 */
	public boolean isAVMBreached(Connection con, HashMap<String, Integer> hmSlaSettings, long lUserID, long lSlaID, long lminBrchCount, String strCountry, String strState, String strCity, String strRegion, String strZone) {
		boolean bReturn = false;
		Date dateLog = LogManager.logMethodStart();
		
		PreparedStatement pstmt = null;
		ResultSet rs1 = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append("SELECT count(*) AS breachcount FROM so_avm_breach_").append(lUserID).append(" ")
					.append("WHERE sla_id = ").append(lSlaID).append(" ")
					.append("  AND (received_on > now() - interval '").append(hmSlaSettings.get("duration")).append(" min') ")
					.append("  AND country = ").append(UtilsFactory.makeValidVarchar(strCountry))
					.append("  AND state = ").append(UtilsFactory.makeValidVarchar(strState))
					.append("  AND city = ").append(UtilsFactory.makeValidVarchar(strCity))
					.append("  AND region = ").append(UtilsFactory.makeValidVarchar(strRegion))
					.append("  AND zone = ").append(UtilsFactory.makeValidVarchar(strZone))
					.append("GROUP BY sla_id ")
					.append("HAVING count(*) > ").append(lminBrchCount);
			
			pstmt  = con.prepareStatement(sbQuery.toString());
			rs1 = pstmt.executeQuery();
			
			// if above query returns result-set, then i.e. breach has happened more than min. breach times.
			bReturn = rs1.next();
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(rs1);
			rs1 = null;
			
			LogManager.logMethodEnd(dateLog);
		}
		
		return bReturn;
	}
	
	/**
	 * checks uid's RUM SLA has breached, based on SLA settings 
	 * 
	 * @param con
	 * @param hmSLASettings
	 * @param lUserID
	 * @param lSlaID
	 * @param nMinBreachCount
	 * @return
	 * @throws Exception
	 */
	public boolean isRUMBreached(Connection con, HashMap<String, Integer> hmSLASettings, long lUserID, long lSlaID, int nMinBreachCount) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		boolean bSLABreached = false;
		
		try {
			// TODO if SLA has combination of ASD-SUM-RUM then SLA-ID's breach should be verified for a period. If ASD is involved it could be 20 seconds
			
			sbQuery	.append("SELECT count(*) AS breachcount ")
					.append("FROM so_rum_threshold_breach_").append(lUserID).append(" ")
					.append("WHERE sla_id = ").append(lSlaID).append(" ")
					.append("  AND received_on > now() - interval '").append(hmSLASettings.get("duration")).append(" minute' ")
					.append("GROUP BY sla_id ")
					.append("HAVING count(*) > ").append(nMinBreachCount).append(" ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			if ( rst.next() ) {
				bSLABreached = true;
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return bSLABreached;
	}
	
	/**
	 * Check whether SLA is already logged by another GUID, in last 20 seconds.
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 * @throws Exception
	 */
	public boolean hasSLABreachedBefore20Seconds(Connection con, long lUserID, long lSlaID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		boolean bSLAAlreadyBreached = false;
		
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append("SELECT true AS logged FROM sla_alert_log_").append(lUserID).append(" ")
					.append("WHERE sla_id = ").append(lSlaID)
					.append("  AND received_on > now() - interval '20 seconds'");
			
			pstmt  = con.prepareStatement(sbQuery.toString());					
			rst = pstmt.executeQuery();
			
			bSLAAlreadyBreached = rst.next();		// if alert is already logged then, this current call is not a breach.
		} catch(Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return bSLAAlreadyBreached;
	}
	
	/**
	 * 
	 * @param lUserID
	 * @param lSlaID
	 * @param nTriggerAlertFreq
	 * @return
	 * @throws Exception
	 */
	/*public boolean isTriggerHealAlertFrq(long lUserID, long lSlaID, int nTriggerAlertFreq ) throws Exception {
			
			ResultSet rs = null;
			Connection con = null;
			StringBuilder sbQuery = new StringBuilder();		
			PreparedStatement pstmt = null;
			JSONObject joSlaSettings = new JSONObject();
			joSlaSettings.put("success",true);
			boolean bReturn = true;
			try{
				con = DataBaseManager.giveConnection();
				sbQuery .append("select count(*) as alertcount from sla_log where sla_id=?")
				.append(" AND user_id=?  AND is_sent=true AND is_success=true AND log_timestamp>now() - interval ")
				.append("'").append(nTriggerAlertFreq).append(" min'");
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setLong(1, lSlaID);
				pstmt.setLong(2, lUserID);
				rs = pstmt.executeQuery();			
				if(rs.next()) {
					if(rs.getLong("alertcount")>0){
						bReturn = true;
					}else{
						bReturn = false;
					}
				}else {
					bReturn = false;
				}
				
			}catch(Exception e){
				joSlaSettings.put("success",false);
				LogManager.errorLog(e);
				e.printStackTrace();
				throw e;
			}finally{
				DataBaseManager.close(pstmt);
				pstmt = null;
				DataBaseManager.close(con);
				con = null;
				UtilsFactory.clearCollectionHieracy(sbQuery);
				sbQuery = null;
			}
			
			return bReturn;
	}*/
	
	/**
	 * To get to email & mobile numbers
	 * @param lUserID
	 * @param lSlaID
	 * @return
	 * @throws Exception
	 */
	/*public String getToEmailMobile(long lUserID, long lSlaID) throws Exception {
		
		String strReturn = null;
		Connection con = null;
		ResultSet rs = null;
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		Date dateLog = LogManager.logMethodStart();
		try{
			con = DataBaseManager.giveConnection();
			sbQuery .append("select a.user_id,sla_name as subject, b.alert_type ,b.email_mobile from so_sla a inner join so_alert b on a.user_id =b.user_id and is_valid=true and a.sla_id=? and a.user_id=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaID);
			pstmt.setLong(2, lUserID);
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				strReturn = rs.getString("email_mobile");
			}
		}catch(Exception e){
			LogManager.errorLog(e);
			e.printStackTrace();
			throw e;
		}finally{
			DataBaseManager.close(pstmt);
			pstmt = null;
			DataBaseManager.close(rs);
			rs = null;
			DataBaseManager.close(con);
			con = null;
			sbQuery = null;
			 LogManager.logMethodEnd(dateLog);
		}
		return strReturn;
	}*/
	
	
	public boolean isSLAAlerted(Connection con, long lUserID, long lSlaID, int nTriggerAlertFreq, String strAlertType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		boolean bAlerted = false;
		
		try {
			sbQuery .append("SELECT count(*) as alertcount ")
					.append("FROM sla_alert_log_").append(lUserID).append(" ")
					.append("WHERE sla_id = ? ")
					.append("AND received_on > now() - interval '").append(nTriggerAlertFreq).append(" min' ");
			if( ! strAlertType.isEmpty() ) {
				if( strAlertType.equals("Email") ) {
					sbQuery.append("AND COALESCE(email_id,'') <> '' ")
							.append(" AND is_sent ");
				} else if( strAlertType.equals("SMS") ) {
					sbQuery.append("AND COALESCE(sms,'') <> '' ");
				}
			}
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaID);
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				bAlerted = rst.getLong("alertcount") > 0;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return bAlerted;
	}
	
	/**
	 * To get the sla settings to specified by user
	 * @param lUserID
	 * @return
	 * @throws Exception
	 */
	/*public JSONObject getSlaSettings(long lUserID) throws Exception {
		
		ResultSet rs = null;
		Connection con = null;
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		JSONObject joSlaSettings = new JSONObject();
		joSlaSettings.put("success",true);
		
		try{
			con = DataBaseManager.giveConnection();
			sbQuery .append("select max_try_count as trycount, try_count_duration_in_min as duration, trigger_alert_every_in_min as alertinmin from sa_sla_setting where user_id = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserID);
			
			rs = pstmt.executeQuery();
			while(rs.next()) {
				joSlaSettings.put("trycount", rs.getString("trycount"));
				joSlaSettings.put("duration", rs.getString("duration"));
				joSlaSettings.put("alertinmin", rs.getString("alertinmin"));
			}
		} catch(Exception e){
			joSlaSettings.put("success",false);
			LogManager.errorLog(e);
			throw e;
		} finally{
			DataBaseManager.close(rs);
			rs = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			DataBaseManager.close(con);
			con = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return joSlaSettings;
	}*/
	
	/**
	 * gets user's SLA alert settings
	 * 
	 * @param con
	 * @param lUserID
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Integer> getSlaSettings(Connection con, long lUserID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		HashMap<String, Integer> hmSLASettings = null;
		
		try {
			sbQuery .append("SELECT max_try_count AS trycount, try_count_duration_in_min AS duration, trigger_alert_every_in_min AS alertinmin ")
					.append("FROM sa_sla_setting ")
					.append("WHERE user_id = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserID);
			rst = pstmt.executeQuery();
			if ( rst.next() ) {
				hmSLASettings = new HashMap<String, Integer>();
				hmSLASettings.put("trycount", rst.getInt("trycount"));
				hmSLASettings.put("duration", rst.getInt("duration"));
				hmSLASettings.put("alertinmin", rst.getInt("alertinmin"));
			}
		} catch(Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return hmSLASettings;
	}
	
	/**
	 * Get the SLA type of the given SLA-Policy.
	 * 
	 * @param con
	 * @param lUserID
	 * @param lSlaid
	 * @return
	 * @throws Exception
	 */
	public String getSLAType(Connection con, long lUserID, long lSlaid) throws Exception {
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();		
		PreparedStatement pstmt = null;
		String strReturn = null;
		
		try{
			sbQuery .append("SELECT sla_type FROM so_sla WHERE user_id = ? and sla_id = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserID);
			pstmt.setLong(2, lSlaid);
			
			rst = pstmt.executeQuery();			
			if(rst.next()) {
				strReturn = rst.getString("sla_type");				
			}
			
		}catch(Exception e){
			throw e;
		}finally{
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return strReturn;
	}
	
	/**
	 * 
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public JSONArray getActions(Connection con, long lSlaId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();		
		JSONObject joActions = null;
		JSONArray jaActions = new JSONArray();
		
		try{
			//sbQuery .append("select a.rule_id,b.action_id,b.action_name,b.connection_string,b.execution_type,b.script from (select * from so_rule_action  where rule_id=(select rule_id from so_sla_rule where sla_id=?) ) a inner join  so_sla_action  b on a.action_id = b.action_id order by sequence_id asc");
			sbQuery .append("select a.rule_id,b.action_id,b.action_name,b.execution_type,b.script from (select * from so_rule_action  where rule_id=(select rule_id from so_sla_rule where sla_id=?) ) a inner join  so_sla_action  b on a.action_id = b.action_id order by sequence_id asc");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				joActions = new JSONObject();
				
				joActions.put("rule_id", rst.getLong("rule_id"));
				joActions.put("action_id", rst.getLong("action_id"));
				joActions.put("action_name", rst.getString("action_name"));
				//joActions.put("connection_string", rs.getString("connection_string"));
				joActions.put("execution_type", rst.getString("execution_type"));
				joActions.put("script", rst.getString("script"));
				
				jaActions.add(joActions);
			}
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return jaActions;
	}
	/**
	 * 
	 * @param joHealLog
	 * @throws Exception
	 */
	public void updateActionLog(Connection con, long lSlaId) throws Exception {
		PreparedStatement pstmt = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		try{
			sbQuery .append("insert into sla_action_log(sla_id,log_timestamp) values (?,now())");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			pstmt.executeUpdate();
		}catch(Exception e){
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
	}
	
	public void updateHealLog(Connection con, JSONObject joHealLog) throws Exception {
		StringBuilder sbQuery = new StringBuilder();		
		PreparedStatement pstmt = null;
		
		try{
			sbQuery .append("insert into sla_log(user_id,sla_id,rule_id,action_id,action_name,script_executed,is_success,server_ip,os_version,reason_for_breach,")
					.append("timestamp_of_start,timestamp_offset_value,timestamp_of_end,timestamp_of_end_offset_value,log_timestamp,log_timestamp_offset_value,action_type,operating_system, action_log, is_sent) ")
					.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, joHealLog.getLong("userid"));
			pstmt.setLong(2, joHealLog.getLong("slaid"));
			pstmt.setLong(3, joHealLog.getLong("ruleid"));
			pstmt.setLong(4, joHealLog.getLong("actionid"));
			pstmt.setString(5, joHealLog.getString("actionname"));
			pstmt.setString(6, joHealLog.getString("script"));
			pstmt.setBoolean(7, joHealLog.getBoolean("issuccess"));
			pstmt.setString(8, joHealLog.getString("serverip"));
			pstmt.setString(9, joHealLog.getString("osversion"));
			pstmt.setString(10, joHealLog.getString("reasonforbreach"));
			pstmt.setString(10, getBreachInfo(joHealLog.getString("reasonforbreach")).toString());
			pstmt.setTimestamp(11, new Timestamp(joHealLog.getLong("actionstarttime")));
			pstmt.setLong(12, joHealLog.getLong("startoffset"));
			pstmt.setTimestamp(13, new Timestamp(joHealLog.getLong("actionendtime")));
			pstmt.setLong(14, joHealLog.getLong("endoffset"));
			pstmt.setTimestamp(15, new Timestamp(new Date().getTime()));
			pstmt.setLong(16, (new Date().getTimezoneOffset()));
			pstmt.setString(17, joHealLog.getString("actiontype"));
			pstmt.setString(18, joHealLog.getString("osversion"));
			pstmt.setString(19, joHealLog.getString("actionLog"));
			pstmt.setBoolean(20, joHealLog.getBoolean("isSent"));
			pstmt.executeUpdate();
		}catch(Exception e){
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
	}
	/**
	 * 
	 * @param strBreachCounters
	 * @return
	 */
	public StringBuilder getBreachInfo(String strBreachCounters) {
		JSONArray jaBreachCounters = JSONArray.fromObject(strBreachCounters);
		StringBuilder sb = new StringBuilder();
		try {
			if(jaBreachCounters.size()>0 && !jaBreachCounters.isEmpty()) {
				for(int i=0; i<jaBreachCounters.size(); i++) {
					sb.append(jaBreachCounters.getJSONObject(i).getString("counter_type_name"));
					sb.append(",");
				}
			}
			sb.setCharAt(sb.length()-1, '.');
		}catch(Exception e) {
			LogManager.errorLog(e);
		   
		}finally {
			UtilsFactory.clearCollectionHieracy(jaBreachCounters);
		}
		return sb; 
	}
	/**
	 * Max try count is calculated with in specified time
	 * @param lTryCountDuration
	 * @return
	 * @throws Exception 
	 */
	public long getActionTakenCount(Connection con, long lSlaID, long lUserID, long lTryCountDuration) throws Exception {
		ResultSet rst = null;		
		PreparedStatement pstmt = null;
		
		StringBuilder sbQuery = new StringBuilder();

		long lReturn =0l;
		
		try {
			//sbQuery .append("select count(*) as maxtrycnt from sla_log where user_id=? and sla_id=? and log_timestamp>now() - interval ")
			
			sbQuery .append("select count(*) as maxtrycnt from sla_action_log where sla_id=? and log_timestamp>now() - interval ")
					.append("'").append(lTryCountDuration).append(" min'");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			//pstmt.setLong(1, lUserID);
			pstmt.setLong(1, lSlaID);
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				lReturn = rst.getLong("maxtrycnt");
			}
			
		}catch(Exception e) {
			throw e;
		}finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		return lReturn;
	}
	
	
	/**
	 * TO update slave status active to inactive 
	 * when there is no communication 
	 * @param con
	 * @throws Throwable
	 */
	public void updateNodeStatus(Connection con) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append("update sla_slave_status SET sla_slave_status = ? ")
					.append("where modified_on< now() - interval ")
					.append("'").append(Constants.INTERVAL).append(" min'");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, "Inactive");
			pstmt.execute();
		} catch (Throwable t) {
			LogManager.errorLog(t);
			throw t;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	/**
	 * To get the server details configured for given sla
	 * @param lSlaId
	 * @return
	 * @throws Exception
	 */
	public JSONObject getServerConfigDetails(Connection con, long lSlaId) throws Exception {
		ResultSet rst = null;
		PreparedStatement pstmt = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		JSONObject joConfigServDetails = null;
		
		try{
			sbQuery .append("SELECT server_details FROM so_sla where is_deleted=false AND sla_id=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSlaId);
			rst = pstmt.executeQuery();
			while(rst.next()) {	
				if(rst.getString("server_details").trim().startsWith("{") && rst.getString("server_details").trim().endsWith("}") ) {
					joConfigServDetails = JSONObject.fromObject(rst.getString("server_details"));
				}								
			}
		}catch(Exception e){
			throw e;
		}finally{
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return joConfigServDetails;
	}
	
	/**
	 * gets user's alert configured email_ids & mobile numbers
	 * 
	 * @param con
	 * @param lSLAId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<HashMap<String, String>> getUserAlertToAddresses(Connection con, long lUserId, String strAlertType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		
		try {
			alAlertAddresses = new ArrayList<HashMap<String, String>>();
			
			sbQuery .append("SELECT alert_type, email_mobile, telephone_code ")
					.append("FROM so_alert ")
					.append("WHERE is_valid = TRUE ")
					.append("AND user_Id = ? ")
					.append("AND ( alert_type = ? OR '' = ? )");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.setString(2, strAlertType);
			pstmt.setString(3, strAlertType);
			rst = pstmt.executeQuery();
			
			while( rst.next() ) {
				hmAlertAddress = new HashMap<String, String>();
				hmAlertAddress.put("alertType", rst.getString("alert_type"));
				hmAlertAddress.put("emailMobile", rst.getString("email_mobile"));
				hmAlertAddress.put("telephoneCode", UtilsFactory.replaceNull(rst.getString("telephone_code"), ""));
				
				alAlertAddresses.add(hmAlertAddress);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return alAlertAddresses;
	}
	
	/**
	 * gets user's alert configured email_ids & mobile numbers
	 * 
	 * @param con
	 * @param lSLAId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<HashMap<String, String>> getUserAlertToAddresses(Connection con, long lSLAId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		
		try {
			alAlertAddresses = new ArrayList<HashMap<String, String>>();
			
			sbQuery .append("SELECT ss.user_id, ss.sla_name AS subject, ")
					.append("sa.alert_type, sa.email_mobile , sa.telephone_code ")
					.append("FROM so_sla ss ")
					.append("INNER JOIN so_alert sa on sa.user_id = ss.user_id ")
					.append("AND sa.is_valid = TRUE ")
					.append("AND ss.sla_id = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lSLAId);
			rst = pstmt.executeQuery();
			while( rst.next() ) {
				hmAlertAddress = new HashMap<String, String>();
				//hmAlertAddress.put("userId", rst.getString("user_id"));
				hmAlertAddress.put("subject", rst.getString("subject"));
				hmAlertAddress.put("alertType", rst.getString("alert_type"));
				hmAlertAddress.put("emailMobile", rst.getString("email_mobile"));
				hmAlertAddress.put("telephoneCode", UtilsFactory.replaceNull(rst.getString("telephone_code"), ""));
				
				alAlertAddresses.add(hmAlertAddress);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return alAlertAddresses;
	}
	
	/**
	 * gets AVM agent's to alert addresses 
	 * 
	 * @param con
	 * @param strGUID
	 * @return
	 * @throws Exception
	 */
	public ArrayList<HashMap<String, String>> getAVMAgentAlertToAddresses(Connection con, String strGUID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		ArrayList<HashMap<String, String>> alAlertAddresses = null;
		HashMap<String, String> hmAlertAddress = null;
		
		try {
			alAlertAddresses = new ArrayList<HashMap<String, String>>();
			
			sbQuery	.append("SELECT agent_id, alert_type, email_mobile ")
					.append("FROM avm_agent_alert_mapping ")
					.append("WHERE guid = ? AND verified_on IS NOT NULL ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strGUID);
			rst = pstmt.executeQuery();
			while ( rst.next() ) {
				hmAlertAddress = new HashMap<String, String>();
				hmAlertAddress.put("agentId", rst.getString("agent_id"));
				// Note: key names `alertType`, `emailMobile` is same for `getUserAlertToAddresses` (default) & `getAVMAgentAlertToAddresses` (AVM agent's) 
				hmAlertAddress.put("alertType", rst.getString("alert_type"));
				hmAlertAddress.put("emailMobile", rst.getString("email_mobile"));
				
				alAlertAddresses.add(hmAlertAddress);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return alAlertAddresses;
	}
	
	/**
	 * gets Alert message dynamic content for the uid
	 * 
	 * @param con
	 * @param lUId
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> getRUMAlertMessageDetails(Connection con, long lUId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		String strQuery = "";
		
		HashMap<String, String> hmMessageDetails = null;
		
		try {
			strQuery = "SELECT module_name FROM module_master WHERE module_code = 'RUM' AND uid = ? ";
			
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, lUId);
			rst = pstmt.executeQuery();
			if ( rst.next() ) {
				hmMessageDetails = new HashMap<String, String>();
				hmMessageDetails.put("moduleName", rst.getString("module_name"));
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			strQuery = null;
		}
		
		return hmMessageDetails;
	}
}
