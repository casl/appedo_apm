package com.appedo.slacollector.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Connection;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.dbi.SlaDBI;
import com.appedo.slacollector.manager.SlaManager;

public class BreachController extends Thread {

	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;
	
	SlaCollectorDBI slaCollectDBI = null;
	SlaManager slaManager = null ;
	SlaDBI slaDBI= null;

	public BreachController(long lUserID, long lSlaID, long lTimeAppedoQueuedOn) {
		this.lSlaID = lSlaID;
		this.lUserID = lUserID;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		
		establishDBConnection();
		
		slaCollectDBI = new SlaCollectorDBI();
		slaDBI = new SlaDBI();
		slaManager = new SlaManager();
	}

	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		BreachActionThread bat = null;
		SlaDBI sladbi = null;
		
		JSONArray jaActions= null;
		HashMap<String, Integer> hmSLASettings = null;
		
		String strSlaType = null;
		long lAlertLogId = -1;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			// Get the Trigger alert frequency. And other parameters of the SLA-Policies
			hmSLASettings = slaManager.getSlaSettings(con, lUserID);
			
			// Check whether 
			//   all counters of the Policy has breached (&) breach count is more than min-breach count then, 
			// do alert (or) alert-&-heal activities based on the SLA type
			boolean bSLABreached = slaManager.hasSlaBreached(con, hmSLASettings, lUserID, lSlaID);
			
			if( bSLABreached ) {
				sladbi = new SlaDBI();
				
				strSlaType = sladbi.getSLAType(con, lUserID, lSlaID);
				
				// if SLA type has alert, means "Alert-&-Heal" is included.
				if( strSlaType != null && strSlaType.startsWith("Alert")){
					
					// Insert into `sla_alert_log_<user_id>`, with blank email,mobile details & is_sent as false
					lAlertLogId = slaManager.insertSLAAlertLog(con, lUserID, lSlaID, lTimeAppedoQueuedOn);
					
					/*
					 * Start a thread and 
					 * check whether User is Alerted, through EMail/SMS, in last "Alert-In-Min" duration.
					 * If not alerted then alert with consolidated email
					 * 
					 * Also send SMS, if required.
					 * 
					 * We can also check whether User is Alerted for particular SLA-Id, in last "Alert-In-Min" duration, using:
					 * if( slaManager.isSLAAlerted(con, lUserID, lSlaID, hmSLASettings.get("alertinmin")) ) {
					 *     bat = new BreachActionThread(lUserID, lSlaID, lTimeAppedoQueuedOn);
					 * }
					 */
					bat = new BreachActionThread(lUserID, lSlaID, lAlertLogId, lTimeAppedoQueuedOn, hmSLASettings.get("alertinmin"));
					bat.start();
				}
				
				// if alert type is alert-&-heal then get the actions
				if( strSlaType != null && strSlaType.equalsIgnoreCase("Alert&Heal") ) {
					
					// Get the number of times action taken for given sla id 
					long lActionTakenCount = sladbi.getActionTakenCount(con, lSlaID, lUserID, hmSLASettings.get("duration"));
					
					// check is max number of times action taken
					if( lActionTakenCount < hmSLASettings.get("trycount") ) {
						// get server config details for sla
						JSONObject joConfigServDetails = sladbi.getServerConfigDetails(con, lSlaID);
						
						// get the actions assigned to sla
						jaActions= sladbi.getActions(con, lSlaID);
						for(int i=0 ; i < jaActions.size(); i++) {
							synchronized(this){								
								doAction(jaActions.getJSONObject(i), strSlaType, joConfigServDetails);
							}
						}
						
						// Need to update in action log table 
						sladbi.updateActionLog(con, lSlaID);
					} else {
						// need to send mail when maximum action taken count
						// exceeds greater than configured
						if( ! slaManager.isSLAAlerted(con, lUserID, lSlaID, hmSLASettings.get("alertinmin"), "") ) {
							boolean bSend = slaManager.sendActionEmailAlert(con, lUserID, lSlaID, "has gone above your specified maxtry count");
						}
					}
				}
			}
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			bat = null;
			sladbi = null;
			jaActions= null;
		}
	}
	
	/***
	 * to connect the slave to take the action for breach
	 * @return 
	 */
	public synchronized void doAction( JSONObject joActions, String strActionType) {

		Socket scon = null;
		InetAddress address = null;
		OutputStream outStream = null;
		InputStream inStream = null;
		byte[] headerInByts = null;
		String strHeader = null;
		String strActionStreamResponse = null;
		String strPrimaryHost = null, strPrimaryHostPort = null, strExecutionType = null;
		String strSecondaryHost = null, strSecondaryHostPort = null;
		// String strStatus = null;
		JSONObject  joActionLog = new JSONObject();
		HashMap<String,String> hmHeaderTokens = null;
		int nStreamLength =0 ;
		boolean bSend = false;
		String strReceivedStreamContent = null;
		SlaManager slaManager = new SlaManager();
		String strActionLogFile = "";
		JSONArray joBreachParam = null;
		
		try	{
			// get primary salve connection details
			strPrimaryHost = joActions.getString("priServer");
			strPrimaryHostPort = joActions.getString("prislavePort");
			strSecondaryHost = joActions.getString("secServer");
			strSecondaryHostPort = joActions.getString("secslavePort");
			//Boolean bHealPri = joActions.getBoolean("healPri");
			strExecutionType = joActions.getString("execution_type");
			
			// Get script
			String strScript = joActions.getString("script");
			String strSlaveStatus = getSlaveActiveStatus(strPrimaryHost, strPrimaryHostPort);
			// if slave is not active need to logit.
			// Here if status :OK active
			// status : error npn active
			if(slaManager.getOperation(strSlaveStatus).trim().equalsIgnoreCase("OK")) {
				
				// Slave is active  then give action commnd
				strHeader = null;
				headerInByts = null;

				//Obtain an address object of the server 
				address = InetAddress.getByName(strPrimaryHost);
				// Establish a socket connection
				scon = new Socket(address, Integer.parseInt(strPrimaryHostPort));
				// Instantiate a BufferedOutputStream object	 
				outStream = scon.getOutputStream();
				inStream=scon.getInputStream();

				strHeader = "ACTION: "+strScript.length()+"\r\ntype= "+strExecutionType+"\r\nsecondaryip= "+strSecondaryHost+"\r\nsecondaryport= "+strSecondaryHostPort+"\r\n\r\n"+strScript;
				headerInByts = strHeader.getBytes();
				outStream.write(headerInByts, 0, headerInByts.length);
				// Get action status 
				strActionStreamResponse = slaManager.readHeader(inStream);
				
				if(strActionStreamResponse != null) {
					// update action status in to the heal log				
					//strStatus = sm.getOperation(strActionStreamResponse);
					nStreamLength = slaManager.getBodyLength(strActionStreamResponse);
					//System.out.println("length = " + nStreamLength);
					//if status "OK" then
					hmHeaderTokens = slaManager.getHeadTokens(strActionStreamResponse);	
				}

				// Read the body (log body)
				if(nStreamLength>0)	{
					int  readCount = 0;
					byte[] bytes = new byte[nStreamLength];

					while (readCount < nStreamLength) {
						readCount+= inStream.read(bytes, readCount, nStreamLength-readCount);
					}
					strReceivedStreamContent = new String(bytes);
				}
				//create sla
				if(strReceivedStreamContent == null){
					strReceivedStreamContent = new String("Action performed successfully.");
				}

				strActionLogFile = hmHeaderTokens.get("log");
				slaManager.createSlaLog(strActionLogFile, strReceivedStreamContent);
				

				// capture all log
				joActionLog.put("userid", lUserID);
				joActionLog.put("slaid", lSlaID);
				joActionLog.put("ruleid", joActions.getLong("rule_id"));
				joActionLog.put("actionid", joActions.getLong("action_id"));
				joActionLog.put("actionname", joActions.getString("action_name"));
				joActionLog.put("script", joActions.getString("script"));
				joActionLog.put("issuccess",true);
				joActionLog.put("serverip", strPrimaryHost);
				joActionLog.put("osversion", hmHeaderTokens.get("os"));
				joActionLog.put("error", hmHeaderTokens.get("error"));
				joActionLog.put("actionstarttime",hmHeaderTokens.get("actionstarttime"));
				joActionLog.put("startoffset",Long.parseLong(hmHeaderTokens.get("startoffset")));
				joActionLog.put("actionendtime",hmHeaderTokens.get("actionendtime"));
				joActionLog.put("endoffset",Long.parseLong(hmHeaderTokens.get("endoffset")));
				
				// get breach details
				joBreachParam = slaCollectDBI.getLatestBreachSummary(con, lUserID, lSlaID);
				joActionLog.put("reasonforbreach", joBreachParam.toString());
				joActionLog.put("actiontype", strActionType);
				
				
				// send an email to configured email
				bSend = slaManager.sendActionEmailAlert(con, lUserID, lSlaID,strReceivedStreamContent);
				
				joActionLog.put("isSent", bSend);
				joActionLog.put("actionLog", strActionLogFile);
				// Insert action action performed				
				slaDBI.updateHealLog(con, joActionLog);
				//slaDBI.updateActionLog(lSlaID);
				
				slaManager.uploadLogFile(strActionLogFile);
				
			} else {
				
				hmHeaderTokens = slaManager.getHeadTokens(strSlaveStatus);	
				strActionLogFile = System.currentTimeMillis()+".log";
				slaManager.createSlaLog(strActionLogFile, hmHeaderTokens.get("error"));
				
				// Slave  not active
				joBreachParam = slaCollectDBI.getLatestBreachSummary(con, lUserID,lSlaID);
				joActionLog.put("reasonforbreach", joBreachParam.toString());
				joActionLog.put("userid", lUserID);
				joActionLog.put("slaid", lSlaID);
				joActionLog.put("ruleid", joActions.getLong("rule_id"));
				joActionLog.put("actionid", joActions.getLong("action_id"));
				joActionLog.put("actionname", joActions.getString("action_name"));
				joActionLog.put("script", joActions.getString("script"));
				joActionLog.put("osversion", "");
				joActionLog.put("error", hmHeaderTokens.get("error"));
				joActionLog.put("serverip", strPrimaryHost);
				joActionLog.put("issuccess",false);
				joActionLog.put("actionstarttime","0");
				joActionLog.put("startoffset",Long.parseLong("0"));
				joActionLog.put("actionendtime","0");
				joActionLog.put("endoffset",Long.parseLong("0"));
				joActionLog.put("actiontype", strActionType);
				
				// send an email to configured email				
				bSend = slaManager.sendActionEmailAlert(con, lUserID, lSlaID,hmHeaderTokens.get("error"));
				
				joActionLog.put("isSent", bSend);
				joActionLog.put("actionLog", strActionLogFile);
				// Insert action action performed				
				slaDBI.updateHealLog(con, joActionLog);
				//slaDBI.updateActionLog(lSlaID);
				
				slaManager.uploadLogFile(strActionLogFile);
			}
			
			scon.close();
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			scon = null;
			address = null;
			outStream = null;
			inStream = null;
			headerInByts = null;
			strHeader = null;
			strActionStreamResponse = null;
			strPrimaryHost = null;
			strPrimaryHostPort = null;
			strExecutionType = null;
			strSecondaryHost = null;
			strSecondaryHostPort = null;
			hmHeaderTokens = null;
			nStreamLength =0 ;
			strReceivedStreamContent = null;
			strActionLogFile = "";
			joBreachParam = null;
			joActionLog = null;
			slaManager = null;
			slaDBI = null;
		}
	}
	
	/***
	 * to connect the slave to take the action for breach
	 * @return 
	 */
	public synchronized void doAction(JSONObject joActions,String strActionType, JSONObject joConfigServDetails) {
		
		String strPrimaryHost = null, strPrimaryHostPort = null;
		String strSecondaryHost = null, strSecondaryHostPort = null;
		
		try	{
			
//			{
//			    "servers": [
//			        {
//			            "priServer": "dev.appedo.com",
//			            "slavePort": "2222",
//			            "healPri": "true",
//			            "secondary": [
//			                {
//			                    "secServer": "dev.appedo.com",
//			                    "slavePort": "3333"
//			                }
//			            ]
//			        }
//			    ]
//			}
			
			JSONArray jaAllConfigServers = JSONArray.fromObject(joConfigServDetails.get("servers"));
			//System.out.println("JSON SIZE:"+jaAllConfigServers.size());
			
			for(int i=0; i<jaAllConfigServers.size(); i++) {
				
				JSONObject joConfigServer = jaAllConfigServers.getJSONObject(i);
				
				// get primary salve connection details
				strPrimaryHost = joConfigServer.getString("priServer");
				strPrimaryHostPort = joConfigServer.getString("slavePort");
				Boolean bHealPri = joConfigServer.getBoolean("healPri");
				if(bHealPri) {
					joActions.put("priServer", strPrimaryHost);
					joActions.put("prislavePort", strPrimaryHostPort);
					joActions.put("secServer", "na");
					joActions.put("secslavePort", "na");
					
					doAction(joActions, strActionType);
				}
				JSONArray jaSecondaryConfigServers = JSONArray.fromObject(joConfigServer.get("secondary"));
				
				if(jaSecondaryConfigServers.size()>0 ) {
					
					for(int nSeconServ=0; nSeconServ<jaSecondaryConfigServers.size(); nSeconServ++) {
						
						JSONObject joSecondaaryConfigServer = jaSecondaryConfigServers.getJSONObject(i);						
						strSecondaryHost = joSecondaaryConfigServer.getString("secServer");
						strSecondaryHostPort = joSecondaaryConfigServer.getString("slavePort");
						
						joActions.put("priServer", strPrimaryHost);
						joActions.put("prislavePort", strPrimaryHostPort);
						joActions.put("secServer", strPrimaryHost);
						joActions.put("secslavePort", strPrimaryHost);	
						
						doAction(joActions, strActionType);
						
					}
					
				}else {
					
				}
			}
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			strPrimaryHost = null;
			strPrimaryHostPort = null;			
			strSecondaryHost = null;
			strSecondaryHostPort = null;
			
			slaDBI = null;
		}
	}
	
	/**
	 * to test slave active status
	 * @param strPrimaryHost
	 * @param strPrimaryHostPort
	 * @return
	 * @throws Throwable
	 */
	public String getSlaveActiveStatus(String strPrimaryHost, String strPrimaryHostPort)  {

		Socket scon = null;
		InetAddress address = null;
		OutputStream outStream = null;
		InputStream inStream = null;
		byte[] headerInByts = null;
		String strSlaveStatus = null;

		try {
			//Obtain an address object of the server 
			address = InetAddress.getByName(strPrimaryHost);
			// Establish a socket connection
			scon = new Socket(address, Integer.parseInt(strPrimaryHostPort));
			// Instantiate a BufferedOutputStream object	 
			outStream = scon.getOutputStream();
			inStream=scon.getInputStream();			
			// check  slave status
			String strHeader = "TEST: 0\r\n\r\n";
			headerInByts = strHeader.getBytes();
			outStream.write(headerInByts, 0, headerInByts.length);
			// agent response 
			strSlaveStatus = slaManager.readHeader(inStream);

		} catch(Throwable e) {
			String txtBody = e.getMessage();
			strSlaveStatus = "ERROR: 0\r\nerror= "+txtBody+"\r\n\r\n";
			LogManager.errorLog(e);

		} finally {
			headerInByts = null;
			try {
				outStream.close();
				inStream.close();
				scon.close();
			} catch(Exception ee) {
				LogManager.errorLog(ee);
			}
			
			address = null;
		}
		
		return strSlaveStatus;
	}
	
	/**
	 * to read the header of stream data
	 * @param stream
	 * @return
	 * @throws Throwable
	 */

	public String readHeader(InputStream stream) throws Throwable {

		StringBuffer instr = null;
		int c;
		try	{
			instr = new StringBuffer();
			while (true ) {
				c=stream.read();
				instr.append( (char) c);
				if(instr.toString().endsWith("\r\n\r\n") )
					break;
			}
		}catch(Throwable t)	{
			throw t;
		}
		return instr.toString();
	}


	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("BreachController :  finalize ");
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
