package com.appedo.slacollector.controller;

import java.util.Date;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SUMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.timer.SUMWriterTimer;

public class SUMCollectorThreadController extends ApplicationThreadsController {
	
	private CollectorManager collectorManager = null;
	int checkCount = 0;
	public SUMCollectorThreadController() {
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	@Override
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		SUMWriterTimer sumWriterTimer = null;
		SUMCollectorBean cbSUMEntry = null;	
		
		while( getWorkSignal() ) {
			try{
				cbSUMEntry = collectorManager.pollSUMBean();
				
				// take one-by-one bean from the queue
				if( cbSUMEntry != null ) {
					sumWriterTimer = new SUMWriterTimer(cbSUMEntry);
					sumWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getSUMQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				sumWriterTimer = null;
				cbSUMEntry = null;
				LogManager.logMethodEnd(dateLog);
			}
		}
		
		printStopReason();
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("CollectorThreadController got stopped");
		
		super.finalize();
	}
}
