package com.appedo.slacollector.controller;

import java.sql.Connection;
import java.util.Date;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SUMManager;

public class SUMBreachActionThread extends Thread {

	private Connection con = null;
	
	long lTimeAppedoQueuedOn = 0L;
	JSONObject joSUMBreachDetails = null;

	SUMBreachActionThread(JSONObject joSUMBreachDetails, long lTimeAppedoQueuedOn) {
		this.joSUMBreachDetails = joSUMBreachDetails;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		
		establishDBConnection();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		// send email alert
		// create child thread for action
		Date dateLog = LogManager.logMethodStart();
		SUMManager sumManager = null;
		
		try {
			// if connection is not established to DB server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			sumManager = new SUMManager();
			
			// Send Alert
			sumManager.sendAlerts(con, joSUMBreachDetails, lTimeAppedoQueuedOn);
			
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			sumManager = null;
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("SUMBreachActionThread :  finalize ");
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
