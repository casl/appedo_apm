package com.appedo.slacollector.controller;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.timer.SlaveWriterTimer;

public class SlaSlaveDetailsThreadController extends ApplicationThreadsController {
	
	private CollectorManager collectorManager = null;
	
	public SlaSlaveDetailsThreadController() {
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	@Override
	public void run() {
		SlaveWriterTimer moduleWriterTimer = null;
		String strSlaveEntry = null;		
		
		while( getWorkSignal() ) {
			try{
				strSlaveEntry = collectorManager.pollSlaveDetails();
				
				// take one-by-one bean from the queue
				if( strSlaveEntry != null ) {
					moduleWriterTimer = new SlaveWriterTimer(strSlaveEntry);
					moduleWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getSlaQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				moduleWriterTimer = null;
				strSlaveEntry = null;
			}
		}
		
		printStopReason();
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("CollectorThreadController got stopped");
		
		super.finalize();
	}
}
