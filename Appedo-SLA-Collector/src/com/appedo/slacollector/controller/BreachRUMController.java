package com.appedo.slacollector.controller;


import java.sql.Connection;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SlaManager;

public class BreachRUMController extends Thread {
	
	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;
	
	SlaManager slaManager = null ;
	
	JSONObject joRUMBreachedData = null;
	
	
	public BreachRUMController(long lUserID, long lSlaID, JSONObject joBreachedData, long lTimeAppedoQueuedOn) {
		this.lSlaID = lSlaID;
		this.lUserID = lUserID;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		this.joRUMBreachedData = joBreachedData;
		
		establishDBonnection();
		
		slaManager = new SlaManager();
	}
	
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBonnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		//String strSlaType = null;
		String strMessage = null;
		
		HashMap<String, Integer> hmSLASettings = null;
		HashMap<String, String> hmMessageDetails = null;
		
		boolean bSLABreached = false;
		
		RUMAlertActionThread rumBreachActionThread = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			// gets user's SLA settings 
			hmSLASettings = slaManager.getSlaSettings(con, lUserID);
			
			// checks SLA has breached, based on user SLA settings
			bSLABreached = slaManager.isRUMBreached(con, hmSLASettings, lUserID, lSlaID, joRUMBreachedData.getInt("min_breach_count"));
			if ( bSLABreached ) {
				// if alert type is alert & heal then get the actions
				//strSlaType = "Alert";
				
				if ( ! slaManager.isSLAAlerted(con, lUserID, lSlaID, hmSLASettings.get("alertinmin"), "") ) {
					// send alert to user 
					rumBreachActionThread = new RUMAlertActionThread(lUserID, lSlaID, joRUMBreachedData, lTimeAppedoQueuedOn);
				} else {
					// gets uid's alert message dynamic content details,
					hmMessageDetails = slaManager.getRUMAlertMessageDetails(con, joRUMBreachedData.getLong("uid"));
					if ( hmMessageDetails == null ) {
						// would have been deleted
						throw new Exception("Uid not found, module has been deleted.");
					}
					strMessage = "RUM \""+hmMessageDetails.get("moduleName")+"\" is breached.";
					
					// insert into `sla_alert_log_<user_id>`
					slaManager.insertSLAAlertLog(con, lUserID, lSlaID, lTimeAppedoQueuedOn, strMessage, "", false, false, "", "");
				}
			}
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			strMessage = null;
		}
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		//System.out.println("SlaCollectorThreadController :  finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
