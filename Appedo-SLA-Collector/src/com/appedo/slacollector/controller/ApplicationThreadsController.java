package com.appedo.slacollector.controller;

import java.util.ArrayList;

import com.appedo.manager.LogManager;

public class ApplicationThreadsController extends Thread {

	public static ArrayList<ApplicationThreadsController> alAllThreadsInApplication = new ArrayList<ApplicationThreadsController>();
	
	private boolean bStopThread = false;
	
	public ApplicationThreadsController() {
		alAllThreadsInApplication.add( this );
	}
	
	public void sendStopSignal(boolean bStop) {
		bStopThread = bStop;
	}
	
	protected boolean getWorkSignal() {
		return ! bStopThread;
	}
	
	protected boolean getStopSignal() {
		return bStopThread;
	}
	
	protected void printStopReason() {
		if( getStopSignal() ) {
			LogManager.infoLog("RUMCollectorThreadController is stopped due to Stop-Signal");
		} else {
			LogManager.errorLog("RUMCollectorThreadController is stopped without Stop-Signal");
		}
	}
	
	public static void stopAllThreads() {
		for( ApplicationThreadsController th: alAllThreadsInApplication ) {
			th.sendStopSignal(true);
		}
	}
}
