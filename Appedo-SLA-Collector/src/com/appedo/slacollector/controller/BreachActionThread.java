package com.appedo.slacollector.controller;

import java.sql.Connection;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SlaManager;

public class BreachActionThread extends Thread {
	
	private Connection con = null;
	
	long lUserID = 0l;
	Long lSlaID = 0l;
	long lAlertLogId = 0l;
	long lTimeAppedoQueuedOn = 0L;
	int nAlertInMin = 0;
	
	BreachActionThread(long lUserID, Long lSlaID, long lAlertLogId, long lTimeAppedoQueuedOn, int nAlertInMin) {
		this.lUserID = lUserID;
		this.lSlaID = lSlaID;
		this.lAlertLogId = lAlertLogId;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		this.nAlertInMin = nAlertInMin;
		
		establishDBConnection();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		// send email alert(
		// create child thread for action
		SlaManager slaManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			slaManager = new SlaManager();
			
			slaManager.sendAlerts(con, lUserID, lSlaID, lAlertLogId, nAlertInMin);
			
			slaManager = null;
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			slaManager = null;
		}
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("BreachActionThread : finalize ");
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
