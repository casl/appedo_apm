package com.appedo.slacollector.controller;

import java.util.Date;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.AVMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.timer.AVMWriterTimer;

public class AVMCollectorThreadController extends ApplicationThreadsController {
	
	private CollectorManager collectorManager = null;
	int checkCount = 0;
	public AVMCollectorThreadController() {
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	@Override
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		AVMWriterTimer avmWriterTimer = null;
		AVMCollectorBean cbAVMEntry = null;	
		
		while( getWorkSignal() ) {
			try{
				cbAVMEntry = collectorManager.pollAVMBean();
				
				// take one-by-one bean from the queue
				if( cbAVMEntry != null ) {
					avmWriterTimer = new AVMWriterTimer(cbAVMEntry);
					avmWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getAVMQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				avmWriterTimer = null;
				cbAVMEntry = null;
				LogManager.logMethodEnd(dateLog);
			}
		}
		
		printStopReason();
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("AVMCollectorThreadController got stopped");
		
		super.finalize();
	}
}
