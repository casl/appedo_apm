package com.appedo.slacollector.controller;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.SlaCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.timer.ModuleWriterTimer;

public class SlaCollectorThreadController extends ApplicationThreadsController {
	
	private CollectorManager collectorManager = null;
	
	public SlaCollectorThreadController() {
		collectorManager = CollectorManager.getCollectorManager();
		
		setName("SlaCollectorThreadController");
	}
	
	@Override
	public void run() {
		ModuleWriterTimer moduleWriterTimer = null;
		SlaCollectorBean cbSlaEntry = null;
		
		while( getWorkSignal() ) {
			try{
				cbSlaEntry = collectorManager.pollSlaBean();
				
				// take one-by-one bean from the queue
				if( cbSlaEntry != null ) {
					moduleWriterTimer = new ModuleWriterTimer(cbSlaEntry);
					moduleWriterTimer.start();
				}
				
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getSlaQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				moduleWriterTimer = null;
				cbSlaEntry = null;
			}
		}
		
		printStopReason();
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("SLACollectorThread-Controller got stopped");
		
		super.finalize();
	}
}
