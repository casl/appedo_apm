package com.appedo.slacollector.controller;

import java.sql.Connection;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.commons.utils.UtilsFactory;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.dbi.AlertLogServiceDBI;
import com.appedo.slacollector.manager.SlaManager;

public class AlertLogThreadController extends Thread {
	JSONObject joUserBreachDet = null;
	AlertLogServiceDBI alertLogServiceDBI = null;
	Connection con = null;
	
	public AlertLogThreadController(JSONObject joUserBreachDet) {
		this.joUserBreachDet = joUserBreachDet;
		alertLogServiceDBI = new AlertLogServiceDBI();
	}
	
	public void run() {
		JSONArray jaSLAAlert = null;
		JSONArray jaSLAAlertLog = null;
		JSONObject joSLAAlert = null;
		JSONObject joSLAAlertLog = null;
		
		try {
			con = DataBaseManager.giveConnection();
			
			if (joUserBreachDet.getLong("oadBreachedMaxId") > 0) {
				jaSLAAlertLog = alertLogServiceDBI.addBreachedToAlert(con, joUserBreachDet.getLong("userId"), joUserBreachDet.getLong("oadBreachedMaxId"));
				
				// send SMS, for each entry of the SLA-Alert-Log entry added.
				for(int idx=0; idx < jaSLAAlertLog.size(); idx++ ) {
					joSLAAlertLog = jaSLAAlertLog.getJSONObject(idx);
					
					(new SlaManager()).sendAlerts(con, joUserBreachDet.getLong("userId"), joSLAAlertLog.getLong("sla_id"), joSLAAlertLog.getLong("alert_log_id"), joSLAAlertLog.getInt("alert_freq_min"));
				}
			}
			
			if (joUserBreachDet.getLong("logBreachedMaxId") > 0) {
				//addBreachedLogToAlert
				jaSLAAlertLog = alertLogServiceDBI.addBreachedLogToAlert(con, joUserBreachDet.getLong("userId"), joUserBreachDet.getLong("logBreachedMaxId"));
				//need to confirm sending SMS required for Log module(Alerts)..
				
				for(int idx=0; idx < jaSLAAlertLog.size(); idx++ ) {
					joSLAAlertLog = jaSLAAlertLog.getJSONObject(idx);
					
					(new SlaManager()).sendLogAlerts(con, joUserBreachDet.getLong("userId"), joSLAAlertLog.getLong("sla_id"), joSLAAlertLog.getLong("alert_log_id"), joSLAAlertLog.getInt("alert_freq_min"));
				}
			}
			
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			alertLogServiceDBI = null;
			
			UtilsFactory.clearCollectionHieracy( jaSLAAlertLog );
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("AlertLogThreadController : finalize");
		
		super.finalize();
	}
}
