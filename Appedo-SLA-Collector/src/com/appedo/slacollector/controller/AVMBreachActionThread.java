package com.appedo.slacollector.controller;

import java.sql.Connection;
import java.util.Date;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.AVMBreachManager;

public class AVMBreachActionThread extends Thread {
	
	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;
	JSONObject joAVMBreachDetails = null;

	AVMBreachActionThread(JSONObject joAVMBreachDetails, long lTimeAppedoQueuedOn) {
		this.joAVMBreachDetails = joAVMBreachDetails;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		
		establishDBConnection();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		// send email alert
		// create child thread for action
		Date dateLog = LogManager.logMethodStart();
		AVMBreachManager avmBreachManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			avmBreachManager = new AVMBreachManager();
			
			// Send Alert
			avmBreachManager.sendAlerts(con, joAVMBreachDetails, lTimeAppedoQueuedOn);
			
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("AVMBreachActionThread :  finalize ");
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
