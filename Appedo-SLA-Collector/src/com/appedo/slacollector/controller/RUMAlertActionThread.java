package com.appedo.slacollector.controller;

import java.sql.Connection;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.RUMManager;

public class RUMAlertActionThread extends Thread {

	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;

	JSONObject joRUMBreachedData = null;
	
	
	RUMAlertActionThread(long lUserID, long lSlaID, JSONObject joBreachedData, long lTimeAppedoQueuedOn) {
		this.lSlaID = lSlaID;
		this.lUserID = lUserID;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		this.joRUMBreachedData= joBreachedData;
		
		establishDBonnection();
		
		start();
	}
	
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBonnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		// send email alert(
		// create child thread for action
		RUMManager rumManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			rumManager = new RUMManager();
			
			//System.out.println("B4 sendAlerts;");
			rumManager.sendAlerts(con, lUserID, lSlaID, joRUMBreachedData, lTimeAppedoQueuedOn);
			
		} catch (Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			rumManager = null;
		}
	}

	
	@Override
	protected void finalize() throws Throwable {
		//System.out.println("SlaCollectorThreadController :  finalize " );
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
