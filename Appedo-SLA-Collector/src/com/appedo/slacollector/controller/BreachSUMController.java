package com.appedo.slacollector.controller;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.manager.SUMManager;
import com.appedo.slacollector.manager.SlaManager;
import com.appedo.slacollector.utils.UtilsFactory;

public class BreachSUMController extends Thread {
	
	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;
	long lminBrchCount = 0l;
	
	SlaManager slaManager = null ;
	JSONObject joSUMBreachDetails = null;
	
	public BreachSUMController(JSONObject joSUMBreachDetails, long lTimeAppedoQueuedOn) {
		this.joSUMBreachDetails = joSUMBreachDetails;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		
		this.lSlaID = joSUMBreachDetails.getLong("sla_id");
		this.lUserID = joSUMBreachDetails.getLong("userid");
		
		establishDBConnection();
		
		this.slaManager = new SlaManager();
	}

	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			//System.out.println("SlaCollectorThreadController :  OPEN " );
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		
		HashMap<String, Integer> hmSLASettings = null;
		
		String strBodyMessage = "";
		
		JSONObject joSUMTestDetails = null;
		
		boolean bSLABreached = false;

		SUMBreachActionThread bat = null;
		SUMManager sumManager = null;
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			// gets user's SLA settings 
			hmSLASettings = slaManager.getSlaSettings(con, lUserID);
			bSLABreached = slaManager.isSUMBreached(con, hmSLASettings, lUserID, lSlaID, joSUMBreachDetails.getLong("min_breach_count"), joSUMBreachDetails.getString("location"));
			
			// if all counters are breached then update 
			// alert & heal log based on the sla type
			if( bSLABreached ) {
				//strSlaType = "Alert";
				//if(strSlaType.trim()!=null && strSlaType.trim().equalsIgnoreCase("Alert")){
					// update alert log table
//					sbBodyMessage = sm.getBreachedCounterNames(lUserID, lSlaID);
					
					if( ! slaManager.isSLAAlerted(con, lUserID, lSlaID, hmSLASettings.get("alertinmin"), "") ) {
						bat = new SUMBreachActionThread(joSUMBreachDetails, lTimeAppedoQueuedOn);
						bat.start();
					} else {
						sumManager = new SUMManager();
						
						// gets SUM test details
						joSUMTestDetails = sumManager.getSUMTestDetails(con, joSUMBreachDetails.getLong("sum_test_id"));
						// message
						strBodyMessage = "SUM \""+joSUMTestDetails.getString("sum_test_name")+"\" is breached in \""+joSUMBreachDetails.getString("location")+"\".";
						
						slaManager.insertSLAAlertLog(con, lUserID, lSlaID, lTimeAppedoQueuedOn, strBodyMessage, "", false, false, "", "");
					}
				//}
			}
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			UtilsFactory.clearCollectionHieracy(joSUMTestDetails);
			joSUMTestDetails = null;
			
			strBodyMessage = null;
			
			bat = null;
			sumManager = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("BreachSUMController :  finalize ");
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
