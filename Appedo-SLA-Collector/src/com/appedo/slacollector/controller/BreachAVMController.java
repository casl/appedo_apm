package com.appedo.slacollector.controller;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.manager.LogManager;
import com.appedo.slacollector.dbi.SlaCollectorDBI;
import com.appedo.slacollector.dbi.SlaDBI;
import com.appedo.slacollector.manager.AVMBreachManager;
import com.appedo.slacollector.manager.SlaManager;
import com.appedo.slacollector.utils.UtilsFactory;

public class BreachAVMController extends Thread {
	private Connection con = null;
	
	long lSlaID = 0l;
	long lUserID = 0l;
	long lTimeAppedoQueuedOn = 0L;
	long lminBrchCount = 0l;
	
	SlaManager sm = null ;
	SlaDBI slaDBI= null;
	JSONObject joAVMBreachDetails = null;
	
	public BreachAVMController(JSONObject joAVMBreachDetails, long lTimeAppedoQueuedOn) {
		this.joAVMBreachDetails = joAVMBreachDetails;
		this.lTimeAppedoQueuedOn = lTimeAppedoQueuedOn;
		
		this.lSlaID = joAVMBreachDetails.getLong("sla_id");
		this.lUserID = joAVMBreachDetails.getLong("test_user_id");
		
		establishDBConnection();
		
		this.sm = new SlaManager();
	}

	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	private void establishDBConnection(){
		try{
			con = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		
		AVMBreachActionThread bat = null;	
		HashMap<String, Integer> hmSLASettings = null;
		
		boolean bBreached = false, bAgentDown = false;
		
		JSONObject joAVMDetails = null;
		
		SlaCollectorDBI slaCollectorDBI = null;
		
		StringBuilder sbMessage = new StringBuilder();
		
		try {
			// if connection is not established to db server then try re-connect continuously, with wait for 10 seconds
			con = DataBaseManager.reEstablishConnection(con);
			
			// check the Trigger alert frequency
			hmSLASettings = sm.getSlaSettings(con, lUserID);
			
			// if the Agent is Down then, no need to check the Min. Breach condition.
			if( joAVMBreachDetails.get("breached_severity").equals("AGENT_DOWN") ) {
				bBreached = bAgentDown = true;
			} else {
				bBreached = sm.isAVMBreached(con, hmSLASettings, joAVMBreachDetails.getLong("userid"), joAVMBreachDetails.getLong("sla_id"), joAVMBreachDetails.getLong("min_breach_count"), joAVMBreachDetails.getString("country"), joAVMBreachDetails.getString("state"), joAVMBreachDetails.getString("city"), joAVMBreachDetails.getString("region"), joAVMBreachDetails.getString("zone"));
			}
			
			// if all counters are breached then update 
			// alert & heal log based on the SLA type
			if( bBreached ) {
				// update alert log table
				// if the Agent is Down then, no need to check whether Alert already sent.
				if( bAgentDown || ! sm.isSLAAlerted(con, joAVMBreachDetails.getLong("userid"), joAVMBreachDetails.getLong("sla_id"), hmSLASettings.get("alertinmin"), "") ) {
					bat = new AVMBreachActionThread(joAVMBreachDetails, lTimeAppedoQueuedOn);
					bat.start();
				} else {
					slaCollectorDBI = new SlaCollectorDBI();
					
					// gets AVM's test details 
					joAVMDetails = slaCollectorDBI.getAVMTestDetails(con, joAVMBreachDetails.getLong("avm_test_id"));
					sbMessage.append("AVM Alert! ")
							.append(joAVMBreachDetails.getString("breached_severity"))
							.append("\nTest Name: ").append(joAVMDetails.getString("avm_test_name"))
							.append("\nLocation: ").append(AVMBreachManager.getLocationText(joAVMBreachDetails));
					
					// inserts into `sla_alert_log_<user_id>`
					sm.insertSLAAlertLog(con, lUserID, lSlaID, lTimeAppedoQueuedOn, sbMessage.toString(), "", false, false, "", "");
				}
			}
		} catch(Throwable t) {
			LogManager.errorLog(t);
		} finally {
			DataBaseManager.close(con);
			con = null;
			
			bat = null;
			slaCollectorDBI = null;
			
			UtilsFactory.clearCollectionHieracy(sbMessage);
			sbMessage = null;
			
			LogManager.logMethodEnd(dateLog);
		}
	}
}
