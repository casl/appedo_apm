package com.appedo.slacollector.controller;

import java.util.Date;

import com.appedo.manager.LogManager;
import com.appedo.slacollector.bean.RUMCollectorBean;
import com.appedo.slacollector.common.Constants;
import com.appedo.slacollector.manager.CollectorManager;
import com.appedo.slacollector.timer.RUMWriterTimer;

public class RUMCollectorThreadController extends ApplicationThreadsController {
	
	private CollectorManager collectorManager = null;
	int checkCount = 0;
	public RUMCollectorThreadController() {
		collectorManager = CollectorManager.getCollectorManager();
	}
	
	@Override
	public void run() {
		Date dateLog = LogManager.logMethodStart();
		RUMWriterTimer rumWriterTimer = null;
		RUMCollectorBean cbRUMEntry = null;
		
		while( getWorkSignal() ) {
			try{
				cbRUMEntry = collectorManager.pollRUMBreachBean();
				
				// take one-by-one bean from the queue
				if( cbRUMEntry != null ) {
					rumWriterTimer = new RUMWriterTimer(cbRUMEntry);
					rumWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getRUMBreachesQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				rumWriterTimer = null;
				cbRUMEntry = null;
				LogManager.logMethodEnd(dateLog);
			}
		}
		
		printStopReason();
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("CollectorThreadController got stopped");
		
		super.finalize();
	}
}
