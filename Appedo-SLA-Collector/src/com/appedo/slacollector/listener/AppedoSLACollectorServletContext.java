package com.appedo.slacollector.listener;

import java.util.Timer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.appedo.slacollector.controller.ApplicationThreadsController;
import com.appedo.slacollector.servlet.InitServlet;


/**
 * This class will handle the operations required to be done when the application is started or stopped in Tomcat.
 * 
 * @author veeru 
 *
 */
public class AppedoSLACollectorServletContext implements ServletContextListener	{

	private ServletContext context = null;
	
	/**
	 * This method is invoked when the Web Application
	 * is ready to service requests
	 */
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		
		//Output a simple message to the server's console
		System.out.println("The SSProfiler Web App is Ready in "+context.getServerInfo());
	}
	
	/**
	 * Stop all the timers when this project is stopped or tomcat is stopped
	 */
	public void contextDestroyed(ServletContextEvent event) {	
		
		try{
			System.out.println("contextDestroyed");
			
			ApplicationThreadsController.stopAllThreads();
			
			
			for( Timer applicationTimer: InitServlet.alAllTimerInApplication ) {
				applicationTimer.cancel();
				applicationTimer.cancel();
				applicationTimer.purge();
			}
		} catch (Exception e) {
			System.out.println("Exception in MySQL timer stop: "+e.getMessage());
		}
		
		System.out.println("The Appedo Collector Web App has Been Removed.");
		this.context = null;
	}
}
