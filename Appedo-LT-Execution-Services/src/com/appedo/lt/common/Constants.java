package com.appedo.lt.common;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author navin
 *
 */
public class Constants {
	
	/**
	 * If "ENVIRONMENT" config-porperty is "DEVELOPMENT" in both DB & appedo_config.properties file then,
	 * it is Development/Testing Environment. So give priority to the properties available in appedo_config.properties file.
	 */
	public static boolean DEV_ENVIRONMENT = false;
	
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	public static String SMTP_MAIL_CONFIG_FILE_PATH = "";
	public static String DELAY_SAMPLING = "";
	
	public static String PATH = ""; 
	public static String VUSCRIPTSPATH = "";
	public static String JMETERVUSCRIPTSFOLDERPATH = "";
	public static String JMETERVUSCRIPTSPATH = "";
	public static String FLOODGATESVUSCRIPTSPATH = "";
	public static String FLOODGATESSCENARIOXMLPATH = "";
	public static String FLOODGATESSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLPATH = "";
	public static String JMETERSCENARIOFOLDERPATH = "";
	public static String JMETERSCENARIOSPATH = "";
	public static String JMETERSCENARIOSPATHFORSHIPPER = "";
	public static String CSVFILE = "";
	public static String UPLOADPATH = "";
	public static String VARIABLEPATH = "";
	public static String SUMMARYREPORTPATH = "";
	public static String VARIABLEXMLPATH = "";
	public static String VARIABLEXMLFOLDERPATH = "";
	public static String JMETERTESTPATH = "";
	public static String JMETERTESTJTLPATH = "";
	public static String JMETERCSVPATH = "";
	public static String JMETERSUMMARYREPORTPATH="";
	public static String DOWNLOADS = "";
	
	public static String SELENIUM_SCRIPT_CLASS_FILE_PATH = "";
	public static String FROMEMAILADDRESS = "";

	public static String LT_QUEUE_IP = "";
	public static String LT_PROCESSING_IP = "";
	public static String FG_CONTROLLER_IP = "";
	public static String FG_CONTROLLER_PORT = "";
	public static String LT_QUEUE_PORT = "";
	public static String LT_PROCESSING_PORT = "";
	public static String FG_CONTROLLER_PORT1 = "";
	public static String FG_APPLICATION_IP = "";
	
	public static String JM_CONTROLLER_IP= "";
	public static String JM_CONTROLLER_PORT = "";

	public static String APPEDO_URL = "";
	public static String LT_EXECUTION_SERVICES_FAILED = "";
	public static String WEBSERVICE_HOST = "";
	public static String FLOODGATESVUSCRIPTSFOLDERPATH = "";
	public static String FLOODGATESVUSCRIPTSTEMPFOLDERPATH = "";
	public static String APPEDO_SCHEDULER = "";
	public static String LICENSEPATH = "";
	public static String LICENSEXMLPATH = "";
	public static String APPEDO_HARFILES = "";
	public static String HAR_REPOSITORY_URL = "";
	public static String APPEDO_COLLECTOR = "";
	public static String MAX_COUNTERS = "";
	public static String MAX_LOADGEN_CREATION = "";
	public static String DEFAULT_LOADGEN = "";
	public static HashMap<String, String> AGENT_LATEST_BUILD_VERSION = new HashMap<String, String>();
	public static String FG_VUSCRIPTS_TEMP_PATH = "";
	public static String APPEDO_URL_FILE_TRANSFER = "";
	public static String JMX_FILE_UPLOAD = "";
	//public static JSONObject LOAD_TEST_LICENSE_DETAILS = new JSONObject();
	
	public static String GRAFANA_DASHBOARD_DEFAULT_SLUG = null;
	
	public static String GRAFANA_COLLECTOR_PROTOCAL = null;
	public static String GRAFANA_COLLECTOR_IP = null;
	public static Integer GRAFANA_COLLECTOR_PORT = null;
	public static String GRAFANA_LOADGEN_PROTOCAL = null;
	public static Integer GRAFANA_LOADGEN_PORT = null;
	
	// Limitation for Counter Charts time window
	public static int COUNTER_CHART_START_DELAY = 60;//62;
	public static int COUNTER_CHART_END_DELAY = 0;//2;
	public static int COUNTER_MINICHART_START_DELAY = 10;//12;
	public static int COUNTER_MINICHART_END_DELAY = 0;//2;
	
	// tried to append 0 for diff between the prev result time and current result time is > 3 min
	public static final int COUNTER_CHART_TIME_INTERVAL = 3;
	public static final int COUNTER_CHART_ADD_0_FOR_EVERY = 3;
	
	public static String CLASS_EXPORT_URL;
	
	public static String SELENIUM_SCRIPT_PACKAGES, DEFAULT_LT_THREADPOOL_NAME, MAX_THREADS_RETAINED;
	
	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";
	
	public static boolean IS_CAPTCHA_VALIDATION_ENABLE = true;
	
	public static HashMap<String, String> MINICHART_COUNTERS = new HashMap<String, String>();
	
	public static AtomicLong count = new AtomicLong(1);
	
	private static String PASSWORD_SALT_KEY = "$2a$31$BQOhDZT/Ppu69lJNNiIRee";
	
	/*public enum AGENT_TYPE {
	TOMCAT_7X("TOMCAT 7.X", "tomcat_performance_counter"), JBOSS("JBOSS", "jboss_performance_counter"), MSIIS("MSIIS", "msiis_performance_counter"), 
	JAVA_PROFILER("JAVA_PROFILER", "tomcat_profiler"), 
	LINUX("LINUX", "linux_performance_counter"), MSWINDOWS("MSWINDOWS", "windows_performance_counter"), 
	MYSQL("MYSQL", "mysql_performance_counter"), MSSQL("MSSQL", "mssql_performance_counter");
	
	private String strAgentType, strTableName;
	
	private AGENT_TYPE(String agentType, String tableName) {
		strAgentType = agentType;
		strTableName = tableName;
	}
	
	public void setMySQLVersion(String strVersionNo) {
		strAgentType = "MYSQL "+strVersionNo;
	}
	
	public String getTableName() {
		return strTableName;
	}
	
	@Override
	public String toString() {
		return strAgentType;
	}
}*/
	
	public static long getLongCount(){
		return count.incrementAndGet();
	}
	
	public enum CHART_START_INTERVAL {
		_1("3 hours"), _2("24 hours"), _3("7 days"), _4("15 days"), _5("30 days");
		
		private String strInterval;
		
		private CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	/**
	 * for SUM chart results interval
	 * @author navin
	 *
	 */
	public enum SUM_CHART_START_INTERVAL {
		_1("24 hours"), _2("7 days"), _3("15 days"), _4("30 days"), _5("60 days"), _6("120 days"), _7("180 days");
		
		private String strInterval;
		
		private SUM_CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	/**
	 * for RUM charts interval
	 * @author navin
	 *
	 */
	/*public enum RUM_CHART_START_INTERVAL {
		_1("24 hours"), _2("7 days"), _3("15 days"), _4("30 days"), _5("60 days"), _6("120 days"), _7("180 days");
		
		private String strInterval;
		
		private RUM_CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}*/
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(srtConstantsPath);
			prop.load(is);
			
			// Appedo application's resource directory path
			RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
			
			APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
			
			PATH = RESOURCE_PATH+prop.getProperty("Path");
			
			FLOODGATESVUSCRIPTSFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptsfolderpath");
			FLOODGATESVUSCRIPTSTEMPFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptstempfolderpath");
			VUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("VUScriptspath");
			FLOODGATESVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptspath");
			JMETERVUSCRIPTSFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptsfolderpath");
			JMETERVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptspath");
			FLOODGATESSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("floodgatesscenarioxmlpath");
			FLOODGATESSCENARIOXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesscenarioxmlfolderpath");
			JMETERSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlpath");
			JMETERSCENARIOXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlfolderpath");
			JMETERSCENARIOFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariofolderpath");
			CSVFILE = RESOURCE_PATH+prop.getProperty("Csvfile");
			UPLOADPATH = RESOURCE_PATH+prop.getProperty("Uploadpath");
			VARIABLEPATH = RESOURCE_PATH+prop.getProperty("Variablepath");
			SUMMARYREPORTPATH = RESOURCE_PATH+prop.getProperty("summaryreportpath");
			VARIABLEXMLPATH = RESOURCE_PATH+prop.getProperty("VariableXMLpath");
			VARIABLEXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("VariableXMLFolderpath");
			JMETERSCENARIOSPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariospath");
			JMETERSCENARIOSPATHFORSHIPPER = prop.getProperty("jmeterscenariospath");
			JMETERTESTPATH = RESOURCE_PATH+prop.getProperty("jmetertestpath");
			JMETERTESTJTLPATH = RESOURCE_PATH+prop.getProperty("jmetertestjtlpath");
			DOWNLOADS = RESOURCE_PATH+prop.getProperty("downloads");
			JMETERCSVPATH = RESOURCE_PATH+prop.getProperty("jmetercsvpath");
			JMETERSUMMARYREPORTPATH = RESOURCE_PATH+prop.getProperty("jmetersummaryreportpath");
			LICENSEPATH = RESOURCE_PATH+prop.getProperty("licensepath");
			LICENSEXMLPATH = RESOURCE_PATH+prop.getProperty("licensexmlpath");
			MAX_COUNTERS = prop.getProperty("max_counters");
			DELAY_SAMPLING = prop.getProperty("DELAY_SAMPLING");
			MAX_LOADGEN_CREATION = prop.getProperty("MAX_LOADGEN_CREATION");
			DEFAULT_LOADGEN = prop.getProperty("DEFAULT_LOADGEN");
			FG_VUSCRIPTS_TEMP_PATH = RESOURCE_PATH+prop.getProperty("FG_VUSCRIPTS_TEMP_PATH");
			// Mail configuration 
			SMTP_MAIL_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("SMTP_MAIL_CONFIG_FILE_PATH");
			FROMEMAILADDRESS = prop.getProperty("FromEmailAddress");
			JMX_FILE_UPLOAD = prop.getProperty("jmxfileupload");
			
			Constants.SELENIUM_SCRIPT_CLASS_FILE_PATH = Constants.RESOURCE_PATH+prop.getProperty("sumtransactionfilepath");
			LOG4J_PROPERTIES_FILE = RESOURCE_PATH+prop.getProperty("LOG4J_CONFIG_FILE_PATH");
			SELENIUM_SCRIPT_PACKAGES = prop.getProperty("seleniumscriptpackages"); 
			DEFAULT_LT_THREADPOOL_NAME = prop.getProperty("DEFAULT_LT_THREADPOOL_NAME");
			MAX_THREADS_RETAINED = prop.getProperty("MAX_THREADS_RETAINED");
			
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Loads appedo config properties, from the system specifed path, 
	 * (Note.. loads other than db related)
	 * 
	 * @param strAppedoConfigPath
	 */
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			if( prop.getProperty("ENVIRONMENT") != null && prop.getProperty("ENVIRONMENT").equals("DEVELOPMENT") 
					&& AppedoConstants.getAppedoConfigProperty("ENVIRONMENT") != null && AppedoConstants.getAppedoConfigProperty("ENVIRONMENT").equals("DEVELOPMENT") ) 
			{
				DEV_ENVIRONMENT = true;
			}
			
			// Load Test configuration & LoadGen Agent details
			FG_CONTROLLER_IP = getProperty("FG_CONTROLLER_IP",prop);
			LT_QUEUE_IP = getProperty("LT_QUEUE_IP",prop);
			LT_PROCESSING_IP = getProperty("LT_PROCESSING_IP",prop);
			FG_CONTROLLER_PORT = getProperty("FG_CONTROLLER_PORT",prop);
			FG_CONTROLLER_PORT1 = getProperty("FG_CONTROLLER_PORT1",prop);
			LT_QUEUE_PORT = getProperty("LT_QUEUE_PORT",prop);
			LT_PROCESSING_PORT = getProperty("LT_PROCESSING_PORT",prop);
			FG_APPLICATION_IP = getProperty("FG_APPLICATION_IP",prop);

			JM_CONTROLLER_IP = getProperty("JM_CONTROLLER_IP",prop);
			JM_CONTROLLER_PORT = getProperty("JM_CONTROLLER_PORT",prop);
			
			// User's email address verification mail's URL link starter
			APPEDO_URL = getProperty("APPEDO_URL",prop);
			LT_EXECUTION_SERVICES_FAILED = getProperty("LT_EXECUTION_SERVICES_FAILED",prop);
			APPEDO_URL_FILE_TRANSFER = getProperty("APPEDO_URL_FILE_TRANSFER",prop);
			
			// Url to delete har files from har repository
			HAR_REPOSITORY_URL = getProperty("HAR_REPOSITORY_URL",prop);
			
			// WebService collector
			WEBSERVICE_HOST = getProperty("WEBSERVICE_HOST",prop);
			APPEDO_SCHEDULER = getProperty("APPEDO_SCHEDULER",prop);
			APPEDO_HARFILES = getProperty("APPEDO_HARFILES",prop);
			CLASS_EXPORT_URL = getProperty("URL_TO_EXPORT_CLASS_FILE",prop);
			APPEDO_COLLECTOR = getProperty("APPEDO_COLLECTOR",prop);
			
			GRAFANA_DASHBOARD_DEFAULT_SLUG = getProperty("GRAFANA_SQLITE_DB_DEFAULT_SLUG",prop);
			
			GRAFANA_COLLECTOR_PROTOCAL = getProperty("GRAFANA_COLLECTOR_PROTOCAL",prop);
			GRAFANA_COLLECTOR_IP = getProperty("GRAFANA_COLLECTOR_IP",prop);
			GRAFANA_COLLECTOR_PORT = Integer.parseInt( getProperty("GRAFANA_COLLECTOR_PORT",prop) );
			GRAFANA_LOADGEN_PROTOCAL = getProperty("GRAFANA_LOADGEN_PROTOCAL",prop);
			GRAFANA_LOADGEN_PORT = Integer.parseInt( getProperty("GRAFANA_LOADGEN_PORT",prop) );
			
			IS_CAPTCHA_VALIDATION_ENABLE = Boolean.parseBoolean( UtilsFactory.replaceNull(getProperty("IS_CAPTCHA_VALIDATION_ENABLE",prop), "true") );
		} catch(Throwable th) {
			throw th;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param con
	 * @param strAppedoConfigPath - Not in use
	 * @throws Throwable
	 */
	public static void loadAppedoConstants(Connection con) throws Throwable {
		
		try {
			// 
			AppedoConstants.getAppedoConstants().loadAppedoConstants(con);
			
		} catch (Throwable th) {
			throw th;
		}
	}
	
	/**
	 * Get the property's value from appedo_config.properties, if it is DEV environment;
	 * Otherwise get the property's value from DB.
	 * 
	 * @param strPropertyName
	 * @param prop
	 * @return
	 */
	private static String getProperty(String strPropertyName, Properties prop) throws Throwable {
		if( DEV_ENVIRONMENT && prop.getProperty(strPropertyName) != null )
			return prop.getProperty(strPropertyName);
		else
			return AppedoConstants.getAppedoConfigProperty(strPropertyName);
	}
	
	public static String getPasswordSaltKey() {
		return PASSWORD_SALT_KEY;
	}
}
