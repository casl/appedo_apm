package com.appedo.lt.controller;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.appedo.commons.bean.LoginUserBean;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.JMeterExecutionManager;
import com.appedo.lt.model.JmeterScriptManager;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.tcpserver.FloodgatesXMLAppend;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * to get the script details from scenarios xml which is located in the \
 * appedo repo
 * @author Anand
 *
 */
public class LTExecutionController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private long tNo;
	//static Properties prop = new Properties();
	/**
	 * do when post request comes
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	
	/**
	 * do when post request comes
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * To get the floodgates scripts from the scenario's xml
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		response.setContentType("text/html");
		String strActionCommand = request.getRequestURI();
		
		if(strActionCommand.endsWith("/ltScheduler/runScenario")) {
			Connection con = null;
			JSONObject joMessage = null;
			JSONObject joUserRunningScenario = null;
			LoginUserBean loginUserBean = null;
			RunManager runManager = null;
			JSONObject joLicense = null;
			long nUserId = 0;
//			int nMaxRuncount = 0;
			int nMaxRuncountperday = 0;
			int nUserTotalRuncount = 0;
			int nUserTotalRundaycount = 0;
			
			String strTestType =  null;
			String strRunType = null;
			String loadgenRegions = null;
			String loadgenDistributions = null;
			String strScenarioId = null;
			String strScenarioName = null;
			String strReportName = null;
			String strScriptName = null;
			String strScriptId = null;
			String guids = null;
			String userAgent = null;
			
			JSONArray jaScriptDetails = null;
			
			try	{
				con = DataBaseManager.giveConnection();
				
				tNo = Constants.getLongCount();
				LogManager.infoLog("Opened Connection count in runScenario: "+tNo);
				LoadTestSchedulerBean schedulerBean = new LoadTestSchedulerBean();
				LTScheduler ltScheduler = new LTScheduler();
				
				loginUserBean = new LoginUserBean();
				loginUserBean.fromJSONObject( JSONObject.fromObject(request.getParameter("login_user_bean")));
				runManager = new RunManager();
				
				joLicense = runManager.loadLoadTestLicenseDetails(con, loginUserBean);
//				nMaxRuncount = Integer.parseInt(joLicense.getString("maxruncount"));
				nMaxRuncountperday = joLicense.getInt("maxruncountperday");
				
				strTestType =  request.getParameter("testType");
				strRunType = request.getParameter("runType");
				loadgenRegions = request.getParameter("regions");
				loadgenDistributions = request.getParameter("distributions");
				strScenarioId = request.getParameter("scenarioId");
				strScenarioName = request.getParameter("scenarioName");
				strReportName = request.getParameter("reportName");
				int nMaxUser = Integer.parseInt(request.getParameter("maxUserCount"));
				userAgent = request.getParameter("userAgent");
				int startMonitor = Integer.parseInt(request.getParameter("startMonitor"));
				int endMonitor = Integer.parseInt(request.getParameter("endMonitor"));
				
				nUserId = loginUserBean.getUserId();
				guids = request.getParameter("guids");
				
				if(strTestType.equals("JMETER")){
					boolean isFirst = true;
					jaScriptDetails = runManager.getScriptDetails(con, Long.valueOf(strScenarioId));
					for( int i =0; i<jaScriptDetails.size(); i++ ){
						JSONObject joScript = jaScriptDetails.getJSONObject(i);
						if( isFirst ) {
							strScriptName = joScript.getString("scriptName");
							strScriptId = joScript.getString("scriptId");
							isFirst = false;
						} else{
							strScriptName = strScriptName+","+ joScript.getString("scriptName");
							strScriptId = strScriptId+","+ joScript.getString("scriptId");
						}
					}
					
					schedulerBean.setScriptId(strScriptId);
					schedulerBean.setScriptName(strScriptName);
				}
				nUserTotalRuncount = runManager.getUserMaxRunCount(con, loginUserBean);
				
//				if( nMaxRuncount <= nUserTotalRuncount ) {
//					throw new Exception("nMaxRuncount");
//			  	}
				
				nUserTotalRundaycount = 0;
				
				if( nMaxRuncountperday <= nUserTotalRundaycount ) {
					throw new Exception("nMaxRuncountPerDay");
			  	}
					try {
						
						schedulerBean.setLoadGenRegions(loadgenRegions);
						schedulerBean.setDistributions(loadgenDistributions);
						schedulerBean.setReportName(strReportName);
						schedulerBean.setScenarioId(strScenarioId);
						schedulerBean.setScenarioName(strScenarioName.toLowerCase());
						schedulerBean.setTestType(strTestType);
						schedulerBean.setUserId(nUserId);
						schedulerBean.setLicense(loginUserBean.getLicense());
						schedulerBean.setMaxUserCount(nMaxUser);
						schedulerBean.setUserAgent(userAgent);
						schedulerBean.setStartMonitor(startMonitor);
						schedulerBean.setEndMonitor(endMonitor);
						
						synchronized (ltScheduler) {
							joUserRunningScenario = runManager.isUserRunningScenario(con, loginUserBean, strTestType);
							if(joUserRunningScenario.containsKey("isActive") && joUserRunningScenario.getBoolean("isActive")){
								joMessage = UtilsFactory.getJSONFailureReturn("Another Run is in progress. Please wait!");
								
							}else{
								long lRunId = runManager.insertIntoReportMaster(con, schedulerBean, loginUserBean);
								schedulerBean.setRunId(lRunId);
//								String queueIP = "inactive";
//								while( i <= 3){
//								  // processingIP = runManager.getIPStatus(Constants.LT_PROCESSING_IP, Integer.valueOf(Constants.LT_PROCESSING_PORT));
//								  queueIP = runManager.getIPStatus(Constants.LT_QUEUE_IP, Integer.valueOf(Constants.LT_QUEUE_PORT));
//									if( queueIP.equalsIgnoreCase("active") ){
//										break;
//									}
//									i++;
//								}
//								if( queueIP.equalsIgnoreCase("active") ){
								
									LTScheduler.queueFGTest(schedulerBean);
									
//									if( loginUserBean.getLicense().equalsIgnoreCase("level0") ){
//										runManager.updatePrivateCounters(con, "lt_free_user_queue", ltScheduler.htFGTestQueues.size());
//									} else {
//										runManager.updatePrivateCounters(con, "lt_paid_user_queue", ltScheduler.htFGTestPaidQueues.size());
//									}
									if( guids.length()>0 ){
										runManager.insertAgentMapping(con, guids, lRunId);
									}
									joMessage = UtilsFactory.getJSONSuccessReturn("Your test is in queue, as soon as the Run starts, status will change to GREEN ");
//								} else {
//									runManager.updateProcessServiceFailed(con, "Queue Service is unavailable", lRunId);
//									joMessage = UtilsFactory.getJSONSuccessReturn("Unable to run Load Testing. Please contact Appedo");
//								}
								
							}
						}
						
					}catch(Throwable t) {
						t.printStackTrace();
						if(t.getMessage().equals("ReportNameExist")){
							joMessage = UtilsFactory.getJSONFailureReturn("Report Name already exist.");
						} else if(t.getMessage().equals("2")){
							joMessage = UtilsFactory.getJSONFailureReturn("License expired. Please contact appedo.com.");
						}
						
					}finally {
//						schedulerBean = null;
//						ltScheduler = null;
					}
			} catch(Exception e) {
				LogManager.errorLog(e);
				if(e.getMessage().equals("nMaxRuncountPerDay")){
					joMessage = UtilsFactory.getJSONFailureReturn("Total run count must not exceed "+nMaxRuncountperday+" for the day.");
				}else if(e.getMessage().equals("1")){
					joMessage = UtilsFactory.getJSONFailureReturn("Report name already exist.");
				} else {
					joMessage = UtilsFactory.getJSONFailureReturn("Exception occured while run: "+e.getMessage());
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joMessage = UtilsFactory.getJSONFailureReturn("Exception occured while run.\n" +t.getMessage());
				if( t.getMessage().equals("SESSION_EXPIRED") ) {
					throw new ServletException("SESSION_EXPIRED");
				}
			} finally {
				DataBaseManager.close(con);
				con = null;
				
				LogManager.infoLog("Closed Connection count in runScenario: "+tNo);
				
//				joUserRunningScenario = null;
//				loginUserBean = null;
//				runManager = null;
//				joLicense = null;
//				strTestType =  null;
//				strRunType = null;
//				loadgenRegions = null;
//				loadgenDistributions = null;
//				strScenarioId = null;
//				strScenarioName = null;
//				strReportName = null;
//				strScriptName = null;
//				strScriptId = null;
//				guids = null;
				
				response.getWriter().write(joMessage.toString());
			}
		} else if (strActionCommand.endsWith("/ltScheduler/deleteScript")){

			LoginUserBean loginUserBean = null;
			String strScriptId = null;
			String strScriptName = null;
			String strVUScriptsXmlPath = null;
			String strScenarioXmlPath = null;
			String strTestType = null;
			JSONObject joRtn = null, joRunningScenario = null;
			RunManager runManager = null;
			Connection con = null;
			try{
				loginUserBean = new LoginUserBean();
				loginUserBean.fromJSONObject( JSONObject.fromObject(request.getParameter("login_user_bean")));
				
				strScriptName = request.getParameter("scriptName");
				strScriptId = request.getParameter("scriptId");
				
				strTestType = request.getParameter("testTypeScript").trim();
				con = DataBaseManager.giveConnection();
				
				runManager = new RunManager();
				
				if ( strTestType.equalsIgnoreCase("APPEDO_LT") ) {
					strVUScriptsXmlPath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
					strScenarioXmlPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
				} else if ( strTestType.equalsIgnoreCase("JMETER") ) {
					strVUScriptsXmlPath = Constants.JMETERVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
					strScenarioXmlPath = Constants.JMETERSCENARIOXMLFOLDERPATH+loginUserBean.getUserId()+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
				}
				joRunningScenario = runManager.getUserRunningScenario(con, loginUserBean, strTestType);
				if(joRunningScenario.containsKey("reportName") && joRunningScenario.getString("reportName") != null ){
					joRtn = UtilsFactory.getJSONFailureReturn("Another Run is in progress. Please wait!");
				}else if(Integer.parseInt(joRunningScenario.getString("userQueueSize"))>0){
					joRtn = UtilsFactory.getJSONFailureReturn("You have a Run in Queue. Please wait!");
				}else{
					runManager.deleteScript(con, strScriptId, loginUserBean, strScriptName);
					joRtn = new FloodgatesXMLAppend().deleteScript(strScriptName, strVUScriptsXmlPath, strScenarioXmlPath, strTestType);
				}
				
			}catch(Exception e){
				LogManager.errorLog(e);
			}catch(Throwable t){
				LogManager.errorLog(t);
			}finally{
				DataBaseManager.close(con);
//				con = null;
//				loginUserBean = null;
//				strScriptId = null;
//				strScriptName = null;
//				strVUScriptsXmlPath = null;
//				strScenarioXmlPath = null;
//				strTestType = null;
//				joRunningScenario = null;
//				runManager = null;
				
				response.getWriter().write(joRtn.toString());
			}
		} else if(strActionCommand.endsWith("/ltScheduler/stopRunningScenario")){
			RunManager runManager = null;
			String runId = null;
			net.sf.json.JSONObject joMessage = null;
			try	{
				runManager = new RunManager();
				runId = request.getParameter("runId");
				joMessage = runManager.stopScenario(runId,Constants.FG_CONTROLLER_IP,Integer.parseInt(Constants.FG_CONTROLLER_PORT));
				
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = UtilsFactory.getJSONFailureReturn("Unable to stop.");
			}finally {
//				runManager = null;
//				runId = null;
				
				response.getWriter().write(joMessage.toString() );
			}
			
		} else if(strActionCommand.endsWith("/ltScheduler/stopJMeterTest")){
			RunManager runManager = null;
			String runId = null, runningIP = "";
			JSONObject joMessage = null;
			Connection con = null;
			
			try	{
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				
				runId = request.getParameter("runId");
				//joMessage = runManager.stopJMeterTest(runId,Constants.FG_CONTROLLER_IP,Integer.parseInt(Constants.FG_CONTROLLER_PORT));
				//isJMeterTestRunning
				runningIP = runManager.getJMeterRunningIP(con, runId);
				if (runningIP.trim().length() > 0) {
					joMessage = runManager.stopJMeterTest(runId,runningIP);
				} else {
					joMessage = UtilsFactory.getJSONFailureReturn("JMeter Test is not in Running state.");
				}
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = UtilsFactory.getJSONFailureReturn("Unable to stop.");
			}finally {
//				runManager = null;
//				runId = null;
				
				response.getWriter().write(joMessage.toString() );
			}
			
		} else if(strActionCommand.endsWith("/ltScheduler/deleteJMeterScript")){
			
			LoginUserBean loginUserBean = null;
			
			String strScenarioName = null, strScenarioId = null;
			String strVUScriptsXmlPath = null;
			String strScenarioXmlPath = null;
			String strTestType = null;
			String strScriptName = null, strScriptId = null;
			net.sf.json.JSONObject joRtn = null, joRunningScenario = null;
			RunManager runManager = null;
			Connection con = null;
			boolean isFirst = true;
			try{
				loginUserBean = new LoginUserBean();
				loginUserBean.fromJSONObject( JSONObject.fromObject(request.getParameter("login_user_bean")));
				
				strScenarioName = request.getParameter("scenarioName");
				strTestType = request.getParameter("testTypeScript").trim();
				strScenarioId = request.getParameter("scenarioId");
				// strScriptName = request.getParameter("scenarioName");
						
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				
				if ( strTestType.equalsIgnoreCase("APPEDO_LT") ) {
					strVUScriptsXmlPath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
					strScenarioXmlPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
				} else if ( strTestType.equalsIgnoreCase("JMETER") ) {
					strVUScriptsXmlPath = Constants.JMETERVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
					strScenarioXmlPath = Constants.JMETERSCENARIOXMLFOLDERPATH+loginUserBean.getUserId()+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
				}
				joRunningScenario = runManager.getUserRunningScenario(con, loginUserBean, strTestType);
				if(joRunningScenario.containsKey("reportName") && joRunningScenario.getString("reportName") != null ){
					joRtn = UtilsFactory.getJSONFailureReturn("Another Run is in progress. Please wait!");
				}else if(Integer.parseInt(joRunningScenario.getString("userQueueSize"))>0){
					joRtn = UtilsFactory.getJSONFailureReturn("You have a Run in Queue. Please wait!");
				}else{
					JSONArray jaScriptDetails = runManager.getScriptDetails(con, Long.valueOf(strScenarioId));
					for( int i =0; i<jaScriptDetails.size(); i++ ){
						JSONObject joScript = jaScriptDetails.getJSONObject(i);
						if( isFirst ) {
							strScriptName = joScript.getString("scriptName");
							strScriptId = joScript.getString("scriptId");
							isFirst = false;
						} else{
							strScriptName = strScriptName+","+ joScript.getString("scriptName");
							strScriptId = strScriptId+","+ joScript.getString("scriptId");
						}
						joScript = null;
					}
					UtilsFactory.clearCollectionHieracy( jaScriptDetails );
					long completedRuns = runManager.scenarioReferenceFromReportMaster(con, loginUserBean.getUserId(), Long.valueOf(strScenarioId));
					if(completedRuns > 0){
						joRtn = UtilsFactory.getJSONFailureReturn("Scenario has reference with Run and Report. Hence, cannot be deleted.");
					} else {
						runManager.deleteJmeterScript(con, strScenarioName, loginUserBean, strScriptName.split(","), strTestType, Long.valueOf(strScenarioId));
						joRtn = new FloodgatesXMLAppend().deleteJmeterScript(strScenarioName, strScriptName.split(","), strVUScriptsXmlPath, strScenarioXmlPath, strTestType);
					}
				}
				
			}catch(Exception e){
				LogManager.errorLog(e);
			}catch(Throwable t){
				LogManager.errorLog(t);
			}finally{
				DataBaseManager.close(con);
//				con = null;
//				loginUserBean = null;
//				strScenarioName = null;
//				strVUScriptsXmlPath = null;
//				strScenarioXmlPath = null;
//				strTestType = null;
//				joRunningScenario = null;
//				runManager = null;
//				strScriptName = null;
				
				response.getWriter().write(joRtn.toString());
			}
			
		} else if(strActionCommand.endsWith("/ltScheduler/pollJmeterQueue")){
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			ServletContext context = getServletContext();
			try	{
//				loadTestSchedulerBean = new LoadTestSchedulerBean();
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject("{\"TestType\":\"JMETER\",\"LoadgenIpAddress\":\"192.168.1.231\",\"ScenarioId\":\"155\",\"ScenarioName\":\"samplewp\",\"ReportName\":\"Test Jmeter 20160208_2\",\"dateQueuedOn\":{\"date\":8,\"day\":1,\"hours\":15,\"minutes\":13,\"month\":1,\"seconds\":5,\"time\":1454924585962,\"timezoneOffset\":-330,\"year\":116},\"UserId\":231}"));
//				LTScheduler.ltProcessingJmeterQueue(loadTestSchedulerBean);
				
				loadTestSchedulerBean = LTScheduler.pollJmeterProcessingQueue();
				
				if(loadTestSchedulerBean != null){
					String strJmxFilePath = Constants.JMETERSCENARIOSPATH +loadTestSchedulerBean.getUserId()+File.separator+ loadTestSchedulerBean.getScenarioName() + ".jmx";
					
					File downloadFile = new File(strJmxFilePath);
					FileInputStream inStream = new FileInputStream(downloadFile);
					String mimeType = context.getMimeType(strJmxFilePath);
			        if (mimeType == null) {
			            // set to binary type if MIME mapping not found
			            mimeType = "application/octet-stream";
			        }
			        
			        // modifies response
			        response.setContentType(mimeType);
			        response.setContentLength((int) downloadFile.length());
			        response.setHeader("file_exist", "1");
			        response.setHeader("fileName", loadTestSchedulerBean.getScenarioName());
			        response.setHeader("loadTestSchedulerBean", loadTestSchedulerBean.toJSON().toString());
			        // obtains response's output stream
			        OutputStream outStream = response.getOutputStream();
			        
			        byte[] buffer = new byte[4096];
			        int bytesRead = -1;
			        while ((bytesRead = inStream.read(buffer)) != -1) {
			            outStream.write(buffer, 0, bytesRead);
			        }
			       inStream.close();
			       outStream.close();
				} else {
					 response.setHeader("file_exist", "-1");
				}
			}catch(Throwable t)	{
				LogManager.errorLog(t);
			}
		} else if(strActionCommand.endsWith("/ltScheduler/getScenarioFile")){
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			ServletContext context = getServletContext();
			DocumentBuilderFactory builderFactory = null;
			DocumentBuilder builder = null;
			Document xmlDocument = null;
			XPath xPath = null;
			NodeList threadGroup = null, childNode = null, childCSVNode = null;
			int instanceCount = 0;
			try	{
//				loadTestSchedulerBean = new LoadTestSchedulerBean();
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject("{\"TestType\":\"JMETER\",\"LoadgenIpAddress\":\"192.168.1.231\",\"ScenarioId\":\"155\",\"ScenarioName\":\"samplewp\",\"ReportName\":\"Test Jmeter 20160208_2\",\"dateQueuedOn\":{\"date\":8,\"day\":1,\"hours\":15,\"minutes\":13,\"month\":1,\"seconds\":5,\"time\":1454924585962,\"timezoneOffset\":-330,\"year\":116},\"UserId\":231}"));
//				LTScheduler.ltProcessingJmeterQueue(loadTestSchedulerBean);
				
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.setUserId(Long.parseLong(request.getParameter("userId")));
				//loadTestSchedulerBean.setRunId(Long.parseLong(request.getParameter("runId")));
				loadTestSchedulerBean.setScenarioName(request.getParameter("scenarioName"));
				
				instanceCount = Integer.parseInt(request.getParameter("neededInstance"));
				
				String strJmxFilePath = Constants.JMETERSCENARIOSPATH +loadTestSchedulerBean.getUserId()+File.separator+ loadTestSchedulerBean.getScenarioName() + ".jmx";
				//strJmxFilePath = "e:\\BackofficeCombined_Stage_4_5.jmx";
				builderFactory = DocumentBuilderFactory.newInstance();
				builder = builderFactory.newDocumentBuilder();
				xmlDocument = builder.parse(new File(strJmxFilePath));
				xPath = XPathFactory.newInstance().newXPath();
				
				threadGroup = (NodeList) xPath.compile("//hashTree/ThreadGroup").evaluate(xmlDocument, XPathConstants.NODESET);
				if (instanceCount > 1) {
					for (int scenarioCount = 0; scenarioCount < threadGroup.getLength(); scenarioCount++) {
						childNode = (NodeList) xPath.compile("//hashTree/ThreadGroup/stringProp[@name='ThreadGroup.num_threads']").evaluate(xmlDocument, XPathConstants.NODESET);
						String val = childNode.item(scenarioCount).getTextContent();
						if (val != null && val.trim().length() > 0 && org.apache.commons.lang.math.NumberUtils.isNumber(val)) {
							long totalTGUser = Long.parseLong(val);
							childNode.item(scenarioCount).setTextContent((totalTGUser%instanceCount == 0 ? totalTGUser/instanceCount : (totalTGUser/instanceCount)+1)+"");
						}
					}
				}
				
				String csvExpression = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
				childCSVNode = (NodeList) xPath.compile(csvExpression).evaluate(xmlDocument, XPathConstants.NODESET);
				for (int csvCount = 0; csvCount < childCSVNode.getLength(); csvCount++) { 
					if(childCSVNode.item(csvCount).getTextContent()!= null && childCSVNode.item(csvCount).getTextContent().trim().length()>0 ) {
						String csvFileFromJMX = childCSVNode.item(csvCount).getTextContent().toLowerCase();
						String csvFilename = "";
						if (UtilsFactory.isValidPath(csvFileFromJMX)) {
							//Path p = Paths.get(csvFileFromJMX);
							//System.out.println("csvFullFileName: "+p.getFileName().toString());
							csvFilename = FilenameUtils.getName(csvFileFromJMX);
							System.out.println("csvFilename: "+csvFilename);
						} else {
							csvFilename = csvFileFromJMX;
						}
						childCSVNode.item(csvCount).setTextContent(csvFilename);
					}
				}
				
				
		        // modifies response
		        response.setContentType("application/octet-stream");
		        response.setHeader("file_exist", "1");
		        response.setHeader("fileName", loadTestSchedulerBean.getScenarioName());
		        
		        XMLSerializer serializer = new XMLSerializer();
				serializer.setOutputByteStream( response.getOutputStream() );
		        serializer.serialize( xmlDocument );
		        response.setContentLength((int) serializer.toString().length());
				
			}catch(Throwable t)	{
				LogManager.errorLog(t);
			} finally {
				UtilsFactory.clearCollectionHieracy(childNode);
				childNode = null;
				
				UtilsFactory.clearCollectionHieracy(threadGroup);
				threadGroup = null;
				
				UtilsFactory.clearCollectionHieracy(xPath);
				xPath = null;
				
				UtilsFactory.clearCollectionHieracy(xmlDocument);
				xmlDocument = null;
				
				UtilsFactory.clearCollectionHieracy(builder);
				builder = null;
				
				UtilsFactory.clearCollectionHieracy(builderFactory);
				builderFactory = null;
			}
		} else if(strActionCommand.endsWith("/ltScheduler/containsCSVFiles")){
			JSONObject joMessage = null;
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			JmeterScriptManager jmeterScriptManager = null;
			List<String> listParam = null;
			RunManager runManager = null;
			Connection con = null;
			
			try {
//				loadTestSchedulerBean = new LoadTestSchedulerBean();
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject("{\"TestType\":\"JMETER\",\"LoadgenIpAddress\":\"192.168.1.231\",\"ScenarioId\":\"155\",\"ScenarioName\":\"samplewp\",\"ReportName\":\"Test Jmeter 20160208_2\",\"dateQueuedOn\":{\"date\":8,\"day\":1,\"hours\":15,\"minutes\":13,\"month\":1,\"seconds\":5,\"time\":1454924585962,\"timezoneOffset\":-330,\"year\":116},\"UserId\":231}"));
//				LTScheduler.ltProcessingJmeterQueue(loadTestSchedulerBean);
				
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.setUserId(Long.parseLong(request.getParameter("userId")));
				loadTestSchedulerBean.setRunId(Long.parseLong(request.getParameter("runId")));
				loadTestSchedulerBean.setScenarioName(request.getParameter("scenarioName"));
					
				con = DataBaseManager.giveConnection();
				
				runManager = new RunManager();
				//runManager.updateJMeterTestRunnningInReportMaster(con, loadTestSchedulerBean.getRunId());
				
				String strJmxFilePath = Constants.JMETERSCENARIOSPATH +loadTestSchedulerBean.getUserId()+File.separator+ loadTestSchedulerBean.getScenarioName() + ".jmx";
				jmeterScriptManager = new JmeterScriptManager();
				boolean isContainsCsv = jmeterScriptManager.isJmxContainsCsvFile(strJmxFilePath);
				
				if (isContainsCsv) { // means jmx contains csv files
					Boolean isParamFilesExist = jmeterScriptManager.isParamFilesExist(strJmxFilePath, Constants.JMETERSCENARIOSPATH+loadTestSchedulerBean.getUserId());
					if (isParamFilesExist) {
						listParam = jmeterScriptManager.getParametersFile(strJmxFilePath);
						String params = "";
						boolean isFirst = true;
						for(String str: listParam){
							if(isFirst){
								params = str;	
								isFirst = false;
							} else {
								params = params + ","+ str;
							}
						}
						joMessage = UtilsFactory.getJSONSuccessReturn(params);
					} else {
						LogManager.infoLog("Parameter files were missed");
						joMessage = UtilsFactory.getJSONFailureReturn("null");
					}
				} else {
					joMessage = UtilsFactory.getJSONFailureReturn("null");
				}
				
				
			} catch (Throwable t)	{
				LogManager.errorLog(t);
			} finally {
				DataBaseManager.close(con);
				con = null;
				runManager = null;
				response.getWriter().write(joMessage.toString());
			}
			
		} else if(strActionCommand.endsWith("/ltScheduler/downloadCSVFile")){
			ServletContext context = getServletContext();
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			response.setHeader("file_exist", "-1");
			
			try {
				String fileName = request.getParameter("fileName");
//				loadTestSchedulerBean = new LoadTestSchedulerBean();
//				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject("{\"TestType\":\"JMETER\",\"LoadgenIpAddress\":\"192.168.1.231\",\"ScenarioId\":\"155\",\"ScenarioName\":\"samplewp\",\"ReportName\":\"Test Jmeter 20160208_2\",\"dateQueuedOn\":{\"date\":8,\"day\":1,\"hours\":15,\"minutes\":13,\"month\":1,\"seconds\":5,\"time\":1454924585962,\"timezoneOffset\":-330,\"year\":116},\"UserId\":231}"));
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.setUserId(Long.parseLong(request.getParameter("userId")));
				
				String str = Constants.JMETERSCENARIOSPATH+loadTestSchedulerBean.getUserId()+File.separator+fileName;
				File downloadFile = new File(str);
				FileInputStream inStream = new FileInputStream(downloadFile);
				String mimeType = context.getMimeType(str);
		        if (mimeType == null) {
		            // set to binary type if MIME mapping not found
		            mimeType = "application/octet-stream";
		        }
		        
		        // modifies response
		        response.setContentType(mimeType);
		        response.setContentLength((int) downloadFile.length());
		        response.setHeader("file_exist", "1");
		        
		        // obtains response's output stream
		        OutputStream outStream = response.getOutputStream();
		        
		        byte[] buffer = new byte[4096];
		        int bytesRead = -1;
		        while ((bytesRead = inStream.read(buffer)) != -1) {
		            outStream.write(buffer, 0, bytesRead);
		        }
		       inStream.close();
		       outStream.close();
				
			} catch (Throwable t)	{
				LogManager.errorLog(t);
			}finally {
				response.getWriter();
			}
		} else if(strActionCommand.endsWith("/ltScheduler/runCompleted")){
			RunManager runManager = null;
			Connection con = null;
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			try {
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(request.getParameter("loadTestSchedulerBean")));
				runManager.updateReportMaster(con, loadTestSchedulerBean.getRunId(), "AUTO REPORT GENERATION STARTED", null);
				runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId() );
				
				// Updateing Scriptwise Status
				String scriptIds[] = loadTestSchedulerBean.getScriptId().split(",");
				String scriptNames[] = loadTestSchedulerBean.getScriptName().split(",");
				for ( int k=0; k<scriptIds.length; k++ ){
					HashMap<String, Object> hmStatuswiseDetails = new HashMap<String, Object>();
					hmStatuswiseDetails.put("runid", String.valueOf(loadTestSchedulerBean.getRunId()));
					hmStatuswiseDetails.put("script_id", String.valueOf(scriptIds[k]));
					hmStatuswiseDetails.put("created_users", "0");
					hmStatuswiseDetails.put("completed_users", "0");
					hmStatuswiseDetails.put("http_200_count", "0");
					hmStatuswiseDetails.put("http_300_count", "0");
					hmStatuswiseDetails.put("http_400_count", "0");
					hmStatuswiseDetails.put("http_500_count", "0");
					hmStatuswiseDetails.put("is_completed", "0");
					hmStatuswiseDetails.put("error_count", "0");
					hmStatuswiseDetails.put("script_name", scriptNames[k]);
					if (runManager.isRecordAvailable(con, String.valueOf(loadTestSchedulerBean.getRunId()), String.valueOf(scriptIds[k]))) {
						hmStatuswiseDetails.put("is_completed", "1");
						runManager.updateScriptwiseStatus(con, hmStatuswiseDetails);
					} else {
						runManager.insertScriptwiseStatus(con, hmStatuswiseDetails);
					}
				}
				
			} catch (Throwable e) {
				LogManager.errorLog(e);
			} finally{
				DataBaseManager.close(con);
				con = null;
				runManager = null;
			}
		}
		/**
		 * Not-In-Use
		 * Used when JMeter report was taken in JTL format.
		 * Now it is in Grafana report.
		 *
		else if(strActionCommand.endsWith("/ltScheduler/getResultHTMLFile")){
			int BUFFER_SIZE = 4096;
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			RunManager runManager = null;
			Connection con = null;
			try {
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject(JSONObject.fromObject(request.getHeader("loadTestSchedulerBean")));
				
				File runIdFolderPath = new File(Constants.JMETERSUMMARYREPORTPATH+File.separator+loadTestSchedulerBean.getRunId());
				if (!runIdFolderPath.exists()) {
					if (runIdFolderPath.mkdir()) {
						System.out.println("Directory is created!");
					} else {
						System.out.println("Failed to create directory!");
					}
				}
				File saveFile = new File(Constants.JMETERSUMMARYREPORTPATH+File.separator+loadTestSchedulerBean.getRunId()+File.separator+request.getHeader("fileName"));
//				System.out.println(request.getParameter("dfs"));
				// opens input stream of the request for reading data
				InputStream inputStream = request.getInputStream();;
				// opens an output stream for writing file
				FileOutputStream outputStream = new FileOutputStream(saveFile);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				LogManager.infoLog("Receiving data...");
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				
				LogManager.infoLog("Data received.");
				runManager.updateReportMasterStatus(con, loadTestSchedulerBean.getRunId(), "AUTO REPORT GENERATION COMPLETED" );
				outputStream.close();
				inputStream.close();
			} catch (Throwable e) {
				LogManager.errorLog(e);
			} finally{
				DataBaseManager.close(con);
				con = null;
				runManager = null;
			}
		}*/
		else if( strActionCommand.endsWith("/ltScheduler/queueGrafanaSnapShotData") ) {
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			Connection con = null;
			FileOutputStream outputStream = null;
			String strLoadTestSchedulerBean, strGrafanaSnapshotData;
			
			try {
				con = DataBaseManager.giveConnection();
				
				// Read data from Request's InputStream.
				// Snapshot length may grow in MB. As unable to send in "POST", sent in "StringRequestEntity".
			    StringBuilder stringBuilder = new StringBuilder();
		        InputStream inputStream = request.getInputStream();
		        BufferedReader bufferedReader = null;
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	            
	            // Separate the variables, which are read from InputStream
	            strLoadTestSchedulerBean = stringBuilder.substring(0, stringBuilder.indexOf("#@DELIM@#"));
	            strGrafanaSnapshotData = stringBuilder.substring((stringBuilder.indexOf("#@DELIM@#")+"#@DELIM@#".length()), stringBuilder.length());
	            
	            // Parse load-test bean
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject( JSONObject.fromObject( strLoadTestSchedulerBean ) );
				
				
				// Save the Grafana-Snapshot in local HDD, so reference.
				File savePath = new File(Constants.JMETERSUMMARYREPORTPATH+File.separator+loadTestSchedulerBean.getRunId());
				if ( ! savePath.exists()) {
					if ( savePath.mkdirs() ) {
						LogManager.infoLog("Directory is created!");
					} else {
						LogManager.infoLog("Failed to create directory!");
					}
				}
				
				File saveFile = new File(savePath.getAbsolutePath()+File.separator+loadTestSchedulerBean.getRunId()+".json");
				// opens an output stream for writing file
				outputStream = new FileOutputStream(saveFile);
				
				LogManager.infoLog("Writing Grafana-JSON-Snapshot-data...");
				outputStream.write( strGrafanaSnapshotData.getBytes("UTF-8") );
				
				JMeterExecutionManager.addJMeterSnapshotData(strLoadTestSchedulerBean, strGrafanaSnapshotData);
				
				LogManager.infoLog("Writing completed Grafana-JSON-Snapshot-data");
				
			} catch (Throwable e) {
				LogManager.errorLog(e);
			} finally{
				outputStream.close();
				
				DataBaseManager.close(con);
				con = null;
			}
		}
		else if( strActionCommand.endsWith("/ltScheduler/pollGrafanaSnapShotData") ) {
			String strResponse[];
			JSONObject joReturn = null;
			
			try {
				strResponse = JMeterExecutionManager.pollJMeterSnapshotData();
				
				if( strResponse != null ) {
					joReturn = UtilsFactory.getJSONSuccessReturn("");
					
					joReturn.put("loadTestSchedulerBean", strResponse[0]);
					joReturn.put("grafanaSnapshotData", strResponse[1]);
				
					LogManager.infoLog("Writing completed Grafana-JSON-Snapshot-data.");
				} else {
					joReturn = UtilsFactory.getJSONFailureReturn("No Snapshot found in queue.");
				}
			} catch (Throwable e) {
				LogManager.errorLog(e);
				joReturn = UtilsFactory.getJSONFailureReturn( e.getMessage() );
			} finally {
				response.getWriter().write(joReturn.toString());
			}
		}
		else if( strActionCommand.endsWith("/ltScheduler/completeJMeterRun") ) {
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			RunManager runManager = null;
			Connection con = null;
			String strLoadTestSchedulerBean, strReportKey;
			JSONObject joReturn = null;
			
			try{
				strReportKey = request.getParameter("report_key");
				strLoadTestSchedulerBean = request.getParameter("loadTestSchedulerBean");
				
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject( JSONObject.fromObject( strLoadTestSchedulerBean ) );
			
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				
				runManager.completeJMeterTestInReportMaster(con, loadTestSchedulerBean.getRunId(), "AUTO REPORT GENERATION COMPLETED", strReportKey );
				
				joReturn = UtilsFactory.getJSONSuccessReturn("Test is maked as COMPLETED.");
			} catch (Throwable e) {
				LogManager.errorLog(e);
				joReturn = UtilsFactory.getJSONFailureReturn( e.getMessage() );
			} finally {
				response.getWriter().write(joReturn.toString());
				
				DataBaseManager.close(con);
				con = null;
			}
		}
		else if( strActionCommand.endsWith("/ltScheduler/failJMeterRun") ) {
			LoadTestSchedulerBean loadTestSchedulerBean = null;
			RunManager runManager = null;
			Connection con = null;
			String strLoadTestSchedulerBean, strErrorMessage;
			JSONObject joReturn = null;
			
			try{
				strLoadTestSchedulerBean = request.getParameter("loadTestSchedulerBean");
				loadTestSchedulerBean = new LoadTestSchedulerBean();
				loadTestSchedulerBean.fromJSONObject( JSONObject.fromObject( strLoadTestSchedulerBean ) );
				
				strErrorMessage = request.getParameter("errorMessage");
				
				runManager = new RunManager();
				con = DataBaseManager.giveConnection();
				
				runManager.updateReportMaster(con, loadTestSchedulerBean.getRunId(), "FAILED", strErrorMessage );
				
				runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId() );
				
				joReturn = UtilsFactory.getJSONSuccessReturn("Test is maked as FAILED.");
			} catch (Throwable e) {
				LogManager.errorLog(e);
				joReturn = UtilsFactory.getJSONFailureReturn( e.getMessage() );
			} finally {
				response.getWriter().write(joReturn.toString());
				
				DataBaseManager.close(con);
				con = null;
			}
		}
	}
	
	public static void main(String s[]) {
		DocumentBuilderFactory builderFactory;
		DocumentBuilder builder;
		Document xmlDocument;
		ArrayList<JSONObject> alThreadGrp;
		NodeList matches, childNode;
		
		String threadGrpRoot = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
		
//		String[] childNodes = { "/stringProp[@name='ThreadGroup.num_threads']",
//				"/stringProp[@name='ThreadGroup.ramp_time']",
//				"/longProp[@name='ThreadGroup.start_time']",
//				"/longProp[@name='ThreadGroup.end_time']", "/boolProp[@name='ThreadGroup.scheduler']",
//				"/stringProp[@name='ThreadGroup.duration']", "/stringProp[@name='ThreadGroup.delay']" };
		HashMap<String, String> hmJMeterVariables = new HashMap<String, String>();
		hmJMeterVariables.put("rumThreads", "/stringProp[@name='ThreadGroup.num_threads']");
		hmJMeterVariables.put("rampTime", "/stringProp[@name='ThreadGroup.ramp_time']");
		hmJMeterVariables.put("scheduler", "/boolProp[@name='ThreadGroup.scheduler']");
		hmJMeterVariables.put("duration", "/stringProp[@name='ThreadGroup.duration']");
		hmJMeterVariables.put("delay", "/stringProp[@name='ThreadGroup.delay']");
		hmJMeterVariables.put("loop", "/elementProp/stringProp[@name='LoopController.loops']");
		hmJMeterVariables.put("continueForever", "/elementProp/boolProp[@name='LoopController.continue_forever']");
		
		HashMap<String, String> hmJMeterReplaceVariables = new HashMap<String, String>();
		hmJMeterReplaceVariables.put("rumThreads", "<stringProp name=\"ThreadGroup.num_threads\">");
		hmJMeterReplaceVariables.put("rampTime", "<stringProp name=\"ThreadGroup.ramp_time\">");
		hmJMeterReplaceVariables.put("scheduler", "<boolProp name=\"ThreadGroup.scheduler\">");
		hmJMeterReplaceVariables.put("duration", "<stringProp name=\"ThreadGroup.duration\">");
		hmJMeterReplaceVariables.put("delay", "<stringProp name=\"ThreadGroup.delay\">");
		hmJMeterReplaceVariables.put("loop", "<stringProp name=\"LoopController.loops\">");
		
		try {
		
		alThreadGrp = new ArrayList<JSONObject>();
		builderFactory = DocumentBuilderFactory.newInstance();

		builder = builderFactory.newDocumentBuilder();
	
		//xmlDocument = builder.parse(new File("C:\\Appedo\\resource\\scenarios\\jmeter\\6\\test.jmx"));
		xmlDocument = builder.parse(new File("e:\\priya\\TestMMDisplay.jmx"));
		
		XPath xPath = XPathFactory.newInstance().newXPath();
		//String expression = "//hashTree/ThreadGroup/stringProp[@name='ThreadGroup.num_threads']/text()";
		
		//String expression = "//hashTree/ThreadGroup";
		matches = (NodeList) xPath.compile(threadGrpRoot).evaluate(xmlDocument, XPathConstants.NODESET);
			
	
		System.out.println(matches.getLength());
		String name = "testname";
		System.out.println(matches.getLength());
		for (int scenarioCount = 0; scenarioCount < matches.getLength(); scenarioCount++) {
			System.out.println(matches.item(scenarioCount).getTextContent().toString());
		}
		//threadGrpRoot = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";				
		threadGrpRoot = "//hashTree/hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
		matches = (NodeList) xPath.compile(threadGrpRoot).evaluate(xmlDocument, XPathConstants.NODESET);
		System.out.println(matches.getLength());
		for (int scenarioCount = 0; scenarioCount < matches.getLength(); scenarioCount++) {
			System.out.println(matches.item(scenarioCount).getTextContent().toString());
		}
		
		/*for (int scenarioCount = 0; scenarioCount < matches.getLength(); scenarioCount++) {
			JSONObject joThreadObj = new JSONObject();
			joThreadObj.put("position", scenarioCount+1);
			joThreadObj.put("scenario", matches.item(scenarioCount).getAttributes().getNamedItem(name).getTextContent());
			for (Map.Entry<String, String> threadGrpObj : hmJMeterVariables.entrySet()) {
				childNode = (NodeList) xPath.compile(threadGrpRoot+threadGrpObj.getValue()).evaluate(xmlDocument, XPathConstants.NODESET);
				//childNode.item(scenarioCount).setTextContent("Test"+scenarioCount);
				//joThreadObj.put(threadGrpObj.getKey(), childNode.item(scenarioCount).getTextContent().toString());
				childNode.item(scenarioCount).setTextContent(threadGrpObj.getKey());
			}
			alThreadGrp.add(joThreadObj);
			
		}
		System.out.println(alThreadGrp.toString());*/
		
		/*try {
			Transformer tf = TransformerFactory.newInstance().newTransformer();
		    tf.setOutputProperty(OutputKeys.INDENT, "yes");
		    tf.setOutputProperty(OutputKeys.METHOD, "xml");
		    tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		    
		    FileOutputStream os = new FileOutputStream(new File("e:\\nex.jmx"));
		    DOMSource domSource = new DOMSource(xmlDocument);
	        StreamResult sr = new StreamResult(os);
	        tf.transform(domSource, sr);
		    
		} catch (Exception e) {
			
		} finally {
			//os = null;
		}*/
		
		
		
		
		//TO update file
//		String fileName = "C:\\Appedo\\resource\\scenarios\\jmeter\\6\\test.jmx";
//		String line;
//		FileReader fileReader = 
//                new FileReader(fileName);
//		
//		BufferedReader bufferedReader = 
//                new BufferedReader(fileReader);
//		
//		while((line = bufferedReader.readLine()) != null) {
//            System.out.println(line);
//        }  
//		
//		bufferedReader.close();  
		
		} catch (XPathExpressionException | SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			xmlDocument = null;
			builder = null;
			builderFactory = null;
		}
		
	}
			
//			System.out.println(matches.item(i).getChildNodes().item(count+2).getNodeName());
//			System.out.println(matches.item(i).getChildNodes().item(count+4).getNodeName());
//			System.out.println(matches.item(i).getChildNodes().item(count+6).getNodeName());
//			System.out.println(matches.item(i).getChildNodes().item(count+8).getNodeName());
//			System.out.println(matches.item(i).getChildNodes().item(count+10).getNodeName());
//			System.out.println("Thread Group " + (i + 1) + " users = " + matches.item(i).getNodeName());
//			System.out.println("nodeName " + matches.item(i).getChildNodes().item(i).getTextContent());
//			System.out.println("nodeNames " + matches.item(i).getChildNodes().item(i).toString());
//			System.out.println("localName " + matches.item(i).getLocalName());
//			System.out.println("nodeValue " + matches.item(i).getNodeValue());
//			System.out.println("attributes " + matches.item(i).getAttributes().item(i));
//			System.out.println("childNodes " + matches.item(i).getChildNodes());
//			System.out.println("nextSiblings " + matches.item(i).getNextSibling());
//			System.out.println("ENTITY_NODE " + matches.item(i).getChildNodes());
		    //System.out.println("Thread Group " + (i + 1) + " users = " + matches.item(i).getTextContent());
		    
		
}
