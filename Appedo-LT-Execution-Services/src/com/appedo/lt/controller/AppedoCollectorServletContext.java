package com.appedo.lt.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.appedo.lt.servlet.InitServlet;

public class AppedoCollectorServletContext implements ServletContextListener {

	private ServletContext context = null;

	/**
	 * This method is invoked when the Web Application
	 * is ready to service requests
	 */
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		
		//Output a simple message to the server's console
		System.out.println("The Appedo Collector Web App is Ready in "+context.getServerInfo());
	}
	
	/**
	 * Stop all the timers when this project is stopped or tomcat is stopped
	 */
	public void contextDestroyed(ServletContextEvent event) {
				
		try{
			InitServlet.timerTaskLoadTest.cancel();
			InitServlet.timerLT.cancel();
			InitServlet.timerLT.purge();
			
			InitServlet.timerTaskCount.cancel();
			InitServlet.timerCount.cancel();
			InitServlet.timerCount.purge();
			
			InitServlet.timerTaskPaid.cancel();
			InitServlet.timerPaid.cancel();
			InitServlet.timerPaid.purge();
			
			InitServlet.timerTaskProcessingQueue.cancel();
			InitServlet.timerProcessing.cancel();
			InitServlet.timerProcessing.purge();
			
			InitServlet.timerJmeter.cancel();
			InitServlet.timerJM.cancel();
			InitServlet.timerJM.purge();
			
			InitServlet.timerJmeterPro.cancel();
			InitServlet.timerJMPro.cancel();
			InitServlet.timerJMPro.purge();
			
			InitServlet.timerJmeterDrain.cancel();
			InitServlet.timerJMDrain.cancel();
			InitServlet.timerJMDrain.purge();
			

			InitServlet.threadLTTCPCommunication.getServSoc().close();
			
			InitServlet.threadLTTCPCommunication.interrupt();
			
		} catch (Exception e) {
			System.out.println("Exception in Java timer stop: "+e.getMessage());
		}
		
		System.out.println("The Appedo Collector Web App has Been Removed.");
		this.context = null;
	}
}
