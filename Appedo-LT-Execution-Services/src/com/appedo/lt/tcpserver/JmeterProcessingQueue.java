package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.HashMap;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class JmeterProcessingQueue extends TimerTask{
	
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	AmazonConfiguration amazonConfiguration = null;
	RunManager runManager = null;
	String region, instanceId;
	JSONObject instance_config, region_details;
	Connection con = null;
	int totalUser = 0;
	JSONArray inactiveInstance;
	int totalAvailability = 0;

	public JmeterProcessingQueue(){
		this.con = DataBaseManager.giveConnection();
	}
	
	@Override
	public void run() {
		try {
			runManager = new RunManager();
			if( LTScheduler.htJmeterQueues.size() > 0 ){
				amazonConfiguration = new AmazonConfiguration();
				loadTestSchedulerBean = LTScheduler.peekJmeterTest();
				if( loadTestSchedulerBean != null ){
					region = loadTestSchedulerBean.getLoadGenRegions();
					totalUser = loadTestSchedulerBean.getMaxUserCount();
					
					inactiveInstance = runManager.getInactiveLoadAgentDetails(con, region, "FEDORA", null);
					
					if ( inactiveInstance.size() > 0 ){
						for( int j=0;j<=inactiveInstance.size(); ){
						JSONObject joObject = (JSONObject) inactiveInstance.get(j);
						loadTestSchedulerBean = LTScheduler.pollJmeterTest();
						loadTestSchedulerBean.setLoadgenIpAddress( joObject.getString("public_ip") );
						runManager.updateLoadAgentDetails(con, joObject.getString("public_ip"), joObject.getString("instance_id"));
						runManager.insertRunGenDetails(con, loadTestSchedulerBean, joObject.getString("instance_id"));
						runManager.updateReportMasterLoadGen(con, loadTestSchedulerBean.getRunId(), joObject.getString("public_ip"));
						
						// To insert Scriptwise Status 
						String scriptIds[] = loadTestSchedulerBean.getScriptId().split(",");
						String scriptNames[] = loadTestSchedulerBean.getScriptName().split(",");
						for ( int k=0; k<scriptIds.length; k++ ){
							HashMap<String, Object> hmStatuswiseDetails = new HashMap<String, Object>();
							hmStatuswiseDetails.put("runid", String.valueOf(loadTestSchedulerBean.getRunId()));
							hmStatuswiseDetails.put("script_id", String.valueOf(scriptIds[k]));
							hmStatuswiseDetails.put("created_users", "0");
							hmStatuswiseDetails.put("completed_users", "0");
							hmStatuswiseDetails.put("http_200_count", "0");
							hmStatuswiseDetails.put("http_300_count", "0");
							hmStatuswiseDetails.put("http_400_count", "0");
							hmStatuswiseDetails.put("http_500_count", "0");
							hmStatuswiseDetails.put("is_completed", "0");
							hmStatuswiseDetails.put("error_count", "0");
							hmStatuswiseDetails.put("script_name", scriptNames[k]);
							if(runManager.isRecordAvailable(con, String.valueOf(loadTestSchedulerBean.getRunId()), String.valueOf(scriptIds[k]))){
								runManager.updateScriptwiseStatus(con,hmStatuswiseDetails);
							}else{
								runManager.insertScriptwiseStatus(con,hmStatuswiseDetails);
							}
						}
						LTScheduler.ltProcessingJmeterQueue( loadTestSchedulerBean );
//						joObject = null;
						break;}
					}
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			region = null;
//			runManager = null;
//			instanceId = null;
//			amazonConfiguration = null;
//			loadTestSchedulerBean = null;
			UtilsFactory.clearCollectionHieracy( instance_config );
			UtilsFactory.clearCollectionHieracy( region_details );
			UtilsFactory.clearCollectionHieracy( inactiveInstance );
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
