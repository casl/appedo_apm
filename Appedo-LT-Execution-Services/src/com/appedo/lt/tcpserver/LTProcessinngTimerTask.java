package com.appedo.lt.tcpserver;

import java.sql.Connection;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LTProcessinngTimerTask extends Thread{
	Connection con = null;
	RunManager rm = null;
	JSONObject json = null;
	JSONArray jaArrayLogs = null, jaArrayErrors = null, jaArrayReports = null, jaArrayTrans = null, jaArrayUser = null;
	long lRunId = 0;
	String loadgens = null;
	int is_completed = 0;
	int created_user = 0, completed_user = 0;
	String ltData = null;
	private long tNo;

	public LTProcessinngTimerTask(String reportData) throws Throwable{
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Connection count in LTProcessinngTimerTask: "+tNo);
		this.ltData = reportData;
		rm = new RunManager();
	}
	
	@Override
	public void run() {
		StringBuilder sbQuery = new StringBuilder();
		try {
			//TODO: if con is null, write to file
			json = JSONObject.fromObject(ltData);
			
			if( json != null){
				
				lRunId = Long.valueOf(json.getString("runid"));
				
				jaArrayLogs = (JSONArray) json.get("log");
				jaArrayErrors = (JSONArray) json.get("error");
				jaArrayReports = (JSONArray) json.get("reporddata");
				jaArrayTrans = (JSONArray) json.get("transactions");
				jaArrayUser = (JSONArray) json.get("userdetail");
				long startTime = System.currentTimeMillis();
				
				if( con != null){
					
					//LogManager.infoLog("User Data: "+jaArrayUser.size());
					if( jaArrayUser.size() > 0 ){
						// rm.insertRunTimeCount(con, jaArrayUser, lRunId);	
						sbQuery .append("insert into lt_user_runtime_"+lRunId+" (runtime, runtype, userid, script_id, loadgen_name) ")
								.append("select runtime, runtype, userid, script_id, loadgen_name from json_populate_recordset(NULL::lt_user_runtime_"+lRunId+","+UtilsFactory.makeValidVarchar(jaArrayUser.toString())+"); ");
					}
					
					//LogManager.infoLog("Log Data: "+jaArrayLogs.size());
					if( jaArrayLogs.size() > 0 ){
						// rm.insertLogDetails(con, jaArrayLogs, lRunId);	
						sbQuery .append("insert into lt_log_"+lRunId+" (iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id) ")
								.append("select iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id from json_populate_recordset(NULL::lt_log_"+lRunId+","+UtilsFactory.makeValidVarchar(jaArrayLogs.toString())+"); ");
					}
					
					//LogManager.infoLog("Error Data: "+jaArrayErrors.size());
					if( jaArrayErrors.size() > 0 ){
						// rm.insertErrorDetails(con, jaArrayErrors, lRunId);
						sbQuery .append("insert into lt_error_"+lRunId+" (iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id) ")
								.append("select iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id from json_populate_recordset(NULL::lt_error_"+lRunId+","+UtilsFactory.makeValidVarchar(jaArrayErrors.toString())+"); ");
					}
					
					//LogManager.infoLog("Report Data: "+jaArrayReports.size());
					if( jaArrayReports.size() > 0 ){
						// rm.insertReportData(con, jaArrayReports, lRunId);
						sbQuery .append("INSERT INTO lt_reportdata_" + lRunId + " (loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize) ")
								.append("SELECT loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize FROM json_populate_recordset(NULL::lt_reportdata_" + lRunId + ","+UtilsFactory.makeValidVarchar(jaArrayReports.toString())+"); ");
					}
					
					//LogManager.infoLog("Transaction Data: "+jaArrayTrans.size());
					if( jaArrayTrans.size() > 0 ){
						// rm.insertTransactionDetails(con, jaArrayTrans, lRunId);
						sbQuery .append("insert into lt_transactions_"+lRunId+" (script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff) ")
								.append("select script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff from json_populate_recordset(NULL::lt_transactions_"+lRunId+","+UtilsFactory.makeValidVarchar(jaArrayTrans.toString())+"); ");
					}
					
					sbQuery.append("UPDATE appedo_pvt_counters SET lt_exec_services_queue="+LTScheduler.reportDataQueues.size()+", lt_exec_status = true");
					rm.insertLTJsonDetails(con, sbQuery.toString());
					
					System.out.println("Time Taken to insert :"+(System.currentTimeMillis() - startTime));
					LogManager.infoLog("Time Taken to insert :"+(System.currentTimeMillis() - startTime));
				} else {
					LogManager.infoLog("jaArrayLogs: "+jaArrayLogs.size()+ " jaArrayUser: "+jaArrayUser.size()+" jaArrayErrors: "+jaArrayErrors.size()+ " jaArrayReports: "+jaArrayReports.size()+" jaArrayTrans: "+jaArrayTrans.size());
				}
				
//					if( is_completed == 1 ){
//						rm.updateReportMaster(con, lRunId );
//						rm.updateInActiveLoadAgent(con, lRunId);
//						rm.ltReportDataTemp(con, lRunId);
//						rm.createSummaryTable(con, lRunId);
//						rm.ltSummaryReport(con, lRunId);
//					}

				LogManager.infoLog("For Testing, after insert using TaskExecutor");
			}
		} catch (Throwable e) {
			LogManager.errorLog("Error in Json Data: "+json);
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(con);
			con = null;
			LogManager.infoLog("Closed Connection count in LTProcessinngTimerTask: "+tNo);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		rm = null;
		super.finalize();
	}
}
