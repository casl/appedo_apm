package com.appedo.lt.tcpserver;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import net.sf.json.JSONObject;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class FloodgatesXMLAppend {

	public String sourcefilename = "";
	public  void appendXml(String sourcefilename,String uploadedfilename)throws Throwable{

		DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
		 mdbf.setValidating(false);
		 
		 DocumentBuilderFactory mdbf1 = DocumentBuilderFactory.newInstance();
		 mdbf1.setValidating(false);
		 DocumentBuilder mdb1 = mdbf1.newDocumentBuilder();
		 LogManager.infoLog(uploadedfilename);
		 // Document mdoc1 = mdb1.parse(new FileInputStream(new File(uploadedfilename)));// uploaded file path
		 
		 String strXml = FileUtils.readFileToString(new File(uploadedfilename), "UTF-8");
		 ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		 Document mdoc1 = mdb1.parse(byteArrayInputStream);
		 
		 NodeList mnode1 = mdoc1.getElementsByTagName("vuscript");
		 
		 
		 for(int i=0; i<mnode1.getLength(); i++){
			 
			 Node bNode = (Node) mnode1.item(i);
			 NamedNodeMap attributes = bNode.getAttributes();
			 for (int b = 0; b < attributes.getLength(); b++) 
			 	{
			 		Node theAttribute = attributes.item(b);
			 		if(theAttribute.getNodeName().equals("id")) 
			 		{
			 			LogManager.infoLog(theAttribute.getNodeName() + "=" + theAttribute.getNodeValue());
			 			issamenodeExist(theAttribute.getNodeValue(),bNode,sourcefilename);
					}
//			 		theAttribute = null;
				}
		 }
	}
	
	/**
	 * to check FLOODGATES script file exists 
	 * @return
	 */
	public boolean isScriptFileExist(String strJmeterScriptPath){
		
		File f = null;
		boolean bool = false;
		
		try{
			//strJmeterScriptPath="C:/Appedo/resource/vuscripts/jmeter/vuscripts1.xml";	
			// create new files
			f = new File(strJmeterScriptPath);
			
			// vuscripts.xml if file exists
			bool = f.exists();
			
			// prints
			LogManager.infoLog("File exists: "+bool);
	         
	         if(bool == false){
	        	 // create blank jmeter script file
	        	 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        	 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        	 
	        	// root elements
	        	 Document doc = docBuilder.newDocument();
	        	 Element rootElement = doc.createElement("root");
	        	 doc.appendChild(rootElement);
	        	 
	        	 // child elements
	        	 Element childElement = doc.createElement("vuscripts");
	        	 rootElement.appendChild(childElement);
	        	 
	        	 // write the content into xml file
	        	 TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        	 Transformer transformer = transformerFactory.newTransformer();
	        	 DOMSource source = new DOMSource(doc);
	        	 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
	        	 
	        	 // Output to console for testing
	        	 // StreamResult result = new StreamResult(System.out);
	        	 
	        	 transformer.transform(source, result);
	        }
	         // tests if file exists
	         bool = f.exists();  
//	         f = null;
	         // prints
	         LogManager.infoLog("File exists: "+bool);
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}
		
		return bool;
	}
	/**
	  * to validate the given node is available in Vuscripts.xml
	  * @param nodeid
	  * @param en1
	  * @return
	  * @throws Throwable
	  */
	 public  int issamenodeExist(String nodeid,Node en1, String sourcefilename)throws Throwable	{
		 
		 RunManager runManager = new RunManager();
		 try	{
			 
			 LogManager.infoLog("xml:"+sourcefilename);
			 Boolean nodeexist=false;
			 DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			 //get current date time with Date()
			 Date date = new Date();
			 
			 DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
			 mdbf.setValidating(false);
			 DocumentBuilder mdb = mdbf.newDocumentBuilder();
			 // Document mdoc = mdb.parse(new FileInputStream(new File(sourcefilename)));
			 
			 String strXml = FileUtils.readFileToString(new File(sourcefilename), "UTF-8");
			 ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			 Document mdoc = mdb.parse(byteArrayInputStream);
			 
			 NodeList mentries = mdoc.getElementsByTagName("vuscript");
			 int mnum = mentries.getLength();
			 
			 for (int i=0; i<mnum; i++)	{
				 
				 Element mnode = (Element) mentries.item(i);
				 NamedNodeMap mattributes = mnode.getAttributes();
				 
				 for (int a = 0; a < mattributes.getLength(); a++)	{
					 
					 Node themAttribute = mattributes.item(a);
					 
					 if(themAttribute.getNodeName().equals("id"))	{						 
						
						 LogManager.infoLog(themAttribute.getNodeName() + "=" + themAttribute.getNodeValue());
						 String nv=themAttribute.getNodeValue();
						 // remove node
						 if(nv.equals(nodeid))	{
							 
							 LogManager.infoLog("Node Exist"+nv);
							 // remove the specific node
							 Element element = (Element) mdoc.getElementsByTagName("vuscript").item(i);
							 // remove the specific node
							 element.getParentNode().removeChild(element);
							 Node en=en1.cloneNode(true);
							// add a attribute receivedon attribute to received node
							 Element ve=(Element)en;
							 ve.setAttribute("receivedon", dateFormat.format(date));
							 en =(Node)ve;
							 mdoc.adoptNode(en); 
							
							 Node appendnode = mdoc.getElementsByTagName("vuscripts").item(0);
							 appendnode.appendChild(en);
							 runManager.saveXmlDoc(mdoc, sourcefilename);
							 
							 nodeexist=true;
							 break;
						}
//						nv = null;
					}
//					 themAttribute = null;
				}
//				 mnode = nULL;
//				 MATTRIBUTes = null;
			}
			 if(nodeexist==false)	{			 
				 Node en=en1.cloneNode(true);
				 // add a attribute receivedon attribute to received node 
				 Element ve=(Element)en;
				 ve.setAttribute("receivedon", dateFormat.format(date));
				 en =(Node)ve;				 
				 mdoc.adoptNode(en);		 
				 Node appendnode = mdoc.getElementsByTagName("vuscripts").item(0);
				 appendnode.appendChild(en);				 
				 runManager.saveXmlDoc(mdoc, sourcefilename);
			}
			
//			dateFormat = null;
//			date = null;
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}
		 return 1;
	}
	 /**
		 * to check FLOODGATES Scenario file exists 
		 * @return
		 */
		public boolean isScenarioFileExist(String strJmeterScriptPath){
			
			File f = null;
			boolean bool = false;
			
			try{
				// create new files
				f = new File(strJmeterScriptPath);
				
				// vuscripts.xml if file exists
				bool = f.exists();
				
				// prints
				LogManager.infoLog("File exists: "+bool);
		         
		         if(bool == false){
		        	 // create blank jmeter script file
		        	 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		        	 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		        	 
		        	// root elements
		        	 Document doc = docBuilder.newDocument();
		        	 Element rootElement = doc.createElement("root");
		        	 doc.appendChild(rootElement);
		        	 
		        	 // child elements
		        	 Element childElement = doc.createElement("scenarios");
		        	 rootElement.appendChild(childElement);
		        	 
		        	 // write the content into xml file
		        	 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        	 Transformer transformer = transformerFactory.newTransformer();
		        	 DOMSource source = new DOMSource(doc);
		        	 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
		        	 
		        	 // Output to console for testing
		        	 // StreamResult result = new StreamResult(System.out);
		        	 
		        	 transformer.transform(source, result);
		        }
		         // tests if file exists
		         bool = f.exists();
//		         f = nul	l;
		         // prints
		         LogManager.infoLog("File exists: "+bool);
				
			}catch(Exception e){
				LogManager.errorLog(e);
			}
			
			return bool;
		}
		/**
		 * delete script
		 * 
		 * @param strScriptId
		 * @param strScriptName
		 * @param strVUScriptsXmlPath
		 * @return
		 */
		public JSONObject deleteScript(String strScriptName, String strVUScriptsXmlPath, String strScenarioXmlPath, String strTestType) throws Throwable{
			RunManager rm = new RunManager();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strVUScriptsXmlPath)));
			String strXml = FileUtils.readFileToString(new File(strVUScriptsXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			String expression = null, scenarioexpression = null;
			XPath varxPath =  XPathFactory.newInstance().newXPath();
			if(strTestType.equalsIgnoreCase("APPEDO_LT")){
				expression = "/root/vuscripts/vuscript[@name='"+strScriptName+"']";
			}else if ( strTestType.equalsIgnoreCase("JMETER") ) {
				expression = "/vuscripts/vuscript[@name='"+strScriptName+"']";
			}
			NodeList recfiletypeList = (NodeList) varxPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			LogManager.infoLog("size="+recfiletypeList.getLength());
			int mnum = recfiletypeList.getLength();
			if(mnum>0){
				
				// Document doc1 = db.parse(new FileInputStream(new File(strScenarioXmlPath)));
//				String strXml1 = FileUtils.readFileToString(new File(strScenarioXmlPath), "UTF-8");
//			    ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream(strXml1.getBytes("UTF-8"));
//			    Document doc1 = db.parse(byteArrayInputStream);
//				
//				XPath scenariovarxPath =  XPathFactory.newInstance().newXPath();
//				if(strTestType.equalsIgnoreCase("APPEDO_LT")){
//					scenarioexpression = "/root/scenarios/scenario/script[@name='"+strScriptName+"']";
//				}else if ( strTestType.equalsIgnoreCase("JMETER") ) {
//					scenarioexpression = "/root/scenarios/scenario[@name='"+strScriptName+"']";
//				}
//				NodeList scenarioRecfiletypeList = (NodeList) scenariovarxPath.compile(scenarioexpression).evaluate(doc1, XPathConstants.NODESET);
//				LogManager.infoLog("size="+scenarioRecfiletypeList.getLength());
//				int scenariomnum = scenarioRecfiletypeList.getLength();
//				for(int i=0; i<scenariomnum; i++){
//					Element mnode = (Element) scenarioRecfiletypeList.item(i);
//					mnode.getParentNode().removeChild(mnode);
//					rm.saveXmlDoc(doc1, strScenarioXmlPath);
//					LogManager.infoLog(mnode.getNodeName());
//				}
				
				Element mnode = (Element) recfiletypeList.item(0);
				mnode.getParentNode().removeChild(mnode);
				rm.saveXmlDoc(doc, strVUScriptsXmlPath);
				LogManager.infoLog(mnode.getNodeName());
			}
			
			
			return UtilsFactory.getJSONSuccessReturn("Script deleted.");
		}
		
		/**
		 * delete jmeter scripts and scenarios
		 * 
		 * @param strScriptId
		 * @param strScriptName
		 * @param strVUScriptsXmlPath
		 * @return
		 */
		public JSONObject deleteJmeterScript(String strScenarioName, String[] strScriptNames, String strVUScriptsXmlPath, String strScenarioXmlPath, String strTestType) throws Throwable{
			RunManager rm = new RunManager();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strVUScriptsXmlPath)));
			String strXml = FileUtils.readFileToString(new File(strVUScriptsXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			String expression = null, scenarioexpression = null;
			XPath varxPath =  XPathFactory.newInstance().newXPath();
			for(int j=0; j<strScriptNames.length; j++){
				
				if ( strTestType.equalsIgnoreCase("JMETER") ) {
					expression = "/vuscripts/vuscript[@name='"+strScriptNames[j]+"']";
				}
				NodeList recfiletypeList = (NodeList) varxPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
				LogManager.infoLog("size="+recfiletypeList.getLength());
				int mnum = recfiletypeList.getLength();
				if(mnum>0){
					
					// Document doc1 = db.parse(new FileInputStream(new File(strScenarioXmlPath)));
					String strXml1 = FileUtils.readFileToString(new File(strScenarioXmlPath), "UTF-8");
				    ByteArrayInputStream byteArrayInputStream1 = new ByteArrayInputStream(strXml1.getBytes("UTF-8"));
				    Document doc1 = db.parse(byteArrayInputStream1);
					
					XPath scenariovarxPath =  XPathFactory.newInstance().newXPath();
					if ( strTestType.equalsIgnoreCase("JMETER") ) {
						scenarioexpression = "/root/scenarios/scenario[@name='"+strScenarioName+"']";
					}
					NodeList scenarioRecfiletypeList = (NodeList) scenariovarxPath.compile(scenarioexpression).evaluate(doc1, XPathConstants.NODESET);
					LogManager.infoLog("size="+scenarioRecfiletypeList.getLength());
					int scenariomnum = scenarioRecfiletypeList.getLength();
					for(int i=0; i<scenariomnum; i++){
						Element mnode = (Element) scenarioRecfiletypeList.item(i);
						mnode.getParentNode().removeChild(mnode);
						rm.saveXmlDoc(doc1, strScenarioXmlPath);
						LogManager.infoLog(mnode.getNodeName());
					}
					
					Element mnode = (Element) recfiletypeList.item(0);
					mnode.getParentNode().removeChild(mnode);
					rm.saveXmlDoc(doc, strVUScriptsXmlPath);
					LogManager.infoLog(mnode.getNodeName());
				}
				
			}
			return UtilsFactory.getJSONSuccessReturn("Script deleted.");
		}
		
		/**
		 * delete scenatio
		 * 
		 * @param strUserScenarioId
		 * @param strUserScenarioName
		 * @param strScenarioXmlPath
		 * @return
		 * @throws Throwable
		 */
		public JSONObject deleteScenario(String strUserScenarioId, String strUserScenarioName, String strScenarioXmlPath) throws Throwable{
			RunManager rm = new RunManager();
			JSONObject joRtn = null;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strScenarioXmlPath)));
			String strXml = FileUtils.readFileToString(new File(strScenarioXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			NodeList entries = doc.getElementsByTagName("scenario");
			int num = entries.getLength();
			for(int i=0; i<num; i++){
				Node aNode = (Node) entries.item(i);
				NamedNodeMap attributes = aNode.getAttributes();
				String strScenarioId = null;
			 	String strScenarioName = null;
			 	for (int a = 0; a < attributes.getLength(); a++) 
			 	{
			 		Node theAttribute = attributes.item(a);
			 		if(theAttribute.getNodeName().equals("id")) 
			 		{
			 			strScenarioId = theAttribute.getNodeValue();
					}
			 		if(theAttribute.getNodeName().equals("name")){
			 			strScenarioName = theAttribute.getNodeValue();
			 		}
				}
			 	if(strScenarioId.equals(strUserScenarioId) && strScenarioName.equalsIgnoreCase(strUserScenarioName)){
			 		Element element = (Element) doc.getElementsByTagName("scenario").item(i);
			 		element.getParentNode().removeChild(element);
			 		rm.saveXmlDoc(doc, strScenarioXmlPath);
			 		joRtn = UtilsFactory.getJSONSuccessReturn("Scenario deleted. ");
			 		break;
			 	}
			}
			return joRtn;
		}
		/**
		 * delete scenario's script
		 * 
		 * @param strUserScriptName
		 * @param strUserScenarioId
		 * @param strUserScenarioName
		 * @param strScenarioXmlPath
		 * @return
		 * @throws Throwable
		 */
		public JSONObject deleteScenarioScript(String strUserScriptName, String strUserScenarioId, String strUserScenarioName, String strScenarioXmlPath) throws Throwable{
			RunManager rm = new RunManager();
			JSONObject joRtn = null;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strScenarioXmlPath)));
			
			String strXml = FileUtils.readFileToString(new File(strScenarioXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			XPath varxPath =  XPathFactory.newInstance().newXPath();
			String expression = "/root/scenarios/scenario/script[@name='"+strUserScriptName+"']";
			NodeList recfiletypeList = (NodeList) varxPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			LogManager.infoLog("size="+recfiletypeList.getLength());
			int mnum = recfiletypeList.getLength();
			for(int i=0; i<mnum; i++){
				Element mnode = (Element) recfiletypeList.item(i);
				Node aNode = mnode.getParentNode();
				NamedNodeMap attributes = aNode.getAttributes();
				String strScenarioId = null;
			 	String strScenarioName = null;
			 	LogManager.infoLog("attributes.getLength()"+attributes.getLength());
				for (int a = 0; a < attributes.getLength(); a++) 
			 	{
			 		Node theAttribute = attributes.item(a);
			 		if(theAttribute.getNodeName().equals("id")) 
			 		{
			 			strScenarioId = theAttribute.getNodeValue();
					}
			 		if(theAttribute.getNodeName().equals("name")){
			 			strScenarioName = theAttribute.getNodeValue();
			 		}
				}
				if(strScenarioId.equals(strUserScenarioId) && strScenarioName.equals(strUserScenarioName)){
					mnode.getParentNode().removeChild(mnode);
					rm.saveXmlDoc(doc, strScenarioXmlPath);
					LogManager.infoLog(mnode.getNodeName());
					joRtn = UtilsFactory.getJSONSuccessReturn("Script deleted.");
				}
			}
			return joRtn;
		}
		public boolean isScriptAvailable(String strScriptName, String strScriptXmlPath) throws Throwable{
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strScriptXmlPath)));
			String strXml = FileUtils.readFileToString(new File(strScriptXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			NodeList entries = doc.getElementsByTagName("script");
			int num = entries.getLength();
			for(int i=0; i<num; i++){
				Node aNode = (Node) entries.item(i);
				NamedNodeMap attributes = aNode.getAttributes();
			 	String strScenarioName = null;
			 	String strScenarioId = null;
			 	for (int a = 0; a < attributes.getLength(); a++) 
			 	{
			 		Node theAttribute = attributes.item(a);
			 		if(theAttribute.getNodeName().equals("name")){
			 			strScenarioName = theAttribute.getNodeValue();
			 		}
			 		if(theAttribute.getNodeName().equals("id")) 
			 		{
			 			strScenarioId = theAttribute.getNodeValue();
					}
				}
			 	LogManager.infoLog(i +"is name:"+strScenarioName+" and id:"+strScenarioId);
			 	if(strScriptName.equals(strScenarioName)){
			 		LogManager.infoLog("is matched");
			 		deleteScenario(strScenarioId, strScenarioName, strScriptXmlPath);
			 		return true;
			 	}
			}
			return false;
		}
		
		public int getNoofScriptsScenarios(String strPath, String expression) throws Exception {
			int nCount = 0;
			
			File f = new File(strPath);
			
			// vuscripts.xml if file exists
			boolean bool = f.exists();
			
			// prints
			LogManager.infoLog("File exists: "+bool);
	         
			if(bool == true){
				DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
				mdbf.setValidating(false);
				
				DocumentBuilderFactory mdbf1 = DocumentBuilderFactory.newInstance();
				mdbf1.setValidating(false);
				DocumentBuilder mdb1 = mdbf1.newDocumentBuilder();
				// Document mdoc1 = mdb1.parse(new FileInputStream(new File(strPath)));// uploaded file path
				
				String strXml = FileUtils.readFileToString(new File(strPath), "UTF-8");
			    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			    Document mdoc1 = mdb1.parse(byteArrayInputStream);
				
				NodeList mnode1 = mdoc1.getElementsByTagName(expression);
				nCount = mnode1.getLength();	
				return nCount;
			}else {
				return 0;
			}
		}
		
		/**
		 * To check variables file exist
		 * @param strJmeterScriptPath
		 * @return
		 */
		public static boolean isVariablesFileExist(String strJmeterScriptPath){
			
			File f = null;
			boolean bool = false;
			
			try{
				//strJmeterScriptPath="C:/Appedo/resource/vuscripts/jmeter/vuscripts1.xml";	
				// create new files
				f = new File(strJmeterScriptPath);
				
				// vuscripts.xml if file exists
				bool = f.exists();
				
				// prints
				LogManager.infoLog("File exists: "+bool);
		         
		         if(bool == false){
		        	 // create blank jmeter script file
		        	 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		        	 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		        	 
		        	// root elements
		        	 Document doc = docBuilder.newDocument();
		        	 Element rootElement = doc.createElement("variables");
		        	 doc.appendChild(rootElement);
		        	 
		        	 // write the content into xml file
		        	 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        	 Transformer transformer = transformerFactory.newTransformer();
		        	 DOMSource source = new DOMSource(doc);
		        	 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
		        	 
		        	 // Output to console for testing
		        	 // StreamResult result = new StreamResult(System.out);
		        	 
		        	 transformer.transform(source, result);
		        }
		         // tests if file exists
		         bool = f.exists();  
//		         f = null;
		         // prints
		         LogManager.infoLog("File exists: "+bool);
				
			}catch(Exception e){
				LogManager.errorLog(e);
			}
			
			return bool;
		}
}
