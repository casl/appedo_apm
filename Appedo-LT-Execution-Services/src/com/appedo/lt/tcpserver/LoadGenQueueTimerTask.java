package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LoadGenQueueTimerTask extends TimerTask{
	
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	AmazonConfiguration amazonConfiguration = null;
	RunManager runManager = null;
	String region, instanceId;
	JSONObject instance_config, region_details;
	String[] regions, distributions;
	Connection con = null;
	int totalUser = 0;
	JSONArray inactiveInstance;
	int totalAvailability = 0;
	private long tNo;

	public LoadGenQueueTimerTask(){
		tNo = Constants.getLongCount();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Connection count in LoadGenQueueTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		StringBuilder stringBuilder_IP = new StringBuilder();
		StringBuilder stringBuilder_Distribution = new StringBuilder();
		boolean isFirstDistribution = true, isFirstIP = true;
		try {
			runManager = new RunManager();
			// For Paid Users
			if( LTScheduler.htFGTestPaidQueues.size() > 0 ){
				amazonConfiguration = new AmazonConfiguration();
				loadTestSchedulerBean = LTScheduler.peekFGPaidTest();
				regions = loadTestSchedulerBean.getLoadGenRegions().split(",");
				distributions = loadTestSchedulerBean.getDistributions().split(",");
				
				// To get maxUser for Particular Instance
				int userCount = runManager.getMaxUserCount(con, loadTestSchedulerBean);
				totalUser = loadTestSchedulerBean.getMaxUserCount();
				
				for( int i=0; i<regions.length; i++ ){
					double percentageValue = Double.valueOf(distributions[i])/100;
					int distributionCount = (int) (totalUser * percentageValue);
							
					int neededInstance = 0;
					if( distributionCount % userCount == 0 ){
						neededInstance = distributionCount/userCount;
					}else{
						neededInstance = (distributionCount/userCount) + 1;
					}
					if( stringBuilder_Distribution.length() > 0){
						isFirstDistribution = false;
					}
					for ( int dist=0; dist<neededInstance; dist++ ){
						if(isFirstDistribution){
							stringBuilder_Distribution.append(Integer.valueOf(distributions[i])/neededInstance);
							isFirstDistribution = false;
						}else{
							stringBuilder_Distribution.append(",").append(Integer.valueOf(distributions[i])/neededInstance);
						}
					}
					
					inactiveInstance = runManager.getInactiveLoadAgentDetails(con, regions[i], "WINDOWS", Constants.FG_CONTROLLER_IP);
					
					if ( inactiveInstance.size() > 0 && neededInstance <= inactiveInstance.size()){
						for( int j=0;j<=inactiveInstance.size(); j++ ){
							runManager.updateLoadAgentDetails(con, ((JSONObject)inactiveInstance.get(j)).getString("public_ip"), ((JSONObject)inactiveInstance.get(j)).getString("instance_id"));
							runManager.insertRunGenDetails(con, loadTestSchedulerBean, ((JSONObject)inactiveInstance.get(j)).getString("instance_id"));
							if( stringBuilder_IP.length() > 0){
								isFirstIP = false;
							}
							if( isFirstIP ){
								stringBuilder_IP.append(((JSONObject)inactiveInstance.get(j)).getString("public_ip"));
								isFirstIP = false;
							}else{
								stringBuilder_IP.append(",").append(((JSONObject)inactiveInstance.get(j)).getString("public_ip"));
							}
							if( j == neededInstance - 1){
								break;
							}
						}
					}
				}
				
				if( stringBuilder_IP.length() > 0){
					if( stringBuilder_Distribution.toString().split(",").length ==  stringBuilder_IP.toString().split(",").length){
						loadTestSchedulerBean = LTScheduler.pollFGPaidTest();
						// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
						loadTestSchedulerBean.setDistributions( stringBuilder_Distribution.toString() );
						loadTestSchedulerBean.setLoadgenIpAddress( stringBuilder_IP.toString() );
						LTScheduler.ltProcessingQueue( loadTestSchedulerBean );
						// runManager.updatePrivateCounters(con, "lt_controller_queue", LTScheduler.ltQueues.size());
					} else {
						runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId());
					}
				}
			}
			
			// For Free Users
			if( LTScheduler.htFGTestQueues.size() > 0){
				loadTestSchedulerBean = LTScheduler.peekFGTest();
				// region = loadTestSchedulerBean.getLoadGenRegions();
				
				inactiveInstance = runManager.getInactiveLoadAgentDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS", null);
				if( inactiveInstance.size() == 0 ){
					// Pending to be discussed
				}else{
					for( int j = 0; j<inactiveInstance.size(); ){
						JSONObject joObject = (JSONObject) inactiveInstance.get(j);
						loadTestSchedulerBean = LTScheduler.pollFGTest();
						// runManager.updatePrivateCounters(con, "lt_free_user_queue", LTScheduler.htFGTestQueues.size());
						loadTestSchedulerBean.setLoadgenIpAddress( joObject.getString("public_ip") );
						runManager.updateLoadAgentDetails(con, joObject.getString("public_ip"), ((JSONObject)inactiveInstance.get(j)).getString("instance_id"));
						runManager.insertRunGenDetails(con, loadTestSchedulerBean, joObject.getString("instance_id"));
						LTScheduler.ltProcessingQueue( loadTestSchedulerBean );
						// runManager.updatePrivateCounters(con, "lt_controller_queue", LTScheduler.ltQueues.size());
//						joObject = null;
						break;
					}
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			DataBaseManager.close(con);
//			con = null;
//			LogManager.infoLog("Closed Connection count in LoadGenQueueTimerTask: "+tNo);
//			region = null;
//			runManager = null;
//			regions = null;
//			instanceId = null;
//			amazonConfiguration = null;
//			loadTestSchedulerBean = null;
			UtilsFactory.clearCollectionHieracy( instance_config );
			UtilsFactory.clearCollectionHieracy( region_details );
			UtilsFactory.clearCollectionHieracy( inactiveInstance );
			UtilsFactory.clearCollectionHieracy( stringBuilder_IP );
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
