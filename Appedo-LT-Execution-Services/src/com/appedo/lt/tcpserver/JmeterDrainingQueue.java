package com.appedo.lt.tcpserver;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.JmeterScriptManager;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.manager.LogManager;

public class JmeterDrainingQueue extends TimerTask {

	LoadTestSchedulerBean loadTestSchedulerBean = null;
	RunManager runManager = null;
	String region;
	Connection con = null;

	public JmeterDrainingQueue() {
		this.con = DataBaseManager.giveConnection();
	}

	@Override
	public void run() {
		try {
			runManager = new RunManager();
			JmeterScriptManager jmeterScriptManager = null;
			List<String> list = null;
			List<String> listParam = null;
			boolean isJmxValid = true;
			if (LTScheduler.jmeterQueues.size() > 0) {

				jmeterScriptManager = new JmeterScriptManager();
				list = new ArrayList<String>();
				listParam = new ArrayList<String>();

				loadTestSchedulerBean = LTScheduler.pollJmeterProcessingQueue();
				// insert report master
				// long currentMillis = System.currentTimeMillis();

				String strJmeterReportUId = String.valueOf(loadTestSchedulerBean.getRunId()).trim();

				// Jmx(jmeter test ) file Path
				String strJmxFilePath = Constants.JMETERSCENARIOSPATH +loadTestSchedulerBean.getUserId()+File.separator+ loadTestSchedulerBean.getScenarioName() + ".jmx";
				// test file name
				String strScenarioFileName = loadTestSchedulerBean.getScenarioName() + ".jmx";
				// test result file name
				String strJtlFileName = strJmeterReportUId + ".csv";

				// split the loadgen to upload jmx & run the script
				// arrayLoadgenIpAddress=loadgenIpAddress.trim().split(",");
				String arrayLoadgenIpAddress = loadTestSchedulerBean.getLoadgenIpAddress();

				// look for the parameter file
				boolean isContainsCsv = jmeterScriptManager.isJmxContainsCsvFile(strJmxFilePath);

				if (isContainsCsv) { // means jmx contains csv files
					// is csv files exist in the repository
					Boolean isParamFilesExist = jmeterScriptManager.isParamFilesExist(strJmxFilePath, Constants.JMETERSCENARIOSPATH+loadTestSchedulerBean.getUserId());
					// if not do not run the script file
					if (isParamFilesExist) {
						// if exist get the list file names
						listParam = jmeterScriptManager.getParametersFile(strJmxFilePath);
					} else {
						// File are missed from repository
						isJmxValid = false;
						// joMessage =
						// UtilsFactory.getJSONFailureReturn("Parameter files were missed");
						LogManager.infoLog("Parameter files were missed");
					}
				}

				if (isJmxValid) { // for valid jmx

					// for(int i=0;i<arrayLoadgenIpAddress.length;i++) {
					// This need to be change as multiple files upload
					LogManager.infoLog("#loadgen :" + arrayLoadgenIpAddress);
					// jmeterScriptManager.uploadFile(strJmxFilePath, arrayLoadgenIpAddress);
					// Upload parameter file --need to be work
					if (listParam.size() > 0) {

						for (String str : listParam) {
							jmeterScriptManager.uploadFile(Constants.JMETERSCENARIOSPATH+loadTestSchedulerBean.getUserId()+File.separator + str, arrayLoadgenIpAddress);
						}
					}
					// run the script
					jmeterScriptManager.runJmeterScript(arrayLoadgenIpAddress, strJtlFileName, strScenarioFileName);
					// get result file for ran scenario at the agent place
					jmeterScriptManager.getResultFile(arrayLoadgenIpAddress, strJtlFileName, Constants.JMETERTESTJTLPATH);
					// collect the all result
					list.add(Constants.JMETERTESTJTLPATH + strJtlFileName);

					// }
					strJtlFileName = strJmeterReportUId + ".csv";
					jmeterScriptManager.mergeCsvFiles(list, strJmeterReportUId);
					jmeterScriptManager.sendResultFile(con, Constants.JMETERCSVPATH + strJtlFileName, loadTestSchedulerBean.getRunId(), strJmeterReportUId);

					 // updatereportMaster(con,ltBean.getRunId(),nJmetercreatedUser,nJmetercompletedUser,"completed",strJmeterReportUId);

					// runManager.updateReportMaster(con, loadTestSchedulerBean.getRunId() );
					runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId() );
					
					// Updateing Scriptwise Status
					String scriptIds[] = loadTestSchedulerBean.getScriptId().split(",");
					String scriptNames[] = loadTestSchedulerBean.getScriptName().split(",");
					for ( int k=0; k<scriptIds.length; k++ ){
						HashMap<String, Object> hmStatuswiseDetails = new HashMap<String, Object>();
						hmStatuswiseDetails.put("runid", String.valueOf(loadTestSchedulerBean.getRunId()));
						hmStatuswiseDetails.put("script_id", String.valueOf(scriptIds[k]));
						hmStatuswiseDetails.put("created_users", "0");
						hmStatuswiseDetails.put("completed_users", "0");
						hmStatuswiseDetails.put("http_200_count", "0");
						hmStatuswiseDetails.put("http_300_count", "0");
						hmStatuswiseDetails.put("http_400_count", "0");
						hmStatuswiseDetails.put("http_500_count", "0");
						hmStatuswiseDetails.put("is_completed", "0");
						hmStatuswiseDetails.put("error_count", "0");
						hmStatuswiseDetails.put("script_name", scriptNames[k]);
						if (runManager.isRecordAvailable(con, String.valueOf(loadTestSchedulerBean.getRunId()), String.valueOf(scriptIds[k]))) {
							hmStatuswiseDetails.put("is_completed", "1");
							runManager.updateScriptwiseStatus(con, hmStatuswiseDetails);
						} else {
							runManager.insertScriptwiseStatus(con, hmStatuswiseDetails);
						}
					}
					// Message to user
					// joMessage = UtilsFactory.getJSONSuccessReturn("Run completed");
					LogManager.infoLog("Run Completed");
					System.out.println("TESTING PURPOSE");
					
//					list = null;
//					listParam = null;
//					jmeterScriptManager = null;
				}
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			region = null;
//			runManager = null;
//			loadTestSchedulerBean = null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
