package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.TimerTask;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.TaskExecutor;
import com.appedo.manager.LogManager;

public class LTDrainingTimerTask extends TimerTask{
	RunManager rm = null;
	Connection con = null;
	private long tNo;
	
	public LTDrainingTimerTask()throws Throwable{
		tNo = Constants.getLongCount();
		this.rm = new RunManager();
		this.con = DataBaseManager.giveConnection();
		LogManager.infoLog("Opened Connection count in LTDrainingTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		
		try {
			
			TaskExecutor executor = null;
			int ltActualQueue = LTScheduler.reportDataQueues.size();
			int connectionSize = DataBaseManager.getMaxTotal() - Integer.valueOf(Constants.MAX_THREADS_RETAINED) - DataBaseManager.getCurrentActiveCount();
			int ltQueueSize = ltActualQueue < connectionSize ? ltActualQueue : connectionSize;
			for(int i=0; i<ltQueueSize; i++ ){
//				if (DataBaseManager.getCurrentActiveCon() < connectionSize) {
					String strReportData = LTScheduler.pollReportData();
					LogManager.infoLog("Converted Data: "+strReportData);
					if (strReportData != null) {
						// rm.updatePrivateCounters(con, "lt_exec_services_queue", LTScheduler.reportDataQueues.size());
						executor = TaskExecutor.getExecutor(Constants.DEFAULT_LT_THREADPOOL_NAME);
						executor.submit(new LTProcessinngTimerTask(strReportData));
					}
//				}
			}
			// System.out.println("DB Connection Count: "+DataBaseManager.getCurrentActiveCon());
			LogManager.infoLog("DB Connection Count: "+DataBaseManager.getCurrentActiveCount());
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		LogManager.infoLog("Closed Connection count in LTDrainingTimerTask: " + tNo);
		rm = null;
		super.finalize();
	}
}
