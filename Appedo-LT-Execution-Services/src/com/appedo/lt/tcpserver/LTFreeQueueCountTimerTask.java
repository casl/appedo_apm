package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LTFreeQueueCountTimerTask extends TimerTask{
	AmazonConfiguration amazonConfiguration = null;
	RunManager runManager = null;
	Connection con = null;
	JSONObject region_details, instance_config;
	String instanceId;
	JSONArray instance_status;
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	long tNo;

	public LTFreeQueueCountTimerTask(){
		this.con = DataBaseManager.giveConnection();
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in LTFreeQueueCountTimerTask: "+tNo);
	}
	
	@Override
	public void run() {
		try {
			
			if( LTScheduler.htFGTestQueues.size() <= 4 && LTScheduler.htFGTestQueues.size() > 0){
				runManager = new RunManager();
				instance_status = runManager.getInactiveLoadAgentDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS", null);
				int count = runManager.getLoadAgentDetailsCount(con, Constants.FG_CONTROLLER_IP);
				if( instance_status.size() == 0 && count == 0 ){
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("instance_id", "123_abc");
					hm.put("region", Constants.DEFAULT_LOADGEN);
					hm.put("endpoint", "");
					hm.put("location", "");
					hm.put("instance_status", "inactive");
					hm.put("public_ip", Constants.FG_CONTROLLER_IP);
					hm.put("start_time", String.valueOf(new Date().getTime()));
					hm.put("os_type", "WINDOWS");
					runManager.insertLoadAgentDetails( con, hm );
				}
			}
			
			else if( LTScheduler.htFGTestQueues.size() > 4 ){
				amazonConfiguration = new AmazonConfiguration();
				runManager = new RunManager();
				instance_status = runManager.getInactiveLoadAgentDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS", null);
				if( instance_status.size() == 0 ){
					// Max Instance Creation for Free User Check
					int maxInstanceCreated = runManager.getCreatedInstanceCount(con, Constants.DEFAULT_LOADGEN, "WINDOWS");
					if( maxInstanceCreated <= Integer.valueOf(Constants.MAX_LOADGEN_CREATION) ){
						String instanceType = runManager.ltLicenseInstanceDetails(con, "level0");
						region_details = runManager.getRegionDetails(con, Constants.DEFAULT_LOADGEN, "WINDOWS");	
						instance_config = runManager.getInstanceDetails(con, "LT");
						int availability = amazonConfiguration.getAmazonCount(region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"));
						if( region_details.getInt("reservation") - availability != 0){
							// List of Threads for stored thread variable
							List<Thread> threads = new ArrayList<Thread>();
							if( instance_config != null ){
								loadTestSchedulerBean = LTScheduler.peekFGTest();
								for ( int i = 0; i< ( LTScheduler.htFGTestQueues.size() )/4; i++ ){
									Thread th = new InstanceCreationThread(instance_config, region_details, instanceId, loadTestSchedulerBean, con, instanceType);
									th.start();
									threads.add(th);
								}
							}
							
							// To check all Threads status before going to finally block
							while(true){
								 int count = 0;
								 for ( Thread th : threads){
									 if(!th.isAlive()){
										 count ++;
									 }
								 }
								 if(count == threads.size()){
									 break;
								 }
								 else{
									Thread.sleep(1000);
								 }
							 }
						}
					}
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			runManager = null;
//			instanceId = null;
//			amazonConfiguration = null;
			UtilsFactory.clearCollectionHieracy( instance_config );
			UtilsFactory.clearCollectionHieracy( region_details );
			UtilsFactory.clearCollectionHieracy( instance_status );
//			DataBaseManager.close(con);
//			con = null;
//			LogManager.infoLog("Closed Connection count in LTFreeQueueCountTimerTask: "+tNo);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
