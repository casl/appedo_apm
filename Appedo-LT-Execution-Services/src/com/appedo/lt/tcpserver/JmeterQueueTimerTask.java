package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.manager.LogManager;

public class JmeterQueueTimerTask extends TimerTask{
	
	Connection con = null;
	AmazonConfiguration amazonConfiguration = null;
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	RunManager runManager = null;
	String instanceId;
	String region = null;
	
	public JmeterQueueTimerTask(){
		this.con = DataBaseManager.giveConnection();
	}

	@Override
	public void run() {
		try {
			if( LTScheduler.htJmeterQueues.size() > 0 ){
				runManager = new RunManager();
				amazonConfiguration = new AmazonConfiguration();
				loadTestSchedulerBean = LTScheduler.peekJmeterTest();
				if( loadTestSchedulerBean != null ){
					region = loadTestSchedulerBean.getLoadGenRegions();
					JSONObject instance_config = runManager.getInstanceDetails(con, "LT");
					
					JSONArray inactiveInstance = runManager.getInactiveLoadAgentDetails(con, region, "FEDORA", null);
					
					if( inactiveInstance.size()> 0){
						LogManager.infoLog("Instance Available in DB");
					}
					else {
						JSONObject region_details = runManager.getRegionDetails(con, region, "FEDORA");	
						int availability = runManager.updateInstanceAvailability(con, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), region_details.getString("os_type") );
						if( region_details.getInt("reservation") - availability > 0 ){
							
							String instanceType = null;
							instanceType = runManager.getInstanceType(con, loadTestSchedulerBean.getMaxUserCount(), "FEDORA");	
							List<Thread> threads = new ArrayList<Thread>();
							Thread th = new InstanceCreationThread(instance_config, region_details, instanceId, loadTestSchedulerBean, con, instanceType);
							th.start();
							threads.add(th);
							
							// To check all Threads status before going to finally block
							while(true){
								 int count = 0;
								 for ( Thread th1 : threads){
									 if(!th1.isAlive()){
										 count ++;
									 }
								 }
								 if(count == threads.size()){
									 break;
								 }
								 else{
									Thread.sleep(1000);
								 }
							 }
							
//							region_details = null;
//							inactiveInstance = null;
						}else{
							loadTestSchedulerBean = LTScheduler.pollJmeterTest();
							runManager.updateNonAvailabilityRegion(con, "No Region Available", loadTestSchedulerBean);
//							loadTestSchedulerBean = null;
						}
					}
				}	
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			runManager = null;
//			amazonConfiguration = null;
//			loadTestSchedulerBean = null;
//			instanceId = null;
//			region = null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
