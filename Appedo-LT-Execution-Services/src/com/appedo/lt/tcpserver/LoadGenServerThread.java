package com.appedo.lt.tcpserver;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.dbi.UserDBI;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LoadGenServerThread extends Thread {
	Connection con = null;
	RunManager rm = null;
	LTScheduler ltScheduler = null;

	long lUserid = 1l;
	Socket socketCon = null;
	int varFirst = 0;
	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	LoadGenServerThread(Socket sCon) throws Throwable {
		try {
			this.socketCon = sCon;
			rm = new RunManager();
			ltScheduler = new LTScheduler();
			start();
		} catch (Throwable th) {
			LogManager.errorLog(th);
		}
	}

	public void run() {
		try {
			acceptData();
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}

	public synchronized void acceptData() throws Throwable {

		String operation = "";
		int streamLength = 0;
		InetAddress address = null;
		OutputStream varos = null;
		InputStream instream = null;
		String strReceivedStreamContent = "";
		UserDBI userDBI = null;
		long lUserId = 0;
		long lRunId = 0L;
		long lcurrentTimeMillis = -1l;
		boolean isCancelled = false;

		try {
			// if(socketCon.getInetAddress().getHostName()=="127.0.0.1")
			// InetAddress ip = InetAddress.getLocalHost();
			/** print the hostname and port number of the connection */
			/** Instantiate an InputStream with the optional character encoding. */
			InputStream bis = socketCon.getInputStream();
			/** Read the socket's InputStream and append to a StringBuffer */
			// To get the header information
			OutputStream os = socketCon.getOutputStream();
			// Get Header from received conection
			String header = rm.readHeader(bis);
			// System.out.println("header  :" + header);
			String[] tokens = header.trim().split(": ");
			operation = tokens[0].toString();
			streamLength = Integer.parseInt(tokens[1].toString().split("\r\n")[0]);
			LogManager.infoLog("OPERATION =" + operation);

			if (operation.equals("VUSCRIPT")) {

				String strZipSourcePath = null;
				FileOutputStream fileOutputStream = null;
				lcurrentTimeMillis = System.currentTimeMillis();

				try {
					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);

					String strSourcePath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator;
					strZipSourcePath = strSourcePath + File.separator + lcurrentTimeMillis + "_" + lUserId + ".rar";

					fileOutputStream = new FileOutputStream(strZipSourcePath);
					int nRead;
					byte[] bytesVUScriptData = new byte[streamLength];
					int timeoutCount = 0;

					while (streamLength > 0) {
						if (bis.available() > 0) {
							nRead = bis.read(bytesVUScriptData, 0, streamLength);
							streamLength = streamLength - nRead;
							fileOutputStream.write(bytesVUScriptData, 0, nRead);
							timeoutCount = 0;
						} else {
							Thread.sleep(5000);
							timeoutCount++;
						}
						if (timeoutCount >= 24) {
							isCancelled = true;
							break;
						}
					}
				} catch (Exception e) {

					LogManager.errorLog(e);

				} finally {
					fileOutputStream.flush();
					fileOutputStream.close();
					// fileOutputStream=null;
					// strZipSourcePath = null;
					// fileOutputStream = null;
				}
			} else {
				if (streamLength > 0) {
					int readCount = 0;
					byte[] bytes = new byte[streamLength];

					while (readCount < streamLength) {
						readCount += bis.read(bytes, readCount, streamLength - readCount);
					}
					strReceivedStreamContent = new String(bytes);
				}
			}

			switch (operation) {

			case "TEST": {
				String strresponse = null;
				byte[] b = null;
				try {

					strresponse = "OK: 0\r\nurl= " + Constants.LT_EXECUTION_SERVICES_FAILED.trim() + "\r\n\r\n";
					b = strresponse.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// strresponse = null;
					// b = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "login": {
				String strresponse = null;
				String strEmailId = null;
				String strPass = null;
				String strModuleName = null;
				String strVersion = null;

				String userCount = "0";
				String uniqueMachineId = "";
				String status = "1";
				String message = "";

				byte[] b = null;
				try {

					userDBI = new UserDBI();
					con = DataBaseManager.giveConnection();
					// Header contains user name , Pass & appedo LT Version

					// user email id
					strEmailId = tokens[1].toString().split("=")[1].trim().split("\r\n")[0];
					LogManager.infoLog("email id " + strEmailId);
					strPass = tokens[1].toString().split("\r\n")[2].trim().split("=")[1];

					if (header.trim().contains("version")) {
						// Version
						strModuleName = tokens[1].toString().split("\r\n")[3].trim().split("=")[1];
						strVersion = tokens[1].toString().split("\r\n")[4].trim().split("=")[1];
						if (strVersion.trim() != null) {
							if (userDBI.validateAppedoLTVersion(con, strModuleName, strVersion) == false) {
								status = "0";
								message = "Invalid version";
							}

						}

					}
					LogManager.infoLog("pawss :" + strPass);

					// lUserId = userDBI.validateUserIdAndPassword(con, strEmailId.trim(), strPass.trim());
					if (status == "1") {
						HashMap<String, String> userDetail = new HashMap<String, String>();
						userDetail = userDBI.validateUserIdAndPasswordAndGetMachineId(con, strEmailId.trim(), strPass.trim());
						if (userDetail.size() > 0) {
							lUserId = Long.parseLong(userDetail.get("userid"));
						} else {
							status = "0";
							message = "Email ID and Password does not match";
							lUserId = 0;
						}

						if (userDetail.get("lt_max_vusers") != null) {
							userCount = userDetail.get("lt_max_vusers");
						}

						if (userDetail.get("unique_machine_id") != null) {
							uniqueMachineId = userDetail.get("unique_machine_id");
						}
					}

					// validate user email id
					// if email success send below comand

					strresponse = "OK: 0\r\n" + "success= " + status + "\r\n" + "userid= " + lUserId + "\r\n" + "machineid= " + uniqueMachineId + "\r\n" + "usercount= " + userCount + "\r\n" + "message= " + message + "\r\n\r\n";

					b = strresponse.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// userDBI = null;
					// b = null;
					// strresponse = null;
					// strEmailId = null;
					// strPass = null;
					UtilsFactory.close(os);
				}

				break;
			}

			case "GETSCRIPT": {

				String strScriptName = null;
				String FILE_TO_SEND = null;
				try {
					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);
					strScriptName = tokens[1].toString().trim().split("=")[2].trim().split("\r\n")[0];
					// To get Enterprise id
					con = DataBaseManager.giveConnection();
					int enterpriseId = rm.getEnterpriseId(con, lUserId);
					// get script id
					long lScriptId = rm.checkScriptExist(con, lUserId, strScriptName, enterpriseId, false);

					if (lScriptId > 0) {
						Node nodeVuscript = rm.getnodefromVuscripts(lScriptId, lUserId);

						boolean bStatus = rm.createVUScriptXml(lScriptId, lUserId, nodeVuscript);

						if (bStatus) {
							String SOURCE_FOLDER = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator + lUserId + "_" + lScriptId;
							String OUTPUT_ZIP_FILE = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator + lUserId + "_" + lScriptId + ".zip";
							rm.zipDirectory(new File(SOURCE_FOLDER), OUTPUT_ZIP_FILE);
						}
					}
					// send file
					try {

						FILE_TO_SEND = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator + lUserId + "_" + lScriptId + ".zip";
						File myFile = new File(FILE_TO_SEND);

						byte[] mybytearray = new byte[(int) myFile.length()];

						String strresponse = "GETSCRIPT: " + mybytearray.length + "\r\n\r\n";
						byte[] b = strresponse.getBytes();
						os.write(b, 0, b.length);

						FileInputStream fis = new FileInputStream(myFile);

						bis = new BufferedInputStream(fis);
						bis.read(mybytearray, 0, mybytearray.length);
						os.write(mybytearray, 0, mybytearray.length);
						// os.flush();
					} finally {
						os.flush();
						if (bis != null)
							bis.close();
						if (os != null)
							os.close();
					}

					// delete rar file
					File fToDelete = new File(FILE_TO_SEND);

					if (fToDelete.exists()) {
						fToDelete.delete();
					}

				} catch (Exception ee) {
					LogManager.errorLog(ee);
				} finally {
					// strScriptName = null;
					// FILE_TO_SEND = null;
					DataBaseManager.close(con);
					con = null;
				}
				break;
			}
			case "AVAILABLESCRIPTS": {

				String strresponse = null;
				String strAvailableScripts = null;
				byte[] b = null;
				try {
					con = DataBaseManager.giveConnection();
					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);
					strAvailableScripts = rm.getAvailableScripts(con, lUserId).toString();

					if (strAvailableScripts.toString().endsWith(",")) {
						strAvailableScripts = strAvailableScripts.substring(0, strAvailableScripts.length() - 1);
					}
					if (lUserId > 0) {
						strresponse = "AVAILABLESCRIPTS: 0\r\nscripts= " + strAvailableScripts + "\r\n\r\n";
					}

					b = strresponse.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception ee) {
					LogManager.errorLog(ee);
				} finally {
					UtilsFactory.close(os);
					DataBaseManager.close(con);
					con = null;
				}

				break;

			}

			case "AVAILABLEVARIABLES": {

				String strresponse = null;
				String strAvailableVariables = null;
				byte[] b = null;

				try {

					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);
					strAvailableVariables = rm.getAvailableVarables(lUserId).toString();

					if (strAvailableVariables.toString().endsWith(",")) {
						strAvailableVariables = strAvailableVariables.substring(0, strAvailableVariables.length() - 1);
					}
					if (lUserId > 0) {
						strresponse = "AVAILABLEVARIABLES: 0\r\nvariables= " + strAvailableVariables + "\r\n\r\n";
					}

					b = strresponse.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception ee) {
					LogManager.errorLog(ee);
				} finally {
					UtilsFactory.close(os);
					// strAvailableVariables = null;
				}

				break;

			}

			case "VUSCRIPT": {
				if (!isCancelled) {

					// Document convxmlDoc = null;
					String strresponse = null;
					String strScriptName = null, message = null;
					byte[] b = null;
					String strZipSourcePath = null;
					long lScriptId = -1l;
					DocumentBuilder mdb = null;
					Document mdoc = null;
					String zipSourcePath = Constants.FG_VUSCRIPTS_TEMP_PATH + File.pathSeparator + "test.rar";

					try {

						lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);
						strScriptName = tokens[1].toString().trim().split("=")[2].trim().split("\r\n")[0];

						if (strScriptName != null) {
							con = DataBaseManager.giveConnection();
							con.setAutoCommit(false);
							int enterpriseId = rm.getEnterpriseId(con, lUserId);
							lScriptId = rm.checkScriptExist(con, lUserId, strScriptName, enterpriseId, true);

							// Extract received script zip
							String strScriptSavePath = lUserId + "_" + lScriptId;

							String strSourcePath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator + strScriptSavePath;
							strZipSourcePath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH + File.separator + lcurrentTimeMillis + "_" + lUserId + ".rar";

							FloodgatesXMLAppend.isVariablesFileExist(Constants.VARIABLEXMLFOLDERPATH + File.separator + lUserId + "_variables.xml");
							// check for folder userid_scriptid
							File file = new File(strSourcePath);
							if (file.exists()) {
								rm.removeDirectory(file);
								// file.delete();
							}
							// if folder not exist need to re create
							if (!file.exists()) {
								if (file.mkdir()) {
									LogManager.infoLog("Directory is created!");
								} else {
									LogManager.infoLog("Failed to create directory!");
								}
							}

							String strFileDestination = strSourcePath;
							rm.unzip(new File(strZipSourcePath), new File(strFileDestination));
							// File Extract Completed

							// Check vscript.xml is available in the temp folder
							if (new File(strSourcePath + File.separator + "vuscript.xml").exists()) {
								// read file content
								// strReceivedStreamContent = new
								// String(Files.readAllBytes(Paths.get(strSourcePath+File.separator+"VUScript.xml")));
								try {
									DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
									mdbf.setValidating(false);
									mdb = mdbf.newDocumentBuilder();
									// mdoc = mdb.parse(new FileInputStream(new File(strSourcePath + File.separator + "vuscript.xml")));
									
									String strXml = FileUtils.readFileToString(new File(strSourcePath + File.separator + "vuscript.xml"), "UTF-8");
								    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
								    mdoc = mdb.parse(byteArrayInputStream);
								} catch (Exception e) {
									throw new Exception("Unable to upload script. Please verify the script for irrelevant requests.");
								}

							} else {
								// stop

							}

							// delete VUScript.xml file
							File fVUScript = new File(strSourcePath + File.separator + "vuscript.xml");
							if (fVUScript.exists()) {
								fVUScript.delete();
							}
						}

						XPath xPathSettings = XPathFactory.newInstance().newXPath();
						String expressionSettings = "/vuscript";
						Node nodeScript = (Node) xPathSettings.compile(expressionSettings.trim()).evaluate(mdoc, XPathConstants.NODE);
						Element e = (Element) nodeScript;

						e.setAttribute("id", String.valueOf(lScriptId));
						rm.appendXml(mdoc, lUserId);

						message = "Uploaded Successfully";
						strresponse = "OK: "+message.getBytes().length+" \r\n\r\n"+message;
						b = strresponse.getBytes();
						os.write(b, 0, b.length);
						// delete received file
						File fRar = new File(strZipSourcePath);
						if (fRar.exists()) {

							boolean bDelStatus = fRar.delete();
						}
						DataBaseManager.commit(con);

					} catch (Exception e) {
						LogManager.errorLog("Uploaded Script: "+e);
						e.printStackTrace();
						message = e.getMessage();
						strresponse = "OK: "+message.getBytes().length+" \r\n\r\n"+message;
						b = strresponse.getBytes();
						os.write(b, 0, b.length);
					} finally {
						// convxmlDoc = null;
						// strresponse = null;
						// b = null;
						// strReceivedStreamContent = null;
						// mdb = null;
						// mdoc = null;
						DataBaseManager.close(con);
						con = null;
						UtilsFactory.close(os);
						message = null;
					}
				}
				break;
			}

			case "VARIABLES": {
				String strresponse = null;
				Document convxmlDoc = null;
				byte[] b = null;
				try {
					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);
					convxmlDoc = rm.convertStringToDocument(strReceivedStreamContent);
					boolean funstatus = rm.appendVariable(convxmlDoc, os, bis, lUserId);
					if (funstatus)
						rm.saveXmlDoc(convxmlDoc, Constants.VARIABLEXMLFOLDERPATH + File.separator + lUserId + "_variables.xml");
					strresponse = "OK: 0\r\n\r\n";
					b = strresponse.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// strresponse = null;
					// convxmlDoc = null;
					UtilsFactory.close(bis);
					UtilsFactory.close(os);
				}
				break;
			}

			case "GETVARIABLES": {
				String strresponse = null;
				DocumentBuilder mdb = null;
				Document mdoc = null;
				byte[] b = null;
				try {
					lUserId = Long.parseLong(tokens[1].toString().trim().split("=")[1].trim().split("\r\n")[0]);

					DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
					mdbf.setValidating(false);

					mdb = mdbf.newDocumentBuilder();
					//mdoc = mdb.parse(new FileInputStream(new File(Constants.VARIABLEXMLFOLDERPATH + File.separator + lUserId + "_variables.xml")));
					
					String strXml = FileUtils.readFileToString(new File(Constants.VARIABLEXMLFOLDERPATH + File.separator + lUserId + "_variables.xml"), "UTF-8");
				    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
				    mdoc = mdb.parse(byteArrayInputStream);

					String strVariablesXml = rm.convertDocumentToString(mdoc);
					strresponse = "GETVARIABLES: " + strVariablesXml.length() + "\r\n\r\n" + strVariablesXml;
					b = strresponse.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// strresponse = null;
					UtilsFactory.close(bis);
					UtilsFactory.close(os);

				}
				break;
			}

			case "isqueueavailable": {
				String strRespHeader = null;
				byte[] b = null;
				try {
					if (LTScheduler.ltQueues.isEmpty()) {
						strRespHeader = "queuestatus: 0\r\n" + "status= 0\r\n\r\n";
					} else {
						strRespHeader = "queuestatus: 0\r\n" + "status= 1\r\n\r\n";
					}

					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// strRespHeader = null;
					// b = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "getrundetail": {
				byte[] b = null;
				LoadTestSchedulerBean ltb = null;
				String process = null;
				String scenarioName = null;
				String reportName = null;
				String loadgen = null;
				String distributions = null;
				String txtbody = null;
				Document varDoc = null;
				Document scenarioDoc = null;
				Document mergeDoc = null;
				HashMap<Long, String> hmap = null;
				String scriptwiseDataConnection = null, scriptDataConnection = null;
				try {
					con = DataBaseManager.giveConnection();
					ltb = LTScheduler.pollProcessingQueue();
					// rm.updatePrivateCounters(con, "lt_controller_queue", LTScheduler.ltQueues.size());
					lRunId = ltb.getRunId();
					varDoc = rm.getVariables(ltb.getUserId());
					scenarioDoc = rm.getScenarios(ltb.getUserId(), ltb.getScenarioId());
					mergeDoc = rm.mergeDoc(varDoc, scenarioDoc);
					txtbody = rm.convertDocumentToString(mergeDoc);
					loadgen = "loadgens= " + ltb.getLoadgenIpAddress();
					reportName = "runid= " + lRunId;
					scenarioName = "scenarioname= " + ltb.getScenarioName();
					distributions = "distribution= " + ltb.getDistributions();
					scriptwiseDataConnection = "scriptwisedataconnection= " + Constants.LT_QUEUE_IP + "," + Constants.LT_QUEUE_PORT;
					scriptDataConnection = "scriptdataconnection= " + Constants.LT_QUEUE_IP + "," + Constants.LT_QUEUE_PORT;
					LogManager.infoLog("scriptwiseDataConnection:: " + scriptwiseDataConnection);
					LogManager.infoLog("scriptDataConnection:: " + scriptDataConnection);
					rm.updateReportMasterLoadGen(con, lRunId, loadgen);
					// Store Script Id and Name to DB
					hmap = rm.getScriptId(ltb.getUserId(), ltb.getScenarioId());
					rm.insertRunScriptDetails(con, lRunId, hmap);
					process = "RUN: " + txtbody.length() + "\r\n" + reportName + "\r\n" + scenarioName + "\r\n" + loadgen + "\r\n" + distributions + "\r\n" + scriptwiseDataConnection + "\r\n" + scriptDataConnection + "\r\n\r\n" + txtbody;
					b = process.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// b = null;
					// process = null;
					// scenarioName = null;
					// reportName = null;
					// loadgen = null;
					// txtbody = null;
					// varDoc = null;
					// scenarioDoc = null;
					// mergeDoc = null;
					// ltb = null;
					DataBaseManager.close(con);
					// con = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "run": {

				byte[] b = null;
				String process = null;
				String scenarioName = null;
				String reportName = null;
				String loadgen = null;
				String regions = null;
				String distributions = null;
				String txtbody = null;
				Document varDoc = null;
				Document scenarioDoc = null;
				Document mergeDoc = null;
				LoadTestSchedulerBean ltb = null;
				try {

					// Instantiate a BufferedOutputStream object
					varos = socketCon.getOutputStream();
					// Instantiate an InputStream with the optional character
					// encoding.
					instream = socketCon.getInputStream();
					ltb = LTScheduler.pollFGTest();
					lRunId = ltb.getRunId();
					varDoc = rm.getVariables(ltb.getUserId());
					scenarioDoc = rm.getScenarios(ltb.getUserId(), ltb.getScenarioId());
					mergeDoc = rm.mergeDoc(varDoc, scenarioDoc);
					txtbody = rm.convertDocumentToString(mergeDoc);
					loadgen = "loadgens= " + ltb.getLoadgenIpAddress();
					reportName = "runid= " + lRunId;
					scenarioName = "scenarioname= " + ltb.getScenarioName();
					regions = "regions= " + ltb.getLoadGenRegions();
					distributions = "distribution= " + ltb.getDistributions();
					process = "RUN: " + txtbody.length() + "\r\n" + reportName + "\r\n" + scenarioName + "\r\n" + loadgen + "\r\n" + regions + "\r\n" + distributions + "\r\n\r\n" + txtbody;
					b = process.getBytes();
					varos.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// b = null;
					// process = null;
					// scenarioName = null;
					// reportName = null;
					// loadgen = null;
					// txtbody = null;
					// varDoc = null;
					// scenarioDoc = null;
					// mergeDoc = null;
					// ltb = null;
					// if(instream!=null)
					// instream = null;
					// if(varos!=null)
					// varos = null;
					UtilsFactory.close(instream);
					UtilsFactory.close(varos);
				}
				break;
			}

			case "runlicense": {

				byte[] b = null;
				String process = null;
				String scenarioName = null;
				String reportName = null;
				String loadgen = null;
				String regions = null;
				String distributions = null;
				String txtbody = null;
				Document varDoc = null;
				Document scenarioDoc = null;
				Document mergeDoc = null;
				LoadTestSchedulerBean ltb = null;
				try {

					// Instantiate a BufferedOutputStream object
					varos = socketCon.getOutputStream();
					// Instantiate an InputStream with the optional character
					// encoding.
					instream = socketCon.getInputStream();
					ltb = LTScheduler.pollFGPaidTest();
					lRunId = ltb.getRunId();
					varDoc = rm.getVariables(ltb.getUserId());
					scenarioDoc = rm.getScenarios(ltb.getUserId(), ltb.getScenarioId());
					mergeDoc = rm.mergeDoc(varDoc, scenarioDoc);
					txtbody = rm.convertDocumentToString(mergeDoc);
					loadgen = "loadgens= " + ltb.getLoadgenIpAddress();
					reportName = "runid= " + lRunId;
					scenarioName = "scenarioname= " + ltb.getScenarioName();
					regions = "regions= " + ltb.getLoadGenRegions();
					distributions = "distribution= " + ltb.getDistributions();
					process = "RUN: " + txtbody.length() + "\r\n" + reportName + "\r\n" + scenarioName + "\r\n" + loadgen + "\r\n" + regions + "\r\n" + distributions + "\r\n\r\n" + txtbody;
					b = process.getBytes();
					varos.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// b = null;
					// process = null;
					// scenarioName = null;
					// reportName = null;
					// loadgen = null;
					// txtbody = null;
					// varDoc = null;
					// scenarioDoc = null;
					// mergeDoc = null;
					// ltb = null;
					// if(instream!=null)
					// instream = null;
					// if(varos!=null)
					// varos = null;
					UtilsFactory.close(instream);
					UtilsFactory.close(varos);
				}
				break;
			}

			case "runAppedo": {
				String strTokens[] = null;
				String strScenarioName = null;
				String strScenarioId = null;
				String strReportname = null;
				String strLoadgenIpAddress = null;
				Document varDoc = null;
				Document scenarioDoc = null;
				Document mergeDoc = null;
				String txtbody = null;
				String loadgen = null;
				String reportName = null;
				String scenarioName = null;
				String process = null;
				String strLoadgenRegions = null;
				String strLoadgenDistributions = null;
				String regions = null;
				String distributions = null;
				byte[] b = null;
				try {

					strTokens = header.split("\r\n");
					strScenarioName = strTokens[1].split("=")[1];
					strScenarioId = strTokens[2].split("=")[1];
					strReportname = strTokens[3].split("=")[1];
					lUserId = Long.parseLong(strTokens[4].split("=")[1].trim());
					lRunId = Long.parseLong(strTokens[5].split("=")[1].trim());
					strLoadgenIpAddress = strTokens[6].split("=")[1].trim();
					strLoadgenRegions = strTokens[7].split("=")[1].trim();
					strLoadgenDistributions = strTokens[8].split("=")[1].trim();
					// Obtain an address object of the server
					address = InetAddress.getByName(Constants.FG_CONTROLLER_IP);
					// Establish a socket connection
					// socketCon = new Socket(address,
					// Integer.parseInt(Constants.FG_CONTROLLER_PORT));
					// Instantiate a BufferedOutputStream object
					varos = socketCon.getOutputStream();
					// Instantiate an InputStream with the optional character
					// encoding.
					instream = socketCon.getInputStream();
					varDoc = rm.getVariables(lUserId);
					scenarioDoc = rm.getScenarios(lUserId, strScenarioId);
					mergeDoc = rm.mergeDoc(varDoc, scenarioDoc);
					txtbody = rm.convertDocumentToString(mergeDoc);
					loadgen = "loadgens= " + strLoadgenIpAddress;
					// loadgen = "loadgens= 127.0.0.1";
					regions = "regions= " + strLoadgenRegions;
					distributions = "distribution= " + strLoadgenDistributions;
					reportName = "runid= " + lRunId;
					scenarioName = "scenarioname= " + strScenarioName;
					process = "RUN: " + txtbody.length() + "\r\n" + reportName + "\r\n" + scenarioName + "\r\n" + loadgen + "\r\n" + regions + "\r\n" + distributions + "\r\n\r\n" + txtbody;
					b = process.getBytes();
					varos.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					// strTokens = null;
					// strScenarioName = null;
					// strScenarioId = null;
					// strReportname = null;
					// strLoadgenIpAddress = null;
					// varDoc = null;
					// scenarioDoc = null;
					// mergeDoc = null;
					// txtbody = null;
					// loadgen = null;
					// reportName = null;
					// scenarioName = null;
					// process = null;
					// b = null;
					UtilsFactory.close(instream);
					UtilsFactory.close(varos);
				}
				break;
			}

			case "result": {
				String strRespHeader = null;
				byte[] b = null;
				String strTokens[] = null;
				File file = null;
				try {

					strTokens = header.split("\r\n");
					lRunId = Long.parseLong(strTokens[1].split("=")[1].trim());
					LogManager.infoLog("Result run id : " + lRunId);
					file = new File(Constants.CSVFILE + lRunId);
					if (!file.exists()) {
						if (file.mkdir()) {
							LogManager.infoLog("Directory is created!");
						} else {
							LogManager.infoLog("Failed to create directory!");
						}
					}

					rm.writeToXmlFile(strReceivedStreamContent, Constants.CSVFILE + lRunId + File.separator + lRunId + ".csv");
					con = DataBaseManager.giveConnection();
					rm.readChartSummary(con, lRunId, Constants.CSVFILE + lRunId + File.separator + lRunId + ".csv");

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRespHeader = null;
					// strTokens = null;
					// b = null;
					// file = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "summaryreport": {
				String strRespHeader = null;
				byte[] b = null;
				String strTokens[] = null;
				try {

					strTokens = header.split("\r\n");
					lRunId = Long.parseLong(strTokens[1].split("=")[1].trim());
					LogManager.infoLog("summaryreport run id : " + lRunId);
					rm.writeToXmlFile(strReceivedStreamContent, Constants.SUMMARYREPORTPATH + lRunId + ".xml");
					// LogManager.infoLog("Created xml file is:"+lRunId+".xml");
					con = DataBaseManager.giveConnection();
					// rm.updateReportMaster(con, lRunId);
					rm.updateInActiveLoadAgent(con, lRunId);

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRespHeader = null;
					// b = null;
					// strTokens = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "scriptwisestatus": {

				String strCreatedUser = "";
				String strCompletedUser = "";
				String str200Count = "", str300Count = "", str400Count = "", str500Count = "", strIsCompleted = "";
				String strScriptId = "";
				String strScriptName = "";
				String strErrorCount = "";
				String strRespHeader = "";
				String strRunId = "";
				String[] splitLines = null;
				String[] splitLine = null;
				byte[] b = null;
				// long lRunId = 0L;
				HashMap<String, Object> hmStatuswiseDetails = null;
				String[] strTokens = null;
				int is_completed = 0;

				try {
					con = DataBaseManager.giveConnection();
					strTokens = tokens[1].split("\r\n");
					strRunId = strTokens[1].toString().split("= ")[1].trim();
					is_completed = Integer.valueOf(strTokens[2].toString().split("= ")[1]);

					lRunId = Long.parseLong(strRunId);
					splitLines = strReceivedStreamContent.split("\r\n");
					for (String str : splitLines) {
						if (str.contains(",")) {
							hmStatuswiseDetails = new HashMap<String, Object>();
							hmStatuswiseDetails.put("runid", strRunId);
							splitLine = str.split(",");
							strScriptId = splitLine[0];
							hmStatuswiseDetails.put("script_id", strScriptId);
							strCreatedUser = splitLine[1];
							hmStatuswiseDetails.put("created_users", strCreatedUser);
							strCompletedUser = splitLine[2];
							hmStatuswiseDetails.put("completed_users", strCompletedUser);
							str200Count = splitLine[3];
							hmStatuswiseDetails.put("http_200_count", str200Count);
							str300Count = splitLine[4];
							hmStatuswiseDetails.put("http_300_count", str300Count);
							str400Count = splitLine[5];
							hmStatuswiseDetails.put("http_400_count", str400Count);
							str500Count = splitLine[6];
							hmStatuswiseDetails.put("http_500_count", str500Count);
							strIsCompleted = splitLine[7];
							hmStatuswiseDetails.put("is_completed", strIsCompleted);
							strErrorCount = splitLine[8];
							hmStatuswiseDetails.put("error_count", strErrorCount);
							strScriptName = splitLine[9];
							hmStatuswiseDetails.put("script_name", strScriptName);
							if (rm.isRecordAvailable(con, strRunId, strScriptId)) {
								rm.updateScriptwiseStatus(con, hmStatuswiseDetails);
							} else {
								rm.insertScriptwiseStatus(con, hmStatuswiseDetails);
							}
						}
					}

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);

					if (is_completed == 1) {
						// rm.updateReportMaster(con, lRunId);
						rm.updateInActiveLoadAgent(con, lRunId);
						rm.createSummaryTable(con, lRunId);
						rm.ltSummaryReport(con, lRunId);
					}

					// LogManager.infoLog("Received Script Wise Status: \n"+strReceivedStreamContent+"\n Run_Id : "+lRunId);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRespHeader = null;
					// strRunId = null;
					// b = null;
					// splitLines = null;
					// splitLine = null;
					// strCreatedUser = null;
					// strCompletedUser = null;
					// str200Count = null;
					// str300Count = null;
					// str400Count = null;
					// str500Count = null;
					// strIsCompleted = null;
					// strScriptId = null;
					// strScriptName = null;
					// strErrorCount = null;
					UtilsFactory.clearCollectionHieracy(hmStatuswiseDetails);
					UtilsFactory.close(os);
				}
				break;
			}

			case "status": {
				String strRespHeader = "";
				byte[] b = null;
				String[] strTokens = null;
				try {
					strTokens = tokens[1].split("\r\n");
					lRunId = Long.parseLong(strTokens[1].toString().split("= ")[1].trim());

					LogManager.infoLog("Data Size: " + streamLength);
					LogManager.infoLog("Actual Data:: " + strReceivedStreamContent.trim());
					LTScheduler.queueReportData(UtilsFactory.makeValidVarcharNewLine(strReceivedStreamContent.trim()));
					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
					con = DataBaseManager.giveConnection();
					// rm.updatePrivateCounters(con, "lt_exec_services_queue", LTScheduler.reportDataQueues.size());
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					strRespHeader = null;
					b = null;
					DataBaseManager.close(con);
					con = null;
					UtilsFactory.close(os);
				}
				break;
			}
			case "updaterunidstatus": {
				String strRunId = "";
				try {
					strRunId = tokens[1].toString().split("=")[1].trim().split("\r\n")[0];
					LogManager.infoLog("run id " + strRunId);
					con = DataBaseManager.giveConnection();
					rm.updateActiveStatus(con, Integer.parseInt(strRunId));

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRunId = null;
				}

				break;
			}

			/*
			 * case "getinstancedetails" : { String licenseType, module; String licenseDetails;
			 * byte[] b = null; String details[]; try { details =
			 * tokens[1].toString().split("\r\n"); licenseType =
			 * details[1].toString().split("=")[1].trim(); module =
			 * details[2].toString().split("=")[1].trim(); LogManager.infoLog("license type "+
			 * licenseType); con = DataBaseManager.giveConnection(); licenseDetails =
			 * rm.getInstanceDetails(con, Integer.parseInt(licenseType), module); b =
			 * licenseDetails.getBytes(); os.write(b, 0, b.length); }catch(Exception e) {
			 * LogManager.errorLog(e); }finally { DataBaseManager.close(con); con = null;
			 * licenseType = null; module = null; licenseDetails = null; details = null; b = null; }
			 * 
			 * break; }
			 */

			case "insertloadagentdetails": {
				String strRespHeader;
				byte[] b = null;
				String details[];
				HashMap<String, String> hmDetails = new HashMap<String, String>();
				try {
					details = tokens[1].toString().split("\r\n");
					for (int i = 0; i < details.length; i++) {
						String value[] = details[i].split("=");
						hmDetails.put(value[0], value[1]);
						value = null;
					}

					con = DataBaseManager.giveConnection();
					rm.insertLoadAgentDetails(con, hmDetails);

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// details = null;
					// b = null;
					// strRespHeader = null;
					UtilsFactory.close(os);
				}

				break;
			}

			case "updateinstancedetails": {
				String instanceId;
				String responseHeader;
				byte[] b = null;
				String details[];
				try {
					details = tokens[1].toString().split("\r\n");
					instanceId = details[1].toString().split("=")[1].trim();
					LogManager.infoLog("Instance Id " + instanceId);
					con = DataBaseManager.giveConnection();
					rm.updateInstanceDetails(con, instanceId);
					responseHeader = "ok: 0\r\n\r\n";
					b = responseHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// instanceId = null;
					// responseHeader = null;
					// details = null;
					// b = null;
					UtilsFactory.close(os);
				}

				break;
			}

			case "log": {
				String strRespHeader = null;
				byte[] b = null;
				String strTokens[] = null;
				try {

					strTokens = header.split("\r\n");
					lRunId = Long.parseLong(strTokens[1].split("=")[1].trim());
					LogManager.infoLog("Result run id : " + lRunId);

					con = DataBaseManager.giveConnection();
					rm.updateRemarksInReportMaster(con, lRunId, strReceivedStreamContent, "COMPLETED");

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRespHeader = null;
					// strTokens = null;
					// b = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "failedrunidstatus": {
				String strRespHeader = null;
				byte[] b = null;
				String strTokens[] = null;
				try {

					strTokens = header.split("\r\n");
					lRunId = Long.parseLong(strTokens[1].split("=")[1].trim());
					LogManager.infoLog("Result run id : " + lRunId);

					con = DataBaseManager.giveConnection();
					rm.updateRemarksInReportMaster(con, lRunId, strReceivedStreamContent, "FAILED");
					rm.updateInActiveLoadAgent(con, lRunId);

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);

				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					DataBaseManager.close(con);
					// con = null;
					// strRespHeader = null;
					// strTokens = null;
					// b = null;
					UtilsFactory.close(os);
				}
				break;
			}

			case "loadgenfailed": {
				String ipAddress;
				String strTokens[] = null;
				try {
					strTokens = tokens[1].split("\r\n");
					lRunId = Long.parseLong(strTokens[1].toString().split("= ")[1].trim());
					ipAddress = strTokens[2].toString().split("= ")[1];
					System.out.println("IPADDRESS:: " + ipAddress);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					ipAddress = null;
					DataBaseManager.close(con);
					con = null;
				}
				break;
			}
			
			case "registermachine": {
				String machineId;
				String strTokens[] = null;
				long userId;
				String strRespHeader = null;
				byte[] b = null;
				try {
					strTokens = tokens[1].split("\r\n");
					userId = Long.parseLong(strTokens[1].toString().split("= ")[1].trim());
					machineId = strTokens[2].toString().split("= ")[1];
					
					con = DataBaseManager.giveConnection();
					
					rm.registerMachineId(con, userId, machineId);

					strRespHeader = "ok: 0\r\n" + "success= 1\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					machineId = null;
					DataBaseManager.close(con);
					con = null;
					UtilsFactory.close(os);
				}
				break;
			}
			
			case "deregistermachine": {
				String machineId;
				String strTokens[] = null;
				long userId;
				String strRespHeader = null;
				byte[] b = null;
				try {
					strTokens = tokens[1].split("\r\n");
					userId = Long.parseLong(strTokens[1].toString().split("= ")[1].trim());
					machineId = strTokens[2].toString().split("= ")[1];
					
					con = DataBaseManager.giveConnection();
					
					rm.deRegisterMachineId(con, userId, machineId);

					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					os.write(b, 0, b.length);
				} catch (Exception e) {
					LogManager.errorLog(e);
				} finally {
					machineId = null;
					DataBaseManager.close(con);
					con = null;
					UtilsFactory.close(os);
				}
				break;
			}

			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			socketCon.close();
			// operation = null;
			// address = null;
			UtilsFactory.close(varos);
			// varos = null;
			UtilsFactory.close(instream);
			// instream = null;
			// strReceivedStreamContent = null;
			// userDBI = null;

		}
	}
}
