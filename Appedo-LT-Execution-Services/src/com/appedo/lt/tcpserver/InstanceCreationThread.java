package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONObject;

import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class InstanceCreationThread extends Thread{
	
	JSONObject instance_config, region_details;
	String instanceId, instanceType; 
	LoadTestSchedulerBean loadTestSchedulerBean;
	Connection con;
	RunManager runManager = null;
	AmazonConfiguration amazonConfiguration = null;

	public InstanceCreationThread(JSONObject instance_config1, JSONObject region_details1, String instanceId1, LoadTestSchedulerBean loadTestSchedulerBean1, Connection con1, String instanceType1){
		this.instanceId = instanceId1;
		this.instanceType = instanceType1;
		this.region_details = region_details1;
		this.instance_config = instance_config1;
		this.loadTestSchedulerBean = loadTestSchedulerBean1;
		this.con = con1;
	}
	
	@Override
	public void run() {
		try {
			runManager = new RunManager();
			amazonConfiguration = new AmazonConfiguration();
			String strControllerStatus = "inactive";
			long startTime = System.currentTimeMillis();
			long endTime = startTime + (600*1000);
			String ipAddress = "";
			if( instance_config != null ){
				instanceId = amazonConfiguration.createInstance(region_details.getString("image_id"), region_details.getString("endpoint"), instanceType, instance_config.getString("keypair"), instance_config.getString("sec_group_name"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"));
				// To update in Region Details Table
				runManager.updateInstanceAvailability(con, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), region_details.getString("os_type"));
				if( instanceId.contains("Error:") ){
					if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
						loadTestSchedulerBean = LTScheduler.pollJmeterTest();
					}else {
						loadTestSchedulerBean = LTScheduler.pollFGPaidTest();
						// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
					}
					updateTableValues(instanceId.replace("Error:", ""), loadTestSchedulerBean);
				} else {
					Thread.sleep(1000);
					LogManager.infoLog("Created instance id: "+instanceId);
					while(ipAddress == "" || ipAddress == null){
						ipAddress = amazonConfiguration.getPublicIP(instanceId, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"));	
					}
					
					if( !loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
						while( System.currentTimeMillis() < endTime ){
							strControllerStatus = runManager.getControllerStatus(ipAddress, 8889);
							if( strControllerStatus.equalsIgnoreCase("active") ){
								break;
							}
						}
						if ( strControllerStatus.equalsIgnoreCase("inactive") ){
							loadTestSchedulerBean = LTScheduler.pollFGPaidTest();
							// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
							updateTableValues("LoadGen is not Active", loadTestSchedulerBean);
						}
					} /*else {
						int i =0;
						// boolean isTomcatStarted = false;
						while (i < 5){
							// isTomcatStarted = runManager.isTomcatStarted(ipAddress, 8080);
							// if (isTomcatStarted){
							//	break;
							//}
							// Tomcat not yet started, so wait for 30 seconds.
							Thread.sleep(60000);
							i++;
						}
						if (!isTomcatStarted) {
							loadTestSchedulerBean = LTScheduler.pollJmeterTest();
							updateTableValues("LoadGen is not Active", loadTestSchedulerBean);
						}
						// Thread.sleep(180000);
					}*/
					HashMap<String, String> hm = new HashMap<String, String>();
					hm.put("instance_id", instanceId);
					hm.put("region", region_details.getString("region"));
					hm.put("endpoint", region_details.getString("endpoint"));
					hm.put("location", "");
					hm.put("instance_status", "inactive");
					hm.put("public_ip", ipAddress);
					hm.put("start_time", String.valueOf(new Date().getTime()));
					if( loadTestSchedulerBean.getTestType().equalsIgnoreCase("JMETER") ){
						hm.put("os_type", "FEDORA" );
					}else{
						hm.put("os_type", "WINDOWS" );
					}
					runManager.insertLoadAgentDetails( con, hm );
					UtilsFactory.clearCollectionHieracy( hm );
					
//					ipAddress = null;
//					instanceId = null;
//					strControllerStatus = null;
//					instance_config = null;
//					instanceType = null;
//					loadTestSchedulerBean = null;
				}
				
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			runManager = null;
//			amazonConfiguration = null;
		}
	}
	
	private void updateTableValues(String status, LoadTestSchedulerBean loadTestSchedulerBean) throws Throwable{
		try {
			runManager.updateNonAvailabilityRegion(con, status, loadTestSchedulerBean);
			runManager.updateInActiveLoadAgent(con, loadTestSchedulerBean.getRunId());
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		
	}
	
}
