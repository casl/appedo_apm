package com.appedo.lt.tcpserver;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.LTScheduler;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LTPaidQueueTimerTask extends TimerTask{
	
	Connection con = null;
	AmazonConfiguration amazonConfiguration = null;
	LoadTestSchedulerBean loadTestSchedulerBean = null;
	RunManager runManager = null;
	String instanceId;
	HashMap<JSONObject, Integer> hmInstance = null;
	String[] regions = null, distributions = null;
	long tNo;
	
	public LTPaidQueueTimerTask(){
		this.con = DataBaseManager.giveConnection();
		tNo = Constants.getLongCount();
		LogManager.infoLog("Opened Connection count in LTPaidQueueTimerTask: "+tNo);
	}

	@Override
	public void run() {
		try {
			if( LTScheduler.htFGTestPaidQueues.size() > 0 ){
				runManager = new RunManager();
				hmInstance = new HashMap<JSONObject, Integer>();
				amazonConfiguration = new AmazonConfiguration();
				loadTestSchedulerBean = LTScheduler.peekFGPaidTest();
				regions = loadTestSchedulerBean.getLoadGenRegions().split(",");
				distributions = loadTestSchedulerBean.getDistributions().split(",");
				JSONObject instance_config = runManager.getInstanceDetails(con, "LT");
				
				// To get maxUser for Particular Instance
				int userCount = runManager.getMaxUserCount(con, loadTestSchedulerBean);
				int totalUser = loadTestSchedulerBean.getMaxUserCount();
				
				for( int i=0; i<regions.length; i++ ){
					double percentageValue = Double.valueOf(distributions[i])/100;
					int distributionCount = (int) (totalUser * percentageValue);
							
					int neededInstance = 0;
					if( distributionCount % userCount == 0 ){
						neededInstance = distributionCount/userCount;
					}else{
						neededInstance = (distributionCount/userCount) + 1;
					}
					
					JSONArray inactiveInstance = runManager.getInactiveLoadAgentDetails(con, regions[i], "WINDOWS", Constants.FG_CONTROLLER_IP);
					JSONObject region_details = runManager.getRegionDetails(con, regions[i], "WINDOWS");	
					int availability = runManager.updateInstanceAvailability(con, region_details.getString("endpoint"), instance_config.getString("access_key"), instance_config.getString("secret_access_key"), region_details.getString("os_type") );
					
					if( region_details.getInt("reservation") - availability >= (neededInstance - inactiveInstance.size()) ){
						if( neededInstance < inactiveInstance.size() ){
							hmInstance.put( region_details, 0 );
						}else{
							hmInstance.put( region_details, (neededInstance - inactiveInstance.size()) );
						}
						region_details = null;
						inactiveInstance = null;
					}else{
						loadTestSchedulerBean = LTScheduler.pollFGPaidTest();
						// runManager.updatePrivateCounters(con, "lt_paid_user_queue", LTScheduler.htFGTestPaidQueues.size());
						runManager.updateNonAvailabilityRegion(con, "No Region Available", loadTestSchedulerBean);
						hmInstance.clear();
//						hmInstance = null;
//						loadTestSchedulerBean = null;
						break;
					}
				}
				
				if( hmInstance!= null ){
					String instanceType = null;
					if( loadTestSchedulerBean != null ){
						instanceType = runManager.ltLicenseInstanceDetails(con, loadTestSchedulerBean.getLicense());	
					}

					// List of Threads for stored thread variable
					List<Thread> threads = new ArrayList<Thread>();
					for( Map.Entry<JSONObject, Integer> e: hmInstance.entrySet() ){
						
						int i = 0;
						while(i != e.getValue()){
							Thread th = new InstanceCreationThread(instance_config, e.getKey(), instanceId, loadTestSchedulerBean, con, instanceType);
							th.start();
							threads.add(th);
							i++;
						}
					}
					
					// To check all Threads status before going to finally block
					while(true){
						 int count = 0;
						 for ( Thread th : threads){
							 if(!th.isAlive()){
								 count ++;
							 }
						 }
						 if(count == threads.size()){
							 break;
						 }
						 else{
							Thread.sleep(1000);
						 }
					 }
				}
			}
			
		} catch (Throwable e) {
			LogManager.errorLog(e);
		} finally {
//			runManager = null;
//			amazonConfiguration = null;
//			loadTestSchedulerBean = null;
//			instanceId = null;
//			regions = null;
//			distributions = null;
			UtilsFactory.clearCollectionHieracy( hmInstance );
//			DataBaseManager.close(con);
//			con = null;
//			LogManager.infoLog("Closed Connection count in LTPaidQueueTimerTask: "+tNo);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
//		con = null;
		super.finalize();
	}
}
