package com.appedo.lt.bean;

import java.sql.Timestamp;

/**
 * Userbean is for replica of usermaster table
 * @author navin
 *
 */
public class LTLogBean {

	private long logId;
	private long iterationId;
	private String loadGen;
	private String logName;
	private String message;
	private String scenarioName;
	private long scriptId;
	private String scriptName;
	private Timestamp logTime;
	private long userId;
	
	public long getLogId() {
		return logId;
	}
	public void setLogId(long logId) {
		this.logId = logId;
	}
	public long getIterationId() {
		return iterationId;
	}
	public void setIterationId(long iterationId) {
		this.iterationId = iterationId;
	}
	public String getLoadGen() {
		return loadGen;
	}
	public void setLoadGen(String loadGen) {
		this.loadGen = loadGen;
	}
	public String getLogName() {
		return logName;
	}
	public void setLogName(String logName) {
		this.logName = logName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getScenarioName() {
		return scenarioName;
	}
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}
	public long getScriptId() {
		return scriptId;
	}
	public void setScriptId(long scriptId) {
		this.scriptId = scriptId;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Timestamp getLogTime() {
		return logTime;
	}
	public void setLogTime(Timestamp logTime) {
		this.logTime = logTime;
	}
}
