package com.appedo.lt.bean;

import java.sql.Timestamp;

/**
 * Userbean is for replica of usermaster table
 * @author navin
 *
 */
public class LTErrorBean {

	private long iterationId;
	private int errorCode;
	private String source;
	private String loadGen;
	private String message;
	private String request;
	private String requestExceptionId;
	private long requestId;
	private String scenarioName;
	private String scriptName;
	private Timestamp errorTime;
	private long userId;
	
	public long getIterationId() {
		return iterationId;
	}
	public void setIterationId(long iterationId) {
		this.iterationId = iterationId;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getLoadGen() {
		return loadGen;
	}
	public void setLoadGen(String loadGen) {
		this.loadGen = loadGen;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getRequestExceptionId() {
		return requestExceptionId;
	}
	public void setRequestExceptionId(String requestExceptionId) {
		this.requestExceptionId = requestExceptionId;
	}
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public String getScenarioName() {
		return scenarioName;
	}
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public Timestamp getErrorTime() {
		return errorTime;
	}
	public void setErrorTime(Timestamp errorTime) {
		this.errorTime = errorTime;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
}
