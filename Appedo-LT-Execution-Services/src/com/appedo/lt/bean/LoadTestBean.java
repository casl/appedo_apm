package com.appedo.lt.bean;

/**
 * Bean replica for the scenarios.xml file.
 * This keeps the scenario related details like Script name, max users to be generated, ramp-up time, etc...
 * 
 * @author Anandraj
 *
 */
public class LoadTestBean {
	
	// for duration/iteration
	private String strScriptName = "";
	private String strScenarioName = "";
	private String strMaxUsers = "";
	private String strLoops = "";
	private String strNoOfThreads = "";
	private String strRAMPTime = "";
	private String strStartTime = "";
	private String strEndTime = "";
	private String strSchedular = "";
	private String strSampleError = "";
	private String strDurationTime = "";
	private String strDelay = "";
	private String strDelayStart = "";
	
	private boolean bDuration = false;
	private boolean bIteration = false;
	
	public boolean isDuration() {
		return bDuration;
	}
	public void setDuration(boolean duration) {
		bDuration = duration;
	}
	
	public boolean isIteration() {
		return bIteration;
	}
	public void setIteration(boolean iteration) {
		bIteration = iteration;
	}
	
	public String getDelay() {
		return strDelay;
	}
	public void setDelay(String strDelay) {
		this.strDelay = strDelay;
	}
	
	public String getDelayStart() {
		return strDelayStart;
	}
	public void setDelayStart(String strDelayStart) {
		this.strDelayStart = strDelayStart;
	}
	
	public String getDurationTime() {
		return strDurationTime;
	}
	public void setDurationTime(String strDurationTime) {
		this.strDurationTime = strDurationTime;
	}
	
	public String getEndTime() {
		return strEndTime;
	}
	public void setEndTime(String strEndTime) {
		this.strEndTime = strEndTime;
	}
	
	public String getLoops() {
		return strLoops;
	}
	public void setLoops(String strLoops) {
		this.strLoops = strLoops;
	}
	
	public String getNoOfThreads() {
		return strNoOfThreads;
	}
	public void setNoOfThreads(String strNoOfThreads) {
		this.strNoOfThreads = strNoOfThreads;
	}
	
	public String getRAMPTime() {
		return strRAMPTime;
	}
	public void setRAMPTime(String strRAMPTime) {
		this.strRAMPTime = strRAMPTime;
	}
	
	public String getSampleError() {
		return strSampleError;
	}
	public void setSampleError(String strSampleError) {
		this.strSampleError = strSampleError;
	}
	
	public String getSchedular() {
		return strSchedular;
	}
	public void setSchedular(String strSchedular) {
		this.strSchedular = strSchedular;
	}
	
	public String getScriptName() {
		return strScriptName;
	}
	public void setScriptName(String strScriptName) {
		this.strScriptName = strScriptName;
	}
	
	public String getStartTime() {
		return strStartTime;
	}
	public void setStartTime(String strStartTime) {
		this.strStartTime = strStartTime;
	}
	
	public String getScenarioName() {
		return strScenarioName;
	}
	public void setScenarioName(String strScenarioName) {
		this.strScenarioName = strScenarioName;
	}
	
	public String getMaxUsers() {
		return strMaxUsers;
	}
	public void setStrMaxUsers(String strMaxUsers) {
		this.strMaxUsers = strMaxUsers;
	}
	
}
