package com.appedo.lt.bean;

import java.util.Date;

import net.sf.json.JSONObject;



public class LoadTestSchedulerBean implements Comparable<LoadTestSchedulerBean> {
	
	private String strTestType = ""; 
	private String strLoadgenIpAddress = "";
	private String strScenarioId = "";
	private String strScenarioName = "";
	private String strReportName = "";	
	private String strLoadGenRegions;
	private String strDistributions;
	private String strLicense;
	private Date dateQueuedOn = null;
	private int maxUserCount;
	private String strScriptName;
	private String lScriptId;
	private long lUserId;
	private long lRunId;
	private String userAgent;
	private int startMonitor;
	private int endMonitor;
	
	public String getScriptName() {
		return strScriptName;
	}
	public void setScriptName(String strScriptName) {
		this.strScriptName = strScriptName;
	}
	public String getScriptId() {
		return lScriptId;
	}
	public void setScriptId(String lScriptId) {
		this.lScriptId = lScriptId;
	}
	public String getLicense() {
		return strLicense;
	}
	public void setLicense(String strLicense) {
		this.strLicense = strLicense;
	}
	public String getDistributions() {
		return strDistributions;
	}
	public void setDistributions(String strDistributions) {
		this.strDistributions = strDistributions;
	}
	public String getLoadGenRegions() {
		return strLoadGenRegions;
	}
	public void setLoadGenRegions(String strLoadGenRegions) {
		this.strLoadGenRegions = strLoadGenRegions;
	}
	public long getRunId() {
		return lRunId;
	}
	public void setRunId(long lrunID) {
		this.lRunId = lrunID;
	}
	
	public long getUserId() {
		return lUserId;
	}
	public void setUserId(long lUserID) {
		this.lUserId = lUserID;
	}
	
	public String getTestType() {
		return strTestType;
	}
	public void setTestType(String strTestType) {
		this.strTestType = strTestType;
	}
	public String getLoadgenIpAddress() {
		return strLoadgenIpAddress;
	}
	public void setLoadgenIpAddress(String strLoadgenIpAddress) {
		this.strLoadgenIpAddress = strLoadgenIpAddress;
	}
	public String getScenarioId() {
		return strScenarioId;
	}
	public void setScenarioId(String strScenarioId) {
		this.strScenarioId = strScenarioId;
	}
	public String getScenarioName() {
		return strScenarioName;
	}
	public void setScenarioName(String strScenarioName) {
		this.strScenarioName = strScenarioName;
	}
	public String getReportName() {
		return strReportName;
	}
	public void setReportName(String strReportName) {
		this.strReportName = strReportName;
	}
	
	public Date getQueuedOn() {
		return dateQueuedOn;
	}
	public void setQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	

	public JSONObject toJSON() {
		
		JSONObject joBean = new JSONObject();
		
		joBean.put("TestType", strTestType);
		joBean.put("LoadgenIpAddress", strLoadgenIpAddress);
		joBean.put("ScenarioId", strScenarioId);
		joBean.put("ScenarioName", strScenarioName);
		joBean.put("ReportName", strReportName);		
		joBean.put("dateQueuedOn", dateQueuedOn);
		joBean.put("scriptId", lScriptId != null && lScriptId.trim().length() > 0 ? lScriptId : "  ");
		joBean.put("scriptName", strScriptName != null && strScriptName.trim().length() > 0 ? strScriptName : "  ");
		joBean.put("userId", lUserId);
		joBean.put("lRunId", lRunId);
		
		return joBean;
	}
	
	public void fromJSONObject(JSONObject joTestBean) {
		lUserId = joTestBean.getLong("userId");
		strTestType = joTestBean.getString("TestType");
		strLoadgenIpAddress = joTestBean.getString("LoadgenIpAddress");
		strScenarioId = joTestBean.getString("ScenarioId");
		strScenarioName = joTestBean.getString("ScenarioName");
		strReportName = joTestBean.getString("ReportName");
		strScriptName = joTestBean.containsKey("scriptName") ? joTestBean.getString("scriptName") : "  ";
		lScriptId = joTestBean.containsKey("scriptId") ? joTestBean.getString("scriptId"): "  ";
		lRunId = joTestBean.getLong("lRunId");
	}
	
	@Override
	public int compareTo(LoadTestSchedulerBean o) {
		// TODO Auto-generated method stub
		return ((int) (dateQueuedOn.getTime() - o.getQueuedOn().getTime()));
		
	}
	public int getMaxUserCount() {
		return maxUserCount;
	}
	public void setMaxUserCount(int maxUserCount) {
		this.maxUserCount = maxUserCount;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public int getStartMonitor() {
		return startMonitor;
	}
	public void setStartMonitor(int startMonitor) {
		this.startMonitor = startMonitor;
	}
	public int getEndMonitor() {
		return endMonitor;
	}
	public void setEndMonitor(int endMonitor) {
		this.endMonitor = endMonitor;
	}

}
