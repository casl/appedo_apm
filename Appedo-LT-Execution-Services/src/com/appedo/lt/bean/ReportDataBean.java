package com.appedo.lt.bean;

import java.sql.Timestamp;

/**
 * Userbean is for replica of usermaster table
 * @author navin
 *
 */
public class ReportDataBean {

	private String loadGen;
	private String sourceIp;
	private String loadgenName;
	private String scenarioName;
	private long scriptId;
	private long containerId;
	private String containerName;
	private long pageId;
	private long requestId;
	private String address;
	private long userId;
	private long iterationId;
	private Timestamp startTime;
	private Timestamp endTime;
	private double diff;
	private int responseCode;
	private int responseSize;
	private long runId;
	
	public String getLoadGen() {
		return loadGen;
	}
	public void setLoadGen(String loadGen) {
		this.loadGen = loadGen;
	}
	public String getSourceIp() {
		return sourceIp;
	}
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}
	public String getLoadgenName() {
		return loadgenName;
	}
	public void setLoadgenName(String loadgenName) {
		this.loadgenName = loadgenName;
	}
	public String getScenarioName() {
		return scenarioName;
	}
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}
	public long getScriptId() {
		return scriptId;
	}
	public void setScriptId(long scriptId) {
		this.scriptId = scriptId;
	}
	public long getContainerId() {
		return containerId;
	}
	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}
	public String getContainerName() {
		return containerName;
	}
	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	public long getPageId() {
		return pageId;
	}
	public void setPageId(long pageId) {
		this.pageId = pageId;
	}
	public long getRequestId() {
		return requestId;
	}
	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getIterationId() {
		return iterationId;
	}
	public void setIterationId(long iterationId) {
		this.iterationId = iterationId;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public int getResponseSize() {
		return responseSize;
	}
	public void setResponseSize(int responseSize) {
		this.responseSize = responseSize;
	}
	public double getDiff() {
		return diff;
	}
	public void setDiff(double diff) {
		this.diff = diff;
	}
	public long getRunId() {
		return runId;
	}
	public void setRunId(long runId) {
		this.runId = runId;
	}
	
}
