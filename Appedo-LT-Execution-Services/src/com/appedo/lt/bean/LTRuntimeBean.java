package com.appedo.lt.bean;

import java.sql.Timestamp;

/**
 * Userbean is for replica of usermaster table
 * @author navin
 *
 */
public class LTRuntimeBean {

	private int runType;
	private int userId;
	private long scriptId;
	private Timestamp runTime;
	private String loadGenName;
	
	public int getRunType() {
		return runType;
	}
	public void setRunType(int runType) {
		this.runType = runType;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Timestamp getRunTime() {
		return runTime;
	}
	public void setRunTime(Timestamp runTime) {
		this.runTime = runTime;
	}
	public long getScriptId() {
		return scriptId;
	}
	public void setScriptId(long scriptId) {
		this.scriptId = scriptId;
	}
	public String getLoadGenName() {
		return loadGenName;
	}
	public void setLoadGenName(String loadGenName) {
		this.loadGenName = loadGenName;
	}
	
}
