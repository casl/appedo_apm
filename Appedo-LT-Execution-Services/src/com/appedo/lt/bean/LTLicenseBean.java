package com.appedo.lt.bean;

public class LTLicenseBean {

	/* below form table `userwise_lic_monthwise` */
	//private long lLicScheduleId;
	private long lUserId;
	
	private String strStartDate;
	private String strEndDate;
	
	private int nReportRetentionPeriod;
	private int nMaxRuns;
	private int nMaxVusers;
	private int nMaxIterations;
	private int nMaxDuration;
	
	
	/* below from `lt_config_parameters` */
	private String strLicInternalName;
	private String strLicExternalName;
	public long getlUserId() {
		return lUserId;
	}
	public void setUserId(long lUserId) {
		this.lUserId = lUserId;
	}
	public String getStartDate() {
		return strStartDate;
	}
	public void setStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}
	public String getEndDate() {
		return strEndDate;
	}
	public void setEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	public int getReportRetentionPeriod() {
		return nReportRetentionPeriod;
	}
	public void setReportRetentionPeriod(int nReportRetentionPeriod) {
		this.nReportRetentionPeriod = nReportRetentionPeriod;
	}
	public int getMaxRuns() {
		return nMaxRuns;
	}
	public void setMaxRuns(int nMaxRuns) {
		this.nMaxRuns = nMaxRuns;
	}
	public int getMaxVusers() {
		return nMaxVusers;
	}
	public void setMaxVusers(int nMaxVusers) {
		this.nMaxVusers = nMaxVusers;
	}
	public int getMaxIterations() {
		return nMaxIterations;
	}
	public void setMaxIterations(int nMaxIterations) {
		this.nMaxIterations = nMaxIterations;
	}
	public int getMaxDuration() {
		return nMaxDuration;
	}
	public void setMaxDuration(int nMaxDuration) {
		this.nMaxDuration = nMaxDuration;
	}
	public String getLicInternalName() {
		return strLicInternalName;
	}
	public void setLicInternalName(String strLicInternalName) {
		this.strLicInternalName = strLicInternalName;
	}
	public String getLicExternalName() {
		return strLicExternalName;
	}
	public void setLicExternalName(String strLicExternalName) {
		this.strLicExternalName = strLicExternalName;
	}
	
	
}
