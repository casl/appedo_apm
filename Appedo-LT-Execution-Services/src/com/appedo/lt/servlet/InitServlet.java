package com.appedo.lt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.tcpserver.JmeterProcessingQueue;
import com.appedo.lt.tcpserver.JmeterQueueTimerTask;
import com.appedo.lt.tcpserver.LTFreeQueueCountTimerTask;
import com.appedo.lt.tcpserver.LTPaidQueueTimerTask;
import com.appedo.lt.tcpserver.LoadGenQueueTimerTask;
import com.appedo.lt.tcpserver.LoadGenServer;
import com.appedo.manager.LogManager;

/**
 * Servlet to handle one operation for the whole application
 * 
 * @author navin
 * 
 */
public class InitServlet extends HttpServlet {
	// set log access

	private static final long serialVersionUID = 1L;
	public static String realPath = null;
	public static TimerTask timerTaskLoadTest = null, timerTaskProcessingQueue = null, timerTaskCount = null, timerTaskPaid = null, timerJmeter = null, timerJmeterPro = null, timerJmeterDrain = null, ltDrainTimerTask= null;
	public static Timer timerLT = new Timer(), timerProcessing = new Timer(), timerCount = new Timer(), timerPaid = new Timer(), timerJM = new Timer(), timerJMPro = new Timer(),timerJMDrain = new Timer(), ltDrainTimer = new Timer();
	public static LoadGenServer threadLTTCPCommunication = null;
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		// super();

		// declare servlet context
		ServletContext context = getServletContext();

		realPath = context.getRealPath("//");

		Connection con = null;

		try {
			String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
			String strLog4jFilePath = context.getInitParameter("LOG4J_PROPERTIES_FILE_PATH");

			Constants.CONSTANTS_FILE_PATH = InitServlet.realPath + strConstantsFilePath;
			Constants.LOG4J_PROPERTIES_FILE = InitServlet.realPath + strLog4jFilePath;
			
			
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator(Constants.LOG4J_PROPERTIES_FILE);
			
			// Loads Constant properties
			Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Execution-Services", Constants.APPEDO_CONFIG_FILE_PATH, true);

			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants: WhiteLabels, Config-Properties
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from DB (or) the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			
			// Updating load test records
			// runManager.updateLoadTestRecords(con);
			
			// Updating dynamic Load Agent Details table
			// runManager.updateDynamicLADetails(con);
			
			
			threadLTTCPCommunication = new LoadGenServer();
			threadLTTCPCommunication.start();
			
//			timerTaskLoadTest = new LoadGenTimerTask();
//			timerLT.schedule(timerTaskLoadTest, 500, 1000*60);
			
			timerTaskCount = new LTFreeQueueCountTimerTask();
			timerCount.schedule(timerTaskCount, 500, 1000*10);
			
			timerTaskPaid = new LTPaidQueueTimerTask();
			timerPaid.schedule(timerTaskPaid, 100, 1000*60);
			
			timerTaskProcessingQueue = new LoadGenQueueTimerTask();
			timerProcessing.schedule(timerTaskProcessingQueue, 1000, 1000*30);
			
			timerJmeter = new JmeterQueueTimerTask();
			timerJM.schedule(timerJmeter, 100, 1000*10);
			
			timerJmeterPro = new JmeterProcessingQueue();
			timerJMPro.schedule(timerJmeterPro, 150, 1000*10);
			
//			timerJmeterDrain = new JmeterDrainingQueue();
//			timerJMDrain.schedule(timerJmeterDrain, 200, 1000*10);
			
//			ltDrainTimerTask = new LTDrainingTimerTask();
//			ltDrainTimer.scheduleAtFixedRate(ltDrainTimerTask, 150, 1000*10);

//			for( int i=0; i<10; i++ ) {
//				(new LTDrainingTimerTask()).start();
//			}
			
//			TaskExecutor.newExecutor(Constants.DEFAULT_LT_THREADPOOL_NAME, 10, 100, 1); // PoolName, minthread, maxthread, queue
			
			strConstantsFilePath = null;
			strLog4jFilePath = null;

		} catch (Throwable e) {
			LogManager.errorLog(e);

		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
