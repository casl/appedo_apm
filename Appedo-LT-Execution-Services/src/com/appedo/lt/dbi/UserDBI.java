package com.appedo.lt.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

/**
 * DBI handles the database operation for User module 
 * @author navin
 *
 */
public class UserDBI {
	/*
	public long validateUserIdAndPassword(Connection con, String strEmailId, String strPassword) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		long lUserId = 0;
		try {
			sbQuery.append("select user_id from usermaster where email_id = ? and password=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strEmailId);
			pstmt.setString(2, strPassword);
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				lUserId = rst.getLong("user_id");
			}
			
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return lUserId;
	}
	*/

	public HashMap<String,String> validateUserIdAndPasswordAndGetMachineId(Connection con, String strEmailId, String strPassword) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		HashMap<String,String> userDetail=new HashMap<String,String>();  
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery.append( "select user_id, unique_machine_id, license_level, CASE WHEN license_level='level0' THEN lt_max_vusers ElSE (  select userwise_lic_monthwise.lt_max_vusers from usermaster, userwise_lic_monthwise where usermaster.user_id=result.user_id and usermaster.user_id=userwise_lic_monthwise.user_id and start_date<=now() and end_date>=now() and module_type = 'LT' limit 1 ) end  from usermaster as result where email_id = ? and pgp_sym_decrypt(password,?) = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strEmailId);
			pstmt.setString(2, Constants.getPasswordSaltKey());
			pstmt.setString(3, strPassword);
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				userDetail.put("userid",  rst.getString("user_id"));
				userDetail.put("unique_machine_id",rst.getString("unique_machine_id"));
				userDetail.put("lt_max_vusers",rst.getString("lt_max_vusers"));
				
			}
			
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return userDetail;
	}
	
	/***
	 * To validate the version
	 * @param con
	 * @param strVersion
	 * @return
	 * @throws Exception
	 */
	public boolean validateAppedoLTVersion(Connection con,String strModuleName, String strVersion) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		boolean bSatus = false;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery.append("SELECT bum.version_number FROM build_upgrade_master bum INNER JOIN (    SELECT module_name, MAX(upgraded_on) AS latest_upgraded_on FROM build_upgrade_master GROUP BY module_name)latest ON bum.module_name = latest.module_name AND bum.upgraded_on = latest_upgraded_on AND bum.module_name=? AND bum.version_number =? ORDER BY 1;");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			
			pstmt.setString(1, strModuleName.trim());
			pstmt.setString(2, strVersion.trim());
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				bSatus=true;
			}else {
				bSatus=false;
			}
			
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return bSatus;
	}
	
}
