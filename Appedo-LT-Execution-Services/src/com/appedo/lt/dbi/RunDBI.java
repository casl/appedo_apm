package com.appedo.lt.dbi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.bean.LoginUserBean;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LTLicenseBean;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.bean.ReportDataBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.model.AmazonConfiguration;
import com.appedo.lt.model.CryptManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class RunDBI {

	
	public StringBuilder  getAvailableScripts(Connection con, long lUserId) throws Exception {
		
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		StringBuilder sbResult = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT script_name as scriptname  ")
					.append("FROM lt_script_master ")
					.append("WHERE user_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				sbResult.append(rst.getString("scriptname"));
				sbResult.append(",");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return sbResult;
		
		
	}
	public long checkScriptExist(Connection con, long lUserId, String strScriptName, int enterpriseId, boolean isVUScript) throws Exception {
		PreparedStatement pstmt = null, pstmtUpdate = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		long lScriptId = 0;
		
		try {
			sbQuery	.append("SELECT script_Id as scriptid  ")
					.append("FROM lt_script_master ")
					.append("WHERE user_id = ?  and script_name = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			pstmt.setString(2, strScriptName);
			rst = pstmt.executeQuery();
			if(rst.next()) {
				lScriptId = rst.getInt("scriptid");
				if(isVUScript){
					sbQuery.setLength(0);
					sbQuery .append("UPDATE lt_script_master SET created_on = now() WHERE script_id = ?");
					pstmtUpdate = con.prepareStatement(sbQuery.toString());
					pstmtUpdate.setLong(1, lScriptId);
					pstmtUpdate.execute();
				}
			}else {
				// insert script with new id
				DataBaseManager.close(pstmt);
//				pstmt = null;
				
				String strQuery = null;
				
				
				try {
					strQuery = "INSERT INTO lt_script_master (user_id, script_name, script_type, created_by, created_on, enterprise_id) VALUES (?, ?, ?, ?, now(), ?) ";
					
					pstmt = con.prepareStatement(strQuery, PreparedStatement.RETURN_GENERATED_KEYS);
					
					pstmt.setLong(1, lUserId);
					pstmt.setString(2, strScriptName);
					pstmt.setString(3, "APPEDO_LT");
					pstmt.setLong(4, lUserId);
					if( enterpriseId == 0 ) {
						pstmt.setNull(5, Types.INTEGER);
					} else {
						pstmt.setInt(5, enterpriseId);	
					}
					
					pstmt.executeUpdate();
					
					lScriptId = DataBaseManager.returnKey(pstmt);
				} catch (Exception e) {
					LogManager.errorLog(e);
					throw e;
				} finally {
					DataBaseManager.close(pstmt);
//					pstmt = null;
					DataBaseManager.close(pstmtUpdate);
					pstmtUpdate = null;
					
//					strQuery = null;
				}
				
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return lScriptId;
		
	}

	/**
	 * Gets user total run count for the current month  
	 * 
	 * @param con
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public int getUserMaxRunCount(Connection con, LoginUserBean loginUserBean) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		int nUserTotalRuncount = 0;
		
		try {
			sbQuery	.append("SELECT count(*) as user_total_runcount ")
					.append("FROM tblreportmaster ")
					.append("WHERE userid = ? AND created_on BETWEEN date_trunc('month', CURRENT_DATE) AND date_trunc('month', CURRENT_DATE) + interval '1month' - interval '1day' ")
					.append("GROUP BY userid ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			rst = pstmt.executeQuery();
			if(rst.next()) {
				nUserTotalRuncount = rst.getInt("user_total_runcount");
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return nUserTotalRuncount;
	}
	
	public int getUserMaxRunCountPerDay(Connection con, LoginUserBean loginUserBean) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		int nUserTotalRuncountPerDay = 0;
		
		try {
			sbQuery	.append("SELECT count(*) as user_total_runcount_perday ")
					.append("FROM tblreportmaster ")
					.append("WHERE userid = ? AND created_on BETWEEN date_trunc('day', CURRENT_DATE) AND date_trunc('day', CURRENT_DATE) + interval '1day' - interval '1sec' ")
					.append("GROUP BY userid ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			rst = pstmt.executeQuery();
			if(rst.next()) {
				nUserTotalRuncountPerDay = rst.getInt("user_total_runcount_perday");
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return nUserTotalRuncountPerDay;
	}
	
	/**
	 * get running scenario
	 * 
	 * @param con
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getRunningScenario(Connection con, LoginUserBean loginUserBean) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		JSONObject joUserRunningScenario = new JSONObject();
		
		try {
			
			sbQuery	.append("SELECT * FROM tblreportmaster ")
						.append("WHERE  is_active = true ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			if(rst.next()) {
				joUserRunningScenario.put("scenarioname", rst.getString("scenarioname"));
				joUserRunningScenario.put("isActive", rst.getBoolean("is_active"));
				joUserRunningScenario.put("runid", rst.getString("runid"));
				joUserRunningScenario.put("createduser", rst.getString("createduser"));
				joUserRunningScenario.put("completeduser", rst.getString("completeduser"));
				joUserRunningScenario.put("status", rst.getString("status"));
				joUserRunningScenario.put("runstarttime", rst.getString("runstarttime"));
				joUserRunningScenario.put("runendtime", rst.getString("runendtime"));
				joUserRunningScenario.put("reportName", rst.getString("reportname"));
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return joUserRunningScenario;
	}
	/**
	 * get user running scenario
	 * 
	 * @param con
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject isUserRunningScenario(Connection con, LoginUserBean loginUserBean, String strTestType ) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		JSONObject joUserRunningScenario = new JSONObject();
		try {
			
			sbQuery	.append("SELECT * FROM tblreportmaster ")
			.append("WHERE  userid = ? AND is_active = true ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			rst = pstmt.executeQuery();
			if(rst.next()) {
				joUserRunningScenario.put("scenarioname", rst.getString("scenarioname"));
				joUserRunningScenario.put("isActive", rst.getBoolean("is_active"));
				joUserRunningScenario.put("runid", rst.getString("runid"));
				joUserRunningScenario.put("createduser", rst.getString("createduser"));
				joUserRunningScenario.put("completeduser", rst.getString("completeduser"));
				joUserRunningScenario.put("status", rst.getString("status"));
				joUserRunningScenario.put("runstarttime", rst.getString("runstarttime"));
				joUserRunningScenario.put("runendtime", rst.getString("runendtime"));
				joUserRunningScenario.put("reportName", rst.getString("reportname"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return joUserRunningScenario;
	}
	public JSONObject getUserRunningScenario(Connection con, LoginUserBean loginUserBean, String strReportType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		JSONObject joUserRunningScenario = new JSONObject();
		try {
			sbQuery	.append("SELECT tr.runid, tr.runstarttime as runstarttime, tr.reportname as reportname, tr.scenarioname as scenarioname, ")
					.append("COALESCE(sum(ss.created_users),0) AS createduser, COALESCE(sum(ss.completed_users),0) AS completeduser ")
					.append("FROM tblreportmaster tr ")
					.append("LEFT join scriptwise_status ss on tr.runid = ss.runid ")
					.append("WHERE tr.userid = ? AND tr.is_active = true AND tr.reporttype = ?")
					.append("GROUP BY tr.runid, tr.reportname, tr.scenarioname, tr.runstarttime");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			pstmt.setString(2, strReportType);
			rst = pstmt.executeQuery();
			while(rst.next()) {
				joUserRunningScenario.put("runid", rst.getLong("runid"));
				joUserRunningScenario.put("reportName", rst.getString("reportname"));
				joUserRunningScenario.put("scenarioname", rst.getString("scenarioname"));
				joUserRunningScenario.put("createduser", rst.getString("createduser"));
				joUserRunningScenario.put("completeduser", rst.getString("completeduser"));
				joUserRunningScenario.put("runstarttime", rst.getTimestamp("runstarttime").getTime());
				
				long lResDate = rst.getTimestamp("runstarttime").getTime();
				long lReqDate = new Date().getTime();
				long diff = lReqDate - lResDate;
				long diffSeconds = (diff / 1000) % 60;
				long diffMinutes = (diff / (60 * 1000)) % 60;
				long diffHours = diff / (60 * 60 * 1000);
				joUserRunningScenario.put("elapsedtime", (diffHours<=9?"0"+diffHours:diffHours)+":"+(diffMinutes<=9?"0"+diffMinutes:diffMinutes)+":"+(diffSeconds<=9?"0"+diffSeconds:diffSeconds));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return joUserRunningScenario;
	}
	/**
	 * to keep the running scenario report details into report master table
	 * @param con
	 * @param ltLicBean 
	 * @param reportName
	 * @param UserId
	 * @param strReportType
	 * @throws Throwable
	 */
	public long  insertIntoReportMaster(Connection con, LTLicenseBean ltLicBean, LoadTestSchedulerBean schedulerBean) throws Throwable {
		
		PreparedStatement pstmt = null, pstmtLog = null, pstmtError = null, pstmtReport = null, pstmtTrans = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		long lRunId = -1L;
		try	{
			
//			if(ltLicBean == null) {
//				// License expired for user
//				throw new Exception("2");
//			} else {
				sbQuery	.append("INSERT INTO tblreportmaster (userid,reportname,reporttype,scenarioname,created_by,created_on, is_active, runstarttime, scenario_id) ")
				.append("VALUES (?, ?, ?, ?, ?, now(), true, now(), ?) ");
				pstmt = con.prepareStatement(sbQuery.toString(), PreparedStatement.RETURN_GENERATED_KEYS);
				
				pstmt.setLong(1, schedulerBean.getUserId());
				pstmt.setString(2, schedulerBean.getReportName());
				pstmt.setString(3, schedulerBean.getTestType());
				pstmt.setString(4, schedulerBean.getScenarioName());
				pstmt.setLong(5,  schedulerBean.getUserId());
				pstmt.setLong(6, Long.valueOf(schedulerBean.getScenarioId()));
				
				pstmt.executeUpdate();
				
				lRunId = DataBaseManager.returnKey(pstmt);
				
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT lt_logs_table_partitions(?)");
				pstmtReport = con.prepareStatement(sbQuery.toString());
				pstmtReport.setLong(1, lRunId);
				pstmtReport.execute();
//			}
		}
		catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			DataBaseManager.close(pstmtLog);
//			pstmtLog = null;
			DataBaseManager.close(pstmtError);
//			pstmtError = null;
			DataBaseManager.close(pstmtReport);
//			pstmtReport = null;
			DataBaseManager.close(pstmtTrans);
//			pstmtTrans = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return lRunId;
		
	}
	/**
	 * License details for free users to run load tests
	 * 
	 * @param strLicenseFilePath
	 */
	public JSONObject loadLoadTestLicenseDetails(Connection con, LoginUserBean userBean) {
		
		JSONObject LOAD_TEST_LICENSE_DETAILS = new JSONObject();
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		ResultSet rs = null;
		
		try {
			if (userBean.getLicense().equalsIgnoreCase("level0")) {
				sbQuery.append("SELECT * from lt_config_parameters where lt_license=?");
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setString(1, userBean.getLicense());
			} else {
				sbQuery.append("SELECT MAX(lt_max_vusers) as max_vusers, SUM(ulm.lt_max_run_per_day) as lt_max_run_per_day, ")
						.append("MAX(ulm.max_duration) as max_duration, MAX(max_iterations) as max_iterations FROM userwise_lic_monthwise ulm ")
						.append("INNER JOIN lt_config_parameters lcp ON lcp.lt_license = ulm.license_level ")
						.append("WHERE ulm.module_type = 'LT' and ulm.user_id = ? and ulm.start_date<= now() and ulm.end_date>=now()");
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setLong(1, userBean.getUserId());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				if (userBean.getLicense().equalsIgnoreCase("level0")) {
					LOAD_TEST_LICENSE_DETAILS.put("mode", "FREE");
				} else {
					LOAD_TEST_LICENSE_DETAILS.put("mode", "PAID");
				}
				LOAD_TEST_LICENSE_DETAILS.put("maxuser", rs.getInt("max_vusers"));
//				LOAD_TEST_LICENSE_DETAILS.put("maxruncount", rs.getInt("max_runs"));
				LOAD_TEST_LICENSE_DETAILS.put("maxruncountperday", rs.getInt("lt_max_run_per_day"));
				LOAD_TEST_LICENSE_DETAILS.put("maxduration", rs.getInt("max_duration"));
				LOAD_TEST_LICENSE_DETAILS.put("maxiteration", rs.getInt("max_iterations"));
			}
			// }
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return LOAD_TEST_LICENSE_DETAILS;
	}
	/**
	 *  inserting Scriptwisestatus 
	 *  
	 * @param con
	 * @param hm
	 * @throws Exception
	 */
	public void insertScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append(
					"INSERT INTO  scriptwise_status(runid, error_count, script_id, script_name, created_users, completed_users, http_200_count, http_300_count, http_400_count, http_500_count, is_completed)")
					.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, Integer.parseInt((String)hm.get("runid")));
			pstmt.setInt(2, Integer.parseInt((String)hm.get("error_count")));
			pstmt.setLong(3, Long.parseLong((String)hm.get("script_id")));
			pstmt.setString(4, (String)hm.get("script_name"));
			if(Integer.parseInt((String)hm.get("created_users")) == 0){
				pstmt.setNull(5, Types.INTEGER);
			} else{
				pstmt.setInt(5, Integer.parseInt((String)hm.get("created_users")));
			}
			if(Integer.parseInt((String)hm.get("completed_users")) == 0){
				pstmt.setNull(6, Types.INTEGER);
			} else{
				pstmt.setInt(6, Integer.parseInt((String)hm.get("completed_users")));
			}
			
			pstmt.setInt(7, Integer.parseInt((String)hm.get("http_200_count")));
			pstmt.setInt(8, Integer.parseInt((String)hm.get("http_300_count")));
			pstmt.setInt(9, Integer.parseInt((String)hm.get("http_400_count")));
			pstmt.setInt(10, Integer.parseInt((String)hm.get("http_500_count")));
			pstmt.setInt(11, Integer.parseInt((String)hm.get("is_completed")));
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	/**
	 * updating ScriptwiseStatus
	 * @param con
	 * @param hm
	 * @throws Exception
	 */
	public void updateScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append(
					"UPDATE  scriptwise_status SET  error_count=?, script_id=?, script_name=?, created_users=?, completed_users=?, http_200_count=?, http_300_count=?, http_400_count=?, http_500_count=?, is_completed=? WHERE runid= ? AND script_id=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, Integer.parseInt((String)hm.get("error_count")));
			pstmt.setLong(2,  Long.parseLong((String)hm.get("script_id")));
			pstmt.setString(3, (String)hm.get("script_name"));
			if(Integer.parseInt((String)hm.get("created_users")) == 0){
				pstmt.setNull(4, Types.INTEGER);
			} else{
				pstmt.setInt(4, Integer.parseInt((String)hm.get("created_users")));
			}
			if(Integer.parseInt((String)hm.get("completed_users")) == 0){
				pstmt.setNull(5, Types.INTEGER);
			} else{
				pstmt.setInt(5, Integer.parseInt((String)hm.get("completed_users")));
			}
			
			pstmt.setInt(6, Integer.parseInt((String)hm.get("http_200_count")));
			pstmt.setInt(7, Integer.parseInt((String)hm.get("http_300_count")));
			pstmt.setInt(8, Integer.parseInt((String)hm.get("http_400_count")));
			pstmt.setInt(9, Integer.parseInt((String)hm.get("http_500_count")));
			pstmt.setInt(10, Integer.parseInt((String)hm.get("is_completed")));
			pstmt.setInt(11, Integer.parseInt((String)hm.get("runid")));
			pstmt.setLong(12,  Long.parseLong((String)hm.get("script_id")));
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	/**
	 * getting running scripts
	 * 
	 * @param con
	 * @param joRunningScenario
	 * @return
	 * @throws Exception
	 */
	public JSONArray getRunningScriptData(Connection con, org.json.JSONObject joRunningScenario) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		JSONArray jaRunningScripts = new JSONArray();
		JSONObject joScripts = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("select * from scriptwise_status where runid=?")
					.append(" ORDER BY script_name");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, joRunningScenario.getLong("runid"));
			
			rst = pstmt.executeQuery();
			while(rst.next()){
				joScripts = new JSONObject();
				joScripts.put("encryptedRunId", CryptManager.encryptEncodeURL(rst.getLong("runid")+""));
				joScripts.put("scriptName", rst.getString("script_name"));
				joScripts.put("createdusers", rst.getInt("created_users"));
				joScripts.put("completedusers", rst.getInt("completed_users"));
				joScripts.put("http200count", rst.getInt("http_200_count"));
				joScripts.put("http300count", rst.getInt("http_300_count"));
				joScripts.put("http400count", rst.getInt("http_400_count"));
				joScripts.put("http500count", rst.getInt("http_500_count"));
				joScripts.put("errorcount", rst.getInt("error_count"));
				joScripts.put("iscompleted", rst.getInt("is_completed"));
				
				jaRunningScripts.add(joScripts);
//				joScripts = null;
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaRunningScripts;
	}
	public JSONArray getCompletedScriptData(Connection con, org.json.JSONObject joCompletedScenario) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		JSONArray jaRunningScripts = new JSONArray();
		JSONObject joScripts = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("select * from scriptwise_status where runid=?")
					.append(" ORDER BY script_name");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, Integer.parseInt(joCompletedScenario.getString("reportId")));
			
			rst = pstmt.executeQuery();
			while(rst.next()){
				joScripts = new JSONObject();
				joScripts.put("encryptedRunId", CryptManager.encryptEncodeURL(rst.getLong("runid")+""));
				joScripts.put("scriptName", rst.getString("script_name"));
				joScripts.put("createdusers", rst.getInt("created_users"));
				joScripts.put("completedusers", rst.getInt("completed_users"));
				joScripts.put("http200count", rst.getInt("http_200_count"));
				joScripts.put("http300count", rst.getInt("http_300_count"));
				joScripts.put("http400count", rst.getInt("http_400_count"));
				joScripts.put("http500count", rst.getInt("http_500_count"));
				joScripts.put("errorcount", rst.getInt("error_count"));
				joScripts.put("iscompleted", rst.getInt("is_completed"));
				
				jaRunningScripts.add(joScripts);
//				joScripts = null;
			}
			
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaRunningScripts;
	}
	
	public void updateQueuedScenario(Connection con, String strReportName) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("update tblreportmaster set status='' where reportname = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strReportName);
			
			pstmt.execute();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	public void updateActiveStatus(Connection con, int nRunid) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("UPDATE tblreportmaster SET is_active = false, status = 'COMPLETED', runendtime = NOW() ")
					.append("WHERE runid = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, nRunid);
			
			pstmt.execute();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	/**
	 * to update tblchartsummary table
	 * @param con
	 * @param strReportName
	 * @param strCsvPath
	 * @throws Exception
	 */
	public void updateSummaryResult(Connection con, String strReportName, String strCsvPath) throws Exception {
		
		BufferedReader br = null;
		FileReader fileReader = null;
		String line = "";
		String cvsSplitBy = ",";
		String strQry="";
		String chartsummaryTable="tblchartsummary";
		int linecount=0;
		Statement stmt = null;
		String[] spiltVal = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			fileReader = new FileReader(strCsvPath);
			br = new BufferedReader(fileReader);
			while ((line = br.readLine()) != null) {
				if(linecount>0 && cvsSplitBy.contains(","))
				{
					line=line.replace(" ", "");
					// use comma as separator
					spiltVal = line.split(cvsSplitBy);
					// make a query to insert
					strQry="insert into "+chartsummaryTable+" values("+spiltVal[0]+","+spiltVal[2]+","+spiltVal[3]+","+spiltVal[4]+","+spiltVal[5]+","+spiltVal[6]+",'"+spiltVal[1]+"',(select max(runid) from tblreportmaster where runid="+strReportName+"),"+spiltVal[7]+")";
					
					stmt = con.createStatement();
					stmt.executeUpdate(strQry);
				}
				linecount=linecount+1;
//				spiltVal = null;
//				strQry = null;
			}
			
			LogManager.infoLog("Records created successfully");
			
		}catch(Exception e) {
			LogManager.errorLog(e);
			
		}finally {
			DataBaseManager.close(stmt);
//			stmt = null;
			UtilsFactory.close(br);
//			br = null;
			UtilsFactory.close(fileReader);
//			fileReader = null;
			
//			line = null;
//			cvsSplitBy = null;
//			chartsummaryTable = null;	
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	/**
	 * Deleting completed scenario's
	 * 
	 * @param con
	 * @param lReportId
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject deleteCompletedLoadTest(Connection con, long lReportId, LoginUserBean loginUserBean) throws Exception {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		JSONObject joMessage = null;
		Date dateLog = LogManager.logMethodStart();

		try{
			sbQuery = new StringBuffer();
			
			sbQuery	.append("update tblreportmaster set is_deleted = true, deleted_by = ?, modified_on = NOW() where runid = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			pstmt.setLong(2, lReportId);
			
			pstmt.execute();
			joMessage = UtilsFactory.getJSONSuccessReturn("Execution report successfully deleted.");
			
		}catch(Exception e){
			System.out.println("Exception in deleteCompletedLoadTest"+e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return joMessage;
	}
	/**
	 * updating loadtest records on server starting.
	 * 
	 * @param con
	 * @throws Exception
	 */
	public void updateLoadTestRecords(Connection con) throws Exception {
		
		PreparedStatement pstmt = null, stmt = null, stmtPvtCounters = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();
			
			sbQuery	.append("update tblreportmaster set is_active = false, status='FAILED', runendtime=NOW() where is_active = true");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.execute();
			
			sbQuery.setLength(0);
			sbQuery	.append("update scriptwise_status set is_completed = 1 where is_completed = 0");
			stmt = con.prepareStatement(sbQuery.toString());
			stmt.execute();
			
			sbQuery.setLength(0);
			sbQuery	.append("update appedo_pvt_counters set lt_exec_status = true, lt_controller_status_last_updated = now()");
			stmtPvtCounters = con.prepareStatement(sbQuery.toString());
			stmtPvtCounters.execute();
			
			LogManager.infoLog("Load Test records are updated successfully");
			UtilsFactory.clearCollectionHieracy( sbQuery );
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			DataBaseManager.close( stmt );
//			stmt = null;
			DataBaseManager.close( stmtPvtCounters );
//			stmtPvtCounters = null;
			LogManager.logMethodEnd(dateLog);
		}
	}
	/**
	 * Checking is report name already exist or not
	 * @param con
	 * @param schedulerBean
	 * @return
	 */
	public boolean isReportNameExist(Connection con, LoadTestSchedulerBean schedulerBean){
		boolean bExist = false;
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();
			
			sbQuery	.append("SELECT true from tblreportmaster  where lower(reportname)=LOWER(?) AND userid=?  AND is_deleted=false");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, schedulerBean.getReportName());
			pstmt.setLong(2, schedulerBean.getUserId());
			rst = pstmt.executeQuery();
			if(rst.next()){
				bExist = true;
			}else{
				bExist = false;
			}
			
		} catch(Exception e){
			LogManager.errorLog(e);
		} finally {
			LogManager.logMethodEnd(dateLog);
		}
		return bExist;
	}
	
	public JSONObject getInstanceDetails(Connection con, String strModule) throws Exception{
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		JSONObject joInstanceDetails = new JSONObject();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT * FROM amazon_instance_config ")
					.append("WHERE module = ?");

			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strModule);
			rst = pstmt.executeQuery();
			
			if( rst.next() ) {
				joInstanceDetails.put("instance_owner", rst.getString("instance_owner"));
				joInstanceDetails.put("module", rst.getString("module"));
				joInstanceDetails.put("access_key", rst.getString("access_key"));
				joInstanceDetails.put("secret_access_key", rst.getString("secret_access_key"));
				joInstanceDetails.put("sec_group_name", rst.getString("sec_group_name"));
				joInstanceDetails.put("keypair", rst.getString("keypair"));
			}
						
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return joInstanceDetails;
	}
	
	public void insertLoadAgentDetails(Connection con, HashMap<String, String> hm ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			Calendar nextCheckTime = Calendar.getInstance();
			nextCheckTime.setTime(new Date(Long.valueOf(hm.get("start_time"))));
			nextCheckTime.add(Calendar.MINUTE, 55);
			
			sbQuery.append(
					"INSERT INTO  lt_dynamic_load_agent_details(instance_id, region, endpoint, location, instance_status, public_ip, created_on, start_time, next_check_time, os_type)")
					.append("VALUES(?, ?, ?, ?, ?, ?, now(), ?, ?,?)");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, hm.get("instance_id"));
			pstmt.setString(2, hm.get("region"));
			pstmt.setString(3, hm.get("endpoint"));
			pstmt.setString(4, hm.get("location"));
			pstmt.setString(5, hm.get("instance_status"));
			pstmt.setString(6, hm.get("public_ip"));
			pstmt.setTimestamp(7, new Timestamp(Long.valueOf(hm.get("start_time"))));
			pstmt.setTimestamp(8, new Timestamp(nextCheckTime.getTime().getTime()));
			pstmt.setString(9, hm.get("os_type"));
			pstmt.executeUpdate();
			
			nextCheckTime = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public JSONArray getDynamicLoadAgentDetails(Connection con) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		JSONArray jaDynamicAgentDetails = new JSONArray();
		JSONObject joLADetails = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("select instance_id, instance_status, next_check_time, endpoint from lt_dynamic_load_agent_details ")
					.append("where instance_status != 'terminated' and terminated_on is null and public_ip != ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, Constants.FG_CONTROLLER_IP);
			rst = pstmt.executeQuery();
			
			while(rst.next()){
				joLADetails = new JSONObject();
				
				joLADetails.put("instance_id", rst.getString("instance_id"));
				joLADetails.put("instance_status", rst.getString("instance_status"));
				joLADetails.put("next_check_time", rst.getTimestamp("next_check_time").getTime());
				joLADetails.put("endpoint", rst.getString("endpoint"));
				jaDynamicAgentDetails.add(joLADetails);
//				joLADetails = null;
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaDynamicAgentDetails;
	}

	public void updateNextCheckTime(Connection con, String instanceId, long curNextTimeValue) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Calendar nextCheckTime = Calendar.getInstance();
		nextCheckTime.setTime(new Date(curNextTimeValue));
		nextCheckTime.add(Calendar.HOUR_OF_DAY, 1);
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("UPDATE lt_dynamic_load_agent_details SET  next_check_time=? WHERE instance_id= ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setTimestamp(1, new Timestamp(nextCheckTime.getTimeInMillis()));
			pstmt.setString(2, instanceId);
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
//			nextCheckTime = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	
	}

	public void updateInstanceDetails(Connection con, String instanceId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("UPDATE lt_dynamic_load_agent_details SET instance_status='terminated', terminated_on=now() WHERE instance_id= ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, instanceId);
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public JSONArray getInactiveLoadAgentDetails(Connection con, String region, String osType, String ipAddress) throws Throwable {
		JSONArray jaInstanceStatus = new JSONArray();
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("SELECT instance_status, public_ip, instance_id FROM lt_dynamic_load_agent_details WHERE region LIKE '%"+region+"%' AND instance_status='inactive' AND os_type = ? " );
			if( osType.equalsIgnoreCase("WINDOWS") && ipAddress != null ){
				sbQuery.append(" AND public_ip <> ?");
			}
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, osType);
			if( osType.equalsIgnoreCase("WINDOWS") && ipAddress != null ){
				pstmt.setString(2, ipAddress);
			}
			rst = pstmt.executeQuery();
			
			while( rst.next() ) {
				JSONObject joInstanceStatus = new JSONObject();
				joInstanceStatus.put("instance_status", rst.getString("instance_status"));
				joInstanceStatus.put("public_ip", rst.getString("public_ip"));
				joInstanceStatus.put("instance_id", rst.getString("instance_id"));
				jaInstanceStatus.add( joInstanceStatus );
//				joInstanceStatus = null;
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaInstanceStatus;
	}
	
	public JSONObject getRegionDetails(Connection con, String region, String osType) throws Throwable {
		JSONObject joRegionDetails = new JSONObject();
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("SELECT region, endpoint, reservation, image_id, os_type FROM lt_region_details WHERE region LIKE '%"+region+"%' AND os_type = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, osType);
			rst = pstmt.executeQuery();
			
			if( rst.next() ) {
				joRegionDetails.put("region", rst.getString("region"));
				joRegionDetails.put("endpoint", rst.getString("endpoint"));
				joRegionDetails.put("reservation", rst.getInt("reservation"));
				joRegionDetails.put("image_id", rst.getString("image_id"));
				joRegionDetails.put("os_type", rst.getString("os_type"));
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return joRegionDetails;
	}
	
	public void updateLoadAgentDetails(Connection con, String publicIp, String instanceId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		Calendar nextCheckTime = Calendar.getInstance();
		nextCheckTime.setTime(new Date());
		nextCheckTime.add(Calendar.HOUR_OF_DAY, 1);
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("UPDATE lt_dynamic_load_agent_details SET instance_status='active', start_time = now(), next_check_time = ? WHERE public_ip= ? AND instance_id=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setTimestamp(1, new Timestamp(nextCheckTime.getTimeInMillis()));
			pstmt.setString(2, publicIp);
			pstmt.setString(3, instanceId);
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
//			nextCheckTime = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateNonAvailabilityRegion(Connection con, String status, LoadTestSchedulerBean loadTestSchedulerBean) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("update tblreportmaster set is_active=false, status='FAILED', runendtime=NOW(), notes=? where reportname = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, status);
			pstmt.setString(2, loadTestSchedulerBean.getReportName());
			
			pstmt.execute();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public int getMaxUserCount(Connection con, LoadTestSchedulerBean loadTestSchedulerBean) throws Exception {
		PreparedStatement pstmt = null, pstmtUser = null;
		ResultSet rst = null, rstUser = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		int nUserTotalRuncount = 0;
		
		try {
			sbQuery.append("SELECT instance_type FROM lt_config_parameters WHERE lt_license=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, loadTestSchedulerBean.getLicense());
			rst = pstmt.executeQuery();
			sbQuery.setLength(0);
			
			if( rst.next() ){
				sbQuery	.append("SELECT max_vuser_per_type ")
						.append("FROM vuser_lt_gen_setting ")
						.append("WHERE instance_type = ? ");
		
				pstmtUser = con.prepareStatement(sbQuery.toString());
				pstmtUser.setString(1, rst.getString("instance_type"));
				rstUser = pstmtUser.executeQuery();
				if(rstUser.next()) {
					nUserTotalRuncount = rstUser.getInt("max_vuser_per_type");
				}
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			DataBaseManager.close(rstUser);
//			rstUser = null;
			DataBaseManager.close(pstmtUser);
//			pstmtUser = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return nUserTotalRuncount;
	}
	
	public int updateInstanceAvailability(Connection con, String region_endpoint, String accesskey, String secretkey,  String osType) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		AmazonConfiguration amazonConfiguration = new AmazonConfiguration();
		int availability = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			availability = amazonConfiguration.getAmazonCount(region_endpoint, accesskey, secretkey);
			
			sbQuery.append("UPDATE lt_region_details SET total_availability=? WHERE endpoint = ? AND os_type = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, availability);
			pstmt.setString(2, region_endpoint);
			pstmt.setString(3, osType);
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
//			amazonConfiguration = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return availability;
	}
	
	public void insertRunGenDetails(Connection con, LoadTestSchedulerBean loadTestSchedulerBean, String instanceId) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery	.append("INSERT INTO lt_run_gen_details(run_id, scenario_id, instance_id, start_time, end_time)")
					.append("VALUES(?, ?, ?, now(), now())");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loadTestSchedulerBean.getRunId());
			pstmt.setLong(2, Long.valueOf(loadTestSchedulerBean.getScenarioId()));
			pstmt.setString(3, instanceId);
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateInActiveLoadAgent(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("UPDATE lt_dynamic_load_agent_details SET instance_status = 'inactive' ")
					.append("WHERE instance_id in ( SELECT instance_id FROM lt_run_gen_details WHERE run_id = ? )");

			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			pstmt.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	public int getLoadAgentDetailsCount(Connection con, String ipAddress) throws Throwable {
		int nTotalDymanic = 0;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT count(*) as total_dynamic FROM lt_dynamic_load_agent_details WHERE instance_status!='terminated' AND public_ip=? AND os_type ='WINDOWS'" );
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, ipAddress);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				nTotalDymanic = rst.getInt("total_dynamic");
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return nTotalDymanic;
	}
	
	public boolean isJMeterTestRunning(Connection con, String runId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		boolean isRunning = false;
		try {
			sbQuery.append("SELECT true  FROM tblreportmaster WHERE runid = ? AND is_active AND status =?" );
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, Long.parseLong(runId));
			pstmt.setString(1, "RUNNING");
			rst = pstmt.executeQuery();
			if(rst.next()) {
				isRunning = true;
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return isRunning;
	}
	
	public String getJMeterRunningIP(Connection con, String runId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		String strLoadGenIp = "", firstIP = "";
		try {
			sbQuery.append("SELECT loadgenipaddresses FROM tblreportmaster WHERE runid = ? AND is_active AND status =?" );
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, Long.parseLong(runId));
			pstmt.setString(2, "RUNNING");
			rst = pstmt.executeQuery();
			if(rst.next()) {
				strLoadGenIp = rst.getString("loadgenipaddresses");
				if (strLoadGenIp.trim().length() > 0) {
					firstIP = strLoadGenIp.split(",")[0];
				}
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return firstIP;
	}
	
	public String ltLicenseInstanceDetails(Connection con, String ltType) {
		
		String instanceType = null;
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rs = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery	.append("SELECT * from lt_config_parameters where lt_license=?"); 
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, ltType);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				instanceType = rs.getString("instance_type");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return instanceType;
	}
	/**
	 * Getting failed scenarios.
	 * @param con
	 * @param nLimit
	 * @param nOffset
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getFailedScenarios(Connection con, int nLimit, int nOffset, LoginUserBean loginUserBean, String strTestType) throws Exception {
		
		PreparedStatement stmt = null, pstmt = null;
		ResultSet rstCnt = null, rs = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		JSONArray jaFailedScenarios = null;
		JSONObject joScenario = null;
		JSONObject joResult = new JSONObject();
		long lTotalResults = -1L;
		try{
			jaFailedScenarios = new JSONArray();
			
			sbQuery .append("SELECT count(*) AS total_results from tblreportmaster where userid=? AND status = 'FAILED' AND reporttype=?");
			
			stmt = con.prepareStatement(sbQuery.toString());
			stmt.setLong(1, loginUserBean.getUserId());
			stmt.setString(2, strTestType);
			rstCnt = stmt.executeQuery();
			
			if(rstCnt.next()) {
				lTotalResults = rstCnt.getLong("total_results");
			}
			
			sbQuery.setLength(0);
			
			sbQuery .append("SELECT * from tblreportmaster where userid=? AND status = 'FAILED' AND reporttype=? ORDER BY runendtime desc limit ? offset ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			pstmt.setString(2, strTestType);
			pstmt.setInt(3, nLimit);
			pstmt.setInt(4, nOffset);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				joScenario = new JSONObject();
				
				joScenario.put("reportName", rs.getString("reportname"));
				joScenario.put("scenarioName", rs.getString("scenarioname"));
				joScenario.put("startTime", rs.getTimestamp("runstarttime").getTime());
				joScenario.put("endTime", rs.getTimestamp("runendtime").getTime());
				joScenario.put("notes", rs.getString("notes"));
				
				jaFailedScenarios.add(joScenario);
				
				joScenario = null;
			}
			joResult.put("totalResults", lTotalResults);
			joResult.put("jaFailedScenarios", jaFailedScenarios);
			jaFailedScenarios = null;
		}catch(Exception e){
			LogManager.errorLog(e);
		}finally{
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return joResult;
	} 
	
	public void updateDynamicLADetails(Connection con) throws Exception {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();
			
			sbQuery	.append("update lt_dynamic_load_agent_details set instance_status = 'inactive' WHERE instance_status = 'active'");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.execute();
			LogManager.infoLog("Dynamic Load Agent Details table's instance_status made inactive");
			UtilsFactory.clearCollectionHieracy( sbQuery );
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			LogManager.logMethodEnd(dateLog);
		}
	}

	public void updateRemarksInReportMaster(Connection con, long lRunId, String notes, String status) {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();

			sbQuery	.append("update tblreportmaster set notes = ?, is_active=false, status = ? , runendtime=NOW() WHERE runid = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, notes);
			pstmt.setString(2, status);
			pstmt.setLong(3, lRunId);
			pstmt.execute();
			LogManager.infoLog("tblreportmaster table's notes updated");
			UtilsFactory.clearCollectionHieracy( sbQuery );
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public String getInstanceType(Connection con, int max_users, String osType) {
		
		String instanceType = null;
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rs = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery	.append("SELECT * from vuser_lt_gen_setting where max_vuser_per_type>=? AND os_type=?"); 
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, max_users);
			pstmt.setString(2, osType);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				instanceType = rs.getString("instance_type");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return instanceType;
	}
	
	public JSONArray getDashboardLoadTestData(Connection con, LoginUserBean loginUserBean) throws Exception {
		
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rs = null;
		Date dateLog = LogManager.logMethodStart();

		JSONObject joTest = null;
		JSONArray jaTests = new JSONArray();
		try {
			
			sbQuery	.append("SELECT reporttype, count(*) as totalcount, SUM(CASE WHEN runstarttime>date_trunc('MONTH',now())::DATE AND runendtime<now() THEN 1 ELSE 0 END) AS monthly_count ")
					.append("FROM tblreportmaster ")
					.append("WHERE userid=? ")
					.append("GROUP BY reporttype"); 
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			
			rs = pstmt.executeQuery();
			while(rs.next()){
				joTest = new JSONObject();
				joTest.put("reporttype", rs.getString("reporttype"));
				joTest.put("totalcount", rs.getInt("totalcount"));
				joTest.put("monthcount", rs.getInt("monthly_count"));
				jaTests.add(joTest);
				joTest = null;
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaTests;
	}
	
	public int getCreatedInstanceCount(Connection con, String region, String osType) throws Throwable {
		int nCreatedInstance = 0;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT count(*) as created_instance FROM lt_dynamic_load_agent_details WHERE instance_status <> 'terminated' AND region LIKE '%"+region+"%' AND os_type =? AND public_ip <> ?" );
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, osType);
			pstmt.setString(2, Constants.FG_CONTROLLER_IP);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				nCreatedInstance = rst.getInt("created_instance");
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return nCreatedInstance;
	}
	
	public void insertLogDetails(Connection con, JSONArray ltLogs, long lRunId) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery = new StringBuilder();
			sbQuery.append("insert into lt_log_"+lRunId+" (iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id) ")
					.append("select iteration_id, loadgen, log_id, log_name, message, scenarioname, scriptid, scriptname, log_time, user_id from json_populate_recordset(NULL::lt_log_"+lRunId+",'"+ltLogs+"')");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertLogDetails: "+ltLogs);
		}finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	
	public void insertErrorDetails(Connection con, JSONArray ltErrors, long lRunId) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery.append("insert into lt_error_"+lRunId+" (iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id) ")
					.append("select iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id from json_populate_recordset(NULL::lt_error_"+lRunId+",'"+ltErrors+"')");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertErrorDetails: "+ltErrors);
		}finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	
	public void insertErrorDetails11(Connection con, JSONArray ltErrors, long lRunId) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			System.out.println("Start Time in Parse Insert"+System.currentTimeMillis());
//			LogManager.infoLog("Start insertErrorDetails:: "+startTime);
			
			sbQuery = new StringBuilder();
			sbQuery.append("insert into lt_error_"+lRunId+" (iteration_id, errorcode, source, loadgen, message, request, requestexceptionid, requestid, scenarioname, scriptname, log_time, user_id) ")
					.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			int batchSize = 5000;
			
			for ( int i=0; i<ltErrors.size(); i++ ){
				JSONObject jo = ltErrors.getJSONObject(i);
				pstmt.setLong(1, jo.getLong("iteration_id"));
				pstmt.setInt(2, jo.getInt("errorcode"));
				pstmt.setString(3, jo.getString("source"));
				pstmt.setString(4, jo.getString("loadgen"));
				pstmt.setString(5, jo.getString("message"));
				pstmt.setString(6, jo.getString("request"));
				pstmt.setString(7, jo.getString("requestexceptionid"));
				pstmt.setLong(8, jo.getLong("requestid"));
				pstmt.setString(9, jo.getString("scenarioname"));
				pstmt.setString(10, jo.getString("scriptname"));
				pstmt.setLong(11, jo.getLong("log_time"));
				pstmt.setLong(12, jo.getLong("user_id"));
				pstmt.addBatch();
				
				if (i % batchSize == 0 || i == ltErrors.size() - 1) {
					pstmt.executeBatch();
	            }
			}
			System.out.println("End Time in Parse Insert"+System.currentTimeMillis());
//			long endTime = System.currentTimeMillis();
//			LogManager.infoLog("End insertErrorDetails:: "+endTime);
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			UtilsFactory.printSQLNextExceptions("SQL Exception", e);
		}finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}

	}
	public void insertReportDetails(Connection con, List<ReportDataBean> ltReportDatas, long lRunId, int is_completed, String loadgens) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
//			long startTime = System.currentTimeMillis();
//			LogManager.infoLog("Start insertReportDetails:: "+startTime);
			
			sbQuery = new StringBuilder();
			sbQuery.append("insert into lt_reportdata_temp (loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize, runid) ")
					.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			int batchSize = 5000;
			
			for ( int i=0; i<ltReportDatas.size(); i++ ){
				pstmt.setString(1, ltReportDatas.get(i).getLoadGen());
				pstmt.setString(2, ltReportDatas.get(i).getSourceIp());
				pstmt.setString(3, ltReportDatas.get(i).getLoadgenName());
				pstmt.setString(4, ltReportDatas.get(i).getScenarioName());
				pstmt.setLong(5, ltReportDatas.get(i).getScriptId());
				pstmt.setLong(6, ltReportDatas.get(i).getContainerId());
				pstmt.setString(7, ltReportDatas.get(i).getContainerName());
				pstmt.setLong(8, ltReportDatas.get(i).getPageId());
				pstmt.setLong(9, ltReportDatas.get(i).getRequestId());
				pstmt.setString(10, ltReportDatas.get(i).getAddress());
				pstmt.setLong(11, ltReportDatas.get(i).getUserId());
				pstmt.setLong(12, ltReportDatas.get(i).getIterationId());
				pstmt.setTimestamp(13, ltReportDatas.get(i).getStartTime());
				pstmt.setTimestamp(14, ltReportDatas.get(i).getEndTime());
				pstmt.setDouble(15, ltReportDatas.get(i).getDiff());
				pstmt.setInt(16, ltReportDatas.get(i).getResponseCode());
				pstmt.setInt(17, ltReportDatas.get(i).getResponseSize());
				pstmt.setLong(18, ltReportDatas.get(i).getRunId());
				pstmt.addBatch();

				if (i % batchSize == 0 || i == ltReportDatas.size() - 1) {
					pstmt.executeBatch();
	            }
			}
			
//			long endTime = System.currentTimeMillis();
//			LogManager.infoLog("End insertReportDetails:: "+endTime);
			
			getChartSummary(con, lRunId, is_completed, loadgens);
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		}finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	
	public void insertTransactionDetails(Connection con, JSONArray ltTransactions, long lRunId) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery.append("insert into lt_transactions_"+lRunId+" (script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff) ")
					.append("select script_id, scenarioname, scriptname, userid, iteration_id, transactionname, starttime, endtime, isend, diff from json_populate_recordset(NULL::lt_transactions_"+lRunId+",'"+ltTransactions+"')");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertTransactionDetails: "+ltTransactions);
		}finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	
	
	public void getChartSummary(Connection con, long lRunId, int is_completed, String loadgens) throws Throwable {
		Timestamp startDate = null, endDate = null, tempEndDt = null;
		JSONObject jsonStartDt = null;
		int sample = 0, sampleRate = 5;
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmtDelUser = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			// End Time
			endDate =  new Timestamp(endTimeFromReportData(con, lRunId, is_completed));
			
			if( endDate.getTime() != 0){
				// Start Time
				jsonStartDt = startTimeFromChartSummary(con, lRunId);
				
				if( jsonStartDt == null){
					startDate =  new Timestamp(startTimeFromReportData(con, lRunId));
					startDate = new Timestamp(startDate.getTime());
					
				} else {
					startDate = new Timestamp((long) jsonStartDt.get("starttime") + 1);
					sample = jsonStartDt.getInt("scenariotime");
					
				}
				tempEndDt = new Timestamp(startDate.getTime() + 3999);
				
				String[] loadgen = loadgens.split(",");
				// Values considered when diff is greater than or equal to 5 secs
				while( (endDate.getTime() - startDate.getTime() >= 5000 || is_completed == 1) && endDate.getTime() >= startDate.getTime() ){
					sample = sample + sampleRate;
					for( String load : loadgen){
						insertChartSummaryWithLoadGen(con, lRunId, sample, startDate, tempEndDt, load);
					}
					insertChartSummary(con, lRunId, sample, startDate, tempEndDt);
					startDate = new Timestamp(tempEndDt.getTime() + 1);
					tempEndDt = new Timestamp(startDate.getTime() + 3999);
					
				}
				
//				if( is_completed == 1 ){
//					sbQuery. append("DELETE FROM lt_user_runtime WHERE runid = ?");
//					pstmtDelUser = con.prepareStatement( sbQuery.toString() );
//					pstmtDelUser.setLong(1, lRunId);
//					pstmtDelUser.executeUpdate();
//				}
//				loadgen = null;
			}

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmtDelUser);
			pstmtDelUser = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public JSONObject startTimeFromChartSummary(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmtStartDt = null;
		ResultSet rstStartDt = null;
		StringBuilder sbQuery = new StringBuilder();
		JSONObject js = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery.append("SELECT MAX(scenariotime) AS scenariotime, MAX(sample_to) AS starttime FROM lt_chartsummary_" + lRunId + " WHERE loadgen_name IS NULL");
			pstmtStartDt = con.prepareStatement(sbQuery.toString());
			rstStartDt = pstmtStartDt.executeQuery();

			if (rstStartDt.next()) {
				if (rstStartDt.getTimestamp("starttime") != null) {
					js = new JSONObject();
					js.put("starttime", rstStartDt.getTimestamp("starttime").getTime());
					js.put("scenariotime", rstStartDt.getInt("scenariotime"));
				}
			}

		} catch (Exception e) {
			LogManager.errorLog(e);

			throw e;
		} finally {
			DataBaseManager.close(rstStartDt);
//			rstStartDt = null;
			DataBaseManager.close(pstmtStartDt);
//			pstmtStartDt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return js;
	}
	
	public long startTimeFromReportData(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmtStartDt = null;
		ResultSet rstStartDt = null;
		StringBuilder sbQuery = new StringBuilder();
		long startDate = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery.append("SELECT MIN(runtime)::timestamp(0) AS starttime FROM lt_user_runtime_").append(lRunId).append(" WHERE runtype = 1");
			pstmtStartDt = con.prepareStatement(sbQuery.toString());
			rstStartDt = pstmtStartDt.executeQuery();
			if (rstStartDt.next()) {
				startDate = rstStartDt.getTimestamp("starttime").getTime() - 1*1000;
			}

		} catch (Exception e) {
			LogManager.errorLog(e);

			throw e;
		} finally {
			DataBaseManager.close(rstStartDt);
//			rstStartDt = null;
			DataBaseManager.close(pstmtStartDt);
//			pstmtStartDt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return startDate;
	}
	
	public long endTimeFromReportData(Connection con, long lRunId, int is_completed) throws Throwable {
		PreparedStatement pstmtEndDt = null;
		ResultSet rstEndDt = null;
		StringBuilder sbQuery = new StringBuilder();
		long endDate = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT MAX(endtime) AS endtime FROM lt_reportdata_temp WHERE runid = ?");
			pstmtEndDt = con.prepareStatement(sbQuery.toString());
			pstmtEndDt.setLong(1, lRunId);
			rstEndDt = pstmtEndDt.executeQuery();

			if (rstEndDt.next()) {
				if (rstEndDt.getTimestamp("endtime") != null) {
					// max Value minus 10 secs
					if (is_completed == 1) {
						endDate = rstEndDt.getTimestamp("endtime").getTime() + 5 * 1000;
					} else {
						endDate = rstEndDt.getTimestamp("endtime").getTime() - Integer.valueOf(Constants.DELAY_SAMPLING) * 1000;
					}
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);

			throw e;
		} finally {
			DataBaseManager.close(rstEndDt);
//			rstEndDt = null;
			DataBaseManager.close(pstmtEndDt);
//			pstmtEndDt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return endDate;
	}
	
	public Timestamp insertChartSummary(Connection con, long lRunId, int sample, Timestamp startDt, Timestamp endDt) throws Throwable {
		PreparedStatement pstmt = null, pstmtInsert = null, pstmtReport = null, pstmtDel = null, pstmtDelUser = null;
		ResultSet rstEndDt = null;
		StringBuilder sbQuery = new StringBuilder();
		Timestamp endDate = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("select ((SELECT COUNT(*) FROM lt_user_runtime_" + lRunId + " WHERE runtype = 1 and runtime <=?)-(SELECT COUNT(*) FROM lt_user_runtime_" + lRunId + " WHERE runtype = 2 and runtime<=?)) AS usercount, count(request_id) AS hits, AVG(diff) AS avgrequestresponse,((sum(responsesize)*8.0)/1048576)/5 AS avgthroughput, ").append("(select count(*) from lt_error_" + lRunId + " where (log_time BETWEEN ? AND ?)) as errorcount, SUM(diff)/count(distinct page_id) AS avgpageresponse ")
					.append("FROM lt_reportdata_temp WHERE endtime BETWEEN ? AND ? AND runid = ?");

			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setTimestamp(1, endDt);
			pstmt.setTimestamp(2, endDt);
			pstmt.setTimestamp(3, startDt);
			pstmt.setTimestamp(4, endDt);
			pstmt.setTimestamp(5, startDt);
			pstmt.setTimestamp(6, endDt);
			pstmt.setLong(7, lRunId);
			rstEndDt = pstmt.executeQuery();

			if (rstEndDt.next()) {
				sbQuery = new StringBuilder();
				sbQuery.append("INSERT INTO lt_chartsummary_" + lRunId + " (scenariotime, sample_from, sample_to, usercount, hitcount, avgrequestresponse, avgthroughput, errorcount, avgpageresponse) ").append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
				pstmtInsert = con.prepareStatement(sbQuery.toString());

				pstmtInsert.setInt(1, sample);
				pstmtInsert.setTimestamp(2, startDt);
				pstmtInsert.setTimestamp(3, endDt);
				pstmtInsert.setInt(4, rstEndDt.getInt("usercount"));
				pstmtInsert.setInt(5, rstEndDt.getInt("hits"));
				pstmtInsert.setDouble(6, rstEndDt.getDouble("avgrequestresponse"));
				pstmtInsert.setDouble(7, rstEndDt.getDouble("avgthroughput"));
				pstmtInsert.setInt(8, rstEndDt.getInt("errorcount"));
				pstmtInsert.setDouble(9, rstEndDt.getDouble("avgpageresponse"));
				pstmtInsert.executeUpdate();
			}

			// Insert into ReportData and Delete from Temp
			sbQuery = new StringBuilder();
			sbQuery .append("INSERT INTO lt_reportdata_" + lRunId + " (loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize) ").append(" SELECT loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize FROM lt_reportdata_temp ")
					.append("WHERE endtime BETWEEN ? AND ? AND runid = ?");
			pstmtReport = con.prepareStatement(sbQuery.toString());

			pstmtReport.setTimestamp(1, startDt);
			pstmtReport.setTimestamp(2, endDt);
			pstmtReport.setLong(3, lRunId);
			pstmtReport.executeUpdate();

			sbQuery = new StringBuilder();
			sbQuery.append("DELETE FROM lt_reportdata_temp WHERE endtime BETWEEN ? AND ? AND runid = ?");
			pstmtDel = con.prepareStatement(sbQuery.toString());
			pstmtDel.setTimestamp(1, startDt);
			pstmtDel.setTimestamp(2, endDt);
			pstmtDel.setLong(3, lRunId);
			pstmtDel.executeUpdate();

			sbQuery = new StringBuilder();

			// sbQuery.
			// append("DELETE FROM lt_user_runtime WHERE runtime_id < (select runtime_id from lt_user_runtime  where runtime  <= ? and runid = ? order by 1 desc limit 1)");
			// pstmtDelUser = con.prepareStatement( sbQuery.toString() );
			// pstmtDelUser.setTimestamp(1, endDt);
			// pstmtDelUser.setLong(2, lRunId);
			// pstmtDelUser.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);

			throw e;
		} finally {
			DataBaseManager.close(rstEndDt);
//			rstEndDt = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			DataBaseManager.close(pstmtInsert);
//			pstmtInsert = null;
			DataBaseManager.close(pstmtReport);
//			pstmtReport = null;
			DataBaseManager.close(pstmtDel);
//			pstmtDel = null;
			DataBaseManager.close(pstmtDelUser);
//			pstmtDelUser = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return endDate;
	}
	
	public Timestamp insertChartSummaryWithLoadGen(Connection con, long lRunId, int sample, Timestamp startDt, Timestamp endDt, String loadgen) throws Throwable {
		PreparedStatement pstmt = null, pstmtInsert = null;
		ResultSet rstEndDt = null;
		StringBuilder sbQuery = new StringBuilder();
		Timestamp endDate = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("select ((SELECT COUNT(*) FROM lt_user_runtime_" + lRunId + " WHERE runtype = 1 AND runtime <=? AND loadgen_name = ?)-(SELECT COUNT(*) FROM lt_user_runtime_" + lRunId + " WHERE runtype = 2 AND runtime<=?  AND loadgen_name = ?)) AS usercount,").append("count(request_id) AS hits, AVG(diff) AS avgrequestresponse,((sum(responsesize)*8.0)/1048576)/5 AS avgthroughput, ").append("(select count(*) from lt_error_" + lRunId + " where (log_time BETWEEN ? AND ?)) as errorcount, SUM(diff)/count(distinct page_id) AS avgpageresponse ")
					.append("FROM lt_reportdata_temp WHERE endtime BETWEEN ? AND ? AND loadgen_name = ? AND runid = ?");

			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setTimestamp(1, endDt);
			pstmt.setString(2, loadgen);
			pstmt.setTimestamp(3, endDt);
			pstmt.setString(4, loadgen);
			pstmt.setTimestamp(5, startDt);
			pstmt.setTimestamp(6, endDt);
			pstmt.setTimestamp(7, startDt);
			pstmt.setTimestamp(8, endDt);
			pstmt.setString(9, loadgen);
			pstmt.setLong(10, lRunId);
			rstEndDt = pstmt.executeQuery();

			if (rstEndDt.next()) {
				sbQuery = new StringBuilder();
				sbQuery.append("INSERT INTO lt_chartsummary_" + lRunId + " (scenariotime, sample_from, sample_to, usercount, hitcount, avgrequestresponse, avgthroughput, errorcount, avgpageresponse, loadgen_name) ").append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				pstmtInsert = con.prepareStatement(sbQuery.toString());

				pstmtInsert.setInt(1, sample);
				pstmtInsert.setTimestamp(2, startDt);
				pstmtInsert.setTimestamp(3, endDt);
				pstmtInsert.setInt(4, rstEndDt.getInt("usercount"));
				pstmtInsert.setInt(5, rstEndDt.getInt("hits"));
				pstmtInsert.setDouble(6, rstEndDt.getDouble("avgrequestresponse"));
				pstmtInsert.setDouble(7, rstEndDt.getDouble("avgthroughput"));
				pstmtInsert.setInt(8, rstEndDt.getInt("errorcount"));
				pstmtInsert.setDouble(9, rstEndDt.getDouble("avgpageresponse"));
				pstmtInsert.setString(10, loadgen);
				pstmtInsert.executeUpdate();
			}

		} catch (Exception e) {
			LogManager.errorLog(e);

			throw e;
		} finally {
			DataBaseManager.close(rstEndDt);
//			rstEndDt = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			DataBaseManager.close(pstmtInsert);
//			pstmtInsert = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		return endDate;
	}
	
	public void insertRunTimeCount(Connection con, JSONArray ltUserDetails, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("insert into lt_user_runtime_"+lRunId+" (runtime, runtype, userid, script_id, loadgen_name) ")
					.append("select runtime, runtype, userid, script_id, loadgen_name from json_populate_recordset(NULL::lt_user_runtime_"+lRunId+",'"+ltUserDetails+"')");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.executeUpdate();
		
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertRunTimeCount: "+ltUserDetails);
		}finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateReportMasterLoadGen(Connection con, long lRunId, String loadgen) {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();

			sbQuery	.append("update tblreportmaster set loadgenipaddresses = ? WHERE runid = ? ");
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.setString(1, loadgen);
			// pstmt.setString(2, "RUNNING");
			pstmt.setLong(2, lRunId);
			pstmt.executeUpdate();
			LogManager.infoLog("tblreportmaster table's loadgen updated");
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	public void insertRunScriptDetails(Connection con, long lRunId, HashMap<Long, String> hmap) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("insert into lt_run_script_details (runid, script_id, script_name) ")
					.append("values (?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			for (Map.Entry<Long, String> e: hmap.entrySet()){
				pstmt.setLong(1, lRunId);
				pstmt.setLong(2, e.getKey());
				pstmt.setString(3, e.getValue());
				pstmt.addBatch();
			}
			
			pstmt.executeBatch();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		}finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	public void createSummaryTable(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		long scriptId = 0;
		String scriptName = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT script_id, script_name FROM lt_run_script_details WHERE runid = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				scriptId = rst.getLong("script_id");
				scriptName = rst.getString("script_name");
				createView(con, lRunId, scriptId, scriptName);
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close( pstmt );
//			pstmt = null;
//			scriptName = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	private void createView(Connection con, long lRunId, long scriptId, String scriptName) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT lt_dynamic_tables(?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			pstmt.setLong(2, scriptId);
			pstmt.setString(3, scriptName);
			pstmt.execute();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void ltSummaryReport(Connection con, long lRunId) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null, pstmtInsert = null, pstmtSelect = null;
		ResultSet rst = null, rstSelect = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT (SELECT MIN(runtime) FROM lt_user_runtime_"+lRunId+") AS start_time, ")
					.append("(SELECT MAX(runtime) FROM lt_user_runtime_"+lRunId+") AS end_time, ")
					.append("(SELECT max(runtime) - min(runtime) FROM lt_user_runtime_"+lRunId+") as duration_sec");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				sbQuery = new StringBuilder();
				sbQuery	.append("SELECT '").append(rst.getLong("start_time")).append("' AS start_time, '")
						.append(rst.getLong("end_time")).append("' AS end_time, ")
						.append(rst.getDouble("duration_sec")/1000).append(" as duration_sec,")
						.append("(SELECT COUNT(*) AS user_count FROM lt_user_runtime_"+lRunId+" WHERE runtype=1),")
						.append("count(*) AS total_hits, NULLIF(AVG(diff)*1.0/1000,0) AS avg_response,")
						.append("CASE WHEN ").append(rst.getDouble("duration_sec")/1000).append(" = 0 THEN 0 ")
						.append("ELSE count(*)*1.0/").append(rst.getDouble("duration_sec")/1000).append(" END AS avg_hits,")
						.append("NULLIF(ROUND((SUM(responsesize)*1.0/1024)/1024,3),0) AS total_throughput,")
						.append("CASE WHEN ").append(rst.getDouble("duration_sec")/1000).append(" = 0 THEN 0 ")
						.append("ELSE ((SUM(responsesize)*8.0)/").append(rst.getDouble("duration_sec")/1000).append(")/1024/1024 END AS avg_throughput,")
						.append("(select count(*)from lt_error_"+lRunId+" ) AS total_errors, ")
						.append("(select count(*) from (SELECT page_id from lt_reportdata_"+lRunId+" group by userid,iteration_id,page_id)as total_page )as total_page, ")
						.append("(select NULLIF(AVG(pageresponse)*1.0/1000,0) from (select sum(diff)as pageresponse from lt_reportdata_"+lRunId+"  group by userid,iteration_id,page_id) avg_page_response) AS avg_page_response,")
						.append("(SELECT COUNT(responsecode) FROM lt_reportdata_"+lRunId+" WHERE responsecode>=200 AND responsecode<300 ) AS response_200, ")
						.append("(SELECT COUNT(responsecode) FROM lt_reportdata_"+lRunId+" WHERE responsecode>=300 AND responsecode<400 ) AS response_300, ")
						.append("(SELECT COUNT(responsecode) FROM lt_reportdata_"+lRunId+" WHERE responsecode>=400 AND responsecode<500 ) AS response_400, ")
						.append("(SELECT COUNT(responsecode) FROM lt_reportdata_"+lRunId+" WHERE responsecode>=500 AND responsecode<600 ) AS response_500 FROM lt_reportdata_"+lRunId+"");

				pstmtSelect = con.prepareStatement(sbQuery.toString());
				rstSelect = pstmtSelect.executeQuery();
				if(rstSelect.next()) {
					sbQuery = new StringBuilder();
					sbQuery	.append("INSERT INTO lt_summaryreport_"+lRunId+" (starttime, endtime, durationsec, usercount, totalhits, avgresponse, avghits, totalthroughput, avgthroughput, totalerrors, totalpage, avgpageresponse, response200, response300, response400, response500) ")
							.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					pstmtInsert = con.prepareStatement( sbQuery.toString() );
					
					pstmtInsert.setTimestamp(1, new Timestamp(rstSelect.getLong("start_time")));
					pstmtInsert.setTimestamp(2, new Timestamp(rstSelect.getLong("end_time")));
					pstmtInsert.setDouble(3, rstSelect.getDouble("duration_sec"));
					pstmtInsert.setLong(4, rstSelect.getLong("user_count"));
					pstmtInsert.setLong(5, rstSelect.getLong("total_hits"));
					pstmtInsert.setDouble(6, rstSelect.getDouble("avg_response"));
					pstmtInsert.setDouble(7, rstSelect.getDouble("avg_hits"));
					pstmtInsert.setDouble(8, rstSelect.getDouble("total_throughput"));
					pstmtInsert.setDouble(9, rstSelect.getDouble("avg_throughput"));
					pstmtInsert.setLong(10, rstSelect.getLong("total_errors"));
					pstmtInsert.setLong(11, rstSelect.getLong("total_page"));
					pstmtInsert.setDouble(12, rstSelect.getDouble("avg_page_response"));
					pstmtInsert.setLong(13, rstSelect.getLong("response_200"));
					pstmtInsert.setLong(14, rstSelect.getLong("response_300"));
					pstmtInsert.setLong(15, rstSelect.getLong("response_400"));
					pstmtInsert.setLong(16, rstSelect.getLong("response_500"));
					pstmtInsert.executeUpdate();
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(rstSelect);
//			rstSelect = null;
			DataBaseManager.close( pstmt );
//			pstmt = null;
			DataBaseManager.close( pstmtSelect );
//			pstmtSelect = null;
			DataBaseManager.close( pstmtInsert );
//			pstmtInsert = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void ltReportDataTemp(Connection con, long lRunId) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null, pstmtSelChart = null, pstmtSelect = null;
		ResultSet rst = null, rstSelect = null, rstSelChart = null;
		Timestamp startTime = null, endTime = null;
		String loadGenName = null;
//		boolean isFirst = true;
		long chartId = 0, totalCount = -1;
		boolean isBreak;
		Date dateLog = LogManager.logMethodStart();

		try {
			while (totalCount != 0){
				sbQuery = new StringBuilder();
				sbQuery	.append("SELECT COUNT(*) AS tot_count FROM lt_reportdata_temp WHERE runid = ?");
				
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setLong(1, lRunId);
				rst = pstmt.executeQuery();
				isBreak = true;
				if(rst.next()) {
					totalCount = rst.getLong("tot_count");
					if( totalCount > 0){
						sbQuery = new StringBuilder();
						sbQuery	.append("SELECT MIN(endtime) AS endTime FROM lt_reportdata_temp WHERE runid = ?");

						pstmtSelect = con.prepareStatement(sbQuery.toString());
						pstmtSelect.setLong(1, lRunId);
						rstSelect = pstmtSelect.executeQuery();
						if(rstSelect.next()) {
							sbQuery = new StringBuilder();
							sbQuery	.append("SELECT * FROM lt_chartsummary_").append(lRunId)
									.append(" WHERE ? BETWEEN sample_from AND sample_to ORDER BY loadgen_name DESC");
							pstmtSelChart = con.prepareStatement( sbQuery.toString() );
							pstmtSelChart.setTimestamp(1, rstSelect.getTimestamp("endTime"));
							rstSelChart = pstmtSelChart.executeQuery();
							while(rstSelChart.next()){
								isBreak = false;
								startTime = rstSelChart.getTimestamp("sample_from");
								endTime = rstSelChart.getTimestamp("sample_to");
								loadGenName = rstSelChart.getString("loadgen_name");
								chartId = rstSelChart.getLong("chart_id");
								// From reportdata temp to report_data_runid
								// if( isFirst ){
								insertRemainingSampling(con, lRunId, startTime, endTime);
								//	isFirst = false;
								chartSummaryUpdate(con, lRunId, chartId, startTime, endTime, loadGenName);
								//} else {
								//	chartSummaryUpdate(con, lRunId, chartId, startTime, endTime, loadGenName);
								// }
							}
							if(isBreak){
								try {
//									insertReportData(con, lRunId);
								} catch (Exception e) {
									LogManager.errorLog(e);
								}
								LogManager.infoLog("Manual Break Count:: "+ totalCount);
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(rstSelect);
//			rstSelect = null;
			DataBaseManager.close(rstSelChart);
//			rstSelChart = null;
			DataBaseManager.close( pstmt );
//			pstmt = null;
			DataBaseManager.close( pstmtSelect );
//			pstmtSelect = null;
			DataBaseManager.close( pstmtSelChart );
//			pstmtSelChart = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
//			startTime = null;
//			endTime = null;
//			loadGenName = null;
		}
	}
	
	public void insertReportData (Connection con, JSONArray ltReportDatas, long lRunId) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmtReport = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery .append("INSERT INTO lt_reportdata_" + lRunId + " (loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize) ")
					.append("SELECT loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize FROM json_populate_recordset(NULL::lt_reportdata_" + lRunId + ",'"+ltReportDatas+"')");
			pstmtReport = con.prepareStatement(sbQuery.toString());
			pstmtReport.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertReportData: "+ltReportDatas);
		} finally{
			DataBaseManager.close(pstmtReport);
			pstmtReport = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	private void insertRemainingSampling(Connection con, long lRunId, Timestamp startDt, Timestamp endDt) throws Throwable{
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmtReport = null, pstmtDel = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuilder();
			sbQuery	.append("INSERT INTO lt_reportdata_"+lRunId+" (loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize) ")
					.append(" SELECT loadgen, source_ip, loadgen_name, scenarioname, script_id, container_id, container_name, page_id, request_id, address, userid, iteration_id, starttime, endtime, diff, responsecode, responsesize FROM lt_reportdata_temp ")
					.append("WHERE endtime BETWEEN ? AND ? AND runid = ?");
			pstmtReport = con.prepareStatement( sbQuery.toString() );
			
			pstmtReport.setTimestamp(1, startDt);
			pstmtReport.setTimestamp(2, endDt);
			pstmtReport.setLong(3, lRunId);
			pstmtReport.executeUpdate();	
			
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_reportdata_temp WHERE endtime BETWEEN ? AND ? AND runid = ?");
			pstmtDel = con.prepareStatement( sbQuery.toString() );
			pstmtDel.setTimestamp(1, startDt);
			pstmtDel.setTimestamp(2, endDt);
			pstmtDel.setLong(3, lRunId);
			pstmtDel.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmtReport );
//			pstmtReport = null;
			DataBaseManager.close( pstmtDel );
//			pstmtDel = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	private void chartSummaryUpdate(Connection con, long lRunId, long chartId, Timestamp startDt, Timestamp endDt, String loadgen) throws Throwable{
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null, pstmtUpdate = null;
		ResultSet rstEndDt = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			if( loadgen !=null ){
				sbQuery .append("select ((SELECT COUNT(*) FROM lt_user_runtime_"+lRunId+" WHERE runtype = 1 AND runtime <=? AND loadgen_name = ?)-(SELECT COUNT(*) FROM lt_user_runtime_"+lRunId+" WHERE runtype = 2 AND runtime<=?  AND loadgen_name = ?)) AS usercount,")
						.append("count(request_id) AS hits, AVG(diff) AS avgrequestresponse,((sum(responsesize)*8.0)/1048576)/5 AS avgthroughput, ")
						.append("(select count(*) from lt_error_"+lRunId+" where (log_time BETWEEN ? AND ?)) as errorcount, SUM(diff)/count(distinct page_id) AS avgpageresponse ")
						.append("FROM lt_reportdata_"+lRunId+" WHERE endtime BETWEEN ? AND ? AND loadgen_name = ?");
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setTimestamp(1, endDt);
				pstmt.setString(2, loadgen);
				pstmt.setTimestamp(3, endDt);
				pstmt.setString(4, loadgen);
				pstmt.setTimestamp(5, startDt);
				pstmt.setTimestamp(6, endDt);
				pstmt.setTimestamp(7, startDt);
				pstmt.setTimestamp(8, endDt);
				pstmt.setString(9, loadgen);
			} else{
				sbQuery .append("select ((SELECT COUNT(*) FROM lt_user_runtime_"+lRunId+" WHERE runtype = 1 and runtime <=?)-(SELECT COUNT(*) FROM lt_user_runtime_"+lRunId+" WHERE runtype = 2 and runtime<=?)) AS usercount, count(request_id) AS hits, AVG(diff) AS avgrequestresponse,((sum(responsesize)*8.0)/1048576)/5 AS avgthroughput, ")
						.append("(select count(*) from lt_error_"+lRunId+" where (log_time BETWEEN ? AND ?)) as errorcount, SUM(diff)/count(distinct page_id) AS avgpageresponse ")
						.append("FROM lt_reportdata_"+lRunId+" WHERE endtime BETWEEN ? AND ?");
				pstmt = con.prepareStatement(sbQuery.toString());
				pstmt.setTimestamp(1, endDt);
				pstmt.setTimestamp(2, endDt);
				pstmt.setTimestamp(3, startDt);
				pstmt.setTimestamp(4, endDt);
				pstmt.setTimestamp(5, startDt);
				pstmt.setTimestamp(6, endDt);
			}
			rstEndDt = pstmt.executeQuery();
			
			if(rstEndDt.next()) {
				sbQuery = new StringBuilder();
				sbQuery	.append("UPDATE lt_chartsummary_"+lRunId+" SET usercount = ?, hitcount = ?, avgrequestresponse = ?, avgthroughput = ?, errorcount = ?, avgpageresponse = ? WHERE chart_id = ? ");
				pstmtUpdate = con.prepareStatement( sbQuery.toString() );
				
				pstmtUpdate.setInt(1, rstEndDt.getInt("usercount"));
				pstmtUpdate.setInt(2, rstEndDt.getInt("hits"));
				pstmtUpdate.setDouble(3, rstEndDt.getDouble("avgrequestresponse"));
				pstmtUpdate.setDouble(4, rstEndDt.getDouble("avgthroughput"));
				pstmtUpdate.setInt(5, rstEndDt.getInt("errorcount"));
				pstmtUpdate.setDouble(6, rstEndDt.getDouble("avgpageresponse"));
				pstmtUpdate.setLong(7, chartId);
				pstmtUpdate.executeUpdate();
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rstEndDt);
//			rstEndDt = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			DataBaseManager.close(pstmtUpdate);
//			pstmtUpdate = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public int getEnterpriseId(Connection con, long lUserId) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		int enterpriseId = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT enterprise_id FROM usermaster WHERE user_id=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lUserId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				enterpriseId = rst.getInt("enterprise_id");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( rst );
//			rst = null;
			DataBaseManager.close( pstmt );
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return enterpriseId;
	}
	
	public void insertAgentMapping(Connection con, String guids, long runId) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("INSERT INTO lt_run_agent_mapping (lt_run_id, guid) VALUES (?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			for ( String guid:guids.split(",")){
				pstmt.setLong(1, runId);
				pstmt.setString(2, guid);	
				pstmt.addBatch();
			}
			
			pstmt.executeBatch();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void deleteScript(Connection con, String strScriptId, LoginUserBean loginUserBean, String scriptName) throws Exception {
		
		PreparedStatement pstmtDel = null, pstmtScript = null;
		StringBuilder sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try{
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_scenario_script_mapping WHERE script_id = ?");
			pstmtDel = con.prepareStatement( sbQuery.toString() );
			pstmtDel.setLong(1, Long.valueOf(strScriptId));
			pstmtDel.executeUpdate();
			
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_script_master WHERE script_id = ? AND script_name = ? AND user_id = ?");
			pstmtScript = con.prepareStatement( sbQuery.toString() );
			pstmtScript.setLong(1, Long.valueOf(strScriptId));
			pstmtScript.setString(2, scriptName);
			pstmtScript.setLong(3, loginUserBean.getUserId());
			pstmtScript.executeUpdate();
			
		}catch(Exception e){
			LogManager.errorLog(e);
			e.printStackTrace();
		}finally{
			DataBaseManager.close(pstmtDel);
//			pstmtDel = null;
			DataBaseManager.close(pstmtScript);
//			pstmtScript = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void deleteJmeterScript(Connection con, String scenarioName, LoginUserBean loginUserBean, String[] scriptNames, String strTestType, Long scenarioId) throws Exception {
		
		PreparedStatement pstmtDel = null, pstmtScript = null, pstmtDelScript = null;
		StringBuilder sbQuery = null;
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();

		try{
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_scenario_script_mapping WHERE scenario_id = ? AND created_by = ?");
			pstmtDel = con.prepareStatement( sbQuery.toString() );
			pstmtDel.setLong(1, scenarioId);
			pstmtDel.setLong(2, loginUserBean.getUserId());
			pstmtDel.executeUpdate();
			
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_scenario_master WHERE scenario_id = ? AND scenario_name = ? AND user_id = ?");
			pstmtScript = con.prepareStatement( sbQuery.toString() );
			pstmtScript.setLong(1, scenarioId);
			pstmtScript.setString(2, scenarioName);
			pstmtScript.setLong(3, loginUserBean.getUserId());
			pstmtScript.executeUpdate();
			
			sbQuery = new StringBuilder();
			sbQuery. append("DELETE FROM lt_script_master WHERE script_name = ? AND user_id = ? AND script_type = ?");
			pstmtDelScript = con.prepareStatement( sbQuery.toString() );
			for(int j=0; j<scriptNames.length; j++){
				pstmtDelScript.setString(1, scriptNames[j]);
				pstmtDelScript.setLong(2, loginUserBean.getUserId());
				pstmtDelScript.setString(3, strTestType);
				pstmtDelScript.executeUpdate();
			}
			
		}catch(Exception e){
			LogManager.errorLog(e);
			e.printStackTrace();
		}finally{
			DataBaseManager.close( rst );
			rst = null;
			DataBaseManager.close(pstmtDel);
			pstmtDel = null;
			DataBaseManager.close(pstmtScript);
			pstmtScript = null;
			DataBaseManager.close(pstmtDelScript);
			pstmtDelScript = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public JSONArray getScriptDetails(Connection con, long scenarioId) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		JSONObject joScript = null;
		JSONArray jaScriptDetails = new JSONArray();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("SELECT sc.script_id AS scriptId, sc.script_name AS scriptName FROM lt_scenario_script_mapping sm ")
					.append("INNER JOIN lt_script_master sc ON sc.script_id = sm.script_id AND sm.created_by = sc.user_id ")
					.append("WHERE scenario_id = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scenarioId);
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				joScript = new JSONObject();
				joScript.put("scriptId", rst.getLong("scriptId"));
				joScript.put("scriptName", rst.getString("scriptName"));
				jaScriptDetails.add(joScript);
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( rst );
			rst = null;
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaScriptDetails;
	}
	
	public void updatePrivateCounters(Connection con, String columnName, long queueValue ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("UPDATE appedo_pvt_counters SET "+columnName+"=?, lt_exec_status = true");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1,  queueValue);
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void insertLTJsonDetails(Connection con, String strQuery) throws Throwable{
		PreparedStatement pstmt = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			pstmt = con.prepareStatement(strQuery);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("Error in inserting insertLTJsonDetails: "+strQuery);
		}finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			LogManager.logMethodEnd(dateLog);
		}
		
	}
	
	public void updateProcessServiceFailed(Connection con, String status, long runId) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("update tblreportmaster set is_active=false, status='FAILED', runendtime=NOW(), notes=? where runid = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, status);
			pstmt.setLong(2, runId);
			
			pstmt.execute();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public Long scenarioReferenceFromReportMaster (Connection con, long scenarioId, long userId) throws Throwable {
		long completedRuns = 0;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("SELECT count(runid) AS total_runs FROM tblreportmaster WHERE scenario_id = ? AND userid = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scenarioId);
			pstmt.setLong(2, userId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				completedRuns = rst.getLong("total_runs");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return completedRuns;
	}
	
	public void registerMachineId(Connection con, long userId, String machineId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("UPDATE usermaster SET unique_machine_id = ? WHERE user_id = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, machineId);
			pstmt.setLong(2, userId);
			pstmt.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void deRegisterMachineId(Connection con, long userId, String machineId) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("UPDATE usermaster SET unique_machine_id = NULL WHERE user_id = ? AND unique_machine_id = ?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, userId);
			pstmt.setString(2, machineId);
			pstmt.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
}
