package com.appedo.lt.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.bean.LoginUserBean;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LTLicenseBean;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LTDBI {

	/**
	 * Gets SUM tests done
	 * 
	 * @param con
	 * @param loginUserBean
	 * @param strTestType 
	 * @return
	 * @throws Exception
	 */
	public JSONArray getVUScripts(Connection con, LoginUserBean loginUserBean, String strTestType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;

		String strQuery = "";

		JSONArray jaScripts = new JSONArray();
		JSONObject joScript = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT * FROM lt_script_master WHERE user_id = ? AND script_type = ?";
			
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, loginUserBean.getUserId());
			pstmt.setString(2, strTestType);
			rst = pstmt.executeQuery();
			while (rst.next()) {
				joScript = new JSONObject();
				
				joScript.put("scriptName", rst.getString("script_name"));
				joScript.put("created_by", loginUserBean.getFirstName());
				joScript.put("created_on", rst.getTimestamp("created_on").getTime());
				joScript.put("type", strTestType);
				joScript.put("script_id", rst.getLong("script_id"));
				
				joScript.put("status", getScriptStatus(con, rst.getLong("script_id")));
				jaScripts.add(joScript);
				
				joScript = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return jaScripts;
	}
	
	public HashMap<Integer, String> getScenarioNames(Connection con, long scriptId, long userId) throws Exception {
		HashMap<Integer, String> hmScenarioNames = null;
		String scenarioName = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		boolean isFirst = true;
		int mappingScenarios = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			hmScenarioNames = new HashMap<Integer, String>();
			sbQuery .append("SELECT scenario_name FROM lt_scenario_master sm LEFT JOIN lt_scenario_script_mapping ssm ON ")
					.append("sm.scenario_id = ssm.scenario_id AND sm.user_id = ssm.created_by WHERE ssm.script_id = ? AND ssm.created_by = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scriptId);
			pstmt.setLong(2, userId);
			rst = pstmt.executeQuery();
			while (rst.next()) {
				mappingScenarios++;
				if(isFirst){
					scenarioName = rst.getString("scenario_name");
					isFirst = false;
				} else {
					scenarioName = scenarioName + "," + rst.getString("scenario_name");	
				}
			}
			if( scenarioName != null){
				hmScenarioNames.put(mappingScenarios, scenarioName);
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
//			scenarioName = null;
			LogManager.logMethodEnd(dateLog);
		}
		return hmScenarioNames;
	}
	
	private String getScriptStatus(Connection con, long scriptId) throws Exception {
		String status = "";
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("SELECT is_completed FROM scriptwise_status WHERE script_id = ? ORDER BY runid DESC LIMIT 1");
	
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scriptId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				if( rst.getInt("is_completed") == 0 ){
					status = "RUNNING";
				} else {
					status = "COMPLETED";
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return status;
	}
	
	public JSONArray getVUScenarios(Connection con, LoginUserBean loginUserBean, String strTestType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String strQuery = "";

		JSONArray jaScenarios = new JSONArray();
		JSONObject joScenario = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT * FROM lt_scenario_master WHERE user_id = ?";
			
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, loginUserBean.getUserId());
			rst = pstmt.executeQuery();
			while (rst.next()) {
				joScenario = new JSONObject();
				joScenario.put("scenarioName", rst.getString("scenario_name"));
				joScenario.put("created_by", loginUserBean.getFirstName());
				joScenario.put("created_on", rst.getTimestamp("created_on").getTime());
				joScenario.put("type", strTestType);
				joScenario.put("virtual_users", rst.getLong("v_users"));
				joScenario.put("scenario_id", rst.getLong("scenario_id"));
				
				jaScenarios.add(joScenario);
				
				joScenario = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return jaScenarios;
	}
	
	public long getMappedScripts(Connection con, long scenarioId, long userId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		long mappedScripts = 0;
		String strQuery = "";
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT count(*) AS mappedScenarios FROM lt_scenario_script_mapping WHERE scenario_id = ? AND created_by = ?";
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, scenarioId);
			pstmt.setLong(2, userId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				mappedScripts = rst.getLong("mappedScenarios");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return mappedScripts;
	}
	
	public HashMap<Long, Boolean> getScenarioRunStatus(Connection con, long scenarioId, long userId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		HashMap<Long, Boolean> hmScenarioStatus = new HashMap<Long, Boolean>();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("SELECT runid, is_active FROM tblreportmaster WHERE scenario_id = ? AND userid = ? ORDER BY runid DESC LIMIT 1");
	
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scenarioId);
			pstmt.setLong(2, userId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				hmScenarioStatus.put(rst.getLong("runid"), rst.getBoolean("is_active"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return hmScenarioStatus;
	}
	
	public Long getCompletedRuns(Connection con, long scenarioId, long userId) throws Exception {
		long completedRuns = 0;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery .append("SELECT count(*) AS total_runs FROM tblreportmaster WHERE scenario_id = ? AND userid = ? AND is_active = false AND status = ?");
	
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, scenarioId);
			pstmt.setLong(2, userId);
			pstmt.setString(3, "COMPLETED");
			rst = pstmt.executeQuery();
			if (rst.next()) {
				completedRuns = rst.getLong("total_runs");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return completedRuns;
	}
	
	public JSONArray getRunningScriptData(Connection con, long runId) throws Exception{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		JSONArray jaRunningScripts = new JSONArray();
		JSONObject joScripts = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("select * from scriptwise_status where runid=?")
					.append(" ORDER BY script_name");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, runId);
			
			rst = pstmt.executeQuery();
			while(rst.next()){
				int err400 = 0, err500 = 0, errCnt = 0, totErr = 0;
				joScripts = new JSONObject();
				joScripts.put("scriptName", rst.getString("script_name"));
				joScripts.put("createdUser", rst.getInt("created_users"));
				joScripts.put("completedUser", rst.getInt("completed_users"));
				joScripts.put("http200Status", rst.getInt("http_200_count"));
				joScripts.put("http300Status", rst.getInt("http_300_count"));
				err400 = rst.getInt("http_400_count");
				err500 = rst.getInt("http_500_count");
				errCnt = rst.getInt("error_count");
				totErr = errCnt - (err400 + err500);
				joScripts.put("http400Status", err400);
				joScripts.put("http500Status", err500);
				joScripts.put("httpOthStatus", totErr);
				joScripts.put("errorCount", errCnt);
				joScripts.put("iscompleted", rst.getInt("is_completed"));
				
				jaRunningScripts.add(joScripts);
//				joScripts = null;
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		return jaRunningScripts;
	}
	
	public JSONArray getReports(Connection con, LoginUserBean loginUserBean) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuilder sbQuery = new StringBuilder();
		JSONObject joReports = null;
		JSONArray jaReports = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT tr.reporttype, tr.runid, tr.reportname, tr.scenarioname, tr.runstarttime, tr.runendtime, tr.status,")
					.append("COALESCE(sum(ss.created_users),0) AS createduser, COALESCE(sum(ss.completed_users),0) AS completeduser ")
					.append("FROM tblreportmaster tr ")
					.append("LEFT join scriptwise_status ss on tr.runid = ss.runid ")
					.append("WHERE tr.userid = ? AND tr.is_deleted = FALSE ")
					.append("GROUP BY tr.runid, tr.reportname,tr.scenarioname, tr.runstarttime, tr.runendtime, tr.is_deleted, tr.reporttype ")
					.append("ORDER BY runendtime DESC");
			
			jaReports =  new JSONArray();
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				joReports = new JSONObject();
				joReports.put("reportType", rs.getString("reporttype"));
				joReports.put("reportId",rs.getString("runid"));
				joReports.put("reportName",rs.getString("reportname"));
				joReports.put("scenarioName",rs.getString("scenarioname"));
				joReports.put("createdUser",rs.getString("createduser"));
				joReports.put("completedUser",rs.getString("completeduser"));
				joReports.put("runStartTime", rs.getTimestamp("runstarttime").getTime());
				joReports.put("status", rs.getString("status"));
				if( rs.getTimestamp("runendtime") != null ){
					joReports.put("runEndTime", rs.getTimestamp("runendtime").getTime());
					long runTime = rs.getTimestamp("runendtime").getTime() - rs.getTimestamp("runstarttime").getTime();
					joReports.put("runTime", runTime);
				} else {
					joReports.put("runEndTime", 0);
					joReports.put("runTime", 0);
				}
				
				jaReports.add(joReports);
//				joReports = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		
	 	return jaReports;
	}
	
	public JSONObject getRunDetails(Connection con, long runId, long userId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String strQuery = "";
		JSONObject joRunDetail = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT * FROM tblreportmaster WHERE runid = ? AND userid = ?";
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, runId);
			pstmt.setLong(2, userId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				joRunDetail = new JSONObject();
				joRunDetail.put("scenarioName", rst.getString("scenarioname"));
				joRunDetail.put("reportName", rst.getString("reportname"));
				joRunDetail.put("runStartTime", rst.getTimestamp("runstarttime").getTime());
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return joRunDetail;
	}
	
	public JSONObject getRunStatus(Connection con, long runId, JSONObject joRunDetails) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String strQuery = "";
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT SUM(created_users) AS createdUser, SUM(completed_users) AS completedUser FROM scriptwise_status WHERE runid = ?";
			pstmt = con.prepareStatement(strQuery);
			pstmt.setLong(1, runId);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				joRunDetails.put("createdUser", rst.getLong("createdUser"));
				joRunDetails.put("completedUser", rst.getLong("completedUser"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return joRunDetails;
	}
	
	public JSONArray getLogReports(Connection con, long runId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuilder sbQuery = new StringBuilder();
		JSONObject joReports = null;
		JSONArray jaReports = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT * FROM lt_log_"+runId);
			
			jaReports =  new JSONArray();
			pstmt = con.prepareStatement(sbQuery.toString());
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				joReports = new JSONObject();
				joReports.put("iteration_id", rs.getLong("iteration_id"));
				joReports.put("loadgen", rs.getString("loadgen"));
				joReports.put("log_id", rs.getLong("log_id"));
				joReports.put("log_name", rs.getString("log_name"));
				joReports.put("message",rs.getString("message"));
				joReports.put("log_time", rs.getTimestamp("log_time").getTime());
				joReports.put("user_id", rs.getLong("user_id"));
				
				jaReports.add(joReports);
//				joReports = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		
	 	return jaReports;
	}
	
	public JSONArray getErrorReports(Connection con, long runId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuilder sbQuery = new StringBuilder();
		JSONObject joReports = null;
		JSONArray jaReports = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT * FROM lt_error_"+runId);
			
			jaReports =  new JSONArray();
			pstmt = con.prepareStatement(sbQuery.toString());
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				joReports = new JSONObject();
				joReports.put("iteration_id", rs.getLong("iteration_id"));
				joReports.put("loadgen", rs.getString("loadgen"));
				joReports.put("requestid", rs.getLong("requestid"));
				joReports.put("errorcode", rs.getInt("errorcode"));
				joReports.put("message",rs.getString("message"));
				joReports.put("log_time", rs.getTimestamp("log_time").getTime());
				joReports.put("user_id", rs.getLong("user_id"));
				
				jaReports.add(joReports);
//				joReports = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
			LogManager.logMethodEnd(dateLog);
		}
		
	 	return jaReports;
	}
	
	public JSONArray getRegions(Connection con, LoginUserBean loginUserBean, String osType) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		JSONObject joRegion = null;
		JSONArray jaRegions = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			jaRegions =  new JSONArray();
			
			sbQuery	.append("SELECT * FROM lt_region_details ")
					.append("WHERE is_deleted = FALSE AND ")
					.append("reservation <> total_availability ")
					.append("AND os_type ='"+osType+"' ")
					.append("ORDER BY created_on DESC ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				joRegion = new JSONObject();
				joRegion.put("lt_region_id",rs.getString("lt_region_id"));
				joRegion.put("instance_owner",rs.getString("instance_owner"));
				joRegion.put("region",rs.getString("region"));
				joRegion.put("location",rs.getBoolean("location"));
				joRegion.put("image_id",rs.getBoolean("image_id"));
				joRegion.put("is_deleted",rs.getBoolean("is_deleted"));
				if(rs.getBoolean("is_default")){
					joRegion.put("showRegion", true);
					joRegion.put("distribution", 100);
				}else{
					joRegion.put("showRegion", false);
					joRegion.put("distribution", 0);
				}
				if(loginUserBean.getLicense().equals("level0")){
					joRegion.put("license", true);
				}else{
					joRegion.put("license", false);
				}
				
				if(rs.getString("os_type").equals("FEDORA")){
					joRegion.put("showRadio", true);
				}else{
					joRegion.put("showRadio", false);
				}
				joRegion.put("userLicense", loginUserBean.getLicense());
				jaRegions.add(joRegion);
//				joRegion = null;
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rs);
//			rs = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
//			joRegion = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	 	return jaRegions;
		
	}
	
	public JSONObject getSummaryReports(Connection con, long runId) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String strQuery = "";
		JSONObject joRunDetail = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			strQuery = "SELECT * FROM lt_summaryreport_"+runId;
			pstmt = con.prepareStatement(strQuery);
			rst = pstmt.executeQuery();
			if (rst.next()) {
				joRunDetail = new JSONObject();
				joRunDetail.put("starttime", rst.getTimestamp("starttime").getTime());
				joRunDetail.put("endtime", rst.getTimestamp("endtime").getTime());
				joRunDetail.put("durationsec", rst.getString("durationsec"));
				joRunDetail.put("usercount", rst.getInt("usercount"));
				joRunDetail.put("totalhits", rst.getInt("totalhits"));
				joRunDetail.put("avgresponse", rst.getDouble("avgresponse"));
				joRunDetail.put("avghits", rst.getDouble("avghits"));
				joRunDetail.put("totalthroughput", rst.getDouble("totalthroughput"));
				joRunDetail.put("avgthroughput", rst.getDouble("avgthroughput"));
				joRunDetail.put("totalerrors", rst.getInt("totalerrors"));
				joRunDetail.put("totalpage", rst.getInt("totalpage"));
				joRunDetail.put("avgpageresponse", rst.getDouble("avgpageresponse"));
				joRunDetail.put("response200", rst.getInt("response200"));
				joRunDetail.put("response300", rst.getInt("response300"));
				joRunDetail.put("response400", rst.getInt("response400"));
				joRunDetail.put("response500", rst.getInt("response500"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
//			rst = null;
			DataBaseManager.close(pstmt);
//			pstmt = null;
			
//			strQuery = null;
			LogManager.logMethodEnd(dateLog);
		}
		return joRunDetail;
	}
	
	public LTLicenseBean getLTUserWiseLicenseMonthWise(Connection con, LoginUserBean loginUserBean) throws Exception {
		LTLicenseBean ltLicBean = null;
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			if( ! loginUserBean.getLicense().equals("level0") ) {
				// For PAID user
				sbQuery	.append("SELECT user_id, start_date, end_date, lt_max_vusers ")
						.append("FROM userwise_lic_monthwise ")
						.append("WHERE user_id = ? AND module_type = 'LT' ")
						.append("AND start_date::date <= now()::date AND end_date::date >= now()::date ");
			} else {
				// For FREE user
				sbQuery	.append("SELECT user_id, created_on AS start_date, lt_max_vusers ")
						.append("FROM usermaster ")
						.append("WHERE user_id = ? ");
			}
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, loginUserBean.getUserId());
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				ltLicBean = new LTLicenseBean();
				
				ltLicBean.setUserId(rst.getLong("user_id"));
				ltLicBean.setStartDate(rst.getString("start_date"));
				if( ! loginUserBean.getLicense().equals("level0") ) 
					ltLicBean.setEndDate(rst.getString("end_date"));
				ltLicBean.setMaxVusers(rst.getInt("lt_max_vusers"));
			}
		}catch (Exception e) {
			LogManager.infoLog(sbQuery.toString());
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			sbQuery= null;
			LogManager.logMethodEnd(dateLog);
		}
		return ltLicBean;
	}
	
	public LTLicenseBean getLTLicenseConfigParameters(Connection con, LoginUserBean loginUserBean) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		LTLicenseBean ltLicBean = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT * ")
					.append("FROM lt_config_parameters ")
					.append("WHERE lt_license = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, loginUserBean.getLicense());
			rst = pstmt.executeQuery();
			if( rst.next() ) {
				ltLicBean = new  LTLicenseBean();
				
				ltLicBean.setLicInternalName(rst.getString("lt_license"));
				ltLicBean.setLicExternalName(rst.getString("lic_external_name"));
				ltLicBean.setMaxIterations(rst.getInt("max_iterations"));
				ltLicBean.setMaxRuns(rst.getInt("max_runs"));
				ltLicBean.setMaxDuration(rst.getInt("max_duration"));
				ltLicBean.setReportRetentionPeriod(rst.getInt("report_retention_period"));
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			sbQuery= null;
			LogManager.logMethodEnd(dateLog);
		}
		
		return ltLicBean;
	}
}
