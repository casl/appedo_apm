package com.appedo.lt.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.appedo.lt.bean.LoadTestBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;



/**
 * To export and validate  jmeter script , parameter files.
 * @author veeru
 *
 */
public class JmeterScriptManager {
	

	 //static String UPLOAD_URL = "http://192.168.1.16:8080/jmeteragent/RunJmx";
	static String UPLOAD_URL = "";
	 static final int BUFFER_SIZE = 4096;
	 StringBuilder sb=new StringBuilder();
	 FileInputStream inputStream = null;
	 OutputStream outputStream = null;
	 BufferedReader reader = null;
	 
	/**
	 * Veeru added, think due version problem below throwing some java.lang.AbstractException
	 * Extracts file name from HTTP header content-disposition
	 * 
	 * @param part
	 * @return
	 */
    /*public String extractFileName(Part part) {
    	System.out.println("Part :"+part);
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }*/
    
    /**
	 * To add the jmx script name  to vuscripts.xml
	 * @param strScriptName
	 * @throws Throwable
	 */
	public void addScript(String strScriptName, String strJmeterScriptPath) throws Throwable {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		RunManager rm = new RunManager();
		try{
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strJmeterScriptPath)));
			
			String strXml = FileUtils.readFileToString(new File(strJmeterScriptPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			XPath varxPath =  XPathFactory.newInstance().newXPath();
			String expression = "/vuscripts/vuscript[@name='"+strScriptName+"']";
			NodeList recfiletypeList = (NodeList) varxPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			LogManager.infoLog("VUS size="+recfiletypeList.getLength());
			int mnum = recfiletypeList.getLength();
			if(mnum>0){
				Element mnode = (Element) recfiletypeList.item(0);
				System.out.println(mnode);
				mnode.getParentNode().removeChild(mnode);
				rm.saveXmlDoc(doc, strJmeterScriptPath);
				LogManager.infoLog(mnode.getNodeName());
			}
			
			Node rootElement = doc.getElementsByTagName("vuscripts").item(0);
			// vuscript elements
			Element vuscript = doc.createElement("vuscript");
			rootElement.appendChild(vuscript);
			
			// set attributes to element
			vuscript.setAttribute("dynamicreqenable", "False");
			vuscript.setAttribute("exclutionfiletypes", "");
			long currentMillis = System.currentTimeMillis();
			vuscript.setAttribute("id", String.valueOf(currentMillis).trim());
			vuscript.setAttribute("name", strScriptName);
			vuscript.setAttribute("type", "http");
			Calendar cal = Calendar.getInstance();			
			vuscript.setAttribute("receivedon", dateFormat.format(cal.getTime()).toString());
			saveXmlDoc(doc, strJmeterScriptPath);
			
//			rm = null;
		}catch(Throwable t){
			LogManager.errorLog(t);
			throw t;
		}
	}
	
	/**
	 *  to create the xml file in the  given path 
	 * @param mdoc
	 * @throws Throwable
	 */
	public void saveXmlDoc(Document mdoc, String strJmeterScriptPath)throws Throwable {
		
		try	{
			 /** write the content into xml file */
			 TransformerFactory transformerFactory = TransformerFactory.newInstance();
			 Transformer transformer = transformerFactory.newTransformer();
			 DOMSource source = new DOMSource(mdoc);
			 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
			 /** Output to console for testing */
			 // StreamResult result = new StreamResult(System.out);
			 transformer.transform(source, result);
			 LogManager.infoLog("File saved!");
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}
	}
	
	/**
	 * to check jmeter script file exists 
	 * @return
	 */
	public boolean isScriptFileExist(String strJmeterScriptPath){
		File f = null;
		boolean bool = false;
		
		try{
			//strJmeterScriptPath="C:/Appedo/resource/vuscripts/jmeter/vuscripts1.xml";	
			// create new files
			f = new File(strJmeterScriptPath);
			
			// vuscripts.xml if file exists
			bool = f.exists();
			
			// prints
			LogManager.infoLog("File exists: "+bool);
	         
	         if(bool == false){
	        	 // create blank jmeter script file
	        	 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        	 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        	 
	        	 // root elements
	        	 Document doc = docBuilder.newDocument();
	        	 Element rootElement = doc.createElement("vuscripts");
	        	 doc.appendChild(rootElement);
	        	 
	        	 // write the content into xml file
	        	 TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        	 Transformer transformer = transformerFactory.newTransformer();
	        	 DOMSource source = new DOMSource(doc);
	        	 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
	        	 
	        	 // Output to console for testing
	        	 // StreamResult result = new StreamResult(System.out);
	        	 
	        	 transformer.transform(source, result);
	        }
	         // tests if file exists
	         bool = f.exists();  
	         // prints
	         LogManager.infoLog("File exists: "+bool);
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}
		
		return bool;
	}
	
	/**
	 * Get the list of script names from the jmx file
	 * @throws Throwable
	 */
	public void getScriptNames(String strSaveJmeterScenarioXml, String strJmeterScriptPath, String strScenarioUploadedFileName, String strJmeterScenarioXmlPath) throws Throwable{
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strSaveJmeterScenarioXml)));
			
			String strXml = FileUtils.readFileToString(new File(strSaveJmeterScenarioXml), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);

			XPath xPath =  XPathFactory.newInstance().newXPath();
			String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname]";
			
			NodeList threadGroupList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			System.out.println("sdfs"+threadGroupList.getLength());
			if(threadGroupList.getLength()>0) {
				for (int i = 0; i < threadGroupList.getLength(); i++)	{
					Node threadGroupNode = (Node) threadGroupList.item(i);
					 NamedNodeMap nodeattributes = threadGroupNode.getAttributes();
					 for (int a = 0; a < nodeattributes.getLength(); a++)	{
						 Node themAttribute = nodeattributes.item(a);
						 if(themAttribute.getNodeName().equals("testname"))	{
							 LogManager.infoLog("testname value :"+themAttribute.getNodeValue().toLowerCase());
							 long currentMillis = System.currentTimeMillis();
							 addScript(themAttribute.getNodeValue().toLowerCase(), strJmeterScriptPath);
							 addDefaultSettings(strScenarioUploadedFileName.substring(0, strScenarioUploadedFileName.length()-4), themAttribute.getNodeValue(), String.valueOf(currentMillis), strJmeterScenarioXmlPath);
						 }
					 }
				}
			}
			
		}catch(Throwable t){
			LogManager.errorLog(t);
			throw t;
		}
	}
/**
 * to set default scenario's settings for executing the jmeter scripts
 * @param strScenarioName
 * @param strScriptName
 * @param strScriptId
 * @param strJmeterScenarioXmlPath
 */
	public void addDefaultSettings(String strScenarioName, String strScriptName, String strScriptId, String strJmeterScenarioXmlPath){
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			//Document doc = db.parse(new FileInputStream(new File(strJmeterScenarioXmlPath)));
			
			String strXml = FileUtils.readFileToString(new File(strJmeterScenarioXmlPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			if(isScenarioFileExist(strJmeterScenarioXmlPath)){
				// removing if scenario already exists.
				XPath varxPath =  XPathFactory.newInstance().newXPath();
				String expression = "/root/scenarios/scenario[@name='"+strScriptName+"']";
				NodeList recfiletypeList = (NodeList) varxPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
				
				int mnum = recfiletypeList.getLength();
				if(mnum>0){
					Element mnode = (Element) recfiletypeList.item(0);
					mnode.getParentNode().removeChild(mnode);
					saveXmlDoc(doc, strJmeterScenarioXmlPath);
				}
				
				Node rootElement = doc.getElementsByTagName("scenarios").item(0);
				// scenario elements
				Element nodeScenario = doc.createElement("scenario");
				rootElement.appendChild(nodeScenario);
				// set attributes to element
				nodeScenario.setAttribute("enableipspoofing", "False");
				
				 long currentMillis = System.currentTimeMillis();
				nodeScenario.setAttribute("id", String.valueOf(currentMillis));
				nodeScenario.setAttribute("name", strScenarioName);
				
				
				//script element
				Element nodeScript = doc.createElement("script");
				nodeScenario.appendChild(nodeScript);
				
				nodeScript.setAttribute("id", strScriptId);
				nodeScript.setAttribute("name", strScriptName);
				
				//setting element
				
				Element nodeSetting = doc.createElement("setting");
				nodeScript.appendChild(nodeSetting);			
				nodeSetting.setAttribute("browsercache","false");
				nodeSetting.setAttribute("currentloadgenid","1");
				nodeSetting.setAttribute("durationtime","0;1;0");
				nodeSetting.setAttribute("incrementtime","0;0;1");
				nodeSetting.setAttribute("incrementuser","1");
				nodeSetting.setAttribute("iterations","1");
				nodeSetting.setAttribute("maxuser","5");
				nodeSetting.setAttribute("startuser","1");
				nodeSetting.setAttribute("startuserid","0");
				nodeSetting.setAttribute("totalloadgen","1");
				nodeSetting.setAttribute("type","2");
				
				saveXmlDoc(doc, strJmeterScenarioXmlPath);
			}
		}catch(Throwable t) {
			LogManager.errorLog(t);
		}
	}
	/**
	 * to update scenario's settings in the jmx file 
	 * @param strJmeterScenarioFilePath
	 * @param loadTestBean
	 * @throws Throwable
	 */
	public void updateJmxFile(String strJmeterScenarioFilePath, LoadTestBean loadTestBean) throws Throwable{
		
		try {
			if(!loadTestBean.getScriptName().trim().isEmpty()) {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setValidating(false);
				DocumentBuilder db = dbf.newDocumentBuilder();
				// Document doc = db.parse(new FileInputStream(new File(strJmeterScenarioFilePath)));
				
				String strXml = FileUtils.readFileToString(new File(strJmeterScenarioFilePath), "UTF-8");
			    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			    Document doc = db.parse(byteArrayInputStream);
				// locate the node(s)
				XPath xPath =  XPathFactory.newInstance().newXPath();
				String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='"+loadTestBean.getScriptName()+"']";
				LogManager.infoLog(expression);			    
				
				Node threadNode= (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
				
				if(threadNode!=null) {
					threadNode.getParentNode().removeChild(threadNode);
					doc.normalize();
					doc=appendElement(doc, loadTestBean);
					
					/** write the content into xml file */
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource source = new DOMSource(doc);
					StreamResult result = new StreamResult(new File(strJmeterScenarioFilePath));
					/** Output to console for testing */
					// StreamResult result = new StreamResult(System.out);
					transformer.transform(source, result);
					LogManager.infoLog("File saved!");
				}
			}
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
	}
	
/**
 * to update jmx file 
 * @param doc
 * @param loadTestBean
 * @return
 */
	public Document appendElement(Document doc, LoadTestBean loadTestBean){
		//String strLoops="-1",num_threads="1",ramp_time="1",start_time="3333" ,end_time="5555",scheduler="false",duration="10",delay="";
		
		try {
			XPath xPath =  XPathFactory.newInstance().newXPath();
			String expression = "/jmeterTestPlan/hashTree/hashTree";
			LogManager.infoLog(expression);			    
			
			Node node= (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			// get the first child node of hashTree
			Node firstNode=node.getFirstChild();
			
			Element eleThreadGroup = doc.createElement("ThreadGroup");
			// node.appendChild(eleThreadGroup);
            node.insertBefore(eleThreadGroup, firstNode);
            
            
            eleThreadGroup.setAttribute("guiclass", "ThreadGroupGui");
            eleThreadGroup.setAttribute("testclass", "ThreadGroup");
            eleThreadGroup.setAttribute("testname", loadTestBean.getScriptName());
            eleThreadGroup.setAttribute("enabled", "true");
            
			Element elementProp = doc.createElement("elementProp");
			eleThreadGroup.appendChild(elementProp);
            // set attributes of elementProp
            elementProp.setAttribute("name","ThreadGroup.main_controller");
            elementProp.setAttribute("elementType", "LoopController");
            elementProp.setAttribute("guiclass", "LoopControlPanel");
            elementProp.setAttribute("testclass", "LoopController");
            elementProp.setAttribute("testname", "Loop Controller");
            elementProp.setAttribute("enabled", "true");
            
            //boolProp
            Element elecontinue_forever = doc.createElement("boolProp");
            elementProp.appendChild(elecontinue_forever);
            elecontinue_forever.setAttribute("name","LoopController.continue_forever");
            elecontinue_forever.setTextContent("false");
            //stringProp--loops
            Element eleloops = doc.createElement("stringProp");
            elementProp.appendChild(eleloops);
            eleloops.setAttribute("name","LoopController.loops");
            eleloops.setTextContent(loadTestBean.getLoops());
            
            //stringProp---num_threads
          
            Element elenum_threads = doc.createElement("stringProp");
            eleThreadGroup.appendChild(elenum_threads);
            elenum_threads.setAttribute("name","ThreadGroup.num_threads");
            elenum_threads.setTextContent(loadTestBean.getNoOfThreads());
            
            //stringProp---ramp_time
	          
            Element eleramp_time = doc.createElement("stringProp");
            eleThreadGroup.appendChild(eleramp_time);
            eleramp_time.setAttribute("name","ThreadGroup.ramp_time");
            eleramp_time.setTextContent(loadTestBean.getRAMPTime());
            
            //stringProp
            Element elestart_time = doc.createElement("boolProp");
            eleThreadGroup.appendChild(elestart_time);
            
            elestart_time.setAttribute("name","ThreadGroup.start_time");
            elestart_time.setTextContent(loadTestBean.getStartTime());
            
          //stringProp
            Element eleend_time = doc.createElement("longProp");
            eleThreadGroup.appendChild(eleend_time);
            
            eleend_time.setAttribute("name","ThreadGroup.end_time");
            eleend_time.setTextContent(loadTestBean.getStartTime());
            LogManager.infoLog("222222222222222222222222  endtme: "+loadTestBean.getEndTime());
            //stringProp
            Element elescheduler = doc.createElement("boolProp");
            eleThreadGroup.appendChild(elescheduler);
            
            elescheduler.setAttribute("name","ThreadGroup.scheduler");
            elescheduler.setTextContent(loadTestBean.getSchedular());
            
            //stringProp
            Element eleduration = doc.createElement("stringProp");
            eleThreadGroup.appendChild(eleduration);
            
            eleduration.setAttribute("name","ThreadGroup.duration");
            eleduration.setTextContent(loadTestBean.getDurationTime());
            
            //stringProp
            Element eledelay = doc.createElement("stringProp");
            eleThreadGroup.appendChild(eledelay);
            
            eledelay.setAttribute("name","ThreadGroup.delay");
            eledelay.setTextContent(loadTestBean.getDelay());
            
            //stringProp
            Element eledelayedStart = doc.createElement("stringProp");
            eleThreadGroup.appendChild(eledelayedStart);
            eledelayedStart.setAttribute("name","ThreadGroup.delayedStart");
            eledelayedStart.setTextContent(loadTestBean.getDelayStart());
			
		}catch(Throwable t){
			LogManager.errorLog(t);
		}
		
        return doc;
	}
	
	/**
	  * To export a file to agent location 
	  * @param filePath
	  * @throws Throwable
	  */
	 public void uploadFile(String filePath,String strNode) throws Throwable {
		 
		 try {
			 
			// takes file path from input parameter
			File uploadFile = new File(filePath);
			 
			LogManager.infoLog("File to upload: " + filePath);
			
			UPLOAD_URL = "http://"+strNode+":8080/appedo_lt_jmeter/RunJmx";
			
			LogManager.infoLog("File to upload: " + UPLOAD_URL);
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST");
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("fileName", uploadFile.getName());
	        httpConn.setRequestProperty("command", "UPLOAD");
	        
	        // opens output stream of the HTTP connection for writing data
	        outputStream = httpConn.getOutputStream();
	        
	        // Opens input stream of the file for reading data
	        inputStream = new FileInputStream(uploadFile);
	        
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        
	        LogManager.infoLog("Start writing data...");
	        
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	        	outputStream.write(buffer, 0, bytesRead);
		    }
	        
	        LogManager.infoLog("Data was written.");
	        
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	        	// reads server's response
	        	reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
	        	String response = reader.readLine();
	        	
	        	LogManager.infoLog("Got response from the hitted URL...");
		    } else {
		    	LogManager.infoLog("Server returned non-OK code: " + responseCode);
		    }
	        
		 }catch(Throwable t) {
			 LogManager.errorLog(t);
			 throw t;
		 }finally {
			 reader.close();
			 inputStream.close();
			 outputStream.close();
		 }
	}
	 /***
	  *  Initiate to the agent for executing the jmx script 
	  * @param strNode
	  * @param strScenarioName
	  */
	 
	public void runJmeterScript(String strNode,String strJtlFileName,String strScenarioName) throws Throwable {
		
		try {
			UPLOAD_URL = "http://"+strNode+":8080/appedo_lt_jmeter/RunJmx";
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        
	        LogManager.infoLog("Jmeter hits URL: "+UPLOAD_URL);
	        
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST");	        
	        
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("command", "RUN");
	        httpConn.setRequestProperty("jmx", strScenarioName);
	        httpConn.setRequestProperty("JTL", strJtlFileName);
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	        	// reads server's response
	        	reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
	        	String response = reader.readLine();
	        	LogManager.infoLog("Server's response: " + response);
		            
		     } else {
		    	 LogManager.infoLog("Server returned non-OK code: " + responseCode);
		     }
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;			
		}finally {
			reader.close();
		}
	}
	/**
	 * to import the result file from the agent
	 * @param strNode
	 * @param strJtlFileName
	 * @param srtJmeterJtlPath
	 * @throws Throwable
	 */
	public void  getResultFile(String strNode,String strJtlFileName, String srtJmeterJtlPath) throws Throwable  {
		// Members
		String str="";
		try {
			
			UPLOAD_URL = "http://"+strNode+":8080/appedo_lt_jmeter/RunJmx";
			// creates a HTTP connection
	        URL url = new URL(UPLOAD_URL);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);
	        httpConn.setRequestMethod("POST"); 
	        
	        // sets file name as a HTTP header
	        httpConn.setRequestProperty("command", "RESULTFILE");
	        httpConn.setRequestProperty("JTL", strJtlFileName);
	        
	        
	        // always check HTTP response code from server
	        int responseCode = httpConn.getResponseCode();
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	            // reads server's response
	            reader = new BufferedReader(new InputStreamReader(
	                    httpConn.getInputStream()));
	            
	            while( (str = reader.readLine()) != null ){
	            //String response = reader.readLine();
	            	sb.append(strNode+","+str+"\n");
	            }
	           // System.out.println("Server's response: " + response);
	            LogManager.infoLog("Server's response: " + sb.toString());
	        } else {
	        	LogManager.infoLog("Server returned non-OK code: " + responseCode);
	        }
			
			writeToFile(srtJmeterJtlPath+strJtlFileName, sb);
			
			
	       // writeToFile("C:/Appedo/resource/jmetertest/jtl/"+strJtlFileName, sb);
	        
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
			reader.close();
			// clear the string builder content
			sb.delete(0, sb.length());
		}
	}
	/**
	 * to create the result file (.csv)
	 * @param strFilePath
	 * @throws Throwable
	 */
	public void writeToFile(String strFilePath, StringBuilder Sb) throws Throwable {
		
		try {
			 
			
			File file = new File(strFilePath);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			LogManager.infoLog("abf :"+file.getAbsoluteFile());
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			LogManager.infoLog("write content :"+Sb.toString());
			
			bw.write(Sb.toString());
			bw.close();
 
			LogManager.infoLog("Done");
 
		} catch (Throwable  e) {
			LogManager.errorLog(e);
		}
	}
	
	
	
	/**
	 * to merge multiple csv files
	 * @param list
	 * @param strJmeterReportUId
	 */
	public void mergeCsvFiles(List<String> list,String strJmeterReportUId) {
		int varFirst=0;
		
		//
		try {
			
			String destination = Constants.JMETERCSVPATH+strJmeterReportUId+".csv";
			File destFile = new File(destination);
			if (!destFile.exists()) {
				destFile.createNewFile();
			}
			FileWriter writer = new FileWriter(destination);
			for (String str : list) {
				LogManager.infoLog("file :"+str);
				createOutputFile(str, writer,varFirst);
				writer.append("\n");
				//varFirst++;
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void insertValue(String[] values, FileWriter writer) {
		try {
			for (int i = 0; i < values.length; i++) {
				 if (i == values.length - 1) {
					writer.append(values[i]);
				} else {
					writer.append(values[i]);
					writer.append(",");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * to create result csv file 
	 * @param source
	 * @param writer
	 * @param varFirst
	 */
	public static void createOutputFile(String source, FileWriter writer,int varFirst) {
		FileInputStream fis = null;
		Scanner csvFileScanner2 = null;
		try {
			String line = "";
			String csvFilePattern;
			String fieldSeparator;
			fieldSeparator = "\\|";
			csvFilePattern = "(.*?fieldSeparator)++(.*?)";
			csvFilePattern = csvFilePattern.replaceAll("fieldSeparator",
					fieldSeparator);

			boolean firstLine = true;

			File file = new File(source);
			fis = new FileInputStream(file);
			csvFileScanner2 = new Scanner(fis);
			while (csvFileScanner2.hasNext()) {
				line = csvFileScanner2.nextLine();
				if (firstLine) {
					firstLine = false;
					continue;
				}
				Pattern pattern = Pattern.compile(csvFilePattern);
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches()) {
					String[] colValues = matcher.group().split("\"");
					if(varFirst>0) {
						writer.append("\n");
					}
					
					String[] values;
					if (colValues.length > 1) {
						writer.append(colValues[0]);
						writer.append("\"" + colValues[1] + "\"");
						values = colValues[2].split(",");
						insertValue(values, writer);
					} else {
						values = colValues[0].split(",");
						insertValue(values, writer);
					}
				}
				varFirst++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			UtilsFactory.close(fis);
//			fis = null;
			if( csvFileScanner2 != null ){
				csvFileScanner2.close();
			}
//			csvFileScanner2 = null;
		}
	}
	/**
	 * export result file to floodgates controller to
	 * get the summaryreport
	 * @param strResultCsv
	 */
	
	public  void sendResultFile(Connection con, String strJmxResultFilePath, long reportId,String strUId)  throws Throwable{
		
		// Members
		InetAddress address = null;
		Socket connection = null;
		OutputStream bos = null;		
		InputStream inst = null;
		
		
		String host= Constants.JM_CONTROLLER_IP;		
		
		int hostport=Integer.parseInt(Constants.JM_CONTROLLER_PORT);
		
		
		String csvFile=Constants.JMETERCSVPATH;// to keep the summary report csv file
		String summaryreportPath = Constants.JMETERSUMMARYREPORTPATH;// to keep the summary report
		
		RunManager runManager = new RunManager();
		
		try {
			/** Obtain an address object of the server */
			address = InetAddress.getByName(host);
			/** Establish a socket connection */
			connection = new Socket(address, hostport);
			LogManager.infoLog("RunDebug:After  connection");
			/** Instantiate a BufferedOutputStream object */		 
			bos = connection.getOutputStream();
			
			LogManager.infoLog("strJmxResultFilePath = "+strJmxResultFilePath);
			String txtbody = new String(Files.readAllBytes(Paths.get(strJmxResultFilePath)));
			
			LogManager.infoLog(txtbody);
			
			String process ="RESULTFILEJMETER: "+txtbody.length()+"\r\n"+"reportid= "+strUId+"\r\n\r\n"+txtbody;
					
			byte[] b=process.getBytes();			
			bos.write(b, 0, b.length);
			
			LogManager.infoLog("content sent to destionation");			
			LogManager.infoLog("Waiting for result report");
			
			
			while(true)
			{
				
				 inst=connection.getInputStream();
				 
				 String reqheader= runManager.readHeader(inst);
				 String[] reqtokens=reqheader.trim().split(": ");
				 String reqoperation=reqtokens[0].toString();
				 int reqstreamLength=Integer.parseInt(reqtokens[1].toString().split("\r\n")[0]);
				 
				 LogManager.infoLog("OPERATION ="+reqoperation+"\r\n----CONTENT LENGTH="+reqstreamLength);
				 int  readCount = 0;
				 byte[] bytes = new byte[reqstreamLength];
				 
				 if(reqstreamLength>0)	{		 
					 
					 while (readCount < reqstreamLength)	{
						 readCount+= inst.read(bytes, readCount, reqstreamLength-readCount);
						 LogManager.infoLog("byte read"+readCount);
					}
				 }
				 
				 String receivedstremContent = new String(bytes);
				 LogManager.infoLog("rece con"+receivedstremContent);
				 if(reqoperation.trim().equals("REPORTGENERATIONG")) {
					 String strresponse="OK: 0\r\n\r\n";
					 byte[] resb=strresponse.getBytes();
					 bos.write(resb, 0, resb.length);
					 LogManager.infoLog("after write done");
					// bos.close();
					 connection.close();
				}
				 else if(reqoperation.trim().equals("RESULT"))
				 {
					 LogManager.infoLog("inside result con");					 
					
					 /** this is to write the received content to csv file */
					 LogManager.infoLog(csvFile+strUId+".csv");
					 writetoxmlFile(receivedstremContent,csvFile+strUId+"result.csv");
					 
					 String rptfilePath=csvFile+strUId+"result.csv";
					 
					 runManager.updateSummaryResult(con, String.valueOf(reportId),rptfilePath);							 
					 String strresponse="OK: 0\r\n\r\n";
					 byte[] resb=strresponse.getBytes();
					 bos.write(resb, 0, resb.length);
					 LogManager.infoLog("after write done");
					 
					  reqheader= runManager.readHeader(inst);
					  reqtokens=reqheader.trim().split(": ");
					  reqoperation=reqtokens[0].toString();
					  reqstreamLength=Integer.parseInt(reqtokens[1].toString().split("\r\n")[0]);
					 
					  LogManager.infoLog("OPERATION ="+reqoperation+"\r\n----CONTENT LENGTH="+reqstreamLength);
					   readCount = 0;
					 bytes = new byte[reqstreamLength];
					 
					 if(reqstreamLength>0)	{		 
						 
						 while (readCount < reqstreamLength)	{
							 readCount+= inst.read(bytes, readCount, reqstreamLength-readCount);
							 LogManager.infoLog("byte read"+readCount);
						}
					 }
					 
					 receivedstremContent = new String(bytes);
					 LogManager.infoLog(summaryreportPath+strUId+".html");
					 writetoxmlFile(receivedstremContent,summaryreportPath+strUId+".xml");
					 strresponse="OK: 0\r\n\r\n";
					 resb=strresponse.getBytes();
					 bos.write(resb, 0, resb.length);
					 //bos.close();
					 //connection.close();
					 break;
					 
				 }				 
				 else if(reqoperation.trim().equals("ERROR"))
				 {
					 LogManager.infoLog("Error with result file");
					 connection.close();
					 break;
				 }
				
				 Thread.sleep(5000);// wait for 10 seconds
			}
			
			
		}catch(Throwable t){
			LogManager.errorLog(t);
			throw t;
			
		}finally {
			
			inst.close();
//			inst = null;
			bos.close();
//			bos = null;
			connection.close();
//			connection = null;
		}
	}
	/**
	 * to create & write the content to xml file
	 * @param fileContent
	 * @param filePath
	 * @throws Throwable
	 */
	public static void writetoxmlFile(String fileContent,String filePath) throws Throwable
	{
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			 
			LogManager.infoLog("file path"+filePath);
			LogManager.infoLog("file content"+fileContent);
			
			
			File file = new File(filePath);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bw.write(fileContent);
			
			LogManager.infoLog("Done");

		} catch (Throwable t) {
			LogManager.errorLog(t);
			throw t;
		} finally {
			UtilsFactory.close(bw);
//			bw = null;
			UtilsFactory.close(fw);
//			fw = null;
		}
	}
	/**
	 * to get list of parameter file names 
	 * @param strJmxPath
	 * @return
	 * @throws Throwable
	 */
	public List<String> getParametersFile(String strJmxPath) throws Throwable {
		// Members
		List<String> list = new ArrayList<String>();
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// Document doc = db.parse(new FileInputStream(new File(strJmxPath)));
			
			String strXml = FileUtils.readFileToString(new File(strJmxPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			// locate the node(s)
			XPath xPath =  XPathFactory.newInstance().newXPath();			
			//String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='Thread Group']";
			String expression = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
			
			LogManager.infoLog(expression);
			
			NodeList paramNode= (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			
			LogManager.infoLog("# files :"+paramNode.getLength());
			
			if(paramNode.getLength()>0) {
				for (int i = 0; i < paramNode.getLength(); i++) {
					//Node threadGroupNode = (Node) paramNode.item(i);
					LogManager.infoLog("val "+paramNode.item(i).getTextContent());
					
					if(paramNode.item(i).getTextContent() != null &&paramNode.item(i).getTextContent().trim().length()>0 ) {
						
							list.add(FilenameUtils.getName(paramNode.item(i).getTextContent()).toLowerCase());
					}
				}
			}
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}		
		
		return list;
	}
	/**
	 * to validate jmx file for parameter files
	 * @param strJmxPath
	 * @return
	 * @throws Throwable
	 */
	public Boolean isJmxContainsCsvFile(String strJmxPath) throws Throwable {
		// Members
		Boolean  bool = false;
		// FileInputStream fileInputStream = null;
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// fileInputStream = new FileInputStream(new File(strJmxPath));
			// Document doc = db.parse(fileInputStream);
			
			String strXml = FileUtils.readFileToString(new File(strJmxPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			// locate the node(s)
			XPath xPath =  XPathFactory.newInstance().newXPath();			
			//String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='Thread Group']";
			String expression = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
			
			LogManager.infoLog(expression);
			
			NodeList paramNode= (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			
			if(paramNode.getLength()>0) {
				bool = true;
			}
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		} finally {
			// UtilsFactory.close(fileInputStream);
//			fileInputStream = null;
		}
		
		return bool;
	}
	/**
	 * to validate jmx file for parameter files
	 * @param strJmxPath
	 * @return
	 * @throws Throwable
	 */
	public ArrayList<String> getJmxContainsCsvFileNames(String strJmxPath) throws Throwable {
		// Members
		// FileInputStream fileInputStream = null;
		ArrayList<String> al = null;
		try {
			
			al = new ArrayList<String>();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// fileInputStream = new FileInputStream(new File(strJmxPath));
			// Document doc = db.parse(fileInputStream);
			
			String strXml = FileUtils.readFileToString(new File(strJmxPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc  = db.parse(byteArrayInputStream);
			
			// locate the node(s)
			XPath xPath =  XPathFactory.newInstance().newXPath();			
			//String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='Thread Group']";
			String expression = "/jmeterTestPlan/hashTree/hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
			
			LogManager.infoLog(expression);
			
			NodeList paramNode= (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			
			for(int i=0; i<paramNode.getLength(); i++) {
				al.add(paramNode.item(i).getTextContent());
			}
			
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		} finally {
			// UtilsFactory.close(fileInputStream);
//			fileInputStream = null;
		}
		
		return al;
}
	/**
	 * to validate parameter files of jmeter script
	 * @param strJmxPath
	 * @param strParamPath
	 * @return
	 * @throws Throwable
	 */
	public Boolean isParamFilesExist(String strJmxPath , String strParamPath) throws Throwable {
	
		// Members
		String strCsvParamFilePath="";
		Boolean bool=true;
		// FileInputStream fileInputStream = null;
		
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			// fileInputStream = new FileInputStream(new File(strJmxPath));
			// Document doc = db.parse(fileInputStream);
			
			String strXml = FileUtils.readFileToString(new File(strJmxPath), "UTF-8");
		    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		    Document doc = db.parse(byteArrayInputStream);
			
			// locate the node(s)
			XPath xPath =  XPathFactory.newInstance().newXPath();			
			//String expression = "/jmeterTestPlan/hashTree/hashTree/ThreadGroup[@testname='Thread Group']";
			String expression = "//hashTree/hashTree/CSVDataSet/stringProp[@name='filename']";
			
			LogManager.infoLog(expression);
			
			NodeList paramNode= (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			
			LogManager.infoLog("# files :"+paramNode.getLength());
			
			if(paramNode.getLength()>0) {
				for (int i = 0; i < paramNode.getLength(); i++) {
					//Node threadGroupNode = (Node) paramNode.item(i);
					LogManager.infoLog("val "+paramNode.item(i).getTextContent());
					
					if(paramNode.item(i).getTextContent().trim().length()>0 && paramNode.item(i).getTextContent().trim()!= null) {
						strCsvParamFilePath = strParamPath+File.separator+paramNode.item(i).getTextContent().toLowerCase();
						File fi = new File(strCsvParamFilePath);
						if(fi.exists()) {
							//list.add(paramNode.item(i).getTextContent());
							//list.put("file"+i,paramNode.item(i).getTextContent() );
							bool = true;
						}
						else {
							bool = false;
							break;
						}
					}
				}
			}
		}catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		} finally {
			// UtilsFactory.close(fileInputStream);
//			fileInputStream = null;
		}
		
		return bool;
	}
	/**
	 * JMETER scenario file exits
	 * @param strJmeterScriptPath
	 * @return
	 */
	public boolean isScenarioFileExist(String strJmeterScriptPath){
		
		File f = null;
		boolean bool = false;
		
		try{
			//strJmeterScriptPath="C:/Appedo/resource/vuscripts/jmeter/vuscripts1.xml";	
			// create new files
			f = new File(strJmeterScriptPath);
			
			// vuscripts.xml if file exists
			bool = f.exists();
			
			// prints
			LogManager.infoLog("File exists: "+bool);
	         
	         if(bool == false){
	        	 // create blank jmeter script file
	        	 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	        	 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	        	 
	        	// root elements
	        	 Document doc = docBuilder.newDocument();
	        	 Element rootElement = doc.createElement("root");
	        	 doc.appendChild(rootElement);
	        	 
	        	 // child elements
	        	 Element childElement = doc.createElement("scenarios");
	        	 rootElement.appendChild(childElement);
	        	 
	        	 // write the content into xml file
	        	 TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        	 Transformer transformer = transformerFactory.newTransformer();
	        	 DOMSource source = new DOMSource(doc);
	        	 StreamResult result = new StreamResult(new File(strJmeterScriptPath));
	        	 
	        	 // Output to console for testing
	        	 // StreamResult result = new StreamResult(System.out);
	        	 
	        	 transformer.transform(source, result);
	        }
	         // tests if file exists
	         bool = f.exists();  
	         // prints
	         System.out.print("File exists: "+bool);
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}
		
		return bool;
	}

}
