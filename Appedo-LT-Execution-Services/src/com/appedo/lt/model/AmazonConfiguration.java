package com.appedo.lt.model;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.appedo.manager.LogManager;

public class AmazonConfiguration {


	public AmazonConfiguration(){
		
	}
	
	public String createInstance(String imageId, String region, String InstanceType, String keyPairName, String secGroupName, String accesskey, String secretkey) throws InterruptedException {
		try {
			AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
			ec2.setEndpoint(region);
			RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
	
			runInstancesRequest.withImageId(imageId).withInstanceType(InstanceType).withMinCount(1).withMaxCount(1).withKeyName(keyPairName).withSecurityGroups(secGroupName);
	
			RunInstancesResult runInstancesResult = ec2.runInstances(runInstancesRequest);
	
			List<Instance> instances = runInstancesResult.getReservation().getInstances();
			if( instances.size() == 0){
				return "";
			}else{
				return instances.get(0).getInstanceId();
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			return "Error:"+e.getMessage();
		}
	}

	public String getPublicIP(String instanceeID, String region, String accesskey, String secretkey){
		String ipAddress = "";
		try {
			AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
			ec2.setEndpoint(region);
			DescribeInstancesRequest ec2Request = new DescribeInstancesRequest();
			List<String> instanceIds = new ArrayList<String>();
			instanceIds.add(instanceeID);
			ec2Request.setInstanceIds(instanceIds);
			DescribeInstancesResult res = ec2.describeInstances(ec2Request);
			List<Reservation> instances = res.getReservations();
			if(instances.size()==0){
				ipAddress = "";
			}else{
				ipAddress = instances.get(0).getInstances().get(0).getPublicIpAddress();
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return ipAddress;
	}
	
	private AmazonEC2Client getAmazonClient(String accesskey, String secretkey) {
		return new AmazonEC2Client(new BasicAWSCredentials(accesskey, secretkey));
	}

	public int getAmazonCount(String region, String accesskey, String secretkey) {
		AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
		ec2.setEndpoint(region);
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		DescribeInstancesResult res = ec2.describeInstances(request);
		System.out.println(res.getReservations().size());
		return res.getReservations().size();
	}
	
	public void terminateInstance(String region, List<String> instanceIds, String accesskey, String secretkey){
		AmazonEC2Client ec2 = getAmazonClient(accesskey, secretkey);
		ec2.setEndpoint(region);
		TerminateInstancesRequest tir = new TerminateInstancesRequest(instanceIds);
        ec2.terminateInstances(tir);
	}
}
