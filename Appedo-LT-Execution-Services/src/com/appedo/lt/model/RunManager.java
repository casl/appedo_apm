package com.appedo.lt.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.appedo.commons.bean.LoginUserBean;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LTLicenseBean;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.dbi.LTDBI;
import com.appedo.lt.dbi.RunDBI;
import com.appedo.lt.tcpserver.FloodgatesXMLAppend;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

/**
 * This is to manage the run operations
 * @author Veeru
 *
 */
public class RunManager {
	
	public static final String SEMAPHORE_RUN_LT = "SEMAPHORE";
	static String variablxmlPath="";
	static String xmlfilePath="";
	static InputStream input = null;
	/**
	 * to stop the scenario which is running 
	 * @param reportId
	 * @throws Throwable
	 */
	public JSONObject stopScenario(String runId , String host , int hostport) throws Throwable {
		
		Socket scon = null;
		InetAddress address = null;
		OutputStream outStream = null;
		InputStream inStream = null;
		String strresponse = null;
		byte[] b = null;
		String[] tokens = null;
		String operation = null;
		JSONObject joRtn = null;
		try	{
			
			/** Obtain an address object of the server */
			address = InetAddress.getByName(host);
			/** Establish a socket connection */
			scon = new Socket(address, hostport);
			/** Instantiate a BufferedOutputStream object */
			outStream = scon.getOutputStream();
			inStream=scon.getInputStream();
			
			strresponse="STOP: 0\r\n"+"runid= "+runId+"\r\n\r\n";
			b=strresponse.getBytes();
			outStream.write(b, 0, b.length);
			String stopHeader= readHeader(inStream);
			
			tokens=stopHeader.trim().split(": ");
		
			operation=tokens[0].toString();
			LogManager.infoLog("Stop Scenario"+operation);
			joRtn = UtilsFactory.getJSONSuccessReturn("Load Test stopped.");
		}catch(Exception e){
			LogManager.errorLog(e);
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}finally {
//			strresponse = null;
//			b = null;
//			tokens = null;
//			operation = null;
			UtilsFactory.close( outStream );
			UtilsFactory.close( inStream );
			scon.close();
//			address = null;
			
		}
		return joRtn;
	}
	
	public JSONObject stopJMeterTest(String runId, String host) throws Throwable {
		
		Socket clientSocket = null;
		ObjectOutputStream objOutputStream = null;
		ObjectInputStream objInputStream = null;
		JSONObject joRtn = null, joReqData = null;
		try	{
			
			joReqData = new JSONObject();
			clientSocket = new Socket(host, Integer.parseInt(Constants.JM_CONTROLLER_PORT));
			objOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
			LogManager.infoLog("Sending stop request to Socket Server running in JMeterLoadGen: "+host);
            
			joReqData.put("stopTestRequest", true);
			joReqData.put("runId", runId);
			joReqData.put("requestTime", System.currentTimeMillis());
			
            objOutputStream.writeObject(joReqData.toString());
            
            objInputStream = new ObjectInputStream(clientSocket.getInputStream());
            String message = (String) objInputStream.readObject();
            
            if (message !=  null && message.trim().startsWith("{") && message.trim().endsWith("}")) {
            	joRtn = JSONObject.fromObject(message);
            	if (joRtn.getBoolean("success")) {
            		LogManager.infoLog(joRtn.getString("message"));
            		//joRtn = UtilsFactory.getJSONSuccessReturn(joResp.getString("message"));
            		//Updating loadGen IP in tblreportmaster table.
            	} else {
            		LogManager.errorLog("Failure response from jmeter loadgen jar: "+joRtn.getString("errorMessage"));
            		//confirm whether does required to queue again.
            	}
            } else {
            	LogManager.errorLog("Invalid server Socket response from jmeter loadgen jar for JMeter Stop request.");
            	System.out.println("Invalid server Socket response from jmeter loadgen jar for JMeter Stop request.");
            	joRtn = UtilsFactory.getJSONFailureReturn("Invalid server Socket response from jmeter loadgen jar for JMeter Stop request.");
            }
			//LogManager.infoLog("Stop Scenario"+operation);
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}finally {
//			strresponse = null;
//			b = null;
//			tokens = null;
//			operation = null;
			UtilsFactory.close( objOutputStream );
			UtilsFactory.close( objInputStream );
			clientSocket.close();
//			address = null;
			
		}
		return joRtn;
	}
	/**
	 * To read the csv file (summary report ) & insert into the chart summary table
	 * @param reportName
	 * @param csvPath
	 */
	public void updateSummaryResult(Connection con, String strReportName,String strCsvPath) throws Exception {
		
		RunDBI runDBI = null;
	
		try {
			runDBI = new RunDBI();
			runDBI.updateSummaryResult(con, strReportName, strCsvPath);
	
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
//			runDBI = null;			
		}
		LogManager.infoLog("Done");
	}
	
	/**
	 * to read the header of stream data
	 * @param stream
	 * @return
	 * @throws Throwable
	 */
	public String readHeader(InputStream stream) throws Throwable {
		StringBuffer instr = null;
		int c;
		try	{
			instr = new StringBuffer();
			while (true ) {
				c=stream.read();
				instr.append( (char) c);
				if(instr.toString().endsWith("\r\n\r\n") )
					break;
			}
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}
		return instr.toString();
	}
	
	
	/**
	 * To convert string object to xml document 
	 * @param xmlStr
	 * @return
	 * @throws Throwable
	 */
	public Document convertStringToDocument(String xmlStr) throws Throwable {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		Document doc=null;
		try {
			
			builder = factory.newDocumentBuilder();
			doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
//			factory = null;
//			builder = null;
		}
		return doc;
	}
	
	/**
	 * to convert xml document to string object
	 * @param doc
	 * @return
	 * @throws Throwable
	 */
	public String convertDocumentToString(Document doc) throws Throwable {
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = null;
		StringWriter writer = null;
		String output="";
		try {
			transformer = tf.newTransformer();
			// below code to remove XML declaration
			writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString();
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
//			transformer = null;
			UtilsFactory.close( writer );
		}
		return output;
	}
	/**
	 * to get the status of the floodgates & jmeter controller status 
	 * @param strControllerIP
	 * @param nPort
	 * @return
	 * @throws Throwable
	 */
	public String getControllerStatus(String strControllerIP , int nPort) throws Throwable {
		// Members 
		Socket socketConn = null;
		OutputStream oStream = null;
		InputStream inStream = null;
		
		byte[] bOperation = null;
		byte[] bytes = null;
		
		String strOperation = null , strReqHeader = null , strReceivedStremContent = null;		
		int nResponseStreamLength = 0 , nReadCount = 0;
		String strStatus = "inactive";
		try {
			
			socketConn = new Socket(strControllerIP, nPort);
			// connection success
			oStream = socketConn.getOutputStream();
			
			// Make operation format
			strOperation = "TEST: 0\r\n\r\n";
			
			bOperation = strOperation.getBytes();
			oStream.write(bOperation, 0, bOperation.length);
			
			// get response from controller
			inStream = socketConn.getInputStream();
			strReqHeader = readHeader(inStream);
			
			String[] reqtokens = strReqHeader.trim().split(": ");
			nResponseStreamLength = Integer.parseInt(reqtokens[1].toString().split("\r\n")[0]);
			
			bytes = new byte[nResponseStreamLength];
			
			if(nResponseStreamLength>0)	{
				while (nReadCount < nResponseStreamLength)	{
					nReadCount+= inStream.read(bytes, nReadCount, nResponseStreamLength-nReadCount);
					LogManager.infoLog("byte read"+nReadCount);
				}
			}
			// response content
			strReceivedStremContent = new String(bytes);
			if(strReqHeader.contains("ok")) {
				// return sucess status
				strStatus = "active";
			}
			
			LogManager.infoLog("received"+strReceivedStremContent);
		} catch(Throwable t) {
			LogManager.errorLog(t);
			//throw t;
			strStatus = "inactive";
			
		} finally {
			
			if(inStream!= null) {
				inStream.close();				
			}
			inStream = null;
			
			if(oStream!= null) {
				oStream.close();
			}
			oStream = null;
			if(socketConn!= null) {
				socketConn.close();
			}
			socketConn = null;
			
//			strOperation = null;
//			strReqHeader = null;
//			strReceivedStremContent = null;
//			bOperation = null;
//			bytes = null;
		}
		
		return strStatus;
	
	}
	
	public boolean isTomcatStarted(String strControllerIP , int nPort) throws IOException {
		boolean bConnected = false;
		Socket socketConn = null;
		try {
			socketConn = new Socket(strControllerIP, nPort);
			bConnected = true;
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (socketConn != null) {
				socketConn.close();
			}
			socketConn = null;
		}
		return bConnected;
	}

	/**
	 * to create the xml document for given document object
	 * @param mdoc
	 * @throws Throwable
	 */
	public void saveXmlDoc(Document mdoc, String xmlfilePath)throws Throwable {
		
		TransformerFactory transformerFactory = null;
		Transformer transformer = null;
		DOMSource source = null;
		StreamResult result = null;
		try {
			// write the content into xml file
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			source = new DOMSource(mdoc);
			result = new StreamResult(new File(xmlfilePath));
			// Output to console for testing
			transformer.transform(source, result);
			LogManager.infoLog("File saved!");
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}finally {
//			transformerFactory = null;
//			transformer = null;
//			source = null;
//			result = null;
		}
	}
	
	/**
	 * to get the element from the vuscripts.xml
	 * @param scriptid
	 * @param appendid
	 * @return
	 * @throws Throwable
	 */
	
	public static Node getnodefromVuscripts(long scriptid, long lUserId) throws Throwable {
		
		// Members of the function
		Node Node = null;
		String nv="";
		int mnum = 0;
		
		DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
		mdbf.setValidating(false);
		DocumentBuilder mdb = mdbf.newDocumentBuilder();
		// Document mdoc = mdb.parse(new FileInputStream(new File(Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+lUserId+"_vuscripts.xml")));
		
		String strXml = FileUtils.readFileToString(new File(Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+lUserId+"_vuscripts.xml"), "UTF-8");
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
		Document mdoc = mdb.parse(byteArrayInputStream);
		
		try {
			
			NodeList mentries = mdoc.getElementsByTagName("vuscript");
			mnum = mentries.getLength();
			
			for (int i=0; i<mnum; i++) {
				
				Node mnode = (Node) mentries.item(i);				
				NamedNodeMap mattributes = mnode.getAttributes();
				for (int a = 0; a < mattributes.getLength(); a++) {
					
					Node themAttribute = mattributes.item(a);
					if(themAttribute.getNodeName().equals("id")) {
						nv=themAttribute.getNodeValue();
						// return node
						if(Integer.parseInt(nv)==scriptid)
							return mnode;
					}
				}
			}
		}
		catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
		
		return Node;
	}
	
	/**
	 * to get the running scenario report id
	 * @param con
	 * @param strReportName
	 * @return
	 * @throws Throwable
	 */
	public static int getreportId(Connection con, String strReportName) throws Throwable 
	{
		int reportId=0;
		try
		{
			//System.out.println("select max(reportid) from tblreportmaster where strReportName='"+strReportName+"'");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select max(reportid) as reportid from tblreportmaster where strReportName='"+strReportName+"'");
			while (rs.next()) {
				reportId =Integer.parseInt(rs.getString("reportid"));
			}
		}catch(Throwable t)
		{
			LogManager.errorLog(t);
			throw t;
		}
		
		
		return reportId;
		
	}
	
	/**
	 * To get the variables doc
	 * @param lUserId
	 * @return
	 * @throws Throwable
	 */
	public Document getVariables(long lUserId) throws Throwable {
		
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docVariableXml = null;
		try {
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			// docVariableXml = db.parse(new FileInputStream(new File(Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml")));
			
			String strXml = FileUtils.readFileToString(new File(Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml"), "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			docVariableXml = db.parse(byteArrayInputStream);
			
		}catch(Throwable th) {
			LogManager.errorLog(th);
		}finally {
//			dbf = null;
//			db = null;
		}
		return docVariableXml;
	}
	
	/**
	 * to get scenarios
	 * @param lUserId
	 * @param strScenarioId
	 * @return
	 * @throws Throwable
	 */
	public Document getScenarios(long lUserId, String strScenarioId) throws Throwable {
		
		String strExpression = null;
		String strScenarioXmlDocPath = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docScenario = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docScenarioXml = null;
		Element rootElement = null;
		XPath xPath = null;
		NodeList nodeList = null;
		XPath xPath1 = null;
		String expression1 = null;
		Node aNode = null;
		NamedNodeMap attributes = null;
		Node theAttribute = null;
		Node vNode = null;
		Node innode = null;
		Node importedNode = null;
		
		try {

			// to keep the scenario's xml file path 
			strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			//return document for sending
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docScenario = builder.newDocument();
			
			
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			
			// docScenarioXml = db.parse(new FileInputStream(new File(strScenarioXmlDocPath)));
			
			String strXml = FileUtils.readFileToString(new File(strScenarioXmlDocPath), "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			docScenarioXml = db.parse(byteArrayInputStream);
			strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			// get the list of scripts under given scenario
			strExpression = "//root/scenarios/scenario[@id='"+strScenarioId+"']/script";
			
			//appending the root element to final return document
			rootElement = docScenario.createElement("root");
			docScenario.appendChild(rootElement);
			
			xPath = XPathFactory.newInstance().newXPath();
			nodeList = (NodeList) xPath.compile(strExpression).evaluate(docScenarioXml, XPathConstants.NODESET);
				
				//System.out.println("fdF"+nodeList.getLength());
				for (int i = 0; i < nodeList.getLength(); i++) {
					
					aNode = (Node) nodeList.item(i);
					attributes = aNode.getAttributes();
					for (int a = 0; a < attributes.getLength(); a++) {
						
						theAttribute = attributes.item(a);
						//System.out.println(theAttribute.getNodeName()+"---"+theAttribute.getNodeValue());
						if(theAttribute.getNodeName().equals("id") && theAttribute.getNodeValue()!=null ) {
							
							//import the vscript from the source file
							vNode = getnodefromVuscripts(Long.parseLong(theAttribute.getNodeValue()),lUserId);
							if(vNode!=null) {
								Node en=vNode.cloneNode(true);
								docScenarioXml.adoptNode(en);
								aNode.appendChild(en);
							}
						}
					}
				}
				
				//make new xpath to get the selected node from the new xml document
				xPath1 = XPathFactory.newInstance().newXPath();
				expression1 = "/root/scenarios/scenario[@id='"+strScenarioId+"']";
				
				// get only user selected scenario
				innode = (Node) xPath1.compile(expression1).evaluate(docScenarioXml, XPathConstants.NODE);
				importedNode = docScenario.importNode(innode, true);			
				rootElement.appendChild(importedNode);
		}catch(Throwable th) {
			LogManager.errorLog(th);
			
		}finally {
			
//			strExpression = null;
//			strScenarioXmlDocPath = null;
//			factory = null;
//			builder = null;
//			dbf = null;
//			db = null;
//			docScenarioXml = null;
//			rootElement = null;
//			xPath = null;
//			nodeList = null;
//			xPath1 = null;
//			expression1 = null;
//			aNode = null;
//			attributes = null;
//			theAttribute = null;
//			vNode = null;
//			innode = null;
//			importedNode = null;
		}		
		return docScenario;
	}
	/**
	 * to merge scenaro's & variable's
	 * @param docVariables
	 * @param docScenarios
	 * @return
	 * @throws Throwable
	 */
	public Document mergeDoc(Document docVariables, Document docScenarios) throws Throwable {
		
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docVarScenarios = null;
		Element rootElement = null;
		String strSceExpression = null;
		String strVarExpression = null;
		XPath xPathSce = null;
		XPath xPathVAr = null;
		Node nodeScenarios = null;
		Node nodeVariables = null;
		Node nodeCloneScen = null;
		Node nodeCloneVar = null;
		try {
			
			// Make a document object to merge & return
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docVarScenarios = builder.newDocument();
			
			rootElement = docVarScenarios.createElement("root");
			docVarScenarios.appendChild(rootElement);
			
			//expression to get the scenarios
			strSceExpression = "/root/scenario";
			//expression to get the variables
			strVarExpression = "/variables";
			
			xPathSce = XPathFactory.newInstance().newXPath();
			xPathVAr = XPathFactory.newInstance().newXPath();
			nodeScenarios = (Node) xPathSce.compile(strSceExpression).evaluate(docScenarios, XPathConstants.NODE);
			nodeVariables = (Node) xPathVAr.compile(strVarExpression).evaluate(docVariables, XPathConstants.NODE);
			
			//System.out.println("inside ++++" +nodeScenarios.getTextContent());
			nodeCloneScen = nodeScenarios.cloneNode(true);
			docVarScenarios.adoptNode(nodeCloneScen);
			rootElement.appendChild(nodeCloneScen);
			
			nodeCloneVar = nodeVariables.cloneNode(true);
			docVarScenarios.adoptNode(nodeCloneVar);
			rootElement.appendChild(nodeCloneVar);
			
		}catch (Throwable th) {
			LogManager.errorLog(th);
			
		}finally {
//			factory = null;
//			builder = null;
//			rootElement = null;
//			strSceExpression = null;
//			strVarExpression = null;
//			xPathSce = null;
//			xPathVAr = null;
//			nodeScenarios = null;
//			nodeVariables = null;
//			nodeCloneScen = null;
//			nodeCloneVar = null;
			
		}
		
		return docVarScenarios;
	}
	
	/**
	 * To read the csv file (summary report ) & insert into the chart summary table
	 * @param reportName
	 * @param csvPath
	 */
	public void readChartSummary(Connection con, long lRunId, String csvPath) {
		
		//System.out.println("vv"+csvPath);
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";
		String strQry="";
		String chartsummaryTable="tblchartsummary";
		int linecount=0;
		Statement stmt = null;
	 
		try {
			
			br = new BufferedReader(new FileReader(csvPath));
			while ((line = br.readLine()) != null) {
				if(linecount>0 && csvSplitBy.contains(","))
				{
					line=line.replace(" ", "");
					// use comma as separator
					String[] spiltVal = line.split(csvSplitBy);
					// make a query to insert
					strQry="insert into "+chartsummaryTable+" values("+spiltVal[0]+","+spiltVal[2]+","+spiltVal[3]+","+spiltVal[4]+","+spiltVal[5]+","+spiltVal[6]+",'"+spiltVal[1]+"',(select max(runid) from tblreportmaster where runid="+lRunId+"),"+spiltVal[7]+")";
					//System.out.println(strQry);
					stmt = con.createStatement();
					stmt.executeUpdate(strQry);
					spiltVal = null;
					strQry = null;
				}
				linecount=linecount+1;
			}
			
			line = null;
			LogManager.infoLog("Records created successfully");
	 
		} catch (FileNotFoundException e) {
			LogManager.errorLog(e);
		} catch (IOException e) {
			LogManager.errorLog(e);
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			UtilsFactory.close(br);
			DataBaseManager.close(stmt);
//			stmt= null;
		}
	 
	}
	
	/**
	 * to create & write the content to the specified xml file 
	 * @param fileContent
	 * @param filePath
	 * @throws Throwable
	 */
	public void writeToXmlFile(String fileContent,String filePath) throws Throwable {
		File file = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			
			file = new File(filePath);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bw.write(fileContent);
			bw.close();
			LogManager.infoLog("Done");

		} catch (Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
			if( file != null )
				file = null;
			if( fw != null)
				fw = null;
			if( bw != null )
				bw = null;
		}

	}
	
	public static void updateReportMaster(Connection con, int reportId,int createdUser,int completedUser,String strStatus,String strUId) throws Throwable {
		
		Statement stmt = null;
		String strQry="";
		try
		{
			if(strStatus.trim().equals("completed")){
				 strQry=" update tblreportmaster set uid='"+strUId+"', completedUser="+completedUser+" , createdUser="+createdUser+" , status='"+strStatus+"', runendtime=getgmtnow() where reportid="+reportId+" ";
			}else {
				strQry=" update tblreportmaster set uid='"+strUId+"', completedUser="+completedUser+" , createdUser="+createdUser+" , status='"+strStatus+"' where reportid="+reportId+" ";
			}
			LogManager.infoLog(strQry);
			//System.out.println("scenario time= " + country[0] + " , usercount=" + country[1] + "]");
			stmt = con.createStatement();
			stmt.executeUpdate(strQry);
			stmt.close();
			strQry = null;
		}
		catch(Throwable t)
		{
			LogManager.errorLog(t);
			throw t;
		}
	}
	
	public void updateReportMaster(Connection con, long lRunId, String status, String strNotes) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		int nPSIndex = 1;
		
		try {
			
			sbQuery	.append("UPDATE tblreportmaster SET is_active = false, runendtime = now(), status = ? ");
			if( strNotes != null ) {
				sbQuery	.append(", notes = (CASE WHEN notes IS NULL THEN '' ELSE notes||' ; ' END)||? ");
			}
			// if test is failed then, grafana-url (which will be /dashboard/appedo-jmeter-report) will be still available. So need to remove it.
			// if report is available then, it can be updated after snapahot is created.
			if( status.equals("FAILED") && status.equals("AUTO REPORT GENERATION FAILED") ) {
				sbQuery	.append(", grafana_report_url = '' ");
			}
			sbQuery	.append("WHERE runid = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(nPSIndex++, status);
			if( strNotes != null ) {
				pstmt.setString(nPSIndex++, strNotes);
			}
			pstmt.setLong(nPSIndex++, lRunId);
			
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	public void updateJMeterTestRunnningInReportMaster(Connection con, long lRunId) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		int nPSTMTIndex = 1;
		
		try {
			sbQuery	.append("UPDATE tblreportmaster SET is_active = true, status = 'RUNNING', ")
					.append("grafana_report_url = (?||'://'||loadgenipaddresses||':'||?||'/dashboard/db/'||?) ")	// TODO complete report-URL
					.append("WHERE runid = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(nPSTMTIndex++, Constants.GRAFANA_LOADGEN_PROTOCAL);
			pstmt.setInt(nPSTMTIndex++, Constants.GRAFANA_LOADGEN_PORT);
			pstmt.setString(nPSTMTIndex++, Constants.GRAFANA_DASHBOARD_DEFAULT_SLUG);
			pstmt.setLong(nPSTMTIndex++, lRunId);
			
			pstmt.executeUpdate();
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
	
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
	}
	
	public void completeJMeterTestInReportMaster(Connection con, long lRunId, String status, String strGrafanaReportKey ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			sbQuery	.append("UPDATE tblreportmaster SET is_active = false, runendtime = now(), ")
					// sometimes FAILED test's report could be generated. So status will look as "FAILED; AUTO REPORT GENERATION IS COMPLETED"
					.append("status = ( (CASE WHEN status IN ('FAILED') THEN status||'; ' ELSE '' END)||? ), ")
					.append("grafana_report_key = ?, ")
					.append("grafana_report_url = ? ")	// TODO complete Grafana snapshot URL
					.append("WHERE runid = ? ");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, status);
			pstmt.setString(2, strGrafanaReportKey);
			pstmt.setString(3, Constants.GRAFANA_COLLECTOR_PROTOCAL+"://"+Constants.GRAFANA_COLLECTOR_IP+":"+Constants.GRAFANA_COLLECTOR_PORT+"/dashboard/snapshot/"+strGrafanaReportKey);
			pstmt.setLong(4, lRunId);
			
			pstmt.executeUpdate();
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
	
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
	}
	
	/**
	 * to keep the running scenario report details into report master table
	 * @param con
	 * @param loginUserBean 
	 * @param reportName
	 * @param UserId
	 * @param strReportType
	 * @throws Throwable
	 */
	public long insertIntoReportMaster(Connection con, LoadTestSchedulerBean schedulerBean, LoginUserBean loginUserBean) throws Throwable {
		
		RunDBI runDbi = new RunDBI();
		long lRunId = -1L;
		boolean bExist = false;
		LTLicenseBean ltLicBean = null;
		
		try	{
			ltLicBean = getLTLicenseDetails(con, loginUserBean);
			bExist = runDbi.isReportNameExist(con, schedulerBean);
			if(bExist) {
				throw new Exception("ReportNameExist");
			} else {
				lRunId = runDbi.insertIntoReportMaster(con, ltLicBean, schedulerBean);
			}
			
		} catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
		
		return lRunId;
	}
	
	/**
	 * Gets user total run count for the current month
	 * 
	 * @param con
	 * @param loginUserBean
	 * @throws Exception
	 */
	public int getUserMaxRunCount(Connection con, LoginUserBean loginUserBean) throws Exception {
		RunDBI runDBI = null;
		int nUserTotalRuncount = 0;
		
		try {
			runDBI = new RunDBI();
			nUserTotalRuncount = runDBI.getUserMaxRunCount(con, loginUserBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return nUserTotalRuncount;
	}
	
	public int getUserMaxRunCountPerDay(Connection con, LoginUserBean loginUserBean) throws Exception {
		RunDBI runDBI = null;
		int nUserTotalRuncountPerDay = 0;
		
		try {
			runDBI = new RunDBI();
			nUserTotalRuncountPerDay = runDBI.getUserMaxRunCountPerDay(con, loginUserBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return nUserTotalRuncountPerDay;
	}

	/**
	 * get running scenario
	 * 
	 * @param con
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getRunningScenario(Connection con, LoginUserBean loginUserBean) throws Exception {
		RunDBI runDBI = null;
	
		JSONObject joRtnUserRunningScenario = null;
		try {
			runDBI = new RunDBI();
			joRtnUserRunningScenario = runDBI.getRunningScenario(con, loginUserBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return joRtnUserRunningScenario;
	}
	/**
	 * get user running scenario
	 * 
	 * @param con
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getUserRunningScenario(Connection con, LoginUserBean loginUserBean, String strReportType) throws Exception {
		RunDBI runDBI = null;
	
		JSONObject joRtnUserRunningScenario = null;
		LoadTestSchedulerBean schedulerBean = null;
		int nUserQueueSize = 0;
		try {
			runDBI = new RunDBI();
			joRtnUserRunningScenario = runDBI.getUserRunningScenario(con, loginUserBean, strReportType);
			Iterator<LoadTestSchedulerBean> it = null;
			if( strReportType.equalsIgnoreCase("JMETER") ){
				it = LTScheduler.htJmeterQueues.iterator();
			}else {
				if( loginUserBean.getLicense().equalsIgnoreCase("level0") ){
					it = LTScheduler.htFGTestQueues.iterator();
				}else{
					it = LTScheduler.htFGTestPaidQueues.iterator();
				}
			}
			
			while(it.hasNext()){
				schedulerBean = (LoadTestSchedulerBean)it.next();
				if(loginUserBean.getUserId()==schedulerBean.getUserId()){
					joRtnUserRunningScenario.clear();
					nUserQueueSize = nUserQueueSize + 1;
				}
				schedulerBean = null;
			}
//			runDBI = null;
			joRtnUserRunningScenario.put("userQueueSize", nUserQueueSize);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return joRtnUserRunningScenario;
	}
	public JSONObject isUserRunningScenario(Connection con, LoginUserBean loginUserBean, String strTestType ) throws Exception {
		RunDBI runDBI = null;
	
		JSONObject joRtnUserRunningScenario = null;
		try {
			runDBI = new RunDBI();
			joRtnUserRunningScenario = runDBI.isUserRunningScenario(con, loginUserBean, strTestType);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return joRtnUserRunningScenario;
	}
	public JSONArray getUserQueuedScenario(Connection con, LoginUserBean loginUserBean, String strTestType) throws Exception {
	
		JSONArray jaUserQueuedSceanarios = new JSONArray();
		JSONObject joQueuedSceanrio = null;
		LoadTestSchedulerBean schedulerBean = null;
		float fWaitingTime = 0.0f;
		try {
			Iterator<LoadTestSchedulerBean> it = null;
			if( strTestType.equalsIgnoreCase("JMETER") ){
				it = LTScheduler.htJmeterQueues.iterator();
			}else{
				if( loginUserBean.getLicense().equalsIgnoreCase("level0") ){
					it = LTScheduler.htFGTestQueues.iterator();
				}else{
					it = LTScheduler.htFGTestPaidQueues.iterator();
				}
			}
			
			while(it.hasNext()){
				schedulerBean = (LoadTestSchedulerBean)it.next();
				joQueuedSceanrio = new JSONObject();
				joQueuedSceanrio.put("runid", schedulerBean.getRunId());
				joQueuedSceanrio.put("reportName",schedulerBean.getReportName());
				joQueuedSceanrio.put("scenarioName", schedulerBean.getScenarioName());
				fWaitingTime = fWaitingTime+15.5f;
				joQueuedSceanrio.put("inTime", fWaitingTime);
				if(loginUserBean.getUserId()==schedulerBean.getUserId()){
					jaUserQueuedSceanarios.add(joQueuedSceanrio);
				}
//				joQueuedSceanrio = null;
//				schedulerBean = null;
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return jaUserQueuedSceanarios;
	}
	public JSONObject deleteFromQueue(Connection con, String strReportName, long lRunId, LoginUserBean loginUserBean) throws Throwable {
		
		JSONObject joMessage = null;
		LoadTestSchedulerBean schedulerBean = null;
		RunDBI runDBI = null;
		RunManager runManager = null;
		PriorityBlockingQueue<LoadTestSchedulerBean> pqLTScheduler = null;
		if( loginUserBean.getLicense().equalsIgnoreCase("level0") ){
			pqLTScheduler = LTScheduler.htFGTestQueues;
		}else{
			pqLTScheduler = LTScheduler.htFGTestPaidQueues;
		}
		synchronized (pqLTScheduler ) {
			try {
				Iterator<LoadTestSchedulerBean> it = pqLTScheduler.iterator();
				runDBI = new RunDBI();
				runManager = new RunManager();
				while(it.hasNext()){
					schedulerBean = (LoadTestSchedulerBean)it.next();
					if(schedulerBean.getReportName().equals(strReportName) && loginUserBean.getUserId() == schedulerBean.getUserId()){
						runDBI.updateQueuedScenario(con, strReportName);
						it.remove();
						// runManager.updateReportMaster(con, lRunId);
						runManager.updateInActiveLoadAgent(con, lRunId);
						joMessage = UtilsFactory.getJSONSuccessReturn("Load Test removed from Queue.");
					}else{
						joMessage = UtilsFactory.getJSONFailureReturn("Test is unavailable in Queue. Verify in Running/Completed Scenario.");
					}
					schedulerBean = null;
				}
//				runDBI = null;
//				runManager = null;
			} catch (Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		return joMessage;
	}
	/**
	 * Queue checking
	 * 
	 * @param lRunId
	 * @param schedulerBean
	 * @param joRunningScenario
	 * @return
	 * @throws Throwable
	 */
	public JSONObject runScenarioQueueChecking(Connection con, long lRunId, LoadTestSchedulerBean schedulerBean, LoginUserBean loginUserBean) throws Throwable {
		JSONObject joMessage = null, joRunningScenario = null;
		LTScheduler ltScheduler = new LTScheduler();
		RunManager runManager = new RunManager();
		
		synchronized (SEMAPHORE_RUN_LT) {
			Date da = (new Date());
			LogManager.infoLog("SEMAPHORE_RUN_LT: ThreadId: "+Thread.currentThread().getId()+" <> Date: "+da+" <> "+da.getTime());
			
			// gets running scenario
			joRunningScenario = runManager.getRunningScenario(con, loginUserBean);
			if( joRunningScenario.containsKey("isActive") && joRunningScenario.getBoolean("isActive") ) {
				ltScheduler.queueFGTest(schedulerBean);

				joMessage = UtilsFactory.getJSONSuccessReturn("Your test is on Queue and as soon as executed you can see the results under results ");
			} else {
				runManager.runScenario(lRunId, schedulerBean);
				joMessage = UtilsFactory.getJSONSuccessReturn("Test has started successfully. You can view results in the results window");
			}
			UtilsFactory.clearCollectionHieracy( joRunningScenario );
			da = (new Date());
			LogManager.infoLog("SEMAPHORE_RUN_LT: ThreadId: "+Thread.currentThread().getId()+" <> Date: "+da+" <> "+da.getTime());
		}
		
		
		return joMessage;
	}
	
	/**
	 * to initiate the run from appedo 
	 * @throws Throwable
	 */
public void runScenario(long lRunId, LoadTestSchedulerBean schedulerBean) throws Throwable {
		
		Socket scon = null;
		InetAddress address = null;
		OutputStream outStream = null;
		InputStream inStream = null;
		
		try	{
			
			/** Obtain an address object of the server */
			address = InetAddress.getByName(Constants.FG_APPLICATION_IP);
			/** Establish a socket connection */
			scon = new Socket(address, Integer.parseInt(Constants.FG_CONTROLLER_PORT1));
			/** Instantiate a BufferedOutputStream object */		 
			outStream = scon.getOutputStream();
			inStream=scon.getInputStream();
			
			String strScenarioName = "scenarioname="+schedulerBean.getScenarioName();
			String strScenarioId = "scenarioid="+schedulerBean.getScenarioId();
			String strReportName = "reportname="+schedulerBean.getReportName();
			String strUserId = "userid="+schedulerBean.getUserId();		
			String strRunId = "runid="+lRunId;
			String strLoadGenIp = "loadgenip="+schedulerBean.getLoadgenIpAddress();
			String strLoadGenRegions = "loadgenregions="+schedulerBean.getLoadGenRegions();
			String strLoadGenDistributions = "loadgendistributions="+schedulerBean.getDistributions();
			//scenarioid
			//runAppedo: 0
			//scenarioname=aaaa
			//scenarioid=3
			//reportname=testrep
			//userid=1
			//runid=122541
			//loadgenip=125242
			
			String strresponse="runAppedo: 0\r\n"+strScenarioName+"\r\n"+strScenarioId+"\r\n"+strReportName+"\r\n"+strUserId+"\r\n"+strRunId+"\r\n"+strLoadGenIp+"\r\n"+strLoadGenRegions+"\r\n"+strLoadGenDistributions+"\r\n\r\n";
			byte[] b=strresponse.getBytes();
			outStream.write(b, 0, b.length);
			
			LogManager.infoLog("run Started");
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}finally {
			outStream.close();
			inStream.close();
			scon.close();
		}
	}

	/**
	 * License details for free users to run load tests
	 * 
	 * @param strLicenseFilePath
	 */
	public JSONObject loadLoadTestLicenseDetails(Connection con, LoginUserBean userBean) {
		
		RunDBI runDBI = null;
		JSONObject joRtn = null;
		try {
			runDBI = new RunDBI();
			joRtn = runDBI.loadLoadTestLicenseDetails(con, userBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return joRtn;
	}
	/**
	 * inserting Scriptwisestatus 
	 * 
	 * @param con
	 * @param hm
	 * @throws Exception
	 */
	public void insertScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			
			runDBI.insertScriptwiseStatus(con, hm);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * updating ScriptwiseStatus
	 * @param con
	 * @param hm
	 * @throws Exception
	 */
	public void updateScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			
			runDBI.updateScriptwiseStatus(con, hm);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Checking for record
	 * 
	 * @param con
	 * @param strRunId
	 * @param strScriptId
	 * @return
	 * @throws Exception
	 */
	public boolean isRecordAvailable(Connection con, String strRunId, String strScriptId) throws Exception{
		boolean bExist = false;
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		try {
			sbQuery	.append("SELECT true from scriptwise_status where runid=? and script_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, Long.parseLong(strRunId));
			pstmt.setLong(2, Long.parseLong(strScriptId));
			
			rst = pstmt.executeQuery();
			
			if(rst.next()){
				bExist = true;
			}else{
				bExist = false;
			}
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
		return bExist;
	}
	/**
	 * getting running scripts
	 * 
	 * @param con
	 * @param joRunningScenario
	 * @return
	 * @throws Exception
	 */
	public JSONArray getRunningScriptData(Connection con, org.json.JSONObject joRunningScenario) throws Exception{
		
		RunDBI runDBI = null;
		JSONArray jaRunningScripts = null;
		try {
			runDBI = new RunDBI();
			
			jaRunningScripts = runDBI.getRunningScriptData(con, joRunningScenario);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return jaRunningScripts;
	}
	public JSONArray getCompletedScriptData(Connection con, org.json.JSONObject joCompletedScenario) throws Exception{
		
		RunDBI runDBI = null;
		JSONArray jaCompletedScripts = null;
		try {
			runDBI = new RunDBI();
			
			jaCompletedScripts = runDBI.getCompletedScriptData(con, joCompletedScenario);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return jaCompletedScripts;
	}
	public void updateActiveStatus(Connection con, int nRunid) throws Exception{
		
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateActiveStatus(con, nRunid);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	/**
	 * To append the variables data to variables.xml
	 * @param doc
	 * @param os
	 * @param bis
	 * @return
	 * @throws Throwable
	 */
	 public boolean appendVariable(Document doc,OutputStream os,InputStream bis,long lUserId) throws Throwable {
		 
		 RunManager runManager = new RunManager();
		 variablxmlPath = Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml";
		 LogManager.infoLog("variable xml file path:"+variablxmlPath);
		 Document responsexmlDoc=null;
		 boolean status=true;
		 try {
			 
			 if(isVariableXMLFileExist(variablxmlPath)){
				//return document for sending
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setNamespaceAware(true);
				DocumentBuilder builder = factory.newDocumentBuilder();
				responsexmlDoc = builder.newDocument();
				
				//appending the root element to final return document
				Element variablesElement = responsexmlDoc.createElement("variables");
				responsexmlDoc.appendChild(variablesElement);
				
			 	DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
				mdbf.setValidating(false);
				DocumentBuilder mdb = mdbf.newDocumentBuilder();
				// Document mdoc = mdb.parse(new FileInputStream(new File(variablxmlPath)));
				
				String strXml = FileUtils.readFileToString(new File(variablxmlPath), "UTF-8");
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
				Document mdoc = mdb.parse(byteArrayInputStream);
				
				NodeList mentries = mdoc.getElementsByTagName("variable");
				int mnum = mentries.getLength();
				
				NodeList listvariableNode = doc.getElementsByTagName("variable");
				int num = listvariableNode.getLength();
				
				if(mnum==0) {
					for (int a = 0; a < listvariableNode.getLength(); a++) {
						Node recNode=listvariableNode.item(a);
						Node en=recNode.cloneNode(true);
						mdoc.adoptNode(en);
						Node appendnode = mdoc.getElementsByTagName("variables").item(0);
						appendnode.appendChild(en);
						runManager.saveXmlDoc(mdoc, variablxmlPath);
					}
				}
				else
					{
						int res=0;
						for (int i=0; i<num; i++)	{				 
							 Node rnode = (Node) listvariableNode.item(i);
							 res=issamenodeAvailable(rnode,responsexmlDoc,i);	
							 if(res==1)
								 break;
						 }
					}
					
					// master variables document updation is over 
					// colllect list of all the updated variables
					XPath xPath = XPathFactory.newInstance().newXPath();
					String expression = "/variables/variable[@type='file']";
					NodeList recdocfiletypeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
					if(recdocfiletypeList.getLength()>0)	{
						
						// respond to client with xmldoc of file type variables
						
						for (int i = 0; i < recdocfiletypeList.getLength(); i++)	{
							Node rdn = (Node) recdocfiletypeList.item(i);
							
							Node aa=rdn.cloneNode(true);
							responsexmlDoc.adoptNode(aa);
							Node bb = responsexmlDoc.getElementsByTagName("variables").item(0);
							bb.appendChild(aa);
							//variablesElement.appendChild(aa);
						}
						
						/** Source document to get the script id's for selected scenario */
						
//						String txtbody = runManager.convertDocumentToString(responsexmlDoc).replace(System.getProperty("line.separator"), "");
//						//String txtbody="1";
//						LogManager.infoLog("text body "+txtbody);
//						String process ="OK: "+txtbody.length()+"\r\n\r\n"+txtbody;
//						byte[] responseBytes=process.getBytes();
//						
//						os.write(responseBytes, 0, responseBytes.length);
//						
//						LogManager.infoLog("doc if to response string:"+txtbody);
//						
					}
					
					else	{
							/** Source document to get the script id's for selected scenario */
//						String txtbody= runManager.convertDocumentToString(responsexmlDoc).replace(System.getProperty("line.separator"), "");
//						LogManager.infoLog("text body else "+txtbody);
//						String process ="OK: "+txtbody.length()+"\r\n\r\n"+txtbody;
//						byte[] responseBytes=process.getBytes();
//						
//						os.write(responseBytes, 0, responseBytes.length);
//						
//						LogManager.infoLog("doc else to response string:"+txtbody);
						
					}
			}
		 }catch(Throwable t)	{
			
				 status=false;
				 LogManager.errorLog(t);
				 throw t;
		 }
		 
		 return status;
	 }
	 
	/**
	 * to validate the given node is available in varaibles.xml
	 * @param rnode
	 * @param responsexmlDoc
	 * @param nodeid
	 * @return
	 * @throws Throwable
	 */
	public static int issamenodeAvailable(Node rnode,Document responsexmlDoc,int nodeid)throws Throwable {
		 
		 RunManager runManager = new RunManager();
		 int returnval=0;
		 try {
			DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
			mdbf.setValidating(false);
			DocumentBuilder mdb = mdbf.newDocumentBuilder();
			// Document mdoc = mdb.parse(new FileInputStream(new File(variablxmlPath)));
			String strXml = FileUtils.readFileToString(new File(variablxmlPath), "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			Document mdoc = mdb.parse(byteArrayInputStream);
			NodeList mentries = mdoc.getElementsByTagName("variable");
			int mnum = mentries.getLength();
			
			String recvnodeName=rnode.getAttributes().getNamedItem("name").getNodeValue();
			// make new xpath to get the selected node from the new xml document
			XPath xPath = XPathFactory.newInstance().newXPath();
			String expression = "/variables/variable[@name='"+recvnodeName+"']";
			LogManager.infoLog("ggg"+expression);
			Node innode = (Node) xPath.compile(expression).evaluate(mdoc, XPathConstants.NODE);		
			
			if(innode==null)// if the received not found then append 
			{
				 Node en=rnode.cloneNode(true);
				 // add a attribute receivedon attribute to received node
				 mdoc.adoptNode(en);
				 
				 Node appendnode = mdoc.getElementsByTagName("variables").item(0);
				 appendnode.appendChild(en);								
				 runManager.saveXmlDoc(mdoc , variablxmlPath);
			}
			else if(mnum>0 )// it the nod found 
			{
				LogManager.infoLog(innode.getAttributes().getNamedItem("modified").getNodeValue());
				LogManager.infoLog(rnode.getAttributes().getNamedItem("modified").getNodeValue());
				
				String masternodeVersion=innode.getAttributes().getNamedItem("modified").getNodeValue().trim();
				String receivednodeVersion=rnode.getAttributes().getNamedItem("modified").getNodeValue().trim();
				
				if(masternodeVersion.trim().equals(receivednodeVersion.trim()))
				{				// NO NEED TO ADD OR REMOVE
					LogManager.infoLog("inside is samnode function ");
					
				}
				else
				{
					// remove the node && append received node					
					//Element element = (Element) mdoc.getElementsByTagName("variable").item(nodeid);
					Node masternode=mdoc.getElementsByTagName("variable").item(nodeid);
					 // remove the specific node
					masternode.getParentNode().removeChild(masternode);
					 
					 Node en=rnode.cloneNode(true);
					 // add a attribute receivedon attribute to received node
					 mdoc.adoptNode(en);
					 
					 Node appendnode = mdoc.getElementsByTagName("variables").item(0);
					 appendnode.appendChild(en);								
					 runManager.saveXmlDoc(mdoc, variablxmlPath);			 
					 
					LogManager.infoLog("received node type="+rnode.getAttributes().getNamedItem("type").getNodeValue().trim());
					
					// check append node type is file then add node to responsxml
					if(rnode.getAttributes().getNamedItem("type").getNodeValue().trim().equals("file"))
					{			
					
						Node en1=rnode.cloneNode(true);
						 // add a attribute receivedon attribute to received node
						responsexmlDoc.adoptNode(en1);
						 
						Node appendnode1 = responsexmlDoc.getElementsByTagName("variables").item(0);
						appendnode1.appendChild(en1);								
					}
				}
			}
		 } catch(Throwable t) {
			 LogManager.errorLog(t);
			 throw t;
		}		
		 
		 return returnval;
	 }
	 
	/**
	 * to check FLOODGATES script file exists 
	 * @return
	 */
	public boolean isVariableXMLFileExist(String strvariablxmlPath){
		
		File f = null;
		boolean bool = false;
		
		try{
			//strJmeterScriptPath="C:/Appedo/resource/vuscripts/jmeter/vuscripts1.xml";	
			// create new files
			f = new File(strvariablxmlPath);
			
			// vuscripts.xml if file exists
			bool = f.exists();
			
			// prints
			LogManager.infoLog("File exists: "+bool);
			 
			 if(bool == false){
				 // create blank jmeter script file
				 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				 
				// root elements
				 Document doc = docBuilder.newDocument();
				 Element rootElement = doc.createElement("variables");
				 doc.appendChild(rootElement);
				 
				 // write the content into xml file
				 TransformerFactory transformerFactory = TransformerFactory.newInstance();
				 Transformer transformer = transformerFactory.newTransformer();
				 DOMSource source = new DOMSource(doc);
				 StreamResult result = new StreamResult(new File(strvariablxmlPath));
				 
				 // Output to console for testing
				 // StreamResult result = new StreamResult(System.out);
				 
				 transformer.transform(source, result);
			}
			 // tests if file exists
			 bool = f.exists();
			 // prints
			 System.out.print("File exists: "+bool);
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}
		
		return bool;
	}
	
	/**
	 * to append the document to vuscripts.xml 
	 * @param doc
	 * @throws Throwable
	 */
	public void appendXml(Document doc, long lUserId)throws Throwable {
		try {
			xmlfilePath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+lUserId+"_vuscripts.xml";
			FloodgatesXMLAppend floodgatesxmlappend = new FloodgatesXMLAppend();
			if (floodgatesxmlappend.isScriptFileExist(xmlfilePath)) {
				NodeList entries = doc.getElementsByTagName("vuscript");
				int num = entries.getLength();
				for (int i=0; i<num; i++) {
					Node aNode = (Node) entries.item(i);
					NamedNodeMap attributes = aNode.getAttributes();
					for (int a = 0; a < attributes.getLength(); a++) {
						Node theAttribute = attributes.item(a);
						if(theAttribute.getNodeName().equals("id")) {
							LogManager.infoLog(theAttribute.getNodeName() + "=" + theAttribute.getNodeValue());
							issamenodeExist(theAttribute.getNodeValue(),aNode);
						}
					}
				}
				doc.normalize();
			}
		}catch (Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}finally {
			if (input != null) {
				try {
					input.close();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * to validate the given node is available in Vuscripts.xml
	 * @param nodeid
	 * @param en1
	 * @return
	 * @throws Throwable
	 */
	public int issamenodeExist(String nodeid,Node en1)throws Throwable	{
		 
		RunManager runManager = new RunManager();
		try	{
		
			 LogManager.infoLog("xml:"+xmlfilePath);
			 Boolean nodeexist=false;
			 DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			 //get current date time with Date()
			 Date date = new Date();
			 
			 DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
			 mdbf.setValidating(false);
			 DocumentBuilder mdb = mdbf.newDocumentBuilder();
			 // Document mdoc = mdb.parse(new FileInputStream(new File(xmlfilePath)));
			 
			 String strXml = FileUtils.readFileToString(new File(xmlfilePath), "UTF-8");
			 ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			 Document mdoc = mdb.parse(byteArrayInputStream);
			 
			 NodeList mentries = mdoc.getElementsByTagName("vuscript");
			 int mnum = mentries.getLength();
			 
			 for (int i=0; i<mnum; i++)	{
				 
				 Element mnode = (Element) mentries.item(i);
				 NamedNodeMap mattributes = mnode.getAttributes();
				 
				 for (int a = 0; a < mattributes.getLength(); a++)	{
					 
					 Node themAttribute = mattributes.item(a);
					 
					 if(themAttribute.getNodeName().equals("id"))	{
						 
						 LogManager.infoLog(themAttribute.getNodeName() + "=" + themAttribute.getNodeValue());
						 String nv=themAttribute.getNodeValue();
						 // remove node
						 if(nv.equals(nodeid))	{
							 
							 LogManager.infoLog("Node Exist"+nv);
							 // remove the specific node
							 Element element = (Element) mdoc.getElementsByTagName("vuscript").item(i);
							 // remove the specific node
							 element.getParentNode().removeChild(element);
							 Node en=en1.cloneNode(true);
							// add a attribute receivedon attribute to received node
							 Element ve=(Element)en;
							 ve.setAttribute("receivedon", dateFormat.format(date));
							 en =(Node)ve;
							 mdoc.adoptNode(en); 
							
							 Node appendnode = mdoc.getElementsByTagName("vuscripts").item(0);
							 appendnode.appendChild(en);
							 runManager.saveXmlDoc(mdoc, xmlfilePath);
							 
							 nodeexist=true;
							 break;
						}
					}
				}
			}
			 if(nodeexist==false)	{			 
				 Node en=en1.cloneNode(true);
				 // add a attribute receivedon attribute to received node 
				 Element ve=(Element)en;
				 ve.setAttribute("receivedon", dateFormat.format(date));
				 en =(Node)ve;				 
				 mdoc.adoptNode(en);		 
				 Node appendnode = mdoc.getElementsByTagName("vuscripts").item(0);
				 appendnode.appendChild(en);				 
				 runManager.saveXmlDoc(mdoc, xmlfilePath);
			}
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}
		 return 1;
	}
		
	/**
	 * Deleting completed Load Test.
	 * 
	 * @param con
	 * @param lReportId
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject deleteCompletedLoadTest(Connection con, long lReportId, LoginUserBean loginUserBean) throws Exception {
		 
		 RunDBI runDBI = null;
		 JSONObject joMessage = null;
		 try{
			 runDBI = new RunDBI();
			 joMessage = runDBI.deleteCompletedLoadTest(con, lReportId, loginUserBean);
		 }catch(Exception e){
			 LogManager.errorLog(e);
		 }finally{
			 runDBI = null;
		 }
		 return joMessage;
	}
	
	/**
	 * updating load test records on server starting.
	 * @param con
	 * @throws Exception
	 */
	public void updateLoadTestRecords(Connection con) throws Exception{
		 
		 RunDBI runDBI = null;
		 try {
			 runDBI = new RunDBI();
			 runDBI.updateLoadTestRecords(con);
//			 runDBI = null;
		 }catch(Exception e) {
			 LogManager.errorLog(e);
		 }
	}
	 
	public JSONObject getInstanceDetails(Connection con, String strmodule) throws Exception{
		 JSONObject joInstanceDetails = new JSONObject();
		 RunDBI runDBI = null;
			try {
				runDBI = new RunDBI();
				joInstanceDetails = runDBI.getInstanceDetails(con, strmodule);
//				runDBI = null;
			} catch (Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
			return joInstanceDetails;
	}
	 
	public void insertLoadAgentDetails(Connection con, HashMap<String, String> hm) throws Exception {
			RunDBI runDBI = null;
			try {
				runDBI = new RunDBI();
				runDBI.insertLoadAgentDetails(con, hm);
//				runDBI = null;
			} catch (Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
	}
	
	public JSONArray getDynamicLoadAgentDetails(Connection con) throws Exception{
			
			RunDBI runDBI = null;
			JSONArray jaRunningScripts = null;
			try {
				runDBI = new RunDBI();
				
				jaRunningScripts = runDBI.getDynamicLoadAgentDetails(con);
//				runDBI = null;
			} catch (Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
			return jaRunningScripts;
	}
	
	public void terminateInstance(Connection con, String region, String instanceId) throws Throwable {
		
		try	{
			updateInstanceDetails(con, instanceId);
			
			AmazonEC2Client ec2 = getAmazonClient(con);
			ec2.setEndpoint(region);
			List<String> instanceIds = new ArrayList<String>();
			instanceIds.add(instanceId);
			TerminateInstancesRequest tir = new TerminateInstancesRequest(instanceIds);
			ec2.terminateInstances(tir);
			LogManager.infoLog("Sent for Terminating Instance");
		} catch(Throwable t) {
			LogManager.errorLog(t);
		}
	}
	
	private AmazonEC2Client getAmazonClient(Connection con) {
		AmazonEC2Client ec2 = null;
		try {
			JSONObject instance_config = getInstanceDetails(con, "LT");
			ec2 = new AmazonEC2Client(new BasicAWSCredentials(instance_config.getString("access_key"), instance_config.getString("secret_access_key")));
			instance_config = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return ec2;
	}

	
	public void updateNextCheckTime(Connection con, String instanceId, long curNextTimeValue) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateNextCheckTime(con, instanceId, curNextTimeValue);
//			runDBI = null;
		} catch (Exception e) {
			
		}
	}
	
	public void updateInstanceDetails(Connection con, String instanceId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateInstanceDetails(con, instanceId);
//			runDBI = null;
		} catch (Exception e) {
			
		}
	}
	
	public JSONArray getInactiveLoadAgentDetails(Connection con, String region, String osType, String ipAddress)throws Throwable {
		JSONArray jaInstanceStatus = new JSONArray();
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			jaInstanceStatus = runDBI.getInactiveLoadAgentDetails(con, region, osType, ipAddress);
//			runDBI = null;
		} catch (Exception e) {
			
		}
		return jaInstanceStatus;
	}
	
	public JSONObject getRegionDetails(Connection con, String region, String osType)throws Throwable {
		JSONObject joRegionDetails = new JSONObject();
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			joRegionDetails = runDBI.getRegionDetails(con, region, osType);
//			runDBI = null;
		} catch (Exception e) {
			
		}
		return joRegionDetails;
	}
	
	public void updateLoadAgentDetails(Connection con, String public_ip, String instanceId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateLoadAgentDetails(con, public_ip, instanceId);
//			runDBI = null;
		} catch (Exception e) {
			
		}
	}
	
	public void updateNonAvailabilityRegion(Connection con, String status, LoadTestSchedulerBean loadTestSchedulerBean) throws Exception{
		
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateNonAvailabilityRegion(con, status, loadTestSchedulerBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public int getMaxUserCount(Connection con, LoadTestSchedulerBean loadTestSchedulerBean) throws Exception {
		RunDBI runDBI = null;
	
		int nUserTotalRuncount = 0;
		
		try {
			runDBI = new RunDBI();
			nUserTotalRuncount = runDBI.getMaxUserCount(con, loadTestSchedulerBean);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return nUserTotalRuncount;
	}
	
	public int updateInstanceAvailability(Connection con, String region_endpoint, String accesskey, String secretkey, String osType )throws Throwable {
		RunDBI runDBI = null;
		int availability = 0;
		try {
			runDBI = new RunDBI();
			availability = runDBI.updateInstanceAvailability(con, region_endpoint, accesskey, secretkey, osType);
//			runDBI = null;
		} catch (Exception e) {
			
		}
		return availability;
	}
	
	public void insertRunGenDetails(Connection con, LoadTestSchedulerBean loadTestSchedulerBean, String instanceId) throws Exception {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertRunGenDetails(con, loadTestSchedulerBean, instanceId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public void updateInActiveLoadAgent(Connection con, long lRunId) throws Throwable{
		
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateInActiveLoadAgent(con, lRunId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public int getLoadAgentDetailsCount(Connection con, String region)throws Throwable {
		int nTotalDynamicCount = 0;
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			nTotalDynamicCount = runDBI.getLoadAgentDetailsCount(con, region);
//			runDBI = null;
		} catch (Exception e) {
			
		}
		return nTotalDynamicCount;
	}
	//isJMeterTestRunning
	
	public boolean isJMeterTestRunning(Connection con, String runId)throws Throwable {
		boolean isRunning = false;
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			isRunning = runDBI.isJMeterTestRunning(con, runId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return isRunning;
	}
	
	public String getJMeterRunningIP(Connection con, String runId)throws Throwable {
		String runningIP = "";
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runningIP = runDBI.getJMeterRunningIP(con, runId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return runningIP;
	}
	
	public String ltLicenseInstanceDetails(Connection con, String ltType) {
		
		RunDBI runDBI = null;
		String joRtn = null;
		try {
			runDBI = new RunDBI();
			joRtn = runDBI.ltLicenseInstanceDetails(con, ltType);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return joRtn;
	}
	/**
	 * Getting failed scenarios
	 * 
	 * @param con
	 * @param strLimit
	 * @param strOffset
	 * @param loginUserBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getFailedScenarios(Connection con, String strLimit, String strOffset, LoginUserBean loginUserBean, String strTestType) throws Exception {
		
		JSONObject joFailedScenarios = null;
		RunDBI runDBI = null;
		
		try {
			runDBI = new RunDBI();
			joFailedScenarios = runDBI.getFailedScenarios(con, Integer.parseInt(strLimit), Integer.parseInt(strOffset), loginUserBean, strTestType);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return joFailedScenarios;
	}
	
	public void updateDynamicLADetails(Connection con) throws Exception{
		 
		 RunDBI runDBI = null;
		 try {
			 runDBI = new RunDBI();
			 runDBI.updateDynamicLADetails(con);
//			 runDBI = null;
		 }catch(Exception e) {
			 LogManager.errorLog(e);
		 }
	 }
	
	public void updateRemarksInReportMaster(Connection con, long lRunId, String notes, String status) throws Exception{
		 
		 RunDBI runDBI = null;
		 try {
			 runDBI = new RunDBI();
			 runDBI.updateRemarksInReportMaster(con, lRunId, notes, status);
//			 runDBI = null;
		 }catch(Exception e) {
			 LogManager.errorLog(e);
		 }
	 }
	
	public String getInstanceType(Connection con, int max_users, String osType) {
		
		RunDBI runDBI = null;
		String joRtn = null;
		try {
			runDBI = new RunDBI();
			joRtn = runDBI.getInstanceType(con, max_users, osType);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return joRtn;
	}
	
	public static void main(String args[]){
		try {
			Socket socketConn = new Socket("54.167.157.172", 8080);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public JSONObject getDashboardLoadTestData(Connection con, LoginUserBean loginUserBean) throws Exception {
		
		RunDBI runDBI = null;
		JSONObject joData = null, joTemp = null;
		JSONArray jaTotalCount = null, jaCounts = null;
		try {
			runDBI = new RunDBI();
			joData = new JSONObject();
			jaTotalCount = runDBI.getDashboardLoadTestData(con, loginUserBean);
			jaCounts = getScriptsAndScenarios(loginUserBean);
			if(jaTotalCount.size()>0){
				for(int i=0; i<jaCounts.size(); i++){
					joTemp = jaTotalCount.getJSONObject(i);
					if(joTemp.getString("reporttype").equals("APPEDO_LT")){
						joData.put("appedototalcount", joTemp.getInt("totalcount"));
						joData.put("appedomonthcount", joTemp.getInt("monthcount"));
					}else if(joTemp.getString("reporttype").equals("JMETER")){
						joData.put("jmetertotalcount", joTemp.getInt("totalcount"));
						joData.put("jmetermonthcount", joTemp.getInt("monthcount"));
					}
//					joTemp = null;
				}
			}else{
				joData.put("appedototalcount", 0);
				joData.put("appedomonthcount", 0);
				joData.put("jmetertotalcount", 0);
				joData.put("jmetermonthcount", 0);
			}
			
			
			for(int i=0; i<jaCounts.size(); i++){
				joTemp = jaCounts.getJSONObject(i);
				if(joTemp.getString("reporttype").equals("APPEDO_LT")){
					joData.put("appedoscriptCount", joTemp.getInt("scriptCount"));
					joData.put("appedoscenarioCount", joTemp.getInt("scenarioCount"));
				}else if(joTemp.getString("reporttype").equals("JMETER")){
					joData.put("jmeterscriptCount", joTemp.getInt("scriptCount"));
					joData.put("jmeterscenarioCount", joTemp.getInt("scenarioCount"));
				}
//				joTemp = null;
			}
//			runDBI = null;
//			jaTotalCount = null;
//			jaCounts = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return joData;
	}
	
	public JSONArray getScriptsAndScenarios(LoginUserBean loginUserBean) throws Exception {
		
		String strScenarioXmlPath = null;
		String strVUScriptPath = null;
		FloodgatesXMLAppend xmlAppend = null;
		JSONArray jaScriptsScenarios = new JSONArray();
		JSONObject joSS = null;
		try {
			xmlAppend = new FloodgatesXMLAppend();
			
			strScenarioXmlPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
			strVUScriptPath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
			joSS = new JSONObject();
			joSS.put("reporttype", "APPEDO_LT");
			joSS.put("scriptCount", xmlAppend.getNoofScriptsScenarios(strVUScriptPath, "vuscript"));
			joSS.put("scenarioCount", xmlAppend.getNoofScriptsScenarios(strScenarioXmlPath, "scenario"));
			jaScriptsScenarios.add(joSS);
			joSS = null;
			
			strScenarioXmlPath = Constants.JMETERSCENARIOFOLDERPATH+File.separator+loginUserBean.getUserId()+"_scenarios.xml";
			strVUScriptPath = Constants.JMETERVUSCRIPTSFOLDERPATH+File.separator+loginUserBean.getUserId()+"_vuscripts.xml";
			joSS = new JSONObject();
			joSS.put("reporttype", "JMETER");
			joSS.put("scriptCount", xmlAppend.getNoofScriptsScenarios(strVUScriptPath, "vuscript"));
			joSS.put("scenarioCount", xmlAppend.getNoofScriptsScenarios(strScenarioXmlPath, "scenario"));
			jaScriptsScenarios.add(joSS);
			
//			strScenarioXmlPath = null;
//			strVUScriptPath = null;
//			xmlAppend = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return jaScriptsScenarios;
	}
	
	/**
	 * This method populates all the files in a directory to a List
	 * @param dir
	 * @throws IOException
	 */
	private void populateFilesList(File dir, List<String> filesListInDir) throws IOException {
		File[] files = dir.listFiles();
		for(File file : files){
			if(file.isFile()) 
				filesListInDir.add(file.getAbsolutePath());
			else 
				populateFilesList(file, filesListInDir);
		}
	}
	
		
	/**
	 * This method zips the directory
	 * @param dir
	 * @param zipDirName
	 */
	public void zipDirectory(File dir, String zipDirName) {
		List<String> filesListInDir = new ArrayList<String>();
		try {
			populateFilesList(dir, filesListInDir);
			//now zip files one by one
			//create ZipOutputStream to write to the zip file
			FileOutputStream fos = new FileOutputStream(zipDirName);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for(String filePath : filesListInDir){
//				System.out.println("Zipping "+filePath);
				//for ZipEntry we need to keep only relative file path, so we used substring on absolute path
				ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
				zos.putNextEntry(ze);
				//read the file and write to ZipOutputStream
				FileInputStream fis = new FileInputStream(filePath);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * to delete the specified directory
	 * @param directory
	 * @return
	 */
	public boolean removeDirectory(File directory) {

		// System.out.println("removeDirectory " + directory);

		if (directory == null)
			return false;
		if (!directory.exists())
			return true;
		if (!directory.isDirectory())
			return false;

		String[] list = directory.list();

		// Some JVMs return null for File.list() when the
		// directory is empty.
		if (list != null) {
			for (int i = 0; i < list.length; i++) {
			File entry = new File(directory, list[i]);

			//		System.out.println("\tremoving entry " + entry);

			if (entry.isDirectory())
			{
				if (!removeDirectory(entry))
				return false;
			}
			else
			{
				if (!entry.delete())
				return false;
			}
			}
		}

		return directory.delete();
	}
	
	public StringBuilder getAvailableVarables(long lUserId) {
		
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docVariableXml = null;
		NodeList nlVariable = null;
		StringBuilder sb = new StringBuilder();
		try {
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			// docVariableXml = db.parse(new FileInputStream(new File(Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml")));
			
			String strXml = FileUtils.readFileToString(new File(Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml"), "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			docVariableXml = db.parse(byteArrayInputStream);
			
			if(docVariableXml!=null) {
				
				nlVariable = docVariableXml.getElementsByTagName("variable");
				int nVariableCount = nlVariable.getLength();
				
				for (int i=0; i<nVariableCount; i++) {
					
					Node nodeVariable = (Node) nlVariable.item(i);				
					NamedNodeMap mattributes = nodeVariable.getAttributes();
					for (int a = 0; a < mattributes.getLength(); a++) {
						
						Node themAttribute = mattributes.item(a);
						if(themAttribute.getNodeName().equals("name")) {
							sb.append(themAttribute.getNodeValue());
							sb.append(",");
						}
					}
				}
				
				
				
			}
			
		}catch(Throwable th) {
			LogManager.errorLog(th);
		}finally {
//			dbf = null;
//			db = null;
		}
		
		return sb;
		
		
	}
	/**
	 * to extract zip file
	 * @param fileZip
	 * @param fileDestination
	 * @throws IOException
	 */
	public static void unzip(File fileZip, File fileDestination) throws IOException {
		ZipFile zip=new ZipFile(fileZip);
		
		Enumeration<ZipArchiveEntry> e = zip.getEntries();
		while (e.hasMoreElements()) {
			ZipArchiveEntry entry = e.nextElement();
			File file = new File(fileDestination, entry.getName());
			if (entry.isDirectory()) {
				file.mkdirs();
			} else {
				InputStream is=zip.getInputStream(entry);
				File parent=file.getParentFile();
				if (parent != null && parent.exists() == false) {
					parent.mkdirs();
				}
				FileOutputStream os=new FileOutputStream(file);
				try {
					IOUtils.copy(is,os);
				} finally {
					os.close();
					is.close();
				}
				file.setLastModified(entry.getTime());
				
			}
		}
		
	 zip.close();
	}
	
	/**
	 * To check for script availablity in the table
	 * @param con 
	 * @param enterpriseId 
	 * @param reportName
	 * @param csvPath
	 */
	public long checkScriptExist(Connection con, long lUserId, String strScriptName, int enterpriseId, boolean bVUScript) throws Exception {
		
		RunDBI runDBI = null;
		long lScriptId = -1l;
		try {
			runDBI = new RunDBI();
			lScriptId = runDBI.checkScriptExist(con, lUserId, strScriptName, enterpriseId , bVUScript);
//			runDBI = null;
	 
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
//			runDBI = null;			
		}	 
		LogManager.infoLog("Done");
		
		return lScriptId;
	}
	
	/**
	 * To get the available scripts for given user
	 * @param con 
	 * @param lUserId
	 * @return
	 */
	public StringBuilder getAvailableScripts(Connection con, long lUserId) {
		
		
		RunDBI runDBI = null;
		StringBuilder sbResult = new StringBuilder();
		try {
			runDBI = new RunDBI();
			sbResult = runDBI.getAvailableScripts(con, lUserId);
			
			
	 
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
//			runDBI = null;			
		}	 
		
		return sbResult;
		
	}
	
	/**
	 * to create a file called "VUScript.xml"
	 * @param lScriptId
	 * @param lUserId
	 * @param nodeVuScript
	 * @return
	 */
	public boolean createVUScriptXml(long lScriptId, long lUserId, Node nodeVuScript) {
		
		
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		File fVUScript = null;
		boolean bStatus = false;
		
		try {
			
			String strScriptSavePath = lUserId+"_"+lScriptId;
			String strVUScriptXmlFilePath = Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+strScriptSavePath+File.separator+"vuscript.xml";
			
			fVUScript = new File(strVUScriptXmlFilePath);
			// vuscripts.xml if file exists
			if(fVUScript.exists()) {
			fVUScript.delete();
			}
			
			if(!fVUScript.exists()) {
			
			docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
			// root elements
			Document doc = docBuilder.newDocument();
			//Element rootElement = doc.createElement("root");
			//doc.appendChild(rootElement);
			
			// child elements
			//Element childElement = doc.createElement("vuscripts");
			if(nodeVuScript!=null) {
			 Node en=nodeVuScript.cloneNode(true);
			 doc.adoptNode(en);
			 doc.appendChild(en);
			 //childElement.appendChild(en);
			}
				//rootElement.appendChild(childElement);
				
				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(strVUScriptXmlFilePath));
				transformer.transform(source, result);
				 }
				// tests if file exists
			bStatus = fVUScript.exists();
//				 fVUScript = null;
				 
			
		}catch(Exception e){
			LogManager.errorLog(e);
		}
		
		return bStatus;
	}
	
	public int getCreatedInstanceCount(Connection con, String region, String osType)throws Throwable {
		int nTotalDynamicCount = 0;
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			nTotalDynamicCount = runDBI.getCreatedInstanceCount(con, region, osType);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return nTotalDynamicCount;
	}
	
	public void insertLogDetails(Connection con, JSONArray ltLogs, long lRunId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertLogDetails(con, ltLogs, lRunId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void insertErrorDetails(Connection con, JSONArray ltErrors, long lRunId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertErrorDetails(con, ltErrors, lRunId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void insertReportDetails(Connection con, JSONArray ltReportDatas, long lRunId, int is_completed, String loadgens) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertReportDetails(con, ltReportDatas, lRunId, is_completed, loadgens);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void insertTransactionDetails(Connection con, JSONArray ltTrans, long lRunId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertTransactionDetails(con, ltTrans, lRunId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void insertRunTimeCount(Connection con, JSONArray ltUserDetails, long lRunId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertRunTimeCount(con, ltUserDetails, lRunId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}		
	}
	
	public void updateReportMasterLoadGen(Connection con, long lRunId, String loadgen) throws Exception{
		 
		 RunDBI runDBI = null;
		 try {
			 runDBI = new RunDBI();
			 runDBI.updateReportMasterLoadGen(con, lRunId, loadgen);
//			 runDBI = null;
		 }catch(Exception e) {
			 LogManager.errorLog(e);
		 }
	 }
	
	public HashMap<Long, String> getScriptId(long lUserId, String strScenarioId) throws Throwable {
		
		String strExpression = null;
		String strScenarioXmlDocPath = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docScenario = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docScenarioXml = null;
		Element rootElement = null;
		XPath xPath = null;
		NodeList nodeList = null;
		Node aNode = null;
		NamedNodeMap attributes = null;
		Node theAttribute = null;
		String scriptName = null;
		long scriptId = 0;
		HashMap<Long, String> hmap = new HashMap<Long, String>();
		
		try {

			// to keep the scenario's xml file path 
			strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			//return document for sending
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docScenario = builder.newDocument();
			
			
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			
			// docScenarioXml = db.parse(new FileInputStream(new File(strScenarioXmlDocPath)));
			String strXml = FileUtils.readFileToString(new File(strScenarioXmlDocPath), "UTF-8");
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(strXml.getBytes("UTF-8"));
			docScenarioXml = db.parse(byteArrayInputStream);
			strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			// get the list of scripts under given scenario
			strExpression = "//root/scenarios/scenario[@id='"+strScenarioId+"']/script";
			
			//appending the root element to final return document
			rootElement = docScenario.createElement("root");
			docScenario.appendChild(rootElement);
			
			xPath = XPathFactory.newInstance().newXPath();
			nodeList = (NodeList) xPath.compile(strExpression).evaluate(docScenarioXml, XPathConstants.NODESET);
				
				//System.out.println("fdF"+nodeList.getLength());
				for (int i = 0; i < nodeList.getLength(); i++) {
					aNode = (Node) nodeList.item(i);
					attributes = aNode.getAttributes();
					for (int a = 0; a < attributes.getLength(); a++) {
						theAttribute = attributes.item(a);
						if( theAttribute.getNodeName().equals("id") ){
							scriptId = Long.valueOf(theAttribute.getNodeValue());
						}
						if( theAttribute.getNodeName().equals("name") ){
							scriptName = theAttribute.getNodeValue();
						}
					}
					
					if( scriptName != null && scriptId != 0){
						hmap.put(scriptId, scriptName);
//						scriptName = null;
						scriptId = 0;
					}
				}
				
		}catch(Throwable th) {
			LogManager.errorLog(th);
			
		}finally {
			
//			strExpression = null;
//			strScenarioXmlDocPath = null;
//			factory = null;
//			builder = null;
//			dbf = null;
//			db = null;
//			docScenarioXml = null;
//			rootElement = null;
//			xPath = null;
//			nodeList = null;
//			aNode = null;
//			attributes = null;
//			theAttribute = null;
//			scriptName = null;
		}		
		return hmap;
	}
	
	public void insertRunScriptDetails(Connection con, long lRunId, HashMap<Long, String> hmap) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertRunScriptDetails(con, lRunId, hmap);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void createSummaryTable(Connection con, long lRunId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.createSummaryTable(con, lRunId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void ltSummaryReport(Connection con, long lRunId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.ltSummaryReport(con, lRunId);
//			runDBI = null;
			WriteXMLDocument(con, lRunId);
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void WriteXMLDocument(Connection con, long lRunId){
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmtSummary = null, pstmtScript = null, pstmtReq = null, pstmtRes = null, pstmtCont = null, pstmtErr = null, pstmtCode = null, pstmt = null;
		ResultSet rstSummary = null, rstScript = null, rstReq = null, rstRes = null, rstCont = null, rstErr = null, rstCode = null, rst = null;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("report");
			doc.appendChild(rootElement);
			
			// First Child
			Element summaryreportElement = doc.createElement("summaryreport");
			rootElement.appendChild(summaryreportElement);
			
			sbQuery.append("SELECT * FROM lt_summaryreport_"+lRunId);
			pstmtSummary = con.prepareStatement(sbQuery.toString());
			rstSummary = pstmtSummary.executeQuery();
			if(rstSummary.next()){
				Element valElement = doc.createElement("val");
				valElement.setAttribute("usercount", String.valueOf(rstSummary.getInt("usercount")));
				valElement.setAttribute("totalthroughput", String.valueOf(rstSummary.getDouble("totalthroughput")));
				valElement.setAttribute("totalpage", String.valueOf(rstSummary.getInt("totalpage")));
				valElement.setAttribute("totalhits", String.valueOf(rstSummary.getInt("totalhits")));
				valElement.setAttribute("totalerrors", String.valueOf(rstSummary.getString("totalerrors")));
				valElement.setAttribute("starttime", String.valueOf(rstSummary.getTimestamp("starttime")));
				valElement.setAttribute("reponse500", String.valueOf(rstSummary.getInt("response500")));
				valElement.setAttribute("reponse400", String.valueOf(rstSummary.getInt("response400")));
				valElement.setAttribute("reponse300", String.valueOf(rstSummary.getInt("response300")));
				valElement.setAttribute("reponse200", String.valueOf(rstSummary.getInt("response200")));
				valElement.setAttribute("endtime", String.valueOf(rstSummary.getTimestamp("endtime")));
				valElement.setAttribute("durationsec", String.valueOf(rstSummary.getInt("durationsec")));
				valElement.setAttribute("avgthroughput", String.valueOf(rstSummary.getDouble("avgthroughput")));
				valElement.setAttribute("avgresponse", String.valueOf(rstSummary.getDouble("avgresponse")));
				valElement.setAttribute("avgpageresponse", String.valueOf(rstSummary.getDouble("avgpageresponse")));
				valElement.setAttribute("avghits", String.valueOf(rstSummary.getDouble("avghits")));
				
				summaryreportElement.appendChild(valElement);
			}
			
			sbQuery = new StringBuilder();
			sbQuery.append("SELECT script_id, script_name FROM lt_run_script_details WHERE runid = "+lRunId);
			pstmtScript = con.prepareStatement(sbQuery.toString());
			rstScript = pstmtScript.executeQuery();
			while(rstScript.next()){
				// Script Child
				Element scriptElement = doc.createElement("script");
				rootElement.appendChild(scriptElement);
				
				scriptElement.setAttribute("id", String.valueOf(rstScript.getInt("script_id")));
				scriptElement.setAttribute("name", rstScript.getString("script_name"));
				
				Element requestsElement = doc.createElement("requests");
				scriptElement.appendChild(requestsElement);
				
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM requests_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmtReq = con.prepareStatement(sbQuery.toString());
				rstReq = pstmtReq.executeQuery();
				while(rstReq.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("requestid", String.valueOf(rstReq.getInt("requestid")));
					valElement.setAttribute("containername", rstReq.getString("containername"));
					valElement.setAttribute("containerid", String.valueOf(rstReq.getInt("containerid")));
					valElement.setAttribute("address", rstReq.getString("address")	);
					requestsElement.appendChild(valElement);
				}
				
				Element responseElement = doc.createElement("requestresponse");
				scriptElement.appendChild(responseElement);
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM requestresponse_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmtRes = con.prepareStatement(sbQuery.toString());
				rstRes = pstmtRes.executeQuery();
				while(rstRes.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("requestid", String.valueOf(rstRes.getInt("requestid")));
					valElement.setAttribute("containername", rstRes.getString("containername"));
					valElement.setAttribute("containerid", String.valueOf(rstRes.getInt("containerid")));
					valElement.setAttribute("address", rstRes.getString("address")	);
					valElement.setAttribute("throughput", String.valueOf(rstRes.getDouble("throughput")));
					valElement.setAttribute("min", String.valueOf(rstRes.getDouble("minvalue")));
					valElement.setAttribute("max", String.valueOf(rstRes.getDouble("maxvalue")));
					valElement.setAttribute("hitcount", String.valueOf(rstRes.getInt("hitcount")));
					valElement.setAttribute("avg", String.valueOf(rstRes.getDouble("avgvalue")));
					responseElement.appendChild(valElement);
				}
				
				Element containerResponseElement = doc.createElement("containerresponse");
				scriptElement.appendChild(containerResponseElement);
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM containerresponse_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmtCont = con.prepareStatement(sbQuery.toString());
				rstCont = pstmtCont.executeQuery();
				while(rstCont.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("containername", rstCont.getString("containername"));
					valElement.setAttribute("containerid", String.valueOf(rstCont.getInt("containerid")));
					valElement.setAttribute("min", String.valueOf(rstCont.getDouble("minvalue")));
					valElement.setAttribute("max", String.valueOf(rstCont.getDouble("maxvalue")));
					valElement.setAttribute("avg", String.valueOf(rstCont.getDouble("avgvalue")));
					containerResponseElement.appendChild(valElement);
				}
				
				Element errorCountElement = doc.createElement("errorcount");
				scriptElement.appendChild(errorCountElement);
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM errorcount_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmtErr = con.prepareStatement(sbQuery.toString());
				rstErr = pstmtErr.executeQuery();
				while(rstErr.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("requestid", String.valueOf(rstErr.getInt("requestid")));
					valElement.setAttribute("containername", rstErr.getString("containername"));
					valElement.setAttribute("containerid", String.valueOf(rstErr.getInt("containerid")));
					valElement.setAttribute("address", rstErr.getString("address")	);
					valElement.setAttribute("count", String.valueOf(rstErr.getInt("errorcount")));
					errorCountElement.appendChild(valElement);
				}
				
				Element errorCodeElement = doc.createElement("errorcode");
				scriptElement.appendChild(errorCodeElement);
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM errorcode_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmtCode = con.prepareStatement(sbQuery.toString());
				rstCode = pstmtCode.executeQuery();
				while(rstCode.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("message", rstCode.getString("message")	);
					valElement.setAttribute("errorcode", rstCode.getString("errorcode"));
					valElement.setAttribute("count", String.valueOf(rstCode.getInt("errorcount")));
					errorCodeElement.appendChild(valElement);
				}
				
				Element transactionsElement = doc.createElement("transactions");
				scriptElement.appendChild(transactionsElement);
				sbQuery = new StringBuilder();
				sbQuery.append("SELECT * FROM transactions_"+lRunId+"_"+rstScript.getInt("script_id"));
				pstmt = con.prepareStatement(sbQuery.toString());
				rst = pstmt.executeQuery();
				while(rst.next()){
					Element valElement = doc.createElement("val");
					valElement.setAttribute("transactionname", rst.getString("transactionname")	);
					valElement.setAttribute("min", String.valueOf(rst.getDouble("minvalue")));
					valElement.setAttribute("max", String.valueOf(rst.getDouble("maxvalue")));
					valElement.setAttribute("avg", String.valueOf(rst.getDouble("avgvalue")));
					transactionsElement.appendChild(valElement);
				}
			}
				 
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(Constants.SUMMARYREPORTPATH+lRunId+".xml"));
			 /* Output to console for testing */
			 // StreamResult result = new StreamResult(System.out);
			 transformer.transform(source, result);
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally{
			DataBaseManager.close( rstSummary );
			rstSummary = null;
			DataBaseManager.close( pstmtSummary );
			pstmtSummary = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
	}
	
	public void ltReportDataTemp(Connection con, long lRunId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.ltReportDataTemp(con, lRunId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}

	public int getEnterpriseId(Connection con, long lUserId)throws Throwable {
		RunDBI runDBI = null;
		int enterpriseId = 0;
		try {
			runDBI = new RunDBI();
			enterpriseId = runDBI.getEnterpriseId(con, lUserId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return enterpriseId;
	}
	
	public void insertAgentMapping(Connection con, String guids, long runId) throws Throwable {
		
		RunDBI runDbi = new RunDBI();
		try	{
			runDbi.insertAgentMapping(con, guids, runId);
		}
		catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
	}
	
	public void deleteScript (Connection con, String strScriptId, LoginUserBean loginUserBean, String scriptName) throws Exception {
		 
		 RunDBI runDBI = null;
		 try{
			 runDBI = new RunDBI();
			 runDBI.deleteScript(con, strScriptId, loginUserBean, scriptName);
		 }catch(Exception e){
			 LogManager.errorLog(e);
		 }finally{
//			 runDBI = null;
		 }
	 }
	
	public void deleteJmeterScript (Connection con, String scenarioName, LoginUserBean loginUserBean, String[] scriptName, String strTestType, Long scenarioId) throws Exception {
		 
		 RunDBI runDBI = null;
		 try{
			 runDBI = new RunDBI();
			 runDBI.deleteJmeterScript(con, scenarioName, loginUserBean, scriptName, strTestType, scenarioId);
		 }catch(Exception e){
			 LogManager.errorLog(e);
		 }finally{
//			 runDBI = null;
		 }
	 }
	
	public JSONArray getScriptDetails(Connection con, long scenarioId) throws Throwable {
		RunDBI runDBI = null;
	
		JSONArray jaScript = null;
		try {
			runDBI = new RunDBI();
			jaScript = runDBI.getScriptDetails(con, scenarioId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return jaScript;
	}
	
	public void insertReportData(Connection con, JSONArray ltReportDatas, long lRunId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertReportData(con, ltReportDatas, lRunId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
//	public void updatePrivateCounters(Connection con, String columnName, long queueValue) throws Exception {
//		
//		RunDBI runDBI = null;
//	 
//		try {
//			runDBI = new RunDBI();
//			runDBI.updatePrivateCounters(con, columnName, queueValue);
//			runDBI = null;	
//		} catch (Exception e) {
//			LogManager.errorLog(e);
//		}
//	}
	
	public String getIPStatus(String strControllerIP , int nPort) throws Throwable {
		// Members 
		Socket socketConn = null;
		OutputStream oStream = null;
		InputStream inStream = null;
		
		byte[] bOperation = null;
		byte[] bytes = null;
		
		String strOperation = null , strReqHeader = null , strReceivedStremContent = null;		
		int nResponseStreamLength = 0 , nReadCount = 0;
		String strStatus = "inactive";
		try {
			
			socketConn = new Socket(strControllerIP, nPort);
			// connection success
			oStream = socketConn.getOutputStream();
			
			// Make operation format
			strOperation = "TEST: 0\r\n\r\n";
			
			bOperation = strOperation.getBytes();
			oStream.write(bOperation, 0, bOperation.length);
			
			// get response from controller
			inStream = socketConn.getInputStream();
			strReqHeader = readHeader(inStream);
			
			String[] reqtokens = strReqHeader.trim().split(": ");
			nResponseStreamLength = Integer.parseInt(reqtokens[1].toString().split("\r\n")[0]);
			
			bytes = new byte[nResponseStreamLength];
			
			if(nResponseStreamLength>0)	{		 
				 
				 while (nReadCount < nResponseStreamLength)	{
					 nReadCount+= inStream.read(bytes, nReadCount, nResponseStreamLength-nReadCount);
					 LogManager.infoLog("byte read"+nReadCount);
				}
			 }
			 // response content
			 strReceivedStremContent = new String(bytes);
			 if(strReqHeader.contains("OK")) {
				 // return sucess status
				 strStatus = "active";
			 }
			 
			 LogManager.infoLog("received"+strReceivedStremContent);
		}catch(Throwable t) {
			LogManager.errorLog(t);
			//throw t;
			strStatus = "inactive";
			
		}finally {
			
			if(inStream!= null) {
				inStream.close();				
			}
			inStream = null;
			
			if(oStream!= null) {
				oStream.close();
			}
			oStream = null;
			if(socketConn!= null) {
				socketConn.close();
			}
			socketConn = null;
			
			strOperation = null;
			strReqHeader = null;
			strReceivedStremContent = null;
			bOperation = null;
			bytes = null;
		}
		
		return strStatus;
	
	}
	
	public LTLicenseBean getLTLicenseDetails(Connection con, LoginUserBean loginBean) throws Exception {
		LTLicenseBean ltLicBean = null;
		LTDBI ltDBI = null;
		try {
			ltDBI = new LTDBI();
			
			// gets license details from month wise table
			ltLicBean = ltDBI.getLTUserWiseLicenseMonthWise(con, loginBean);
			
			if(ltLicBean != null) {
				// get license details from `apm config params`
				LTLicenseBean ltLicBean1 = ltDBI.getLTLicenseConfigParameters(con, loginBean);

				ltLicBean.setLicInternalName(ltLicBean1.getLicInternalName());
				ltLicBean.setLicExternalName(ltLicBean1.getLicExternalName());
				ltLicBean.setMaxIterations(ltLicBean1.getMaxIterations());
				ltLicBean.setMaxRuns(ltLicBean1.getMaxRuns());
				ltLicBean.setMaxDuration(ltLicBean1.getMaxDuration());
				ltLicBean.setReportRetentionPeriod(ltLicBean1.getReportRetentionPeriod());
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return ltLicBean;
	}

	public void insertLTJsonDetails(Connection con, String strQuery) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.insertLTJsonDetails(con, strQuery);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	public void updateProcessServiceFailed(Connection con, String status, long runId) throws Exception{
		
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateProcessServiceFailed(con, status, runId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public Long scenarioReferenceFromReportMaster(Connection con, long userId, long scenarioId) throws Throwable{
		RunDBI runDBI = null;
		Long completedRuns = 0L;
		try {
			runDBI = new RunDBI();
			completedRuns = runDBI.scenarioReferenceFromReportMaster(con, scenarioId, userId );
			runDBI = null;
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			runDBI = null;
		}
		return completedRuns;
	}
	
	public void registerMachineId(Connection con, long userId, String machineId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.registerMachineId(con, userId, machineId );
			runDBI = null;
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			runDBI = null;
		}
	}
	
	public void deRegisterMachineId(Connection con, long userId, String machineId) throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.deRegisterMachineId(con, userId, machineId );
			runDBI = null;
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			runDBI = null;
		}
	}
}
