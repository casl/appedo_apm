package com.appedo.lt.model;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class to keep the static Queue objects which is required for JMeter-Load-Test.
 * 
 * @author Ramkumar
 *
 */
public class JMeterExecutionManager {
	
	// Queue which keeps the SnapShot data, which has to be created in Grafana-Collector's SQLite-DB.
	public static LinkedBlockingQueue<String[]> lbqGrafanaSnapshotData = new LinkedBlockingQueue<String[]>();
	
	/**
	 * Queue the SnapShot Data.
	 * 
	 * @param strLoadTestSchedulerBean
	 * @param strGrafanaSnapshotData
	 */
	public static void addJMeterSnapshotData(String strLoadTestSchedulerBean, String strGrafanaSnapshotData) {
		
		lbqGrafanaSnapshotData.add( new String[]{strLoadTestSchedulerBean, strGrafanaSnapshotData} );
	}
	
	/**
	 * Poll the snapshot data, out from the Queue.
	 * 
	 * @return
	 */
	public static String[] pollJMeterSnapshotData() {

		return lbqGrafanaSnapshotData.poll();
	}
}
