package com.appedo.lt.model;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.commons.bean.LoginUserBean;
import com.appedo.lt.dbi.LTDBI;
import com.appedo.manager.LogManager;

public class LTManager {

	/**
	 * to get sum test details
	 * 
	 * @param con
	 * @param loginUserBean
	 * @param strTestType 
	 * @return
	 * @throws Exception
	 */
	public JSONArray getVUScripts(Connection con, LoginUserBean loginUserBean, String strTestType) throws Exception {
		LTDBI ltdbi = null;
		HashMap<Integer, String> hmScenarioNames = null;
		JSONArray jaScripts = null;

		try {
			ltdbi = new LTDBI();

			jaScripts = ltdbi.getVUScripts(con, loginUserBean, strTestType);
			for ( int i = 0; i < jaScripts.size(); i++ ){
				JSONObject joScript = jaScripts.getJSONObject(i);
				hmScenarioNames = ltdbi.getScenarioNames(con, joScript.getLong("script_id"), loginUserBean.getUserId());
				if( hmScenarioNames.size() > 0 ){
					for( Map.Entry<Integer, String> e:hmScenarioNames.entrySet() ){
						joScript.put("mapped_scenarios", e.getKey());
						joScript.put("scenarioName", e.getValue());
					}
				} else {
					joScript.put("mapped_scenarios", 0);
					joScript.put("scenarioName", "");
				}
//				hmScenarioNames = null;
			}
//			ltdbi = null;

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}

		return jaScripts;
	}
	
	public JSONArray getVUScenarios(Connection con, LoginUserBean loginUserBean, String strTestType) throws Exception {
		LTDBI ltdbi = null;
		HashMap<Long, Boolean> hmScenarioStatus = null;
		JSONArray jaScenarios = null;

		try {
			ltdbi = new LTDBI();

			jaScenarios = ltdbi.getVUScenarios(con, loginUserBean, strTestType);
			for (int i = 0; i<jaScenarios.size(); i++ ){
				JSONObject joScenario = jaScenarios.getJSONObject(i);
				joScenario.put("mappedo_scripts", ltdbi.getMappedScripts(con, joScenario.getLong("scenario_id"), loginUserBean.getUserId()));
				joScenario.put("completed_runs", ltdbi.getCompletedRuns(con, joScenario.getLong("scenario_id"), loginUserBean.getUserId()));
				hmScenarioStatus = ltdbi.getScenarioRunStatus(con, joScenario.getLong("scenario_id"), loginUserBean.getUserId());
				if( hmScenarioStatus.size() > 0 ){
					for( Map.Entry<Long, Boolean> e:hmScenarioStatus.entrySet() ){
						joScenario.put("run_status", e.getKey());
						joScenario.put("run_id", e.getValue());
					}
				} else {
					joScenario.put("run_status", false);
					joScenario.put("run_id", 0);
				}
//				hmScenarioStatus = null;
			}
//			ltdbi = null;

		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}

		return jaScenarios;
	}
	
	public JSONObject getRunningScriptData(Connection con, long runId, long userId) throws Exception{
		
		LTDBI ltDBI = null;
		JSONArray jaRunningScripts = null;
		JSONObject joRunDetails = null;
		try {
			ltDBI = new LTDBI();
			joRunDetails = ltDBI.getRunDetails(con, runId, userId);
			if ( joRunDetails != null ){
				joRunDetails = ltDBI.getRunStatus(con, runId, joRunDetails);
				jaRunningScripts = ltDBI.getRunningScriptData(con, runId);
				joRunDetails.put("scripts", jaRunningScripts);
//				jaRunningScripts = null;
			}
//			ltDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return joRunDetails;
	}
	
	public JSONArray readReports(Connection con, LoginUserBean loginUserBean) throws Exception {
		LTDBI ltDBI = null;
		JSONArray jaResults = null;
		
		try {
			jaResults = new JSONArray();
			ltDBI = new LTDBI();
			
			jaResults = ltDBI.getReports(con, loginUserBean);
//			ltDBI = null;
			
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return jaResults;
	}
	
	public JSONArray getLogReports(Connection con, long runId) throws Exception {
		LTDBI ltDBI = null;
		JSONArray jaResults = null;
		
		try {
			jaResults = new JSONArray();
			ltDBI = new LTDBI();
			
			jaResults = ltDBI.getLogReports(con, runId);
//			ltDBI = null;
			
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return jaResults;
	}
	
	public JSONArray getErrorReports(Connection con, long runId) throws Exception {
		LTDBI ltDBI = null;
		JSONArray jaResults = null;
		
		try {
			jaResults = new JSONArray();
			ltDBI = new LTDBI();
			
			jaResults = ltDBI.getErrorReports(con, runId);
//			ltDBI = null;
			
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return jaResults;
	}
	
	public JSONArray getRegions(Connection con, LoginUserBean loginUserBean, String osType) throws Exception {
		LTDBI ltDBI = null;
		JSONArray jaRtnRegionss = null;
		
		try {
			ltDBI = new LTDBI();
			jaRtnRegionss = ltDBI.getRegions(con, loginUserBean, osType);
//			ltDBI = null;
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		
		return jaRtnRegionss;
	}
	
	public JSONObject getSummaryReports(Connection con, long runId) throws Exception{
		
		LTDBI ltDBI = null;
		JSONObject joRunDetails = null;
		try {
			ltDBI = new LTDBI();
			joRunDetails = ltDBI.getSummaryReports(con, runId);
//			ltDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
		return joRunDetails;
	}
}
