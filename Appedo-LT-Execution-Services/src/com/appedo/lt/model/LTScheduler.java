package com.appedo.lt.model;

import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.manager.LogManager;

public class LTScheduler {
	
public static  PriorityBlockingQueue<LoadTestSchedulerBean> htFGTestQueues = new PriorityBlockingQueue<LoadTestSchedulerBean>();
public static  PriorityBlockingQueue<LoadTestSchedulerBean> htFGTestPaidQueues = new PriorityBlockingQueue<LoadTestSchedulerBean>();
public static  PriorityBlockingQueue<LoadTestSchedulerBean> htJmeterQueues = new PriorityBlockingQueue<LoadTestSchedulerBean>();
public static  PriorityBlockingQueue<LoadTestSchedulerBean> ltQueues = new PriorityBlockingQueue<LoadTestSchedulerBean>();
public static  PriorityBlockingQueue<LoadTestSchedulerBean> jmeterQueues = new PriorityBlockingQueue<LoadTestSchedulerBean>();
public static  PriorityBlockingQueue<String> reportDataQueues = new PriorityBlockingQueue<String>();
	
	public static void queueFGTest(LoadTestSchedulerBean testBean) throws Exception {
		
		try {
			
			synchronized ( testBean ) {
				// queue needs to compare this bean with others to find priority
				testBean.setQueuedOn(new Date());
				LogManager.infoLog("Scheduling FG TEST  <> "+testBean.toJSON());
				if ( testBean.getTestType().equalsIgnoreCase("JMETER") ){
					htJmeterQueues.put( testBean );
				}else{
					if( testBean.getLicense().equalsIgnoreCase("level0") ){
						htFGTestQueues.put(testBean);
					}else{
						htFGTestPaidQueues.put(testBean);
					}
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	
	public synchronized static LoadTestSchedulerBean pollFGTest() throws Exception {
		
		try{
			
			return htFGTestQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean pollFGPaidTest() throws Exception {
		
		try{
			
			return htFGTestPaidQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean peekFGTest() throws Exception {
		
		try{
			
			return htFGTestQueues.peek();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean peekFGPaidTest() throws Exception {
		
		try{
			
			return htFGTestPaidQueues.peek();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public static void ltProcessingQueue(LoadTestSchedulerBean testBean) throws Exception {
		
		try {
			
			synchronized ( testBean ) {
				// queue needs to compare this bean with others to find priority
				LogManager.infoLog("Scheduling Processing Queue "+testBean.toJSON());
				ltQueues.put(testBean);
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public static LoadTestSchedulerBean pollProcessingQueue() throws Exception {
		
		try{
			
			return ltQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}

	public synchronized static LoadTestSchedulerBean pollJmeterTest() throws Exception {
		
		try{
			
			return htJmeterQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean peekJmeterTest() throws Exception {
		
		try{
			
			return htJmeterQueues.peek();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public static void ltProcessingJmeterQueue(LoadTestSchedulerBean testBean) throws Exception {
		
		try {
			
			synchronized ( testBean ) {
				// queue needs to compare this bean with others to find priority
				LogManager.infoLog("Scheduling Processing Queue "+testBean.toJSON());
				jmeterQueues.put(testBean);
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean pollJmeterProcessingQueue() throws Exception {
		
		try{
			
			return jmeterQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static LoadTestSchedulerBean peekJmeterProcessingQueue() throws Exception {
		
		try{
			
			return jmeterQueues.peek();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static void queueReportData(String reportData) throws Exception {
		
		try {
			// queue needs to compare this bean with others to find priority
			//LogManager.infoLog("Scheduling queue ReportData "+reportData);
			reportDataQueues.put(reportData);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public synchronized static String pollReportData() throws Exception {
		
		try{
			
			return reportDataQueues.poll();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
}
