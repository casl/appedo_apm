package com.appedo.rumcollector.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * This class will handle the operations required to be done when the application is started or stopped in Tomcat.
 * 
 * @author veeru 
 *
 */
public class AppedoRUMCollectorServletContext implements ServletContextListener	{

	private ServletContext context = null;
	
	/**
	 * This method is invoked when the Web Application
	 * is ready to service requests
	 */
	public void contextInitialized(ServletContextEvent event) {
		this.context = event.getServletContext();
		
		//Output a simple message to the server's console
		System.out.println("The Appedo-RUM-Collector Web App is Ready in "+context.getServerInfo());
	}
	
	/**
	 * Stop all the timers when this project is stopped or tomcat is stopped
	 */
	public void contextDestroyed(ServletContextEvent event) {	

		System.out.println("The Appedo-RUM-Collector Web App has Been Removed.");
		this.context = null;
	}
}
