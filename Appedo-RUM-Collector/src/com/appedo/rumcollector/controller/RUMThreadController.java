package com.appedo.rumcollector.controller;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.manager.RUMCollectorManager;
import com.appedo.rumcollector.timer.RUMWriterTimer;
import com.appedo.rumcollector.utils.Constants;

public class RUMThreadController extends Thread {
	
	private RUMCollectorManager collectorManager = null;
	
	public RUMThreadController() {
		collectorManager = RUMCollectorManager.getRUMCollectorManager();
	}
	
	@Override
	public void run() {
		RUMWriterTimer rumWriterTimer = null;
		RUMCollectorBean cbRumEntry = null;		
		while(true) {
			try{
				cbRumEntry = collectorManager.pollRumBean();
				
				// take one-by-one bean from the queue
				if( cbRumEntry != null ) {
					rumWriterTimer = new RUMWriterTimer(cbRumEntry);
					rumWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = collectorManager.getRumQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				rumWriterTimer = null;
				cbRumEntry = null;
			}
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("RUM CollectorThreadController got stopped");
		LogManager.errorLog( new Exception("CollectorThreadController got stopped") );
		
		super.finalize();
	}
}
