package com.appedo.rumcollector.controller;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.CICollectorBean;
import com.appedo.rumcollector.manager.CICollectorManager;
import com.appedo.rumcollector.timer.CIWriterTimer;
import com.appedo.rumcollector.utils.Constants;

public class CIThreadController extends Thread {
	
	private CICollectorManager ciCollectorManager = null;
	
	public CIThreadController() {
		ciCollectorManager = CICollectorManager.getCICollectorManager();
	}
	
	@Override
	public void run() {
		CIWriterTimer ciWriterTimer = null;
		CICollectorBean cbCIEntry = null;
		
		while(true) {
			try{
				cbCIEntry = ciCollectorManager.pollCIBean();
				
				// take one-by-one bean from the queue
				if( cbCIEntry != null ) {
					ciWriterTimer = new CIWriterTimer(cbCIEntry);
					ciWriterTimer.start();
				}
				
				// if the above loop was stopped as queue was empty then wait for 30 seconds.
				int nQueueSize = ciCollectorManager.getCIQueueLength();
				
				if( nQueueSize == 0 ) {
					//System.out.println(this_agent+"PerfCounter sleeping...");
					Thread.sleep( Constants.COUNTER_CONTROLLER_REST_MILLESECONDS );
					//System.out.println(this_agent+"PerfCounter wokeup...");
				}
				
				nQueueSize = 0;
			} catch(Throwable th) {
				LogManager.errorLog(th);
			} finally {
				ciWriterTimer = null;
				cbCIEntry = null;
			}
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("CI CollectorThreadController got stopped");
		LogManager.errorLog( new Exception("CollectorThreadController got stopped") );
		
		super.finalize();
	}
}
