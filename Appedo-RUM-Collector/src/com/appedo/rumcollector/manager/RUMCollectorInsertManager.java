package com.appedo.rumcollector.manager;

import java.sql.Connection;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.bean.RUMSLABean;
import com.appedo.rumcollector.common.Constants;
import com.appedo.rumcollector.common.Constants.SLA_BREACH_SEVERITY;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.dbi.RUMDBI;
import com.appedo.rumcollector.utils.UtilsFactory;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author Navin
 *
 */
public class RUMCollectorInsertManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected RUMDBI collectorDBI = null;
	
	protected RUMCollectorManager collectorManager = null;
	
	
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public RUMCollectorInsertManager() throws Exception {
		collectorDBI = new RUMDBI();
		
		establishDBonnection();
		
		collectorManager = RUMCollectorManager.getRUMCollectorManager();
	}
	
	
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}

	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @throws Exception
	 */
	public void fetchBean(RUMCollectorBean cbRumEntry) throws Exception {
		long lRUMId = -1L;
		
		try {
			// if connection is not established to db server then wait for 10 seconds
			if( conPC == null || ! DataBaseManager.isConnectionExists(conPC) ){
				conPC = DataBaseManager.reEstablishConnection(conPC);
			}
			
			JSONObject joLoc = null;
			boolean isIpExist = collectorDBI.checkIpExist(conPC, cbRumEntry.getIpAddress());
			if ( !isIpExist ){
				joLoc = getLocationDetails(cbRumEntry.getIpAddress());
			}
			lRUMId = collectorDBI.insertRumTable(conPC, cbRumEntry);
			cbRumEntry.setRUMId(lRUMId);
			
			if( joLoc != null ){
				collectorDBI.insertIPLocationMaster(conPC, joLoc, cbRumEntry.getIpAddress());
			}
			
			// if SLA has configured, checks breach limits, if breaches send to `Appedo-SLA-Collector`
			verifySLABreach(cbRumEntry);
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(conPC);
			conPC = null;
		}
	}
	
	public static JSONObject getLocationDetails(String ipAddress) throws Throwable {
		HttpClient client = null;
		PostMethod method = null;
		
		String responseStream = null;
		JSONObject joResponse = null;
		
		try{
			client = new HttpClient();
			// URLEncoder.encode(requestUrl,"UTF-8");
			method = new PostMethod(Constants.RUM_LOCATION.replace("<IPADDRESS>", ipAddress));
			method.setRequestHeader("Connection", "close");
			int statusCode = client.executeMethod(method);
			// System.err.println("statusCode: "+statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				System.out.println("Method failed: " + method.getStatusLine());
			}
			
			responseStream = method.getResponseBodyAsString();
			if( responseStream.trim().startsWith("{") && responseStream.trim().endsWith("}")) {
				joResponse = JSONObject.fromObject(responseStream);
				System.out.println(joResponse.toString());
			}
			// System.out.println("responseJSONStream: "+responseStream);
		} catch(Throwable e) {
			e.printStackTrace();
			LogManager.errorLog(e);
			throw e;
		} finally {
			if ( method != null ) {
				method.releaseConnection();	
			}
			method = null;
		}
		
		return joResponse;
	}
	
	/**
	 * checks RUM breach for the uid and send to `Appedo-SLA-Collector`,
	 *   if SLA has configured for the guid, if response pageloadtime breached, send breached details to `Appedo-SLA-Collector`
	 * 
	 * @param rumCollectorBean
	 * @return
	 * @throws Exception
	 */
	public boolean verifySLABreach(RUMCollectorBean rumCollectorBean) throws Exception {
		RUMSLABean rumslaBean = null;
		
		JSONObject joRtnRUMSLABreach = null;
		
		boolean bBreached = false;
		
		long lUID = -1L;
		
		try {/*
			// if connection is not established to db server then wait for 10 seconds
			if( conPC == null || ! DataBaseManager.isConnectionExists(conPC) ){
				conPC = DataBaseManager.reEstablishConnection(conPC);
			}*/
			
			// get uid for given guid 
			lUID = collectorDBI.getModuleUID(conPC, rumCollectorBean.getGuid());
			if( lUID == -1l ) {				
				throw new Exception("Given GUID is not matching: "+rumCollectorBean.getGuid());				
			}
			
			// gets RUM uid's configured SLA details, `null` if no SLA has configured
			rumslaBean = collectorDBI.getRUMSLADetails(conPC, lUID);
			if ( rumslaBean != null ) {	// has SLA configured
				// SLA configured 
				
				// checks with threshold limits & gets breached data, `null` if not breached
				joRtnRUMSLABreach = getBreachedData(rumCollectorBean, rumslaBean);
				if ( joRtnRUMSLABreach != null ) {	// has SLA breached
					bBreached = true;
					
					// send breached details to `Appedo-SLA-Collector`, 
					sendBreachedDataToSLACollector(joRtnRUMSLABreach, rumslaBean.getGUID());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			rumslaBean = null;
			
			UtilsFactory.clearCollectionHieracy(joRtnRUMSLABreach);
			joRtnRUMSLABreach = null;
		}
		
		return bBreached;
	}
	
	/**
	 * checks RUM collected data with configured threshold limits, 
	 *   if breached gets breached details, `null` if not breached
	 * 
	 * @param rumCollectorBean
	 * @param rumslaBean
	 * @return
	 * @throws Exception
	 */
	public JSONObject getBreachedData(RUMCollectorBean rumCollectorBean, RUMSLABean rumslaBean) throws Exception {
		JSONObject joRUMSLABreach = null;
		
		SLA_BREACH_SEVERITY sla_breach_severity = null;
		
		long lPageLoadTime = -1L;
		
		boolean bHasBreached = false;
		
		try {
			// TODO: RUM pageLoadTime formula, should be in single place
			lPageLoadTime = rumCollectorBean.getNt_domcomp() - rumCollectorBean.getNt_nav_st();
			
			// check with threshold limit
			if( rumslaBean.isAboveThreshold() ) {
				if ( lPageLoadTime > rumslaBean.getCriticalThresholdValue() ) {	// Critical
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.CRITICAL;
				} else if(lPageLoadTime > rumslaBean.getWarningThresholdValue() ) {	// Warning
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.WARNING; 
				}
			} else if( ! rumslaBean.isAboveThreshold() ) {
				if( lPageLoadTime < rumslaBean.getCriticalThresholdValue() ) {	// Critical
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.CRITICAL;
				} else if( lPageLoadTime < rumslaBean.getWarningThresholdValue() ) {	// Warning
					bHasBreached = true;
					sla_breach_severity = SLA_BREACH_SEVERITY.WARNING;
				}
			}
			
			if ( bHasBreached ) {
				joRUMSLABreach = new JSONObject();
				joRUMSLABreach.put("slaId", rumslaBean.getSlaId());
				joRUMSLABreach.put("userId", rumslaBean.getUserId());
				
				joRUMSLABreach.put("uid", rumslaBean.getUId());
				joRUMSLABreach.put("guid", rumslaBean.getGUID());
				joRUMSLABreach.put("rum_id", rumCollectorBean.getRUMId());
				
				joRUMSLABreach.put("is_above_threshold", rumslaBean.isAboveThreshold());
				joRUMSLABreach.put("warning_threshold_value", rumslaBean.getWarningThresholdValue());
				joRUMSLABreach.put("critical_threshold_value", rumslaBean.getCriticalThresholdValue());
				joRUMSLABreach.put("received_value", lPageLoadTime);
				joRUMSLABreach.put("appedo_received_on", rumCollectorBean.getDateQueuedOn().getTime());
				joRUMSLABreach.put("breached_severity", sla_breach_severity.toString());
				joRUMSLABreach.put("min_breach_count", rumslaBean.getMinBreachCount());
				
				// RUM collector
				joRUMSLABreach.put("url", rumCollectorBean.getUrl());
			}
		} catch (Exception e) {
			throw e;
		}
		
		return joRUMSLABreach;
	}
	
	/**
	 * send the breached details to SLA collector
	 * 
	 * @param joRUMSLABreach
	 * @param strGUID
	 * @throws Exception
	 */
	public void sendBreachedDataToSLACollector(JSONObject joRUMSLABreach, String strGUID) throws Exception {
		WebServiceManager wsm = null;
		
		JSONObject joResp = null;
		
		try {
			wsm = new WebServiceManager();
			wsm.addParameter("command", "collectRUMUIDBreach");
			wsm.addParameter("breach_data", joRUMSLABreach.toString());
			wsm.addParameter("guid", strGUID);

			// web service request
			wsm.sendRequest(Constants.APPEDO_SLA_COLLECTOR_URL);
			if( wsm.getStatusCode() != null && wsm.getStatusCode() == HttpStatus.SC_OK ) {
				// success
				joResp = JSONObject.fromObject(wsm.getResponse());
			} else {
				throw new Exception("Problem with SLA Collector");
			}
			
			// web service response
			if( joResp.getBoolean("success") ) {
				// success
				LogManager.infoLog("SLA policy added and mapped to RUM.");
			} else {
				// exception in SLA
				throw new Exception("Unable to queue RUM SLA breached details.");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if ( wsm != null ) {
				wsm.destory();
			}
			wsm = null;
		}
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}
