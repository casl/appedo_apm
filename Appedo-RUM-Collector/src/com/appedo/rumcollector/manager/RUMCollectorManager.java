package com.appedo.rumcollector.manager;

import java.sql.Connection;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.utils.UtilsFactory;

/**
 * Manager which holds the queues, of all the performance counters
 * Singleton class. So no objects can be created.
 * 
 * @author veeeru
 *
 */
public class RUMCollectorManager{
	
	// Singleton object, used globally with static getCollectorManager().
	private static RUMCollectorManager rumCollectorManager = new RUMCollectorManager();
	
	// Queue to store performance counter data of agents
	private PriorityBlockingQueue<RUMCollectorBean> pqModulePerformanceRum = new PriorityBlockingQueue<RUMCollectorBean>();
	
	private Connection conBackup = null;
	/**
	 * Avoid multiple object creation, by Singleton
	 */
	private RUMCollectorManager() {
		
		try{
			// initialize all required queue in this private constructor.
			pqModulePerformanceRum = new PriorityBlockingQueue<RUMCollectorBean>();			
			
		} catch(Exception ex) {
			LogManager.errorLog(ex);
		}
	}
	
	
	/**
	 * Access the only[singleton] CollectorManager object.
	 * 
	 * @return CollectorManager
	 */
	public static RUMCollectorManager getRUMCollectorManager(){
		return rumCollectorManager;
	}

	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectRumBean(RUMCollectorBean collBean) throws Exception {
		
		try {
					
			return pqModulePerformanceRum.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public RUMCollectorBean pollRumBean(){
		RUMCollectorBean cbRumEntry = null;
		
		try {
			cbRumEntry = pqModulePerformanceRum.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
		
		return cbRumEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getRumQueueLength(){
		return pqModulePerformanceRum.size();
	}
	
	/*
	public long getUTCTime(long lTime) {
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date= new Date(lTime);			
			String dateString = formatter.format(date);			
			//formatter.format(date).;
		}catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally {
			
		}
		return 0l;
	}
	*/
	
	@Override
	/**
	 * Before destroying clear the objects. To prevent from MemoryLeak.
	 */
	protected void finalize() throws Throwable {
		UtilsFactory.clearCollectionHieracy(pqModulePerformanceRum);
		
		DataBaseManager.close(conBackup);
		conBackup = null;
		
		super.finalize();
	}

}