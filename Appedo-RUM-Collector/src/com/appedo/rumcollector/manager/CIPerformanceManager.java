package com.appedo.rumcollector.manager;

import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.CICollectorBean;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.dbi.CIDBI;

/**
 * This will fetch the queue then insert them into database.
 * 
 * @author veeru
 *
 */
public class CIPerformanceManager {
	
	// Database Connection object. Single connection will be maintained for entire operations.
	protected Connection conPC = null;
	
	protected CIDBI ciDBI = null;
	
	protected CICollectorManager collectorManager = null;
	
	// TODO enable_1min 
	// private HashMap<Integer, Double> hmCounterForMin = new HashMap<Integer, Double>();
	
	/**
	 * Avoid multiple object creation, by Singleton
	 * Do the initialization operations like
	 * Connection object creation and CollectorManager's object creation.
	 * 
	 * @throws Exception
	 */
	public CIPerformanceManager() throws Exception {
		ciDBI = new CIDBI();
		
		establishDBonnection();
		
		collectorManager = CICollectorManager.getCICollectorManager();
	}
	
	/**
	 * Create a db connection object for all the operations related to this Class.
	 */
	protected void establishDBonnection(){
		try{
			conPC = DataBaseManager.giveConnection();
			
		} catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	/**
	 * Retrieves the counter-sets from the respective queue.
	 * And inserts them in the database.
	 * 
	 * @param cbCIEntry
	 * @throws Exception
	 */
	public void fetchBean(CICollectorBean cbCIEntry) throws Exception {
		
		try{
			// if connection is not established to db server then wait for 10 seconds
			if( conPC == null || ! DataBaseManager.isConnectionExists(conPC) ){
				conPC = DataBaseManager.reEstablishConnection(conPC);
			} else {
				// get uid for given guid 
				long lUID = ciDBI.getModuleUID(conPC, cbCIEntry.getGuid());
				
				if( lUID == -1l ) {				
					throw new Exception("Given GUID is not matching: "+cbCIEntry.getGuid());				
				} else {
					// Check ci_events_.. for entry
					long eventId = ciDBI.isExistEvents(conPC, lUID, cbCIEntry.getEventMethod(), cbCIEntry.getEventName(), cbCIEntry.getAgentType(), cbCIEntry.getEnv());
					
					// Insert into CIEvents
					if( eventId == 0 ){
						eventId = ciDBI.insertCIEvents(conPC, lUID, cbCIEntry.getEventName(), cbCIEntry.getEventMethod(), cbCIEntry.getAgentType(), cbCIEntry.getEnv());
						
						// Create partition table for Trans
						ciDBI.createCITransTablePartitions(conPC, lUID, eventId);
					}
					
					// Insert ci_evt_prop_template
					ciDBI.insertEventProperty(conPC, lUID, eventId, cbCIEntry);
					
					// Get inserted Values in Hashmap
					String strYear = Integer.toString( Calendar.getInstance().get(Calendar.YEAR) );
					HashMap<String, String> hmap = ciDBI.getEventProperty(conPC, lUID, eventId);
					
					// Insert for Trans table
					ciDBI.insertCITable(conPC, lUID, eventId, hmap, strYear, cbCIEntry);		
				}
			}
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(conPC);
			conPC = null;
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		// close the connection before this object is destroyed from JVM
		DataBaseManager.close(conPC);
		conPC = null;
		
		super.finalize();
	}
}