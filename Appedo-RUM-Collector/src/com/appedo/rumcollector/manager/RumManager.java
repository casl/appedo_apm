package com.appedo.rumcollector.manager;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.rumcollector.bean.RUMCollectorBean;
public class RumManager {
	
	public static ArrayList< PriorityBlockingQueue<RUMCollectorBean> > alRumLogQueues = null;
	
	public static void createRumLogQueues() {
		PriorityBlockingQueue<RUMCollectorBean> pqRumLogBeans = null;
		alRumLogQueues = new ArrayList< PriorityBlockingQueue<RUMCollectorBean> >();
		
		for(int nLogTimerIndex=0; nLogTimerIndex<1; nLogTimerIndex++){
			pqRumLogBeans = new PriorityBlockingQueue<RUMCollectorBean>();
			alRumLogQueues.add(pqRumLogBeans);
		}
	}
	
	public static void rumLogQueue(RUMCollectorBean rumLogBean){
		
		PriorityBlockingQueue<RUMCollectorBean> pqRumLogBeans = alRumLogQueues.get( (int)Thread.currentThread().getId()%1 );
		synchronized ( pqRumLogBeans ) {
			pqRumLogBeans.add(rumLogBean);	
		}
	}

}
