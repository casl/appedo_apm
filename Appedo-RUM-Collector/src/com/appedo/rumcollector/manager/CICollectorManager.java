package com.appedo.rumcollector.manager;

import java.sql.Connection;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.CICollectorBean;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.utils.UtilsFactory;
//import com.appedo.rumcollector.DBI.CollectorDBI;

/**
 * Manager which holds the queues, of all the performance counters
 * Singleton class. So no objects can be created.
 * 
 * @author veeeru
 *
 */
public class CICollectorManager{
	
	// Singleton object, used globally with static getCollectorManager().
	private static CICollectorManager ciCollectorManager = new CICollectorManager();
	
	// Queue to store performance counter data of agents
	private PriorityBlockingQueue<CICollectorBean> pqModulePerformanceCI = new PriorityBlockingQueue<CICollectorBean>();
	
	private Connection conBackup = null;
	/**
	 * Avoid multiple object creation, by Singleton
	 */
	private CICollectorManager() {
		
		try{
			// initialize all required queue in this private constructor.
			pqModulePerformanceCI = new PriorityBlockingQueue<CICollectorBean>();			
			
		} catch(Exception ex) {
			LogManager.errorLog(ex);
		}
	}
	
	
	/**
	 * Access the only[singleton] CollectorManager object.
	 * 
	 * @return CollectorManager
	 */
	public static CICollectorManager getCICollectorManager(){
		return ciCollectorManager;
	}

	
	/**
	 * Queue the given bean CollectorBean into the asked Family queue.
	 * 
	 * @param strCounterParams
	 * @return
	 * @throws Exception
	 */
	public boolean collectCIBean(CICollectorBean collBean) throws Exception {
		
		try {
					
			return pqModulePerformanceCI.add(collBean);
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	/**
	 * Poll the top Counter CollectorBean from the asked Family queue.
	 * 
	 * @param agent_family
	 * @param nIndex
	 * @return
	 */
	public CICollectorBean pollCIBean(){
		CICollectorBean cbCIEntry = null;
		
		try {
			cbCIEntry = pqModulePerformanceCI.poll();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
		
		return cbCIEntry;
	}
	/**
	 * Get the size of the asked Family queue.
	 * 
	 * @return int
	 */
	public int getCIQueueLength(){
		return pqModulePerformanceCI.size();
	}
	
	/*
	public long getUTCTime(long lTime) {
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date date= new Date(lTime);			
			String dateString = formatter.format(date);			
			//formatter.format(date).;
		}catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally {
			
		}
		return 0l;
	}
	*/
	
	@Override
	/**
	 * Before destroying clear the objects. To prevent from MemoryLeak.
	 */
	protected void finalize() throws Throwable {
		UtilsFactory.clearCollectionHieracy(pqModulePerformanceCI);
		
		DataBaseManager.close(conBackup);
		conBackup = null;
		
		super.finalize();
	}

}