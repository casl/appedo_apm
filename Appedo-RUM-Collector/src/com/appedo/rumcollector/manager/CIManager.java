package com.appedo.rumcollector.manager;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.rumcollector.bean.CICollectorBean;
public class CIManager {
	
	public static ArrayList< PriorityBlockingQueue<CICollectorBean> > alCILogQueues = null;
	
	public static void createCILogQueues() {
		PriorityBlockingQueue<CICollectorBean> pqCILogBeans = null;
		alCILogQueues = new ArrayList< PriorityBlockingQueue<CICollectorBean> >();
		
		for(int nLogTimerIndex=0; nLogTimerIndex<1; nLogTimerIndex++){
			pqCILogBeans = new PriorityBlockingQueue<CICollectorBean>();
			alCILogQueues.add(pqCILogBeans);
		}
	}
	
	public static void ciLogQueue(CICollectorBean ciLogBean){
		
		PriorityBlockingQueue<CICollectorBean> pqCILogBeans = alCILogQueues.get( (int)Thread.currentThread().getId()%1 );
		synchronized ( pqCILogBeans ) {
			pqCILogBeans.add(ciLogBean);	
		}
	}

}
