package com.appedo.rumcollector.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.appedo.manager.LogManager;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author Ramkumar
 *
 */
public class Constants {

	public static String RESOURCE_PATH = null;
	public static String CONSTANTS_FILE_PATH = null;
	public static String APPEDO_CONFIG_FILE_PATH = null;
	public static String SMTP_MAIL_CONFIG_FILE_PATH = null;
	
	// milliseconds unit
	public final static long FREQUENCY_ONE_DAY = 1000 * 60 * 60 * 24;
	public final static long FREQUENCY_ONE_HOUR = 1000 * 60 * 60;
	public final static long FREQUENCY_FOUR_HOUR = 1000 * 60 * 60 * 4;
	public final static long FREQUENCY_ONE_MINUTE = 1000 * 60 * 1;
	
	public static long COUNTER_CONTROLLER_REST_MILLESECONDS = 1000;
	
	public static int MODULE_CONTROLLER_THREADS = 10;
	public static int JAVA_PROFILER_CONTROLLER_THREADS = 10;
	public static int DOTNET_PROFILER_CONTROLLER_THREADS = 10;
	
	public static String LOG4J_PROPERTIES_FILE;
	
	public enum AGENT_FAMILY {
		APPLICATION("APPLICATION"), 
		SERVER("SERVER"),  
		DATABASE("DATABASE"), 
		NETWORK("NETWORK");
		
		private String strAgentFamily;
		
		private AGENT_FAMILY(String agentFamily) {
			strAgentFamily = agentFamily;
		}
		
		public String getAgentFamily() {
			return this.strAgentFamily;
		}
		
		public String toString() {
			return strAgentFamily;
		}
	}
	
	public enum AGENT_TYPE {
		TOMCAT("TOMCAT", "APPLICATION"), JBOSS("JBOSS", "APPLICATION"), MSIIS("MSIIS", "APPLICATION"), 
		LINUX("LINUX", "SERVER"), WINDOWS("WINDOWS", "SERVER"), 
		MYSQL("MYSQL", "DATABASE"), MSSQL("MSSQL", "DATABASE"),
		NETWORK("NETWORK", "NETWORK"),
		JAVA_PROFILER("JAVA_PROFILER", "APPLICATION"), DOTNET_PROFILER("DOTNET_PROFILER", "APPLICATION");
		
		private String strAgentType;
		private String strAgentCategory;
		
		private AGENT_TYPE(String agentType, String agentCategory) {
			strAgentType = agentType;
			strAgentCategory = agentCategory;
		}

		public void setMySQLVersion(String strVersionNo) {
			strAgentType = "MYSQL "+strVersionNo;
		}
		
		public String getAgentCategory() {
			return this.strAgentCategory;
		}
		
		public String toString() {
			return strAgentType;
		}
	}
	
	public enum PROFILER_KEY {
		THREAD_ID(1), TYPE(2), START_TIME(3), DURATION_MS(4), APPROX_NANO_SEC_START(5), DURATION_NS(6), REQUEST_URI(7), LOCALHOST_NAME_IP(8), REFERER_URI(9), 
		CLASS_NAME(51), METHOD_NAME(52), METHOD_SIGNATURE(53), CALLER_METHOD_ID(54), CALLEE_METHOD_ID(55), 
		QUERY(101), 
		EXCEPTION_TYPE(151), EXCEPTION_MESSAGE(152), EXCEPTION_STACKTRACE(153);
		
		private Integer key;
		
		private PROFILER_KEY(Integer nKey) {
			key = nKey;
		}
		
		public String toString() {
			return key+"";
		}
	}
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) {
    	Properties prop = new Properties();
    	InputStream is = null;
    	
        try {
    		is = new FileInputStream(srtConstantsPath);
    		prop.load(is);
    		
     		// Appedo application's resource directory path
     		RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
     		
     		APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
     		SMTP_MAIL_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("SMTP_MAIL_CONFIG_FILE_PATH");
     		
     		COUNTER_CONTROLLER_REST_MILLESECONDS = Integer.parseInt(prop.getProperty("COUNTER_CONTROLLER_REST_MILLESECONDS"));
     		
        } catch(Exception e) {
        	LogManager.errorLog(e);
        }
	}
}
