package com.appedo.rumcollector.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.common.Constants;


public class Controller extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Handles POST request
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * Handles GET request comes
	 */	
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * Accessed in both GET and POSTrequests for the operations below, 
	 * 1. Loads agents latest build version
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String action = request.getServletPath();
		
		if( action.equals("/common/reloadConfigProperties")) {
			// to reload config, appedo_config properties & appedo whitelabels  
			
			try {
				// Loads Constant properties 
				Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
				
				// Loads Appedo config properties from the system path
				Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
				
				// load appedo constants; say loads appedoWhiteLabels, 
				Constants.loadAppedoWhiteLabels(Constants.APPEDO_CONFIG_FILE_PATH);
				
				response.getWriter().write("Loaded <B>Appedo-RUM-Collector</B>, config, appedo_config & appedo whitelabels.");
			} catch (Exception e) {
				LogManager.errorLog(e);
				response.getWriter().write("<B style=\"color: red; \">Exception occurred Appedo-RUM-Collector: "+e.getMessage()+"</B>");
			}
		}
	}
}
