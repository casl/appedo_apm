package com.appedo.rumcollector.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.appedo.rumcollector.bean.CICollectorBean;
import com.appedo.rumcollector.manager.CICollectorManager;
import com.appedo.rumcollector.utils.UtilsFactory;

/**
 * Servlet implementation class RumCollector
 */
//@WebServlet("/RumCollector")
public class CICollector extends HttpServlet {
	private static final long serialVersionUID = 1L;
		
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CICollector() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) {
		
		// Members
		CICollectorBean ciBean = new CICollectorBean();
		CICollectorManager ciCollectorManager = CICollectorManager.getCICollectorManager();
		boolean bQueued = false;
		
		try {
			//Get client ip address
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}		
			
			ciBean.setEventStartTime(new Timestamp((new Date()).getTime()));
			
			// capture all input parameters & set parameters to bean
			ciBean.setIpAdress(ipAddress);
			
//			ciBean.setBrowserName(request.getParameter("browserName"));
//			ciBean.setDeviceType(request.getParameter("deviceType"));
//			ciBean.setOs(request.getParameter("OS"));
//			ciBean.setDevicename(request.getParameter("devicename"));
//			ciBean.setGuid(request.getParameter("guid"));
//			
//			// Merchant Name
//			if(request.getParameter("merchantname")!=null)
//				ciBean.setMerchantName(request.getParameter("merchantname"));
//			else 
//				ciBean.setMerchantName("Other");
//			
//			// OS Version
//			if( request.getParameter("deviceType").trim().toUpperCase().equals("MOBILE")) {
//				if(request.getParameter("OS").trim().contains("+")) {
//					ciBean.setOsVersion(request.getParameter("OS").trim().split("+")[1]);
//					
//				}
//				else {
//					ciBean.setOsVersion("-1");
//				}
//			}else {
//				ciBean.setOsVersion("-1");
//			}
			
			ciBean.setEnv(request.getParameter("env"));
			ciBean.setAgentType(request.getParameter("agent_type"));
			ciBean.setGuid(request.getParameter("guid"));
			ciBean.setEventMethod(UtilsFactory.replaceNull(request.getParameter("eventMethod"),""));
			ciBean.setEventName(UtilsFactory.replaceNull(request.getParameter("eventName"),""));
			ciBean.setBrowserName(UtilsFactory.replaceNull(request.getParameter("browserName"),""));
			ciBean.setDeviceType(UtilsFactory.replaceNull(request.getParameter("deviceType"),""));
			ciBean.setOs(UtilsFactory.replaceNull(request.getParameter("OS"),""));
			if( !request.getParameter("devicename").equalsIgnoreCase("undefined") ){
				if(!request.getParameter("devicename").equalsIgnoreCase("")){
					ciBean.setDevicename(request.getParameter("devicename"));
				}  else{
					ciBean.setDevicename(request.getParameter("Not Available"));
				}
			} else{
				ciBean.setDevicename(request.getParameter("Not Available"));
			}
		
			ciBean.setMerchantName(UtilsFactory.replaceNull(request.getParameter("merchantname"), "Other"));
//			if(request.getParameter("merchantname")!= null)
//				ciBean.setMerchantName(request.getParameter("merchantname"));
//			else 
//				ciBean.setMerchantName("Other");
			
//			ciBean.setEventDuration(ciBean.getEventEndTime().getTime() - ciBean.getEventStartTime().getTime());
			
//			user_params
			JSONObject json = JSONObject.fromObject(request.getParameter("ci_params"));
			
			if( json.containsKey("age") ){
				ciBean.setAge(json.getString("age"));
				json.remove("age");
			}
			
			if( json.containsKey("eventStartTime") ){
				ciBean.setEventStartTime(new Timestamp(Long.parseLong(json.getString("eventStartTime"))));
				json.remove("eventStartTime");
			}
			
			if( json.containsKey("eventEndTime") ){
				ciBean.setEventEndTime(new Timestamp(Long.parseLong(json.getString("eventEndTime"))));
				json.remove("eventEndTime");
			} else{
				ciBean.setEventEndTime(new Timestamp((new Date()).getTime()));
			}
			
			Iterator keys = json.keys();
			HashMap<String, String> ciColumns = new HashMap<String, String>();
			while(keys.hasNext()) {
				// loop to get the dynamic key
				String currentDynamicKey = (String)keys.next();

				// get the value of the dynamic key
				String currentDynamicValue = json.getString(currentDynamicKey);
				//System.out.println(currentDynamicKey+" = "+currentDynamicValue);
				
				ciColumns.put(currentDynamicKey,currentDynamicValue);
				ciBean.setCiColumns(ciColumns);
				// do something here with the value...
			}
			
			JSONObject user_json = JSONObject.fromObject(request.getParameter("user_params"));
			if( user_json.containsKey("email_id") ){
				ciBean.setEmailId(user_json.getString("email_id"));
				user_json.remove("email_id");
			}
			
			if( user_json.containsKey("user_first_name") ){
				ciBean.setFirstName(user_json.getString("user_first_name"));
				user_json.remove("user_first_name");
			}
			
			if( user_json.containsKey("user_last_name") ){
				ciBean.setLastName(user_json.getString("user_last_name"));
				user_json.remove("user_last_name");
			}
			
			if( user_json.containsKey("mobile_no") ){
				ciBean.setMobileNo(user_json.getString("mobile_no"));
				user_json.remove("mobile_no");
			}
			
			if( ciBean.getEventEndTime() != null && ciBean.getEventStartTime() != null ){
				ciBean.setEventDuration(ciBean.getEventEndTime().getTime() - ciBean.getEventStartTime().getTime());
			}
			ciBean.setDateQueuedOn(new Date());
			
			//add to Queue
			bQueued = ciCollectorManager.collectCIBean(ciBean);
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			ciBean = null;
			ciCollectorManager = null;
		}
		
	}

}
