package com.appedo.rumcollector.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.manager.RUMCollectorManager;

/**
 * Servlet implementation class RumCollector
 */
//@WebServlet("/RumCollector")
public class RumCollector extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RumCollector() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) {
		
		// Members
		RUMCollectorBean rumBean = new RUMCollectorBean();
		RUMCollectorManager collectorManager = RUMCollectorManager.getRUMCollectorManager();
		boolean bQueued = false;

		String strURL = "", strURLWithoutQueryString = "", strQueryString = "";
		
		int nIdxStartQueryString = -1;
		
		try {
			//Get client ip address
			String ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			
			strURL = request.getParameter("u");
			nIdxStartQueryString = strURL.indexOf("?");
			// url without query string
			strURLWithoutQueryString = nIdxStartQueryString != -1 ? strURL.substring(0, nIdxStartQueryString) : strURL;
			// query string
			strQueryString = nIdxStartQueryString != -1 ? strURL.substring(nIdxStartQueryString, strURL.length()) : "";
			
			// capture all input parameters & set parameters to bean
			rumBean.setIpAddress(ipAddress);
			
			rumBean.setBrowserName(request.getParameter("browserName"));
			rumBean.setDeviceType(request.getParameter("deviceType"));
			rumBean.setOs(request.getParameter("OS"));
			rumBean.setDevicename(request.getParameter("devicename"));
			rumBean.setGuid(request.getParameter("guid"));
			
			rumBean.setNt_red_cnt(Long.parseLong(request.getParameter("nt_red_cnt")));
			rumBean.setNt_nav_type(Long.parseLong(request.getParameter("nt_nav_type")));
			rumBean.setNt_nav_st(Long.parseLong(request.getParameter("nt_nav_st")));
			rumBean.setNt_red_st(Long.parseLong(request.getParameter("nt_red_st")));
			rumBean.setNt_red_end(Long.parseLong(request.getParameter("nt_red_end")));					
			rumBean.setNt_fet_st(Long.parseLong(request.getParameter("nt_fet_st")));
			rumBean.setNt_dns_st(Long.parseLong(request.getParameter("nt_dns_st")));
			rumBean.setNt_dns_end(Long.parseLong(request.getParameter("nt_dns_end")));
			rumBean.setNt_con_st(Long.parseLong(request.getParameter("nt_con_st")));
			rumBean.setNt_con_end(Long.parseLong(request.getParameter("nt_con_end")));
			rumBean.setNt_req_st(Long.parseLong(request.getParameter("nt_req_st")));
			rumBean.setNt_res_st(Long.parseLong(request.getParameter("nt_res_st")));
			rumBean.setNt_res_end(Long.parseLong(request.getParameter("nt_res_end")));
			
			rumBean.setNt_domloading(Long.parseLong(request.getParameter("nt_domloading")));
			rumBean.setNt_domint(Long.parseLong(request.getParameter("nt_domint")));
			rumBean.setNt_domcontloaded_st(Long.parseLong(request.getParameter("nt_domcontloaded_st")));
			rumBean.setNt_domcontloaded_end(Long.parseLong(request.getParameter("nt_domcontloaded_end")));
			rumBean.setNt_domcomp(Long.parseLong(request.getParameter("nt_domcomp")));
			rumBean.setNt_load_st(Long.parseLong(request.getParameter("nt_load_st")));
			rumBean.setNt_load_end(Long.parseLong(request.getParameter("nt_load_end")));
			
			rumBean.setNt_unload_st(Long.parseLong(request.getParameter("nt_unload_st")));
			rumBean.setNt_unload_end(Long.parseLong(request.getParameter("nt_unload_end")));
			
			rumBean.setUrl(strURL);
			rumBean.setURLWithoutQueryString(strURLWithoutQueryString);
			rumBean.setQueryString(strQueryString);
			
			rumBean.setBoomerang_ver(request.getParameter("v"));
			
			// Merchant Name
			if(request.getParameter("merchantname")!=null)
			rumBean.setMerchantName(request.getParameter("merchantname"));
			else 
				rumBean.setMerchantName("Other");
			
			// OS Version
			if( request.getParameter("deviceType").trim().toUpperCase().equals("MOBILE")) {
				if(request.getParameter("OS").trim().contains("+")) {
					rumBean.setOsVersion(request.getParameter("OS").trim().split("+")[1]);
					
				}
				else {
					rumBean.setOsVersion("-1");
				}
			}else {
				rumBean.setOsVersion("-1");
			}
			
			rumBean.setBeacon_url("becon url");
			rumBean.setDateQueuedOn(new Date());
			
			//add to Queue
			bQueued = collectorManager.collectRumBean(rumBean);
			
		}catch(Exception e) {
			LogManager.errorLog(e);
		}finally {
			rumBean = null;
			collectorManager = null;
		}
	}
}
