package com.appedo.rumcollector.servlet;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.common.Constants;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.controller.CIThreadController;
import com.appedo.rumcollector.controller.RUMThreadController;


/**
 * Servlet to handle one operation for the whole application
 * @author navin
 *
 */
public class InitServlet extends HttpServlet {
	// set log access
	
	private static final long serialVersionUID = 1L;
	public static String realPath = null;	
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		//super();
		
		// declare servlet context
		ServletContext context = getServletContext();
		
		realPath = context.getRealPath("//");
		
		try {
			String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
			String strLog4jFilePath = context.getInitParameter("LOG4J_PROPERTIES_FILE_PATH");
			
			
			Constants.CONSTANTS_FILE_PATH = InitServlet.realPath+strConstantsFilePath;
			Constants.LOG4J_PROPERTIES_FILE = InitServlet.realPath+strLog4jFilePath;
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator(Constants.LOG4J_PROPERTIES_FILE);
			
			// Loads Constant properties 
			Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
			
			// loads Appedo config properties from the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired(Constants.APPEDO_CONFIG_FILE_PATH);

			// loads appedo constants; say loads appedoWhiteLabels, 
			Constants.loadAppedoWhiteLabels(Constants.APPEDO_CONFIG_FILE_PATH);
			
			/*
			 * CI JS `application_ci.js`, replaces appedo whitelabels from file and saves in another file `<download_appln_name_lowercase>_ci.js`
			 * Note: after loaded appedowhitelabels, replaced file 
			 */
			Constants.replaceCIJSAppedoWhiteLabels();
			
			
			for( int i=0; i<50; i++ ) {
				(new RUMThreadController()).start();
			}
			
			for( int i=0; i<50; i++ ) {
				(new CIThreadController()).start();
			}
			
		} catch(Throwable e) {
        	System.out.println("Exception in InitServlet.init: "+e.getMessage());
			e.printStackTrace();
			
			LogManager.errorLog(e);
		} finally {
		}
	}
	
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
