package com.appedo.rumcollector.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import net.sf.json.JSONObject;

import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;
import com.appedo.rumcollector.servlet.InitServlet;
import com.appedo.rumcollector.utils.UtilsFactory;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author veeru
 *
 */
public class Constants {
	
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	
	public static String PATH = ""; 
	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";

	public static JSONObject CI_COLLECTOR = null;
	public static String RUM_LOCATION;
	
	public static String APPEDO_SLA_COLLECTOR_URL = "";
	public static String CI_COLLECTOR_URL = null;
	
	
	public enum SLA_BREACH_SEVERITY {
		WARNING("WARNING"), CRITICAL("CRITICAL");
		
		private String strSLABreachSeverity;
		
		private SLA_BREACH_SEVERITY(String sSLABreachSeverity) {
			strSLABreachSeverity = sSLABreachSeverity;
		}
		
		public String toString() {
			return strSLABreachSeverity;
		}
	}
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) {
    	Properties prop = new Properties();
    	InputStream is = null;
    	
        try {
    		is = new FileInputStream(srtConstantsPath);
    		prop.load(is);
    		
     		// Appedo application's resource directory path
     		RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
     		
     		APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
     		
     		PATH = RESOURCE_PATH+prop.getProperty("Path");   		
			
			LOG4J_PROPERTIES_FILE = RESOURCE_PATH+prop.getProperty("LOG4J_CONFIG_FILE_PATH");
     		
        } catch(Exception e) {
        	LogManager.errorLog(e);
        } finally {
        	UtilsFactory.close(is);
        	is = null;
        }	
	}
	
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Exception {
    	Properties prop = new Properties();
    	InputStream is = null;
    	
        try {
    		is = new FileInputStream(strAppedoConfigPath);
    		prop.load(is);
    		
    		CI_COLLECTOR = JSONObject.fromObject(prop.getProperty("CI_COLLECTOR"));
    		RUM_LOCATION = prop.getProperty("RUM_LOCATION");
    		
     		APPEDO_SLA_COLLECTOR_URL = prop.getProperty("APPEDO_SLA_COLLECTOR");
     		CI_COLLECTOR_URL = getAsURL(CI_COLLECTOR);
     		
        } catch(Exception e) {
        	LogManager.errorLog(e);
        	throw e;
        } finally {
        	UtilsFactory.close(is);
        	is = null;
        }	
	}
	
	/**
	 * Collector in JSON format return as URL
	 * 
	 * @param joAppedoCollector
	 * @return
	 */
	public static String getAsURL(JSONObject joAppedoCollector) {
		return joAppedoCollector.getString("protocol") +"://"+ joAppedoCollector.getString("server") +":"+ joAppedoCollector.getString("port") +"/"+ joAppedoCollector.getString("application_name") +"/";
	}
	
	/**
	 * CI JS `application_ci.js`, replaces appedo whitelabels from file and saves in another file `<download_appln_name_lowercase>_ci.js`,
	 *   `download_appln_name_lowercase` is from `appedo_whitelabel`
	 * 
	 * @throws Exception
	 */
	public static void replaceCIJSAppedoWhiteLabels() throws Exception {
		HashMap<String, String> hmLabelReplacement = null;
		
		String strSourcePath = "", strTargetPath = "";
		
		File fileSource = null, fileTarget = null;
		
		boolean bFileSourceDeleted = false;
		
		try {
			strSourcePath = InitServlet.realPath+"/js/application_ci.js";
			strTargetPath = InitServlet.realPath+"/js/"+AppedoConstants.getAppedoWhiteLabel("download_appln_name_lowercase")+"_ci.js";
			
			fileSource = new File(strSourcePath);
			fileTarget = new File(strTargetPath);
			
			// 
			if ( ! fileSource.exists() ) {
				LogManager.infoLog("File not exists for whitelabel replacement: "+strSourcePath);
				//System.out.println("File not exists for whitelabel replacement: "+strSourcePath);
			} else {
				hmLabelReplacement = new HashMap<String, String>();
				hmLabelReplacement.put("CI_COLLECTOR_URL", Constants.CI_COLLECTOR_URL);
				
				// replaces whitelabels in file & saves in another file, `<download_appln_name_lowercase>_ci.js`
				AppedoConstants.applyAppedoWhiteLabelsInFile(strSourcePath, strTargetPath, hmLabelReplacement);
				
				// deletes `application_ci.js`
				bFileSourceDeleted = fileSource.delete();
				
				LogManager.infoLog("Replaces whitelabels & file saved: "+strTargetPath);
				//System.out.println("Replaces whitelabels & file saved: "+strTargetPath);
				
				LogManager.infoLog("Deleted "+strSourcePath+": "+bFileSourceDeleted);
				//System.out.println("Deleted "+strSourcePath+": "+bFileSourceDeleted);
			}
			
		} catch (Exception e) {
			throw e;
		} finally {
			strSourcePath = null;
			strTargetPath = null;
			
			fileSource = null;
			fileTarget = null;
		}
	}
	
	/**
	 * loads AppedoConstants, 
	 *   of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 *   
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoWhiteLabels(String strAppedoConfigPath) throws Exception {
		
		try {
			// 
			AppedoConstants.getAppedoConstants().loadAppedoConstants(strAppedoConfigPath);
		} catch (Exception e) {
			throw e;
		}
	}
}