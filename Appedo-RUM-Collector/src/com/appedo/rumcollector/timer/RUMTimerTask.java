package com.appedo.rumcollector.timer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.common.Constants;
import com.appedo.rumcollector.connect.DataBaseManager;
import com.appedo.rumcollector.dbi.RUMDBI;
import com.appedo.rumcollector.manager.RumManager;



public class RUMTimerTask extends TimerTask{
	
	RUMCollectorBean rumBean = null;
	
	int nLogTimerIndex = 0;
	
	private Connection con = null;
	
	public RUMTimerTask(int nLogTimerIndex) {
		try {
			this.nLogTimerIndex = nLogTimerIndex;
			DataBaseManager.doConnectionSetupIfRequired(Constants.APPEDO_CONFIG_FILE_PATH);
			this.con = DataBaseManager.giveConnection();
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	public void run() {
		PriorityBlockingQueue<RUMCollectorBean> pqAuditLogBeans = null;
		
		try {
			RUMDBI rumdbi = null;
			pqAuditLogBeans = RumManager.alRumLogQueues.get(nLogTimerIndex);
			
			if( pqAuditLogBeans != null ){
				rumdbi = new RUMDBI();
				while( ! pqAuditLogBeans.isEmpty() ){
					rumBean = pqAuditLogBeans.poll();
					if( rumBean != null ){
						rumdbi.insertRumTable(con, rumBean);
					}
				}
			}
		} catch (Throwable e) {
			LogManager.errorLog(e);
			// re-establish the connection if it is disconnected.
			// this will keep on waiting for the Connection to get established.
			try {
				if( ! DataBaseManager.isConnectionExists(con) ){
					pqAuditLogBeans.add(rumBean);
					con = DataBaseManager.reEstablishConnection(con);
				}
			} catch (SQLException e1) {
				LogManager.errorLog(e);
			}
			
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		DataBaseManager.close(con);
		con = null;
		
		super.finalize();
	}
}
