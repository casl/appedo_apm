package com.appedo.rumcollector.timer;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.CICollectorBean;
import com.appedo.rumcollector.manager.CIPerformanceManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Veeru
 *
 */
public class CIWriterTimer extends Thread {
	
	CICollectorBean cbCIEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public CIWriterTimer(CICollectorBean cbCIEntry) {
		this.cbCIEntry = cbCIEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting CI-Writer timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new CIPerformanceManager()).fetchBean(cbCIEntry);
			
//			Thread.currentThread().interrupt();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("CI-Writer thread destoryed "+lThisThreadId);
		
		super.finalize();
	}
}