package com.appedo.rumcollector.timer;

import com.appedo.manager.LogManager;
import com.appedo.rumcollector.bean.RUMCollectorBean;
import com.appedo.rumcollector.manager.RUMCollectorInsertManager;

/**
 * Timer class to start the queue drain-insert operation for given interval.
 * 
 * @author Navin
 *
 */
public class RUMWriterTimer extends Thread {
	
	RUMCollectorBean cbRumEntry = null;
	
	long lThisThreadId = 0, lDistributorThreadId = 0l;
	
	public RUMWriterTimer(RUMCollectorBean cbRumEntry) {
		this.cbRumEntry = cbRumEntry;
	}
	
	/**
	 * Start the queue drain-insert operation.
	 * 
	 */
	public void run() {
		try{
			lThisThreadId = Thread.currentThread().getId();
			
			LogManager.infoLog("Starting Module Perf.Counter timer thread(db insert) "+lThisThreadId+" <> Distributor: "+lDistributorThreadId);
			
			(new RUMCollectorInsertManager()).fetchBean(cbRumEntry);
			
//			Thread.currentThread().interrupt();
		} catch(Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		LogManager.infoLog("Module Perf.Counter thread destoryed "+lThisThreadId);
		
		super.finalize();
	}
}
