package com.appedo.rumcollector.bean;

import java.util.Date;

public class RUMCollectorBean implements Comparable<RUMCollectorBean>, Cloneable {
	
	private long lRUMId;
	private String browserName;
	private String devicename;
	private String guid;
	private long nt_red_end;
	private long nt_fet_st;
	private long nt_dns_st;
	private long nt_dns_end;
	private long nt_con_st;
	private long nt_con_end;
	private long nt_req_st;
	private long nt_res_st;
	private long nt_res_end;
	private long nt_domloading;
	private long nt_domint;
	private long nt_domcontloaded_st;
	private long nt_domcontloaded_end;
	private long nt_domcomp;
	private long nt_load_st;
	private long nt_load_end;
	private long nt_unload_st;
	private long nt_unload_end;
	private long nt_spdy;
	private long nt_first_paint;
	private long nt_red_cnt;
	private long nt_nav_type;
	private long nt_nav_st;
	private long nt_red_st;
	private String os;
	private String ipAddress;
	private String url;
	private String boomerang_ver;
	private String beacon_url;
	private String deviceType;
	private String merchantName;
	private String osVersion;
	private String strURLWithoutQueryString;
	private String strQueryString;
	private Date dateQueuedOn = null;
	
	
	public long getRUMId() {
		return lRUMId;
	}
	public void setRUMId(long lRUMId) {
		this.lRUMId = lRUMId;
	}
	
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getBeacon_url() {
		return beacon_url;
	}
	public void setBeacon_url(String beacon_url) {
		this.beacon_url = beacon_url;
	}

	public String getBoomerang_ver() {
		return boomerang_ver;
	}
	public void setBoomerang_ver(String boomerang_ver) {
		this.boomerang_ver = boomerang_ver;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}

	public String getBrowserName() {
		return browserName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public String getDevicename() {
		return devicename;
	}
	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}

	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}

	public long getNt_red_end() {
		return nt_red_end;
	}
	public void setNt_red_end(long nt_red_end) {
		this.nt_red_end = nt_red_end;
	}

	public long getNt_fet_st() {
		return nt_fet_st;
	}
	public void setNt_fet_st(long nt_fet_st) {
		this.nt_fet_st = nt_fet_st;
	}

	public long getNt_dns_st() {
		return nt_dns_st;
	}
	public void setNt_dns_st(long nt_dns_st) {
		this.nt_dns_st = nt_dns_st;
	}

	public long getNt_dns_end() {
		return nt_dns_end;
	}
	public void setNt_dns_end(long nt_dns_end) {
		this.nt_dns_end = nt_dns_end;
	}

	public long getNt_con_st() {
		return nt_con_st;
	}
	public void setNt_con_st(long nt_con_st) {
		this.nt_con_st = nt_con_st;
	}

	public long getNt_con_end() {
		return nt_con_end;
	}
	public void setNt_con_end(long nt_con_end) {
		this.nt_con_end = nt_con_end;
	}

	public long getNt_req_st() {
		return nt_req_st;
	}
	public void setNt_req_st(long nt_req_st) {
		this.nt_req_st = nt_req_st;
	}

	public long getNt_res_st() {
		return nt_res_st;
	}
	public void setNt_res_st(long nt_res_st) {
		this.nt_res_st = nt_res_st;
	}

	public long getNt_res_end() {
		return nt_res_end;
	}
	public void setNt_res_end(long nt_res_end) {
		this.nt_res_end = nt_res_end;
	}

	public long getNt_domloading() {
		return nt_domloading;
	}
	public void setNt_domloading(long nt_domloading) {
		this.nt_domloading = nt_domloading;
	}

	public long getNt_domint() {
		return nt_domint;
	}
	public void setNt_domint(long nt_domint) {
		this.nt_domint = nt_domint;
	}

	public long getNt_domcontloaded_st() {
		return nt_domcontloaded_st;
	}
	public void setNt_domcontloaded_st(long nt_domcontloaded_st) {
		this.nt_domcontloaded_st = nt_domcontloaded_st;
	}

	public long getNt_domcontloaded_end() {
		return nt_domcontloaded_end;
	}
	public void setNt_domcontloaded_end(long nt_domcontloaded_end) {
		this.nt_domcontloaded_end = nt_domcontloaded_end;
	}

	public long getNt_domcomp() {
		return nt_domcomp;
	}
	public void setNt_domcomp(long nt_domcomp) {
		this.nt_domcomp = nt_domcomp;
	}

	public long getNt_load_st() {
		return nt_load_st;
	}
	public void setNt_load_st(long nt_load_st) {
		this.nt_load_st = nt_load_st;
	}

	public long getNt_load_end() {
		return nt_load_end;
	}
	public void setNt_load_end(long nt_load_end) {
		this.nt_load_end = nt_load_end;
	}

	public long getNt_unload_st() {
		return nt_unload_st;
	}
	public void setNt_unload_st(long nt_unload_st) {
		this.nt_unload_st = nt_unload_st;
	}

	public long getNt_unload_end() {
		return nt_unload_end;
	}
	public void setNt_unload_end(long nt_unload_end) {
		this.nt_unload_end = nt_unload_end;
	}

	public long getNt_spdy() {
		return nt_spdy;
	}
	public void setNt_spdy(long nt_spdy) {
		this.nt_spdy = nt_spdy;
	}

	public long getNt_first_paint() {
		return nt_first_paint;
	}
	public void setNt_first_paint(long nt_first_paint) {
		this.nt_first_paint = nt_first_paint;
	}

	public long getNt_red_cnt() {
		return nt_red_cnt;
	}
	public void setNt_red_cnt(long nt_red_cnt) {
		this.nt_red_cnt = nt_red_cnt;
	}

	public long getNt_nav_type() {
		return nt_nav_type;
	}
	public void setNt_nav_type(long nt_nav_type) {
		this.nt_nav_type = nt_nav_type;
	}

	public long getNt_nav_st() {
		return nt_nav_st;
	}
	public void setNt_nav_st(long nt_nav_st) {
		this.nt_nav_st = nt_nav_st;
	}

	public long getNt_red_st() {
		return nt_red_st;
	}
	public void setNt_red_st(long nt_red_st) {
		this.nt_red_st = nt_red_st;
	}
	
	public String getURLWithoutQueryString() {
		return strURLWithoutQueryString;
	}
	public void setURLWithoutQueryString(String strURLWithoutQueryString) {
		this.strURLWithoutQueryString = strURLWithoutQueryString;
	}
	
	public String getQueryString() {
		return strQueryString;
	}
	public void setQueryString(String strQueryString) {
		this.strQueryString = strQueryString;
	}
	
	public Date getDateQueuedOn() {
		return dateQueuedOn;
	}
	public void setDateQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}

	
	
	@Override
	public int compareTo(RUMCollectorBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (dateQueuedOn.getTime() - another.getDateQueuedOn().getTime()));
	}
	
	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
