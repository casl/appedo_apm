var @download_appln_name_camelcase@_CI = {
	userDetails: {},
	setUserProps: function( params ){
		this.userDetails = params;
	},
	triggerEvent: function (event_name, event_method, params){
			var xmlhttp = new XMLHttpRequest();
			var user = detect.parse(navigator.userAgent);
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
	
			xmlhttp.open("POST", "@CI_COLLECTOR_URL@ciCollector", true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			
			xmlhttp.send("agent_type=JS&env="+@download_appln_name_uppercase@_YOUR_ENVIRONMENT+"&ci_params="+JSON.stringify(params)+"&user_params="+JSON.stringify(this.userDetails)+"&guid="+@download_appln_name_uppercase@_UI_GUID+"&eventName="
					+event_name+"&eventMethod="+event_method+"&browserName="+user.browser.name+"&deviceType="
					+user.device.type+"&OS="+user.os.name+"&devicename="+user.device.name+"&merchantname="+user.device.family);
	},
	
	startTransaction: function (event_name, event_method, params){
		params.eventName = event_name;
		params.eventMethod = event_method;
		params.eventStartTime = new Date().getTime();
		// alert(params.eventStartTime);
		return params;
	}, 
	
	completeTransaction: function (params){
		var eventName, eventMethod;
		eventName = params.eventName;
		eventMethod = params.eventMethod;
		
		delete params.eventName;
		delete params.eventMethod;
		
		params.eventEndTime = new Date().getTime();
		// alert("Start: "+params.eventStartTime+", Stop: "+ params.eventEndTime);
		this.triggerEvent(eventName, eventMethod, params);
	}

};

