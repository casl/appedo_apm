package com.appedo.collector.bean;

/**
 * This Bean is to hold the counter-sets as String, along with its received date-time.
 * 
 * @author Ramkumar R
 *
 */
public class CollectorBean implements Comparable<CollectorBean> {
	private String strCounterParams = null;
	private Long dateReceivedOn = null;
	
	/**
	 * Returns the Counter-sets which is received as string in the HTTP request.
	 * 
	 * @return
	 */
	public String getCounterParams() {
		return strCounterParams;
	}
	/**
	 * Sets the Counter-sets to the bean member.
	 * 
	 * @param strCounterParams
	 */
	public void setCounterParams(String strCounterParams) {
		this.strCounterParams = strCounterParams;
	}
	
	/**
	 * Returns the date-time when this counter-set is received.
	 * 
	 * @return
	 */
	public Long getReceivedOn() {
		return dateReceivedOn;
	}
	/**
	 * Sets the given Date object to the bean member.
	 * 
	 * @param dateReceivedOn
	 */
	public void setReceivedOn(Long dateReceivedOn) {
		this.dateReceivedOn = dateReceivedOn;
	}
	
    @Override
    /**
     * Overridden the compareTo, with the received date, to queue the bean in PriorityBlockingQueue.
     * 
     */
    public int compareTo(CollectorBean other){
        // compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	long lDiff = dateReceivedOn - other.getReceivedOn();
    	if( lDiff != 0 ){
    		return 1;
    	}
		
    	return strCounterParams.compareTo(other.getCounterParams());
    }
    
    /**
     * Destroy the members o this class.
     */
    public void destory() {
    	this.strCounterParams = null;
		this.dateReceivedOn = null;
	}
    
    @Override
    protected void finalize() throws Throwable {
    	this.strCounterParams = null;
		this.dateReceivedOn = null;
		
    	super.finalize();
    }
}
