package com.appedo.collector.bean;

import java.util.Date;

import net.sf.json.JSONArray;

public class SlaCollectorBean implements Comparable<SlaCollectorBean>, Cloneable {
private static JSONArray  joSlaBreachCountersBean = new JSONArray();
	
	/**
	 * @return the hmSlaBreachCountersBean
	 */
	public static JSONArray getJoCountersBean() {		
		return joSlaBreachCountersBean;
	}
	
	/**
	 * @param hmSlaBreachCountersBean the hmCountersBean to set
	 */
	public static void setJoCountersBean(JSONArray JoBreachCounters) {
		
		joSlaBreachCountersBean = JoBreachCounters;
	}

	private Date dateQueuedOn = null;
	
	public Date getDateQueuedOn() {
		return dateQueuedOn;
	}

	public void setDateQueuedOn(Date dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}

	
	
	@Override
	public int compareTo(SlaCollectorBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (dateQueuedOn.getTime() - another.getDateQueuedOn().getTime()));
	}
	
	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
