package com.appedo.collector.bean;

import net.sf.json.JSONObject;

public class LOGDataBean implements Comparable<LOGDataBean>, Cloneable {
	
	private Long dateQueuedOn = null;
	private JSONObject joLogData = new JSONObject();
	
	public Long getDateQueuedOn() {
		return dateQueuedOn;
	}
	public void setDateQueuedOn(Long dateQueuedOn) {
		this.dateQueuedOn = dateQueuedOn;
	}
	
	public JSONObject getLogData() {
		return joLogData;
	}
	public void setLogData(JSONObject joLogData) {
		this.joLogData = joLogData;
	}
	
	@Override
	public int compareTo(LOGDataBean another) {
		// compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
    	
    	return ((int) (getDateQueuedOn() - another.getDateQueuedOn()));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}