package com.appedo.collector.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.appedo.collector.bean.LOGDataBean;
import com.appedo.collector.bean.RUMDataBean;
import com.appedo.collector.manager.CollectorManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

/**
 * Servlet implementation class LogCollector
 */
//@WebServlet("/LogCollector")
public class LogCollector extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogCollector() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) {
		
		// Members
		StringBuffer sb = new StringBuffer();
		String line = null;
		
		LOGDataBean logBean = new LOGDataBean();
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		boolean bQueued = false;
		
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null){
				sb.append(line);
			}
			
			if( sb.toString().startsWith("{") || sb.toString().endsWith("}")) {
				JSONObject joLogData = JSONObject.fromObject(sb.toString());
				
				logBean.setLogData(joLogData);
				logBean.setDateQueuedOn(new Date().getTime());
				
				//add to Queue
				bQueued = collectorManager.collectLogBean(logBean);
			}
			
/*			Enumeration<String> parameterNames = request.getParameterNames();

			while (parameterNames.hasMoreElements()) {
				String paramName = parameterNames.nextElement();
				
				String[] paramValues = request.getParameterValues(paramName);
				for (int i = 0; i < paramValues.length; i++) {
					String paramValue = paramValues[i];
					System.out.println(paramName+"-"+"\t"+paramValue);
				}

			}*/
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			logBean = null;
			collectorManager = null;
		}
	}
}