package com.appedo.collector.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.collector.bean.RUMDataBean;
import com.appedo.collector.manager.CollectorManager;

/**
 * Servlet implementation class RumCollector
 */
//@WebServlet("/RumCollector")
public class RumCollector extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RumCollector() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doAction(request,response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) {
		
		// Members
		RUMDataBean rumBean = new RUMDataBean();
		CollectorManager collectorManager = CollectorManager.getCollectorManager();
		boolean bQueued = false;
		String ipAddress = null, strURL = null, strURLWithoutQueryString = "", strQueryString = "";
		int nIdxStartQueryString = -1;
		
		try {
			//Get client ip address
			ipAddress = request.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}		
			
			strURL = request.getParameter("u");
			rumBean.setUrl(strURL);
			
			// URL without query string
			nIdxStartQueryString = strURL.indexOf("?");
			strURLWithoutQueryString = nIdxStartQueryString != -1 ? strURL.substring(0, nIdxStartQueryString) : strURL;
			rumBean.setURLWithoutQueryString(strURLWithoutQueryString);
			
			// query string
			strQueryString = nIdxStartQueryString != -1 ? strURL.substring(nIdxStartQueryString, strURL.length()) : "";
			rumBean.setQueryString(strQueryString);
			
			// capture all input parameters & set parameters to bean
			rumBean.setIpAddress(ipAddress);
			
			rumBean.setBrowserName(request.getParameter("browserName"));
			rumBean.setDeviceType(request.getParameter("deviceType"));
			rumBean.setOs(request.getParameter("OS"));
			rumBean.setDevicename(request.getParameter("devicename"));
			rumBean.setGuid(request.getParameter("guid"));
			
			if( request.getParameter("nt_red_cnt") != null ) {
				rumBean.setNt_red_cnt(Long.parseLong(request.getParameter("nt_red_cnt")));
			}
			if( request.getParameter("nt_nav_type") != null ) {
				rumBean.setNt_nav_type(Long.parseLong(request.getParameter("nt_nav_type")));
			}
			if( request.getParameter("nt_nav_st") != null ) {
				rumBean.setNt_nav_st(Long.parseLong(request.getParameter("nt_nav_st")));
			}
			if( request.getParameter("nt_red_st") != null ) {
				rumBean.setNt_red_st(Long.parseLong(request.getParameter("nt_red_st")));
			}
			if( request.getParameter("nt_red_end") != null ) {
				rumBean.setNt_red_end(Long.parseLong(request.getParameter("nt_red_end")));
			}
			if( request.getParameter("nt_fet_st") != null ) {				
				rumBean.setNt_fet_st(Long.parseLong(request.getParameter("nt_fet_st")));
			}
			if( request.getParameter("nt_dns_st") != null ) {
				rumBean.setNt_dns_st(Long.parseLong(request.getParameter("nt_dns_st")));
			}
			if( request.getParameter("nt_dns_end") != null ) {
				rumBean.setNt_dns_end(Long.parseLong(request.getParameter("nt_dns_end")));
			}
			if( request.getParameter("nt_con_st") != null ) {
				rumBean.setNt_con_st(Long.parseLong(request.getParameter("nt_con_st")));
			}
			if( request.getParameter("nt_con_end") != null ) {
				rumBean.setNt_con_end(Long.parseLong(request.getParameter("nt_con_end")));
			}
			if( request.getParameter("nt_req_st") != null ) {
				rumBean.setNt_req_st(Long.parseLong(request.getParameter("nt_req_st")));
			}
			if( request.getParameter("nt_res_st") != null ) {
				rumBean.setNt_res_st(Long.parseLong(request.getParameter("nt_res_st")));
			}
			if( request.getParameter("nt_res_end") != null ) {
				rumBean.setNt_res_end(Long.parseLong(request.getParameter("nt_res_end")));
			}
			
			if( request.getParameter("nt_domloading") != null ) {
				rumBean.setNt_domloading(Long.parseLong(request.getParameter("nt_domloading")));
			}
			if( request.getParameter("nt_domint") != null ) {
				rumBean.setNt_domint(Long.parseLong(request.getParameter("nt_domint")));
			}
			if( request.getParameter("nt_domcontloaded_st") != null ) {
				rumBean.setNt_domcontloaded_st(Long.parseLong(request.getParameter("nt_domcontloaded_st")));
			}
			if( request.getParameter("nt_domcontloaded_end") != null ) {
				rumBean.setNt_domcontloaded_end(Long.parseLong(request.getParameter("nt_domcontloaded_end")));
			}
			if( request.getParameter("nt_domcomp") != null ) {
				rumBean.setNt_domcomp(Long.parseLong(request.getParameter("nt_domcomp")));
			}
			if( request.getParameter("nt_load_st") != null ) {
				rumBean.setNt_load_st(Long.parseLong(request.getParameter("nt_load_st")));
			}
			if( request.getParameter("nt_load_end") != null ) {
				rumBean.setNt_load_end(Long.parseLong(request.getParameter("nt_load_end")));
			}
			
			if( request.getParameter("nt_unload_st") != null ) {
				rumBean.setNt_unload_st(Long.parseLong(request.getParameter("nt_unload_st")));
			}
			if( request.getParameter("nt_unload_end") != null ) {
				rumBean.setNt_unload_end(Long.parseLong(request.getParameter("nt_unload_end")));
			}
			
			rumBean.setBoomerang_ver(request.getParameter("v"));
			
			// Merchant Name
			if(request.getParameter("merchantname")!=null)
				rumBean.setMerchantName(request.getParameter("merchantname"));
			else 
				rumBean.setMerchantName("Other");
			
			// OS Version
			if( request.getParameter("deviceType").trim().toUpperCase().equals("MOBILE")) {
				if(request.getParameter("OS").trim().contains("+")) {
					rumBean.setOsVersion(request.getParameter("OS").trim().split("+")[1]);
				} else {
					rumBean.setOsVersion("-1");
				}
			}else {
				rumBean.setOsVersion("-1");
			}
			
			rumBean.setBeacon_url("becon url");
			
			rumBean.setDateQueuedOn(new Date().getTime());
			
			//LogManager.infoLog("received RUMBean: " + JSONObject.fromObject(rumBean));
			
			//add to Queue
			bQueued = collectorManager.collectRumBean(rumBean);
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			rumBean = null;
			collectorManager = null;
		}
	}
}