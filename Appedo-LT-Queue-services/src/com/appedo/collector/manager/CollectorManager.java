package com.appedo.collector.manager;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.PriorityBlockingQueue;

import com.appedo.collector.bean.CIDataBean;
import com.appedo.collector.bean.CollectorBean;
import com.appedo.collector.bean.LOGDataBean;
import com.appedo.collector.bean.RUMDataBean;
import com.appedo.collector.bean.SlaCollectorBean;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.dbi.CollectorDBI;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Manager which holds the queues, of all the performance counters
 * Singleton class. So no objects can be created.
 * 
 * @author : Veeru
 * @Date   : 01-07-2015
 *
 */
public class CollectorManager{
	
	// Singleton object, used globally with static getCollectorManager().
		private static CollectorManager collectorManager = new CollectorManager();
		
		// Queue to store performance counter data of agents
		private PriorityBlockingQueue<CollectorBean> pqModulePerformanceCounters = new PriorityBlockingQueue<CollectorBean>();
		private PriorityBlockingQueue<RUMDataBean> pqRUMData = new PriorityBlockingQueue<RUMDataBean>();
		private PriorityBlockingQueue<CIDataBean> pqCIData = new PriorityBlockingQueue<CIDataBean>();
		private PriorityBlockingQueue<SlaCollectorBean> pqModulePerformanceSla = new PriorityBlockingQueue<SlaCollectorBean>();
		
		private PriorityBlockingQueue<LOGDataBean> pgLOGData = new PriorityBlockingQueue<LOGDataBean>();
		
		private PriorityBlockingQueue<String> pqJavaProfiler = null;
		private PriorityBlockingQueue<String> pqDotNetProfiler = null;
		private PriorityBlockingQueue<String> pqPGSlowQueries = null;
		
		// private Connection con = null;
		private Connection conBackup = null;
		
		private HashMap<String, Object[]> hmLatestCounters = null;
		
		private static HashMap<String,Object>  hmCountersBean = new HashMap<String,Object>();
		private static HashMap<String,Object>  hmSlaCountersBean = new HashMap<String,Object>();
		public static HashMap<String,Object>  hmUpgradeAgentBean = new HashMap<String,Object>();
		private PriorityBlockingQueue<String> pqUpgradeStatus = null;
		
		public Object[] getLatestCounters(String strKey) {
			return hmLatestCounters.get(strKey);
		}
		public boolean isLatestCountersContains(String strKey) {
			return hmLatestCounters.containsKey(strKey);
		}
		public void addLatestCounter(String strKey, Object[] objArr) {
			this.hmLatestCounters.put(strKey, objArr);
		}
		
		

		/**
		 * @return the hmCountersBean
		 */
		public static JSONArray getCounters(String strGUID) {		
			return (JSONArray) hmCountersBean.get(strGUID);
		}
		
		/**
		 * @return the hmCountersBean
		 */
		public static JSONArray getSlaCounters(String strGUID) {		
			return (JSONArray) hmSlaCountersBean.get(strGUID);
		}

		/**
		 * @param hmCountersBean the hmCountersBean to set
		 */
		public static void setCounters(String strGUID, JSONArray jaNewCounterSet) {
			
			CollectorManager.hmCountersBean.put(strGUID, jaNewCounterSet);
		}
		
		/**
		 * @param hmCountersBean the hmCountersBean to set
		 */
		public static void setSlaCounters(String strGUID, JSONArray jaNewCounterSet) {
			
			CollectorManager.hmSlaCountersBean.put(strGUID, jaNewCounterSet);
		}
		/**
		 * to remove the counter set from hashmap table
		 * @param strGUID
		 */
		public static void removeCounterSet(String strGUID) {
			if(CollectorManager.hmCountersBean.containsKey(strGUID)) {
				CollectorManager.hmCountersBean.remove(strGUID);
			}
			
		}

		/**
		 * to remove the counter set from hashmap table
		 * @param strGUID
		 */
		public static void removeSlaCounterSet(String strGUID) {
			if(CollectorManager.hmSlaCountersBean.containsKey(strGUID)) {
				CollectorManager.hmSlaCountersBean.remove(strGUID);
			}
			
		}
		/**
		 * Avoid multiple object creation, by Singleton
		 */
		private CollectorManager() {
			
			try{
				// initialize all required queue in this private constructor.
				pqModulePerformanceCounters = new PriorityBlockingQueue<CollectorBean>();
				pqJavaProfiler = new PriorityBlockingQueue<String>();
				pqDotNetProfiler = new PriorityBlockingQueue<String>();
				pqPGSlowQueries = new PriorityBlockingQueue<String>();
				pqUpgradeStatus = new PriorityBlockingQueue<String>();
				// con = DataBaseManager.giveConnection();
				hmLatestCounters = new HashMap<String, Object[]>();
				
			} catch(Exception ex) {
				LogManager.errorLog(ex);
			}
		}
		
		/**
		 * Access the only[singleton] CollectorManager object.
		 * 
		 * @return CollectorManager
		 */
		public static CollectorManager getCollectorManager(){
			return collectorManager;
		}
		
		
		
		
		
		/**
		 * Returns the application profiler agent's configuration details such as UID & last ThreadId used for this GUID.
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public StringBuilder getProfilerConfigurations(String strGUID) throws Exception {
			StringBuilder sbReturn = null;
			//String strEncryptedUID = null;
			long lLastThreadId = 0l, lUID = 0l;
			HashMap<String, Object> hmKeyValues = null;
			
			try{
				CollectorDBI collectorDBI = new CollectorDBI();
				//strEncryptedUID = collectorDBI.getEncryptedModuleUID(con, strGUID);
				lUID = collectorDBI.getApplicationUID(con, strGUID);
				
				JavaProfilerDBI javaProfilerDBI = new JavaProfilerDBI();
				lLastThreadId = javaProfilerDBI.getLastThreadId(con, lUID);
				
				hmKeyValues = new HashMap<String, Object>();
				// Commented as UID-Concept is removed
				// hmKeyValues.put("uid", strEncryptedUID);
				
				hmKeyValues.put("lastThreadId", new Long(lLastThreadId));
				
				sbReturn = UtilsFactory.getJSONSuccessReturn(hmKeyValues);
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				throw ex;
			} finally {
				//strEncryptedUID = null;
				lLastThreadId = 0l;
				lUID = 0l;
				UtilsFactory.clearCollectionHieracy(hmKeyValues);
				hmKeyValues = null;
			}
			
			return sbReturn;
		}*/
		
		/**
		 * Queue the given Counter CollectorBean into the asked Family queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */
		public boolean collectPerformanceCounters(String strCounterParams) throws Exception {
			CollectorBean collBean = null;
			
			try {
				collBean = new CollectorBean();
				collBean.setCounterParams(strCounterParams);
				collBean.setReceivedOn(new Date().getTime());
				
				return pqModulePerformanceCounters.add(collBean);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		/**
		 * Poll the top Counter CollectorBean from the asked Family queue.
		 * 
		 * @param agent_family
		 * @param nIndex
		 * @return
		 */
		public CollectorBean pollCounter(){
			CollectorBean cbCounterEntry = null;
			
			try {
				cbCounterEntry = pqModulePerformanceCounters.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return cbCounterEntry;
		}
		/**
		 * Get the size of the asked Family queue.
		 * 
		 * @return int
		 */
		public int getCounterLength(){
			return pqModulePerformanceCounters.size();
		}
		
		
		/**
		 * Collect and add the Java's profiled data into MySQL queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */
		public boolean collectJavaProfiler(String strCounterParams) throws Exception {
			
			try {
				return pqJavaProfiler.add(strCounterParams);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		/**
		 * Get the top counter data from the java profile data queue.
		 * 
		 * @return Counter JSON data in String format
		 */
		public String pollJavaProfiler(){
			String strCounterEntry = null;
			
			try {
				strCounterEntry = pqJavaProfiler.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return strCounterEntry;
		}
		/**
		 * Get the size of Java profile entries queue.
		 * 
		 * @return int
		 */
		public int getJavaProfilerLength(){
			return pqJavaProfiler.size();
		}
		
		/**
		 * Collect and add the DOTNET's profiled data into MySQL queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */
		public boolean collectDotNetProfiler(String strCounterParams) throws Exception {
			
			try {
				return pqDotNetProfiler.add(strCounterParams);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		/**
		 * Collect and add the POSTGRES's slow queries data into postgres queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */

		public boolean collectPGSlowQueries(String strCounterParams) throws Exception {
			
			try {
				return pqPGSlowQueries.add(strCounterParams);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		
		/**
		 * Get the top counter data from the POSTGRES data queue.
		 * 
		 * @return Counter JSON data in String format
		 */
		public String pollPGSlowQueries(){
			String strCounterEntry = null;
			
			try {
				strCounterEntry = pqPGSlowQueries.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return strCounterEntry;
		}
		/**
		 * Get the top counter data from the DOTNET profile data queue.
		 * 
		 * @return Counter JSON data in String format
		 */
		public String pollDotNetProfiler(){
			String strCounterEntry = null;
			
			try {
				strCounterEntry = pqDotNetProfiler.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return strCounterEntry;
		}
		
		/**
		 * Get the size of Postgres Slow Queries entries queue.
		 * 
		 * @return int
		 */
		public int getPGSlowQueriesLength(){
			return pqPGSlowQueries.size();
			
		}
		
		/**
		 * Get the size of DOTNET profile entries queue.
		 * 
		 * @return int
		 */
		public int getDotNetProfilerLength(){
			return pqDotNetProfiler.size();
		}
		/**
		 * Returns the application agent's configuration details such as UID.
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public StringBuilder getModuleConfigCounters(String strGUID) throws Exception {
			StringBuilder sbReturn = null;
			HashMap<String, Object> hmKeyValues = null;
			JSONArray jaCounters = null;
			JSONObject joLicense = null, joUserLic = null;
		
			JSONArray jaSlaCounters = null;
			try{
				CollectorDBI collectorDBI = new CollectorDBI(); 
				// TODO get the new Counter-set to be collected
				
				long lUID = collectorDBI.getModuleUID(con, strGUID);
				if( lUID == -1l ) {
					sbReturn = UtilsFactory.getJSONSuccessReturn("kill");
					//throw new Exception("Given GUID is not matching: "+strGUID);				
				}else {
					// get UserId and License level
					joUserLic = collectorDBI.getUserLicense(con, strGUID);
					
					// get APM License details
					joLicense = collectorDBI.getLicenseAPMDetails(con, joUserLic);
					hmKeyValues = new HashMap<String, Object>();
					// get user selected counter for monitor
					//if( joLicense != null){
						jaCounters = collectorDBI.getConfigCounters(con, lUID, joLicense);
						
						//hmKeyValues.put("MonitorCounterSet", jaCounters);
					//}else {
						hmKeyValues.put("MonitorCounterSet", jaCounters);	
					//}
					
					// get user mapped counters for sla
					jaSlaCounters = getSlaConfigCounters(strGUID);
					//System.out.println("jaSlaCounters " + jaSlaCounters.toString());
					if(jaSlaCounters!=null) {
						//System.out.println("INSIDE IF ");
						hmKeyValues.put("SlaCounterSet", jaSlaCounters);
					}else  {
						//System.out.println("INSIDE else ");
						jaSlaCounters = new JSONArray();
					}
					
					//System.out.println("hmKeyValues :" + hmKeyValues.toString());
					sbReturn = UtilsFactory.getJSONSuccessReturn(hmKeyValues);
					// remove guid from master				
					CollectorManager.hmCountersBean.remove(strGUID);
					
				}
				
				
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				throw ex;
			} finally{
				joLicense = null;
				joUserLic = null;
			}
			
			return sbReturn;
		}*/
		
		/**
		 * 
		 * @param strGUID
		 * @return
		 *
		public JSONArray getSlaConfigCounters(String strGUID) {
			PostMethod method = null;
			HttpClient client = null;
			String responseJSONStream = null;
			JSONArray jaSlaFirstResp = null;
			JSONArray jaCounters = null;
			
			try {
				method = new PostMethod(Constants.APPEDO_SLA_COLLECTOR_URL);
				method.setParameter("guid", strGUID);
				method.setParameter("command", "AgentFirstRequest");
				client = new HttpClient();
				int statusCode = client.executeMethod(method);
				method.setRequestHeader("Connection", "close");
				if (statusCode != HttpStatus.SC_OK) {
					
					System.err.println("Method failed: " + method.getStatusLine());
					
				}else {
					responseJSONStream = method.getResponseBodyAsString();
					
					//System.out.println("Sla Counters :" +responseJSONStream);
					if( responseJSONStream.trim().startsWith("{") && responseJSONStream.trim().endsWith("}")) {
						JSONObject joResp = JSONObject.fromObject(responseJSONStream);
						if(joResp.getBoolean("success")){
							if(joResp.getString("message").trim()!=null && joResp.getString("message").trim().startsWith("[") && joResp.getString("message").trim().endsWith("]")) {
								jaCounters = joResp.getJSONArray("message");
							}else {
								jaCounters = new JSONArray();
							}
							
						}
					}
				}
				
				
			}catch(Exception e) {			
				LogManager.errorLog(e);
				jaCounters = new JSONArray();
			}finally {
				
			}
			
			return jaCounters;
		}*/
		
		/**
		 * Returns the application agent's configuration details such as UID.
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public StringBuilder getModuleConfigurations(String strGUID) throws Exception {
			StringBuilder sbReturn = null;
			HashMap<String, Object> hmKeyValues = null;
			JSONArray jaCounters = null;
			
			try{
				// TODO get the new Counter-set to be collected
				jaCounters = CollectorManager.getCounters(strGUID);
				
				hmKeyValues = new HashMap<String, Object>();
				hmKeyValues.put("MonitorCounterSet", jaCounters);
				
				sbReturn = UtilsFactory.getJSONSuccessReturn(hmKeyValues);
				// remove guid from master
				
				CollectorManager.removeCounterSet(strGUID);
			} catch (Exception ex) {
				ex.printStackTrace();
				LogManager.errorLog(ex);
				throw ex;
			}
			
			return sbReturn;
		}*/
		
		/**
		 * Returns the application agent's configuration details such as UID.
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public StringBuilder setModuleConfigurations(String strGUID, String strnewCounterSet) throws Exception {
			StringBuilder sbReturn = null;
			HashMap<String, Object> hmKeyValues = null;
			
			// { success: true, false: false, newCounterSets: [ {}, {}, {} ] }
			try{
				LogManager.infoLog("strnewCounterSet :"+strnewCounterSet);
				CollectorManager.setCounters(strGUID, JSONArray.fromObject(strnewCounterSet));
				
				hmKeyValues = new HashMap<String, Object>();
				hmKeyValues.put("guid", strGUID);
				
				sbReturn = UtilsFactory.getJSONSuccessReturn(hmKeyValues);
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				throw ex;
			}
			
			return sbReturn;
		}*/
		
		/**
		 * Returns the application agent's configuration details such as UID.
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public StringBuilder setSlaModuleConfigurations(String strGUID, String strnewCounterSet) throws Exception {
			StringBuilder sbReturn = null;
			HashMap<String, Object> hmKeyValues = null;
			
			// { success: true, false: false, newCounterSets: [ {}, {}, {} ] }
			try{
				LogManager.infoLog("strnewSlaCounterSet :"+strnewCounterSet);
				CollectorManager.setSlaCounters(strGUID, JSONArray.fromObject(strnewCounterSet));
				
				hmKeyValues = new HashMap<String, Object>();
				hmKeyValues.put("guid", strGUID);
				
				sbReturn = UtilsFactory.getJSONSuccessReturn(hmKeyValues);
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				throw ex;
			}
			
			return sbReturn;
		}*/

		/**
		 * 
		 * @param strGUID
		 *
		public  void updateStatusOfCounterMaster(String strGUID) {
			
			CollectorDBI collectorDBI = null;
			try{
				collectorDBI = new CollectorDBI();
				collectorDBI.updateStatusOfCounterMaster(con, strGUID);
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				throw ex;
			}finally {
				collectorDBI = null;
			}
			
		}*/
		
		/**
		 * 
		 * @param strGUID
		 * @param joRecChildCounters
		 * @return
		 *
		public StringBuilder updateChildCounters(String strGUID, JSONObject joRecChildCounters) {
			StringBuilder sbReturn = null;
			CollectorDBI collectorDBI = null;
			
			try{
				collectorDBI = new CollectorDBI();
				
				long lUID = collectorDBI.getModuleUID(con, strGUID);
				if( lUID == -1l ) {
					sbReturn = UtilsFactory.getJSONSuccessReturn("kill");
					//throw new Exception("Given GUID is not matching: "+strGUID);				
				}else {
					sbReturn = collectorDBI.updateChildCounters(con, strGUID, lUID, joRecChildCounters);
				}
				
			} catch (Exception ex) {
				LogManager.errorLog(ex);
				
			}finally {
				collectorDBI = null;
			}
			
			return sbReturn;
		}*/
		
		/**
		 * Returns the UID of the Application for the given encrypted UID, for this agent.
		 * 
		 * @param strEncryptedUID
		 * @return
		 * @throws Exception
		 *
		public Long getUID(String strGUID) throws Exception {
			Long lUID = null;
			CollectorDBI collectorDBI = null;
			
			try{
				collectorDBI = new CollectorDBI();
				
				lUID = collectorDBI.getModuleUID(con, strGUID);
			} catch ( Throwable th ) {
				throw th;
			} finally {
				collectorDBI = null;
			}
			
			return lUID;
		}*/
		
		/**
		 * Queue the given bean CollectorBean into the asked Family queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */
		public boolean collectRumBean(RUMDataBean collBean) throws Exception {
			
			try {
				return pqRUMData.add(collBean);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		/**
		 * Poll the top Counter CollectorBean from the asked Family queue.
		 * 
		 * @param agent_family
		 * @param nIndex
		 * @return
		 */
		public RUMDataBean pollRumBean(){
			RUMDataBean cbRumEntry = null;
			
			try {
				cbRumEntry = pqRUMData.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return cbRumEntry;
		}
		/**
		 * Get the size of the asked Family queue.
		 * 
		 * @return int
		 */
		public int getRumQueueLength(){
			return pqRUMData.size();
		}
		
		/**
		 * Queue the given bean CollectorBean into the asked Family queue.
		 * 
		 * @param collBean
		 * @return boolean
		 * @throws Exception
		 */
		public boolean collectLogBean(LOGDataBean collBean) throws Exception {
			
			try {
				return pgLOGData.add(collBean);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		
		/**
		 * Poll the top Counter CollectorBean from the asked Family queue.
		 * 
		 * @return LOGDataBean
		 */
		public ArrayList<LOGDataBean> pollLogBean(){
			ArrayList<LOGDataBean> alLogEntry = new ArrayList<LOGDataBean>();
			try {
				pgLOGData.drainTo(alLogEntry);
				//pgLOGData.drainTo
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			return alLogEntry;
		}
		
		/**
		 * Get the size of the asked Family queue.
		 * 
		 * @return int
		 */
		public int getLogQueueLength(){
			return pgLOGData.size();
		}
		
		/**
		 * Queue the given bean CollectorBean into the asked Family queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception
		 */
		public boolean collectCIBean(CIDataBean collBean) throws Exception {
			try {
				return pqCIData.add(collBean);
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
		}
		/**
		 * Poll the top Counter CollectorBean from the asked Family queue.
		 * 
		 * @param agent_family
		 * @param nIndex
		 * @return
		 */
		public CIDataBean pollCIBean(){
			CIDataBean cbCIEntry = null;
			
			try {
				cbCIEntry = pqCIData.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return cbCIEntry;
		}
		/**
		 * Get the size of the asked Family queue.
		 * 
		 * @return int
		 */
		public int getCIQueueLength(){
			return pqCIData.size();
		}
		
		/**
		 * 
		 * @param strGUID
		 * @return
		 * @throws Exception
		 *
		public JSONArray getSLACounters(String strGUID) throws Exception {
			JSONArray jaSLACounters = null;
			CollectorDBI collectorDBI = null;
			try{
				collectorDBI = new CollectorDBI();
				jaSLACounters = collectorDBI.getSLACounters(con, strGUID);
			}catch(Exception e){
				LogManager.errorLog(e);
			}finally {
				collectorDBI = null;
			}
			
			return jaSLACounters;
		}*/
		
		/**
		 * Queue the given bean CollectorBean into the asked Family queue.
		 * 
		 * @param strCounterParams
		 * @return
		 * @throws Exception 
		 */
		public void collectSlaBreachCountersBean(String strBreachCounterSet) throws Exception {
			SlaCollectorBean slaBean = null;
			
			try {
				
				if(strBreachCounterSet!=null) {
					slaBean = new SlaCollectorBean();
					JSONArray  jaBreachCounters= JSONArray.fromObject(strBreachCounterSet);
					SlaCollectorBean.setJoCountersBean(jaBreachCounters);
					slaBean.setDateQueuedOn(new Date());
					//add to Queue
					pqModulePerformanceSla.add(slaBean);
				}
			} catch(Exception e) {
				LogManager.errorLog(e);
				throw e;
			}
			
		}
		
		public HashMap<String, Object> getActiveSLAsCounters(Connection con) throws Exception {
			CollectorDBI collectorDBI = null;
			
			HashMap<String, Object> hmGUIDsMappedToSLAs = null;
			
			try{
				collectorDBI = new CollectorDBI();
				
				// gets GUID(s) active SLA(s)
				hmGUIDsMappedToSLAs = collectorDBI.getActiveLogSlaConfig(con);
				
				/*if (hmGUIDsMappedToSLAs != null) {
					// HashMap to JSON format
					strRtnGUIDsSLACounters = UtilsFactory.getJSONSuccessReturn(hmGUIDsMappedToSLAs).toString();
				} else {
					strRtnGUIDsSLACounters = UtilsFactory.getJSONFailureReturn("Problem with services").toString();
				}*/
				
			} catch(Exception e){
				throw e;
			} finally {
				
				collectorDBI = null;
			}
			
			return hmGUIDsMappedToSLAs;
		}
		
		/**
		 * Poll the top Counter CollectorBean from the asked Family queue.
		 * 
		 * @param agent_family
		 * @param nIndex
		 * @return
		 */
		public SlaCollectorBean pollSlaBreachCountersBean(){
			SlaCollectorBean cbRumEntry = null;
			
			try {
				cbRumEntry = pqModulePerformanceSla.poll();
			} catch(Throwable e) {
				LogManager.errorLog(e);
			}
			
			return cbRumEntry;
		}
		/**
		 * Get the size of the asked Family queue.
		 * 
		 * @return int
		 */
		public int getSlaBreachCountersQueueLength(){
			return pqModulePerformanceSla.size();
		}
		
		@Override
		/**
		 * Before destroying clear the objects. To prevent from MemoryLeak.
		 */
		protected void finalize() throws Throwable {
			UtilsFactory.clearCollectionHieracy(pqModulePerformanceCounters);
			
			DataBaseManager.close(conBackup);
			conBackup = null;
			
			super.finalize();
		}

}
