package com.appedo.lt.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import net.sf.json.JSONObject;

import com.appedo.lt.servlet.RUMCollectorInitServlet;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.AppedoConstants;
import com.appedo.manager.LogManager;

/**
 * This class holds the application level variables which required through the application.
 * 
 * @author navin
 *
 */
public class Constants {
	
	public static boolean DEV_ENVIRONMENT = false;
	
	public static String CONSTANTS_FILE_PATH = "";
	
	public static String RESOURCE_PATH = "";
	
	public static String APPEDO_CONFIG_FILE_PATH = "";
	public static String SMTP_MAIL_CONFIG_FILE_PATH = "";
	public static String DELAY_SAMPLING = "";
	
	public static String PATH = ""; 
	public static String VUSCRIPTSPATH = "";
	public static String JMETERVUSCRIPTSFOLDERPATH = "";
	public static String JMETERVUSCRIPTSPATH = "";
	public static String FLOODGATESVUSCRIPTSPATH = "";
	public static String FLOODGATESSCENARIOXMLPATH = "";
	public static String FLOODGATESSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLFOLDERPATH = "";
	public static String JMETERSCENARIOXMLPATH = "";
	public static String JMETERSCENARIOFOLDERPATH = "";
	public static String JMETERSCENARIOSPATH = "";
	public static String CSVFILE = "";
	public static String UPLOADPATH = "";
	public static String VARIABLEPATH = "";
	public static String SUMMARYREPORTPATH = "";
	public static String VARIABLEXMLPATH = "";
	public static String VARIABLEXMLFOLDERPATH = "";
	public static String JMETERTESTPATH = "";
	public static String JMETERTESTJTLPATH = "";
	public static String JMETERCSVPATH = "";
	public static String JMETERSUMMARYREPORTPATH="";
	public static String DOWNLOADS = "";
	
	public static String SELENIUM_SCRIPT_CLASS_FILE_PATH = "";
	public static String FROMEMAILADDRESS = "";

	public static String LT_QUEUE_IP = "";
	public static String FG_CONTROLLER_PORT = "";
	public static String LT_QUEUE_PORT = "";
	public static String FG_APPLICATION_IP = "";
	
	public static String JM_CONTROLLER_IP= "";
	public static String JM_CONTROLLER_PORT = "";
	
	public static String APPEDO_URL = "";
	public static String APPEDO_URL_FILE_TRANSFER = "";
	public static String LT_EXECUTION_SERVICES_FAILED = "";
	public static String WEBSERVICE_HOST = "";
	public static String FLOODGATESVUSCRIPTSFOLDERPATH = "";
	public static String FLOODGATESVUSCRIPTSTEMPFOLDERPATH = "";
	public static String APPEDO_SCHEDULER = "";
	public static String LICENSEPATH = "";
	public static String LICENSEXMLPATH = "";
	public static String APPEDO_HARFILES = "";
	public static String HAR_REPOSITORY_URL = "";
	public static String APPEDO_COLLECTOR = "";
	public static String MAX_COUNTERS = "";
	public static String MAX_LOADGEN_CREATION = "";
	public static String DEFAULT_LOADGEN = "";
	public static HashMap<String, String> AGENT_LATEST_BUILD_VERSION = new HashMap<String, String>();
	public static String FG_VUSCRIPTS_TEMP_PATH = "";
	//public static JSONObject LOAD_TEST_LICENSE_DETAILS = new JSONObject();
	
	
	// Limitation for Counter Charts time window
	public static int COUNTER_CHART_START_DELAY = 60;//62;
	public static int COUNTER_CHART_END_DELAY = 0;//2;
	public static int COUNTER_MINICHART_START_DELAY = 10;//12;
	public static int COUNTER_MINICHART_END_DELAY = 0;//2;
	
	// tried to append 0 for diff between the prev result time and current result time is > 3 min
	public static final int COUNTER_CHART_TIME_INTERVAL = 3;
	public static final int COUNTER_CHART_ADD_0_FOR_EVERY = 3;
	
	public static String CLASS_EXPORT_URL;
	
	public static String SELENIUM_SCRIPT_PACKAGES, DEFAULT_LT_THREADPOOL_NAME, MAX_THREADS_RETAINED;
	
	//log4j properties file path
	public static String LOG4J_PROPERTIES_FILE = "";
	
	public static boolean IS_CAPTCHA_VALIDATION_ENABLE = true;
	
	public static HashMap<String, String> MINICHART_COUNTERS = new HashMap<String, String>();
	
	public static AtomicLong count = new AtomicLong(1);
	
	public static int QUEUE_WAITING_LOOP_COUNT = 10;//2;

	public static String AUTO_REPORT_PREPARATION_STARTED = "AUTO REPORT PREPARATION STARTED";
	public static String AUTO_REPORT_GENERATION_STARTED = "AUTO REPORT GENERATION STARTED";
	public static String AUTO_REPORT_GENERATION_COMPLETED = "AUTO REPORT GENERATION COMPLETED";
	public static String AUTO_REPORT_GENERATION_FAILED = "AUTO REPORT GENERATION FAILED";
	public static String AUTO_REPORT_PROCESS_IN_QUEUE = "AUTO REPORT PROCESS IN QUEUE";
	
	// for LT run, older scripts settings, used the default parallel connections value
	public static String DEFAULT_PARALLEL_CONNECTIONS;
	
	public static JSONObject CI_COLLECTOR = null;
	public static String CI_COLLECTOR_URL = null;
	
	/*public enum AGENT_TYPE {
	TOMCAT_7X("TOMCAT 7.X", "tomcat_performance_counter"), JBOSS("JBOSS", "jboss_performance_counter"), MSIIS("MSIIS", "msiis_performance_counter"), 
	JAVA_PROFILER("JAVA_PROFILER", "tomcat_profiler"), 
	LINUX("LINUX", "linux_performance_counter"), MSWINDOWS("MSWINDOWS", "windows_performance_counter"), 
	MYSQL("MYSQL", "mysql_performance_counter"), MSSQL("MSSQL", "mssql_performance_counter");
	
	private String strAgentType, strTableName;
	
	private AGENT_TYPE(String agentType, String tableName) {
		strAgentType = agentType;
		strTableName = tableName;
	}
	
	public void setMySQLVersion(String strVersionNo) {
		strAgentType = "MYSQL "+strVersionNo;
	}
	
	public String getTableName() {
		return strTableName;
	}
	
	@Override
	public String toString() {
		return strAgentType;
	}
}*/
	
	public static long getLongCount(){
		return count.incrementAndGet();
	}
	
	public enum CHART_START_INTERVAL {
		_1("3 hours"), _2("24 hours"), _3("7 days"), _4("15 days"), _5("30 days");
		
		private String strInterval;
		
		private CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	/**
	 * for SUM chart results interval
	 * @author navin
	 *
	 */
	public enum SUM_CHART_START_INTERVAL {
		_1("24 hours"), _2("7 days"), _3("15 days"), _4("30 days"), _5("60 days"), _6("120 days"), _7("180 days");
		
		private String strInterval;
		
		private SUM_CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}
	
	/**
	 * for RUM charts interval
	 * @author navin
	 *
	 */
	/*public enum RUM_CHART_START_INTERVAL {
		_1("24 hours"), _2("7 days"), _3("15 days"), _4("30 days"), _5("60 days"), _6("120 days"), _7("180 days");
		
		private String strInterval;
		
		private RUM_CHART_START_INTERVAL(String strInterval) {
			this.strInterval = strInterval;
		}
		
		public String getInterval() {
			return strInterval;
		}
	}*/
	
	/**
	 * Loads constants properties 
	 * 
	 * @param srtConstantsPath
	 */
	public static void loadConstantsProperties(String srtConstantsPath) throws Exception {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(srtConstantsPath);
			prop.load(is);
			
			// Appedo application's resource directory path
			RESOURCE_PATH = prop.getProperty("RESOURCE_PATH");
			
			APPEDO_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("APPEDO_CONFIG_FILE_PATH");
			
			PATH = RESOURCE_PATH+prop.getProperty("Path");
			
			FLOODGATESVUSCRIPTSFOLDERPATH = prop.getProperty("floodgatesvuscriptsfolderpath");
			FLOODGATESVUSCRIPTSTEMPFOLDERPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptstempfolderpath");
			VUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("VUScriptspath");
			FLOODGATESVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("floodgatesvuscriptspath");
			JMETERVUSCRIPTSFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptsfolderpath");
			JMETERVUSCRIPTSPATH = RESOURCE_PATH+prop.getProperty("jmetervuscriptspath");
			FLOODGATESSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("floodgatesscenarioxmlpath");
			FLOODGATESSCENARIOXMLFOLDERPATH = prop.getProperty("floodgatesscenarioxmlfolderpath");
			JMETERSCENARIOXMLPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlpath");
			JMETERSCENARIOXMLFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenarioxmlfolderpath");
			JMETERSCENARIOFOLDERPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariofolderpath");
			CSVFILE = RESOURCE_PATH+prop.getProperty("Csvfile");
			UPLOADPATH = RESOURCE_PATH+prop.getProperty("Uploadpath");
			VARIABLEPATH = RESOURCE_PATH+prop.getProperty("Variablepath");
			SUMMARYREPORTPATH = RESOURCE_PATH+prop.getProperty("summaryreportpath");
			VARIABLEXMLPATH = RESOURCE_PATH+prop.getProperty("VariableXMLpath");
			VARIABLEXMLFOLDERPATH = prop.getProperty("VariableXMLFolderpath");
			JMETERSCENARIOSPATH = RESOURCE_PATH+prop.getProperty("jmeterscenariospath");
			JMETERTESTPATH = RESOURCE_PATH+prop.getProperty("jmetertestpath");
			JMETERTESTJTLPATH = RESOURCE_PATH+prop.getProperty("jmetertestjtlpath");
			DOWNLOADS = RESOURCE_PATH+prop.getProperty("downloads");
			JMETERCSVPATH = RESOURCE_PATH+prop.getProperty("jmetercsvpath");
			JMETERSUMMARYREPORTPATH = RESOURCE_PATH+prop.getProperty("jmetersummaryreportpath");
			LICENSEPATH = RESOURCE_PATH+prop.getProperty("licensepath");
			LICENSEXMLPATH = RESOURCE_PATH+prop.getProperty("licensexmlpath");
			MAX_COUNTERS = prop.getProperty("max_counters");
			DELAY_SAMPLING = prop.getProperty("DELAY_SAMPLING");
			MAX_LOADGEN_CREATION = prop.getProperty("MAX_LOADGEN_CREATION");
			DEFAULT_LOADGEN = prop.getProperty("DEFAULT_LOADGEN");
			FG_VUSCRIPTS_TEMP_PATH = RESOURCE_PATH+prop.getProperty("FG_VUSCRIPTS_TEMP_PATH");
			// Mail configuration 
			SMTP_MAIL_CONFIG_FILE_PATH = RESOURCE_PATH+prop.getProperty("SMTP_MAIL_CONFIG_FILE_PATH");
			FROMEMAILADDRESS = prop.getProperty("FromEmailAddress");
			
			Constants.SELENIUM_SCRIPT_CLASS_FILE_PATH = Constants.RESOURCE_PATH+prop.getProperty("sumtransactionfilepath");
			LOG4J_PROPERTIES_FILE = RESOURCE_PATH+prop.getProperty("LOG4J_CONFIG_FILE_PATH");
			SELENIUM_SCRIPT_PACKAGES = prop.getProperty("seleniumscriptpackages"); 
			DEFAULT_LT_THREADPOOL_NAME = prop.getProperty("DEFAULT_LT_THREADPOOL_NAME");
			MAX_THREADS_RETAINED = prop.getProperty("MAX_THREADS_RETAINED");
			
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Loads appedo config properties, from the system specifed path, 
	 * (Note.. loads other than db related)
	 * 
	 * @param strAppedoConfigPath
	 */
	public static void loadAppedoConfigProperties(String strAppedoConfigPath) throws Throwable {
		Properties prop = new Properties();
		InputStream is = null;
		
		try {
			is = new FileInputStream(strAppedoConfigPath);
			prop.load(is);
			
			if( prop.getProperty("ENVIRONMENT") != null && prop.getProperty("ENVIRONMENT").equals("DEVELOPMENT") 
					&& AppedoConstants.getAppedoConfigProperty("ENVIRONMENT") != null && AppedoConstants.getAppedoConfigProperty("ENVIRONMENT").equals("DEVELOPMENT") ) 
			{
				DEV_ENVIRONMENT = true;
			}
			
			// Load Test configuration & LoadGen Agent details
			LT_QUEUE_IP = getProperty("LT_QUEUE_IP",prop);
			FG_CONTROLLER_PORT = getProperty("FG_CONTROLLER_PORT",prop);
			LT_QUEUE_PORT = getProperty("LT_QUEUE_PORT",prop);
			FG_APPLICATION_IP = getProperty("FG_APPLICATION_IP",prop);

			JM_CONTROLLER_IP = getProperty("JM_CONTROLLER_IP",prop);
			JM_CONTROLLER_PORT = getProperty("JM_CONTROLLER_PORT",prop);
			
			// User's email address verification mail's URL link starter
			APPEDO_URL = getProperty("APPEDO_URL",prop);
			LT_EXECUTION_SERVICES_FAILED = getProperty("LT_EXECUTION_SERVICES_FAILED",prop);
			APPEDO_URL_FILE_TRANSFER = getProperty("APPEDO_URL_FILE_TRANSFER",prop);
			
			// Url to delete har files from har repository
			HAR_REPOSITORY_URL = getProperty("HAR_REPOSITORY_URL",prop);	
			
			// Webservice collector
			WEBSERVICE_HOST = getProperty("WEBSERVICE_HOST",prop);
			APPEDO_SCHEDULER = getProperty("APPEDO_SCHEDULER",prop);
			APPEDO_HARFILES = getProperty("APPEDO_HARFILES",prop);
			CLASS_EXPORT_URL = getProperty("URL_TO_EXPORT_CLASS_FILE",prop);
			APPEDO_COLLECTOR = getProperty("APPEDO_COLLECTOR",prop);
			
			IS_CAPTCHA_VALIDATION_ENABLE = Boolean.parseBoolean( UtilsFactory.replaceNull(getProperty("IS_CAPTCHA_VALIDATION_ENABLE",prop), "true") );
			
			// default parallel connections, for RUN settings
			DEFAULT_PARALLEL_CONNECTIONS = getProperty("DEFAULT_PARALLEL_CONNECTIONS",prop);
			
			if (getProperty("CI_COLLECTOR",prop) != null) {
				CI_COLLECTOR = JSONObject.fromObject(getProperty("CI_COLLECTOR",prop));
				CI_COLLECTOR_URL = getAsURL(CI_COLLECTOR);
			}
			
			
		} catch(Throwable th) {
			throw th;
		} finally {
			UtilsFactory.close(is);
			is = null;
		}
	}
	
	/**
	 * Get the property's value from appedo_config.properties, if it is DEV environment;
	 * Otherwise get the property's value from DB.
	 * 
	 * @param strPropertyName
	 * @param prop
	 * @return
	 */
	private static String getProperty(String strPropertyName, Properties prop) throws Throwable {
		if( DEV_ENVIRONMENT && prop.getProperty(strPropertyName) != null )
			return prop.getProperty(strPropertyName);
		else
			return AppedoConstants.getAppedoConfigProperty(strPropertyName);

	}
	
	/**
	 * Collector in JSON format return as URL
	 * 
	 * @param joAppedoCollector
	 * @return
	 */
	public static String getAsURL(JSONObject joAppedoCollector) {
		return joAppedoCollector.getString("protocol") +"://"+ joAppedoCollector.getString("server") +":"+ joAppedoCollector.getString("port") +"/"+ joAppedoCollector.getString("application_name") +"/";
	}
	
	/**
	 * CI JS `application_ci.js`, replaces appedo whitelabels from file and saves in another file `<download_appln_name_lowercase>_ci.js`,
	 * `download_appln_name_lowercase` is from `appedo_whitelabel`
	 * 
	 * @throws Exception
	 */
	public static void replaceCIJSAppedoWhiteLabels() throws Throwable {
		HashMap<String, String> hmLabelReplacement = null;
		
		String strSourcePath = "", strTargetPath = "";
		
		File fileSource = null;
		
		boolean bFileSourceDeleted = false;
		
		try {
			strSourcePath = RUMCollectorInitServlet.realPath+"/js/application_ci.js";
			strTargetPath = RUMCollectorInitServlet.realPath+"/js/"+AppedoConstants.getAppedoWhiteLabel("download_appln_name_lowercase")+"_ci.js";
			
			fileSource = new File(strSourcePath);
			
			// 
			if ( ! fileSource.exists() ) {
				LogManager.infoLog("File not exists for whitelabel replacement: "+strSourcePath);
				//System.out.println("File not exists for whitelabel replacement: "+strSourcePath);
			} else {
				hmLabelReplacement = new HashMap<String, String>();
				hmLabelReplacement.put("CI_COLLECTOR_URL", Constants.CI_COLLECTOR_URL);
				
				// replaces whitelabels in file & saves in another file, `<download_appln_name_lowercase>_ci.js`
				AppedoConstants.applyAppedoWhiteLabelsInFile(strSourcePath, strTargetPath, hmLabelReplacement);
				
				// deletes `application_ci.js`
				bFileSourceDeleted = fileSource.delete();
				
				LogManager.infoLog("Replaces whitelabels & file saved: "+strTargetPath);
				//System.out.println("Replaces whitelabels & file saved: "+strTargetPath);
				
				LogManager.infoLog("Deleted "+strSourcePath+": "+bFileSourceDeleted);
				//System.out.println("Deleted "+strSourcePath+": "+bFileSourceDeleted);
			}
			
		} catch (Throwable th) {
			throw th;
		} finally {
			strSourcePath = null;
			strTargetPath = null;
			
			fileSource = null;
		}
	}
	
	/**
	 * loads AppedoConstants, 
	 * of loads Appedo whitelabels, replacement of word `Appedo` as configured in DB
	 * 
	 * @param strAppedoConfigPath
	 * @throws Exception
	 */
	public static void loadAppedoConstants(Connection con) throws Throwable {
		
		try {
			// 
			AppedoConstants.getAppedoConstants().loadAppedoConstants(con);
		} catch (Throwable th) {
			throw th;
		}
	}
}
