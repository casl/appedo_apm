package com.appedo.lt.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.manager.LogManager;

/**
 * Servlet to handle one operation for the whole application
 * 
 */ 
public class LOGCollectorInitServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static String realPath = null;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		Connection con = null;
		
		// declare Servlet context
		ServletContext context = getServletContext();
		realPath = context.getRealPath("//");
		
		try{
			String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
			String strLog4jFilePath = context.getInitParameter("LOG4J_PROPERTIES_FILE_PATH");
			
			Constants.CONSTANTS_FILE_PATH = LOGCollectorInitServlet.realPath + strConstantsFilePath;
			Constants.LOG4J_PROPERTIES_FILE = LOGCollectorInitServlet.realPath + strLog4jFilePath;
			
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator(Constants.LOG4J_PROPERTIES_FILE);
			
			// Loads Constant properties
			Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LOG-Collector", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads Appedo constants; say loads appedoWhiteLabels, 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
		} catch(Throwable th) {
			System.out.println("Exception in Init.load: "+th.getMessage());
			th.printStackTrace();
		} finally {
			DataBaseManager.close(con);
			con = null;
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}
	
	/**
	 * Accessed in both GET and POSTrequests for the operations below, 
	 * 1. Loads agents latest build version
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		Connection con = null;
		
		response.setContentType("text/html");
		String strActionCommand = request.getRequestURI();
		
		if(strActionCommand.endsWith("/init/reloadConfigProperties")) {
			// to reload config and appedo_config properties 
			
			try {
				// Loads Constant properties
				Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
				
				con = DataBaseManager.giveConnection();
				
				// loads Appedo constants; say loads appedoWhiteLabels, 
				Constants.loadAppedoConstants(con);
				
				// Loads Appedo config properties from the system path
				Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
				
				response.getWriter().write("Loaded <B>Appedo-LOG-Collector</B>, config, appedo_config & appedo whitelabels.");
			} catch (Throwable th) {
				LogManager.errorLog(th);
				response.getWriter().write("<B style=\"color: red; \">Exception occurred Appedo-LOG-Collector: "+th.getMessage()+"</B>");
			} finally {
				DataBaseManager.close(con);
				con = null;
			}
		}
	}
}
