package com.appedo.lt.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.tcpserver.LoadGenServerQueue;
import com.appedo.manager.LogManager;

/**
 * Servlet to handle one operation for the whole application
 * 
 * @author navin
 * 
 */
public class LTQueueServiceInitServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static String realPath = null;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public void init() {
		// declare Servlet context
		ServletContext context = getServletContext();
		realPath = context.getRealPath("//");
		
		Connection con = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		
		try{
			String strConstantsFilePath = context.getInitParameter("CONSTANTS_PROPERTIES_FILE_PATH");
			String strLog4jFilePath = context.getInitParameter("LOG4J_PROPERTIES_FILE_PATH");
			
			Constants.CONSTANTS_FILE_PATH = LTQueueServiceInitServlet.realPath + strConstantsFilePath;
			Constants.LOG4J_PROPERTIES_FILE = LTQueueServiceInitServlet.realPath + strLog4jFilePath;
			
			// Loads log4j configuration properties
			LogManager.initializePropertyConfigurator(Constants.LOG4J_PROPERTIES_FILE);
			
			// Loads Constant properties
			Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
			
			// Loads db config
			DataBaseManager.doConnectionSetupIfRequired("Appedo-LT-Queue-Services", Constants.APPEDO_CONFIG_FILE_PATH, true);
			
			con = DataBaseManager.giveConnection();
			
			// loads appedo constants; say loads appedoWhiteLabels, 
			Constants.loadAppedoConstants(con);
			
			// Loads Appedo config properties from the system path
			Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
			
		} catch(Throwable th) {
			System.out.println("Exception in Init.load: "+th.getMessage());
			th.printStackTrace();
		}
		
		// Reset LT Module's private counters
		try{
			System.out.println("Reset Appedo-Private-Variables...");
			
			con = DataBaseManager.reEstablishConnection(con);
			
			String query = "update appedo_pvt_counters set pvt_counter_value = 0 WHERE pvt_counter_name='lt_processing_q'";
			pstmt = con.prepareStatement(query);
			pstmt.executeUpdate();
			
			System.out.println("Updated private counters.");
			
			query = "update tblreportmaster set queue_length = 0 where is_active = false";
			pstmt1 = con.prepareStatement(query);
			pstmt1.executeUpdate();
			
			System.out.println("Queue length is reseted.");
		} catch(Throwable th) {
			System.out.println("Exception in init.reset: "+th.getMessage());
			LogManager.errorLog(th);
		} finally {
			DataBaseManager.close(pstmt);
			
			DataBaseManager.close(pstmt1);
			
			DataBaseManager.close(con);
		}
		
		// Open the TCP communication port to receive data from Appedo-LT-Controller
		Thread t1;
		try {
			System.out.println("Starting LoadGenServerQueue Thread.");
			t1 = new LoadGenServerQueue();
			t1.start();
		} catch (Throwable e) {
			System.out.println("Exception in init.start: "+e.getMessage());
			LogManager.errorLog(e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}
	
	/**
	 * Accessed in both GET and POSTrequests for the operations below, 
	 * 1. Loads agents latest build version
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		Connection con = null;
		
		response.setContentType("text/html");
		String strActionCommand = request.getRequestURI();
		
		if(strActionCommand.endsWith("/init/reloadConfigProperties")) {
			// to reload config and appedo_config properties 
			
			try {
				// Loads Constant properties
				Constants.loadConstantsProperties(Constants.CONSTANTS_FILE_PATH);
				
				con = DataBaseManager.giveConnection();
				
				// loads Appedo constants; say loads appedoWhiteLabels, 
				Constants.loadAppedoConstants(con);
				
				// Loads Appedo config properties from the system path
				Constants.loadAppedoConfigProperties(Constants.APPEDO_CONFIG_FILE_PATH);
				
				response.getWriter().write("Loaded <B>Appedo-LT-Queue-Service</B>, config, appedo_config & appedo whitelabels.");
			} catch (Throwable th) {
				LogManager.errorLog(th);
				response.getWriter().write("<B style=\"color: red; \">Exception occurred Appedo-LT-Queue-Service: "+th.getMessage()+"</B>");
			} finally {
				DataBaseManager.close(con);
				con = null;
			}
		}
	}
}
