package com.appedo.lt.model;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.dbi.RunDBI;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;


/**
 * This is to manage the run operations
 * @author Veeru
 *
 */
public class RunManager {
	
	public Document getVariables(long lUserId) throws Throwable {
		
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docVariableXml = null;
		try {
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			String xmlString = readFile(Constants.VARIABLEXMLFOLDERPATH+File.separator+lUserId+"_variables.xml");
			docVariableXml = db.parse( new InputSource( new StringReader( xmlString ) ) );
			
		}catch(Throwable th) {
			LogManager.errorLog(th);
		}finally {
			dbf = null;
			db = null;
		}
		return docVariableXml;
	} 
	
	public static String readFile(String filePath) throws Throwable {
		
		HttpClient client = null;
		PostMethod method = null;
		
		String responseStream = null, responseRtn = "";
		
		try{
			client = new HttpClient();
			method = new PostMethod(Constants.APPEDO_URL_FILE_TRANSFER+"readFile");
			method.addParameter("file_path", filePath);
			//System.out.println("req file_path: "+filePath+" by "+Constants.APPEDO_URL_FILE_TRANSFER+"readFile");
			method.setRequestHeader("Connection", "close");
			int statusCode = client.executeMethod(method);
			LogManager.infoLog("statusCode: "+Constants.APPEDO_URL_FILE_TRANSFER+"readFile <> "+filePath+" < >"+statusCode);
			
			if (statusCode != HttpStatus.SC_OK) {
				LogManager.errorLog("Method failed: " + method.getStatusLine());
			}
			
			responseStream = method.getResponseBodyAsString();
			
			if( responseStream.startsWith("{") && responseStream.endsWith("}") ) {
				JSONObject joResponse = JSONObject.fromObject(responseStream);
				
				if( joResponse.getBoolean("success") ) {
					if( joResponse.containsKey("message") ) {
						responseRtn = joResponse.get("message").toString();
					} else {
						throw new Exception("1");	// TODO Inform that Service has problem
					}
				} else {
					if( joResponse.containsKey("errorMessage") ) {
						throw new Exception( joResponse.getString("errorMessage") );
					}
				}
			}
		} catch(Throwable e) {
			throw e;
		} finally {
			method.releaseConnection();
			method = null;
		}
		
		return responseRtn;
	}
	
	public  Document getScenarios(long lUserId, String strScenarioId) throws Throwable {
		
		String strExpression = null;
		String strScenarioXmlDocPath = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docScenario = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docScenarioXml = null;
		Element rootElement = null;
		XPath xPath =  null;
		NodeList nodeList = null;
		XPath xPath1 =  null;
		String expression1 = null;
		Node aNode = null;
		NamedNodeMap attributes = null;
		Node theAttribute = null;
		Node vNode = null;
		Node innode = null;
		Node importedNode = null;
		
		try {

			// to keep the scenario's xml file path 
			strScenarioXmlDocPath = readFile(Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml");
			
			//return document for sending
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docScenario = builder.newDocument();
			
			
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();

			docScenarioXml = db.parse( new InputSource( new StringReader( strScenarioXmlDocPath ) ) );
			// strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			// get the list of scripts under given scenario
			strExpression = "//root/scenarios/scenario[@id='"+strScenarioId+"']/script";
			
			//appending the root element to final return document
			rootElement = docScenario.createElement("root");
			docScenario.appendChild(rootElement);
			
			xPath =  XPathFactory.newInstance().newXPath();
			nodeList = (NodeList) xPath.compile(strExpression).evaluate(docScenarioXml, XPathConstants.NODESET);
				
				//System.out.println("fdF"+nodeList.getLength());
				for (int i = 0; i < nodeList.getLength(); i++) {
					
					aNode = (Node) nodeList.item(i);
					attributes = aNode.getAttributes();
					for (int a = 0; a < attributes.getLength(); a++) {
						
						theAttribute = attributes.item(a);
						//System.out.println(theAttribute.getNodeName()+"---"+theAttribute.getNodeValue());
						if(theAttribute.getNodeName().equals("id") && theAttribute.getNodeValue()!=null ) {
							
							//import the vscript from the source file
							vNode = getnodefromVuscripts(Long.parseLong(theAttribute.getNodeValue()),lUserId);
							if(vNode!=null) {
								Node en=vNode.cloneNode(true);
								docScenarioXml.adoptNode(en);
								aNode.appendChild(en);
							}
						}
					}
				}
				
				//make new xpath to get the selected node from the new xml document
				xPath1 =  XPathFactory.newInstance().newXPath();
				expression1 = "/root/scenarios/scenario[@id='"+strScenarioId+"']";
				
				// get only user selected scenario
				innode = (Node) xPath1.compile(expression1).evaluate(docScenarioXml, XPathConstants.NODE);
				importedNode = docScenario.importNode(innode, true);			
				rootElement.appendChild(importedNode);
		} catch(Throwable th) {
			LogManager.errorLog(th);
		} finally {
			strExpression = null;
			strScenarioXmlDocPath = null;
			factory = null;
			builder = null;
			dbf = null;
			db = null;
			docScenarioXml = null;
			rootElement = null;
			xPath =  null;
			nodeList = null;
			xPath1 =  null;
			expression1 = null;
			aNode = null;
			attributes = null;
			theAttribute = null;
			vNode = null;
			innode = null;
			importedNode = null;
		}		
		return docScenario;
	}
	
	public static Node getnodefromVuscripts(long scriptid, long lUserId) throws Throwable {
		
		// Members of the function
		Node Node = null;
		String nv="";
		int mnum = 0;
		
		DocumentBuilderFactory mdbf = DocumentBuilderFactory.newInstance();
		mdbf.setValidating(false);
		DocumentBuilder mdb = mdbf.newDocumentBuilder();
		 
		Document mdoc = mdb.parse(new InputSource( new StringReader( readFile(Constants.FLOODGATESVUSCRIPTSFOLDERPATH+File.separator+lUserId+"_vuscripts.xml") ) ) );
		
		try {
			
			NodeList mentries = mdoc.getElementsByTagName("vuscript");
			mnum = mentries.getLength();
			
			for (int i=0; i<mnum; i++) {
				
				Node mnode = (Node) mentries.item(i);				
				NamedNodeMap mattributes = mnode.getAttributes();
				for (int a = 0; a < mattributes.getLength(); a++) {
					
					Node themAttribute = mattributes.item(a);
					if(themAttribute.getNodeName().equals("id")) {
						nv=themAttribute.getNodeValue();
						// return node
						if(Integer.parseInt(nv)==scriptid)
							return mnode;
					}
				}
			}
		}
		catch(Throwable t) {
			LogManager.errorLog(t);
			throw t;
		}
		
		return Node;
	}
	
	public Document mergeDoc(Document docVariables, Document docScenarios) throws Throwable {
		
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docVarScenarios = null;
		Element rootElement = null;
		String strSceExpression = null;
		String strVarExpression = null;
		XPath xPathSce = null;
		XPath xPathVAr = null;
		Node nodeScenarios = null;
		Node nodeVariables = null;
		Node nodeCloneScen = null;
		Node nodeCloneVar = null;
		try {
			
			// Make a document object to merge & return
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docVarScenarios = builder.newDocument();
			
			rootElement = docVarScenarios.createElement("root");
			docVarScenarios.appendChild(rootElement);
			
			//expression to get the scenarios
			strSceExpression = "/root/scenario";
			//expression to get the variables
			strVarExpression = "/variables";
			
			xPathSce =  XPathFactory.newInstance().newXPath();
			xPathVAr =  XPathFactory.newInstance().newXPath();
			nodeScenarios = (Node) xPathSce.compile(strSceExpression).evaluate(docScenarios, XPathConstants.NODE);
			nodeVariables = (Node) xPathVAr.compile(strVarExpression).evaluate(docVariables, XPathConstants.NODE);
			
			//System.out.println("inside ++++" +nodeScenarios.getTextContent());
			nodeCloneScen = nodeScenarios.cloneNode(true);
			docVarScenarios.adoptNode(nodeCloneScen);
			rootElement.appendChild(nodeCloneScen);
			
			nodeCloneVar = nodeVariables.cloneNode(true);
			docVarScenarios.adoptNode(nodeCloneVar);
			rootElement.appendChild(nodeCloneVar);
			
		}catch (Throwable th) {
			LogManager.errorLog(th);
			
		}finally {
			factory = null;
			builder = null;
			rootElement = null;
			strSceExpression = null;
			strVarExpression = null;
			xPathSce = null;
			xPathVAr = null;
			nodeScenarios = null;
			nodeVariables = null;
			nodeCloneScen = null;
			nodeCloneVar = null;
		}
		return docVarScenarios;
	}
	
	public String convertDocumentToString(Document doc) throws Throwable {
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = null;
		StringWriter writer = null;
		String output="";
	    try {
	        transformer = tf.newTransformer();
	        // below code to remove XML declaration
	        writer = new StringWriter();
	        transformer.transform(new DOMSource(doc), new StreamResult(writer));
	        output = writer.getBuffer().toString();
	        
	    } catch(Throwable t) {
	    	LogManager.errorLog(t);
			throw t;
		}finally {
			transformer = null;
			UtilsFactory.close( writer );
		}
	    return output;
	}
	
	public HashMap<Long, String> getScriptId(long lUserId, String strScenarioId) throws Throwable {
		
		String strExpression = null;
		String strScenarioXmlDocPath = null;
		DocumentBuilderFactory factory = null;
		DocumentBuilder builder = null;
		Document docScenario = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document docScenarioXml = null;
		Element rootElement = null;
		XPath xPath =  null;
		NodeList nodeList = null;
		Node aNode = null;
		NamedNodeMap attributes = null;
		Node theAttribute = null;
		String scriptName = null;
		long scriptId = 0;
		HashMap<Long, String> hmap = new HashMap<Long, String>();
		
		try {

			// to keep the scenario's xml file path 
			strScenarioXmlDocPath = readFile(Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml");
			
			//return document for sending
			factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			docScenario = builder.newDocument();
			
			//Source document to get the script id's for selected scenario
			dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			db = dbf.newDocumentBuilder();
			
			docScenarioXml = db.parse( new InputSource( new StringReader( strScenarioXmlDocPath ) ) );
			// strScenarioXmlDocPath = Constants.FLOODGATESSCENARIOXMLFOLDERPATH+File.separator+lUserId+"_scenarios.xml";
			
			// get the list of scripts under given scenario
			strExpression = "//root/scenarios/scenario[@id='"+strScenarioId+"']/script";
			
			//appending the root element to final return document
			rootElement = docScenario.createElement("root");
			docScenario.appendChild(rootElement);
			
			xPath =  XPathFactory.newInstance().newXPath();
			nodeList = (NodeList) xPath.compile(strExpression).evaluate(docScenarioXml, XPathConstants.NODESET);
				
				for (int i = 0; i < nodeList.getLength(); i++) {
					aNode = (Node) nodeList.item(i);
					attributes = aNode.getAttributes();
					for (int a = 0; a < attributes.getLength(); a++) {
						theAttribute = attributes.item(a);
						if( theAttribute.getNodeName().equals("id") ){
							scriptId = Long.valueOf(theAttribute.getNodeValue());
						}
						if( theAttribute.getNodeName().equals("name") ){
							scriptName = theAttribute.getNodeValue();
						}
					}
					
					if( scriptName != null && scriptId != 0){
						hmap.put(scriptId, scriptName);
						scriptName = null;
						scriptId = 0;
					}
				}
		}catch(Throwable th) {
			LogManager.errorLog(th);
			
		}finally {
			strExpression = null;
			strScenarioXmlDocPath = null;
			factory = null;
			builder = null;
			dbf = null;
			db = null;
			docScenarioXml = null;
			rootElement = null;
			xPath =  null;
			nodeList = null;
			aNode = null;
			attributes = null;
			theAttribute = null;
			scriptName = null;
		}		
		return hmap;
	}
	
	public void updateInActiveLoadAgent(Connection con, long lRunId) throws Throwable{
		
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			runDBI.updateInActiveLoadAgent(con, lRunId);
//			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long getRunQueueLength(Connection con, long runId) throws Throwable {
		RunDBI runDBI = null;
		long queueLength = 0;
		try {
			runDBI = new RunDBI();
			queueLength = runDBI.getRunQueueLength(con, runId);
			runDBI = null;
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
		return queueLength;
	}
	
	public void updateReportMaster(Connection con, long lRunId, String status ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			
			sbQuery	.append("update tblreportmaster SET is_active=false, runendtime = now(), status=? ")
					.append("WHERE runid = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, status);
			pstmt.setLong(2, lRunId);
			
			pstmt.executeUpdate();
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
	
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
		}
	}
	
	public Boolean autoReportPreparation(Connection con, long lRunId)throws Throwable {
		RunDBI runDBI = null;
		Boolean status = false;
		try {
			runDBI = new RunDBI();
			if(runDBI.autoSummaryReportPreparationScriptwiseContainerwiseResponse(con, lRunId)){
				if(runDBI.autoSummaryReportPreparationTransactionError(con, lRunId)){
					if(runDBI.autoSummaryReportPreparationRequestwise(con, lRunId)){
						status = true;
						updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_STARTED );
					}else{
						status = false;
						updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
					}
				}else{
					status = false;
					updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
				}
			}else{
				status = false;
				updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		}
		return status;
	}
	
	public void autoReportGeneration(Connection con, long lRunId)throws Throwable {
		RunDBI runDBI = null;
		try {
			runDBI = new RunDBI();
			if(runDBI.autoSummaryReportGenerationScriptwiseContainerwiseResponse(con, lRunId)){
				if(runDBI.autoSummaryReportGenerationTransactionError(con, lRunId)){
					if(runDBI.autoSummaryReportGenerationRequestwise(con, lRunId)){
						updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_COMPLETED );
					}else{
						updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
					}
				}else{
					updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
				}
			}else{
				updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		}
	}
}
