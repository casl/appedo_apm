
package com.appedo.lt.tcpserver;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONObject;

import org.w3c.dom.Document;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.common.Constants;
import com.appedo.lt.dbi.QueueDBI;
import com.appedo.lt.dbi.RunDBI;
import com.appedo.lt.model.RunManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class LoadGenQueueServerThread extends Thread {
	QueueDBI queueDBI = null;
	Socket socketCon = null;
	
	Connection con = null;
	RunManager rm = null;
	RunDBI runDBI = null;
	
	LoadGenQueueServerThread(Socket sCon) throws Throwable {
		try {
			this.socketCon = sCon;
			queueDBI = new QueueDBI();
			start();
		}catch(Throwable th) {
			LogManager.errorLog(th);
		}
	}
	
	public void run() {
		try {
			acceptData();
		} catch (Throwable e) {
			LogManager.errorLog(e);
		}
	}
	
	public synchronized void acceptData() throws Throwable {
		String operation = "", strReceivedStreamContent = "", header = null;
		String[] tokens = null;
		int streamLength = 0;
		
		InputStream isLTControllerTCP = null;
		OutputStream osLTControllerTCP = null;
		
		try {
			/** print the hostname and port number of the connection */
			/**Instantiate an InputStream with the optional character encoding. */
			isLTControllerTCP = socketCon.getInputStream();
			
			/**Read the socket's InputStream and append to a StringBuffer */		      
			osLTControllerTCP = socketCon.getOutputStream();
			
			header = queueDBI.readHeader(isLTControllerTCP);	
			tokens = header.trim().split(": ");
			operation = tokens[0].toString();
			streamLength = Integer.parseInt(tokens[1].toString().split("\r\n")[0]);					
//			LogManager.infoLog("OPERATION ="+operation);
			
			if ( operation.equals("status") ) {
				LogManager.infoLog("Data Size: "+streamLength);
				if ( streamLength > 0 ) {
					int readCount = 0;
					byte[] bytes = new byte[streamLength];
					
					while (readCount < streamLength) {
						readCount+= isLTControllerTCP.read(bytes, readCount, streamLength-readCount);
					}
					strReceivedStreamContent = new String(bytes);
				}
				
				String strTokens[] = null;
				String strRespHeader = "", strRunId = null;
				byte[] b = null;
				
				try {
					strTokens = tokens[1].split("\r\n");
					strRunId = strTokens[1].toString().split("= ")[1].trim();
					System.out.println("strRunId, strReceivedStreamContent: "+strRunId+" <> "+strReceivedStreamContent.length());
					queueDBI.put(strRunId, strReceivedStreamContent);
					
					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					osLTControllerTCP.write(b, 0, b.length);
				} catch(Exception e) {
					LogManager.errorLog(e);
				} finally {
					strRespHeader = null;
					UtilsFactory.close(osLTControllerTCP);
				}
			} else if(operation.equalsIgnoreCase("test")) {
				String strresponse = null;
				byte[] b = null;
				try {
					strresponse="OK: 0\r\nurl= "+Constants.LT_EXECUTION_SERVICES_FAILED.trim()+"\r\n\r\n";
					b=strresponse.getBytes();
					osLTControllerTCP.write(b, 0, b.length);
				}catch(Exception e) {
					LogManager.errorLog(e);
				}finally {
					UtilsFactory.close(osLTControllerTCP);
				}
			} else if(operation.equalsIgnoreCase("scriptwisestatus")) {
				byte[] b = null;
				if(streamLength>0)	{
					int  readCount = 0;
					byte[] bytes = new byte[streamLength];

					while (readCount < streamLength) {
						readCount+= isLTControllerTCP.read(bytes, readCount, streamLength-readCount);
					}
					strReceivedStreamContent = new String(bytes);
				}

				String strCreatedUser = "";
				String strCompletedUser = "";
				String str200Count = "", str300Count = "", str400Count ="", str500Count = "", strIsCompleted = "";
				String strScriptId = "";
				String strScriptName = "";
				String strErrorCount = "";
				String strRespHeader = "";
				String strRunId = "";
				String[] splitLines = null;
				String[] splitLine = null;
				//long lRunId = 0L;
				HashMap<String, Object> hmStatuswiseDetails = null;
				String[] strTokens = null;
				int is_completed = 0;
				Long lRunId = 0L;
				
				try {
					con = DataBaseManager.reEstablishConnection(con);
					
					runDBI = new RunDBI();
					rm = new RunManager();
					strTokens = tokens[1].split("\r\n");
					strRunId = strTokens[1].toString().split("= ")[1].trim();
					is_completed = Integer.valueOf(strTokens[2].toString().split("= ")[1]);
					
					lRunId = Long.parseLong(strRunId);
					splitLines = strReceivedStreamContent.split("\r\n");
					for(String str : splitLines) {
						if(str.contains(",")) {
							hmStatuswiseDetails = new HashMap<String, Object>();
							hmStatuswiseDetails.put("runid", strRunId);
							splitLine = str.split(",");
							strScriptId = splitLine[0];
							hmStatuswiseDetails.put("script_id", strScriptId);
							strCreatedUser = splitLine[1] ;
							hmStatuswiseDetails.put("created_users", strCreatedUser);
							strCompletedUser = splitLine[2];
							hmStatuswiseDetails.put("completed_users", strCompletedUser);
							str200Count = splitLine[3];
							hmStatuswiseDetails.put("http_200_count", str200Count);
							str300Count = splitLine[4];
							hmStatuswiseDetails.put("http_300_count", str300Count);
							str400Count = splitLine[5];
							hmStatuswiseDetails.put("http_400_count", str400Count);
							str500Count = splitLine[6];
							hmStatuswiseDetails.put("http_500_count", str500Count);
							strIsCompleted =  splitLine[7];
							hmStatuswiseDetails.put("is_completed", strIsCompleted);
							strErrorCount = splitLine[8];
							hmStatuswiseDetails.put("error_count", strErrorCount);
							strScriptName = splitLine[9];
							hmStatuswiseDetails.put("script_name", strScriptName);
							if(runDBI.isRecordAvailable(con, strRunId, strScriptId)){
								runDBI.updateScriptwiseStatus(con,hmStatuswiseDetails);
							}else{
								runDBI.insertScriptwiseStatus(con,hmStatuswiseDetails);
							}
						}
					}
					
					strRespHeader = "ok: 0\r\n\r\n";
					b = strRespHeader.getBytes();
					osLTControllerTCP.write(b, 0, b.length);
					
					if( is_completed == 1 ){
						
						rm.updateInActiveLoadAgent(con, lRunId);
						int iWaitLoop=0;
						while(iWaitLoop<11){
							long queuelength = rm.getRunQueueLength(con, lRunId);
							if(queuelength == 0){
								iWaitLoop=0;
								// rm.createSummaryTable(con, lRunId);
								// Queue
								queueDBI.putRunID(lRunId+"");
								rm.updateReportMaster(con, lRunId, Constants.AUTO_REPORT_PROCESS_IN_QUEUE );
								break;
							}else{
								iWaitLoop++;
							}
							Thread.sleep(3000); // 11 Sec delay as we are polling every 10 secs.
						}
						
						LogManager.infoLog("RunId Queue Length Validation: "+iWaitLoop);
						if(iWaitLoop >= Constants.QUEUE_WAITING_LOOP_COUNT){ //10
							rm.updateReportMaster(con, lRunId, Constants.AUTO_REPORT_GENERATION_FAILED );
						}
						
					}
					
//					System.out.println("Received Script Wise Status: \n"+strReceivedStreamContent+"\n Run_Id : "+lRunId);						
				}catch(Exception e) {
					System.out.println("Exception in acceptData: "+e.getMessage());
				}finally {
					DataBaseManager.close(con);
					con = null;
					
					strRespHeader = null;
					strRunId = null;
					b = null;
					splitLines = null;
					splitLine = null;
					strCreatedUser = null;
					strCompletedUser = null;
					str200Count = null;
					str300Count = null;
					str400Count = null;
					str500Count = null;
					strIsCompleted = null;
					strScriptId = null;
					strScriptName = null;
					strErrorCount = null;
					UtilsFactory.clearCollectionHieracy(hmStatuswiseDetails);
					UtilsFactory.close(osLTControllerTCP);
				}
			} else if (operation.equalsIgnoreCase("isqueueavailable")){
				String strRespHeader = null;
				byte[] b = null;
				try {
					if(queueDBI.controllerQueueSize()==0) {
						strRespHeader = "queuestatus: 0\r\n"+"status= 0\r\n\r\n";
					}else {
						strRespHeader = "queuestatus: 0\r\n"+"status= 1\r\n\r\n";
					}
					
					b = strRespHeader.getBytes();
					osLTControllerTCP.write(b, 0, b.length);
				}catch(Exception e) {
					LogManager.errorLog(e);
				}finally {
					strRespHeader = null;
					b = null;
					UtilsFactory.close(osLTControllerTCP);
				}
			} else if (operation.equalsIgnoreCase("getrundetail")) {
				byte[] b = null;
				LoadTestSchedulerBean ltb = null;
				String process = null;
				String scenarioName = null;
				String reportName = null;
				String loadgen = null;
				String distributions = null;
				String txtbody = null;
				Document varDoc = null;
				Document scenarioDoc = null;
				Document mergeDoc = null;
				HashMap<Long, String> hmap = null;
				String scriptwiseDataConnection = null, scriptDataConnection = null; 
				long lRunId = 0;
				Pattern pattern;
				Matcher matcher;
				try {
					LogManager.infoLog("getrundetail::");
					con = DataBaseManager.reEstablishConnection(con);
					
					rm = new RunManager();
					runDBI = new RunDBI();
					ltb = new LoadTestSchedulerBean();
					// System.out.println("{\"TestType\":\"APPEDO_LT\",\"LoadgenIpAddress\":\"54.158.191.98\",\"ScenarioId\":\"123\",\"ScenarioName\":\"new scenarios\",\"ReportName\":\"dfdfewr we\",\"dateQueuedOn\":{\"date\":8,\"day\":5,\"hours\":16,\"minutes\":26,\"month\":0,\"seconds\":42,\"time\":1452250602246,\"timezoneOffset\":-330,\"year\":116},\"userId\":231,\"strLoadGenRegions\":\"Virginia\",\"strDistributions\":\"100\",\"maxUserCount\":15,\"strLicense\":\"level1\",\"lRunId\":876,\"userAgent\":\"Recorded Agent\"}");
					ltb.fromJSONObject(JSONObject.fromObject(queueDBI.pollControllerQueue()));
					// rm.updatePrivateCounters(con, "lt_controller_queue", LTScheduler.ltQueues.size());
					lRunId = ltb.getRunId();
					varDoc = rm.getVariables(ltb.getUserId());
					scenarioDoc = rm.getScenarios(ltb.getUserId(), ltb.getScenarioId());
					mergeDoc =rm.mergeDoc(varDoc, scenarioDoc);
					txtbody = rm.convertDocumentToString(mergeDoc);
					String regexString ="<header name=\"User-Agent\" value=\"(.*?)\"";// Pattern.quote("<header name=\"User-Agent\" value='") + "(.*?)" + Pattern.quote("'/>");
					pattern = Pattern.compile(regexString);
					matcher = pattern.matcher(txtbody);
					if(!ltb.getUserAgent().equalsIgnoreCase("Recorded Agent")){
						while (matcher.find()) {
							String textInBetween = matcher.group(1); // Since (.*?) is capturing group 1
							txtbody = txtbody.replace(textInBetween, ltb.getUserAgent());
						}
					}
					loadgen = "loadgens= "+ltb.getLoadgenIpAddress();
					reportName = "runid= "+lRunId;
					scenarioName = "scenarioname= "+ltb.getScenarioName();
					distributions = "distribution= "+ltb.getDistributions();
					scriptwiseDataConnection = "scriptwisedataconnection= "+Constants.LT_QUEUE_IP+","+Constants.LT_QUEUE_PORT;
					scriptDataConnection =  "scriptdataconnection= "+Constants.LT_QUEUE_IP+","+Constants.LT_QUEUE_PORT;
					LogManager.infoLog("scriptwiseDataConnection:: "+scriptwiseDataConnection);
					LogManager.infoLog("scriptDataConnection:: "+scriptDataConnection);
					runDBI.updateReportMasterLoadGen(con, lRunId, loadgen);
					// Store Script Id and Name to DB
					hmap = rm.getScriptId(ltb.getUserId(), ltb.getScenarioId());
					runDBI.insertRunScriptDetails(con, lRunId, hmap);
					
					// Storing Run Details 
					runDBI.insertRunDetails(con, lRunId, hmap, Long.valueOf(ltb.getScenarioId()), ltb.getScenarioName(), ltb.getUserAgent());
					process ="RUN: "+txtbody.getBytes().length+"\r\n"+reportName+"\r\n"+scenarioName+"\r\n"+loadgen+"\r\n"+distributions+"\r\n"+scriptwiseDataConnection+"\r\n"+scriptDataConnection+"\r\n\r\n"+txtbody;
					b = process.getBytes();
					osLTControllerTCP.write(b, 0, b.length);
					
				} catch(Exception e) {
					LogManager.errorLog(e);
				} finally {
					b = null;
					process = null;
					scenarioName = null;
					reportName = null;
					loadgen = null;
					txtbody = null;
					varDoc = null;
					scenarioDoc = null;
					mergeDoc = null;
					ltb = null;
					DataBaseManager.close(con);
					con = null;
					UtilsFactory.close(osLTControllerTCP);
				}
			}
		} catch(Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			UtilsFactory.close(isLTControllerTCP);
			
			UtilsFactory.close(osLTControllerTCP);
			
			socketCon.close();
		}
	}
}