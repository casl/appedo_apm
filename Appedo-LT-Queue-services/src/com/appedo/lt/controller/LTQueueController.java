package com.appedo.lt.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.dbi.QueueDBI;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

/**
 * to get the script details from scenarios xml which is located in the \
 * appedo repo
 * @author Anand
 *
 */
public class LTQueueController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	//static Properties prop = new Properties();
	/**
	 * do when post request comes
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	
	/**
	 * do when post request comes
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * To get the floodgates scripts from the scenario's xml
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		response.setContentType("text/html");
		
		String strActionCommand = request.getRequestURI();
		
		// TODO: Convert the if statements to switch and add pvt counters
		// TODO: Remove the LoadTestSchedulerBean, get queue name in parameter
		
		if( strActionCommand.endsWith("/ltQueue/getData") ){
				QueueDBI queueDBI = null;
				String strMessage = null;
				
				try	{
					queueDBI = new QueueDBI();
					strMessage = queueDBI.get();
				} catch(Throwable t)	{
					LogManager.errorLog(t);
					strMessage = "Unable to get the data from queue.";
				} finally {
					if(strMessage == null){
						strMessage = "";
					}
					response.getWriter().write(strMessage);
				}
		}else if(strActionCommand.endsWith("/ltQueue/getSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.size());
				
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/putRunID")){
			QueueDBI queueDBI = null;
			String strMessage = "true";
			try	{
				String runid = request.getParameter("runid");
				queueDBI = new QueueDBI();
				queueDBI.putRunID(runid);
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to put the runid in queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getRunID")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.getRunID();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the runid from queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getRunIDQueueSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.getRunIDQueueSize());
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/putTestInQueue")){
			QueueDBI queueDBI = null;
			String strMessage = "true";
			try	{
				LoadTestSchedulerBean ltSchedulerBean = new LoadTestSchedulerBean();
				ltSchedulerBean.fromJSONObject( JSONObject.fromObject(request.getParameter("testBean")));
				queueDBI = new QueueDBI();
				queueDBI.putTestInQueue(ltSchedulerBean);
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to put the bean in queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/getFreeQueueSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.freeQueueSize());
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/peekFreeQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.peekFreeQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to peek bean from queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getFreeQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.pollFreeQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getPaidQueueSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.paidQueueSize());
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/peekPaidQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.peekPaidQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getPaidQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.pollPaidQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}else if(strActionCommand.endsWith("/ltQueue/getJmeterQueueSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.jmeterQueueSize());
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/peekJmeterQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.peekJmeterQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/getJmeterQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.pollJmeterQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/getJMeterProcessQueueSize")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			
			try	{
				queueDBI = new QueueDBI();
				strMessage = Long.toString(queueDBI.getJMeterProcessQueueSize());
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to get the size of queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/peekJMeterProcessQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			JSONObject joRtn = new JSONObject();
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.peekJMeterProcessQueue();
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to peek the data from queue.";
			}finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/getJMeterProcessQueue")){
			QueueDBI queueDBI = null;
			String strMessage = null;
			JSONObject joRtn = new JSONObject();
			try	{
				queueDBI = new QueueDBI();
				strMessage = queueDBI.pollJMeterProcessQueue();
				if (strMessage == null) {
					joRtn = UtilsFactory.getJSONFailureReturn("Queue is empty.");
				} else {
					joRtn = UtilsFactory.getJSONSuccessReturn(strMessage);
				}
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				//strMessage = "Unable to get the data from queue.";
				joRtn = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
			}finally {
				
				response.getWriter().write(joRtn.toString());
			}
		} else if(strActionCommand.endsWith("/ltQueue/putInControllerQueue")){
			QueueDBI queueDBI = null;
			String strMessage = "true";
			try	{
				LoadTestSchedulerBean ltSchedulerBean = new LoadTestSchedulerBean();
				ltSchedulerBean.fromJSONObject( JSONObject.fromObject(request.getParameter("testBean")));
				queueDBI = new QueueDBI();
				queueDBI.putInControllerQueue(ltSchedulerBean);
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to put the bean in processing queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if(strActionCommand.endsWith("/ltQueue/putInJMeterProcessQueue")){
			QueueDBI queueDBI = null;
			String strMessage = "true";
			try	{
				LoadTestSchedulerBean ltSchedulerBean = new LoadTestSchedulerBean();
				ltSchedulerBean.fromJSONObject( JSONObject.fromObject(request.getParameter("testBean")));
				queueDBI = new QueueDBI();
				queueDBI.putInJMeterProcessQueue(ltSchedulerBean);
			}catch(Throwable t)	{
				LogManager.errorLog(t);
				strMessage = "Unable to put the bean in processing queue.";
			}finally {
				response.getWriter().write(strMessage);
			}
		} else if( strActionCommand.endsWith("/ltQueue/setupGrafanaForAppedoUser") ) {
			JSONObject hmUserDetails = new JSONObject();
			QueueDBI queueDBI = null;
			String strMessage = null;
			
			try	{
				hmUserDetails.put("user_id", request.getParameter("user_id"));
				hmUserDetails.put("email_id", request.getParameter("email_id"));
				hmUserDetails.put("full_name", request.getParameter("full_name"));
				System.out.println("got hmUserDetails: "+hmUserDetails);
				
				queueDBI = new QueueDBI();
				queueDBI.putInSetupGrafana(hmUserDetails);
				
				strMessage = UtilsFactory.getJSONSuccessReturn("").toString();
			} catch(Throwable t) {
				LogManager.errorLog(t);
				strMessage = "Unable to put the bean in setupGrafanaForAppedoUser queue: "+t.getMessage();
			} finally {
				response.getWriter().write(strMessage);
			}
		} else if( strActionCommand.endsWith("/ltQueue/pollSetupGrafanaQueue") ) {
			QueueDBI queueDBI = null;
			JSONObject joSetupGrafanaBean = null;
			String strMessage = null;
			
			try	{
				queueDBI = new QueueDBI();
				joSetupGrafanaBean = queueDBI.pollSetupGrafanaQueue();
				
				if( joSetupGrafanaBean != null ) {
					strMessage = joSetupGrafanaBean.toString();
				} else {
					strMessage = "";
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				strMessage = "Unable to get the pollSetupGrafanaQueue: "+t.getMessage();
			} finally {
				if(strMessage == null){
					strMessage = "";
				}
				response.getWriter().write(strMessage);
			}
		}
	}
}
