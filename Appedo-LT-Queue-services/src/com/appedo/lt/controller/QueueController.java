package com.appedo.lt.controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.appedo.collector.bean.CIDataBean;
import com.appedo.collector.bean.CollectorBean;
import com.appedo.collector.bean.LOGDataBean;
import com.appedo.collector.bean.RUMDataBean;
import com.appedo.collector.manager.CollectorManager;
import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.dbi.QueueDBI;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;
/**
 * To get the queue size & queue data from the  performance counters queue
 * 
 * @author : Veeru
 * @Date   : 01-07-2015
 *
 */
public class QueueController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	//static Properties prop = new Properties();
	/**
	 * do when post request comes
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	
	/**
	 * do when post request comes
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException,IOException{
		doAction(request, response);
	}
	
	/**
	 * To get the details & data about queue
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		response.setContentType("text/html");
		String strActionCommand = request.getRequestURI();
		//System.out.println("URL Name : "+strActionCommand);
		
		if(strActionCommand.endsWith("/ltQueue/getData")){
			QueueDBI queueDBI = null;
			String joMessage = null;
			try	{
				queueDBI = new QueueDBI();
				joMessage = queueDBI.get();
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = "Unable to get the data from queue.";
			} finally {
				if(joMessage == null){
					joMessage = "";
				}
				response.getWriter().write(joMessage);
			}
		}
		else if(strActionCommand.endsWith("/ltQueue/getSize")){
			QueueDBI queueDBI = null;
			String joMessage = null;
			try	{
				queueDBI = new QueueDBI();
				joMessage = Long.toString(queueDBI.size());
				
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = "Unable to get the size of queue.";
			} finally {
				response.getWriter().write(joMessage);
			}
		}
		else if(strActionCommand.endsWith("/ltQueue/putRunID")){
			QueueDBI queueDBI = null;
			String joMessage = "true";
			try	{
				String runid = request.getParameter("runid");
				queueDBI = new QueueDBI();
				queueDBI.putRunID(runid);
				
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = "Unable to put the runid in queue.";
			} finally {
				response.getWriter().write(joMessage);
			}
		}
		else if(strActionCommand.endsWith("/ltQueue/getRunID")){
			QueueDBI queueDBI = null;
			String joMessage = null;
			try	{
				queueDBI = new QueueDBI();
				joMessage = queueDBI.getRunID();
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joMessage = "Unable to get the runid from queue.";
			} finally {
				response.getWriter().write(joMessage);
			}
		}
		else if(strActionCommand.endsWith("/ltQueue/getRunIDQueueSize")){
			QueueDBI queueDBI = null;
			String joMessage = null;
			
			try	{
				queueDBI = new QueueDBI();
				joMessage = Long.toString(queueDBI.getRunIDQueueSize());
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joMessage = "Unable to get the size of queue.";
			} finally {
				response.getWriter().write(joMessage);
			}
		}
		
		else if( strActionCommand.endsWith("/asdQueue/getSize") ) {
			CollectorManager collectorManager = null;
			JSONObject joCounterLength = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				joCounterLength = UtilsFactory.getJSONSuccessReturn( Long.toString(collectorManager.getCounterLength()) );
				
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joCounterLength = UtilsFactory.getJSONFailureReturn("Unable to get the size of queue.");
			}
			
			response.getWriter().write(joCounterLength.toString());
		}
		else if( strActionCommand.endsWith("/asdQueue/getData") ) {
			CollectorManager collectorManager = null;
			JSONObject joCounterEntry = null;
			CollectorBean collBean = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				collBean = collectorManager.pollCounter();
				
				if( collBean != null ) {
					joCounterEntry = UtilsFactory.getJSONSuccessReturn(JSONArray.fromObject(collBean));
				} else {
					joCounterEntry = UtilsFactory.getJSONSuccessReturn("Unable to get the data from queue.");
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
			} finally {					
				collBean = null;
			}
			
			response.getWriter().write(joCounterEntry.toString());
		}
		
		else if( strActionCommand.endsWith("/rumQueue/getSize") ) {
			CollectorManager collectorManager = null;
			JSONObject joRumQueueLength = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				joRumQueueLength = UtilsFactory.getJSONSuccessReturn( Long.toString(collectorManager.getRumQueueLength()) );
				
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joRumQueueLength = UtilsFactory.getJSONFailureReturn("Unable to get the size of queue.");
			}
			
			response.getWriter().write(joRumQueueLength.toString());
		}
		else if( strActionCommand.endsWith("/rumQueue/getData") ) {
			CollectorManager collectorManager = null;
			JSONObject joCounterEntry = null;
			RUMDataBean rumBean = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				rumBean = collectorManager.pollRumBean();
				
				if(rumBean!=null) {
					joCounterEntry = UtilsFactory.getJSONSuccessReturn(JSONObject.fromObject(rumBean));
				}else {
					joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
			} finally {					
				rumBean = null;
			}
			
			response.getWriter().write(joCounterEntry.toString());
		}
		
		else if( strActionCommand.endsWith("/ciQueue/getSize") ) {
			CollectorManager collectorManager = null;
			JSONObject joRumQueueLength = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				joRumQueueLength = UtilsFactory.getJSONSuccessReturn( Long.toString(collectorManager.getCIQueueLength()) );
				
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joRumQueueLength = UtilsFactory.getJSONFailureReturn("Unable to get the size of queue.");
			}
			
			response.getWriter().write(joRumQueueLength.toString());
		}
		else if( strActionCommand.endsWith("/ciQueue/getData") ) {
			CollectorManager collectorManager = null;
			JSONObject joCounterEntry = null;
			CIDataBean ciBean = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				ciBean = collectorManager.pollCIBean();
				
				if(ciBean!=null) {
					joCounterEntry = UtilsFactory.getJSONSuccessReturn(JSONObject.fromObject(ciBean));
				}else {
					joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
			} finally {					
				ciBean = null;
			}
			
			response.getWriter().write(joCounterEntry.toString());
		}
		
		else if( strActionCommand.endsWith("/logQueue/getSize") ) {
			CollectorManager collectorManager = null;
			JSONObject joLogQueueLength = null;
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				joLogQueueLength = UtilsFactory.getJSONSuccessReturn( Long.toString(collectorManager.getLogQueueLength()) );
				
			} catch(Throwable t)	{
				LogManager.errorLog(t);
				joLogQueueLength = UtilsFactory.getJSONFailureReturn("Unable to get the size of queue.");
			}
			
			response.getWriter().write(joLogQueueLength.toString());
		} else if( strActionCommand.endsWith("/logQueue/getData") ) {
			CollectorManager collectorManager = null;
			JSONObject joCounterEntry = null;
			ArrayList<LOGDataBean> alLogEntry = new ArrayList<LOGDataBean>();
			
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				alLogEntry = collectorManager.pollLogBean();
				
				if(alLogEntry != null && alLogEntry.size() > 0) {
					joCounterEntry = UtilsFactory.getJSONSuccessReturn(JSONArray.fromObject(alLogEntry));
				}else {
					joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				joCounterEntry = UtilsFactory.getJSONFailureReturn("Unable to get the data from queue.");
			} finally {					
				alLogEntry = null;
			}
			
			response.getWriter().write(joCounterEntry.toString());
		} else if( strActionCommand.endsWith("/logQueue/getLogSLA") ) {
			CollectorManager collectorManager = null;
			Connection con = null;
			String rtnValue = "";
			HashMap<String, Object> hmGUIDsMappedToSLAs = null;
			try	{
				collectorManager = CollectorManager.getCollectorManager();
				
				con = DataBaseManager.giveConnection();
				
				hmGUIDsMappedToSLAs = collectorManager.getActiveSLAsCounters(con);
				
				if(hmGUIDsMappedToSLAs != null && hmGUIDsMappedToSLAs.size() > 0) {
					rtnValue = UtilsFactory.getJSONSuccessReturn(hmGUIDsMappedToSLAs).toString();
				} else {
					rtnValue = UtilsFactory.getJSONFailureReturn("NO Active SLA found").toString();
				}
			} catch(Throwable t) {
				LogManager.errorLog(t);
				rtnValue = UtilsFactory.getJSONFailureReturn("Unable to get the active SLA of LOG.").toString();
			} finally {					
				DataBaseManager.close(con);
				con = null;
			}
			
			response.getWriter().write(rtnValue);
		} 
	}
}
