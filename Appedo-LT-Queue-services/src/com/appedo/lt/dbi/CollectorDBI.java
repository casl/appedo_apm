package com.appedo.lt.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;



/**
 * Collection operation related Database Interface layer.
 * This will do the general operations related to the Application, Server or Database.
 * 
 * @author Ramkumar
 *
 */
public class CollectorDBI {
	
	public String[] strCounterTables = {"tomcat_performance_counter", "jboss_performance_counter", "msiis_performance_counter", "linux_performance_counter", "windows_performance_counter", "mysql_performance_counter", "mssql_performance_counter", "tomcat_profiler" };
	
	/**
	 * Returns the UID of the Module, which can be Application, Server, Database or other, in the encrypted format.
	 * 
	 * @param con
	 * @param strGUID
	 * @return
	 * @throws Exception
	 *
	public String getEncryptedModuleUID(Connection con, String strGUID) throws Exception {
		String strUID = "";
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			//String strQuery = "SELECT encrypted_application_id FROM applicationmaster WHERE guid = '"+strGUID+"'";
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"'";
			
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				//strUID = rst.getString("encrypted_application_id");
				strUID = rst.getString("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
		}
		
		return strUID;
	}
	
	/**
	 * Returns the UID of the Module, which can be Application, Server, Database or other, for the given encrypted UID.
	 * 
	 * @param con
	 * @param strEncryptedUID
	 * @return
	 * @throws Exception
	 */
	public long getModuleUID(Connection con, String strGUID) throws Exception {
		long lUID = -1l;
		
		String strQuery = null;
		Statement stmt = null;
		ResultSet rst = null;
		
		try{
			strQuery = "SELECT uid FROM module_master WHERE guid = '"+strGUID+"'";
			stmt = con.createStatement();
			rst = stmt.executeQuery(strQuery);
			
			while( rst.next() ){
				lUID = rst.getLong("uid");
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			strQuery = null;
		}
		
		return lUID;
	}
	
	/**
	 * Get the UID (primary key) for the given GUID.
	 * 
	 * @param con
	 * @param strGUID
	 * @return
	 * @throws Exception
	 */
	public long getApplicationUID(Connection con, String strGUID) throws Exception {
		Statement stmt = null;
		ResultSet rst = null;
		
		StringBuilder sbQuery = new StringBuilder();
		
		long lUID = -1l;
		
		try {
			sbQuery.append("SELECT application_id FROM applicationmaster WHERE guid = '").append(strGUID).append("' ");
			stmt = con.createStatement();
			rst = stmt.executeQuery(sbQuery.toString());
			
			while( rst.next() ){
				lUID = rst.getLong("application_id");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			LogManager.infoLog("sbQuery : " + sbQuery.toString());
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			
			DataBaseManager.close(stmt);
			stmt = null;
			
			UtilsFactory.clearCollectionHieracy(sbQuery);
			sbQuery = null;
		}
		
		return lUID;
	}
	
	
	/**
	 * To get sla mapped Counters
	 * @param con
	 * @param joLicense 
	 * @param strGUID
	 * @return 
	 */
	public JSONArray getSlaConfigCounters(Connection con, String  strGUID) { 
		StringBuilder sbQry = new StringBuilder();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		try {
			
			//select sla_id, created_by, counter_id, counter_template_id, is_above_threshold from so_sla_counter where sla_id=200;
			
			sbQry.append("select sla_id, created_by, counter_id, counter_template_id, is_above_threshold,threshold_value from so_sla_counter")			
			.append(" where guid = '").append(strGUID).append("'");
			
			pstmt = con.prepareStatement(sbQry.toString());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				
				joCounter = new JSONObject();
				joCounter.put("slaid", rs.getLong("sla_id"));
				joCounter.put("userid", rs.getLong("created_by"));
				joCounter.put("counterid", rs.getLong("counter_id"));
				joCounter.put("countertempid", rs.getLong("counter_template_id"));
				joCounter.put("isabovethreshold", rs.getBoolean("is_above_threshold"));
				joCounter.put("thresholdvalue", rs.getLong("threshold_value"));
				
				jaCounters.add(joCounter);
				
				joCounter = null;
			}
			
			
		}catch(Exception ex) {			
			LogManager.errorLog(ex);
			ex.printStackTrace();
		}finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(rs);
			rs = null;
			
			UtilsFactory.clearCollectionHieracy(sbQry);
		}
//		System.out.println("jaCounters:"+jaCounters.toString());
		return jaCounters;
	}
	
	/**
	 * To get sla mapped Counters
	 * @param con
	 * @param joLicense 
	 * @param strGUID
	 * @return 
	 */
	public HashMap<String, Object> getActiveLogSlaConfig(Connection con) { 
		StringBuilder sbQry = new StringBuilder();
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		String strGUID="";
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		HashMap<String, Object> hmGUIDsMappedToSLAs = null;
		ArrayList<JSONObject> alSLAs = null;
		try {
			
			//select sla_id, created_by, counter_id, counter_template_id, is_above_threshold from so_sla_counter where sla_id=200;
			
			sbQry.append("SELECT ssl.sla_id, ss.user_id, ssl.sla_log_id, ssl.guid, ssl.uid, ssl.log_table_name, ssl.log_grok_name, ssl.breach_pattern,")
					.append("ssl.grok_column, ssl.breached_severity, ssl.is_above_threshold, ")			
					.append(" ssl.warning_threshold_value, ssl.critical_threshold_value, ssl.min_breach_count FROM so_sla_log ssl ")
					.append(" INNER JOIN so_sla ss ON ss.sla_id = ssl.sla_id AND ss.is_active = TRUE ");
			
			pstmt = con.prepareStatement(sbQry.toString());
			rst = pstmt.executeQuery();
			hmGUIDsMappedToSLAs = new HashMap<String, Object>();
			while(rst.next()) {
				
				joCounter = new JSONObject();
				strGUID = rst.getString("guid");
				
				joCounter.put("sla_id", rst.getLong("sla_id"));
				joCounter.put("user_id", rst.getLong("user_id"));
				joCounter.put("sla_log_id", rst.getLong("sla_log_id"));
				joCounter.put("guid", strGUID);
				joCounter.put("uid", rst.getLong("uid"));
				joCounter.put("log_table_name", rst.getString("log_table_name"));
				joCounter.put("log_grok_name", rst.getString("log_grok_name"));
				joCounter.put("breach_pattern", rst.getString("breach_pattern"));
				joCounter.put("grok_column", rst.getString("grok_column"));
				joCounter.put("breached_severity", rst.getString("breached_severity"));
				joCounter.put("is_above_threshold", rst.getBoolean("is_above_threshold"));
				joCounter.put("warning_threshold_value", rst.getString("warning_threshold_value"));
				joCounter.put("critical_threshold_value", rst.getString("critical_threshold_value"));
				joCounter.put("min_breach_count", rst.getString("min_breach_count"));
				
				if (hmGUIDsMappedToSLAs.containsKey(strGUID)) {
					alSLAs = (ArrayList<JSONObject>) hmGUIDsMappedToSLAs.get(strGUID);
				} else {
					alSLAs = new ArrayList<JSONObject>();
					// obj by ref
					hmGUIDsMappedToSLAs.put(strGUID, alSLAs);
				}
				alSLAs.add(joCounter);
			}
			
			
		}catch(Exception ex) {			
			LogManager.errorLog(ex);
			ex.printStackTrace();
		}finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(rst);
			rst = null;
			
			UtilsFactory.clearCollectionHieracy(sbQry);
		}
//		System.out.println("jaCounters:"+jaCounters.toString());
		return hmGUIDsMappedToSLAs;
	}
	
	/**
	 * To get selected counters from counter master table 
	 * @param con
	 * @param joLicense 
	 * @param strGUID
	 * @return 
	 */
	public JSONArray getConfigCounters(Connection con, long lUID, JSONObject joLicense) { 
		StringBuilder sbQry = new StringBuilder();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		try {
			sbQry.append("select counter_id,query_string,execution_type,is_delta from counter_master_")
				.append(lUID)
				.append(" where is_selected = true")
				.append(" ORDER BY counter_id limit 10 ");
				
			//if(joLicense.getInt("apm_max_counters") != -1){
				//sbQry.append("LIMIT ").append(joLicense.getInt("apm_max_counters"));
			//}
			
			pstmt = con.prepareStatement(sbQry.toString());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				
				joCounter = new JSONObject();
				joCounter.put("counter_id", rs.getString("counter_id"));
				joCounter.put("query", rs.getString("query_string"));
				joCounter.put("executiontype", rs.getString("execution_type"));
				joCounter.put("isdelta", rs.getBoolean("is_delta"));

				jaCounters.add(joCounter);
				
				joCounter = null;
			}
			
			
		}catch(Exception ex) {			
			LogManager.errorLog(ex);
		}finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			DataBaseManager.close(rs);
			rs = null;
			
			UtilsFactory.clearCollectionHieracy(sbQry);
		}
		
		return jaCounters;
	}
	
	/**
	 * To update parent counter status
	 * @param con
	 * @param strGUID
	 * @param lUID
	 * @param joRecChildCounters
	 *
	public StringBuilder updateChildCounters(Connection con, String strGUID, long  lUID, JSONObject joRecChildCounters) {
		JSONArray jaAry = new JSONArray();
		StringBuilder sbReturn = null;
		JSONObject joParentCounterDetails = new JSONObject();
		try {
			
			jaAry = joRecChildCounters.getJSONArray("parentcounter");
			for(int i=0; i<jaAry.size(); i++){
				JSONObject joChild = jaAry.getJSONObject(i);
				
				int nParentCounterId = joChild.getInt("parentcounterid");
				
				
				
				// get the parent counter instance,category and countername
				joParentCounterDetails = getParentCounterDetails(con,lUID, nParentCounterId );
				
				joParentCounterDetails.put("parentcounterid", nParentCounterId);
				joParentCounterDetails.put("uid", lUID);
				
				//joParentCounterDetails.put("counter_template_id", rst.getLong("counter_template_id"));
				//joParentCounterDetails.put("user_id", rst.getLong("user_id"));
				//joParentCounterDetails.put("instance_name", rst.getString("instance_name"));
				//joParentCounterDetails.put("counter_name", rst.getString("counter_name"));
				//joParentCounterDetails.put("category", rst.getString("category"));
				
				if(joParentCounterDetails.getBoolean("success")) {
					updateParentCounter(con, joParentCounterDetails);				
				}
				
				// update parent counter 
				//updateParentCounter(con, lUID, nParentCounterId);
				//updateParentCounter(con, joParentCounterDetails);
				
				
				JSONArray jaChild = joChild.getJSONArray("childcounterdetail");
				
				for(int j=0; j<jaChild.size(); j++) {
					JSONObject joChildDetails = jaChild.getJSONObject(j);
					String strInstanceName = joChildDetails.getString("instancename");
					String strCounterName = joChildDetails.getString("countername");
					String strQuery = joChildDetails.getString("query");
					String strCategory = joChildDetails.getString("category");
					
					JSONObject joAppChildCounter = new JSONObject();
					joAppChildCounter.put("guid", strGUID);
					joAppChildCounter.put("queryString", strQuery);
					joAppChildCounter.put("executionType", "");
					joAppChildCounter.put("category", strCategory);
					joAppChildCounter.put("instance_name", strInstanceName);
					joAppChildCounter.put("counterName", strCounterName);
					joAppChildCounter.put("isDefault", false);
					joAppChildCounter.put("is_enabled", true);
					joAppChildCounter.put("has_instance", false);
					
					
					if(joParentCounterDetails.get("instance_name").toString().trim().equals("NA")) {
						joAppChildCounter.put("display_name", strCounterName);
						insertCounterMasterTable(con,joAppChildCounter,lUID,nParentCounterId);
					}else {
						if(!strInstanceName.equalsIgnoreCase(joParentCounterDetails.getString("instance_name")) && !strCounterName.equalsIgnoreCase(joParentCounterDetails.getString("counter_name")) &&  !strCategory.equalsIgnoreCase(joParentCounterDetails.getString("category"))) {
							joAppChildCounter.put("display_name", strCounterName+"-"+strInstanceName);
							insertCounterMasterTable(con,joAppChildCounter,lUID,nParentCounterId);
						}
					}
					
					
					
					joAppChildCounter = null;
					joChildDetails = null;
					strInstanceName = null;
					strCounterName = null;
					strQuery = null;
					
					UtilsFactory.clearCollectionHieracy(joChildDetails);
					UtilsFactory.clearCollectionHieracy(joAppChildCounter);
				}
				
			}
			sbReturn = UtilsFactory.getJSONSuccessReturn("Updated");
			
		}catch(Exception ex) {
			sbReturn = UtilsFactory.getJSONFailureReturn("Unable to update child counters. Try again later.");
			LogManager.errorLog(ex);
		}finally {
			
			UtilsFactory.clearCollectionHieracy(joParentCounterDetails);
			UtilsFactory.clearCollectionHieracy(jaAry);
		}
		
		return sbReturn;
	}*/
	
	/**
	 * 
	 * @param con
	 * @param luid
	 * @param nParentCounterId
	 * @return
	 * @throws Exception
	 */
	public JSONObject getParentCounterDetails(Connection con, long luid, int nParentCounterId) throws Exception {
		
		PreparedStatement pstmt = null;
		ResultSet rst = null;

		StringBuilder sbQuery = new StringBuilder();

		JSONObject joParentCounterDetails = new JSONObject();
		
		// Get counter template id & user id
		try {
			
			joParentCounterDetails.put("success", true);
			sbQuery.append("select  counter_template_id,user_id,case when instance_name is null then 'NA' else instance_name end ,counter_name,category,query_string  from counter_master_")
					.append(luid)
					.append(" where counter_id = "+nParentCounterId+"");
			pstmt = con.prepareStatement(sbQuery.toString());
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				
				
				joParentCounterDetails.put("counter_template_id", rst.getLong("counter_template_id"));
				joParentCounterDetails.put("user_id", rst.getLong("user_id"));				
				joParentCounterDetails.put("instance_name", rst.getString("instance_name"));
				joParentCounterDetails.put("counter_name", rst.getString("counter_name"));
				joParentCounterDetails.put("category", rst.getString("category"));
				joParentCounterDetails.put("queryString", rst.getString("query_string"));
			}
		}catch(Exception ex) {
			joParentCounterDetails.put("success", false);
			LogManager.errorLog(ex);
		}finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			DataBaseManager.close(rst);
			rst = null;
			UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return joParentCounterDetails;
	}
	
	
	
	
	/**
	 * To insert child counters of parent counter
	 * @param con
	 * @param joMasterCounter
	 * @param lUID
	 * @param nParentCounterId
	 * @throws Exception
	 */
	public void insertCounterMasterTable(Connection con, JSONObject joMasterCounter, long lUID, int nParentCounterId) throws Exception {
		
		PreparedStatement pstmt = null;
		StringBuilder sbQry = new StringBuilder();
		ResultSet rs = null;
		long lCounterTemId = 0l;
		long lUserId = 0l;
		
		try {
			
			// Get counter template id & user id 
			try {
				sbQry.append("select counter_template_id,user_id  from counter_master_")
				.append(lUID)
				.append(" where counter_id = "+nParentCounterId+"");
				pstmt = con.prepareStatement(sbQry.toString());
				rs = pstmt.executeQuery();
				
				while(rs.next()) {
					
					lCounterTemId = Long.parseLong(rs.getString("counter_template_id")) ;
					lUserId = Long.parseLong(rs.getString("user_id"));

				}
				
			}catch(Exception ex) {			
				LogManager.errorLog(ex);
			}finally {
				
				DataBaseManager.close(pstmt);
				pstmt = null;
				
				DataBaseManager.close(rs);
				rs = null;
				
				UtilsFactory.clearCollectionHieracy(sbQry);
			}
			
			
			sbQry .append("insert into counter_master_")
					.append(lUID)
					.append("(counter_template_id, ")
					.append("user_id , ")
					.append("guid , ")
					.append("query_string , ")
					.append("execution_type , ")
					.append("counter_name, ")
					.append("category , ")
					.append("is_selected, ")
					.append("has_instance, ")
					.append("is_enabled, ")
					.append("last_date_sent_to_agent, ")
					.append("created_on, ")
					.append("created_by, instance_name,display_name) ")
					
					.append("values(? , ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now(), ?, ?, ?)");
			
			pstmt = con.prepareStatement(sbQry.toString());
			
			pstmt.setLong(1, lCounterTemId);
			pstmt.setLong(2, lUserId);
			pstmt.setString(3, joMasterCounter.getString("guid"));
			pstmt.setString(4, joMasterCounter.getString("queryString"));
			pstmt.setString(5, joMasterCounter.getString("executionType"));
			pstmt.setString(6, joMasterCounter.getString("counterName"));
			pstmt.setString(7, joMasterCounter.getString("category"));			
			pstmt.setBoolean(8, joMasterCounter.getBoolean("isDefault"));			
			pstmt.setBoolean(9, joMasterCounter.getBoolean("has_instance"));
			pstmt.setBoolean(10, joMasterCounter.getBoolean("is_enabled"));
			pstmt.setLong(11, lUserId);
			pstmt.setString(12, joMasterCounter.getString("instance_name"));
			pstmt.setString(13, joMasterCounter.getString("display_name"));
			
			
			pstmt.execute();
		}catch(Exception e){
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(pstmt);
			pstmt = null;
			sbQry = null;
		}
	}
	
	/**
	 * to update counter master table for given parent counter
	 * @param con
	 * @param lUID
	 * @param nParentCounterId
	 */
	public void updateParentCounter(Connection con, JSONObject joParentCounterDetails) {
			// where instance name equla tonull
		StringBuilder sbQry = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			
			if(joParentCounterDetails.get("instance_name").toString().trim().equals("NA")) {
				sbQry.append("update counter_master_")
				.append(joParentCounterDetails.getLong("uid"))
				.append(" set is_selected = false , is_enabled=false   where counter_id ="+joParentCounterDetails.getLong("parentcounterid")+"");
			} else {
				sbQry.append("update counter_master_")
				.append(joParentCounterDetails.getLong("uid"))
				.append(" set has_instance=false , query_string = ?   where counter_id ="+joParentCounterDetails.getLong("parentcounterid")+"");
				
				pstmt.setString(1, joParentCounterDetails.get("queryString").toString().replaceAll("TRUE,", "FALSE,"));
			}
			pstmt = con.prepareStatement(sbQry.toString());
			pstmt.execute();
			
			LogManager.infoLog("counter_master_"+joParentCounterDetails.getLong("uid") + "Updated");
//			System.out.println("jaAry:"+jaAry.toString());
			
			
			//pstmt = con.prepareStatement(sbQry.toString());
			
		}catch(Exception ex) {
			LogManager.errorLog(ex);
		}finally {
			DataBaseManager.close(pstmt);
			pstmt = null;	
			
			UtilsFactory.clearCollectionHieracy(sbQry);
		}
		
	}
	
	
	
	/**
	 * To update the is_agent_updated & last_date_sent_to_agent in  counter master table 
	 * @param con
	 * @param strGUID
	 */
	public void updateStatusOfCounterMaster(Connection con, String strGUID) {
		
		StringBuilder sbQry = new StringBuilder();
		PreparedStatement pstmt = null;
		try {
			long lUid = getModuleUID(con, strGUID);
			sbQry.append("update counter_master_")
			.append(lUid)
			.append(" set last_date_sent_to_agent=now() , is_agent_updated=false where is_agent_updated= true");
			pstmt = con.prepareStatement(sbQry.toString());
			pstmt.execute();
			
//			System.out.println("counter_master_"+lUid + "Updated");
			
		}catch(Exception ex) {			
			LogManager.errorLog(ex);
		}finally {
			
			DataBaseManager.close(pstmt);
			pstmt = null;	
			
			UtilsFactory.clearCollectionHieracy(sbQry);
		}
		
	}
	
	public JSONObject getUserLicense(Connection con, String strGUID) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;

		StringBuilder sbQuery = new StringBuilder();

		JSONObject joUserLic = null;

		try {
			sbQuery.append("SELECT um.user_id, um.lic_apm FROM module_master mm INNER JOIN usermaster um ON um.user_id = mm.user_id AND mm.guid = ?");
			
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.setString(1, strGUID);
			rst = pstmt.executeQuery();
			
			if (rst.next()) {
				joUserLic = new JSONObject();
				joUserLic.put("user_id", rst.getLong("user_id"));
				joUserLic.put("lic_apm", rst.getString("lic_apm"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
		return joUserLic;
	}
	
	public JSONObject getLicenseAPMDetails(Connection con, JSONObject joUserLic) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rst = null;

		StringBuilder sbQuery = new StringBuilder();

		JSONObject joLicenseDetails = null;

		try {
			if( !joUserLic.getString("lic_apm").equalsIgnoreCase("level0") ){
				sbQuery.append("SELECT apm_max_agents, apm_max_counters FROM userwise_lic_monthwise WHERE user_id=? AND ")
						.append("module_type = 'APM' AND start_date::DATE <= now()::DATE AND ")
						.append("end_date::DATE >= now()::DATE");
			} else{
				sbQuery.append("SELECT apm_max_agents, apm_max_counters FROM usermaster WHERE user_id=? AND ")
						.append("apm_lic_start_date::DATE <= now()::DATE AND ")
						.append("apm_lic_end_date::DATE >= now()::DATE");
			}
			
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.setLong(1, joUserLic.getLong("user_id"));
			rst = pstmt.executeQuery();
			
			if (rst.next()) {
				joLicenseDetails = new JSONObject();
				joLicenseDetails.put("apm_max_agents", rst.getInt("apm_max_agents"));
				joLicenseDetails.put("apm_max_counters", rst.getInt("apm_max_counters"));
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
		return joLicenseDetails;
	}
	
	public ResultSet collectUpgradeGUID(Connection con) throws Exception {
		
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();

		JSONObject joUpgradeGUID = null;

		try {
			
			// sbQuery.append("select guid,download_url from module_master where version_updated=false order by uid limit 100");
			sbQuery.append("select guid from module_master where version_updated=false AND download_updated=false order by uid ");
			pstmt = con.prepareStatement( sbQuery.toString() );		
			
			return pstmt.executeQuery();	
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			
			//DataBaseManager.close(pstmt);
			//pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
		}
		
	}
	
	
	public JSONArray getSLACounters(Connection con, String strGUID) throws Exception {
		StringBuilder sbQuery = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		JSONArray jaCounters = new JSONArray();
		JSONObject joCounter = null;
		try{
			sbQuery = new StringBuilder();
			sbQuery .append("SELECT sla_id, created_by, counter_id, counter_template_id, is_above_threshold,threshold_value ")
					.append("FROM so_sla_counter ")
					.append("WHERE guid = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1, strGUID);
			rs = pstmt.executeQuery();
			
			while( rs.next() ){
				joCounter = new JSONObject();
				joCounter.put("slaid", rs.getLong("sla_id"));
				joCounter.put("userid", rs.getLong("created_by"));
				joCounter.put("counterid", rs.getLong("counter_id"));
				joCounter.put("countertempid", rs.getLong("counter_template_id"));
				joCounter.put("isabovethreshold", rs.getBoolean("is_above_threshold"));
				joCounter.put("thresholdvalue", rs.getLong("threshold_value"));
				
				jaCounters.add(joCounter);
				
				joCounter = null;
			}
		} catch (Exception ex) {
			LogManager.errorLog(ex);
			throw ex;
		} finally {
			DataBaseManager.close(rs);
			rs = null;
			
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			
			com.appedo.lt.utils.UtilsFactory.clearCollectionHieracy(sbQuery);
		}
		return jaCounters;
	}
	
}
