package com.appedo.lt.dbi;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.bean.LoadTestSchedulerBean;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

import net.sf.json.JSONObject;

public class QueueDBI {

	private static LinkedBlockingDeque<String> lbdReportDataQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> reportRunIDQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> freeUserQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> paidUserQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> jmeterQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> controllerQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque<String> jmeterProcessQueues = new LinkedBlockingDeque<String>();
	private static LinkedBlockingDeque< JSONObject > setupGrafanaQueues = new LinkedBlockingDeque< JSONObject >();
	
	// synchronized not needed as java will use the lock internally
	public void put(String runid, String reportData) throws Exception {
		Connection con = null;
		
		try {
			lbdReportDataQueues.put(reportData);
			
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_processing_q", 1);
			
			updateQueueLength(con, runid);
			
			LogManager.infoLog("Pushed the data into queue for run id :"+runid);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally{
			DataBaseManager.close(con);
		}
	}

	public String get() throws Exception {
		Connection con = null;
		String data = null;
		
		try{
			data = lbdReportDataQueues.poll();
			con = DataBaseManager.reEstablishConnection(con);
			
			if( data != null ) {
				updatePrivateCounters(con, "lt_processing_q", -1);
				LogManager.infoLog("Polled the data from queue");
			}
		} catch (Exception e) {
			//updatePrivateCounters(con, "lt_exec_services_queue",1);
			throw e;
		} finally {
			DataBaseManager.close(con);
		}
		
		return data;
	}

	public long size() throws Exception {
		try{
			return lbdReportDataQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}

	public void putRunID(String runid) throws Exception {
		Connection con = null;
		try {
			reportRunIDQueues.put(runid);
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_report_q",1);
			LogManager.infoLog("Pushed the run id into queue :"+runid);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}

	public String getRunID() throws Exception {
		Connection con = null;
		try{
			String data = reportRunIDQueues.poll();
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_report_q",-1);
			LogManager.infoLog("Polled the run id from queue");
			return data;
		} catch (Exception e) {
			//updatePrivateCounters(con, "lt_reporting_queue",1);
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}

	public long getRunIDQueueSize() throws Exception {
		try{
			return reportRunIDQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public String readHeader(InputStream stream) throws Throwable {
		StringBuffer instr = null;
		int c;
		try	{
			instr = new StringBuffer();
			while (true ) {
				c=stream.read();
				instr.append( (char) c);
				if(instr.toString().endsWith("\r\n\r\n") )
					break;
			}
		}catch(Throwable t)	{
			LogManager.errorLog(t);
			throw t;
		}
		return instr.toString();
	}

	public void updateQueueLength(Connection con,String runid) throws Exception {
		PreparedStatement pstmt = null;
		Date dateLog = LogManager.logMethodStart();
		StringBuilder sbQuery = new StringBuilder();
		
		try{
			sbQuery.append("UPDATE tblreportmaster SET queue_length = queue_length+1 WHERE runid = ").append(runid);
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.executeUpdate();	
			
		} catch(Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updatePrivateCounters(Connection con, String columnName, long queueValue ) throws Exception 
	{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();
		
		try {
			sbQuery.append("UPDATE appedo_pvt_counters SET pvt_counter_value=?, pvt_counter_status = true, last_upd_on = now() WHERE pvt_counter_name=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1,  queueValue);
			pstmt.setString(2, columnName);
			pstmt.executeUpdate();
		} catch (Exception e) {
			LogManager.errorLog(e, sbQuery);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
			
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateReportMasterLoadGenIPs(Connection con, long lRunId, String loadgen) {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();

			sbQuery	.append("update tblreportmaster set loadgenipaddresses = ?, status = ? WHERE runid = ? ");
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.setString(1, loadgen);
			pstmt.setString(2, "RUNNING");
			pstmt.setLong(3, lRunId);
			pstmt.executeUpdate();
			LogManager.infoLog("tblreportmaster table's loadgen updated");
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void putTestInQueue(LoadTestSchedulerBean testBean) throws Exception {
		Connection con = null;
		try {
			con = DataBaseManager.reEstablishConnection(con);
			
			testBean.setQueuedOn(new Date());
			LogManager.infoLog("Scheduling FG TEST  <> "+testBean.toJSON());
			if ( testBean.getTestType().equalsIgnoreCase("JMETER") ){
				jmeterQueues.put( testBean.toJSON().toString() );
			}else{
				if( testBean.getLicense().equalsIgnoreCase("level0") ){
					freeUserQueues.put(testBean.toJSON().toString());
					updatePrivateCounters(con, "lt_free_q", freeUserQueues.size());
				}else{
					paidUserQueues.put(testBean.toJSON().toString());
					updatePrivateCounters(con, "lt_paid_q", paidUserQueues.size());
					// update the updatePrivateCounters methods once amen changed the table
				}
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	} 
	
	public String pollFreeQueue() throws Exception {
		Connection con = null;
		try{
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_free_q", freeUserQueues.size());
			return freeUserQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}
	
	public String peekFreeQueue() throws Exception {
		try{
			return freeUserQueues.peek();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long freeQueueSize() throws Exception {
		try{
			return freeUserQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public String pollPaidQueue() throws Exception {
		Connection con = null;
		try{
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_paid_q", paidUserQueues.size());
			return paidUserQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}
	
	public String peekPaidQueue() throws Exception {
		try{
			return paidUserQueues.peek();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long paidQueueSize() throws Exception {
		try{
			return paidUserQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public String pollJmeterQueue() throws Exception {
		try{
			return jmeterQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public String peekJmeterQueue() throws Exception {
		try{
			return jmeterQueues.peek();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long jmeterQueueSize() throws Exception {
		try{
			return jmeterQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public void putInControllerQueue(LoadTestSchedulerBean testBean) throws Exception {
		Connection con = null;
		try {
			con = DataBaseManager.reEstablishConnection(con);
			
			testBean.setQueuedOn(new Date());
			LogManager.infoLog("Processing Queue <> "+testBean.toJSON());
			controllerQueues.put(testBean.toJSON().toString());
			updatePrivateCounters(con, "lt_controller_q", controllerQueues.size());
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	} 
	
	public String pollControllerQueue() throws Exception {
		Connection con = null;
		try{
			con = DataBaseManager.reEstablishConnection(con);
			
			updatePrivateCounters(con, "lt_controller_q", controllerQueues.size());
			return controllerQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}
	
	public String peekControllerQueue() throws Exception {
		try{
			return controllerQueues.peek();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long controllerQueueSize() throws Exception {
		try{
			return controllerQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public void putInJMeterProcessQueue(LoadTestSchedulerBean testBean) throws Exception {
		Connection con = null;
		try {
			con = DataBaseManager.reEstablishConnection(con);
			
			testBean.setQueuedOn(new Date());
			LogManager.infoLog("Processing Queue <> "+testBean.toJSON());
			jmeterProcessQueues.put(testBean.toJSON().toString());
			//updatePrivateCounters(con, "lt_controller_q", controllerQueues.size());
			//Update loadGen IP address in tblreportmaster table.
			updateReportMasterLoadGenIPs(con, testBean.getRunId(), testBean.getLoadgenIpAddress());
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	} 
	
	public String pollJMeterProcessQueue() throws Exception {
		Connection con = null;
		try{
			//con = DataBaseManager.reEstablishConnection(con);
			
			//updatePrivateCounters(con, "lt_controller_q", controllerQueues.size());
			return jmeterProcessQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}finally{
			DataBaseManager.close(con);
		}
	}
	
	public String peekJMeterProcessQueue() throws Exception {
		try{
			return jmeterProcessQueues.peek();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
	public long getJMeterProcessQueueSize() throws Exception {
		try{
			return jmeterProcessQueues.size();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}	
	}
	
	public void putInSetupGrafana(JSONObject joUserDetails) throws Exception {
		try {
			LogManager.infoLog("Setup Grafana Queue <> "+joUserDetails);
			
			setupGrafanaQueues.put(joUserDetails);
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	} 
	
	public JSONObject pollSetupGrafanaQueue() throws Exception {
		try{
			return setupGrafanaQueues.poll();
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		}
	}
	
/*	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException, InterruptedException{
        //get the localhost IP address, if server is running on some other IP, you need to use that
        InetAddress host = InetAddress.getLocalHost();
        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        JSONObject joTest = new JSONObject();
            //establish socket connection to server
            socket = new Socket("localhost", 8080);
            //write to socket using ObjectOutputStream
            oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Sending request to Socket Server");
            joTest.put("test1", 123);
            joTest.put("test2", 444);
            joTest.put("test3", "trrrrrrf");
            joTest.put("test4", 777);
            joTest.put("test5", "asdfasdf");
            
            oos.writeObject(joTest.toString());
            oos.writeObject("asdfaaaaaaaas\nasdfasdfasdfasdf\nasdfasdfasdfasd\n");
            
            //read the server response message
//            ois = new ObjectInputStream(socket.getInputStream());
//            String message = (String) ois.readObject();
//            System.out.println("Message: " + message);
//            //close resources
//            ois.close();
            oos.close();
    }*/

}
