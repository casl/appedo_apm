package com.appedo.lt.dbi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.appedo.commons.connect.DataBaseManager;
import com.appedo.lt.common.Constants;
import com.appedo.lt.utils.UtilsFactory;
import com.appedo.manager.LogManager;

public class RunDBI {

	public boolean isRecordAvailable(Connection con, String strRunId, String strScriptId) throws Exception{
		boolean bExist = false;
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("SELECT true from scriptwise_status where runid=? and script_id = ?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, Long.parseLong(strRunId));
			pstmt.setLong(2, Long.parseLong(strScriptId));
			
			rst = pstmt.executeQuery();
			
			if(rst.next()){
				bExist = true;
			}else{
				bExist = false;
			}
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			LogManager.logMethodEnd(dateLog);
		}
		return bExist;
	}
	
	public void updateScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("UPDATE  scriptwise_status SET  error_count=?, script_id=?, script_name=?, created_users=?, completed_users=?, http_200_count=?, http_300_count=?, http_400_count=?, http_500_count=?, is_completed=? WHERE runid= ? AND script_id=?");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, Integer.parseInt((String)hm.get("error_count")));
			pstmt.setLong(2,  Long.parseLong((String)hm.get("script_id")));
			pstmt.setString(3, (String)hm.get("script_name"));
			if(Integer.parseInt((String)hm.get("created_users")) == 0){
				pstmt.setNull(4, Types.INTEGER);
			} else{
				pstmt.setInt(4, Integer.parseInt((String)hm.get("created_users")));
			}
			if(Integer.parseInt((String)hm.get("completed_users")) == 0){
				pstmt.setNull(5, Types.INTEGER);
			} else{
				pstmt.setInt(5, Integer.parseInt((String)hm.get("completed_users")));
			}
			
			pstmt.setInt(6, Integer.parseInt((String)hm.get("http_200_count")));
			pstmt.setInt(7, Integer.parseInt((String)hm.get("http_300_count")));
			pstmt.setInt(8, Integer.parseInt((String)hm.get("http_400_count")));
			pstmt.setInt(9, Integer.parseInt((String)hm.get("http_500_count")));
			pstmt.setInt(10, Integer.parseInt((String)hm.get("is_completed")));
			pstmt.setInt(11, Integer.parseInt((String)hm.get("runid")));
			pstmt.setLong(12,  Long.parseLong((String)hm.get("script_id")));
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void insertScriptwiseStatus(Connection con, HashMap<String, Object> hm ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery.append("INSERT INTO  scriptwise_status(runid, error_count, script_id, script_name, created_users, completed_users, http_200_count, http_300_count, http_400_count, http_500_count, is_completed) ")
					.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setInt(1, Integer.parseInt((String)hm.get("runid")));
			pstmt.setInt(2, Integer.parseInt((String)hm.get("error_count")));
			pstmt.setLong(3, Long.parseLong((String)hm.get("script_id")));
			pstmt.setString(4, (String)hm.get("script_name"));
			if(Integer.parseInt((String)hm.get("created_users")) == 0){
				pstmt.setNull(5, Types.INTEGER);
			} else{
				pstmt.setInt(5, Integer.parseInt((String)hm.get("created_users")));
			}
			if(Integer.parseInt((String)hm.get("completed_users")) == 0){
				pstmt.setNull(6, Types.INTEGER);
			} else{
				pstmt.setInt(6, Integer.parseInt((String)hm.get("completed_users")));
			}
			
			pstmt.setInt(7, Integer.parseInt((String)hm.get("http_200_count")));
			pstmt.setInt(8, Integer.parseInt((String)hm.get("http_300_count")));
			pstmt.setInt(9, Integer.parseInt((String)hm.get("http_400_count")));
			pstmt.setInt(10, Integer.parseInt((String)hm.get("http_500_count")));
			pstmt.setInt(11, Integer.parseInt((String)hm.get("is_completed")));
			
			pstmt.executeUpdate();

		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateInActiveLoadAgent(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null, pstmtUpdate = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT instance_id FROM lt_run_gen_details WHERE run_id=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			sbQuery.setLength(0);
			
			while( rst.next() ){
				sbQuery = new StringBuilder();
				sbQuery.append("UPDATE lt_dynamic_load_agent_details SET instance_status='inactive' WHERE instance_id= ?");

				pstmtUpdate = con.prepareStatement(sbQuery.toString());
				pstmtUpdate.setString(1, rst.getString("instance_id"));
				pstmtUpdate.executeUpdate();
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
			
			throw e;
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close(pstmt);
			pstmt = null;
			DataBaseManager.close(pstmtUpdate);
			pstmtUpdate = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public long getRunQueueLength(Connection con, long runId) throws Throwable{
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		ResultSet rst = null;
		int queueLength = 0;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT queue_length FROM tblreportmaster WHERE runid=?");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, runId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				queueLength = rst.getInt("queue_length");
			}
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( rst );
			rst = null;
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	  return queueLength;
	}
	
	public void updateReportMaster(Connection con, long lRunId ) throws Exception {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			
			sbQuery	.append("update tblreportmaster SET is_active=false, runendtime = now(), status='REPORT GENERATION STARTED' ")
					.append("WHERE runid = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			
			pstmt.executeUpdate();
			
			UtilsFactory.clearCollectionHieracy( sbQuery );
	
		} catch (Exception e) {
			LogManager.errorLog(e);
			throw e;
		} finally {
			DataBaseManager.close(pstmt);
			pstmt = null;
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void createSummaryTable(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		long scriptId = 0;
		String scriptName = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT script_id, script_name FROM lt_run_script_details WHERE runid = ? ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			while(rst.next()) {
				scriptId = rst.getLong("script_id");
				scriptName = rst.getString("script_name");
				createView(con, lRunId, scriptId, scriptName);
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close(rst);
			rst = null;
			DataBaseManager.close( pstmt );
			pstmt = null;
			scriptName = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	private void createView(Connection con, long lRunId, long scriptId, String scriptName) throws Throwable {
		StringBuilder sbQuery = new StringBuilder();
		PreparedStatement pstmt = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT lt_dynamic_tables(?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			pstmt.setLong(2, scriptId);
			pstmt.setString(3, scriptName);
			pstmt.execute();
		} catch (Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void updateReportMasterLoadGen(Connection con, long lRunId, String loadgen) {
		
		PreparedStatement pstmt = null;
		StringBuffer sbQuery = null;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery = new StringBuffer();

			sbQuery	.append("update tblreportmaster set loadgenipaddresses = ?, status = ? WHERE runid = ? ");
			pstmt = con.prepareStatement( sbQuery.toString() );
			pstmt.setString(1, loadgen);
			pstmt.setString(2, "RUNNING");
			pstmt.setLong(3, lRunId);
			pstmt.executeUpdate();
			LogManager.infoLog("tblreportmaster table's loadgen updated");
		} catch(Exception e) {
			LogManager.errorLog(e);
		} finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void insertRunScriptDetails(Connection con, long lRunId, HashMap<Long, String> hmap) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("insert into lt_run_script_details (runid, script_id, script_name) ")
					.append("values (?, ?, ?)");
			pstmt = con.prepareStatement(sbQuery.toString());
			for (Map.Entry<Long, String> e: hmap.entrySet()){
				pstmt.setLong(1, lRunId);
				pstmt.setLong(2, e.getKey());
				pstmt.setString(3, e.getValue());
				pstmt.addBatch();
			}
			
			pstmt.executeBatch();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
		}finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public void insertRunDetails(Connection con, long lRunId, HashMap<Long, String> hmap, long scenarioId, String scenarioName, String userAgent) throws Throwable {
		PreparedStatement pstmt = null;
		StringBuilder sbQuery = new StringBuilder();
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery	.append("insert into lt_run_details (run_id, scenario_id, scenario_name, script_id, script_name, v_users, ramp_up, runtype, browser_cache, iteration, duration, created_on, browser_name, parallel_connections) ")
					.append(" (SELECT ?, ?, ?, ?, ?, (scenario_settings->>'maxuser')::bigint, scenario_settings->>'incrementtime', CASE WHEN (scenario_settings->>'type') = '1' THEN 'Iteration' ELSE 'Duration' END, ")
					.append("    (scenario_settings->>'browsercache')::boolean, (scenario_settings->>'iterations')::bigint, scenario_settings->>'durationtime', now(), ?, ")
					.append("    (CASE WHEN (scenario_settings->>'parallelconnections') IS NULL THEN ").append(Constants.DEFAULT_PARALLEL_CONNECTIONS).append(" ELSE (scenario_settings->>'parallelconnections')::smallint END) AS parallel_connections ")
					.append("  FROM lt_scenario_script_mapping ")
					.append("  WHERE scenario_id = ? ")
					.append("    AND script_id = ? ) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			for (Map.Entry<Long, String> e: hmap.entrySet()){
				pstmt.setLong(1, lRunId);
				pstmt.setLong(2, scenarioId);
				pstmt.setString(3, scenarioName);
				pstmt.setLong(4, e.getKey());
				pstmt.setString(5, e.getValue());
				pstmt.setString(6, userAgent);
				pstmt.setLong(7, scenarioId);
				pstmt.setLong(8, e.getKey());
				pstmt.addBatch();
			}
			
			pstmt.executeBatch();
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			e.printStackTrace();
		}finally {
			DataBaseManager.close( pstmt );
			pstmt = null;
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
	}
	
	public boolean autoSummaryReportPreparationScriptwiseContainerwiseResponse(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_preparation_scriptwise_containerwise_response(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_preparation_scriptwise_containerwise_response");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}

	public boolean autoSummaryReportGenerationScriptwiseContainerwiseResponse(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_generation_scriptwise_containerwise_response(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_generation_scriptwise_containerwise_response");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}

	public boolean autoSummaryReportPreparationTransactionError(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_preparation_transaction_error(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_preparation_transaction_error");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}

	public boolean autoSummaryReportGenerationTransactionError(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_generation_transaction_error(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_generation_transaction_error");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}

	public boolean autoSummaryReportGenerationRequestwise(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_generation_requestwise(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_generation_requestwise");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}
	

	public boolean autoSummaryReportPreparationRequestwise(Connection con, long lRunId) throws Throwable {
		PreparedStatement pstmt = null;
		ResultSet rst = null;
		StringBuilder sbQuery = new StringBuilder();
		Boolean status = false;
		Date dateLog = LogManager.logMethodStart();

		try {
			sbQuery.append("SELECT * FROM summary_report_preparation_requestwise(?) ");
			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setLong(1, lRunId);
			rst = pstmt.executeQuery();
			
			if(rst.next()) {
				status = rst.getBoolean("summary_report_preparation_requestwise");
			}
			
		} catch (Exception e) {
			LogManager.errorLog(e);
			status = false;
		} finally {
			DataBaseManager.close(rst);
			DataBaseManager.close( pstmt );
			UtilsFactory.clearCollectionHieracy( sbQuery );
			LogManager.logMethodEnd(dateLog);
		}
		
		return status;
	}
}
