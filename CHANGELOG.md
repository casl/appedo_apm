# Changelog
All notable changes to this project will be documented in this file.





### Release-Version: **R-3.456** [02-Sep-2020]

* Major Updates:
	- Implemented the visualize report scheduler Email.



### Release-Version: **R-3.455** [28-Aug-2020]

* Issues Fixed:
	- QAM-Id: 12998
	- QAM-Id: 12999



### Release-Version: **R-3.454** [26-Aug-2020]

* Major Updates:
	- Implemented the AWS instance creation till M5 series.
	- Converted the JMeter module code to Node js.



### Release-Version: **R-3.453** [17-Aug-2020]

* Issues Fixed:
	- QAM-Id: 12996



### Release-Version: **R-3.452** [13-Aug-2020]

* Major Updates:
	- Implemented the user deletion module.



### Release-Version: **R-3.451** [11-Aug-2020]

* Major Updates:
	- NSE SlowQuery issue Fixed - "error: dbMetricIns exception Unexpected token in JSON at position 9600 trace SyntaxError".

* Issues Fixed:
	- QAM-Id: 12981
	- QAM-Id: 12983
	- QAM-Id: 12978
	- QAM-Id: 12958
	- QAM-Id: 12947



### Release-Version: **R-2.450** [29-Jul-2020]

* Major Updates:
	- Monitoring of Java processes in Linux environment
	- JAVA Agent downloade processes added in the UI.
	- Migrated the Angular 10 in Evolution_2021.



### Release-Version: **R-2.449** [27-Jul-2020]

* Issues Fixed:
	- AVM agent SSL Request URL.



### Release-Version: **R-2.448** [21-Jul-2020]

* Major Updates:
	- Monitoring of Java processes in Windows environment

* Issues Fixed:
	- QAM-Id: 12927



### Release-Version: **R-2.447** [08-Jul-2020]

* Issues Fixed:
	- QAM-Id: 12926



### Release-Version: **R-2.446** [04-Jul-2020]

* Major Updates:
	- AVM Agent via Proxy service call.



### Release-Version: **R-2.445** [01-Jul-2020]

* Major Updates:
	- AVM Alert Mail and SMS triggered when the URL service Up & Down Status.



### Release-Version: **R-2.444** [17-Jun-2020]

* Major Updates:
	- Alert trigger when the min_breach count is the same or greater than.



### Release-Version: **R-2.443** [08-Jun-2020]

* Major Updates:
	- Monitoring the Websphere server in Linux Unification service



### Release-Version: **R-2.442** [03-Jun-2020]

* Issues Fixed:
	- QAM-Id: 12860



### Release-Version: **R-2.441** [21-May-2020]

* Major Updates:
	- Implemented the below mail service part in the Evolution-2019 Module_services.
		- Register verification mail.
		- Forgot password mail.
		- SLA Alert verification mail.
		- Enterprise Mapped user verification mail.
			- New user Mapped.
			- Existing user Mapped.



### Release-Version: **R-2.440** [18-May-2020]

* Major Updates:
	- Modification of the test name instead of test URL in the AVM Customized Alert Mail content.



### Release-Version: **R-2.439** [08-May-2020]

* Major Updates:
	- No need for any modification in the Linux agent configuration file during the auto-upgrade process.



### Release-Version: **R-2.438** [06-May-2020]

* Issues Fixed:
	- QAM-Id: 12809
	- QAM-Id: 12808



### Release-Version: **R-2.437** [04-May-2020]

* Major Updates:
	- Implemented Customized Alert feature.



### Release-Version: **R-2.436** [27-Apr-2020]

* Major Updates:
	- Implemented the Abort-Signal process, the process does the request is canceled if there is no response within the 60 sec.



### Release-Version: **R-2.435** [21-Apr-2020]

* Major Updates:
	- Auto Upgrade information automatically added to the custom log.

Issues Fixed:
	- QAM-Id: 12803



### Release-Version: **R-2.434** [20-Apr-2020]

* Major Updates:
	- Implemented the AUTO Upgrade feature in Linux Environment.



### Release-Version: **R-2.433** [03-Apr-2020]

* Major Updates:
	- For Oracle locks - the chart is not displayed in UI.

* Issues Fixed:
	- QAM-Id: 12804



### Release-Version: **R-2.432** [2-Apr-2020]

* Issues Fixed:
	- QAM-Id: 12804



### Release-Version: **R-2.431** [25-Mar-2020]

* Issues Fixed:
	- QAM-Id: 12802
	- QAM-Id: 12805
	- QAM-Id: 12806



### Release-Version: **R-2.430** [23-March-2020]

* Major Updates:
	- Implemented Lock Monitoring for PostgreSQL DB in Linux Unification Service.
	- MySQL Locks Not getting data in UI and DB.
	- FOR AVM & LOG, Alert emails are not receiving as per the settings configured in Alert settings (Alert trigger frequency in Mins). 
		Always receiving emails every 5 mins.



### Release-Version: **R-2.429** [23-Mar-2020]

* Major Updates:
	- ErrMsg:Value of '105' is not valid for 'Value'. 'Value' should be between 'minimum' and 'maximum'. -- Fixed. 
		This is caused while the progress bar value exceeds 100.
	- DevOps once uploaded please update the version number mentioned above to the current_release_versions table.



### Release-Version: **R-2.428** [20-Mar-2020]

* Major Updates:
	- SLA not working for Windows -- fixed.
	- DevOps once uploaded please update the version number mentioned above to the current_release_versions table.
	- Update to test instance first once cleared upgrade the same to production.



### Release-Version: **R-2.427** [19-Mar-2020]

* Issues Fixed:
	- QAM-Id: 12799



### Release-Version: **R-2.426** [16-Mar-2020]

* Issues Fixed:
	- QAM-Id: 12798



### Release-Version: **R-2.425** [08-Mar-2020]

* Major Updates:
	- Added code to manage deletion of module from ui
	- Added code to connect to collector service on the deletion of a module using node-fetch package.
	- Message spelling correction while doing updateModuleStatus on exception (testing might not able to test this)
	- Added a variable devEnvironment true as default for managing the url of the collector service to localhost or from table appedo_config_properties for property AVM_COLLECTOR_NODE
	- This to be added dev team and DevOps. DevOps team to set as false as a default value, so that test env and prod env will get the URL from the table.



### Release-Version: **R-2.424** [07-Mar-2020]

* Major Updates:
	- WindowsAgentSetup, AgentNotification, AgentAutoStartup are modified for Agent auto startup feature.
	- On-screen change, charts alignment is modified for better viewing. 
	- (Screen size less than 750px single chart per row. for 750px and above 2 chart per row. Tree view is always single chart per row)
	- AVM Last appedo received on status update not happening -- Fixed.
	- Duplicate charts created for Log module -- Fixed.



### Release-Version: **R-2.423** [06-Mar-2020]

* Major Updates:
	- Implemented Monitoring of WebLogic Admin and All Managed servers in the Linux Unification Service.



### Release-Version: **R-2.422** [05-Mar-2020]

* Major Updates:
	- Corrected the grok_column value in SMS alert.



### Release-Version: **R-2.421** [01-Mar-2020]

* Major Updates:
	- Incorrect counter value for total CPU percentage on Server Monitoring in Linux Unification Service.



### Release-Version: **R-2.420** [05-Mar-2020]

* Major Updates:
	- Already this module given and running currently.
	- Net stack path was not configured as expected by filebeat.yml and hence filebeat service not got started.
	- IIS access and error yml files were wrongly configured and it is fixed now.
	- Datalog changed the starting character metric to netsatck for better filtering.
	- For change to take place through auto-installer.
	- Added code for last updated on status for net stack
	- New ui added for viewing net stack charts
	- While net stack card is created, net_stack_func_<uid> table has not got created.



### Release-Version: **R-2.419** [28-Feb-2020]

* Major Updates:
	- Implemented Mysql Locking in Linux Unification Service.
	- Incorrect counter value for total cpu percentage on Server Monitoring in Linux Unification Service



### Release-Version: **R-2.418** [28-Feb-2020]

* Major Updates:
	- Issue related to Ping ICMP service
	- On-prem installation configuration implemented. 



### Release-Version: **R-2.417** [27-Feb-2020]

* Major Updates:
	- MSSQL slow query, procedure, locks, wait stat chart visual not got created
	- Agent Notification addition modules added. Now we can uninstall agent and dot net profiler start and stop through notification tray icon.
	- Windows Agent downloader introduced to ensure windows setup is upgraded when setup itself changes. Agent downloader will not have any code base and its job is to ensure it triggers the windows setup file.



### Release-Version: **R-2.416** [25-Feb-2020]

* Major Updates:
	- IIS module for both access and error, configuration added to Filebeat.
	- Parameter for the Data file path added to Filebeat.yml.
	- Service Status is used in agent file instead of process details. This will ensure to get the details of the version even if the service is in the stopped state.
	- Added more function to manage the configuration of metrics from ui to agent.



### Release-Version: **R-2.415** [19-Feb-2020]

* Major Updates:
	- The initial download will have an agent setup. It is a windows form-based process application
	- On starting, will get the system information, running versions and currently available versions of filebeat, winlogbeat, packetbeat and windows unified agent
	- Any difference in versions will start downloading the files for upgrade. Only the version that does not match will be downloaded.
	- For SAAS model, download happens from the S3 server and for on-prem download must happen from the local APM server.
	- There are few DB patches as mentioned below for the auto setup to work. Important tables are counter_type and current_release_versions(New table).
	- Added collection of all logical disk counters as default. (work to be done on the visibility of charts for those logical disks)
	- Auto-update feature when there is a change to the setup files - This is yet to be done will be part of the next release.
	- ICMP link test to be done.
	- Service availability test to be done.



### Release-Version: **R-2.414** [14-Feb-2020]

* Issues Fixed:
	- QAM-Id: 12725
	- QAM-Id: 12726
	- QAM-Id: 12729



### Release-Version: **R-2.413** [13-Feb-2020]

* Issues Fixed:
	- QAM-Id: 12671



### Release-Version: **R-2.412** [11-Feb-2020]

* Major Updates:
	- Implemented Monitoring of Mysql Server, SlowQueries and SlowProcedure in Linux Unification Service.(Mysql Locking is yet to be completed).

* Issues Fixed:
	- QAM-Id: 12693
	- QAM-Id: 12671
	- QAM-Id: 12670



### Release-Version: **R-2.411** [03-Feb-2020]

* Issues Fixed:
	- QAM-Id: 12698
	- QAM-Id: 12688
	- QAM-Id: 12687
	- QAM-Id: 12686
	- QAM-Id: 12678
	- QAM-Id: 12672
	- QAM-Id: 12671
	- QAM-Id: 12669
	- QAM-Id: 12693



### Release-Version: **R-2.410** [27-Jan-2020]

* Issues Fixed:
	- QAM-Id: 12667
	- QAM-Id: 12678



### Release-Version: **R-2.409** [22-Jan-2020]

* Issues Fixed:
	- QAM-Id: 12673



### Release-Version: **R-2.408** [10-Jan-2020]

* Major Updates:
	- Implemented Monitoring of WebLogic Server in Linux Unification Service.
	- Implemented New Module Services for Linux agent(OAD) in the Node side(Module Services for windows agent is yet to be completed).



